<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPaymentReceivedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_payment_receiveds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_id')->nullable()->constrained('admin_accounts');
            $table->foreignId('owner_id')->nullable()->constrained('owners');
            $table->foreignId('property_id')->nullable()->constrained('owner_properties');
            $table->boolean('status')->default(0)->comment('0-not-received, 1-received');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_payment_receiveds');
    }
}
