<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserPropertyDetailsToAgentTrackingUserTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agent_tracking_user_tour', function (Blueprint $table) {
            //
            $table->foreignId('user_property')->nullable()->constrained('user_properties')->after('status');
            $table->foreignId('booking_id')->nullable()->constrained('user_property_bookings')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agent_tracking_user_tour', function (Blueprint $table) {
            //
            $table->dropColumn('user_property');
            $table->dropColumn('booking_id');
        });
    }
}
