<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPropertyServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_property_service_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('user_property_id')->constrained('user_properties');
            $table->foreignId('service_id')->constrained('services');
            $table->date('date');
            $table->time('time');
            $table->bigInteger('inspection_team_id')->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->comment('0-Request has been send,1-Inspection completed,2-Payment done,3-Completed,4-Cancelled')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_property_service_requests');
    }
}
