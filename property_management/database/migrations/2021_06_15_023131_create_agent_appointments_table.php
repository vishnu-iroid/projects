<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('agent_id')->constrained('agents');
            $table->tinyInteger('type')->default(1)->comment('1 - User, 2- Owner');
            $table->date('date');
            $table->time('time');
            $table->foreignId('user_id')->nullable()->constrained('users');
            $table->foreignId('owner_id')->nullable()->constrained('owners');
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->unsignedBigInteger('request_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_appointments');
    }
}
