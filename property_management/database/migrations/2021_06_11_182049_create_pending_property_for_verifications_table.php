<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendingPropertyForVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_property_for_verifications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('agent_id')->nullable()->constrained('agents');
            $table->date('verification_date');
            $table->time('time');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_property_for_verifications');
    }
}
