<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertyServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_property_service_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('owner_id')->constrained('owners');
            $table->foreignId('owner_property_id')->constrained('owner_properties');
            $table->foreignId('service_id')->constrained('services');
            $table->date('date');
            $table->time('time');
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->comment('0-Request has been send,1-Inspection completed,2-Payment done,3-Completed,4-Cancelled')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_property_service_requests');
    }
}
