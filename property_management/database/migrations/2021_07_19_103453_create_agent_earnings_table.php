<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentEarningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_earnings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tour_id')->constrained('agent_tracking_user_tour');
            $table->foreignId('agent_id')->constrained('agents');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->decimal('amount',12,2);
            $table->tinyInteger('status')->default(0)->comment('0-active_requested, 1-accepted_requested');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_earnings');
    }
}
