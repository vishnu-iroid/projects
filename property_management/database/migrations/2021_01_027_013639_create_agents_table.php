<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->id();
            $table->string('name',40);
            $table->string('email',160)->unique();
            $table->string('phone',10)->unique();
            $table->string('image',250);
            $table->foreignId('country')->constrained();
            $table->foreignId('state')->constrained();
            $table->foreignId('city')->constrained();
            $table->foreignId('zip_code')->constrained('pincodes');
            $table->text('password')->nullable();
            $table->text('address',50);
            $table->decimal('latitude',20,8);
            $table->decimal('longitude',20,8);
            $table->string('account_number',160)->unique();
            $table->string('ifsc');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->boolean('status')->default(1)->comment('0-Inactive,1-Active');
            $table->text('api_token')->nullable();
            $table->foreignId('approved_by')->constrained('admins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
