<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContractFileToOwnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->string('contract_file',150)->nullable();
            $table->string('contract_no',150)->nullable();
            $table->string('contract_type',150)->nullable();
            $table->date('tenancy_start_date')->nullable();
            $table->date('tenancy_end_date')->nullable();
            $table->string('property_name',255)->nullable();
            $table->string('property_type',150)->nullable();
            $table->string('token_amount',150)->nullable();
            $table->string('property_meter_reading',150)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_bookings', function (Blueprint $table) {
            $table->dropColumn('approve_status');
        });
    }
}
