<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentRequestingContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_requesting_contract', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_property_id')->constrained('user_properties');
            $table->foreignId('agent_id')->constrained('agents');
            $table->timestamp('requested_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->boolean('status')->comment('0-inactive,1-active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_requesting_contract');
    }
}
