<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('country')->constrained();
            $table->foreignId('state')->constrained();
            $table->timestamps();
        });

        $array = [
            [ 'name' => 'Kochi', 'country' => 1, 'state' => 1, 'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'Kannur', 'country' => 1, 'state' => 1, 'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'TVM', 'country' => 1, 'state' => 1, 'created_at' => now(), 'updated_at' => now() ]
        ];

        DB::table('cities')->insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
