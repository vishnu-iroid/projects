<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserServiceRequestApprovelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_service_request_approvels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('request_id')->constrained('user_property_service_requests');
            $table->foreignId('owner_id')->constrained('owners');
            $table->tinyInteger('status')->default(0)->comment('0-active-request,1-accepted-request,2-rejected-request')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_service_request_approvels');
    }
}
