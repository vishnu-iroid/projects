<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDocumentStatusToUserPropertyBookingDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('user_property_booking_documents', function (Blueprint $table) {
            $table->integer('document_status')->comment('1-bank-pay,2-cash-pay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_booking_documents', function (Blueprint $table) {
            $table->dropColumn('document_status');
        });
    }
}
