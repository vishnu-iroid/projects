<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInspectionTeamToOwnerPropertyServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_property_service_requests', function (Blueprint $table) {
            //
            $table->foreignId('inspection_team')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_property_service_requests', function (Blueprint $table) {
            //
            $table->dropColumn('inspection_team');
        });
    }
}
