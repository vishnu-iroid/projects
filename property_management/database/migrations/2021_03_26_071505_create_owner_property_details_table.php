<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_property_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('detail_id')->constrained('details');
            $table->integer('value');
            $table->foreignId('owner_id')->constrained('details');
            $table->timestamps();

            $table->index('property_id');
            $table->index('owner_id');
            $table->index('detail_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_property_details');
    }
}
