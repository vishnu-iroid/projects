<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceAllocatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_allocates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('service_id')->constrained('services');
            $table->tinyInteger('status')->default(0)->comment('0-inactive,1-active')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_allocates');
    }
}
