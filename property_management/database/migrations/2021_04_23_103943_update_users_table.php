<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('country')->nullable()->change();
            $table->unsignedBigInteger('state')->nullable()->change();
            $table->unsignedBigInteger('city')->nullable()->change();
            $table->unsignedBigInteger('zip_code')->nullable()->change();
            $table->text('address')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('country')->nullable(false)->change();
            $table->unsignedBigInteger('state')->nullable(false)->change();
            $table->unsignedBigInteger('city')->nullable(false)->change();
            $table->unsignedBigInteger('zip_code')->nullable(false)->change();
            $table->text('address')->nullable(false)->change();
        });
    }
}
