<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email',160)->unique();
            $table->string('phone',10)->unique();
            $table->foreignId('country',50)->constrained();
            $table->foreignId('state',50)->constrained();
            $table->foreignId('city',50)->constrained();
            $table->foreignId('zip_code')->constrained('pincodes');
            $table->integer('otp')->unsigned()->nullable();
            $table->decimal('latitude',20,8)->nullable();
            $table->decimal('longitude',20,8)->nullable();
            $table->text('address');
            $table->boolean('isOwner')->default(0)->comment('0-No,1-Yes');
            $table->boolean('status')->default(1)->comment('0-Inactive,1-Active');
            $table->boolean('profile_status')->default(0)->comment('0-incomplete,1-completed');
            $table->text('api_token')->nullable();
            $table->timestamps();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
