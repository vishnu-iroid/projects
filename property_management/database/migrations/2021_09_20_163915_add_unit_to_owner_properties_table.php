<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitToOwnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->integer('unit_num')->nullable();
            $table->integer('floor_num')->nullable();
            $table->enum('commission_in',['amount','percentage'])->nullable();
            $table->bigInteger('commission')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->dropColumn('unit_num');
            $table->dropColumn('floor_num');
            $table->dropColumn('commission_in');
            $table->dropColumn('commission');
        });
    }
}
