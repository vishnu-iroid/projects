<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertyAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_property_amenities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('amenity_id')->constrained('amenities');
            $table->foreignId('amenities_category_id')->constrained('amenities_categories');
            $table->foreignId('owner_id')->constrained('owners');
            $table->timestamps();

            $table->index('property_id');
            $table->index('amenity_id');
            $table->index('amenities_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_property_amenities');
    }
}
