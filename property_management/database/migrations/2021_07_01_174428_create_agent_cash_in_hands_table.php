<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentCashInHandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_cash_in_hands', function (Blueprint $table) {
            $table->id();
            $table->foreignId('agent_id')->constrained('agents');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->unsignedBigInteger('payment_id');
            $table->tinyInteger('payment_type')->default(0)->comment('This may differ according to various payment');
            $table->decimal('payment_amount',10,2)->nullable();
            $table->tinyInteger('status')->default(0)->comment('1-cash-in-hand, 2-paid-to-admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_cash_in_hands');
    }
}
