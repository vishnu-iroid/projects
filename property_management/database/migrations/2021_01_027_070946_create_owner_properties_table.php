<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_properties', function (Blueprint $table) {
            $table->id();
            $table->string('property_reg_no',160)->unique();
            $table->foreignId('owner_id')->constrained('owners');
            $table->foreignId('agent_id')->nullable()->constrained();
            $table->boolean('property_to')->comment('0-rent,1-buy');
            $table->boolean('category')->comment('0-residential,1-commercial');
            $table->longText('street_address_1');
            $table->longText('street_address_2')->nullable();
            $table->string('city',150);
            $table->string('state',150);
            $table->foreignId('zip_code')->constrained('pincodes');
            $table->string('country',150);
            $table->decimal('latitude',20,8)->nullable();
            $table->decimal('longitude',20,8)->nullable();
            $table->boolean('occupied')->comment('0-vacant,1-occupied');
            $table->integer('furnished')->unsigned()->comment('0 - not furnished, 1- semi furnished , 2- fully furnished');
            $table->integer('frequency')->unsigned()->nullable()->comment('q-weekly,2-monthly,3-bimonthly,4-biweekly');
            $table->decimal('rent',10,2)->nullable();
            $table->decimal('selling_price',12,2)->nullable();
            $table->longText('description')->nullable();
            $table->boolean('status')->default(0)->comment('0-pending,1-admin_approved,2-admin_rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_properties');
    }
}
