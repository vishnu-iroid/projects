<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('country')->constrained();
            $table->timestamps();
        });

        $array = [
            [ 'name' => 'Kerala',  'country' => 1,  'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'Tamilnadu', 'country' => 1, 'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'GOA', 'country' => 1, 'created_at' => now(), 'updated_at' => now() ]
        ];

        DB::table('states')->insert($array);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
