<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendingPropertyAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_property_agents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('agent_id')->constrained('agents');
            $table->tinyInteger('status')->default(0)->comment('0-Pending,1-Accepted,2-Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_property_agents');
    }
}
