<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountToOfferPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_packages', function (Blueprint $table) {
            $table->decimal('actual_amount', 12, 2)->nullable()->default(0.00);
            $table->decimal('discount_amount', 12, 2)->nullable()->default(0.00);
            $table->dropColumn('no_of_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_packages', function (Blueprint $table) {
            $table->dropColumn('actual_amount');
            $table->dropColumn('discount_amount');
            $table->bigInteger('no_of_days')->nullable()->after('end_date');
        });
    }
}
