<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnInOwnerProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->renameColumn('tenancy_start_date', 'contract_start_date');
            $table->renameColumn('tenancy_end_date','contract_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->renameColumn('contract_start_date', 'tenancy_start_date');
            $table->renameColumn('contract_end_date','tenancy_end_date');
        });
    }
}
