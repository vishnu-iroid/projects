<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOwnerAmountToUserPropertyServiceRequestDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_service_request_documents', function (Blueprint $table) {
           $table->integer('owner_amount')->nullable();
           $table->integer('customer_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_service_request_documents', function (Blueprint $table) {
            $table->dropColumn('owner_amount');
            $table->dropColumn('customer_amount');
        });
    }
}
