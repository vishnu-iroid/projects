<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInspectionTeamToUserPropertyServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_service_requests', function (Blueprint $table) {
            //
            $table->bigInteger('inspection_team')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_service_requests', function (Blueprint $table) {
            //
            $table->dropColumn('inspection_team');
        });
    }
}
