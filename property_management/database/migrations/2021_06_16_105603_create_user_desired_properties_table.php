<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDesiredPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_desired_properties', function (Blueprint $table) {
            $table->id();
            $table->string('request_id_no',160)->unique();
            $table->foreignId('user_id')->constrained('users');
            $table->boolean('property_to')->comment('0-rent,1-buy');
            $table->boolean('category')->comment('0-residential,1-commercial');
            $table->foreignId('type_id')->constrained('property_types');
            $table->integer('area');
            $table->date('request_date')->nullable();
            $table->decimal('min_price',12,2)->nullable();
            $table->decimal('max_price',12,2)->nullable();
            $table->decimal('latitude',20,8)->nullable();
            $table->decimal('longitude',20,8)->nullable();
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0-active-request,1-closed-request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_desired_properties');
    }
}
