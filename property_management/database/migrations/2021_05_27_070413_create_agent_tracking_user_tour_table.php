<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentTrackingUserTourTable extends Migration
{
    /**user_tour_id
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_tracking_user_tour', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tour_id')->constrained('user_tour_bookings');
            $table->foreignId('agent_id')->constrained('agents');
            $table->integer('status')->unsigned()->comment('1 - vist-started, 2- proceed-to-booking , 3- paid-token-amount, 4- Request-for-contract, 5-Full Paid or Key Handovered ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_tracking_user_tour');
    }
}
