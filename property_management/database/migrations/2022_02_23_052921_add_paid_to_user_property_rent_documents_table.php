<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaidToUserPropertyRentDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_rent_documents', function (Blueprint $table) {
            //
            $table->string('payment_check')->default('0')->comment('Pending - 0 , Accept - 1, Reject -2')->after('payment_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_rent_documents', function (Blueprint $table) {
            //
            $table->dropColumn('payment_check');

        });
    }
}
