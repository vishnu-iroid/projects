<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPropertyDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_property_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_property_id')->constrained('user_properties');
            $table->foreignId('user_id')->constrained();
            $table->string('document',150);
            $table->integer('payment_status')->nullable()->comment('1-bank-pay,2-cash-pay');
            $table->integer('document_type')->nullable()->comment('1-payment,2-certificate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_property_documents');
    }
}
