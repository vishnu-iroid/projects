<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyTypeDetailsContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_type_details_contents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('detail_id')->constrained('details');
            $table->foreignId('type_id')->constrained('property_types');
            $table->timestamps();

            $table->index('detail_id');
            $table->index('type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_type_details_contents');
    }
}
