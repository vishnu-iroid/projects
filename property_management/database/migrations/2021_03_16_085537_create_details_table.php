<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->integer('sort_order');
            $table->string('placeholder',100);
            $table->string('icon',150);
            $table->timestamps();
        });
        
        $array = [
            [ 'name' => 'floors', 'sort_order' => 1, 'placeholder' =>  'Number of floors' , 'icon' => 'wdwd', 'created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'apartments', 'sort_order' => 2, 'placeholder' =>  'Number of apartments' ,'icon' => 'wdwd','created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'area', 'sort_order' => 3, 'placeholder' => 'Area(Sq.ft)' ,'icon' => 'wdwd','created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'Beds', 'sort_order' => 4, 'placeholder' => 'Number of Bedrooms' ,'icon' => 'wdwd','created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'Bathroom', 'sort_order' => 5, 'placeholder' => 'Number of Bathrooms' ,'icon' => 'wdwd','created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'conference_hall', 'sort_order' => 6, 'placeholder' => 'Max.Capacity' ,'icon' => 'wdwd','created_at' => now(), 'updated_at' => now() ],
            [ 'name' => 'Restaurant', 'sort_order' => 7, 'placeholder' => 'Dining Area' ,'icon' => 'wdwd','created_at' => now(), 'updated_at' => now() ],
            
        ];

        DB::table('details')->insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
    }
}
