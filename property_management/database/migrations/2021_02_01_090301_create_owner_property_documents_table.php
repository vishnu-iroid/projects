<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertyDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_property_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->string('document',150);
            $table->tinyInteger('type')->comment('0-Image,1-video,2-floorplan');
            $table->foreignId('owner_id')->constrained('owners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_property_documents');
    }
}
