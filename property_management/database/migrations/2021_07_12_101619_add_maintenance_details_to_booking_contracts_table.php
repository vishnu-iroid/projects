<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaintenanceDetailsToBookingContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_contracts', function (Blueprint $table) {
            //
            $table->string('total_free_maintenance', 100)->nullable()->after('contract_end_date');
            $table->string('completed_maintenance', 100)->nullble()->after('contract_end_date');
            $table->string('maintenance_charge', 100)->nullble()->after('contract_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_contracts', function (Blueprint $table) {
            //
            $table->dropColumn('total_free_maintenance');
            $table->dropColumn('completed_maintenance');
            $table->dropColumn('maintenance_charge');
        });
    }
}
