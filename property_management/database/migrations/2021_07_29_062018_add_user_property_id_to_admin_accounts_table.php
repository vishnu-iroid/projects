<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserPropertyIdToAdminAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_accounts', function (Blueprint $table) {
            $table->foreignId('user_property_id')->nullable()->after('user_id')->constrained('user_properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_accounts', function (Blueprint $table) {
            $table->dropForeign('admin_accounts_user_property_id_foreign');
            $table->dropColumn('user_property_id');
        });
    }
}
