<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserPropertyIdToBookingContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_contracts', function (Blueprint $table) {
            $table->dropForeign('booking_contracts_booking_id_foreign');
            $table->dropColumn('booking_id');
            $table->foreignId('user_property_id')->nullable()->after('user_id')->constrained('user_properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_contracts', function (Blueprint $table) {
            $table->dropForeign('booking_contracts_user_property_id_foreign');
            $table->dropColumn('user_property_id');
            $table->foreignId('booking_id')->nullable()->after('user_id')->constrained('user_property_bookings');
        });
    }
}
