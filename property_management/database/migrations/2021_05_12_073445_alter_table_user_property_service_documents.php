<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUserPropertyServiceDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_service_request_documents', function (Blueprint $table) {
            $table->integer('payment_status')->nullable()->comment('1-bank-pay,2-cash-pay')->after('request_id');
            $table->integer('document_type')->default(0)->comment('0-service,1-payment,2-esitimate-doc')->after('request_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_service_request_documents', function (Blueprint $table) {
            $table->dropColumn('payment_status');
            $table->dropColumn('document_type');
        });
    }
}
