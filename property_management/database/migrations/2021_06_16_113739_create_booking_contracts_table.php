<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_contracts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('booking_id')->constrained('user_property_bookings');
            $table->foreignId('user_id')->constrained('users');
            $table->integer('contract_status')->nullable()->comment('1-user-booking,2-agent-booking');
            $table->string('contract_no', 150)->nullable();
            $table->string('contract_type', 150)->nullable();
            $table->date('contract_start_date')->nullable();
            $table->date('contract_end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_contracts');
    }
}
