<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApproveStatusToUserPropertyBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_bookings', function (Blueprint $table) {
            $table->boolean('approve_status')->default(0)->after('coupoun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_bookings', function (Blueprint $table) {
            $table->dropColumn('approve_status');
        });
    }
}
