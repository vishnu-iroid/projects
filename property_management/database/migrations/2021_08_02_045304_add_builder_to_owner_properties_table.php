<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBuilderToOwnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->boolean('is_builder')->after('property_reg_no')->nullable()->default(false);
            $table->decimal('owner_amount',12,2)->nullable();
            $table->bigInteger('builder_id')->nullable();
            // $table->boolean('property_to')->nullable()->change();
            // $table->longText('street_address_1')->nullable()->change();
            // $table->string('city')->nullable()->change();
            // $table->string('state')->nullable()->change();
            // $table->string('city')->nullable()->change();
            $table->integer('furnished')->nullable()->change();
            $table->boolean('occupied')->nullable()->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->dropColumn('is_builder');
            $table->dropColumn('builder_id');
            $table->dropColumn('owner_amount');

            $table->integer('furnished')->nullable(false)->change();
            $table->boolean('occupied')->nullable(false)->change();
        });
    }
}
