<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentCheckToUserPropertyFullAmountDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_full_amount_documents', function (Blueprint $table) {
            //
            $table->string('payment_check')->default('0')->comment('Pending - 0 , Accept - 1, Reject -2')->after('document_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_full_amount_documents', function (Blueprint $table) {
            //
            $table->dropColumn('payment_check');
        });
    }
}
