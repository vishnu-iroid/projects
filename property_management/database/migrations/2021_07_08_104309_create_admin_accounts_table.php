<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_accounts', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('amount_type')->default(0)->comment('1-revenue , 2-expense');
            $table->decimal('amount', 12, 2);
            $table->integer('payment_type')->comment('There are many types of payments');
            // 1 - user service payment
            // 2 - user token payment
            // 3 - user security payment
            // 4 - user full amount payment
            // 5 - user rent payment
            // 6 - owner service payment
            // 7 - owner property payment
            // 8 - maintenance_amount payment
            // 9 - agent commission
            $table->bigInteger('reference_id');
            $table->foreignId('property_id')->nullable()->constrained('owner_properties');
            $table->foreignId('owner_id')->nullable()->constrained('owners');
            $table->foreignId('agent_id')->nullable()->constrained('agents');
            $table->foreignId('user_id')->nullable()->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_accounts');
    }
}
