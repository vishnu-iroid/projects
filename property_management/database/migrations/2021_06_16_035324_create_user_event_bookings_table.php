<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEventBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_event_bookings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('package_id')->constrained('event_packages');
            $table->foreignId('user_id')->constrained('users');
            $table->string('name');
            $table->string('email', 160);
            $table->string('phone', 16);
            $table->decimal('price',12,2);
            $table->integer('person_count')->unsigned();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_event_bookings');
    }
}
