<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerPropertyServiceRequestDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_property_service_request_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('request_id')->constrained('owner_property_service_requests');
            $table->integer('payment_status')->nullable()->comment('1-bank-pay,2-cash-pay');
            $table->integer('document_type')->default(0)->comment('1-payment,0-service');
            $table->string('document',150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_property_service_request_documents');
    }
}
