<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAgentPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_agent_properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tour_id')->constrained('user_tour_bookings');
            $table->foreignId('agent_id')->constrained('agents');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->tinyInteger('status')->default(0)->comment('0- Pending, 1 - Accepted, 2- Rejected ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_agent_properties');
    }
}
