<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContratUserToOwnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->enum('contract_owner', ['0', '1'])->default(0)->comment('0-contract-not-created,1-contract-created');
            $table->enum('contract_user', ['0', '1'])->default(0)->comment('0-contract-not-created,1-contract-created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            //
        });
    }
}
