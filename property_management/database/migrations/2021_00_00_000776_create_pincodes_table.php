<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePincodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pincodes', function (Blueprint $table) {
            $table->id();
            $table->string('pincode',30)->unique();
            $table->foreignId('city')->constrained();
            $table->timestamps();

            $table->index('pincode');
        });
        $array =[
            [ 'pincode' => '683106', 'city' => 1, 'created_at' => now(), 'updated_at' => now() ],
            [ 'pincode' => '683101', 'city' => 1, 'created_at' => now(), 'updated_at' => now() ],
            [ 'pincode' => '683103', 'city' => 1, 'created_at' => now(), 'updated_at' => now() ]
        ];
        DB::table('pincodes')->insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pincodes');
    }
}
