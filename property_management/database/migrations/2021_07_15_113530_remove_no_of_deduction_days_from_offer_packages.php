<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveNoOfDeductionDaysFromOfferPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_packages', function (Blueprint $table) {
            //
            $table->dropColumn('no_of_deduction_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_packages', function (Blueprint $table) {
            //
            $table->string('no_of_deduction_days', 100);
        });
    }
}
