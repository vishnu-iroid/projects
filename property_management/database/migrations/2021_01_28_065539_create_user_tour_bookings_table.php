<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTourBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tour_bookings', function (Blueprint $table) {
            $table->id();
            $table->string('booking_id',30)->unique();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->date('booked_date');
            $table->string('time_range');
            $table->timestamps();

            $table->index('booking_id');
            $table->index('user_id');
            $table->index('property_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tour_bookings');
    }
}
