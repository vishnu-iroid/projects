<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceProviderToOwnerPropertyServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_property_service_requests', function (Blueprint $table) {
            $table->foreignId('service_provider')->nullable()->constrained('service_providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_property_service_requests', function (Blueprint $table) {
            $table->dropForeign('owner_property_service_requests_service_provider_foreign');
            $table->dropColumn('service_provider');
        });
    }
}
