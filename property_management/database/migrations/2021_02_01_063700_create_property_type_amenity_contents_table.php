<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyTypeAmenityContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_type_amenity_contents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_id')->constrained('property_types');
            $table->foreignId('amenity_category_id')->constrained('amenities_categories');
            $table->foreignId('amenity_id')->constrained('amenities');
            $table->timestamps();
            
            $table->index('type_id');
            $table->index('amenity_category_id');
            $table->index('amenity_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_type_amenity_contents');
    }
}
