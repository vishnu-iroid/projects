<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyEmiDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_emi_details', function (Blueprint $table) {
            $table->id();
            $table->integer('property_id');
            $table->enum('period_type',['days','months','years']);
            $table->integer('emi_count');
            $table->integer('emi_amount');
            $table->integer('interest_rate');
            $table->string('descriptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_emi_details');
    }
}
