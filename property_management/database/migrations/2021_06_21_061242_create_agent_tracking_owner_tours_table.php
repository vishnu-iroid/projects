<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentTrackingOwnerToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_tracking_owner_tours', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tour_id')->constrained('pending_property_for_verifications');
            $table->foreignId('agent_id')->constrained('agents');
            $table->integer('status')->unsigned()->comment('1 - vist-started, 2- edit/submit details , 3- Admin Approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_tracking_owner_tours');
    }
}
