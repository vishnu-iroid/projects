<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFrequencyDetailsToOfferPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offer_packages', function (Blueprint $table) {
            //
            $table->foreignId('frequency_id')->nullable()->constrained('frequencies')->after('end_date');
            $table->foreignId('property_id')->nullable()->constrained('owner_properties')->after('end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offer_packages', function (Blueprint $table) {
            //
            $table->dropColumn('frequency_id');
            $table->dropColumn('property_id');
        });
    }
}
