<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->id();
            $table->string('tenants_id', 160)->unique();
            $table->foreignId('booking_id')->constrained('user_property_bookings');
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('contract_id')->constrained('booking_contracts');
            $table->boolean('status')->default(0)->comment('0-active,1-inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
