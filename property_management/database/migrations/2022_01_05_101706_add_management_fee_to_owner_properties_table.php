<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddManagementFeeToOwnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            $table->enum('property_management_fee_type',array('amount', 'percentage'))->default('amount')->nullable()->after('commission');
            $table->bigInteger('management_fee')->after('property_management_fee_type')->nullable();
            $table->bigInteger('ads_number')->after('management_fee')->nullable();
            $table->bigInteger('guards_number')->after('ads_number')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('owner_properties', function (Blueprint $table) {
            //
            $table->dropColumn('property_management_fee_type');
            $table->dropColumn('management_fee');
            $table->dropColumn('ads_number');
            $table->dropColumn('guards_number');
        });
    }
}
