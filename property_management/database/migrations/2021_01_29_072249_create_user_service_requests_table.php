<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_service_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->foreignId('service_id')->constrained();
            $table->date('date');
            $table->string('time',20);
            $table->longText('address');
            $table->text('location');
            $table->longText('description');
            $table->tinyInteger('status')->comment('0-Request has been send,1-Inspection completed,2-Payment done,3-Completed,4-Cancelled')->unsigned();
            $table->timestamps();
            
            $table->index('user_id');
            $table->index('property_id');
            $table->index('service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_service_requests');
    }
}
