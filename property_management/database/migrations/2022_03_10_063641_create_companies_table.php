<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email',160)->unique();
            $table->string('phone',10)->unique();           
            $table->text('address');
            $table->string('contact_person');
            $table->string('website',160)->unique();
            $table->string('register_no',10)->unique();
            $table->string('vat',10)->unique();
            $table->string('other_info');
            $table->string('logo',120);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
