<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_packages', function (Blueprint $table) {
            $table->id();
            $table->string('offer_package_name', 100);
            $table->date('start_date');
            $table->date('end_date');
            $table->bigInteger('no_of_days');
            $table->bigInteger('no_of_deduction_days');
            $table->integer('status')->comment('1-active,0-inactive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_packages');
    }
}
