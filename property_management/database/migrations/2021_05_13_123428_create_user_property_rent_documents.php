<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPropertyRentDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_property_rent_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_property_id')->constrained('user_properties');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->string('document',150);
            $table->integer('payment_status')->nullable()->comment('1-bank-pay,2-cash-pay');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_property_rent_documents');
    }
}
