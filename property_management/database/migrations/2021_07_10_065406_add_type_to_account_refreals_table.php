<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToAccountRefrealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_refreals', function (Blueprint $table) {
            $table->integer('type')->comment('There are many types of payments');
            // 1- user service id
            // 2- user token  id
            // 3- user security  id
            // 4- user full amount  id
            // 5- user rent  id
            // 6- owner service id
            // 7- owner property id

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_refreals', function (Blueprint $table) {
            //
            $table->dropColumn('type');
        });
    }
}
