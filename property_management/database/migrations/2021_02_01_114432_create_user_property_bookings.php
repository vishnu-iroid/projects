<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPropertyBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_property_bookings', function (Blueprint $table) {
            $table->id();
            $table->string('booking_id',30)->unique();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('property_id')->constrained('owner_properties');
            $table->date('check_in');
            $table->date('check_out');
            $table->string('coupoun',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_property_bookings');
    }
}
