<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email',160)->unique();
            $table->string('phone',10)->unique();
            $table->foreignId('country',50)->constrained();
            $table->foreignId('state',50)->constrained();
            $table->foreignId('city',50)->constrained();
            $table->foreignId('zip_code')->constrained('pincodes')->nullable();
            $table->text('password')->nullable();
            $table->string('profile_image',120);
            $table->string('id_image',120)->nullable();
            $table->decimal('latitude',20,11)->nullable();
            $table->decimal('longitude',20,11)->nullable();
            $table->text('address');
            $table->string('account_number',50)->nullable();
            $table->string('branch_name',80)->nullable();
            $table->string('ifsc',60)->nullable();
            $table->integer('rental_properties')->unsigned();
            $table->integer('sale_properties')->unsigned();
            $table->boolean('status')->default(1)->comment('0-Inactive,1-Active');
            $table->text('api_token')->nullable();
            $table->boolean('profile_completed')->default(0);
            $table->timestamps();

            $table->index('id');
            $table->index('email');
            $table->index('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
