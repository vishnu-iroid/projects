<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('first_name',50);
            $table->string('last_name',50);
            $table->string('employee_id',50)->nullable();
            $table->foreignId('country',50)->constrained();
            $table->foreignId('state',50)->constrained();
            $table->foreignId('city',50)->constrained();
            $table->foreignId('zip_code')->constrained('pincodes');
            $table->text('address');
            $table->string('phone',10)->unique();
            $table->string('email',160)->unique();
            $table->string('password',100);
            $table->foreignId('department')->constraint();
            $table->foreignId('designation')->constraint();
            $table->integer('role')->unsigned()->comment('1-superadmin,2-documentation,3-accounts,4-service');
            $table->text('token')->nullable();
            $table->boolean('active')->default(1)->comment('0-inactive,1-active');
            $table->timestamps();
        });

        DB::table('admins')->insert([
            'first_name' => 'super',
            'last_name' => 'admin',
            'country' => '1',
            'state' => '1',
            'city' => '1',
            'zip_code' => '1',
            'address' => 'spwhs road ambattukavu',
            'phone' => '9961000000',
            'email' => 'admin@property.com',
            'password' => bcrypt('admin@123'),
            'department' => 1,
            'designation' => 1,
            'role' => 1,
            'email' => 'admin@property.com',
            'active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
