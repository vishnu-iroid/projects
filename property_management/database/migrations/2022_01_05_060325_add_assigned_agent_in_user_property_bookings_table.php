<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssignedAgentInUserPropertyBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_bookings', function (Blueprint $table) {
            $table->foreignId('assigned_agent')->nullable()->constrained('agents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_bookings', function (Blueprint $table) {
            $table->dropForeign('user_property_bookings_assigned_agent_foreign');
            $table->dropColumn('assigned_agent');
        });
    }
}
