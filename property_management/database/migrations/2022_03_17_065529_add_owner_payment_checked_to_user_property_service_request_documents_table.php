<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOwnerPaymentCheckedToUserPropertyServiceRequestDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_property_service_request_documents', function (Blueprint $table) {
            $table->integer('owner_payment_checked')->after('is_verified')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_property_service_request_documents', function (Blueprint $table) {
            $table->dropColumn('owner_payment_checked');
        });
    }
}
