<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEventBookingDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_event_booking_documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_booking_id')->constrained('user_event_bookings');
            $table->foreignId('user_id')->constrained('users');
            $table->integer('payment_status')->nullable()->comment('1-bank-pay,2-cash-pay');
            $table->string('document',150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_event_booking_documents');
    }
}
