<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\admin\LoginController;
use App\Http\Controllers\admin\NotificationController;
use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\DepartmentController;
use App\Http\Controllers\admin\DesignationController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\AgentController;
use App\Http\Controllers\admin\PropertyTypeController;
use App\Http\Controllers\admin\AmenityController;
use App\Http\Controllers\admin\AmenityCategoryController;
use App\Http\Controllers\admin\PropertyController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\OwnerController;
use App\Http\Controllers\admin\ServiceController;
use App\Http\Controllers\admin\ResetPasswordController;
use App\Http\Controllers\admin\RegionController;
use App\Http\Controllers\admin\DetailsController;
use App\Http\Controllers\admin\PublicController;
use App\Http\Controllers\admin\MasterController;
use App\Http\Controllers\admin\UserBookingController;
use App\Http\Controllers\admin\RequestInspectionController;
use App\Http\Controllers\admin\OwnerPropertyController;
use App\Http\Controllers\admin\propertyManagementController;
use App\Http\Controllers\admin\TermsConditionsController;
use App\Http\Controllers\admin\PrivacyController;
use App\Http\Controllers\admin\LegalController;
use App\Http\Controllers\admin\AboutController;
use App\Http\Controllers\admin\FaqController;
use App\Http\Controllers\admin\FeedbackController;
use App\Http\Controllers\admin\UserRequestController;
use App\Http\Controllers\admin\EventsController;
use App\Http\Controllers\admin\OwnerRequestController;
use App\Http\Controllers\admin\TenantController;
use App\Http\Controllers\admin\ReportController;
use App\Http\Controllers\admin\BecomeOwnerController;
use App\Http\Controllers\admin\DesiredPropertyController;
use App\Http\Controllers\admin\IncomeController;
use App\Http\Controllers\admin\ExpenseController;
// use App\Models\InsecptionTeam;
use App\Http\Controllers\admin\OfferPackageController;
use App\Http\Controllers\admin\RewardsController;
use App\Http\Controllers\admin\VacateRequestController;
use App\Http\Controllers\admin\OwnerBuildingController;
use App\Http\Controllers\DummyDepartmentController;
use App\Http\Controllers\admin\RoleController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\LeaseTypeController;

Route::get('/', [LoginController::class, 'showLogin'])->name('admin.login');

 // Route::any('/login', [LoginController::class, 'showLogin'])->name('showLogin');
Route::any('/login', [LoginController::class, 'login'])->name('login');
Route::post('/password-reset', [ResetPasswordController::class, 'adminPasswordReset'])->name('adminPasswordReset');
Route::get('/get-cities/{stateId}', [PublicController::class, 'getCity'])->name('getCity');
Route::get('/get-states/{countryId}', [PublicController::class, 'getState'])->name('getState');
Route::get('/get-pincodes/{cityId}', [PublicController::class, 'getPincode'])->name('getPincode');
Route::get('/get-type/{categoryId}', [PublicController::class, 'getCategory'])->name('getCategory');
Route::get('/getfaq', [FaqController::class, 'getfaq'])->name('getfaq');


// seperate view for user side
    Route::get('/show-user-privacy/{lang}', [PrivacyController::class, 'showUserPrivacyPolicy'])->name('showUserPrivacyPolicy');
    Route::get('/show-user-about/{lang}', [AboutController::class, 'showUserAbout'])->name('showUserAbout');
    Route::get('/show-user-legal/{lang}', [LegalController::class, 'showUserLegalInformation'])->name('showUserLegalInformation');

Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
    Route::get('/dashboard', [DashboardController::class, 'showDashboard'])->name('showDashboard');
    Route::get('/dashboard/chart', [DashboardController::class, 'showChart'])->name('showChart');
    //department
    Route::get('/department', [DepartmentController::class, 'showDepartment'])->name('showDepartment');
    Route::post('/department/add', [DepartmentController::class, 'addDepartment'])->name('addDepartment');
    Route::get('/department/delete/{id}', [DepartmentController::class, 'deleteDepartment'])->name('deleteDepartment');
    Route::get('/department/view/{id}', [DepartmentController::class, 'viewDepartment'])->name('viewDepartment');
    //region
    Route::get('/region-management', [RegionController::class, 'showRegion'])->name('showRegion');
    Route::get('/country/view/{id}', [RegionController::class, 'viewCountry'])->name('viewCountry');
    Route::post('/country/edit', [RegionController::class, 'editCountry'])->name('editCountry');
    Route::post('/contry/add', [RegionController::class, 'addCountry'])->name('addCountry');
    Route::post('/state/add', [RegionController::class, 'addState'])->name('addState');
    Route::get('/state/view/{id}', [RegionController::class, 'viewState'])->name('viewState');
    Route::get('/state/delete/{id}', [RegionController::class, 'deleteState'])->name('deleteState');
    Route::get('/region-city-pincode', [RegionController::class, 'showPincode'])->name('showPincode');
    Route::post('/city/add', [RegionController::class, 'addCity'])->name('addCity');
    Route::get('/city/view/{id}', [RegionController::class, 'viewCity'])->name('viewCity');
    Route::get('/city/delete/{id}', [RegionController::class, 'deleteCity'])->name('deleteCity');
    Route::post('/pincodes/add', [RegionController::class, 'addPincodes'])->name('addPincodes');
    Route::get('/pincodes/view/{id}', [RegionController::class, 'viewPincode'])->name('viewPincode');
    Route::get('/pincode/delete/{id}', [RegionController::class, 'deletePin'])->name('deletePin');
    // Route::post('/showRole',[]
    Route::resource('banks', BankController::class);
    Route::get('/bank/view/{id}', [BankController::class, 'show'])->name('showBankDetails');
    Route::get('/bank/delete/{id}', [BankController::class, 'destroy'])->name('deleteBankDetails');
    //LeaseType
    Route::any('/owned/property/', [LeaseTypeController::class, 'showProperty'])->name('showOwnedProperty');
    Route::any('/add/lease-type/', [LeaseTypeController::class, 'addLeaseType'])->name('addLeaseType');
    //designation
    Route::get('/designation', [DesignationController::class, 'showDesignation'])->name('showDesignation');
    Route::post('/designation/add', [DesignationController::class, 'addDesignation'])->name('addDesignation');
    Route::get('/designation/delete/{id}', [DesignationController::class, 'deleteDesignation'])->name('deleteDesignation');
    Route::get('/designation/view/{id}', [DesignationController::class, 'viewDesignation'])->name('viewDesignation');
    //admins
    Route::get('/admin-management', [AdminController::class, 'showAdmins'])->name('showAdmins');
    Route::post('/admin/add', [AdminController::class, 'addAdmin'])->name('addAdmin');
    Route::get('/admin/view/{id}', [AdminController::class, 'viewAdmin'])->name('viewAdmin');
    Route::get('/admin/delete/{id}', [AdminController::class, 'deleteEmployee'])->name('deleteEmployee');
    Route::get('/admin/block/{id}/{action}', [AdminController::class, 'disabledAdmin'])->name('disabledAdmin');
    //agents
    Route::get('/agent-management', [AgentController::class, 'showAgent'])->name('showAgent');
    Route::post('/agent/add', [AgentController::class, 'addAgent'])->name('addAgent');
    Route::get('/agent/view/{id}', [AgentController::class, 'viewAgent'])->name('viewAgent');
    Route::get('/agent/list/{adminid}', [AgentController::class, 'listAgent'])->name('listAgent');
    Route::get('/agent/details/{id}', [AgentController::class, 'viewAgentDetails'])->name('viewAgentDetails');
    Route::get('/agent/block/{id}/{action}', [AgentController::class, 'disableAgent'])->name('disableAgent');
    Route::get('/agent-cash-request', [AgentController::class, 'showAgentCashPayment'])->name('showAgentCashPayment');
    Route::get('/agent_pay_request/accept/{id}/{action}', [AgentController::class, 'acceptPaymentRequest'])->name('acceptPaymentRequest');
    Route::get('/agent_pay_request/reject/{id}', [AgentController::class, 'rejectPaymentRequest'])->name('rejectPaymentRequest');
    Route::get('/agent_pay_request/revise/{id}', [AgentController::class, 'revisePaymentRequest'])->name('rejectPaymentRequest');
    //users
    Route::get('/user-management', [UserController::class, 'showUser'])->name('showUser');
    Route::post('/user/add', [UserController::class, 'addUser'])->name('addUser');
    Route::get('/user/block/{id}/{action}', [UserController::class, 'disableUser'])->name('disableUser');
    Route::get('/user/view/{id}', [UserController::class, 'viewUser'])->name('viewUser');
    Route::get('/user/move-to-owner/{id}', [UserController::class, 'makeUserOwner'])->name('makeUserOwner');
    //property-types
    Route::get('/property-type', [PropertyTypeController::class, 'showTypes'])->name('showTypes');
    Route::post('/property-type/add', [PropertyTypeController::class, 'addType'])->name('addType');
    Route::get('/property-type/view/{id}', [PropertyTypeController::class, 'viewType'])->name('viewType');
    //amenities
    Route::get('/amenity', [AmenityController::class, 'showAmenity'])->name('showAmenity');
    Route::post('/amenity/add', [AmenityController::class, 'addAmenity'])->name('addAmenity');
    Route::get('/amenity/view/{id}', [AmenityController::class, 'viewAmenity'])->name('viewAmenity');
    Route::post('/amenity-category/add', [AmenityController::class, 'addAmenityCategory'])->name('addAmenityCategory');
    Route::get('/amenity-category/view/{id}', [AmenityController::class, 'viewAmenityCategory'])->name('viewAmenityCategory');
    //details
    Route::get('/details', [DetailsController::class, 'showDetails'])->name('showDetails');
    Route::post('/details/add', [DetailsController::class, 'addDetail'])->name('addDetail');
    Route::get('/details/view/{id}', [DetailsController::class, 'viewDetail'])->name('viewDetail');
    //property-listing
    Route::get('/property', [PropertyController::class, 'showProperty'])->name('showProperty');
    // Route::get('/property-edit-request/{id}', [PropertyController::class, 'editProperty'])->name('editProperty');
    Route::get('/property-edit-request', [PropertyController::class, 'editProperty'])->name('editProperty');
    Route::get('/property-edit-list/{id}', [PropertyController::class, 'editProperty'])->name('editProperty');
    Route::post('/property/add', [PropertyController::class, 'addProperty'])->name('addProperty');
    Route::post('/property/add-building', [OwnerBuildingController::class, 'addBuilding'])->name('addBuilding');
    Route::post('/property/add-building-apartment', [OwnerBuildingController::class, 'addBuildingApartment'])->name('addBuildingApartment');
    Route::post('/apartmment/add', [PropertyController::class, 'addApartment'])->name('addApartment');
    Route::get('/property/view/{id}', [PropertyController::class, 'viewProperty'])->name('viewProperty');
    Route::post('/property/agent', [PropertyController::class, 'addAgentinProperty'])->name('addAgentinProperty');
    Route::post('contract_no-validate', [PropertyController::class, 'checkContractno'])->name('checkContractno');
    Route::get('/propertyagent/list/{adminid}', [PropertyController::class, 'propertyAgent'])->name('propertyAgent');
    // property listing-agents
    Route::get('/agentlists', [PropertyController::class, 'getAgents'])->name('getAgents');
    //owner
    Route::get('/owner-management', [OwnerController::class, 'showOwner'])->name('showOwner');
    Route::post('/owner/add', [OwnerController::class, 'addOwner'])->name('addOwner');
    Route::get('/owner/view/{id}', [OwnerController::class, 'viewOwner'])->name('viewOwner');
    Route::get('/showOwnerProperty/{id}', [OwnerController::class, 'showOwnerProperty'])->name('showOwnerProperty');
    Route::get('/owner/block/{id}/{action}', [OwnerController::class, 'disableowner'])->name('disableowner');
    Route::get('/owner/available-amenities/{id}', [OwnerController::class, 'getAvailableAmenities'])->name('getAvailableAmenities');
    //details-on-type-change
    Route::get('/owner/details-on-type-change/{type}', [OwnerController::class, 'getDetails'])->name('getDetails');
    Route::get('/admin/commission/report', [AdminController::class, 'showCommissionReport'])->name('showCommissionReport');
    Route::get('/property-management-fee/{id}', [AdminController::class, 'viewPropertyManagementFee'])->name('viewPropertyManagementFee');
    // owner data table list
    Route::resource('owners-list', OwnerController::class);

    // Set up Management
    Route::get('companyDetails',[CompanyController::class,'index'])->name('companyDetails');
  
    Route::post('addCompany',[CompanyController::class,'store'])->name('addCompany');
    Route::get('/company-view/{id}', [CompanyController::class, 'edit'])->name('viewCompany');
    Route::any('/company-edit', [CompanyController::class, 'update'])->name('editCompany');
    Route::delete('/company-delete/{id}', [CompanyController::class, 'destroy'])->name('deleteCompany');
    //services
    Route::get('/service-management', [ServiceController::class, 'showService'])->name('showService');
    Route::post('/service/add', [ServiceController::class, 'addService'])->name('addService');
    Route::get('/service/view/{id}', [ServiceController::class, 'viewService'])->name('viewService');
    Route::get('/service/delete/{id}', [ServiceController::class, 'deleteServices'])->name('deleteServices');
    //service-provider
    Route::get('/serviceProvider', [ServiceController::class, 'showProvider'])->name('showProvider');
    Route::post('/service-provider/add', [ServiceController::class, 'addServiceProvider'])->name('addServiceProvider');
    Route::get('/service-provider/view/{id}', [ServiceController::class, 'viewServiceProvider'])->name('viewServiceProvider');
    //Inspection Team
    //agents
    Route::get('/Inspection-team', [RequestInspectionController::class, 'showInspectionTeam'])->name('showInspectionTeam');
    Route::post('/Inspection-team/add', [RequestInspectionController::class, 'addInspectionTeam'])->name('addInspectionTeam');
    Route::get('/inspection-team/view/{id}', [RequestInspectionController::class, 'viewInspectionTeam'])->name('viewInspectionTeam');
    //  Route::get('/agent/view/{id}', [AgentController::class, 'viewAgent'])->name('viewAgent');
    //  Route::get('/agent/list/{id}', [AgentController::class, 'listAgent'])->name('listAgent');
    Route::get('/inspectionmemeber/block/{id}/{action}', [RequestInspectionController::class, 'disableid'])->name('disableInspection');
    //User Service Requset
    Route::get('userRequest', [UserRequestController::class, 'showuserRequset'])->name('showUserRequest');
    Route::get('/userRequest-details/{id}', [UserRequestController::class, 'userRequestDetails'])->name('userRequestDetails');
    Route::get('/owner/approval/{id}/{action}', [UserRequestController::class, 'approvalSent'])->name('approvalSent');
    Route::get('/user_request/Inspection/{id}/{memberid}', [UserRequestController::class, 'assignUserRequestInspection'])->name('assignUserRequestInspection');
    Route::post('/inspectionEstimate/add', [UserRequestController::class, 'addrequestEstimate'])->name('addrequestEstimate');
    Route::get('/userrequest/approval/{id}/{action}', [UserRequestController::class, 'paymentCompleted'])->name('paymentCompleted');
    Route::get('cancel-request', [UserRequestController::class, 'requestForCancellation'])->name('showCancelRequest');
    Route::get('cancelledRequest', [UserRequestController::class, 'cancelledRequest'])->name('cancelledRequest');
    Route::get('completedRequest', [UserRequestController::class, 'completedRequest'])->name('completedRequest');
    Route::get('/downloadfile/{file}', [UserRequestController::class, 'downloadfile'])->name('downloadfile');
    Route::post('request/mark-user-completed/{id}', [UserRequestController::class, 'markUserServiceAsCompleted'])->name('markUserServiceAsCompleted');
    Route::post('approve-request-cancel', [UserRequestController::class, 'approveCancelRequest'])->name('approveCancelRequest');
    Route::get('/service-view/{serviceid}/{status_type}', [UserRequestController::class, 'showServiceRequest'])->name('showServiceRequest');
    /////// Owner Service Request    //////
    Route::get('/owner_request/Inspection/{id}/{memberid}', [UserRequestController::class, 'assignOwnerRequestInspection'])->name('assignOwnerRequestInspection');
    Route::post('/inspectionOwnerEstimate/add', [UserRequestController::class, 'addownerrequestEstimate'])->name('addownerrequestEstimate');
    Route::get('/request/approval/{id}/{provider}', [UserRequestController::class, 'ownerPaymentCompleted'])->name('ownerPaymentCompleted');
    Route::get('/request/mark-completed/{id}', [UserRequestController::class, 'markServiceAsCompleted'])->name('markServiceAsCompleted');
    // contract details
    Route::post('/addcontractdetails', [PropertyController::class, 'addContractDetails'])->name('addContractDetails');
    //admin masters
    Route::any('/frequencymaster', [MasterController::class, 'showFrequency'])->name('showFrequency');
    Route::any('/addfrequency', [MasterController::class, 'addFrequency'])->name('addFrequency');
    Route::get('/frequency/edit/{id}', [MasterController::class, 'editFrequency'])->name('editFrequency');
    // user bookings
    Route::get('/user-bookings', [UserBookingController::class, 'userBookings'])->name('userBookings');
    Route::any('/request-user-contract', [UserBookingController::class, 'requestForContractByAgent'])->name('requestUserContract');
    Route::get('/agent-bookings', [UserBookingController::class, 'agentBookings'])->name('agentBookings');
    Route::get('/agent-bookings/details/{id}', [UserBookingController::class, 'agentBookingsDetails'])->name('agentBookingsDetails');
    Route::get('/complete-user-tour', [UserBookingController::class, 'completeUserTour'])->name('completeUserTour');
    Route::get('/complete-user-payment/{id}', [UserBookingController::class, 'approveCompleteUserTour'])->name('approveCompleteUserTour');
    Route::get('reject/complete-user-payment/{id}', [UserBookingController::class, 'rejectCompleteUserTour'])->name('approveCompleteUserTour');
    Route::get('reject/contract/{id}', [UserBookingController::class, 'rejectContract'])->name('rejectContract');
    Route::any('/issued-contract', [UserBookingController::class, 'issuedContract'])->name('issuedContract');
    Route::get('/booking/cancel/{id}', [UserBookingController::class, 'cancelBooking'])->name('cancelBooking');
    Route::get('/booking-details/{id}', [UserBookingController::class, 'viewBookingDetails'])->name('bookingDetails');
    Route::get('/property_verification/{propertyid}', [UserBookingController::class, 'propertDocumentVerfication'])->name('propertDocumentVerfication');
    Route::post('/approve-booking', [UserBookingController::class, 'approveBooking'])->name('approveBooking');
    Route::get('/assign_agent/{bookid}/{agentid}', [UserBookingController::class, 'assignAgent'])->name('assignAgent');
    Route::post('/user-booking-contract', [UserBookingController::class, 'addUserBookingContract'])->name('addUserBookingContract');
    Route::post('/user-booking-contract-edit', [UserBookingController::class, 'editUserBookingContract'])->name('editUserBookingContract');
    Route::post('/agent-booking-contract', [UserBookingController::class, 'addAgentBookingContract'])->name('addAgentBookingContract');
    // Property Management
    Route::any('/property/management/list', [propertyManagementController::class, 'showPropertyManage'])->name('showPropertyManage');
    Route::any('/property/management/add', [propertyManagementController::class, 'addPropertyManageagent'])->name('addPropertyManageagent');
    Route::any('/buildings/property', [propertyManagementController::class, 'showBuilding'])->name('showBuilding');

    // Route::post('/property-unit-edit', [propertyManagementController::class, 'propertyUnitEdit'])->name('propertyUnitEdit');
    Route::any('/property/management/addservice', [propertyManagementController::class, 'addPropertyManageagentService'])->name('addPropertyManageagentService');
    Route::post('/property/management/add-property', [propertyManagementController::class, 'addProperty']);
    Route::any('/property/editOwner-property', [propertyController::class, 'editOwnerProperty'])->name('editOwnerProperty');
    Route::post('/deleteimage/{pid}', [propertyController::class, 'deletegalleryimage'])->name('deletegalleryimage');
    // edit property-list.blade
    Route::get('/property-list/view/{id}', [propertyManagementController::class, 'viewProperty'])->name('viewProperty');
    Route::get('/property/view/{id}', [propertyManagementController::class, 'viewIndividualProperty'])->name('viewIndividualProperty');
    Route::get('/property-edit/{id}', [propertyManagementController::class, 'viewEditProperty'])->name('viewEditProperty');
    Route::get('/property/block/{id}/{action}', [PropertyController::class, 'disableProperty'])->name('disableProperty');
    //property List-datatable
    Route::get('/property-list',[propertyManagementController::class,'index'])->name('propertyListing');

    //About Us
    Route::get('/show-about', [AboutController::class, 'showAbout'])->name('showAbout');
    Route::post('/add-about', [AboutController::class, 'addAbout'])->name('addAbout');
    //Privacy Policy
    Route::get('/show-privacy', [PrivacyController::class, 'showPrivacyPolicy'])->name('showPrivacyPolicy');
    Route::post('/add-privacy', [PrivacyController::class, 'addPrivacy'])->name('addPrivacy');
    //Legal Information
    Route::get('/show-legal', [LegalController::class, 'showLegalInformation'])->name('showLegalInformation');
    Route::post('/add-legal', [LegalController::class, 'addLegal'])->name('addLegal');
    // seperate view for user side
    //Route::get('/show-user-about', [AboutController::class, 'showUserAbout'])->name('showUserAbout');
    // seperate view for user side
    // Route::get('/show-user-about/{lang}', [AboutController::class, 'showUserAbout'])->name('showUserAbout');
    Route::get('/show-about', [AboutController::class, 'showAbout'])->name('showAbout');
    Route::post('/add-about', [AboutController::class, 'addAbout'])->name('addAbout');
    //Privacy Policy
    // seperate view for user side
    // Route::get('/show-user-privacy', [PrivacyController::class, 'showUserPrivacyPolicy'])->name('showUserPrivacyPolicy');
    // Route::get('/show-user-privacy/{lang}', [PrivacyController::class, 'showUserPrivacyPolicy'])->name('showUserPrivacyPolicy');
    Route::get('/show-privacy', [PrivacyController::class, 'showPrivacyPolicy'])->name('showPrivacyPolicy');
    Route::post('/add-privacy', [PrivacyController::class, 'addPrivacy'])->name('addPrivacy');
    //Legal Information
    // seperate view for user side
    // Route::get('/show-user-legal', [LegalController::class, 'showUserLegalInformation'])->name('showUserLegalInformation');
    // Route::get('/show-user-legal/{lang}', [LegalController::class, 'showUserLegalInformation'])->name('showUserLegalInformation');
    Route::get('/show-legal', [LegalController::class, 'showLegalInformation'])->name('showLegalInformation');
    Route::post('/add-legal', [LegalController::class, 'addLegal'])->name('addLegal');
    //Show Faq
    Route::get('/show-faq', [FaqController::class, 'showFaq'])->name('showFaq');
    Route::get('/add-faq-view', [FaqController::class, 'addFaq'])->name('addFaq');
    Route::post('/add-faq', [FaqController::class, 'addFaqDetails'])->name('addFaqDetails');
    Route::get('/edit-faq/{id}', [FaqController::class, 'editFaq'])->name('editFaq');
    Route::get('/delete-faq/{id}', [FaqController::class, 'deleteFaq'])->name('deleteFaq');
    //Messages
    Route::get('/show-feedbacks', [FeedbackController::class, 'showFeedbacks'])->name('showFeedbacks');
    Route::get('/edit-feedbacks/{id}', [FeedbackController::class, 'editFeedback'])->name('editFeedback');
    Route::any('/property/management/add', [propertyManagementController::class, 'addPropertyManageagent'])->name('addPropertyManageagent');
    Route::get('/show-feedbacks', [FeedbackController::class, 'showFeedbacks'])->name('showFeedbacks');
    Route::get('/edit-feedbacks/{id}', [FeedbackController::class, 'editFeedback'])->name('editFeedback');
    //Events
    Route::get('/show-events', [EventsController::class, 'showEvents'])->name('showEvents');
    Route::get('/add-events-view', [EventsController::class, 'showAddPage'])->name('showAddPage');
    Route::post('/add-events', [EventsController::class, 'addEvents'])->name('addEvents');
    Route::post('/edit-events', [EventsController::class, 'editEvents'])->name('editEvents');
    Route::get('/delete-event/{id}', [EventsController::class, 'deleteEvents'])->name('deleteEvents');
    Route::get('/edit-event/{id}', [EventsController::class, 'showEditPage'])->name('showEditPage');
    //Events packages
    Route::get('/add-package-view', [EventsController::class, 'showPackageAddPage'])->name('showPackageAddPage');
    Route::get('/delete-feature/{id}', [EventsController::class, 'deleteFeature'])->name('deleteFeature');
    Route::get('/delete-package/{id}', [EventsController::class, 'removeEventPackages'])->name('removeEventPackages');
    Route::post('/add-seperate-feature', [EventsController::class, 'addSeperateFeature'])->name('addSeperateFeature');
    //Become An Owner Section
    Route::get('/show-become-owner-list', [BecomeOwnerController::class, 'showBecomeOwnerList'])->name('showBecomeOwnerList');
    //   Become an owner datatable
    Route::get('/become-an-owner-list',[BecomeOwnerController::class,'index'])->name('becomeAnOwnerListing');
    //    become an owner detail view
    Route::get('/become-list/view/{id}',[BecomeOwnerController::class,'becomeAnOwnerView'])->name('becomeAnOwnerView');
    ////// Tenant /////////////////////
    Route::get('/show-tenants', [TenantController::class, 'showTenants'])->name('showTenants');
    Route::get('/view-tenants/{id}', [TenantController::class, 'residentsDetails'])->name('residentsDetails');
    //Show Desired Property
    Route::get('/show-desired-property', [DesiredPropertyController::class, 'showDesiredProperty'])->name('showDesiredProperty');
    Route::get('/assign-desired-property/{id}', [DesiredPropertyController::class, 'assignDesiredPropertyDetails'])->name('assignDesiredPropertyDetails');
    Route::post('/save-to-assign', [DesiredPropertyController::class, 'saveToAssignedList'])->name('saveToAssignedList');
    Route::post('/delete-from-assign', [DesiredPropertyController::class, 'deleteAssignedList'])->name('deleteAssignedList');
    //   Accounts
    Route::get('/show-User-security-payment', [IncomeController::class, 'showUserSecurityAmountList'])->name('showUserSecurityAmountList');
    Route::get('/show-User-maintenance-payment', [IncomeController::class, 'showUserMaintenanceList'])->name('showUserMaintenanceList');
    Route::get('/show-owner-properties-payment', [IncomeController::class, 'showOwnerPaymentList'])->name('showOwnerPaymentList');
    Route::get('/show-user-rent', [IncomeController::class, 'showUserRental'])->name('showUserRental');
    Route::get('/show-user-token', [IncomeController::class, 'showTokenList'])->name('showTokenList');
    Route::get('/show-owner-service', [IncomeController::class, 'showOwnerServiceList'])->name('showOwnerServiceList');
    Route::get('/show-user-service', [IncomeController::class, 'showUserServiceList'])->name('showUserServiceList');
    Route::get('/show-rent', [IncomeController::class, 'showUserRent'])->name('showUserRent');
    Route::get('/show-commission', [IncomeController::class, 'showUserCommission'])->name('showUserCommission');
    Route::post('/payCommission-owner', [IncomeController::class, 'payOwnerCommission'])->name('payOwnerCommission');
    Route::get('/show-income', [IncomeController::class, 'showIncome'])->name('showIncome');
    Route::get('/show-user-properties-token', [IncomeController::class, 'showPropertyToken'])->name('showPropertyToken');
    Route::get('owner-service-details/{id}', [IncomeController::class, 'ownerServiceDetails'])->name('ownerServiceDetails');
    Route::get('user-service-details/{id}', [IncomeController::class, 'userServiceDetails'])->name('userServiceDetails');
    Route::get('/show-service-paid', [IncomeController::class, 'showPaidService'])->name('showPaidService');
    Route::post('/payService', [IncomeController::class, 'servicePayment'])->name('servicePayment');
    Route::post('/payService-owner', [IncomeController::class, 'ownerServicePayment'])->name('ownerServicePayment');
    Route::post('/payRental-user', [IncomeController::class, 'payRental'])->name('payRental');
    Route::post('/payToken-amount', [IncomeController::class, 'payToken'])->name('payToken');
    Route::post('/pay-agent', [ExpenseController::class, 'payAgent'])->name('payAgent');
    Route::get('/show-Agent-Payment', [ExpenseController::class, 'showAgentPayment'])->name('showAgentPayment');
    Route::get('agent_tracking_details/{id}', [ExpenseController::class, 'viewAgentTrackingDetails'])->name('viewAgentTrackingDetails');
    Route::get('/show-agent-pay-list', [ExpenseController::class, 'showAgentPaidList'])->name('showAgentPaidList');
    Route::get('/despoite-report', [ReportController::class, 'depoisteReport'])->name('depoisteReport');
    // data table rental list
    Route::get('/rental-details', [IncomeController::class, 'index']);
    // view payment token
    Route::get('/viewPaymentDetails/{id}', [IncomeController::class, 'viewPaymentDetails'])->name('viewPaymentDetails');
    // view Revenue
    Route::get('/payment-list/view/{id}',[IncomeController::class,'viewRevenuePaymnt'])->name("viewRevenuePaymnt");
    Route::get('/account/verify/{id}',[IncomeController::class,'approveVerification'])->name("approveVerification");
    Route::get('/cancel/pay/request/{id}',[IncomeController::class,'cancelPayRequest'])->name("cancelPayRequest");

    //Rewards Section
    Route::get('/show-coupons', [RewardsController::class, 'showCoupon'])->name('showCoupon');
    Route::post('/add-coupons', [RewardsController::class, 'addCoupon'])->name('addCoupon');
    Route::get('/coupon/view/{id}', [RewardsController::class, 'editCoupon'])->name('editCoupon');
    Route::get('/delete/coupon/{id}', [RewardsController::class, 'deleteCoupon'])->name('deleteCoupon');
    //Vacate Request
    Route::get('/show-vacate-request', [VacateRequestController::class, 'showVacateRequest'])->name('showVacateRequest');
    Route::get('/approve/request/{id}', [VacateRequestController::class, 'approveRequest'])->name('approveRequest');
    Route::get('/reject/request/{id}', [VacateRequestController::class, 'rejectRequest'])->name('rejectRequest');
    //notifications
    Route::get('/notifications', [NotificationController::class, 'list'])->name('notifications');
    Route::get('/viewNotificationDetails/{id}', [NotificationController::class, 'viewNotificationDetails'])->name('viewNotificationDetails');

    Route::resource('roles', RoleController::class);
    Route::any('/roles/addpermission/{id}', [RoleController::class, 'addPerimission'])->name('roles.addPerimission');
    Route::any('/roles/savepermission', [RoleController::class, 'saveAdminPerimission'])->name('roles.saveAdminPerimission');
    //Offer Package
    Route::get('show-package/{id}', [OfferPackageController::class, 'showOfferPackage'])->name('showOfferPackage');
    Route::post('add-offer-package', [OfferPackageController::class, 'addOfferPackage'])->name('addOfferPackage');
    Route::get('offerpackage-deactivation/{id}/{action}', [OfferPackageController::class, 'deactivationPackage'])->name('deactivationPackage');
    Route::get('/offer/view/{id}', [OfferPackageController::class, 'viewOffer'])->name('viewOffer');

    //report
    Route::get('show-owner-report', [ReportController::class, 'showOwnerReport'])->name('showOwnerReport');
    Route::get('show-agent-report', [ReportController::class, 'showAgentReport'])->name('showAgentReport');
    Route::get('show-tenant-report', [ReportController::class, 'showTenantReport'])->name('showTenantReport');
    Route::get('show-rental-report', [ReportController::class, 'showRentalReport'])->name('showRentalReport');
    Route::get('show-property-reservation-report', [ReportController::class, 'showReservationFeeReport'])->name('showReservationFeeReport');
    Route::get('show-service-report', [ReportController::class, 'showServiceReport'])->name('showServiceReport');
    Route::get('show-owner-property-details/{id}', [ReportController::class, 'showOwnerPropertyDetails'])->name('showOwnerPropertyDetails');
    Route::get('show-Tenant-property-details/{id}', [ReportController::class, 'showTenantPropertyDetails'])->name('showTenantPropertyDetails');
    Route::get('show-rental-property-details/{id}', [ReportController::class, 'showRentalPropertyDetails'])->name('showRentalPropertyDetails');
    Route::resource('showowner-reportList', ReportController::class);
    Route::get('showTenantReportView',[ReportController::class,'showTenantReportView'])->name('showTenantReportView');
    Route::get('tenantDetailedReport/{id}',[ReportController::class,'tenantDetailedReport'])->name('tenantDetailedReport');
    Route::get('rentalReport/{id}',[ReportController::class,'rentalReport'])->name('rentalReport');
    // Report Menu
    Route::get('proprtyReport',[ReportController::class,'proprtyReport'])->name('proprtyReport');
    Route::get('proprtyReportPdf/{id}',[ReportController::class,'proprtyReportPdf'])->name('proprtyReportPdf');
    Route::get('vacateReport',[ReportController::class,'vacateReport'])->name('vacateReport');
    Route::get('vacateReportPdf',[ReportController::class,'vacateReportPdf'])->name('vacateReportPdf');
    Route::get('agentReport',[ReportController::class,'agentReport'])->name('agentReport');
    Route::get('dueReport',[ReportController::class,'dueReport'])->name('dueReport');
    Route::get('/pending/request/{id}', [VacateRequestController::class, 'pendingRequest'])->name('pendingRequest');
    // owner buiding management urls
    Route::get('/manage-owner-buildings/{id}', [OwnerBuildingController::class, 'ownerBuildings'])->name('ownerbuildings');

    Route::get('/add-property-unit/{id}', [OwnerBuildingController::class, 'addPropertyUnits'])->name('addPropertyUnits');
    Route::post('/add-property-unit', [PropertyController::class, 'addPropertyUnits'])->name('savePropertyUnits');
    Route::any('/edit-property-unit/{id}', [PropertyManagementController::class, 'editPropertyUnits'])->name('editPropertyUnits');
    Route::any('/view-property-unit/{id}', [PropertyManagementController::class, 'viewPropertyUnits'])->name('viewPropertyUnits');
    Route::any('/update-property-unit', [PropertyController::class, 'updatePropertyUnits'])->name('updatePropertyUnits');
    // Route::get('/add-property-unit', [OwnerBuildingController::class, 'addPropertyUnits'])->name('addPropertyUnits');
    ////////////////////////////////dummy dashboard set-up////////////////////////////
    Route::get('/department/superadmin', [DummyDepartmentController::class, 'showDashboardSuper'])->name('showDashboardSuper');
    Route::get('/department/accounts-management', [DummyDepartmentController::class, 'showDashboardAccounts'])->name('showDashboardAccounts');
    Route::get('/department/document-management', [DummyDepartmentController::class, 'showDashboardDocument'])->name('showDashboardDocument');
    Route::get('/department/property-management', [DummyDepartmentController::class, 'showDashboardPropertyManager'])->name('showDashboardPropertyManager');
    Route::get('/department/facility-management', [DummyDepartmentController::class, 'showDashboardService'])->name('showDashboardService');

});
