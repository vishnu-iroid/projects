<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\api\owner\OwnerController;
use App\Http\Controllers\api\user\UserLoginController;
use App\Http\Controllers\api\user\UserRegisterController;
use App\Http\Controllers\api\user\UserController;
use App\Http\Controllers\api\user\PropertyController;
use App\Http\Controllers\api\agent\AgentController;
use App\Http\Controllers\api\ApiController;

Route::get('/countries', [ApiController::class, 'countries']);
Route::get('/states', [ApiController::class, 'states']);
Route::get('/cities', [ApiController::class, 'cities']);
Route::get('/zipcodes', [ApiController::class, 'zipcodes']);
Route::get('/available-amenities', [ApiController::class, 'getAvailableAmenities']);
Route::get('/types', [ApiController::class, 'types']);
Route::get('/getfaq', [ApiController::class, 'getfaq']);
Route::get('/getabout', [ApiController::class, 'getabout'])->name('getabout');
Route::get('/getprivacy/{lang}', [ApiController::class, 'getprivacy'])->name('getprivacy');
Route::get('/getterms/{lang}', [ApiController::class, 'getterms'])->name('getterms');

//Send password reset mail
Route::post('reset-password-mail', [ApiController::class, 'sendResetPasswordMail']);

// Route::get('/maintenance-list',[ApiController::class,'maintenance']);
Route::get('/type-list', [OwnerController::class, 'typeOnCategory']);
Route::post('/login', [ApiController::class, 'Login']);
Route::group(['prefix' => 'owner'], function () {
    Route::group(['middleware' => 'auth:owner'], function () {
        Route::get('/logout', [OwnerController::class, 'Logout']);
        Route::get('/profile', [OwnerController::class, 'ownerProfile']);
        Route::post('/profile-update', [OwnerController::class, 'profileUpdate']);
        Route::post('/change-password', [OwnerController::class, 'changePassword']);
        Route::post('/add-property', [OwnerController::class, 'addProperty']);
        Route::post('/add-apartment', [OwnerController::class, 'addApartment']);
        Route::post('/propertylist', [OwnerController::class, 'propertyList']);
        Route::post('/submit-for-verification', [OwnerController::class, 'propertySubmittingForVerification']);
        Route::post('/maintenance-list', [OwnerController::class, 'maintenanceList']);
        Route::post('/request-service', [OwnerController::class, 'sendOwnerServiceRequest']);
        Route::post('/list-requested-service', [OwnerController::class, 'listOwnerRequestedServices']);
        Route::post('/requested-service', [OwnerController::class, 'ownerRequestedServiceDetails']);
        Route::post('/properties-for-service', [OwnerController::class, 'ownerPropertyListForServiceRequest']);
        Route::post('/owner-service-paymentbill', [OwnerController::class, 'payOwnerPropertyService']);
        Route::post('/cancel-service-request', [OwnerController::class, 'cancelOwnerRequestedService']);
        Route::post('/property-details', [OwnerController::class, 'ownerPropertyDetails']);
        Route::post('/requested-service-for-approval', [OwnerController::class, 'requestedServiceForApproval']);
        Route::post('/accept-requested-service', [OwnerController::class, 'ownerAcceptingServiceRequest']);
        Route::post('/reject-requested-service', [OwnerController::class, 'ownerRejectingServiceRequest']);
        //payment
        Route::post('/list-property-payment', [OwnerController::class, 'listPaymentPropertyAccordingToFilter']);
        Route::post('/property-payment-details', [OwnerController::class, 'paymentPropertyDetails']);
        Route::post('/list-payment-history', [OwnerController::class, 'listPaymentPropertyHistory']);
        Route::post('/payment-history-details', [OwnerController::class, 'eachPaymentHistoryDetails']);
        Route::post('/payment-received', [OwnerController::class, 'setPaymentReceived']);
        Route::post('/yearly-accounts', [OwnerController::class, 'monthlyAccountsList']);
        Route::post('/monthly-accounts', [OwnerController::class, 'monthWisePropertyAccounts']);
        Route::post('/list-accounts-filter-property', [OwnerController::class, 'listPropertyForViewingAccountsOwnerHome']);
        Route::post('/owner-home-count', [OwnerController::class, 'ownerHomeCounts']);
        //building appartment listing details api
        Route::post('/owner-building', [OwnerController::class, 'ownerPropertyBuildingDetails']);
        //owner notification
        Route::post('/notifications', [OwnerController::class, 'ownerNotifications']);
        Route::post('/update-notification-status', [OwnerController::class, 'updateNotificationStatus']);
        Route::post('/owner-property-report', [OwnerController::class, 'downloadPropertyReport']);
        Route::post('/owner-overall-report', [OwnerController::class, 'downloadAllPropertyReport']);
        Route::post('/yearly-report', [OwnerController::class, 'overAllYearlyReport']);
        Route::post('/monthly-report', [OwnerController::class, 'eachMonthPropertyReport']);
        Route::post('/occupied-report', [OwnerController::class, 'downloadOwnerPropertyOccupiedReport']);
        Route::post('/vacant-report', [OwnerController::class, 'downloadOwnerPropertyVacantReport']);

    });
});
Route::group(['prefix' => 'user'], function () {
    Route::post('/phone-verification', [UserLoginController::class, 'phoneVerification']);
    Route::post('/resend-otp', [UserLoginController::class, 'resendOTP']);
    Route::post('/login', [UserLoginController::class, 'userLogin']);
    Route::post('/login-with-other', [UserLoginController::class, 'loginWithOther']);
    Route::post('/sign-up', [UserRegisterController::class, 'signUp']);
    Route::group(['middleware' => 'auth:user'], function () {
        Route::post('/logout', [UserLoginController::class, 'userLogout']);
        // user profile
        Route::get('/profile', [UserController::class, 'getUserProfile']);
        Route::post('/update-profile', [UserController::class, 'updateProfile']);
        Route::post('/remove-profilepic', [UserController::class, 'removeUserProfilePic']);

        Route::post('/list-property-sort', [PropertyController::class, 'showProperty']);
        Route::post('/list-property-filter', [PropertyController::class, 'showPropertyFullPageFilter']);

        Route::post('/list-property', [UserController::class, 'showProperty']);
        Route::post('/list-all-property', [UserController::class, 'listAllAvailableProperties']);

        Route::post('/update-favourite', [UserController::class, 'updatePropertyToFavourites']);
        Route::post('/favourite-list', [UserController::class, 'showProperty']);
        Route::post('/property-details', [UserController::class, 'showPropertDetails']);
        Route::post('/rental-details', [UserController::class, 'showRentalDetails']);

        Route::post('/book-tour', [UserController::class, 'bookToVisitProperty']);
        Route::post('/confirm-booking-details', [UserController::class, 'confirmTourBookingDetails']);
        Route::post('/tour-booking-details', [UserController::class, 'viewBookedTourDetails']);
        Route::post('/confirm-tour-booking', [UserController::class, 'confirmTourBooking']);
        Route::post('/list-events', [UserController::class, 'showAllEvents']);
        Route::post('/event-details', [UserController::class, 'showEventDetails']);

        Route::get('/book-property', [UserController::class, 'getBookPropertyDetails']);
        Route::post('/book-property', [UserController::class, 'bookProperty']);
        Route::post('/upload-book-paymentbill', [UserController::class, 'uploadBookPropertyDocument']);

        // My properties
        Route::get('/list-user-properties', [UserController::class, 'userPropertyList']);
        Route::get('/booked-property-details', [UserController::class, 'userBookedPropertyDetails']);

        Route::post('/upload-property-documents', [UserController::class, 'uploadUserPropertyDocuments']);
        Route::post('/upload-user-property-paymentbill', [UserController::class, 'payBookedProperty']);
        Route::post('/cancel-booked-property', [UserController::class, 'cancelBookedPropertyDetails']);

        Route::post('/list-services', [UserController::class, 'listServices']);
        Route::post('/request-services', [UserController::class, 'sendServiceRequest']);
        Route::post('/list-requested-service', [UserController::class, 'listRequestedServices']);
        Route::post('/requested-service', [UserController::class, 'requestedServiceDetails']);
        Route::post('/user-service-paymentbill', [UserController::class, 'payUserPropertyService']);
        Route::post('/cancel-service-request', [UserController::class, 'cancelRequestedService']);
        Route::post('/vacate-request', [UserController::class, 'userPropertyVacateRequest']);
        Route::post('/list-vacate-request', [UserController::class, 'listUserVacateRequest']);
        Route::post('/vacate-request-details', [UserController::class, 'vacateRequestDetails']);
        Route::post('/user-property-detail', [UserController::class, 'userPropertyDetails']);
        Route::post('/property-rent-paymentbill', [UserController::class, 'payUserPropertyRent']);
        Route::post('/send-feedback', [UserController::class, 'sendFeedback']);
        Route::post('/faq', [UserController::class, 'listFAQ']);
        Route::post('/privacy-policy', [UserController::class, 'listPrivacyPolicy']);
        Route::post('/legal-information', [UserController::class, 'listLegalInformations']);
        //event packages
        Route::post('/event-packages', [UserController::class, 'showEventPackages']);
        Route::post('/package-details', [UserController::class, 'eventBookingDetails']);
        Route::post('/book-package', [UserController::class, 'bookEventPackage']);
        Route::post('/user-event-package-paymentbill', [UserController::class, 'uploadEventBookingDocument']);

        // request desired
        Route::post('/request-desired-property', [UserController::class, 'userRequestingDesiredProperty']);
        Route::get('/fetch-request-property-data', [UserController::class, 'fetchDataToRequestDesiredProperty']);
        Route::post('/desired-property-request-list', [UserController::class, 'listDesiredPropertyRequest']);
        Route::post('/desired-property-request-details', [UserController::class, 'desiredPropertyRequestDetails']);

        //Become An Owner
        Route::post('/become-owner', [UserController::class, 'sendBecomeAnOwnerRequest']);

        //referral code showing
        Route::post('/show-referral-code', [UserController::class, 'getReferalCode']);

        //user payment history
        Route::post('/user-payment-history', [PropertyController::class, 'user_payment_history']);

        //terms of stay
        Route::post('/terms-of-stay/{id}', [PropertyController::class, 'terms_of_stay']);

        //user notifications
        Route::post('/user-notifications', [PropertyController::class, 'user_notifications']);

        //user rewards
        Route::post('/user-rewards', [PropertyController::class, 'user_rewards']);

        //read notifications
        Route::post('/read-notifications', [PropertyController::class, 'read_notifications']);


        //rental-packages
        Route::post('/rental-packages', [PropertyController::class, 'rental_packages']);

        //location showing at user side
        Route::post('/show-list-property-location', [UserController::class, 'showListingPropertyLocation']);
        //show near by property location
        Route::post('/show-nearby-property-location', [UserController::class, 'listAllNearByPropertyLocations']);
        Route::post('/all-property-locations', [UserController::class, 'listAllPropertyLocations']);
        Route::post('/property-location-details', [UserController::class, 'showEachPropertyLocationDeatils']);

        //help and support
        Route::post('/help-and-support', [PropertyController::class, 'helpAndSupports']);

        //property categories
        Route::post('/categories', [PropertyController::class, 'categoryFilter']);

        //beds list for filter
        Route::post('/details-for-filter', [PropertyController::class, 'propertyDetailsFilter']);
        //frequency for filter
        Route::post('/frequency-for-filter', [PropertyController::class, 'frequencyForFilter']);
        //amenities for filter
        Route::post('/filter-property-details', [PropertyController::class, 'propertyTypesForFilter']);

        //agent - feedback
        Route::post('/agent-feedback', [PropertyController::class, 'agent_feedback']);

        //user scheduled tour list api
        Route::post('/scheduled-tour-list', [UserController::class, 'scheduledTourList']);


        //other packages api
        Route::post('/other-packages', [PropertyController::class, 'propertyOtherPackages']);

        //rate property by user and send agent feedback
        Route::post('/agent-feed-and-property-rating', [PropertyController::class, 'agentFeedbackAndPropertyRating']);

        //download property details
        Route::post('/download-property-details-pdf', [PropertyController::class, 'downloadPropertyDetailsPDF']);
    });
});

Route::group(['prefix' => 'agent'], function () {
    Route::group(['middleware' => 'auth:agent'], function () {
        Route::get('/logout', [AgentController::class, 'Logout']);
        // profile management
        Route::post('/profile', [AgentController::class, 'agentProfile']);
        Route::post('/remove-profile-image', [AgentController::class, 'removeAgentProfilePic']);

        Route::post('/profile/update', [AgentController::class, 'updateAgentProfile']);
        Route::post('/agentproperty', [AgentController::class, 'assignedPropertyofAgent']);
        Route::post('/agentpropertydetails', [AgentController::class, 'agentPropertyDetails']);
        Route::post('/propertydetailsupdate', [AgentController::class, 'assignedPropertydetailsUpdateofAgent']);

        Route::post('/building-details', [AgentController::class, 'agentPropertyBuildingDetails']);
        Route::post('/update-building-details', [AgentController::class, 'updateBuildingDetails']);
        Route::post('/building-unit-details', [AgentController::class, 'propertyUnitDetails']);
        Route::post('/update-building-unit', [AgentController::class, 'updateBuildingUnitsDetails']);

        Route::post('/changepassword', [AgentController::class, 'changePassword']);
        Route::post('/agent-home', [AgentController::class, 'agentHome']);
        Route::post('/start-user-tour', [AgentController::class, 'apointmentUserTourDetails']);
        Route::post('/proceed-booking-details', [AgentController::class, 'proceedToBookDetailsForAgent']);
        Route::post('/proceed-booking', [AgentController::class, 'agentProceedToBook']);
        Route::post('/remove-property-document', [AgentController::class, 'removePropertyDocument']);
        Route::post('/list-property-document', [AgentController::class, 'listPropertyDocument']);
        Route::post('/upload-user-property-document', [AgentController::class, 'uploadUserPropertyDocumentsByAgent']);
        Route::post('/request-contract', [AgentController::class, 'requestForContractByAgent']);
        Route::post('/update-tour-not-interested', [AgentController::class, 'updatingUserNotInterestedStatusByAgent']);

        Route::post('/my-user-properties', [AgentController::class, 'myUserProperties']);
        Route::post('/my-user-property-details', [AgentController::class, 'agentOngoingAndCompletedPropertyDetails']);

        Route::post('/add-task', [AgentController::class, 'addTask']);
        Route::post('/calendar-task-count', [AgentController::class, 'agentCalendarTaskCount']);
        Route::post('/calendar-task-list', [AgentController::class, 'agentCalendarTaskList']);
        Route::post('/task-list', [AgentController::class, 'agentTaskList']);
        Route::post('/update-task-status', [AgentController::class, 'updateTaskCompleteStatus']);


        Route::post('/my-request', [AgentController::class, 'myRequest']);
        Route::post('/accept-my-request', [AgentController::class, 'acceptingMyRequest']);
        Route::post('/reject-my-request', [AgentController::class, 'rejectingMyRequest']);

        Route::post('/start-owner-tour', [AgentController::class, 'startOwnerTourApointment']);

        Route::post('/my-owner-properties', [AgentController::class, 'myOwnerProperties']);
        Route::post('/my-owner-property-details', [AgentController::class, 'agentOwnerPropertyDetails']);

        // agent cash in hand
        Route::post('/cash-in-hand-list', [AgentController::class, 'agentCashInHandList']);
        Route::post('/total-cash-in-hand', [AgentController::class, 'agentTotalCashInHand']);
        Route::post('/pay-cash-in-hand', [AgentController::class, 'agentPayCashInHandToAdmin']);

        //property contract
        Route::post('/property-contract-details', [AgentController::class, 'agentPropertyContractDetails']);

        //user property contract
        Route::post('/user-property-contract-details', [AgentController::class, 'agentUserPropertyContractDetails']);

        //agent tour api to locate in map
        Route::post('/tour-location-details', [AgentController::class, 'showTourLocationDetails']);

        //agent earnings
        Route::post('/my-earnings', [AgentController::class, 'agentEarnings']);
        Route::post('/total-earnings', [AgentController::class, 'agentTotalEarnings']);
        // add agent commission
        Route::post('/add-commission', [AgentController::class, 'addAgentEarnings']);
        //edit agent commission
        Route::post('/edit-commission', [AgentController::class, 'editAgentEarnings']);

        // agent request for earnings
        Route::post('/request-earnings', [AgentController::class, 'requestAgentEarnings']);

        //terms of stay
        Route::post('/terms-of-stay/{id}', [AgentController::class, 'terms_of_stay']);

        //agent notifications
        Route::post('/agent-notifications', [AgentController::class, 'agent_notifications']);

        //agent - read notifications
        Route::post('/read-notifications', [AgentController::class, 'read_notifications']);


        //agent - price_details
        Route::post('/price-details', [AgentController::class, 'price_details']);

        //agent - fetch property pending amount to pay from user
        Route::post('/fetch-pending-amount', [AgentController::class, 'fetchPendingAmount']);

        //agent - bill full amount
        Route::post('/pay-full-amount-bill', [AgentController::class, 'userPropertyFullAmountPayment']);

        //agent - feedbacks
        Route::post('/feedbacks', [AgentController::class, 'feedbacks']);

        //agent - property other packages
        Route::post('/other-packages', [AgentController::class, 'propertyOtherPackages']);
        Route::post('/package-list', [AgentController::class, 'propertyOtherPackagesForDropdown']);


        Route::post('/download-property-details-pdf', [AgentController::class, 'downloadPropertyDetailsPDF']);


    });
});
