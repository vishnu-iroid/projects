<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\OwnerProperty;


class AgentEarnings extends Model
{
    use HasFactory;

    public function property_rel(){
        return $this->hasOne(OwnerProperty::class,'id','property_id');

    }

    /**
     * Get the Agents associated with the AgentEarnings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function agent_rel()
    {
        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }
}
