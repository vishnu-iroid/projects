<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Agent extends Authenticatable
{
    use HasFactory,HasApiTokens,Notifiable;

    protected $hidden = ['password'];
    public function agentProp_rel(){
        return $this->hasMany(AgentProperties::class,'agent_id','id');
    }
    
}