<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\City;

class Pincode extends Model
{
    use HasFactory;

    public function city_rel(){
        return $this->hasOne(City::class,'id','city');
    }
}
