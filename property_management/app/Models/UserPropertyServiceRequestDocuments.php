<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\UserPropertyServiceRequests;

use function PHPUnit\Framework\returnValueMap;

class UserPropertyServiceRequestDocuments extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'user_property_service_request_documents';


    public function requested_service()
    {
        return $this->hasOne(UserPropertyServiceRequests::class,'id','request_id')->with('service_related:id,service');
    }

    public function service_related()
    {
        return $this->hasOne(Services::class, 'id', 'service_id');
    }

    public function user_doc()
    {
        return $this->hasMany(UserPropertyServiceRequestDocuments::class, 'request_id', 'id')->where('document_type',0);
    }

    public function user_related()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function userRequestService_rel(){
        return $this->hasOne(UserProperty::class,'id','user_property_id');
    }
    public function userPropertyServiceRequestsRelation()
    {
        return $this->hasMany(UserPropertyServiceRequests::class, 'id', 'request_id');
    }
}
