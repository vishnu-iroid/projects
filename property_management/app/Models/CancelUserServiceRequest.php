<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\UserPropertyServiceRequests;

class CancelUserServiceRequest extends Model
{
    use HasFactory;

    protected $table = 'cancel_user_service_request';

    public function request_rel(){
        return $this->belongsTo(UserPropertyServiceRequests::class, 'request_id', 'id');
        // return $this->hasOne(UserPropertyServiceRequests::class, 'id', 'request_id')->with('service_related:id,service','user_related:id,name');
    }
}
