<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BookUserTour;

class AgentTrackingUserTour extends Model
{
    use HasFactory;

    protected $table = 'agent_tracking_user_tour';

    public function booking_rel()
    {
        return $this->hasOne(BookUserTour::class, 'id', 'tour_id ');
    }

    public function agent_rel()
    {

        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }

    public function property_rel()
    {

        return $this->hasMany(UserPropertyBookings::class, 'assigned_agent', 'agent_id');
    }
}
