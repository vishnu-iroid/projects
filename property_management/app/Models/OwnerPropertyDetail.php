<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerPropertyDetail extends Model
{
    use HasFactory;

    protected $fillable = ['property_id', 'detail_id', 'owner_id','value','created_at','updated_at'];


    public function details_rel(){
        return $this->hasOne(Detail::class,'id','detail_id');
    }
}
