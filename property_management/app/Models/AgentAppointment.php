<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\OwnerProperty;
use App\Models\Owner;
use App\Models\User;

class AgentAppointment extends Model
{
    use HasFactory;

    public function property_details(){
        return $this->hasOne(OwnerProperty::class,'id','property_id');
    }

    public function owner_details(){
        return $this->hasOne(Owner::class,'id','owner_id');
    }

    public function user_details(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
