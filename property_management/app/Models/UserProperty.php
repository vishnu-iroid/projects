<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OwnerProperty;
use App\Models\User;
use App\Models\UserPropertyBookings;
use Auth;


class UserProperty extends Model
{
    use HasFactory;

    public function user_property_related()
    {
        return $this->belongsTo(OwnerProperty::class, 'property_id');
    }
    public function user_rel()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function doc_rel()
    {
        return $this->hasMany(UserPropertyDocument::class, 'user_property_id', 'id');
    }
    public function prop_rel()
    {
        return $this->hasOne(OwnerProperty::class, 'id', 'property_id');
    }
    public function contract_rel()
    {
        return $this->hasOne(BookingContract::class, 'user_property_id', 'id');
    }

    public function  rent1_rel()
    {
        return $this->hasOne(AdminAccount::class, 'user_property_id', 'id')->where('payment_type', 5)->orderBy('date', 'ASC');
    }
    public function  rent_rel()
    {
        return $this->hasOne(AdminAccount::class, 'user_property_id', 'id')->where('payment_type', 5)->orderBy('date', 'ASC');
    }

    public function rent_collected()
    {
        return $this->hasMany(AdminAccount::class, 'user_property_id', 'id')->where('payment_type', 5)->orderBy('date', 'ASC');
    }
    public function  service_rel()
    {
        return $this->hasOne(AdminAccount::class, 'user_property_id', 'id')->where('payment_type', 1)->orderBy('date', 'ASC');
    }
    public function  rent_report_rel()
    {
        return $this->hasMany(AdminAccount::class, 'property_id', 'property_id');
    }

    public function  property_service_payment()
    {
        return $this->hasMany(AdminAccount::class, 'user_property_id', 'id')->where('payment_type', 1);
    }
    public function user_property_bookings()
    {
        return $this->hasOne(UserPropertyBookings::class, 'property_id', 'property_id');
    }

    public function accounts_rel()
    {
        return $this->hasMany(AdminAccount::class, 'user_property_id','id');
    }


}
