<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserPropertyServiceRequestDocuments;
use App\Models\Services;
use App\Models\User;
use App\Models\UserProperty;
use App\Models\UserServiceRequestApproval;
use Auth;

class UserPropertyServiceRequests extends Model
{
    use HasFactory;

    protected $table = 'user_property_service_requests';

    public function service_related()
    {
        return $this->hasOne(Services::class, 'id', 'service_id');
    }

    public function prop_related()
    {
        return $this->hasOne(OwnerProperty::class, 'id', 'user_property_id');
    }
    public function user_related()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function user_doc()
    {
        return $this->hasMany(UserPropertyServiceRequestDocuments::class, 'request_id', 'id')->where('document_type',0);
    }

    public function service_payemnt_doc()
    {
        return $this->hasOne(UserPropertyServiceRequestDocuments::class, 'request_id', 'id')->where('document_type',1);
    }

    public function service_estimate_doc()
    {
        return $this->hasOne(UserPropertyServiceRequestDocuments::class, 'request_id', 'id')->where('document_type',2);
    }

    public function service_amount()
    {
        return $this->hasOne(AdminAccount::class, 'reference_id', 'id')->where('payment_type','1');
    }

    public function user_property_related()
    {
        return $this->hasOne(UserProperty::class, 'id', 'user_property_id');
    }

    public function user_service_related()
    {
        return $this->hasOne(UserServiceRequestApprovel::class, 'request_id', 'id');
    }

    public function owner_approval_status()
    {
        return $this->hasOne(UserServiceRequestApprovel::class, 'request_id', 'id');
    }
}
