<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Agent;
use App\Models\User;
use App\Models\OwnerProperty;
use App\Models\UserPropertyBookingDocuments;
use App\Models\OfferPackage;


class UserPropertyBookings extends Model
{
    use HasFactory;

    protected $table = 'user_property_bookings';

    public function property_details()
    {
        return $this->belongsTo(OwnerProperty::class, 'property_id','id' );
    } 
    public function user_details()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function agent_details()
    {
        return $this->hasOne(Agent::class, 'id', 'assigned_agent');
    }

    public function booking_doc()
    {
        return $this->hasOne(UserPropertyBookingDocuments::class, 'booking_id', 'id');
    }

    public function account_rel()
    {

        return $this->hasOne(AdminAccount::class, 'property_id', 'property_id');
    }

    /**
     * Get the offer package associated with the UserPropertyBookings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function offer_rel()
    {
        return $this->hasOne(OfferPackage::class, 'id', 'package_id');
    }

    /**
     * Get the user associated with the UserPropertyBookings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function coupon_rel()
    {
        return $this->hasOne(Rewards::class, 'id', 'coupoun');
    }

    public function property_relation()
    {
        return $this->hasOne(OwnerProperty::class, 'id','property_id' );
    } 

}
