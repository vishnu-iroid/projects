<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OwnerProperty;
use App\Models\User;
use App\Models\BookingContract;

class Tenant extends Model
{
    use HasFactory;
    public function user_rel()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function prop_rel()
    {
        return $this->hasOne(OwnerProperty::class, 'id', 'property_id');
    }
    public function contract_rel()
    {
        return $this->hasOne(BookingContract::class, 'id', 'property_id');
    }
}
