<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerPropertyAmenity extends Model
{
    use HasFactory;

    protected $fillable = ['property_id','amenity_id','amenities_category_id','owner_id','created_at','updated_at'];
    public function amenity(){
        return $this->belongsTo(Amenity::class,'amenity_id','id');
    }
    public function amenity_category(){
        return $this->belongsTo(AmenitiesCategory::class,'amenities_category_id','id');
    }
}
