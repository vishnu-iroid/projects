<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserServiceRequestApproval extends Model
{
    use HasFactory;

    protected $table = 'user_service_request_approval';
}
