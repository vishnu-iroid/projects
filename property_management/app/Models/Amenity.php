<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\AmenitiesCategory;

class Amenity extends Model
{
    use HasFactory;

    public function category(){
        return $this->hasOne(AmenitiesCategory::class,'id','amenity_category_id');
    }
}
