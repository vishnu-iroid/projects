<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Services;
use Auth;

class OwnerPropertyServiceRequests extends Model
{
    use HasFactory;

    protected $table = 'owner_property_service_requests';

    public function service_related()
    {
        return $this->hasOne(Services::class, 'id', 'service_id');
    }

    public function property_related()
    {
        return $this->hasOne(OwnerProperty::class, 'id', 'owner_property_id');
    }
    public function owner_related()
    {
        return $this->hasOne(Owner::class, 'id', 'owner_id');
    }
    public function owner_doc()
    {
        return $this->hasMany(OwnerPropertyServiceRequestDocuments::class, 'request_id', 'id');
    }

    public function service_payemnt_doc()
    {
        return $this->hasOne(OwnerPropertyServiceRequestDocuments::class, 'request_id', 'o_id')->where('document_type',1);
    }
}
