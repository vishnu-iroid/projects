<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\OfferPackageFeature;

class OfferPackage extends Model
{
    use HasFactory;

    public function package_features()
    {
        return $this->hasMany(OfferPackageFeature::class,'package_id','id');
    }
}
