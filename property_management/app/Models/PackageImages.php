<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageImages extends Model
{
    use HasFactory;
    protected $table = 'package_images';

    public function package_feature(){
        return $this->belongsTo(EventPackages::class,'package_id','id');
    }
}
