<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Help_and_supports extends Model
{
    use HasFactory;
    protected $fillable = ['problem_description','user_id'];
}
