<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPropertyFullAmountDocument extends Model
{
    use HasFactory;

    public function user_rel()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function user_property_rel()
    {
        return $this->hasOne(UserProperty::class,'id','user_property_id');
    }

    public function accounts_rel()
    {
        return $this->hasOne(AdminAccount::class,'user_property_id','user_property_id');
    }

}
