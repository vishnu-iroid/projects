<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class OwnerPropertyServiceRequestDocuments extends Model
{
    use HasFactory;

    // protected $table = 'owner_property_service_request_documents';


    public function service_related()
    {
        return $this->hasOne(Services::class, 'id', 'service_id');
    }

    public function owner_doc()
    {
        return $this->hasMany(OwnerPropertyServiceRequestDocuments::class, 'request_id', 'id');
    }
    public function owner_related()
    {
        return $this->hasOne(Owner::class, 'id', 'owner_id');
    }
    public function OwnerPropertyServiceRequestDocuments_relation(){
        return $this->hasOne(OwnerPropertyServiceRequests::class,'id','request_id');
    }

}
