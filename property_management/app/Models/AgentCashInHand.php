<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\OwnerProperty;

class AgentCashInHand extends Model
{
    use HasFactory;


    public function property_rel(){
        return $this->hasOne(OwnerProperty::class,'id','property_id');
    }

    public function user_rel(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
