<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\EventPackages;

class PackageFeatures extends Model
{
    use HasFactory;
    protected $table = 'package_features';
    
    public function events(){
        return $this->belongsTo(EventPackages::class,'package_id','id');
    }
}
