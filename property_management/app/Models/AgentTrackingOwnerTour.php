<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\PendingPropertyForVerification;


class AgentTrackingOwnerTour extends Model
{
    use HasFactory;


    public function verification_property(){
        return $this->hasOne(PendingPropertyForVerification::class,'id','tour_id');
    }
}
