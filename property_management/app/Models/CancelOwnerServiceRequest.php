<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\OwnerPropertyServiceRequests;
class CancelOwnerServiceRequest extends Model
{
    use HasFactory;

    protected $table = 'cancel_owner_service_request';

    public function request_rel(){
        return $this->belongsTo(OwnerPropertyServiceRequests::class, 'request_id', 'id');
    }

}
