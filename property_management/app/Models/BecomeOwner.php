<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BecomeOwner extends Model
{
    use HasFactory;
    protected $table = 'become_owners';
}
