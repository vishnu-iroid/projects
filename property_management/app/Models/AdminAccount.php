<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\OwnerPaymentReceived;
use App\Models\User;
use App\Models\OwnerProperty;
use App\Models\Owner;
use App\Models\UserProperty;

class AdminAccount extends Model
{
    use HasFactory;

    public function user_rel()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function prop_rel()
    {

        return $this->hasOne(OwnerProperty::class, 'id', 'property_id');
    }

    public function owner_rel()
    {

        return $this->hasOne(Owner::class, 'id', 'owner_id');
    }

    public function payment_received()
    {
        return $this->hasOne(OwnerPaymentReceived::class, 'payment_id', 'id');
    }

    public function agent_rel()
    {
        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }

    public function user_property_rel()
    {
        return $this->hasOne(UserProperty::class, 'id', 'user_property_id');
    }

    // public function service_reference()
    // {
    //     return $this->where('payment_type',1)->
    // }
}
