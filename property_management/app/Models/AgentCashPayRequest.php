<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Agent;

class AgentCashPayRequest extends Model
{
    use HasFactory;


    public function Agent_rel()
    {
        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }
}
