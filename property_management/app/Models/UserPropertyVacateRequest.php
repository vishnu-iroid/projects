<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserProperty;
use App\Models\OwnerProperty;


class UserPropertyVacateRequest extends Model
{
    use HasFactory;

    protected $table = 'user_property_vacate_request';

    protected $hidden = ['laravel_through_key'];

    public function user_property_related(){
        return $this->hasOneThrough(OwnerProperty::class,UserProperty::class,'id','id','user_property_id','property_id');
    }
    public function user_rel(){
        return $this->hasOne(User::class,'id','user_id');
    }



}
