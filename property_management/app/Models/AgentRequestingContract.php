<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgentRequestingContract extends Model
{
    use HasFactory;

    protected $table = 'agent_requesting_contract';

    public function agent_rel()
    {
        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }

    public function user_property_rel()
    {
         return $this->hasOne(UserProperty::class, 'id' , 'user_property_id');
    }


}
