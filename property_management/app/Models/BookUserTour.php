<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Agent;
use App\Models\OwnerProperty;
use App\Models\AgentTrackingUserTour;
use App\Models\OwnerPropertyDocument;

class BookUserTour extends Model
{
    use HasFactory;

    protected $table = 'user_tour_bookings';


    public function user_rel()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function agent_rel()
    {

        return $this->hasOne(AgentTrackingUserTour::class, 'tour_id', 'id');
    }



    // public function agent_rel()
    // {

    //     return "helloworld";
    // }



    public function booking_rel()
    {
        return $this->hasOne(BookUserTour::class, 'id', 'tour_id ');
    }


    public function property_details()
    {
        return $this->hasOne(OwnerProperty::class, 'id', 'property_id');
    }

    public function property_priority_image()
    {
        return $this->hasOne(OwnerPropertyDocument::class, 'property_id', 'property_id')->where('type', 0);
    }
}
