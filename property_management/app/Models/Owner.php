<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Owner extends Authenticatable
{
    use HasFactory,HasApiTokens,Notifiable;

    protected $hidden = ['password'];

    /**
     * Get all of the comments for the Owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function property_rel()
    {
        return $this->hasMany(OwnerProperty::class, 'owner_id', 'id');
    }
    public function country_rel(){
        return $this->belongsTo(Country::class,'country');
    }
    public function state_rel(){
        return $this->belongsTo(State::class,'state');
    }
    public function city_rel(){
        return $this->belongsTo(City::class,'city');
    }

    public function zipcode_rel(){
        return $this->hasOne(Pincode::class,'id','zip_code');
    }
}
