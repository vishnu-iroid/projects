<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\State;
use App\Models\City;

class Country extends Model
{
    use HasFactory;

    public function state(){
        return $this->hasMany(State::class,'country','id');
    }
    public function city(){
        return $this->hasMany(City::class,'country','id');
    }
}
