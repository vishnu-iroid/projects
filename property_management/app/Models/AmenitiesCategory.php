<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Amenity;
use App\Models\PropertyTypeAmenityContent;

class AmenitiesCategory extends Model
{
    use HasFactory;

    public function amenity(){
        $this->hasMany(Amenity::class,'id','amenity_id');
    }
    public function availableAmenity(){
        return $this->hasManyThrough(Amenity::class,PropertyTypeAmenityContent::class,'amenity_category_id','id','id','amenity_id');
    }
}
