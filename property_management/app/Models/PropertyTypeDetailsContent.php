<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Detail;

class PropertyTypeDetailsContent extends Model
{
    use HasFactory;

    public function detail(){
        return $this->hasOne(Detail::class,'id','detail_id');
    }
}
