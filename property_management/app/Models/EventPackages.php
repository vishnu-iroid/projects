<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PackageImages;
use App\Models\PackageFeatures;

class EventPackages extends Model
{
    use HasFactory;
    protected $table = 'event_packages';
    

    public function event_package_image(){
        return $this->hasOne(PackageImages::class,'package_id','id');
    }
    public function events(){
        return $this->belongsTo(Event::class,'event_id','id');
    }
    public function event_package_features(){
        return $this->hasMany(PackageFeatures::class,'package_id','id');
    }
}
