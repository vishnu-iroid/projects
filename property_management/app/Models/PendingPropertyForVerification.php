<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Owner;
use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDocument;

class PendingPropertyForVerification extends Model
{
    use HasFactory;

    public function user_property_related(){
        return $this->belongsTo(OwnerProperty::class,'property_id');
    }

    public function property_priority_image(){
        return $this->hasOne(OwnerPropertyDocument::class,'property_id','property_id')->where('type',0);
    }

    public function owner_rel(){
        return $this->belongsTo(Owner::class,'owner_id');
    }
}
