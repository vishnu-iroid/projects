<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgentProperties extends Model
{
    use HasFactory;

    public function agent_rel(){
        return $this->hasOne(Agent::class,'id','agent_id');
    }
    public function prop_rel(){
        return $this->hasOne(OwnerProperty::class,'id','property_id');
    }
}
