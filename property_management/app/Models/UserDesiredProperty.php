<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\PropertyType;

class UserDesiredProperty extends Model
{
    use HasFactory;


    public function property_type(){
        return $this->hasOne(PropertyType::class ,'id','type_id');
    }
}
