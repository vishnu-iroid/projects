<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Pincode;
use App\Models\OwnerPropertyDocument;
use App\Models\OwnerPropertyDetail;
use App\Models\OwnerPropertyAmenity;
use App\Models\Detail;
use App\Models\Owner;
use App\Models\UserFavourites;
use App\Models\UserPropertyBookings;
use App\Models\UserProperty;
use App\Models\Frequency;
use App\Models\PropertyType;
use App\Models\AdminAccount;
use App\Models\AgentProperties;

use Auth;

class OwnerProperty extends Model
{
    use HasFactory;

    protected $hidden = ['laravel_through_key'];

    public function builders(){
        return $this->hasOne(OwnerProperty::class,'builder_id','id');
    }
    public function country_rel(){
        return $this->belongsTo(Country::class,'country');
    }
    public function state_rel(){
        return $this->belongsTo(State::class,'state');
    }
    public function city_rel(){
        return $this->belongsTo(City::class,'city');
    }

    public function zipcode_rel(){
        return $this->hasOne(Pincode::class,'id','zip_code');
    }

    public function frequency_rel(){
        return $this->hasOne(Frequency::class,'id','frequency');
    }

    public function documents(){
        return $this->hasMany(OwnerPropertyDocument::class,'property_id','id')->where('type','!=',2)->orderBy('type','ASC');
    }

    public function property_images(){  //safir
        return $this->hasMany(OwnerPropertyDocument::class,'property_id','id');
    }

    public function property_details(){
        return $this->hasManyThrough(Detail::class,OwnerPropertyDetail::class,'property_id','id','id','detail_id');
    }
    public function owner_property_details(){
        return $this->hasMany(OwnerPropertyDetail::class,'property_id','id');
    }
    public function owner_rel(){
        return $this->belongsTo(Owner::class,'owner_id');
    }
    public function userfavourites(){
        $user_id = Auth::user()->id;
        return $this->hasOne(UserFavourites::class,'property_id','id')->where('user_id',$user_id)->where('status','1');
    }
    public function amenity_details(){
        return $this->hasMany(OwnerPropertyAmenity::class ,'property_id','id');
    }
    public function agent_details(){
        return $this->hasOne(Agent::class ,'id','agent_id');
    }

    public function assigned_agent_details(){
        return $this->hasOne(AgentProperties::class ,'property_id','id')->where('status',1);
    }
    public function assigned_agent_rel(){
        return $this->hasMany(AgentProperties::class ,'property_id','id')->where('status',1);
    }
    public function floor_plans(){
        return $this->hasMany(OwnerPropertyDocument::class,'property_id','id')->where('type',2);
    }
    public function property_priority_image(){
        return $this->hasOne(OwnerPropertyDocument::class,'property_id','id')->where('type',0);
    }

    public function user_booked_property(){
        $user_id = Auth::user()->id;
        return $this->hasOne(UserProperty::class,'property_id','id')
                                                    ->where('user_id',$user_id)
                                                    ->where('status',2); #booked status is 2
    }

    public function user_property_related(){
        $user_id = Auth::user()->id;
        return $this->hasOne(UserProperty::class,'property_id','id')
                                                    ->where('user_id',$user_id)
                                                    ->where('status','!=',3)
                                                    ->where('cancel_status',false);
    }

    public function property_rent_frequency(){
        return $this->hasOne(Frequency::class ,'id','frequency');
    }
    public function assigned_user_desired_property(){
        return $this->hasOne(AssignedUserDesiredProperty::class ,'property_id','id');
    }

    public function last_owner_payament(){
        $owner_id = Auth::user()->id;
        return $this->hasOne(AdminAccount::class ,'property_id','id')->where('owner_id',$owner_id)
                                                                    ->where('payment_type',7)
                                                                    ->orderBy('date','DESC');
    }

    public function first_owner_payament(){
        $owner_id = Auth::user()->id;
        return $this->hasOne(AdminAccount::class ,'property_id','id')->where('owner_id',$owner_id)
                                                                    ->where('payment_type',7)
                                                                    ->orderBy('date','ASC');
    }

    public function type_details(){
        return $this->hasOne(PropertyType::class ,'id','type_id');

    }

    /**
     * Get all of the a for the OwnerProperty
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_service_amount()
    {
        return $this->hasMany(AdminAccount::class, 'property_id', 'id')->where('payment_type',1);

    }
    public function user_token_amount()
    {
        return $this->hasMany(AdminAccount::class, 'property_id', 'id')->where('payment_type',2);

    }

    /**
     * Get all of the comments for the OwnerProperty
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function owner_amount()
    {
        return $this->hasMany(AdminAccount::class, 'property_id', 'id')->where('payment_type',7);
    }

    public function owner_service_amount()
    {
        return $this->hasMany(AdminAccount::class, 'property_id', 'id')->where('payment_type',);
    }
    public function admin_account_type()
    {

        return $this->hasMany(AdminAccount::class, 'property_id', 'id')
        ->where('payment_type',5)->orWhere('payment_type',2);
    }
    public function freequecy_rel(){
        return $this->hasOne(Frequency::class,'id','frequency');
    }
    public function admin_accounts_rel(){
        return $this->hasMany(AdminAccount::class,'property_id','id')->where('payment_type',5);
    }
    public function user_property_rel(){
        return $this->hasOne(UserProperty::class,'user_id','user_id');

    }



}
