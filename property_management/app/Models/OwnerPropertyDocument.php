<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerPropertyDocument extends Model
{
    use HasFactory;

    protected $fillable = ['property_id','document','type','owner_id','created_at','updated_at']; 
}
