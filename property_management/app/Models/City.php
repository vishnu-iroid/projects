<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\State;
use App\Models\Country;

class City extends Model
{
    use HasFactory;

    public function state_rel(){
        return $this->hasOne(State::class,'id','state');
    }
    public function country_rel(){
        return $this->hasOne(Country::class,'id','country');
    }
}
