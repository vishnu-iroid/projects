<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserFeedbacks extends Model
{
    use HasFactory;

    public function user_rel(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
