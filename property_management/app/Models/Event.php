<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\EventImage;
use App\Models\PackageImages;
use App\Models\PackageFeatures;
use App\Models\Admin;


class Event extends Model
{
    use HasFactory;

    public function event_docs()
    {
        return $this->hasMany(EventImage::class, 'event_id', 'id');
    }
    public function event_packages()
    {
        return $this->hasMany(EventPackages::class, 'event_id', 'id');
    }
    public function event_package_features()
    {
        return $this->hasMany(PackageFeatures::class, 'package_id', 'id');
    }
    public function event_package_image()
    {
        return $this->hasOne(PackageImages::class, 'package_id', 'id');
    }

    public function event_priority_image()
    {
        return $this->hasOne(EventImage::class, 'event_id', 'id');
    }


    public function admin_events()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }
}
