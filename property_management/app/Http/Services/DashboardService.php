<?php

namespace App\Http\Services;

use App\Models\Admin;
use App\Models\AdminAccount;
use App\Models\User;
use App\Models\Owner;
use App\Models\Agent;
use App\Models\OwnerProperty;
use App\Models\ServiceProvider;
use App\Models\UserProperty;
use App\Models\UserPropertyServiceRequests;
use Illuminate\Support\Facades\DB;

class DashboardService{

    public function getDashbaordData(){

        $users = User::count();
        $users_active = User::where('status',1)->count();
        $owners = Owner::count();
        $owners_active = Owner::where('status',1)->count();
        $agents = Agent::count();
        $agents_active = Agent::where('status',1)->count();
        $service_provider = ServiceProvider::count();
        $total_income=AdminAccount::where('amount_type','1')->sum('amount');
        $adminAccount=AdminAccount::where('amount_type','1')->get();
        $employee=Admin::count();
        $employee_accountant=Admin::where('role','3')->count();
        $employee_admin=Admin::where('role','1')->count();
        $employee_doc=Admin::where('role','2')->count();
        // $charts=AdminAccount::get();
        $charts = AdminAccount::select('amount','date')->where('amount_type', '1')->get();
        $properties=OwnerProperty::count();
        $propertie_occupied=OwnerProperty::where('occupied','1')->count();
        $propertie_vacant=OwnerProperty::where('occupied','0')->count();
        $tenant=UserProperty::count();
        $tenant_rental=UserProperty::where('status',0)->count();
        $tenant_owner=UserProperty::where('status',1)->count();
       
  
        
        $data = [
          'users' => $users,
          'users_active' => $users_active,
          'owners' => $owners,
          'owners_active' => $owners_active,
          'agents' => $agents,
          'agents_active' => $agents_active,
          'service_provider' => $service_provider,
          'total_income'=> $total_income,
          'adminAccount'=>$adminAccount,
          'employee'=>$employee,
          'employee_accountant'=>$employee_accountant,
          'employee_admin'=>$employee_admin,
          'employee_doc'=> $employee_doc,
          'charts'=>$charts,
          'properties'=>$properties,
          'propertie_occupied'=>$propertie_occupied,
          'propertie_vacant'=>$propertie_vacant,
          'tenant'=>$tenant,
          'tenant_owner'=>$tenant_owner,
          'tenant_rental'=>$tenant_rental,
        

        ];
        return $data;
    }
}

?>