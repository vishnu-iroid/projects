<?php

namespace App\Http\Services\api\owner;

use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDocument;
use App\Models\Agent;
use App\Models\Amenity;
use App\Models\OwnerPropertyAmenity;
use App\Models\OwnerPropertyDetail;
use Validator;
use DB;
use Auth;

class PropertyService{

    public function addProperty($request){
        $property   = new OwnerProperty;
        $maxid      = OwnerProperty::max('property_reg_no');
        if($maxid){
            $exid = explode('-',$maxid)[1] + 1;
            $property->property_reg_no = '#REG'.time().'-'.$exid;
        }else{
            $property->property_reg_no = '#REG'.time().'-'.'100';
        }
        if($request->is_builder == 1){
            $property->is_builder       = 1;
            $property->occupied         = NULL;
            $property->furnished        = NULL;
        }else{
            $property->is_builder       = 0;
            $property->owner_confirmation  = 1;
            $property->occupied         = $request->occupied?$request->occupied:0;
            $property->furnished        = $request->furnished?$request->furnished:0;
        }
        $property->owner_id             = Auth::user()->id;
        $property->property_name        = $request->property_name?$request->property_name:null;
        $property->property_to          = $request->property_to;
        $property->category             = $request->category;
        $property->street_address_1     = $request->address1;
        $property->street_address_2     = $request->address2 ? $request->address2 : "";
        $property->country              = $request->country;
        $property->city                 = $request->city;
        $property->state                = $request->state;
        $property->zip_code             = $request->zipcode;
        $property->description          = $request->description;
        $property->frequency            = $request->frequency?$request->frequency:null;
        $property->status               = '0';
        $property->rent                 = $request->rent? $request->rent : null;
        $property->expected_amount      = $request->expected_amount? $request->expected_amount : null;
        $property->type_id              = $request->type_id?$request->type_id : null;
        $property->longitude            = $request->longitude? $request->longitude : 76.30134207;
        $property->latitude             = $request->latitude ? $request->latitude  : 10.00084910; 
        try{
            DB::beginTransaction();
            $property->save();
            $savedoc = array(); $savefloor= array(); $savevideo= array(); $amenities= array();
            $savebuilder = array();

            if($request->file('building_images')){
                foreach($request->file('building_images') as $key=>$row){
                    $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'),$imagename);
                    $savebuilder[$key]['property_id'] = $property->id;
                    $savebuilder[$key]['document']    = '/uploads/property/images/'.$imagename;
                    $savebuilder[$key]['type']        = '0';
                    $savebuilder[$key]['owner_id']    = Auth::user()->id;
                    $savebuilder[$key]['created_at']  = now();
                    $savebuilder[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savebuilder);
            }
            
            if($request->file('images')){
                foreach($request->file('images') as $key=>$row){
                    $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'),$imagename);
                    $savedoc[$key]['property_id']   = $property->id;
                    $savedoc[$key]['document']      = '/uploads/property/images/'.$imagename;
                    $savedoc[$key]['type']          = '0';
                    $savedoc[$key]['owner_id']      = Auth::user()->id;
                    $savedoc[$key]['created_at']    = now();
                    $savedoc[$key]['updated_at']    = now();
                }
                OwnerPropertyDocument::insert($savedoc);
            }
            if($request->file('floor_plans')){
                foreach($request->file('floor_plans') as $key=>$row){
                    $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/floor_plans'),$imagename);
                    $savefloor[$key]['property_id'] = $property->id;
                    $savefloor[$key]['document']    = '/uploads/property/floor_plans/'.$imagename;
                    $savefloor[$key]['type']        = '2';
                    $savefloor[$key]['owner_id']    = Auth::user()->id;
                    $savefloor[$key]['created_at']  = now();
                    $savefloor[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savefloor);
            }
            if($request->file('videos')){
                foreach($request->file('videos') as $key=>$row){
                    $videoname = time().rand().'.'.$row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/videos'),$videoname);
                    $savevideo[$key]['property_id'] = $property->id;
                    $savevideo[$key]['document']    = '/uploads/property/videos/'.$videoname;
                    $savevideo[$key]['type']        = '1';
                    $savevideo[$key]['owner_id']    = Auth::user()->id;
                    $savevideo[$key]['created_at']  = now();
                    $savevideo[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savevideo);
            }
            if($request->amenities){
                foreach($request->amenities as $key=>$row){
                    $category_id = Amenity::where('id',$row)->pluck('amenity_category_id');
                    foreach($amenities as $row6){
                        if($row6['property_id'] == $property->id && $row6['amenity_id'] == $row && $row6['amenities_category_id'] == $category_id[0] && $row6['amenities_category_id'] == Auth::user()->id){
                            DB::rollback();
                            return ['status'=>400,'response'=>'Something went wrong'];
                        }
                    }
                    $amenities[$key]['property_id']             = $property->id;
                    $amenities[$key]['amenity_id']              = $row;
                    $amenities[$key]['amenities_category_id']   = $category_id[0];
                    $amenities[$key]['owner_id']                = Auth::user()->id;
                    $amenities[$key]['created_at']              = now();
                    $amenities[$key]['updated_at']              = now();
                }
                OwnerPropertyAmenity::insert($amenities);
            }
            if($request->detailskey){
                $max            = sizeof($request->detailskey);
                $detailsvalue   = $request->detailsvalue;
                $detailkeyvalue = $request->detailskey;

                for($i=0; $i < $max; $i++){

                    if($request->detailskey[$i]!=""){
                        //dd($detailkeyvalue[$i]);
                        $details               = new OwnerPropertyDetail();
                        $details['property_id']= $property->id;
                        $details['detail_id']  = $detailkeyvalue[$i];
                        $details['value']      = $detailsvalue[$i];
                        $details['owner_id']   = Auth::user()->id;
                        $details['created_at'] = now();
                        $details['updated_at'] = now();
                        $details->save();
                    }
                }
            }

            DB::commit();
            return ['status'=>200,'response'=>'Successfully added property'];
        }catch(\Exception $e){
            DB::rollback();
            return ['status'=>400,'response'=>$e->getMessage()];
        }
    }


    public function addBuildingApartment($request){

        $rules = [
            'building_id' => 'required',
        ];
        $messages = [
            'building_id' => 'Building is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $building_id        = $request->building_id;
            $building_details   = OwnerProperty::where('id',$building_id)->first();
            if($building_details){
                $property   = new OwnerProperty;
                $maxid      = OwnerProperty::max('property_reg_no');
                if($maxid){
                    $exid = explode('-',$maxid)[1] + 1;
                    $property->property_reg_no = '#REG'.time().'-'.$exid;
                }else{
                    $property->property_reg_no = '#REG'.time().'-'.'100';
                }
                $property->owner_id             = Auth::user()->id;
                $property->builder_id           = $building_details->id;
                $property->property_name        = $request->property_name?$request->property_name:null;
                $property->property_to          = $building_details->property_to;
                $property->category             = $building_details->category;
                $property->street_address_1     = $building_details->street_address_1;
                $property->street_address_2     = $building_details->street_address_2 ? $building_details->street_address_2 : null;
                $property->country              = $building_details->country;
                $property->city                 = $building_details->city;
                $property->state                = $building_details->state;
                $property->zip_code             = $building_details->zip_code;
                $property->description          = $request->description?$request->description:null;
                $property->furnished            = $request->furnished;
                $property->frequency            = $request->frequency? $request->frequency : null;
                $property->status               = '0';
                $property->rent                 = $request->rent;
                $property->expected_amount      = $request->expected_amount? $request->expected_amount : null;
                $property->type_id              = $building_details->type_id;
                $property->longitude            = $request->longitude? $request->longitude : $building_details->longitude;
                $property->latitude             = $request->latitude ? $request->latitude : $building_details->latitude;
                $property->is_builder           = NULL;
                $property->occupied             = $request->occupied?$request->occupied:0;
                DB::beginTransaction();
                $property->save();
                $savedoc = array(); $savefloor= array(); $savevideo= array(); $amenities= array();
                
                if($request->file('images')){
                    foreach($request->file('images') as $key=>$row){
                        $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/images'),$imagename);
                        $savedoc[$key]['property_id']   = $property->id;
                        $savedoc[$key]['document']      = '/uploads/property/images/'.$imagename;
                        $savedoc[$key]['type']          = '0';
                        $savedoc[$key]['owner_id']      = Auth::user()->id;
                        $savedoc[$key]['created_at']    = now();
                        $savedoc[$key]['updated_at']    = now();
                    }
                    OwnerPropertyDocument::insert($savedoc);
                }
                if($request->file('floor_plans')){
                    foreach($request->file('floor_plans') as $key=>$row){
                        $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/floor_plans'),$imagename);
                        $savefloor[$key]['property_id'] = $property->id;
                        $savefloor[$key]['document']    = '/uploads/property/floor_plans/'.$imagename;
                        $savefloor[$key]['type']        = '2';
                        $savefloor[$key]['owner_id']    = Auth::user()->id;
                        $savefloor[$key]['created_at']  = now();
                        $savefloor[$key]['updated_at']  = now();
                    }
                    OwnerPropertyDocument::insert($savefloor);
                }
                if($request->file('videos')){
                    foreach($request->file('videos') as $key=>$row){
                        $videoname = time().rand().'.'.$row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/videos'),$videoname);
                        $savevideo[$key]['property_id'] = $property->id;
                        $savevideo[$key]['document']    = '/uploads/property/videos/'.$videoname;
                        $savevideo[$key]['type']        = '1';
                        $savevideo[$key]['owner_id']    = Auth::user()->id;
                        $savevideo[$key]['created_at']  = now();
                        $savevideo[$key]['updated_at']  = now();
                    }
                    OwnerPropertyDocument::insert($savevideo);
                }
                if($request->amenities){
                    foreach($request->amenities as $key=>$row){
                        $category_id = Amenity::where('id',$row)->pluck('amenity_category_id');
                        foreach($amenities as $row6){
                            if($row6['property_id'] == $property->id && $row6['amenity_id'] == $row && $row6['amenities_category_id'] == $category_id[0] && $row6['amenities_category_id'] == Auth::user()->id){
                                DB::rollback();
                                return ['status'=>400,'response'=>'Something went wrong'];
                            }
                        }
                        $amenities[$key]['property_id']             = $property->id;
                        $amenities[$key]['amenity_id']              = $row;
                        $amenities[$key]['amenities_category_id']   = $category_id[0];
                        $amenities[$key]['owner_id']                = Auth::user()->id;
                        $amenities[$key]['created_at']              = now();
                        $amenities[$key]['updated_at']              = now();
                    }
                    OwnerPropertyAmenity::insert($amenities);
                }

                if($request->detailskey){
                    $max            = sizeof($request->detailskey);
                    $detailsvalue   = $request->detailsvalue;
                    $detailkeyvalue = $request->detailskey;

                    for($i=0; $i < $max; $i++){

                        if($request->detailskey[$i]!=""){
                            //dd($detailkeyvalue[$i]);
                            $details               = new OwnerPropertyDetail();
                            $details['property_id']= $property->id;
                            $details['detail_id']  = $detailkeyvalue[$i];
                            $details['value']      = $detailsvalue[$i];
                            $details['owner_id']   = Auth::user()->id;
                            $details['created_at'] = now();
                            $details['updated_at'] = now();
                            $details->save();
                        }
                    }
                }
                DB::commit();
                return ['status'=>200,'response'=>'Successfully added apartment'];
            }else{
                return ['status'=>200,'response'=>'Invalid Building'];
            }
        }catch(\Exception $e){
            DB::rollback();
            return ['status'=>400,'response'=>$e->getMessage()];
        }
    }
}

?>
