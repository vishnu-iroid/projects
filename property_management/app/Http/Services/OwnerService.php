<?php

namespace App\Http\Services;

use Illuminate\Support\Str;

use App\Models\Owner;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Pincode;
use App\Models\Amenity;
use App\Models\AmenitiesCategory;
use App\Models\BankDetails;
use App\Models\PropertyType;
use App\Models\PropertyTypeAmenityContent;
use App\Models\Frequency;
use App\Models\PropertyTypeDetailsContent;

use DB;

class OwnerService {

    public function getOwner($request){
        $owner     = Owner::orderBy('id','DESC')->get();
        $countries = Country::orderBy('id','DESC')->get();
        $states    = State::orderBy('id','DESC')->get();
        $frequency = Frequency::all();
        $banks      = BankDetails::orderBy('id','DESC')->get();
        $types     = PropertyType::orderBy('id','DESC')->get();
        if(count($types)>0)
        {
            $amenities_listing = $this->availableAmenities($types[0]->id);
            $details = $this->availableDetails($types[0]->id);
        }else
        {
            $amenities_listing = array();
            $details = array();
        }
        $data = [
            'owners'  => $owner,
            'countries' => $countries,
            'states'  => $states,
            'amenities_listing' => $amenities_listing,
            'types'   => $types,
            'details' => $details,
            'frequency' =>$frequency,
            'banks'     => $banks
        ];
        return $data;
    }

    public function listAmenities($id){
        $amenities_listing = $this->availableAmenities($id);
        $html = "";
        foreach($amenities_listing as $row){
            $part2 = "";
            $part1 =    '<div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> '.$row->name.' <span class="tx-danger"></span></label><br>
                                    <div class="row mg-l-1">';
            foreach($row->amenities as $row2){
                        $part2  .=      '<label class="ckbox ml-2">
                                            <input type="checkbox" value="'.$row2->id.'" name="amenities[]">
                                            <span>'.$row2->name.'</span>
                                        </label>';
            }
            $part3 =                '</div>
                                </div>
                            </div>
                        </div>';
            $html .= $part1.$part2.$part3;
        }
        return $html;
    }

    public function amenitiesAndDetails($type){
        $response['amenitiesData'] = $this->availableAmenities($type);
        $response['details'] = $this->availableDetails($type);
        return $response;
    }

    public function availableAmenities($type){
        $amenities_listing = [];
        $base_url          = url('/');
        $amenities_listing = AmenitiesCategory::join('property_type_amenity_contents','amenities_categories.id','=','property_type_amenity_contents.amenity_category_id')
                                        ->where('property_type_amenity_contents.type_id',$type)
                                        ->select('amenities_categories.id','amenities_categories.name')
                                        ->orderBy('sort_order','ASC')
                                        ->distinct()
                                        ->get();
        foreach($amenities_listing as $row){
            $data = Amenity::join('property_type_amenity_contents','amenities.id','=','property_type_amenity_contents.amenity_id')
                        ->where('property_type_amenity_contents.type_id',$type)
                        ->where('amenities.amenity_category_id',$row->id)
                        ->select('amenities.id','amenities.name',DB::raw('CONCAT("' . $base_url . '",image) AS image'))
                        ->get();
            $row->amenities = $data;
        }
        return $amenities_listing;
    }

    public function availableDetails($type){
        $details = [];
        $details = PropertyTypeDetailsContent::join('details','details.id','=','property_type_details_contents.detail_id')
                                        ->where('property_type_details_contents.type_id',$type)
                                        ->select('detail_id','name','sort_order','placeholder')
                                        ->orderBy('details.sort_order','ASC')
                                        ->get();
        return $details;
    }
}

?>
