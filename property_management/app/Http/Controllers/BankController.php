<?php

namespace App\Http\Controllers;

use App\Models\BankDetails;
use Validator;
use Illuminate\Http\Request;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banks = BankDetails::paginate(10);

        // dd($banks);
        $data = [
            'banks' => $banks,
        ];
        return view('admin.bank.bankes', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'bank_name' => 'required',
        ];
        $messages = [
            'bank_name.required' => 'Service name is required',
        ];
        if ($request->bankId) {
             $data =  BankDetails::find($request->bankId);
             $msg = 'Updated Bank successfully';
            $rules['bank_name'] = 'unique:services,service,' . $request->serviceId;
        } else {
            $data = new BankDetails();
            $msg = 'Bank Added  successfully';
            $rules['bank_name'] = 'unique:services,service';
            $messages['image.required'] = 'image is required';
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
         $data->bank_name = $request->bank_name;
        try {
            $data->save();
            return response()->json(['status' => true, 'response' => $msg]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'response' => $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (!$id) {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
        $data = BankDetails::find($id);
        return response()->json(['status' => true, 'response' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            BankDetails::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted Bank Name']);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }


}
