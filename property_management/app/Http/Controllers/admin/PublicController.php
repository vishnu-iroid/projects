<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PropertyType;
use App\Models\City;
use App\Models\Pincode;
use App\Models\State;

class PublicController extends Controller
{
    public function getState($countryId){
        if(!$countryId){
            return response()->json(['status'=>false,'response'=>"Something went wrong"]); 
        }else{
            $state=State::where('country',$countryId)->get();
            return response()->json(['status'=>true,'response'=>$state]);
        }
    }
    public function getCity($stateId){
        if(!$stateId){
            return response()->json(['status'=>false,'response'=>"Something went wrong"]); 
        }
        $city = City::where('state',$stateId)->get();
        return response()->json(['status'=>true,'response'=>$city]);
    }

    public function getCategory($categoryId){
        if(!$categoryId){
            return response()->json(['status'=>false,'response'=>"Something went wrong"]); 
        }else{
            if($categoryId == 1){
                $category   = 0;
            }else{
                $category   = 1;
            }
        }
        $category = PropertyType::where('category',$category)->get();
        return response()->json(['status'=>true,'response'=>$category]);
    }

 
    public function getPincode($cityId){
        if(!$cityId){
            return response()->json(['status'=>false,'response'=>"Something went wrong"]); 
        }
        $pincode = Pincode::where('city',$cityId)->get();
        return response()->json(['status'=>true,'response'=>$pincode]);
    }
}
