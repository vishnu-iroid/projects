<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AgentTrackingUserTour;
use App\Models\UserPropertyBookings;
use App\Models\AdminAccount;
use App\Models\AgentEarnings;
use Illuminate\Support\Facades\DB;

class ExpenseController extends Controller
{
    //

    public function showAgentPaidList()
    {

        $details = AdminAccount::where('payment_type', '9')->with('agent_rel:id,name,phone')->get();

        $data = [
            'details' => $details
        ];
        return view('admin.accounts.agent_paid_list', $data);
    }
    public function showAgentPayment()
    {
  
        $details = AgentEarnings::where('status', 0 )->with('agent_rel:id,name,email,phone,address')->get();

        $data = ['agents' => $details];
        return view('admin.accounts.showagentdetails', $data);
    }


    public function viewAgentTrackingDetails($agent_id)
    {


        $details = UserPropertyBookings::where('assigned_agent', $agent_id)
            ->whereNotIn('property_id', function ($q) use ($agent_id) {
                $q->select('property_id')->where('agent_id', $agent_id)->from('agent_payment_histories');
            })
            ->where('approve_status', 1)
            ->where('cancel_status', 0)
            ->where('is_verified', 1)
            ->with('agent_details:id,name', 'property_details:id,property_name,property_reg_no', 'user_details:id,name')->get();

        $data = [
            'datas' => $details,
            'agent_id' => $agent_id
        ];
        return view('admin.accounts.viewagentdetails', $data);
    }

    public function payAgent(Request $request)
    {
        $id = "";
       
        if ($request->refreal_id) {

            $id = $request->refreal_id;
            $data = AgentEarnings::find($id);
            $payment = new AdminAccount();
            $payment->amount_type = 2;
            $payment->amount = $request->amount;
            $payment->payment_type = 9;
            $payment->agent_id = $data->agent_id;
            $payment->date = $request->date;
            $payment->save();

            $update = AgentEarnings::where('id', $id)->update(['status' => '1']);

            $msg = "Payment Successfully";
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went Wrong']);
        }
    }
}
