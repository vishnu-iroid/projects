<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\Response;
use App\Http\Services\OwnerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDocument;
use App\Models\Agent;
use App\Models\Amenity;
use App\Models\OwnerPropertyAmenity;
use App\Models\OwnerPropertyDetail;
use App\Models\Frequency;
use App\Models\PendingPropertyAgent;
use App\Models\PendingPropertyForVerification;
use App\Models\AgentTrackingOwnerTour;
use App\Models\AmenitiesCategory;
use App\Models\Country;
use App\Models\Owner;
use App\Models\PropertyType;
use App\Models\PropertyTypeDetailsContent;
use App\Models\State;
use App\Models\City;
use App\Models\Detail;
use App\Models\Pincode;
use Illuminate\Support\Arr;
use Validator;
use Illuminate\Support\Facades\DB;

class PropertyController extends Controller
{
    public function showProperty(Request $request)
    {
        $status = $request->status;
        if ($status == "") {
            $status = 0;
        }

        if ($request->status == 0) {
            $property = OwnerProperty::orderBy('id', 'DESC')
                ->where('status', 0)
                //  ->where('owner_confirmation', 1)
                ->whereIn('is_builder', [0, 1])
                ->with('country_rel:id,name', 'city_rel:id,name', 'state_rel:id,name')
                ->paginate(10);
            $types     = PropertyType::orderBy('id', 'DESC')->get();
            if (count($types) > 0) {
                $amenities_listing = $this->availableAmenities($types[0]->id);
                $details = $this->availableDetails($types[0]->id);
            } else {
                $amenities_listing = array();
                $details = array();
                $details_prop = array();
            }

            $owners = Owner::all();
            $countries = Country::all();
            $frequency = Frequency::select('id', 'type')->get();
            $data = [
                'property' => $property,
                'owners' => $owners,
                'countries' => $countries,
                'amenities_listing' => $amenities_listing,
                'types'   => $types,
                'details' => $details,
                'frequency' => $frequency,



            ];

            $agents = Agent::orderBy('id', 'DESC')->where('status', 1)->get();
            $data['agents'] = $agents;
            // dd($data['details']);
            return view('admin.property.property', $data);
        } elseif ($request->status == 1) {
            //get all agent verified properties
            $verification_properties    = AgentTrackingOwnerTour::with('verification_property:id,property_id')->where('status', 2)->get();
            $properties                 = json_decode($verification_properties);
            if ($properties) {
                $property_array         = array_column($properties, 'verification_property');
                $property_ids           = array_column($property_array, 'property_id');
            } else {
                $property_ids           = [0];
            }
            $properties_list            = OwnerProperty::orderBy('id', 'DESC')
                ->whereIn('id', $property_ids)
                ->whereIn('is_builder', [0, 1])
                ->where('contract_owner', '!=', '1')
                ->where('status', 1)
                ->with('country_rel:id,name', 'city_rel:id,name', 'state_rel:id,name')
                ->paginate(10);

            $frequency = Frequency::select('id', 'type')->get();
            $data['property']   = $properties_list;
            $data['frequency']  = $frequency;
            return view('admin.property.property_approved', $data);
        } elseif ($request->status == 2) {
            $properties_list            = OwnerProperty::orderBy('id', 'DESC')
                ->where('status', 2)
                ->with('country_rel:id,name', 'city_rel:id,name', 'state_rel:id,name')
                ->paginate(10);

            $frequency = Frequency::select('id', 'type')->get();
            $data['property']   = $properties_list;
            return view('admin.property.property_rejected', $data);
        }
    }
    public function addProperty(Request $request)
    {
        // dd($request->all());
        if (!$request->property_owner_id) {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
        $rules = [
            'property_name'     => 'required',
            'category'          => 'required',
            'property_country'  => 'required',
            'property_state'    => 'required',
            'property_city'     => 'required',
            'property_zipcode'  => 'required',
            // 'furnished'         => 'required',
            'building_image'    => 'array',
            'building_image.*'  => 'max:3000|mimes:jpeg,jpg,png',
            'images'            => 'array',
            'images.*'          => 'max:3000|mimes:jpeg,jpg,png',
            'floor_plans'       => 'nullable|array',
            'floor_plans.*'     => 'max:3000|mimes:jpeg,jpg,png',
            'videos'            => 'nullable|array',
            'videos.*'          => 'max:3000|mimes:mp4,mov,ogg,qt,webm',
            'amenities'         => 'array',
            'detailsdata'       => 'array|min:1',
            // 'detailsdata.*' => 'required',
            'contract_no'       => 'required',
            'adsnumber'         => 'required',
            'guard_number'      => 'required',
            'contract_file'     => 'required',
            'commision_as'      => 'required',
            'owner_amount'      => 'required',
            'property_management_fee_type' => 'required',
            'management_fee'    => 'required',
            'contract_start_date' => 'required',
            'contract_end_date' => 'required',
            'selling_price'     => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'mrp'               => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'rent'              => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'security_deposit'  => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'token_amount'      => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            // 'description' => 'required',
        ];
        $messages = [
            'property_name.required' => 'Property Name to is required',
            // 'isbuilding.required' => 'select any on',
            'property_to.required' => 'property to is required',
            'property_country.required' => 'country is required',
            'property_state.required' => 'state is required',
            'property_city.required' => 'city is required',
            'property_zipcode.required' => 'zipcode is required',
            'images.required' => 'images are required',
            'furnished.required' => 'Furnish is required',
            'detailsdata.required' => 'Details fields are required',
            'contract_no.required' => 'Contract Number is required',
            'contract_file.required' => 'Contract file is required',
            'contract_start_date.required' => 'Contract start date is required',
            'contract_end_date.required' => 'Contract end date is required',
            'token_amount.required' => 'Token amount is required',
            // 'description.required' => 'Description fields is required',
        ];

        // return $request->all();

        if (!$request->individual_building) {
            $detailsdata = "";
        } else {
            $detailsdata = $request->detailsdata;
        }

        if ($detailsdata) {
            foreach ($detailsdata as $key => $value) {
                $messages['detailsdata' . $key] = 'Details ' . $key . ' field is required';
            }
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        $furnished  = $request->furnished;
        if (!in_array($furnished, array(0, 1, 2))) {
            return response()->json(['status' => false, 'response' => "Please enter a valid furnish type"]);
        }
        $property = new OwnerProperty;
        $maxid = OwnerProperty::max('property_reg_no');
        $exid  = 000;
        if ($maxid) {
            $exid = explode('-', $maxid)[1] + 1;
            $property->property_reg_no = '#REG' . time() . '-' . $exid;
        } else {
            $property->property_reg_no = '#REG' . time() . '-' . '100';
        }

        // dd($request->all());

        $contract_no                    = $request->contract_no;
        $contract_type                  = $request->contract_type;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
        $type                           = $request->type;
        $property_to                    = $request->property_to;
        $property_name                  = $request->property_name;
        $unit_num                       = $request->unit_num;
        $floor_num                      = $request->floor_num;
        $commission_in                  = $request->commision_as;

        // property management fee
        $property_management_fee_type = $request->property_management_fee_type;
        $adsnumber = $request->adsnumber;
        $guardnumber = $request->guard_number;


        $property->is_builder           = $request->isbuilding ? $request->isbuilding : '0';
        $property->owner_id             = $request->property_owner_id;
        $property->property_name        = $property_name;
        $property->property_to          = $property_to;
        $property->unit_num             = $unit_num;
        $property->floor_num            = $floor_num;
        $property->commission_in        = $commission_in;


        // property management fee
        $property->property_management_fee_type = $property_management_fee_type;
        $property->ads_number = $adsnumber;
        $property->guards_number = $guardnumber;


        $property->type_id              = $type;
        if ($request->category == 1) {
            $property->category             = 0;
        } else {
            $property->category             = 1;
        }
        // dd($request->description);
        $property->street_address_1     = $request->address1;
        $property->street_address_2     = $request->address2 ? $request->address2 : "";
        $property->description          = $request->description;
        $property->country              = $request->property_country;
        $property->city                 = $request->property_city;
        $property->state                = $request->property_state;
        $property->zip_code             = $request->property_zipcode;
        $property->occupied             = $request->property_status;
        $property->status               = '1';
        $property->furnished            = $request->furnished ? $request->furnished : '0';
        $property->contract_no          = $contract_no ? $contract_no : null;
        $property->contract_type        = $contract_type ? $contract_type : null;
        $property->contract_start_date  = $contract_start_date ? $contract_start_date : null;
        $property->contract_end_date    = $contract_end_date ? $contract_end_date : null;
        $property->property_to          = $property_to;
        $property->contract_owner       = '1';
        $property->commission           = $request->owner_amount ? $request->owner_amount : null;

        // property management fee
        $property->management_fee = $request->management_fee ? $request->management_fee : null;
        $property->ads_number = $adsnumber ? $adsnumber : null;
        $property->guards_number = $guardnumber ? $guardnumber : null;


        $property->latitude             = $request->lat_prop ? $request->lat_prop : 9.93123300;
        $property->longitude            = $request->lng_prop ? $request->lng_prop : 76.267303;


        if ($request->individual_building  == 1) {
            $frequency                      = $request->frequency;
            $selling_price                  = $request->min_sellingprice;
            $mrp                            = $request->max_sellingprice;

            $property->occupied             =  $request->property_status;
            $property->furnished        = $request->furnished ? $request->furnished : '0';

            $property->selling_price        = $selling_price ? $selling_price : null;
            $property->is_builder           = 0;
            $property->mrp                  = $mrp ? $mrp : null;
            $property->frequency            = $frequency ? $frequency : null;
            $property->rent                 = $request->min_rent ? $request->min_rent : null;
            $property->token_amount         = $request->token_amount ? $request->token_amount : null;
            $property->security_deposit     = $request->security_deposit ? $request->security_deposit : null;
        } else {
            $property->is_builder           = 1;
        }

        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $property->contract_file        = $file_name;
        }

        // ownership document
        if ($request->file('ownership_doc')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('ownership_doc')) {
                $path = public_path('uploads/property/ownership_doc/');
                $file          = $request->file('ownership_doc');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/ownership_doc/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $property->ownership_doc        = $file_name;
        }

        try {
            DB::beginTransaction();
            $property->save();
            $savedoc = array();
            $savefloor = array();
            $savevideo = array();
            $amenities = array();
            $building_image = array();

            if ($request->file('building_image')) {
                // dd("hjty");
                foreach ($request->file('building_image') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'), $imagename);
                    $building_image[$key]['property_id']   = $property->id;
                    $building_image[$key]['document']      = '/uploads/property/images/' . $imagename;
                    $building_image[$key]['type']          = '3';
                    $building_image[$key]['owner_id']      = $request->property_owner_id;
                    $building_image[$key]['created_at']    = now();
                    $building_image[$key]['updated_at']    = now();
                }
                OwnerPropertyDocument::insert($building_image);
            }

            if ($request->file('images')) {
                foreach ($request->file('images') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'), $imagename);
                    $savedoc[$key]['property_id']   = $property->id;
                    $savedoc[$key]['document']      = '/uploads/property/images/' . $imagename;
                    $savedoc[$key]['type']          = '0';
                    $savedoc[$key]['owner_id']      = $request->property_owner_id;
                    $savedoc[$key]['created_at']    = now();
                    $savedoc[$key]['updated_at']    = now();
                }
                OwnerPropertyDocument::insert($savedoc);
            }
            if ($request->file('floor_plans')) {
                foreach ($request->file('floor_plans') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                    $savefloor[$key]['property_id'] = $property->id;
                    $savefloor[$key]['document'] = '/uploads/property/floor_plans/' . $imagename;
                    $savefloor[$key]['type'] = '2';
                    $savefloor[$key]['owner_id'] = $request->property_owner_id;
                    $savefloor[$key]['created_at'] = now();
                    $savefloor[$key]['updated_at'] = now();
                }
                OwnerPropertyDocument::insert($savefloor);
            }
            if ($request->file('videos')) {
                foreach ($request->file('videos') as $key => $row) {
                    $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/videos'), $videoname);
                    $savevideo[$key]['property_id'] = $property->id;
                    $savevideo[$key]['document'] = '/uploads/property/videos/' . $videoname;
                    $savevideo[$key]['type'] = '1';
                    $savevideo[$key]['owner_id'] = $request->property_owner_id;
                    $savevideo[$key]['created_at'] = now();
                    $savevideo[$key]['updated_at'] = now();
                }
                OwnerPropertyDocument::insert($savevideo);
            }
            if ($request->amenities) {
                foreach ($request->amenities as $key => $row) {
                    $category_id = Amenity::where('id', $row)->pluck('amenity_category_id');
                    $amenities[$key]['property_id'] = $property->id;
                    $amenities[$key]['amenity_id']  = $row;
                    $amenities[$key]['amenities_category_id'] = $category_id[0];
                    $amenities[$key]['owner_id']   = $request->property_owner_id;
                    $amenities[$key]['created_at'] = now();
                    $amenities[$key]['updated_at'] = now();
                }
            }
            OwnerPropertyAmenity::insert($amenities);
            if ($detailsdata) {

                foreach ($detailsdata as $key => $detail) {
                    $details[$key]['property_id']   = $property->id;
                    $details[$key]['detail_id']     = $key;
                    $details[$key]['value']         = $detail;
                    $details[$key]['owner_id']      = (int)$request->property_owner_id;
                    $details[$key]['created_at']    = now();
                    $details[$key]['updated_at']    = now();
                }
                OwnerPropertyDetail::insert($details);
            }
            DB::commit();

            $return_data = [
                'property_id' => $property->id,
            ];
            // return redirect()->route('showPropertyManage');
            return response()->json(['status' => true, 'response' => 'Successfully added property', 'response_data' => $return_data]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }
    public function viewProperty($id)
    {
        $owner = OwnerProperty::where('id', $id)
            ->with('documents', 'country_rel', 'state_rel', 'city_rel', 'property_details')
            ->first();
        return response()->json(['status' => true, 'response' => $owner]);
    }
    public function disableProperty($id, $action)
    {
        if ($id) {
            OwnerProperty::where('id', $id)->update(['status' => $action]);

            if ($action == 0) {
                $msg = 'Blocked property successfully';
            } else {
                $msg = 'Unblocked property successfully';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }
    public function addAgentinProperty(Request $request)
    {
        //dd($request->property_id);

        $rules = [
            'agents' => 'required|array|min:1',
        ];
        $messages = [
            'agents.required' => 'Agent is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        $agent_verfication  =  new PendingPropertyForVerification;
        $agent_verfication->property_id = $request->property_id;
        $agent_verfication->verification_date = $request->date;
        $agent_verfication->time = $request->fromtime;
        $agent_verfication->status = 0;
        $agent_verfication->save();

        $agents = sizeof($request->agents);
        for ($i = 0; $i < $agents; $i++) {
            $agentsProperty                 = new PendingPropertyAgent();
            $agentsProperty->property_id    = $request->property_id;
            $agentsProperty->agent_id       = $request->agents[$i];
            $agentsProperty->status         = '0';
            $agentsProperty->save();
        }

        $msg = "successfully Agent Added";
        return response()->json(['status' => true, 'response' => $msg]);
    }
    public function addContractDetails(Request $request)
    {
        $rules = [
            'selling_price'          => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'mrp'                    => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'rent'                   => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'security_deposit'       => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'token_amount'           => 'regex:/^\d+(\.\d{1,2})?$/|nullable',

        ];
        $messages = [];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        $contract_no                    = $request->contract_no;
        $property_id                    = $request->property_id;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
        $rent                           = $request->rent;
        $frequency                      = $request->frequency;
        $selling_price                  = $request->selling_price;
        $mrp                            = $request->mrp;
        $token_amount                   = $request->token_amount;
        $security_deposit               = $request->security_deposit;
        $commission_in                  = $request->commision_as;
        $commission                     = $request->commission;
        $property_management_fee_type = $request->property_management_fee_type;

        $adsnumber = $request->adsnumber;
        $guardnumber = $request->guard_number;


        if ($request->property_to  == '0') {
            $property_to                    = 0;
        } else {
            $property_to                    = 1;
        }





        $owner                          = OwnerProperty::where('id', $property_id)->first();
        $owner->contract_no             = $contract_no;
        $owner->contract_start_date     = $contract_start_date ? $contract_start_date : null;
        $owner->contract_end_date       = $contract_end_date ? $contract_end_date : null;
        $owner->selling_price           = $selling_price ? $selling_price : null;
        $owner->mrp                     = $mrp ? $mrp : null;
        $owner->frequency               = $frequency ? $frequency : null;
        $owner->rent                    = $rent ? $rent : null;
        $owner->token_amount            = $token_amount ? $token_amount : null;
        $owner->security_deposit        = $security_deposit ? $security_deposit : null;
        $owner->property_to             = $property_to;
        $owner->contract_owner          = '1';
        $owner->status          = '1';
        $owner->commission_in           = $commission_in ? $commission_in : 'amount';
        $owner->commission              = $commission ? $commission : 0;
        $owner->token_amount            = $request->token_amount ? $request->token_amount : '0';
        $owner->security_deposit        = $request->security_deposit ? $request->security_deposit : '0';


        // property management fee
        // property management fee

        $owner->property_management_fee_type = $property_management_fee_type ? $property_management_fee_type : 'amount';
        $owner->management_fee          = $request->management_fee ? $request->management_fee : null;
        $owner->ads_number = $adsnumber;
        $owner->guards_number = $guardnumber;

        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $property_id . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $owner->contract_file        = $file_name;
        }
        $exid  = 000;
        if ($request->file('ownership_doc')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('ownership_doc')) {
                $path = public_path('uploads/property/ownership_doc/');
                $file          = $request->file('ownership_doc');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/ownership_doc/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $owner->ownership_doc        = $file_name;
        }
        $owner->save();
        if ($owner) {
            $property_id        = $owner->id;
            $agent_detail       = PendingPropertyForVerification::where('property_id', $property_id)->first();
            if ($agent_detail) {
                AgentTrackingOwnerTour::where('tour_id', $agent_detail->id)->where('agent_id', $agent_detail->agent_id)->update(['status' => 3]);
                OwnerProperty::where('id', $property_id)->update(['status' => 1]);
            }
        }
        return response()->json(['status' => true, 'response' => "Success"]);
    }


    public function propertyAgent($id)
    {
        $properties = OwnerProperty::where('id', $id)->select('id', 'latitude', 'longitude')->first();
        $latitude  =  $properties->latitude;
        $longitude  = $properties->longitude;
        if ($latitude   &&  $longitude) {
            $agent = Agent::select('id', 'name', DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                + sin(radians(" . $latitude . ")) * sin(radians(latitude))) AS distance"))
                ->orderBy('distance', 'ASC')
                ->where('status', 1)
                ->get();
            if (!$agent) {
                $agent = Agent::select('id', 'name')
                    ->orderBy('distance', 'ASC')
                    ->where('status', 1)
                    ->get();
            }
            return response()->json(['status' => true, 'agent' => $agent]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }



    public function addPropertyUnits(Request $request)
    {
        $unit_count = $request->unit_count;

        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id' => 'Building is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        try {
            DB::beginTransaction();
            $building_id        = $request->property_id;
            $building_details   = OwnerProperty::where('id', $building_id)->first();


            if ($building_details) {
                for ($i = 1; $i <= $unit_count; $i++) {
                    // dd($unit_count);
                    $property   = new OwnerProperty;
                    $maxid      = OwnerProperty::max('property_reg_no');
                    if ($maxid) {
                        $exid = explode('-', $maxid)[1] + 1;
                        $property->property_reg_no = '#REG' . time() . '-' . $exid;
                    } else {
                        $property->property_reg_no = '#REG' . time() . '-' . '100';
                    }

                    $property->owner_id             = $building_details->owner_id;
                    $property->agent_id = null;
                    $property->builder_id           = $building_details->id;
                    $property->property_name        = isset($request->property_name[$i]) ? $request->property_name[$i] : null;
                    $property->is_builder           = "0";
                    $property->property_to          = $building_details->property_to;
                    $property->category             = $building_details->category;
                    $property->street_address_1     = $building_details->street_address_1;
                    $property->street_address_2     = $building_details->street_address_2 ? $building_details->street_address_2 : null;
                    $property->country              = $building_details->country;
                    $property->city                 = $building_details->city;
                    $property->unit_num             = '0';
                    $property->state                = $building_details->state;
                    $property->zip_code             = $building_details->zip_code;
                    $property->description          = isset($request->description[$i]) ? $request->description[$i] : null;
                    $property->furnished            = isset($request->furnished[$i]) ? $request->furnished[$i] : '0';
                    $property->frequency            = isset($request->frequency[$i]) ? $request->frequency[$i] : null;
                    $property->status               = '1';
                    $property->rent                 = isset($request->min_rent[$i]) ? $request->min_rent[$i] : null;
                    $property->selling_price        = isset($request->min_sellingprice[$i]) ? $request->min_sellingprice[$i] : null;
                    $property->rating               = $building_details->rating;
                    $property->mrp                  = isset($request->max_sellingprice[$i]) ? $request->max_sellingprice[$i] : null;
                    $property->commission           =  $building_details->commission;
                    $property->commission_in        =  $building_details->commission_in;
                    $property->property_management_fee_type = $building_details->property_management_fee_type;
                    $property->expected_amount      = isset($request->expected_amount[$i]) ? $request->expected_amount[$i] : null;
                    $property->type_id              = $building_details->type_id;
                    $property->longitude            = isset($request->longitude[$i]) ? $request->longitude[$i] : $building_details->longitude;
                    $property->latitude             = isset($request->latitude[$i]) ? $request->latitude[$i] : $building_details->latitude;
                    $property->occupied             =  isset($request->vacant[$i]) ? $request->vacant[$i] : 0;
                    $property->security_deposit     = isset($request->security_deposit[$i]) ? $request->security_deposit[$i] : null;
                    $property->token_amount     =       isset($request->token_amount[$i]) ? $request->token_amount[$i] : null;
                    $property->ads_number     =       $building_details->ads_number;
                    $property->guards_number     =      $building_details->guards_number;
                    $property->management_fee         = $building_details->management_fee;
                    $property->ownership_doc         = $building_details->ownership_doc;
                    //contract details
                    $property->contract_no          = $building_details->contract_no;
                    $property->contract_file        = $building_details->contract_file;
                    $property->contract_type        = $building_details->contract_type;
                    $property->contract_start_date  = $building_details->contract_start_date;
                    $property->contract_end_date    = $building_details->contract_end_date;
                    $property->contract_owner   = '1';

                    $property->save();
                    $savedoc = array();
                    $savefloor = array();
                    $savevideo = array();
                    $amenities = array();
                    $details = array();
                    if (isset($request->file('images')[$i])) {
                        foreach ($request->file('images')[$i] as $key => $row) {
                            $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/property/images'), $imagename);
                            $savedoc['property_id']   = $property->id;
                            $savedoc['document']      = '/uploads/property/images/' . $imagename;
                            $savedoc['type']          = '0';
                            $savedoc['owner_id']      = $building_details->owner_id;
                            $savedoc['created_at']    = now();
                            $savedoc['updated_at']    = now();
                            OwnerPropertyDocument::insert($savedoc);
                        }
                    }
                    if (isset($request->file('floor_plans')[$i])) {

                        foreach ($request->file('floor_plans')[$i] as $key => $row) {
                            $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                            $savefloor['property_id'] = $property->id;
                            $savefloor['document']    = '/uploads/property/floor_plans/' . $imagename;
                            $savefloor['type']        = '2';
                            $savefloor['owner_id']    = $building_details->owner_id;
                            $savefloor['created_at']  = now();
                            $savefloor['updated_at']  = now();
                            OwnerPropertyDocument::insert($savefloor);
                        }
                    }
                    if (isset($request->file('videos')[$i])) {
                        foreach ($request->file('videos')[$i] as $key => $row) {
                            $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/property/videos'), $videoname);
                            $savevideo['property_id'] = $property->id;
                            $savevideo['document']    = '/uploads/property/videos/' . $videoname;
                            $savevideo['type']        = '1';
                            $savevideo['owner_id']    = $building_details->owner_id;
                            $savevideo['created_at']  = now();
                            $savevideo['updated_at']  = now();
                            OwnerPropertyDocument::insert($savevideo);
                        }
                    }

                    if (isset($request->amenities[$i])) {

                        foreach ($request->amenities[$i] as $amkey => $amrow) {

                            $category_id = Amenity::where('id', $amrow)->value('amenity_category_id');

                            $amenities['property_id']             = $property->id;
                            $amenities['amenity_id']              = $amrow;
                            $amenities['amenities_category_id']   = $category_id;
                            $amenities['owner_id']                = (int) $building_details->owner_id;
                            $amenities['created_at']              = now();
                            $amenities['updated_at']              = now();
                            OwnerPropertyAmenity::insert($amenities);
                        }
                    }

                    if (isset($request->detailsdata[$i])) {
                        foreach ($request->detailsdata[$i] as $key =>  $detail) {
                            $details['property_id']   = $property->id;

                            $details['detail_id']     = $key;

                            $details['value']         = isset($detail) ? $detail : 0;

                            $details['owner_id']      = $building_details->owner_id;
                            $details['created_at']    = now();
                            $details['updated_at']    = now();
                            OwnerPropertyDetail::insert($details);
                        }
                    }
                }
                DB::commit();
                return redirect()->route('showPropertyManage');
                // return response()->json(['status' => true, 'response' => "Successfully added units"]);
            } else {
                return ['status' => 200, 'response' => 'Invalid Building'];
            }
        } catch (\Exception $e) {
            DB::rollback();
            return ['status' => 400, 'response' => $e->getMessage()];
        }
    }


    public function updatePropertyUnits(Request $request)
    {

        try {
            // DB::beginTransaction();

            $unit_count = $request->unitnum;
            // dd($unit_count);
            for ($i = 1; $i <= $unit_count; $i++) {
                $propertyId = $request->property_id[$i];
                $propertyTo = $request->property_to[$i];

                // print_r($i);
                $property   = OwnerProperty::where('id', $propertyId)->first();
                if ($property->property_reg_no) {
                    $property->property_reg_no = $property->property_reg_no;
                }
                $property->is_builder             = '0';
                $property->owner_id             = $property->owner_id;
                $property->agent_id = null;
                $property->builder_id           = $property->builder_id;

                $property->is_builder           = '0';


                $property->property_to          = $property->property_to;
                $property->category             = $property->category;
                $property->street_address_1     = $property->street_address_1;
                $property->street_address_2     = $property->street_address_2 ? $property->street_address_2 : null;

                $property->country              = $property->country;
                $property->city                 = $property->city;
                $property->state                = $property->state;
                $property->zip_code             = $property->zip_code;
                $property->contract_file        = $property->contract_file;
                $property->contract_no        = $property->contract_no;
                $property->unit_num                = '0';
                $property->contract_start_date    = $property->contract_start_date;
                $property->contract_end_date        = $property->contract_end_date;
                $property->status               = '1';
                $property->rating               = $property->rating;
                $property->commission           =  $property->commission;
                $property->commission_in        =  $property->commission_in;
                $property->property_management_fee_type = $property->property_management_fee_type;
                $property->type_id              = $property->type_id;

                $property->longitude            = isset($request->longitude[$i]) ? $request->longitude[$i] : $property->longitude;
                $property->latitude             = isset($request->latitude[$i]) ? $request->latitude[$i] : $property->latitude;
                $property->ads_number     =       $property->ads_number;
                $property->guards_number     =      $property->guards_number;
                $property->management_fee         = $property->management_fee;
                $property->ownership_doc         = $property->ownership_doc;
                $property->property_name        = isset($request->property_name[$i]) ? $request->property_name[$i]  : null;
                $property->description          = isset($request->description[$i]) ? $request->description[$i]  : null;
                $property->furnished            = isset($request->furnished[$i]) ? $request->furnished[$i]  : '0';
                $property->frequency            = isset($request->frequency[$i]) ? $request->frequency[$i]  : null;
                $property->rent                 = isset($request->min_rent[$i]) ? $request->min_rent[$i]  : null;
                $property->selling_price        = isset($request->min_sellingprice[$i]) ? $request->min_sellingprice[$i]  : null;

                $property->mrp                 = isset($request->max_sellingprice[$i]) ? $request->max_sellingprice[$i]  : null;

                $property->expected_amount      = isset($request->expected_amount[$i]) ? $request->expected_amount[$i]  : null;
                $property->occupied             = isset($request->vacant[$i]) ? $request->vacant[$i] : $property->occupied;
                $property->security_deposit     = isset($request->security_deposit[$i]) ? $request->security_deposit[$i]  : null;

                $property->token_amount         = isset($request->token_amount[$i]) ? $request->token_amount[$i]  : null;
                // dd( $property);
                //contract details
                $property->contract_no          = $property->contract_no;
                $property->contract_file        = $property->contract_file;
                $property->contract_type        = $property->contract_type;
                $property->contract_start_date  = $property->contract_start_date;
                $property->contract_end_date    = $property->contract_end_date;
                $property->contract_owner   = "1";
                $property->save();

                $savedoc = array();
                $savefloor = array();
                $savevideo = array();
                $amenities = array();
                $details = array();

                // // $property_detail = OwnerProperty::where('id', $request->propertyId)->first();
                // // return $property_detail;
                // if ($request->file('building_image')[$i] != null) {
                //     $this->fileUpload($request->file('building_image')[$i], 3, $propertyId, $property->owner_id);
                // } elseif ($request->file('images')[$i] != null) {
                //     // if($property_detail != null){
                //     $this->fileUpload($request->file('images')[$i], 0, $propertyId, $property->owner_id);
                //     // }

                // } elseif ($request->file('floor_plans')[$i] != null) {
                //     // if($property_detail != null){
                //     $this->fileUpload($request->file('floor_plans')[$i], 2, $propertyId, $property->owner_id);
                //     // }

                // } elseif ($request->file('videos')[$i] != null) {
                //     //  if($property_detail != null){
                //     $this->fileUpload($request->file('videos')[$i], 1, $propertyId, $property->owner_id);
                //     // }

                // }

                if (isset($request->file('images')[$i])) {
                    foreach ($request->file('images')[$i] as $key => $row) {
                        $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/images'), $imagename);
                        $savedoc['property_id']   = $property->id;
                        $savedoc['document']      = '/uploads/property/images/' . $imagename;
                        $savedoc['type']          = '0';
                        $savedoc['owner_id']      =  $property->owner_id;
                        $savedoc['created_at']    = now();
                        $savedoc['updated_at']    = now();
                        OwnerPropertyDocument::insert($savedoc);
                    }
                }
                if (isset($request->file('floor_plans')[$i])) {

                    foreach ($request->file('floor_plans')[$i] as $key => $row) {
                        $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                        $savefloor['property_id'] = $property->id;
                        $savefloor['document']    = '/uploads/property/floor_plans/' . $imagename;
                        $savefloor['type']        = '2';
                        $savefloor['owner_id']    =  $property->owner_id;
                        $savefloor['created_at']  = now();
                        $savefloor['updated_at']  = now();
                        OwnerPropertyDocument::insert($savefloor);
                    }
                }
                if (isset($request->file('videos')[$i])) {
                    foreach ($request->file('videos')[$i] as $key => $row) {
                        $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/videos'), $videoname);
                        $savevideo['property_id'] = $property->id;
                        $savevideo['document']    = '/uploads/property/videos/' . $videoname;
                        $savevideo['type']        = '1';
                        $savevideo['owner_id']    =  $property->owner_id;
                        $savevideo['created_at']  = now();
                        $savevideo['updated_at']  = now();
                        OwnerPropertyDocument::insert($savevideo);
                    }
                }



                if (isset($request->amenities[$i])) {
                    DB::table('owner_property_amenities')->where('property_id', $propertyId)->delete();
                    foreach ($request->amenities[$i] as $amkey => $amrow) {

                        $category_id = Amenity::where('id', $amrow)->value('amenity_category_id');

                        $amenities['property_id']             = $property->id;
                        $amenities['amenity_id']              = $amrow;
                        $amenities['amenities_category_id']   = $category_id;
                        $amenities['owner_id']                = (int) $property->owner_id;
                        $amenities['created_at']              = now();
                        $amenities['updated_at']              = now();
                        OwnerPropertyAmenity::insert($amenities);
                    }
                }




                $detailspropDetails = OwnerPropertyDetail::where('property_id', $propertyId)->get();
                foreach ($detailspropDetails as $detailsprop) {
                    if (isset($request->details_prop_data[$i])) {
                        foreach ($request->details_prop_data[$i] as $key =>  $detail) {
                            $details = $detailsprop->find($key);
                            $details['property_id']   = $details->property_id;
                            $details['detail_id']     = $details->detail_id;
                            $details['value']         = $detail;
                            $details['owner_id']      = $property->owner_id;
                            $details['created_at']    = now();
                            $details['updated_at']    = now();
                            $details->save();
                        }
                    }
                }
                // print_r($propertyId);
            }
            // DB::commit();
            // return redirect()-route('admin.property.property');
            return redirect()->route('showPropertyManage');
            // } else {
            //     return ['status' => 200, 'response' => 'Invalid Building'];
            // }
        } catch (\Exception $e) {
            // DB::rollback();
            return redirect()->route('showPropertyManage');
            // return ['status' => 400, 'response' => $e->getMessage()];
        }
    }


    public function editProperty(Request $request)
    {

        $id = $request->id;
        $data['types']     = PropertyType::orderBy('id', 'DESC')->get();
        if (count($data['types']) > 0) {
            $data['amenities_listing'] = $this->availableAmenities($data['types'][0]->id);
            $data['details'] = $this->availableDetails($data['types'][0]->id);
        } else {
            $data['amenities_listing'] = array();
            $data['amenities_listing'] = array();
        }
        // dd($data['amenities_listing']);
        $data['property'] = OwnerProperty::with('type_details', 'country_rel', 'state_rel', 'zipcode_rel', 'city_rel', 'amenity_details', 'owner_rel')
            ->where('id', $id)
            ->first();
        $data['details_prop'] = DB::table('owner_property_details')
            ->select('owner_property_details.id', 'details.name', 'owner_property_details.value', 'details.placeholder')
            ->join('details', 'details.id', 'owner_property_details.detail_id')
            ->where('owner_property_details.property_id', $id)
            ->get();
        $data['property_amenities'] =  OwnerPropertyAmenity::where('property_id', $id)->get();
        $data['owners'] = Owner::all();
        $data['countries'] = Country::all();
        $data['states'] = State::all();
        $data['cities'] = City::all();
        $data['zipcodes'] = Pincode::all();
        $data['frequency'] = Frequency::all();
        $data['property_owner'] = Owner::where('id', $data['property']['owner_id'])->first();
        $data['property_country'] = Country::where('id', $data['property']['country'])->first();

        $data['property_state'] = State::where('id', $data['property']['state'])->first();
        $data['property_city'] = City::where('id', $data['property']['city'])->first();
        $data['property_zipcode'] = Pincode::where('id', $data['property']['zip_code'])->first();
        $data['property_frequency'] = Frequency::where('id', $data['property']['frequency'])->first();
        $data['property_image'] = OwnerPropertyDocument::where('property_id', $data['property']['id'])->get();

        return view('admin.property.edit', $data);
    }
    public function availableAmenities($type)
    {
        $amenities_listing = [];
        $base_url          = url('/');
        $amenities_listing = AmenitiesCategory::join('property_type_amenity_contents', 'amenities_categories.id', '=', 'property_type_amenity_contents.amenity_category_id')
            ->where('property_type_amenity_contents.type_id', $type)
            ->select('amenities_categories.id', 'amenities_categories.name')
            ->orderBy('sort_order', 'ASC')
            ->distinct()
            ->get();
        foreach ($amenities_listing as $row) {
            $data = Amenity::join('property_type_amenity_contents', 'amenities.id', '=', 'property_type_amenity_contents.amenity_id')
                ->where('property_type_amenity_contents.type_id', $type)
                ->where('amenities.amenity_category_id', $row->id)
                ->select('amenities.id', 'amenities.name', DB::raw('CONCAT("' . $base_url . '",image) AS image'))
                ->get();
            $row->amenities = $data;
        }
        return $amenities_listing;
    }
    public function availableDetails($type)
    {
        $details = [];
        $details = PropertyTypeDetailsContent::join('details', 'details.id', '=', 'property_type_details_contents.detail_id')
            ->where('property_type_details_contents.type_id', $type)
            ->select('detail_id', 'details.name', 'details.sort_order', 'details.placeholder')
            ->orderBy('details.sort_order', 'ASC')
            ->get();

        return $details;
    }

    // geting agents to temaplate
    public function getAgents(Request $request)
    {
        $agents = Agent::all();
        return view('property_management.list', compact('agents'));
    }
    public function editOwnerProperty(Request $request)
    {
        //  dd($request->all());

        if (!$request->ownerslist) {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
        $rules = [
            'property_name' => 'required',
            'property_to' => 'required',
            'category' => 'required',
            'property_country' => 'required',
            'property_state' => 'required',
            'property_city' => 'required',
            'property_zipcode' => 'required',
            'building_image' => 'array',
            'building_image.*' => 'max:3000|mimes:jpeg,jpg,png',
            'images' => 'array',
            'images.*' => 'max:3000|mimes:jpeg,jpg,png',
            'floor_plans' => 'nullable|array',
            'floor_plans.*' => 'max:3000|mimes:jpeg,jpg,png',
            'amenities' => 'array',
            'detailsdata' => 'array|min:1',
            'selling_price'   => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'mrp'             => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'rent'            => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'security_deposit' => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'token_amount'    => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
        ];
        $messages = [
            'property_name.required' => 'Property Name to is required',
            'property_to.required' => 'property to is required',
            'property_country.required' => 'country is required',
            'property_state.required' => 'state is required',
            'property_city.required' => 'city is required',
            'property_zipcode.required' => 'zipcode is required',
            'images.required' => 'images are required',
            'furnished.required' => 'Furnish is required',
            'detailsdata.required' => 'Details fields are required',
            'token_amount.required' => 'Token amount is required',
        ];
        if (!$request->individual_building) {
            $detailsdata = "";
        } else {
            $detailsdata = $request->details_prop_data;
        }
        //   dd($request->details_prop_data);
        if ($detailsdata) {
            foreach ($detailsdata as $key => $value) {
                $messages['detailsdata' . $key] = 'Details ' . $key . ' field is required';
            }
        }

        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        $furnished  = $request->furnished;
        if (!in_array($furnished, array(0, 1, 2))) {
            return response()->json(['status' => false, 'response' => "Please enter a valid furnish type"]);
        }
        $exid  = 000;
        if ($request->propertyId) {

            $property = OwnerProperty::where('id', $request->propertyId)->first();
            if ($property->property_reg_no) {
                $property->property_reg_no = $property->property_reg_no;
            }
        } else {
            $property = new OwnerProperty;

            $maxid = OwnerProperty::max('property_reg_no');
            if ($maxid) {
                $exid = explode('-', $maxid)[1] + 1;
                $property->property_reg_no = '#REG' . time() . '-' . $exid;
            } else {
                $property->property_reg_no = '#REG' . time() . '-' . '100';
            }
        }


        $type                           = $request->type;
        $property_to                    = $request->property_to;
        $property_name                  = $request->property_name;
        $unit_num                       = $request->unit_num;
        $floor_num                      = $request->floor_num;
        $contract_no                    = $request->contract_no;
        $contract_type                  = $request->contract_type;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
        $commission_in                  = $request->commision_as;
        $commission                     = $request->owner_amount;
        $property_management_fee_type   = $request->property_management_fee_type;
        $management_fee                 = $request->management_fee;
        $adsnumber                      = $request->adsnumber;
        // dd($adsnumber);
        $guardnumber                    = $request->guard_number;

        $property->owner_id             = $request->ownerslist;
        // $property->owner_id             = isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
        $property->property_name        = $property_name;
        $property->property_to          = $property_to;
        $property->unit_num             = $unit_num;
        $property->floor_num            = $floor_num;


        $property->type_id              = $type;


        $property->contract_no          = $contract_no ? $contract_no : null;
        $property->contract_type        = $contract_type ? $contract_type : null;
        $property->contract_start_date  = $contract_start_date ? $contract_start_date : null;
        $property->contract_end_date    = $contract_end_date ? $contract_end_date : null;
        $property->commission_in        = $commission_in ? $commission_in : null;
        $property->commission           = $commission ? $commission : null;
        $property->property_management_fee_type = $property_management_fee_type ? $property_management_fee_type : null;
        $property->management_fee       = $management_fee ? $management_fee : null;
        $property->ads_number = $adsnumber ? $adsnumber : null;
        $property->guards_number = $guardnumber ? $guardnumber : null;


        if ($request->category == 1) {
            $property->category             = 0;
        } else {
            $property->category             = 1;
        }

        $property->street_address_1     = $request->address1;
        $property->street_address_2     = $request->address2 ? $request->address2 : "";
        $property->description          = $request->description;
        $property->country              = $request->property_country;
        $property->city                 = $request->property_city;
        $property->state                = $request->property_state;
        $property->zip_code             = $request->property_zipcode;
        $property->occupied             =  $request->property_status;
        $property->status               = '1';
        $property->furnished            = $request->furnished ? $request->furnished : '0';


        $property->latitude             = $request->lat_prop ? $request->lat_prop : 9.93123300;
        $property->longitude            = $request->lng_prop ? $request->lng_prop : 76.267303;

        // dd($request->individual_building);
        if ($request->individual_building  == 1) {

            $frequency                      = $request->frequency;
            $selling_price                  = $request->min_sellingprice;
            $mrp                            = $request->max_sellingprice;

            $property->occupied             =  $request->property_status;
            $property->furnished        = $request->furnished ? $request->furnished : '0';

            $property->selling_price        = $selling_price ? $selling_price : null;
            $property->is_builder           = '0';
            $property->mrp                  = $mrp ? $mrp : null;
            $property->frequency            = $frequency ? $frequency : null;
            $property->rent                 = $request->min_rent ? $request->min_rent : null;
            $property->token_amount         = $request->token_amount ? $request->token_amount : null;
            $property->security_deposit     = $request->security_deposit ? $request->security_deposit : null;
        } else {
            $property->is_builder           = 1;
        }

        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $property->contract_file        = $file_name;
        }else{
            $property->contract_file=$property->contract_file;
        }


        if ($request->file('ownership_doc')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('ownership_doc')) {
                $path = public_path('uploads/property/ownership_doc/');
                $file          = $request->file('ownership_doc');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/ownership_doc/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $property->ownership_doc        = $file_name;
        }
        else{
            $property->ownership_doc=$property->ownership_doc;
        }

        // try {
        //     DB::beginTransaction();
        $property->save();
        $savedoc = array();
        $savefloor = array();
        $savevideo = array();
        $amenities = array();
        $building_image = array();
        // file updation
        // $property_detail = OwnerProperty::where('id', $request->propertyId)->first();

        // if ($request->file('building_image') != null) {

        //     $this->fileUpload($request->file('building_image'), 3, $request->propertyId,  $property_detail->owner_id);
        // } elseif ($request->file('images') != null) {

        //     if ($property_detail != null) {

        //         $this->fileUpload($request->file('images'), 0, $request->propertyId,  $property_detail->owner_id);
        //     }
        // } elseif ($request->file('floor_plans') != null) {
        //     if ($property_detail != null) {
        //         $this->fileUpload($request->file('floor_plans'), 2, $request->propertyId,  $property_detail->owner_id);
        //     }
        // } elseif ($request->file('videos') != null) {
        //     if ($property_detail != null) {
        //         $this->fileUpload($request->file('videos'), 1, $request->propertyId,  $property_detail->owner_id);
        //     }
        // }
        if ($request->file('building_image')) {
            foreach ($request->file('building_image') as $key => $row) {
                $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                $row->move(public_path('/uploads/property/images'), $imagename);
                $savedoc['property_id']   = $property->id;
                $savedoc['document']      = '/uploads/property/images/' . $imagename;
                $savedoc['type']          = '3';
                $savedoc['owner_id']      = $property->owner_id;
                $savedoc['created_at']    = now();
                $savedoc['updated_at']    = now();
                OwnerPropertyDocument::insert($savedoc);
            }
        }
        if ($request->file('images')) {
            foreach ($request->file('images') as $key => $row) {
                $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                $row->move(public_path('/uploads/property/images'), $imagename);
                $savedoc['property_id']   = $property->id;
                $savedoc['document']      = '/uploads/property/images/' . $imagename;
                $savedoc['type']          = '0';
                $savedoc['owner_id']      = $property->owner_id;
                $savedoc['created_at']    = now();
                $savedoc['updated_at']    = now();
                OwnerPropertyDocument::insert($savedoc);
            }
        }
        if ($request->file('floor_plans')) {

            foreach ($request->file('floor_plans') as $key => $row) {
                $imagename =  time() . rand() . '.' . $row->getClientOriginalExtension();
                $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                $savefloor['property_id'] = $property->id;
                $savefloor['document']    = '/uploads/property/floor_plans/' . $imagename;
                $savefloor['type']        = '2';
                $savefloor['owner_id']    = $property->owner_id;
                $savefloor['created_at']  = now();
                $savefloor['updated_at']  = now();
                OwnerPropertyDocument::insert($savefloor);
            }
        }
        if ($request->file('videos')) {
            foreach ($request->file('videos') as $key => $row) {
                $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                $row->move(public_path('/uploads/property/videos'), $videoname);
                $savevideo['property_id'] = $property->id;
                $savevideo['document']    = '/uploads/property/videos/' . $videoname;
                $savevideo['type']        = '1';
                $savevideo['owner_id']    = $property->owner_id;
                $savevideo['created_at']  = now();
                $savevideo['updated_at']  = now();
                OwnerPropertyDocument::insert($savevideo);
            }
        }






        DB::table('owner_property_amenities')->where('property_id', $request->propertyId)->delete();
        if ($request->amenities) {
            foreach ($request->amenities as $key => $row) {
                $category_id = Amenity::where('id', $row)->pluck('amenity_category_id');
                $amenities = new OwnerPropertyAmenity();
                $amenities['property_id'] = $request->propertyId;
                $amenities['amenity_id']  = $row;
                $amenities['amenities_category_id'] = $category_id[0];
                $amenities['owner_id']   = $request->ownerslist;
                $amenities['created_at'] = now();
                $amenities['updated_at'] = now();
                $amenities->save();
            }
        }

        $detailspropDetails = OwnerPropertyDetail::where('property_id', $request->propertyId)->get();
        foreach ($detailspropDetails as $detailsprop) {
            if ($detailsdata) {
                foreach ($detailsdata as $key =>  $detail) {
                    $details = $detailsprop->find($key);
                    $details['property_id']   = $request->propertyId;
                    $details['detail_id']     = $details->detail_id;
                    $details['value']         = $detail;
                    $details['owner_id']      = $request->ownerslist;
                    $details['created_at']    = now();
                    $details['updated_at']    = now();
                    $details->save();
                }
            }
        }

        // DB::commit();
        $return_data = [
            'property_id' => $property->id,
        ];

        return response()->json(['status' => true, 'response' => 'Successfully updated property', 'response_data' => $return_data]);
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        // }
    }
    public function deletegalleryimage(Request $request, $pid)
    {

        //   dd("dfudsgf");
        $rules = [];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            try {
                $result = null;


                DB::transaction(function () use ($request, $pid, &$result) {
                    // dd($pid);
                    OwnerPropertyDocument::find($pid)->delete();
                });

                return response()->json(['success' => 1]);
            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['databaseerror' => $e->errorInfo]);
            }
        } else {

            return response()->json(['errors' => $validator->errors()]);
        }
    }

    public function checkContractno(Request $request)
    {
        $input = $request->only(['contract_no']);

        $request_data = [
            'contract_no' => 'required|unique:owner_properties,contract_no',
        ];

        $validator = Validator::make($input, $request_data);
        // json is null
        if ($validator->fails()) {
            $errors = json_decode(json_encode($validator->errors()), 1);
            return response()->json([
                'success' => false,
                'message' => array_reduce($errors, 'array_merge', array()),
            ]);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'The Contract No is available'
            ]);
        }
    }


    // public function fileUpload($file, $fileType, $propertyId, $ownerId)
    // {
    //     if (count($file) > 0) {


    //         // OwnerPropertyDocument::where('property_id', $propertyId)
    //         //     ->where('type', $fileType)->delete();
    //         foreach ($file as $key => $row) {
    //             $propertyImg = [];

    //             $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
    //             if ($fileType == 0 || $fileType == 3) {
    //                 // dd($fileType);
    //                 $row->move(public_path('/uploads/property/images'), $imagename);
    //                 $propertyImg['document']      = '/uploads/property/images/' . $imagename;
    //             } elseif ($fileType == 1) {
    //                 $row->move(public_path('/uploads/property/videos'), $imagename);
    //                 $propertyImg['document'] = '/uploads/property/videos/' . $imagename;
    //             } elseif ($fileType == 2) {
    //                 // dd("uyt");
    //                 $row->move(public_path('/uploads/property/floor_plans'), $imagename);

    //                 $propertyImg['document'] = '/uploads/property/floor_plans/' . $imagename;
    //             }
    //             $propertyImg['type']          = strval($fileType);
    //             $propertyImg['property_id']   = $propertyId;

    //             $propertyImg['owner_id']      = $ownerId;
    //             $propertyImg['created_at']    = now();
    //             $propertyImg['updated_at']    = now();
    //             OwnerPropertyDocument::insert($propertyImg);
    //         }
    //     }
    //     return true;
    // }
}
