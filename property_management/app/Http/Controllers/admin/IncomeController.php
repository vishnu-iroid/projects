<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserPropertyServiceRequests;
use App\Models\OwnerPropertyServiceRequests;
use App\Models\UserProperty;
use Validator;
use App\Models\AdminAccount;
use App\Models\AgentTrackingUserTour;
use App\Models\AccountRefreal;
use App\Models\Admin;
use App\Models\OwnerProperty;
use App\Models\UserPropertyBookings;
use App\Models\UserPropertyServiceRequestDocuments;
use App\Models\OwnerPropertyServiceRequestDocuments;
use App\Models\Notifications;
use App\Models\User;
use App\Models\UserPropertyRentDocuments;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Pagination\Paginator;

class IncomeController extends Controller
{
    //

    public function showIncome()
    {
        return view('admin.accounts.income');
    }



    public function showUserServiceList(Request $request)
    {

        $details = AdminAccount::where('payment_type', '1')->with('user_rel:id,name', 'prop_rel:id,property_name')->get();


        $data = [
            'details' => $details
        ];

        return view('admin.accounts.user_service_list', $data);
    }

    public function showOwnerServiceList(Request $request)
    {

        $details = AdminAccount::where('payment_type', '6')->with('owner_rel:id,name', 'prop_rel:id,property_name')->get();

        $data = [
            'details' => $details
        ];

        return view('admin.accounts.owner_service_list', $data);
    }
    public function showOwnerPaymentList(Request $request)
    {

        $details = AdminAccount::where('payment_type', '7')->with('owner_rel:id,name', 'prop_rel:id,property_name')->get();

        $data = [
            'details' => $details
        ];

        return view('admin.accounts.owner_payment_list', $data);
    }

    public function showTokenList(Request $request)
    {

        $details = AdminAccount::where('payment_type', '2')->with('user_rel:id,name', 'prop_rel:id,property_name')->get();


        $data = [
            'details' => $details
        ];
        return view('admin.accounts.user_token_list', $data);
    }

    public function showUserRental(Request $request)
    {

        $details = AdminAccount::where('payment_type', '5')
                                ->with('user_rel:id,name', 'prop_rel:id,property_name')->paginate(10);

        $data = [
            'details' => $details
        ];

        return view('admin.accounts.user_rental_list', $data);
    }

    public function showUserMaintenanceList(Request $request)
    {
        $details = AdminAccount::where('payment_type', '8')->with('user_rel:id,name', 'prop_rel:id,property_name')->get();

        $data = [
            'details' => $details
        ];

        return view('admin.accounts.user_maintain_list', $data);
    }

    public function showUserSecurityAmountList(Request $request)
    {
        $details = AdminAccount::where('payment_type', '3')->with('user_rel:id,name', 'prop_rel:id,property_name')->get();

        $data = [
            'details' => $details
        ];

        return view('admin.accounts.user_security_amount_list', $data);
    }



    public function showPaidService(Request $request)
    {

        $status = $request->status;
        if ($status == "") {
            $status = 0;
        }

        if ($request->status == 0) {
            // dd("dfgdfg");    
            $services_request = UserPropertyServiceRequestDocuments::select(
                'user_property_service_requests.id',
                'user_property_service_requests.service_id',
                'user_property_service_requests.date',
                'user_property_service_requests.time',
                'user_property_service_requests.status',
                'owner_properties.property_name',
                'user_property_service_requests.send_owner_approval',
                'user_property_service_requests.description',
                'user_property_service_request_documents.document',
                'user_property_service_request_documents.owner_payment_checked',
                'user_property_service_request_documents.id as doc_id'
                
            )
               ->with('service_related','user_related')
                // ->whereNotIn('user_property_service_requests.id', function ($q) {
                //     $q->select('refreal_id')->where('type', 1)->from('account_refreals');
                // })
                ->join('user_property_service_requests', 'user_property_service_request_documents.request_id', 'user_property_service_requests.id')
                ->where('user_property_service_request_documents.document_type',1)
                // ->where('user_property_service_request_documents.is_verified','=',NULL)
                ->orWhereNull('user_property_service_request_documents.is_verified')
                ->join('user_properties', 'user_properties.id', 'user_property_id')
                ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
                ->orderBy('user_property_service_requests.date', 'DESC')
                ->groupBy('user_property_service_requests.id')
                ->paginate(15);
                // dd( $services_request);

        } elseif ($request->status == 1) {
            $services_request = OwnerPropertyServiceRequestDocuments::select(
                'owner_property_service_requests.id as o_id',
                'service_id',
                'owner_property_service_requests.owner_property_id',
                'date',
                'time',
                'owner_property_service_requests.status',
                'owner_property_service_requests.description',
                'owner_properties.property_name'
               
            )
                // ->whereNotIn('owner_property_service_requests.id', function ($q) {
                //     $q->select('refreal_id')->where('type', 6)->from('account_refreals');
                // })
                ->with('service_related:id,service', 'owner_doc:id,document,request_id')
                ->join('owner_property_service_requests', 'owner_property_service_request_documents.request_id', 'owner_property_service_requests.id')
                ->where('owner_property_service_request_documents.document_type',1)
                ->orWhereNull('owner_property_service_request_documents.is_verified')
                ->join('owner_properties', 'owner_properties.id', 'owner_property_service_requests.owner_property_id')
                // ->join('user_properties', 'owner_properties.id', 'user_properties.property_id')
                ->orderBy('owner_property_service_requests.date', 'DESC')
                ->groupBy('owner_property_service_requests.id')
                ->paginate(15);
        }
// dd($services_request);
        $data = [
            'request'  => $services_request,
            'status'   =>  $status
        ];

        return view('admin.accounts.services', $data);
    }

    public function ownerServiceDetails($id)
    {
        $services_request = OwnerPropertyServiceRequestDocuments::select(
            'owner_property_service_requests.id',
            'service_id',
            'date',
            'time',
            'owner_property_service_requests.status',
            'owner_property_service_request_documents.document',
            'owner_property_service_requests.description',
            'owner_properties.property_name',
            'owner_properties.street_address_1',
            'owner_property_service_requests.*',
            'owner_properties.property_reg_no',
            'owner_properties.id as property_id'
        )
            ->where('owner_property_service_requests.id', $id)
            ->with('service_related:id,service', 'owner_related:id,name,address', 'owner_doc:id,document,request_id')
            ->join('owner_property_service_requests', 'owner_property_service_request_documents.request_id', 'owner_property_service_requests.id')
            ->where('owner_property_service_request_documents.document_type',1)
            ->whereNotNull('owner_property_service_request_documents.is_verified')
            ->join('owner_properties', 'owner_properties.id', 'owner_property_service_requests.owner_property_id')
            ->orderBy('owner_property_service_requests.date', 'DESC')
            ->groupBy('owner_property_service_requests.id')
            ->first();

        // dd($services_request);

        $data = [
            'request' => $services_request
        ];

        return view('admin.accounts.owner_service_details', $data);
    }
    public function userServiceDetails($id)
    {
      

        $services_request = UserPropertyServiceRequests::
        select(
            'user_property_service_requests.id as request_id',
            'service_id',
            'date',
            'time',
            'user_properties.id as u_p_id',
            'user_property_service_request_documents.*',
            'user_property_service_requests.user_id',
            'owner_properties.property_reg_no',
            'owner_properties.street_address_1',
            'owner_properties.id as property_id',
            'owner_properties.property_name as propertyName',
            'user_property_service_requests.send_owner_approval',
            'user_property_service_requests.description',
            'user_property_service_requests.status as servicestatus',
            'user_property_service_request_documents.document',
            'user_property_service_request_documents.is_verified',
            'user_property_service_request_documents.id as doc_id'

        )
            ->where('user_property_service_requests.id', $id)
            ->join('user_properties', 'user_properties.id', 'user_property_service_requests.user_property_id')
            ->join('owner_properties', 'user_properties.property_id', 'owner_properties.id')
            ->join('user_property_service_request_documents', 'user_property_service_request_documents.request_id', 'user_property_service_requests.id')
            ->where('user_property_service_request_documents.document_type',1)
            // ->orWhereNull('user_property_service_request_documents.is_verified')
            ->with('service_related:id,service','user_related:id,name,address','service_estimate_doc:id,document')
            // ->join('user_properties', 'user_properties.id', 'user_property_service_requests.user_property_id')
            // ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
            ->groupBy('user_property_service_requests.id')
            ->first();

        // dd(json_encode($services_request));
       //  return $services_request;
        $data = [
            'request' => $services_request
        ];

        return view('admin.accounts.user_service_details', $data);
    }

    public function servicePayment(Request $request)
    {

        $rule = [
            'service_amount' => 'required'
        ];

        $message = [

            'service_amount' => 'Amount is Required'
        ];

        $validator = Validator::make($request->all(), $rule, $message);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }

        if ($request->service_amount) {
            $payment = new AdminAccount();
            $payment->amount_type = 1;
            $payment->amount = $request->service_amount;
            $payment->payment_type = 1;
            $payment->user_id = $request->user;
            $payment->property_id = $request->property_id;
            $payment->user_property_id  = $request->user_property_id;
            $payment->date = $request->date;
            $max_id = DB::table('admin_accounts')->max('id');
            $payment->reference_id = $request->doc_id;
            $payment->save();

            $refreal = new AccountRefreal();
            $refreal->refreal_id = $request->refreal_id;
            $refreal->type = 1;
            $refreal->save();
            // echo $request->doc_id;die;
            UserPropertyServiceRequestDocuments::where('id',$request->doc_id)->where('document_type',1)->update(['is_verified'=> 1]);
            
            UserPropertyServiceRequests::where('id',$request->refreal_id)->update(['status'=>2]);


            $msg = "Payment Successfully";
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went Wrong']);
        }
    }

    public function ownerServicePayment(Request $request)
    {
        $rule = [
            'service_amount' => 'required'
        ];

        $message = [

            'service_amount' => 'Amount is Required'
        ];

        $validator = Validator::make($request->all(), $rule, $message);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }

        if ($request->service_amount) {
            $payment = new AdminAccount();
            $payment->amount_type = 1;
            $payment->amount = $request->service_amount;
            $payment->payment_type = 6;
            $payment->date = $request->date;
            $payment->owner_id = $request->owner;
            $payment->property_id = $request->property_id;
            $max_id = DB::table('admin_accounts')->max('id');
            $payment->reference_id = $request->refreal_id;
            $payment->save();

            $refreal = new AccountRefreal();
            $refreal->refreal_id = $request->refreal_id;
            $refreal->type = 6;
            $refreal->save();

            OwnerPropertyServiceRequestDocuments::where('request_id',$request->refreal_id)->where('document_type',1)->update(['is_verified'=>1]);
            OwnerPropertyServiceRequests::where('id',$request->refreal_id)->update(['status'=>2]);

            $msg = "Payment Successfully";
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went Wrong']);
        }
    }


    public function showUserRent(Request $request)
    {

        // $status = $request->status;
        // if ($status == "") {
        //     $status = 0;
        // }

        // if ($status == 0) {
            $details = UserProperty::select(
                'user_properties.id',
                'user_properties.property_id',
                'user_properties.user_id',
                'user_properties.status as prop_status',
                'owner_properties.frequency',
                'user_properties.rent',
                'user_properties.created_at',
                'user_property_rent_documents.id as refrence_id',
                'user_property_rent_documents.document',

            )
                // ->whereNotIn('user_properties.id', function ($q) {
                //     $q->select('refreal_id')->where('type', 5)->from('account_refreals');
                // })
                ->where('user_property_rent_documents.payment_check',0)
                ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
                ->join('user_property_rent_documents', 'user_property_rent_documents.user_property_id', 'user_properties.id',)
                ->with('user_rel:id,name', 'prop_rel:id,property_name','rent_rel')
                ->paginate(10);
        // }

        $data = [
            'data' => $details,
            // 'status' => $status
        ];

        return view('admin.accounts.rental_details', $data);
    }
    public function showUserCommission(Request $request)
    {

        // $status = $request->status;
        //  if ($status == '1') {

            $details = OwnerProperty::select(
                'owner_properties.id',
                'owner_properties.owner_id',
                'owner_properties.property_name',
                'owner_properties.status',
                'owner_properties.expected_amount',
                'owner_properties.*'
            )
                ->whereNotIn('owner_properties.id', function ($q) {
                    $q->select('refreal_id')->where('type', 7)->from('account_refreals');
                })
                ->with('owner_rel:id,name','property_priority_image')->paginate(15);
        // }

        $data = [
            'details' => $details,
            // 'status' => $status
        ];

        return view('admin.accounts.commission_details', $data);
    }


    public function payRental(Request $request)
    {

        //
        if ($request->userbookingdata) {
            $id = $request->tableId;
            $date = $request->userbookingdata;

            $prop_details = UserProperty::find($id);
            //dd($prop_details);
            $rent = $prop_details->rent;


            if ($request->sercurity_amount) {

                $Sercurity_amount = new AdminAccount();
                $Sercurity_amount->amount_type = 1;
                $Sercurity_amount->amount = $request->sercurity_amount;
                //rent
                $Sercurity_amount->payment_type = 3;
                $Sercurity_amount->date = $date;
                $Sercurity_amount->reference_id = $request->refrence_id;
                $Sercurity_amount->user_id = $prop_details->user_id;
                $Sercurity_amount->property_id = $prop_details->property_id;
                $Sercurity_amount->user_property_id = $id ;
                $Sercurity_amount->save();
            }
            if ($request->maintenance_amount) {
                $maintainPayment = new AdminAccount();
                $maintainPayment->amount_type = 1;
                $maintainPayment->amount = $request->maintenance_amount;
                $maintainPayment->payment_type = 8;
                $maintainPayment->date = $date;
                $maintainPayment->reference_id = $request->refrence_id;
                $maintainPayment->user_id = $prop_details->user_id;
                $maintainPayment->property_id = $prop_details->property_id;
                $maintainPayment->user_property_id = $id ;
                $maintainPayment->save();
            }

            $payment = new AdminAccount();
            $payment->amount_type = 1;
            if ($request->sercurity_amount) {
                $sercurity_amount = $request->sercurity_amount;

                $payment->amount = $rent -  $sercurity_amount;
            } else {

                $payment->amount = $rent;
            }

            $payment->payment_type = 5;
            $payment->date = $date;
            $payment->user_id = $prop_details->user_id;
            $payment->property_id = $prop_details->property_id;
            $payment->user_property_id = $id ;
            $payment->reference_id = $request->refrence_id;
            $payment->save();

            if($request->refrence_id)
            {
                UserPropertyRentDocuments::where('id',$request->refrence_id)->update(['payment_check'=>1]);
            }

            $refreal = new AccountRefreal();
            $refreal->refreal_id = $id;
            $refreal->type = 5;
            $refreal->save();

            $msg = "Payment Successfully";
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went Wrong']);
        }
    }

    public function payOwnerCommission(Request $request)
    {
        if ($request->propertyId) {

            $id = $request->propertyId;
            $date = $request->propertybookingdata;
            $prop_details = OwnerProperty::find($id);
            $payment = new AdminAccount();
            $payment->amount_type = 1;
            $payment->amount = $request->amount;
            $payment->payment_type = 7;
            $payment->date = $date;
            $payment->owner_id = $prop_details->owner_id;
            $payment->property_id = $prop_details->id;
            $max_id = DB::table('admin_accounts')->max('id');
            $payment->reference_id = $max_id + 1;
            $payment->save();
            $refreal = new AccountRefreal();
            $refreal->refreal_id = $id;
            $refreal->type = 7;
            $refreal->save();
            $msg = "Payment Successfully";
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went Wrong']);
        }
    }

    public function showPropertyToken(Request $request)
    {
        $details = UserPropertyBookings::whereNotIn('user_property_bookings.id', function ($q) {
            $q->select('refreal_id')->where('type', 2)->from('account_refreals');
        })
            ->with(
                'property_relation.builders',
                'user_details:id,name,phone',
                'booking_doc:id,booking_id,document',
                'account_rel'
            )
            ->where('is_verified', 0)
            ->where('cancel_status', 0)
            ->paginate(10);
// dd($details);
            $property=OwnerProperty::get();
            $owners=Admin::get();
            $agents=User::get();

        $data = [
            'details' => $details,
            'property'=>$property,
            'owners'=>$owners,
            'agents'=>$agents
        ];

        // if ($request->ajax()) {
            
        //     return DataTables::of($data['details'])
        //         ->addIndexColumn()
                
        //         ->make(true);
        // }

        return view('admin.accounts.booking_token_details', $data);
    }

    public function payToken(Request $request)
    {

        if ($request->amount) {
            $id = $request->tableId;
            $date = $request->data;

            $prop_details = UserPropertyBookings::find($id);

            $payment = new AdminAccount();
            $payment->amount_type = 1;
            $payment->amount = $request->amount;
            $payment->property_id = $prop_details->property_id;
            $payment->payment_type = 2;
            $payment->date = $date;
            //$payment->user_property_id =
            $payment->user_id = $prop_details->user_id;
            $payment->reference_id = $request->refrenceId;
            $payment->save();

             UserPropertyBookings::where('id',$id)->update(['is_verified' => 1]);

            $check = UserPropertyBookings::find($id);
            if ($check->assignedagent != '') {

                 AgentTrackingUserTour::where('booking_id', $id)->update(['status' => '3']);
            }

            $refreal = new AccountRefreal();
            $refreal->refreal_id = $id;
            $refreal->type = 2;
            $refreal->save();
            $msg = "Payment Successfully";
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went Wrong']);
        }
    }


    public function viewPaymentDetails(Request $request,$id)
    {

        $user   = UserPropertyBookings::select('id as b_id', 'booking_id', 'user_id','package_id', 'property_id', 'check_in', 'check_out', 'coupoun', 'is_verified')

            ->with(
                'property_details:id,property_reg_no,rent,selling_price,occupied,property_name,property_to,category',
                'user_details:id,name',
                'booking_doc:id,booking_id,document',
                'offer_rel:id,discount_amount',
                'account_rel',
                'coupon_rel',
                'agent_details:id,name',
            )
            ->where('id', $id)
            ->first();

        $data = [
            'user' => $user,
            // 'details'=>$details,
        ];
        return view('admin.accounts.paymentViewDetail',$data);
    }

    public function viewRevenuePaymnt($id){
        if ($id) {
            $details = AdminAccount::where('id', $id)->with('user_rel:id,name', 'prop_rel:id,property_name,property_to,rent,selling_price')->first();
            return response()->json(['status' => true, 'response' => $details]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }

    public function cancelPayRequest($id)
    {
        if($id)
        {
            $bookings = UserPropertyBookings::find($id);
            $bookings->cancel_status = 1;
            $bookings->save();
            return response()->json(['status' => true , 'response' => 'Cancelled Succesfully']);
        }
        else
        {
            return response()->json(['status' => false , 'response' => 'Id is required']);
        }

    }

   public function approveVerification($id)
   {
     try{
            if($id)
            {
                $bookings = UserPropertyBookings::find($id);

                $bookings->is_verified = 1;
                $bookings->save();


                $notification = new Notifications();
                $notification->type_of_notification ='Doc Verification';
                $notification->user_id = $bookings->user_id;
                $notification->notification_heading ='Document Verification';
                $notification->notification_text = 'Document Verification';

                $notification->save();

                if($bookings->is_verified== 1)
                {
                $notification->status= 1;
                }
                else
                {
                    $notification->status=0;
                }

                return response()->json(['status' => true , 'response' => 'Document verified']);
            }
            else{
                return response()->json(['status' => false , 'response' => 'Id is required']);
            }
        }
        catch(\Exception $e)
        {
            return response()->json(['status' => false , 'response' => 'Document Not verified']);
        }
    }

}
