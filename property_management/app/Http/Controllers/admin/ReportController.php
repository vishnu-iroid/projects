<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Owner;
use App\Models\OwnerProperty;
use App\Models\AdminAccount;
use App\Models\AgentTrackingUserTour;
use App\Models\Frequency;
use App\Models\UserProperty;
use App\Models\AgentProperties;
use App\Models\UserPropertyServiceRequestDocuments;
use App\Models\UserPropertyServiceRequests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use DateTime;
use PDF;

class ReportController extends Controller
{
    //

    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data =   DB::table('owners')
                ->select(
                    DB::raw('COUNT(owner_properties.id) as count'),
                    'owners.*',
                    'owners.id as o_id',
                    'owner_properties.id as prop_id',
                    'owner_properties.property_name as prop_name',
                    'owner_properties.type_id as type_id',
                    'owner_properties.category as prop_category',
                    'owner_properties.management_fee',
                    'owner_properties.property_management_fee_type'
                )
                ->Join('owner_properties', 'owners.id', '=', 'owner_properties.owner_id')
                ->groupBy('o_id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('occupied', function ($row) {
                    $no_of_occupied = OwnerProperty::where('owner_id', $row->o_id)->where('occupied', '1')->count();

                    return $no_of_occupied;
                })
                ->addColumn('management_fee', function ($row) {
                    $no_of_occupied = OwnerProperty::where('owner_id', $row->o_id)->sum('management_fee');
                    return strval(number_format($no_of_occupied,2));
                })
                ->addColumn('collected_management_fee', function ($row) {
                    $users = OwnerProperty::select('owner_properties.id','admin_accounts.property_id','admin_accounts.amount')
                            ->leftjoin('admin_accounts','owner_properties.id','admin_accounts.property_id')
                            ->where('admin_accounts.payment_type', 8)
                            ->where('owner_properties.owner_id', $row->id)
                            ->sum('admin_accounts.amount');
                    return number_format($users,2);
                })
                ->addColumn('vacant', function ($row) {
                    $no_of_vacant = OwnerProperty::where('owner_id', $row->o_id)->where('occupied', '0')->count();

                    return $no_of_vacant;
                })
                // ->addColumn('management_fee', function ($row) {
                //     $properties = OwnerProperty::where('owner_id', $row->o_id)->where('occupied', '1')->where('property_management_fee_type','amount')->sum('management_fee');

                //     $fee = '';

                //     // foreach($properties as $property)
                //     // {
                //     //     if()
                //     // }

                //     return $properties;


                // })
                ->addColumn('actions', function ($row) {
                    return '<a href="' . route('showOwnerPropertyDetails', $row->id) . '" class="fad fa-eye tx-20  mr-1 btn btn-primary bt_cus" href="javascript:0;" title="View"></a>';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
    }
    public function showServiceReport(Request $request)
    {
        if ($request->ajax()) {
            $data = AdminAccount::where('payment_type', 1)->with('user_rel','prop_rel')->get();
            // dd($data);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {
                    return $row->user_rel->name;
                })
                ->addColumn('phone', function ($row) {
                    return $row->user_rel->phone;
                })
                ->addColumn('property', function ($row) {
                    return $row->prop_rel->property_name;
                })
                ->addColumn('unit', function ($row) {
                    if ($row->prop_rel->builder_id > 0) {

                        $building_image = OwnerProperty::where('id', $row->prop_rel->builder_id)->first();
                        return $building_image->property_name;
                    }
                })
                ->addColumn('service', function ($row) {
                    $reference_id   = $row->reference_id;
                    if ($reference_id) {
                        $service_payment = UserPropertyServiceRequestDocuments::select('id', 'request_id')
                            ->with('requested_service.service_related')
                            ->where('id', $reference_id)
                            ->where('document_type', 1)
                            ->first();
                        if (isset($service_payment->requested_service->service_related)) {
                            return $service_payment->requested_service->service_related->service ?? 'NIL';
                        } else {
                            return 'NIL';
                        }
                    } else {
                        return 'NIL';
                    }
                })
                ->addColumn('status', function () {
                    return '<a href="#" class="badge badge-success">Success</a>';
                })
                ->rawColumns(['status'])
                ->make(true);
        }

        return view('admin.report.service_report');
    }

    public function showReservationFeeReport(Request $request)
    {

        if ($request->ajax()) {
            $data = AdminAccount::where('payment_type', 2)->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('name', function ($row) {
                    return $row->user_rel->name;
                })
                ->addColumn('phone', function ($row) {
                    return $row->user_rel->phone;
                })
                ->addColumn('property', function ($row) {
                    return $row->prop_rel->property_name;
                })
                ->addColumn('unit', function ($row) {
                    if ($row->prop_rel->builder_id > 0) {

                        $building_image = OwnerProperty::where('id', $row->prop_rel->builder_id)->first();
                        return $building_image->property_name;
                    }
                })
                ->addColumn('expected_amount', function ($row) {
                    if ($row->prop_rel->token_amount) {
                        return number_format($row->prop_rel->token_amount,2);
                    } else {
                        return 'NIL';
                    }
                })
                ->addColumn('status', function ($row) {
                    return '<a href="#" class="badge badge-success">Success</a>';
                })
                ->rawColumns(['status'])
                ->make(true);
        }
        return view('admin.report.property_reservation_fee_report');
    }

    public function showOwnerReport(Request $request)
    {

        $list = Owner::with('property_rel:owner_id,id')->paginate(10);


        $data = [
            'list' => $list,
        ];
        return view('admin.report.show_owner_report', $data);
    }

    public function showOwnerPropertyDetails(Request $request)
    {
        $id = $request->id;
        $details = OwnerProperty::select('*')
            ->with('owner_amount:id,property_id,amount', 'owner_service_amount:id,property_id', 'frequency_rel', 'owner_rel')
            ->where('owner_id', $id)
            // ->where('is_builder', '!=', 1)
            ->paginate(10);
        $data = [
            'details' => $details
        ];
        // $id = $request->id;
        // $details = UserProperty::select('id', 'user_id', 'property_id')->with('user_rel', 'rent_rel:id,amount,user_property_id', 'prop_rel')
        //     ->where('user_id', $id)->get();

        // foreach ($details as $detai) {
        //     $i = 0;
        //     // foreach ($detai->rent_rel as $variation) {
        //     //     $i = $i + $variation->amount;
        //     // }
        //     unset($details->rent_rel);
        //     $details->amount = $i;
        // }

        // $data = [
        //     'details' => $details
        // ];

        return view('admin.report.showOwnerPropertyDetails', $data);
    }

    public  function showAgentReport()
    {

        $list = Agent::paginate(10);

        foreach ($list as $agent) {
            $agent_id               = $agent->id;
            $agent_commision        = AdminAccount::where('agent_id', $agent_id)->where('amount_type', 2)->where('payment_type', 9)->sum('amount');
            $agent->net_commission  = $agent_commision;
            $agent_task             = AgentTrackingUserTour::where('agent_id', $agent_id)->where('status', 5)->count();
            $agent->completed_task  = $agent_task;
        }

        $data = [
            'list' => $list
        ];
        return view('admin.report.show_agent_report', $data);
    }

    public function depoisteReport(Request $request)
    {
        if($request->ajax()){
                $data = UserProperty::select('*')->with('user_rel', 'prop_rel.owner_rel')->distinct('user_id')->groupBy('user_id')->get();
                return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('uid', function ($row) {
                    return $row->user_rel->id;
                })
                ->addColumn('name', function ($row) {
                    if($row->prop_rel->owner_rel)
                    {
                        return $row->prop_rel->owner_rel->name;
                    }
                    else
                    {
                        return 'NIL';
                    }
                   // return $row->user_rel->name;
                })
                ->addColumn('phone', function ($row) {
                    if($row->prop_rel->owner_rel)
                    {
                        return $row->prop_rel->owner_rel->phone;
                    }
                    else
                    {
                        return 'NIL';
                    }
                })
                ->addColumn('property', function ($row) {
                    return $row->prop_rel->property_name;
                })
                ->addColumn('unit', function ($row) {
                    if ($row->prop_rel->builder_id > 0) {

                        $building_image = OwnerProperty::where('id', $row->prop_rel->builder_id)->first();
                        return $building_image->property_name;
                    }
                })
                ->addColumn('rent', function ($row) {
                    return $row->prop_rel->rent;
                })
                ->addColumn('depoiste', function ($row) {
                    return $row->prop_rel->security_deposit;
                })
                ->addColumn('frequency', function ($row) {
                    if ($row->prop_rel->frequency) {
                        $frequency = Frequency::find($row->prop_rel->frequency);

                        return $frequency->type;
                    } else {
                        return 'NIL';
                    }
                    return $row->prop_rel->security_deposit;
                })
                ->make(true);
        }

        return view('admin.report.show_depoist_report');
    }

    public function showTenantReportView(Request $request)
    {

        if ($request->ajax()) {
            $data = UserProperty::select('*')->with('user_rel', 'prop_rel')->distinct('user_id')->groupBy('user_id')->get();
            //    dd($data);
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('uid', function ($row) {
                    return $row->user_rel->id;
                })
                ->addColumn('name', function ($row) {
                    return $row->user_rel->name;
                })
                ->addColumn('phone', function ($row) {
                    return $row->user_rel->phone;
                })
                ->addColumn('property', function ($row) {
                    if($row->user_rel)
                    {
                        $count = UserProperty::where('user_id',$row->user_rel->id)->count();
                        return $count ;
                    }
                    else
                    {
                        return "NIL";
                    }
                })
                ->addColumn('property', function ($row) {
                    if($row->user_rel)
                    {
                        $count = UserProperty::where('user_id',$row->user_rel->id)->count();
                        return $count ;
                    }
                    else
                    {
                        return "NIL";
                    }
                })
                ->addColumn('due', function ($row)
                {
                    if($row->user_rel)
                    {
                        $rent_sum = AdminAccount::where('user_id',$row->user_rel->id)->where('payment_type' , 5)->sum('amount');

                        return $rent_sum ;
                    }
                    else
                    {
                        return 'NIL';
                    }
                })

                ->addColumn('rent', function ($row)
                {

                    if($row->property_id)
                    {
                       //*--- SQL ---   SELECT user_properties.property_id,sum(owner_properties.rent), owner_properties.id, owner_properties.rent FROM user_properties LEFT JOIN owner_properties ON user_properties.property_id=owner_properties.id  *--//
                        $expired = UserProperty::select('user_properties.property_id','owner_properties.id','owner_properties.rent')
                                                ->leftjoin('owner_properties','user_properties.property_id','owner_properties.id')
                                                ->where('user_properties.property_id', $row->property_id )
                                                ->sum('owner_properties.rent');
                        return $expired ;
                    }
                    else
                    {
                        return 'NIL';
                    }
                })
                ->addColumn('expired', function ($row)
                {
                    if($row->user_rel)
                    {
                        $expired = UserProperty::select('user_properties.id',' booking_contracts.user_property_id', 'booking_contracts.contract_end_date')
                                                ->leftjoin('booking_contracts','user_properties.id','booking_contracts.user_property_id')
                                                ->where('booking_contracts.contract_end_date', '>=' , date('Y-m-d'))
                                                ->where('user_properties.user_id', $row->user_rel->id )
                                                ->count();
                        return $expired ;
                    }
                    else
                    {
                        return 'NIL';
                    }
                })
                ->addColumn('actions', function ($row) {
                    return '<a href="' . route('showTenantPropertyDetails', $row->user_rel->id) . '" class="fad fa-eye btn btn-primary mb-1 bt_cus tx-20  mr-1" href="javascript:0;" title="View"></a>';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
    }
    public function showTenantReport(Request $request)
    {


        $distinct = UserProperty::select('*')->with('user_rel', 'user_property_related')->distinct('user_id')->groupBy('user_id')->get();
        $data = ['result' => $distinct];

        return view('admin.report.show_tenant_report', $data);
    }

    public function showTenantPropertyDetails(Request $request)
    {

        $id = $request->id;
        $details = UserProperty::select('*')->with('user_rel','contract_rel', 'rent_rel', 'prop_rel','prop_rel.frequency_rel')
            ->where('user_id', $id)->get();
        //  dd($details);
        foreach ($details as $detai) {
            $i = 0;
            if (!empty($detai->rent_rel)) {
                foreach ($detai->rent_rel as $variation)
                {
                    $i = $i + isset($variation->amount) ? $variation->amount : 0;
                }
            }
            unset($details->rent_rel);
            $details->amount = $i;
        }

        $data = [
            'details' => $details
        ];

        return view('admin.report.tenant_properties_details', $data);
    }

    public function showRentalReport(Request $request)
    {
        $payment_history = UserProperty::select('user_properties.*', 'owner_properties.property_to','owner_properties.frequency','owner_properties.rent as rent_amount')
            ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
            ->with('user_rel:id,name','contract_rel', 'prop_rel:id,property_reg_no,property_name')
            ->where('owner_properties.property_to', 0)
            ->where('user_properties.status', 0)
            ->where('user_properties.cancel_status', 0)
            ->get();

            if($payment_history){
                foreach($payment_history as $each_payment){
                    if($each_payment->contract_rel){
                        $contract_start_data = $each_payment->contract_rel->contract_start_date;
                        $contract_end_data = $each_payment->contract_rel->contract_end_data;
                        $datetime1 = new DateTime($contract_start_data);
                        $datetime2 = new DateTime($contract_end_data);
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');//now do whatever you like with $days
                        $frequency =  Frequency::find($each_payment->frequency);
                        $no_of_payments   = $days / $frequency->days ;
                        $total_amount_for_pay = $no_of_payments *  $each_payment->rent_amount ;
                        $paid_amount = AdminAccount::where('payment_type', 5)->where('user_id',$each_payment->user_rel->id)->sum('amount');
                        $remaining_amount = $total_amount_for_pay -  $paid_amount ;

                        $each_payment->setAttribute('remaining_amount', $remaining_amount);

                    }
                }
            }

           // dd($payment_history) ;

        $data = [
            'list' => $payment_history,
        ];
        return view('admin.report.show_rental_report', $data);
    }

    public function showRentalPropertyDetails(Request $request)
    {
        $id = $request->id;
        $details = UserProperty::select('*')
            ->where('id', $id)
            ->with('property_service_payment', 'rent1_rel','contract_rel', 'prop_rel','prop_rel.owner_rel','prop_rel.frequency_rel','accounts_rel')
            ->first();
            $diff=$this->dateDiffInDays($details->prop_rel->contract_start_date,$details->prop_rel->contract_end_date);
            $frequency=$details->prop_rel->freequecy_rel->days;
            $noOfDays=$diff/$frequency;
            $service=isset($details->accounts_rel->amount)? $details->accounts_rel->amount:0;
            $netamount=round(($noOfDays*$details->rent)+$details->prop_rel->security_deposit+$service);
            $amount=isset($details->prop_rel->token_amount)?$details->prop_rel->token_amount:0;
            $balance=$netamount-$amount;
        $data = [
            'details' => $details,
            'netamount' => $netamount,
            'balance' => $balance
        ];
        return view('admin.report.rental_property_details', $data);
    }
    public function tenantDetailedReport($id)
    {

        $properties = UserProperty::select('*')->with('user_rel', 'prop_rel', 'contract_rel', 'prop_rel.freequecy_rel', 'accounts_rel')
            ->where('property_id', $id)->first();

        $ordered_services = AdminAccount::where('payment_type', 1)->where('property_id', $id)->count();
        $service          = UserPropertyServiceRequests::with('service_related', 'service_payemnt_doc')->where('user_property_id', $properties->id)->get();

        $check_in_date =  $properties->check_in;
        $check_out_date =  $properties->check_out;

        $diff = $this->dateDiffInDays($check_in_date, $check_out_date);
        $noOfDays = $properties->prop_rel->freequecy_rel;

        $data = [
            'title' => 'Welcome to Siaaha',
            'report' => 'Tenant Report',
            'date' => date('m/d/Y'),
            'properties' => $properties,
            'service_count' => $ordered_services,
            'services'    => $service,
        ];

        $pdf = PDF::loadView('admin.pdf.tenanat_pdf', $data);

        return $pdf->stream();
    }
    public function rentalReport($id)
    {

        $details = UserProperty::select('*')
            ->where('id', $id)
            ->with('property_service_payment', 'rent1_rel', 'prop_rel', 'prop_rel.owner_rel', 'prop_rel.frequency_rel', 'accounts_rel')
            ->first();
        // dd($details);
        $diff = $this->dateDiffInDays($details->prop_rel->contract_start_date, $details->prop_rel->contract_end_date);
        $frequency = $details->prop_rel->freequecy_rel->days;

        $noOfDays = $diff / $frequency;

            $service=isset($details->accounts_rel->amount)? $details->accounts_rel->amount:0;
            $netamount=round(($noOfDays*$details->rent)+$details->prop_rel->security_deposit+$service);
            $balance=$netamount-$details->rent1_rel->amount;
            $data = [
                'details' => $details,
                'netamount'=>$netamount,
                'balance'=>$balance
            ];

        $pdf = PDF::loadView('admin.pdf.rental_report', $data);

        return $pdf->stream();
    }

    function dateDiffInDays($date1, $date2)
    {
        $diff = strtotime($date2) - strtotime($date1);
        return abs(round($diff / 86400));
    }

    public function proprtyReport()
    {
        $proprtyReport = OwnerProperty::with('frequency_rel', 'state_rel', 'city_rel')->paginate(15);

        $data = ['proprtyReport' => $proprtyReport];
        return view('admin.report.property', $data);
    }
    public function proprtyReportPdf($id)
    {

        $property = OwnerProperty::where('id',$id)->with('frequency_rel', 'state_rel', 'city_rel','owner_rel')->first();
        $units =OwnerProperty::
        where('builder_id', $property->id)
        ->with('frequency_rel')
        ->get();

        $data = ['property' => $property,
    'units'=>$units];
        $pdf = PDF::loadView('admin.pdf.propertyReportPdf', $data);

        return $pdf->stream();
        // $data=['proprtyReport'=>$proprtyReport ];
        // return view('admin.report.property',$data);
    }
    public function vacateReport(Request $request)
    {

        $proprtyvacateReport = OwnerProperty::where('occupied', '0')->with('frequency_rel', 'state_rel', 'city_rel')->paginate(15);

        $data = ['proprtyvacateReport' => $proprtyvacateReport];
        return view('admin.report.propertyvacate', $data);
    }
    public function propertyVacantView(Request $request,$id)
    {

        $property = OwnerProperty::where('id',$id)->where('occupied', '0')->with('frequency_rel', 'state_rel', 'city_rel')->first();

        $data = ['property' => $property];
        return view('admin.report.propertyvacantView', $data);
    }
    public function vacateReportPdf()
    {
        $proprtyvacateReport = OwnerProperty::where('occupied', '0')->with('frequency_rel', 'state_rel', 'city_rel')->get();

        $data = ['proprtyvacateReport' => $proprtyvacateReport];
        $pdf = PDF::loadView('admin.pdf.vacateReportPdf', $data);

        return $pdf->stream();

        // return view('admin.pdf.vacateReportPdf',$data);
    }

    public function dueReport(Request $request)
    {
        if ($request->ajax()) {
            $data = UserProperty::select('*',)->with('user_rel','rent_collected','prop_rel','prop_rel.frequency_rel')->distinct('user_id')->groupBy('user_id')->get();

            return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('uid', function ($row) {
                        return $row->user_rel->id;
                    })
                    ->addColumn('name', function ($row) {
                        return $row->user_rel->name;
                    })
                    ->addColumn('phone', function ($row) {
                        return $row->user_rel->phone;
                    })
                    ->addColumn('property', function ($row) {
                        return $row->prop_rel->property_name;
                    })
                    ->addColumn('unit', function ($row) {
                        if ($row->prop_rel->builder_id > 0)
                        {
                            $building_image=OwnerProperty::where('id',$row->prop_rel->builder_id)->first();
                            return $building_image->property_name;
                        }
                    })
                    ->addColumn('frequency', function ($row) {
                        if($row->prop_rel->frequency)
                        {
                            $frequency = Frequency::find($row->prop_rel->frequency);
                            return $frequency->type;
                        }
                        else
                        {
                            return 'NIL';
                        }
                        return $row->prop_rel->security_deposit;
                    })
                    ->addColumn('over_due', function ($row) {
                        $diff=$this->dateDiffInDays($row->prop_rel->contract_start_date,date('Y-m-d'));
                            if($row->prop_rel->frequency_rel)
                            {
                                if($row->prop_rel->frequency_rel)
                                {
                                    $frequency=$row->prop_rel->frequency_rel->days;
                                    $noOfDays = $diff/$frequency;
                                    $netamount=round(($noOfDays*$row->rent));
                                    $rent = AdminAccount::where('user_id',$row->user_rel->id)->where('payment_type',5)->sum('amount');
                                    return $netamount - $rent  ;
                                }
                                else
                                {
                                    return 'NIL';
                                }
                            }
                            else
                            {
                                return 'NIL';
                            }
                    })
                    ->addColumn('due', function ($row) {
                        if($row->prop_rel->rent)
                        {
                            return $row->prop_rel->rent;
                        }
                        else
                        {
                            return 'NIL';
                        }
                    })
                    ->addColumn('balance', function($row){
                        $diff=$this->dateDiffInDays(date('Y-m-d'),$row->prop_rel->contract_end_date);
                        if($row->prop_rel->frequency_rel)
                            {
                                if($row->prop_rel->frequency_rel)
                                {
                                    $frequency=$row->prop_rel->frequency_rel->days;
                                    $noOfDays = $diff/$frequency;
                                    $netamount=round(($noOfDays*$row->rent));
                                    $rent = AdminAccount::where('user_id',$row->user_rel->id)->where('payment_type',5)->sum('amount');
                                    return $netamount - $rent  ;
                                }
                                else
                                {
                                    return 'NIL';
                                }
                            }
                            else
                            {
                                return 'NIL';
                            }
                    })
                    ->make(true);
        }

        return view('admin.report.due_amount');

    }
    public function agentReport(Request $request){
        if ($request->ajax()) {
            $data   = Agent::with('agentProp_rel', 'agentProp_rel.prop_rel')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('counts', function ($row) {
                    $agentCount = DB::table('agent_properties')
                                            ->where('agent_id', $row->id)
                                            ->count('property_id');
                                            return $agentCount;
                })
                ->addColumn('opendeal', function ($row)
                {
                    // $opendeal = AgentProperties::select('agent_properties.id','agent_properties.agent_id','agent_properties.property_id as assigned_p_id','user_property_bookings.status')->leftjoin('user_property_bookings','agent_properties.property_id','user_property_bookings.property_id')->where('agent_properties.agent_id', $row->id)->count();
                    return '';
                })
                ->addColumn('closing_deal', function ($row) {
                    $deal = DB::table('agent_properties')
                        ->join('user_properties', 'agent_properties.property_id', '=', 'user_properties.property_id')
                        ->where('agent_properties.agent_id', $row->id)
                        ->groupBy('agent_properties.agent_id')
                        ->count();
                    return $deal ;
                })
                ->addColumn('terminated', function ($row) {
                    $deal = DB::table('agent_properties')
                        ->join('owner_properties', 'agent_properties.property_id', '=', 'owner_properties.id')
                        ->where('agent_properties.agent_id', $row->id)
                        ->where('owner_properties.occupied', 0)
                        ->groupBy('agent_properties.agent_id')
                        ->count();
                    return $deal ;
                })
                ->addColumn('pending', function ($row) {
                    $deal = DB::table('agent_properties')
                        ->join('owner_properties', 'agent_properties.property_id', '=', 'owner_properties.id')
                        ->where('agent_properties.agent_id', $row->id)
                        ->where('owner_properties.occupied', 0)
                        ->groupBy('agent_properties.agent_id')
                        ->count();

                    return $deal ;
                })

                ->make(true);
        }

        return view('admin.report.agent');
    }
}
