<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

use App\Models\PropertyType;
use App\Models\AmenitiesCategory;
use App\Models\Amenity;
use App\Models\PropertyTypeAmenityContent;
use App\Models\Detail;
use App\Models\PropertyTypeDetailsContent;

class PropertyTypeController extends Controller
{
    public function showTypes(){
        $type               = PropertyType::orderBy('id','DESC')->paginate(10);
        $amenity_categories = AmenitiesCategory::orderBy('id','DESC')->get();
        $amenities          = Amenity::orderBy('id','DESC')->get();
        $details            = Detail::get();
        $data = [
            'type'                  => $type,
            'amenity_categories'    => $amenity_categories,
            'amenities'             => $amenities,
            'details'               => $details,
        ];
        return view('admin.property.type',$data);
    }
    public function addType(Request $request){
        $rules = [
            'type'          => 'required',
            'amenities'     => 'required|array',
            'details'       => 'required|array'
        ];
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()->all()]);
        }
        if($request->id){
            $type = PropertyType::find($request->id);
            $msg  = "Type updated Successfully" ;
        }else{
            $type = new PropertyType;
            $msg  = "Type added Successfully";
        }
        $type->type = $request->type;
        $type->category = $request->category;
        try{
            DB::beginTransaction();
            $type->save();
            if($request->id){
                PropertyTypeAmenityContent::where('type_id',$request->id)->delete();
                PropertyTypeDetailsContent::where('type_id',$request->id)->delete();
            }
            $amenityarray = array();$detailsarray = array();
            foreach($request->amenities as $key=>$row){
                $category                                   = Amenity::where('id',$row)->pluck('amenity_category_id');
                $amenityarray[$key]['type_id']              = $type->id;
                $amenityarray[$key]['amenity_category_id']  = $category[0];
                $amenityarray[$key]['amenity_id']           = $row;
                $amenityarray[$key]['created_at']           = now();
                $amenityarray[$key]['updated_at']           = now();
            }
            PropertyTypeAmenityContent::insert($amenityarray);
            foreach($request->details as $key=>$row6){
                $detailsarray[$key]['type_id']      = $type->id;
                $detailsarray[$key]['detail_id']    = $row6;
                $detailsarray[$key]['created_at']   = now();
                $detailsarray[$key]['updated_at']   = now();
            }
            PropertyTypeDetailsContent::insert($detailsarray);
            DB::commit();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
    public function viewType($id){
        if($id){
            $type           = PropertyType::find($id);
            $amenities      = PropertyTypeAmenityContent::where('type_id',$id)->get();
            $details        = PropertyTypeDetailsContent::where('type_id',$id)->get();
            $data = [
                'type'      => $type,
                'amenities' => $amenities,
                'details'   => $details,
            ];
            return response()->json(['status'=>true,'response'=>$data]);
        }else{
            return response()->json(['status'=>true,'response'=>'Something went wrong']);
        }
    }
}
