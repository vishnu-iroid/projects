<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PrivacyPolicy;
use Hash;
use Validator;
use File;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;

class PrivacyController extends Controller
{



    public function showPrivacyPolicy(Request $request)
    {
        $privacy = PrivacyPolicy::first();
        $data = array(
            'privacy' => $privacy
        );
        return view('admin.privacy.index', $data);
    }

    public function showUserPrivacyPolicy(Request $request)
    {
        $lang = $request->lang;
        if ($lang == 1) {
            $privacy = PrivacyPolicy::select('english_description AS description')->first();
        } else {
            $privacy = PrivacyPolicy::select('arabic_description AS description')->first();
        }
        $data = array(
            'privacy' => $privacy,
            'lang' => $lang,
        );
        return view('admin.privacy.user_view_privacy', $data);
    }
    public function addPrivacy(Request $request)
    {
        $privacy =  PrivacyPolicy::first();
        if(!$privacy){
            $privacy =  new PrivacyPolicy();
        }
        if ($request->english_description) {
            $privacy->english_description = $request->english_description;
        }
        if ($request->arabic_description) {
            $privacy->arabic_description = $request->arabic_description;
        }
        try {
            if ($request->id) {
                Session::flash('message', "Updated successfully");
            } else {
                Session::flash('message', "Saved successfully");
            }
            Session::flash('alert-class', 'tx-success');
            Session::flash('alert-icon', 'ion-ios-checkmark-outline');
            $privacy->save();
        } catch (\Exception $e) {
            // print_R($e->getMessage());exit;
            Session::flash('message', "Something went wrong");
            Session::flash('alert-class', 'tx-danger');
            Session::flash('alert-icon', 'ion-ios-close-outline');
        }
        return Redirect::to('show-privacy');
    }
}
