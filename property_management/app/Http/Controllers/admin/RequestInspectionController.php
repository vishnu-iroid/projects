<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\InsecptionTeam;


class RequestInspectionController extends Controller
{
    //
    public function showInspectionTeam()
    {
        $inspectDetails   = InsecptionTeam::orderBy('id', 'DESC')->paginate(10);
        $data = [
            'inspectDetails'  => $inspectDetails,

        ];
        return view('admin.InspectionTeam.inspectionTeam', $data);
    }


    public function addInspectionTeam(Request $request)
    {

        $rules = [
            'name'  => 'required',
            'phone' => 'required',
            'status' => 'required',

        ];
        $messages = [
            'name.required'  => 'name is required',
            'phone.required' => 'phone is required',
            'status.required' => 'status is required',

        ];

        $agent = new InsecptionTeam;
        $rules['email'] = 'required|email|unique:agents,email';
        $rules['phone'] = 'required|unique:agents,phone';
        $msg = 'Agent added successfully';
        $validation = Validator::make($request->all(), $rules, $messages);
        $agent->name = $request->name;
        $agent->email = $request->email;
        $agent->phone = $request->phone;
        $agent->status = $request->status;
        try {
            $agent->save();
            return response()->json(['status' => true, 'response' => $msg]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }


    ///////// Blocking  of Inspection Member id /////////////////////////
    public function disableid($id, $action)
    {
        if ($id) {
            $admin = InsecptionTeam::where('id', $id)->update(['status' => $action]);
            if ($action == 0) {
                $msg = 'Blocked  successfully';
            } else {
                $msg = 'Unblocked  successfully';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    ///view//
    public function viewInspectionTeam($id)
    {
        if (!$id) {
            return response()->json(['status' => false, 'response' => 'Something went wrong', 'type' => 'inspection team']);
        }
        $inspection = InsecptionTeam::find($id);
        return response()->json(['status' => true, 'response' => $inspection, 'type' => 'inspection team']);
    }
}
