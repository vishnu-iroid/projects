<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\AdminAccount;
use Illuminate\Http\Request;
use App\Models\UserPropertyBookings;
use App\Models\UserPropertyBookingDocuments;
use App\Models\UserProperty;
use App\Models\BookUserTour;
use App\Models\AgentTrackingUserTour;
use App\Models\UserTourBookingDocument;
use App\Models\User;
use App\Models\Tenant;
use App\Models\Agent;
use App\Models\AgentRequestingContract;
use App\Models\UserReferral;
use App\Models\Rewards;
use App\Models\BookingContract;
use App\Models\Frequency;
use App\Models\UserRewards;
use App\Models\OfferPackage;
use App\Models\Owner;
use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDetail;
use App\Models\UserPropertyFullAmountDocument;
use File;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class UserBookingController extends Controller
{


    public function userBookings(Request $request)
    {
        $status = $request->status;
        if ($status == "") {
            $status = 0;
        }
        if ($request->status == 0) {
            $user = UserPropertyBookings::select('user_property_bookings.*', 'agents.name as agent_name', 'agents.phone')
                ->with('user_details:id,name,phone', 'agent_details', 'property_relation', 'property_relation.builders')
                ->leftjoin('agents', 'agents.id', 'user_property_bookings.assigned_agent')
                ->where('user_property_bookings.cancel_status', 0)
                // ->where('user_property_bookings.is_verified', 1)
                ->where('user_property_bookings.approve_status', 0)
                //  ->orderBy('user_property_bookings.id', 'DESC')
                ->groupBy('user_property_bookings.id')
                ->paginate(10);
            //  dd($user);
            $data = [
                'users' => $user,
            ];
            return view('admin.user.user-bookings-pending', $data);
        } elseif ($request->status == 1) {
            $user = UserProperty::orderBy('check_in', 'DESC')
                ->with('prop_rel')
                ->with('user_rel')
                ->with('doc_rel', 'prop_rel.assigned_agent_details')
                ->where('document_verified', 0)
                ->paginate(10);
            $data = [
                'users' => $user,
            ];
            return view('admin.user.user-bookings-approved', $data);
        } elseif ($request->status == 2) {
            // $agent = Agent::select('id', 'name')->get();
            // $user = UserProperty::orderBy('check_in', 'DESC')
            //     ->with('prop_rel')
            //     ->with('user_rel')
            //     ->where('document_verified', 0)
            //     ->where('status' ,'!=', 3)
            //     ->paginate(10);
            // $data = [
            //     'users' => $user,
            //     'agent' => $agent,
            // ];
            $users = AgentRequestingContract::where('upload_status', 2)
                ->with('user_property_rel', 'agent_rel', 'user_property_rel.user_rel', 'user_property_rel.user_property_related')
                ->paginate(10);
            // dd($users);
            $data = ['users' => $users];

            return view('admin.user.user-bookings-terminated', $data);
        }
    }

    public function issuedContract(Request $request)
    {
        if ($request->ajax()) {

            $data = UserProperty::with('prop_rel', 'user_rel', 'contract_rel')
                ->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('tanent', function (UserProperty $tour) {
                    if (isset($tour->user_rel)) {

                        $name = $tour->user_rel->name;
                        return $name;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('tanent_contact', function (UserProperty $tour) {
                    if (isset($tour->user_rel)) {
                        $phone = $tour->user_rel->phone;
                        return $phone;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('property', function (UserProperty $tour) {
                    if (isset($tour->prop_rel)) {
                        $property = $tour->prop_rel->property_name;
                        return $property;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('unit', function (UserProperty $unit) {
                    if ($unit->prop_rel->builder_id != Null) {
                        $building_image = OwnerProperty::where('id', $unit->prop_rel->builder_id)->first();
                        return $building_image->property_name;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('contract_start_date', function (UserProperty $tour) {
                    if (isset($tour->contract_rel)) {
                        $start_date = $tour->contract_rel->contract_start_date;
                        return $start_date;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('contract_end_date', function (UserProperty $tour) {
                    if (isset($tour->contract_rel)) {
                        $end_date = $tour->contract_rel->contract_end_date;
                        return $end_date;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('owner', function (UserProperty $tour) {
                    if (isset($tour->prop_rel)) {
                        if (isset($tour->prop_rel->owner_id)) {
                            $id = $tour->prop_rel->owner_id;
                            $owner_data = Owner::find($id);
                            if ($owner_data) {
                                return $owner_data->name;
                            } else {
                                return "NIL";
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('owner_contact', function (UserProperty $tour) {
                    if (isset($tour->prop_rel)) {
                        if (isset($tour->prop_rel->owner_id)) {
                            $id = $tour->prop_rel->owner_id;
                            $owner_data = Owner::find($id);
                            if ($owner_data) {
                                return $owner_data->phone;
                            } else {
                                return "NIL";
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('actions', function (UserProperty $tour) {
                    return  '<a class=" mr-1" href="javascript:0;"   onclick="renewContractFunction(' . $tour->id . ',' . $tour->prop_rel->property_to . ' ,' . $tour->rent . ')" data-property-id="' . $tour->prop_rel->id . '" title="Contract Renew Button"> <img src="' . asset('assets/images/repeat.png') . '" class="" width="20px" alt=""></a>';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.user.user-bookings-completed');
    }


    public function propertDocumentVerfication($id)
    {

        if ($id) {
            $user = UserProperty::where('id', $id)->update(['document_verified' => '1']);
            return response()->json(['status' => true, 'response' => "Document Verified"]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }

    //////////////////////////////////// Book Tour ////////////////////////////////

    public function agentBookingsDetails($id)
    {

        $data = BookUserTour::select('user_tour_bookings.*', 'user_tour_bookings.time_range as time', 'user_tour_bookings.booked_date as date', 'user_tour_bookings.id as ids', 'owner_properties.*', 'agent_tracking_user_tour.*', 'agent_tracking_user_tour.status as tstatus')
            ->with('user_rel:id,name')
            ->join('owner_properties', 'owner_properties.id', 'user_tour_bookings.property_id')
            ->join('agent_tracking_user_tour', 'agent_tracking_user_tour.tour_id', 'user_tour_bookings.id')
            ->where('user_tour_bookings.id', $id)
            ->first();

        return response()->json(['status' => true, 'response' => $data]);
    }

    public function agentBookings(Request $request)
    {
        $owner = Owner::select('id', 'name')->get();
        $agent = Agent::select('id', 'name')->get();
        $property = OwnerProperty::select('id', 'property_reg_no')->get();
        if ($request->ajax()) {
            $data = BookUserTour::select('user_tour_bookings.*', 'user_tour_bookings.id as ids', 'owner_properties.*', 'agent_tracking_user_tour.*', 'agent_tracking_user_tour.status as tstatus')
                ->with('user_rel:id,name', 'property_details')
                ->join('owner_properties', 'owner_properties.id', 'user_tour_bookings.property_id')
                ->join('agent_tracking_user_tour', 'agent_tracking_user_tour.tour_id', 'user_tour_bookings.id');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('user', function (BookUserTour $tour) {
                    return (isset($tour->user_rel->name)) ? $tour->user_rel->name : 'Nil';
                })
                ->addColumn('agent', function ($row) {
                    return (isset($row->agent_rel->name)) ? $row->agent_rel->name : 'Nil';
                })
                ->addColumn('unit', function ($row) {
                    if ($row->builder_id != null) {
                        $building_image = OwnerProperty::where('id', $row->builder_id)->first();
                        return $building_image->property_name;
                    } else {
                        return "---";
                    }
                })
                ->addColumn('status', function ($data) {
                    if ($data->tstatus == '1') {
                        $payment_type = 'Not Started ';
                    } else {
                        $payment_type = ' vist-started';
                    }
                    return $payment_type;
                })
                ->addColumn('action', function ($row) {
                    $button = '<a class="fad fa-eye tx-20 view-button mr-1" href="javascript:;" onclick="viewFunction(' . $row->ids . ')" data-booking="{{$row->ids}}" title="View"></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->filter(function ($instance) use ($request) {

                    if ($status = $_GET['status']) {
                        $status = $_GET['status'];
                        if ($status = "") {
                            $status = 0;
                        }
                        if ($status = 0) {
                            $instance->where(function ($w) use ($status) {
                                $w->where('agent_tracking_user_tour.status', 'LIKE', "%$status%");
                            });
                        } elseif ($status = 1) {
                            $instance->where(function ($w) use ($status) {
                                $w->where('agent_tracking_user_tour.status', 'LIKE', "%$status%");
                            });
                        } elseif ($status = 2) {
                            $instance->where(function ($w) use ($status) {
                                $w->where('agent_tracking_user_tour.status', 'LIKE', "%$status%");
                            });
                        }
                    }
                    if (!empty($request->get('agent'))) {
                        $instance->where(function ($w) use ($request) {
                            $agent = $request->get('agent');
                            $w->where('agent_tracking_user_tour.agent_id', 'LIKE', "%$agent%");
                        });
                    }
                    if (!empty($request->get('agent'))) {
                        $instance->where(function ($w) use ($request) {
                            $agent = $request->get('agent');
                            $w->where('agent_tracking_user_tour.agent_id', 'LIKE', "%$agent%");
                        });
                    }
                    if (!empty($request->get('owner'))) {
                        $instance->where(function ($w) use ($request) {
                            $owner = $request->get('owner');
                            $w->where('owner_properties.owner_id', 'LIKE', "%$owner%");
                        });
                    }
                    if (!empty($request->get('date'))) {
                        $instance->where(function ($w) use ($request) {
                            $date = $request->get('date');
                            $new_date = date('Y-m-d', strtotime($date));
                            $w->where('user_tour_bookings.booked_date', 'LIKE', "%$new_date%");
                        });
                    }
                    if (!empty($request->get('property'))) {
                        $instance->where(function ($w) use ($request) {
                            $property = $request->get('property');
                            $w->where('user_tour_bookings.property_id', 'LIKE', "%$property%");
                        });
                    }
                })
                ->make(true);
        }
        $data = [
            'owners' => $owner,
            'agents' => $agent,
            'property' => $property,
        ];

        return view('admin.user.agent-booking-pending', $data);

        // $status = $request->stastus;
        // $owner = Owner::select('id','name')->get();

        // if ($status == "") {
        // $status == "0";
        // }

        // if ($request->status == 0) {
        // $user = BookUserTour::select('user_tour_bookings.*', 'owner_properties.*', 'agent_tracking_user_tour.*', 'agent_tracking_user_tour.status as tstatus')
        // ->with('user_rel:id,name')
        // ->join('owner_properties', 'owner_properties.id', 'user_tour_bookings.property_id')
        // ->join('agent_tracking_user_tour', 'agent_tracking_user_tour.tour_id', 'user_tour_bookings.id')
        // ->paginate(10);

        // $data = [
        // 'users' => $user,
        // 'owners' => $owner,
        // ];
        // return view('admin.user.agent-booking-pending', $data);
        // }
        // if ($request->status == 1) {
        // $user = BookUserTour::select('user_tour_bookings.*', 'owner_properties.*', 'agent_tracking_user_tour.*', 'agent_tracking_user_tour.status as tstatus')
        // ->with('user_rel:id,name')
        // ->join('owner_properties', 'owner_properties.id', 'user_tour_bookings.property_id')
        // ->join('agent_tracking_user_tour', 'agent_tracking_user_tour.tour_id', 'user_tour_bookings.id')
        // ->where('agent_tracking_user_tour.status', 2)
        // ->paginate(10);

        // $data = [
        // 'users' => $user,
        // 'owners' => $owner,
        // ];
        // return view('admin.user.agent-booking-booked', $data);
        // }
        // if ($request->status == 2) {
        // $user = BookUserTour::select('user_tour_bookings.*', 'owner_properties.*', 'agent_tracking_user_tour.*', 'agent_tracking_user_tour.status as tstatus')
        // ->with('user_rel:id,name')
        // ->join('owner_properties', 'owner_properties.id', 'user_tour_bookings.property_id')
        // ->join('agent_tracking_user_tour', 'agent_tracking_user_tour.tour_id', 'user_tour_bookings.id')
        // ->where('agent_tracking_user_tour.status', 3)
        // ->paginate(10);

        // $data = [
        // 'users' => $user,
        // 'owners' => $owner,
        // ];
        // return view('admin.user.agent-booking-completed', $data);
        // }
    }

    /////////////////////////////////////END ///////////////////////////////////////////////

    public function cancelBooking($id)
    {

        if ($id) {
            if (UserPropertyBookings::where('id', $id)->update(['cancel_status' => '1'])) {

                $booking_details = UserPropertyBookings::find($id);
                if ($booking_details->package_id) {
                    $package_id = $booking_details->package_id;
                    $package_details = OfferPackage::where('id', $package_id)->update(['status' => 1]);
                }
                $msg = 'Cancelled booking successfully';
            } else {
                $msg = 'Something went wrong';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }

    public function viewBookingDetails($id)
    {
        $user   = UserPropertyBookings::select('id', 'booking_id', 'user_id', 'package_id', 'property_id', 'check_in', 'check_out', 'coupoun', 'is_verified')

            ->with(
                'property_details',
                'user_details:id,name',
                'booking_doc:id,booking_id,document',
                'offer_rel:id,discount_amount',
                'account_rel',
                'coupon_rel',
                'agent_details:id,name'
            )
            ->where('id', $id)
            ->first();


        $data = [
            'user' => $user
        ];
        return view('admin.user.booking-detail', $data);
    }


    public function approveBooking(Request $request)
    {
        $rules = [
            'due_date' => 'required|date',
            'check_in' => 'required|date',
            'check_out' => 'date',
            'booking_id' => 'required',
        ];
        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json(['status' => 0, 'response' => $validation->errors()->all()]);
        }
        try {
            $booking_id  = $request->booking_id;

            $booking_details   = UserPropertyBookings::where('id', $booking_id)
                ->where('is_verified', 1)
                ->first();
                //dd($booking_details);

            if ($booking_details) {
                $approve_booking                    = new UserProperty();
                $approve_booking->user_id           = $booking_details->user_id;
                $approve_booking->property_id       = $booking_details->property_id;
                $approve_booking->due_date          = $request->due_date;
                $approve_booking->check_in          = $request->check_in;
                $approve_booking->check_out         = $request->check_out;
                $approve_booking->status            = 2;
                $approve_booking->document_verified = 0;
                $approve_booking->cancel_status     = 0;


                // if ($booking_details->package_id) {
                //     $id = $booking_details->package_id;
                //     $package_details =  OfferPackage::find($id);
                //     $approve_booking->rent = $package_details->discount_amount;
                //     $approve_booking->package_id = $booking_details->package_id;
                // } elseif ($booking_details->coupoun) {

                //     $property_id = $booking_details->property_id;
                //     $coupon = $booking_details->coupoun;
                //     $owner_properties = OwnerProperty::find($property_id);
                //     $coupon_details = Rewards::find($coupon);
                //     $amount = "";
                //     $rent = "";
                //     if ($owner_properties->property_to == 0) {
                //         $amount = $owner_properties->rent - $request->token;
                //     } elseif ($owner_properties->property_to == 1) {
                //         $amount = $owner_properties->selling_price - $request->token;
                //     }
                //     if ($coupon_details->percentage) {
                //         $rent = ($coupon_details->percentage / 100) * $amount;
                //     } elseif ($coupon_details->amount) {
                //         $rent = $amount -  $coupon_details->amount;
                //     }

                //     $approve_booking->rent  = $request->token;
                // } else {
                    $property_id = $booking_details->property_id;
                    $owner_properties = OwnerProperty::find($property_id);
                    $net_amount = $owner_properties->security_deposit +  $owner_properties->rent ;
                    // $rent = $net_amount - (int)$request->token;
                    // dd($rent);
                    $approve_booking->rent =  $request->token;
                    //dd($approve_booking->rent);
                // }
                $approve_booking->save();
               //
               // dd($approve_booking);
                $id = $approve_booking->id;
                if ($id) {
                    $property_id = $booking_details->property_id;
                    OwnerProperty::where('id', $property_id)->update(['occupied' => "1"]);
                }
                // update property approved to bookings
                $booking_details->approve_status       = 1;
             //   $booking_details->save();

                $check = UserPropertyBookings::find($booking_id);

                if ($check->assigned_agent) {
                    AgentTrackingUserTour::where('booking_id', $booking_id)->update(['status' => "3", 'user_property' =>  $approve_booking->id]);
                }

                return response()->json(['status' => 1, 'response' => "Booking approved successfully"]);
            } else {
                return response()->json(['status' => 0, 'response' => "This booking doesn't exist or not yet verified"]);
            }
            return response()->json(['status' => 1, 'response' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'response' => [$e->getMessage()]]);
        }
    }

    public function assignAgent($bookid, $agentid)
    {
        if ($agentid) {
            if (UserPropertyBookings::where('booking_id', $bookid)->update(['assigned_agent' => $agentid])) {
                $msg = 'Assigning Agent successfully';
            } else {
                $msg = 'Something went wrong';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }

    public function editUserBookingContract(Request $request)
    {
        // dd($request->contract_no);

        $contract_no                    = $request->contract_no;
        $user_property_id               = $request->bookid;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
        $booking_details = UserProperty::find($user_property_id);


        $Contract_details = new BookingContract;
        $Contract_details->user_property_id =  $user_property_id;
        $Contract_details->user_id =  $booking_details->user_id;
        $Contract_details->contract_no = $contract_no;
        $Contract_details->contract_status = 1;
        $Contract_details->contract_start_date =  $contract_start_date;
        $Contract_details->contract_end_date = $contract_end_date;
        $Contract_details->total_free_maintenance = $request->free_maintenance;
        $Contract_details->completed_maintenance = 0;
        $Contract_details->status = 1;

        $Contract_details->maintenance_charge = $request->maintenance_charge;
        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $user_property_id . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $Contract_details->contract_file = $file_name;
        }
        $Contract_details->save();

        return response()->json(['status' => true, 'response' => "Contract Added SuccessFully"]);
    }
    public function addUserBookingContract(Request $request)
    {
        // dd($request->contract_no);

        $contract_no                    = $request->contract_no;
        $user_property_id               = $request->bookedid;
        $request_id                     = $request->requestid;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
        $booking_details = UserProperty::find($user_property_id);

        $user = $booking_details->user_id;
        $find_refreed = UserReferral::where(['user_id' => $user, 'status' => 0])->first();

        if ($find_refreed) {
            $coupon = Rewards::where('max_count', '>=', 1)->where('status', 1)->inRandomOrder()->first();;
            $allocate = new UserRewards();
            $allocate->user_id = $find_refreed->referred_user_id;
            $allocate->reward_id = $coupon->id;
            $allocate->status = 0;
            $allocate->save();
            $update = Rewards::where('id', $coupon->id)->update(['max_count' => $coupon->max_count - 1]);
        }

        if ($request_id) {
            AgentRequestingContract::where('id', $request_id)->update(['upload_status' => '1']);
        }

        $Contract_details = new BookingContract;
        $Contract_details->user_property_id =  $user_property_id;
        $Contract_details->user_id =  $booking_details->user_id;
        $Contract_details->contract_no = $contract_no;
        $Contract_details->contract_status = 1;
        $Contract_details->contract_start_date =  $contract_start_date;
        $Contract_details->contract_end_date = $contract_end_date;
        $Contract_details->total_free_maintenance = $request->free_maintenance;
        $Contract_details->completed_maintenance = 0;
        $Contract_details->maintenance_charge = $request->maintenance_charge;
        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $user_property_id . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $Contract_details->contract_file = $file_name;
        }
        $Contract_details->save();


        $property_changes = UserProperty::where('id', $user_property_id)->first();

        $id = $property_changes->package_id;
        $owner_prop = OwnerProperty::find($property_changes->property_id);

        $package_details = OfferPackage::find($id);
        if ($package_details) {
            $frequency = $package_details->frequency_id;
        } else {
            $frequency = $owner_prop->frequency;
        }

        $date = $property_changes->due_date;
        if ($frequency == '1') {
            $date = date('Y-m-d', strtotime($date . ' + 7 days'));
        } elseif ($frequency == '2') {
            $date = date('Y-m-d', strtotime($date . ' + 1 months'));
        } elseif ($frequency == '3') {
            $date = date('Y-m-d', strtotime($date . ' + 1 years'));
        } elseif ($frequency == '4') {
            $date = date('Y-m-d', strtotime($date . ' + 1 days'));
        }

        $property_changes->due_date = $date;
        if ($owner_prop->property_to == 0) {
            $property_changes->status = "0";
        } elseif ($owner_prop->property_to == 1) {
            $property_changes->status = "1";
        }

        $property_changes->save();


        return response()->json(['status' => true, 'response' => "Contract Added SuccessFully"]);
    }

    public function addAgentBookingContract(Request $request)
    {

        $contract_no                    = $request->contract_no;
        $booking_id                    = $request->bookedid;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));

        $booking_details = BookUserTour::find($booking_id);

        $Contract_details = new BookingContract;
        $Contract_details->booking_id = $booking_id;
        $Contract_details->user_id =   $booking_details->user_id;
        $Contract_details->contract_status = 2;
        $Contract_details->contract_start_date =  $contract_start_date;
        $Contract_details->contract_end_date = $contract_end_date;
        $Contract_details->save();

        $document = new UserTourBookingDocument;
        $document->booking_id   = $booking_id;

        $document->user_id = $booking_details->user_id;
        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                if (!File::isDirectory($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $booking_id . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $document->document = $file_name;
        }
        $document->save();


        return response()->json(['status' => true, 'response' => "Success"]);
    }

    public function requestForContractByAgent(Request $request)
    {
        if ($request->ajax()) {
            $agent_tour = AgentRequestingContract::with('agent_rel')->where('upload_status', '0')->get();
            return DataTables::of($agent_tour)
                ->addIndexColumn()
                ->addColumn('agent', function (AgentRequestingContract $tour) {
                    return (isset($tour->agent_rel->name)) ? $tour->agent_rel->name : 'Nil';
                })
                ->addColumn('agent_contact', function (AgentRequestingContract $tour) {
                    return (isset($tour->agent_rel->phone)) ? $tour->agent_rel->phone : 'Nil';
                })
                ->addColumn('user', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        $user_id = $tour->user_property_rel->user_id;
                        if ($user_id) {
                            $name = User::find($user_id);
                            return $name->name;
                        } else {
                            return "NIL";
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('check_in', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {

                        $check_out = $tour->user_property_rel->check_in;
                        return   $check_out;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('check_out', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {

                        $check_out = $tour->user_property_rel->check_out;
                        return   $check_out;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('attachment', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            $property_name = OwnerProperty::with('property_priority_image')->first();
                            return $property_name->property_priority_image->document;
                        }
                    } else {
                        return "NIL";
                    }
                })

                ->addColumn('property', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            $property_name = OwnerProperty::find($id);
                            return $property_name->property_name;
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('property_rent', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            $property_name = OwnerProperty::find($id);
                            return $property_name->rent;
                        }
                    } else {
                        return "NIL";
                    }
                })

                ->addColumn('building', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            $property_name = OwnerProperty::find($id);

                            return $property_name->property_name;
                        } else {
                            return "NIL";
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('unit', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        $id = $tour->user_property_rel->property_id;
                        $property = OwnerProperty::find($id);
                        if ($property->is_builder > 0) {
                            $unit = OwnerProperty::where('id', $property->builder_id)->first();

                            return $unit->property_name;
                        } else {
                            return "NIL";
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('ownership_doc', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            $property = OwnerProperty::find($id);
                            if ($property) {
                                return $property->ownership_doc;
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('security_deposit', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            $property = OwnerProperty::find($id);
                            if ($property) {
                                return number_format($property->security_deposit,2);
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })


                ->addColumn('frequency', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            if ($property = OwnerProperty::find($id)) {
                                $frequenct_data = Frequency::find($property->frequency);
                                if ($frequenct_data) {
                                    return $frequenct_data->type;
                                } else {
                                    return "NIL";
                                }
                            } else {
                                return "Nil";
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('floorNo', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            if ($property = OwnerProperty::find($id)) {
                                $floorNo = OwnerPropertyDetail::where('property_id', $property->id)->where('detail_id', 1)->first();
                                if ($floorNo) {
                                    return $floorNo->value;
                                } else {
                                    return "NIL";
                                }
                            } else {
                                return "Nil";
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('owner', function (AgentRequestingContract $tour) {
                    if (isset($tour->user_property_rel)) {
                        if (isset($tour->user_property_rel->property_id)) {
                            $id = $tour->user_property_rel->property_id;
                            if ($property = OwnerProperty::find($id)) {
                                $owner_data = Owner::find($property->owner_id);
                                if ($owner_data) {
                                    return $owner_data->name;
                                } else {
                                    return "NIL";
                                }
                            } else {
                                return "Nil";
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('action', function ($row) {
                    $button = '<a class="fad fa-upload tx-20 upload-button mr-1 btn btn-primary bt_cus mb-1" href="javascript:;" data-requestId="' . $row->id . '"  data-property="' . $row->user_property_id . '" title="Upload Contract"></a>';
                    $button .= '<a class="fad fa-ban   tx-20 accept-button mr-1 btn btn-danger bt_cus mb-1" href="javascript:;" onclick="rejectFunction(' . $row->id . ')"    data-property="' . $row->id . '" da title="Reject Contract"></a>';
                    return $button;
                })

                ->make(true);
        }
        return view('admin.user.request_contract');
    }

    public function completeUserTour(Request $request)
    {
        if ($request->ajax()) {
            $data = UserPropertyFullAmountDocument::with('user_rel', 'accounts_rel', 'user_property_rel', 'user_property_rel.prop_rel')->where('payment_check', '0')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('user', function (UserPropertyFullAmountDocument $tour) {
                    return (isset($tour->user_rel->name)) ? $tour->user_rel->name : 'Nil';
                })
                ->addColumn('user_contact', function (UserPropertyFullAmountDocument $tour) {
                    return (isset($tour->user_rel->phone)) ? $tour->user_rel->phone : 'Nil';
                })

                ->addColumn('property', function (UserPropertyFullAmountDocument $tour) {
                    if (isset($tour->user_property_rel->prop_rel)) {
                        if (isset($tour->user_property_rel->prop_rel->id)) {
                            return $tour->user_property_rel->prop_rel->property_name;
                            // $id = $tour->user_property_rel->property_id;
                            // $property_name = OwnerProperty::find($id);
                            // return isset($property_name->property_name)?$property_name->property_name:"---";
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('unit', function (UserPropertyFullAmountDocument $tour) {
                    if (isset($tour->user_property_rel->prop_rel)) {
                        if (isset($tour->user_property_rel->prop_rel->id)) {
                            // $id = $tour->user_property_rel->prop_rel->id;
                            // $property = OwnerProperty::find($id);
                            if (isset($tour->user_property_rel->prop_rel->builder_id)) {
                                $unit = OwnerProperty::where('id', $tour->user_property_rel->prop_rel->builder_id)->first();
                                return isset($unit->property_name)?$unit->property_name:"---";
                            } else {
                                return "NIL";
                            }
                        }
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('rent_amount', function (UserPropertyFullAmountDocument $tour) {
                    if ($tour->user_property_rel->prop_rel) {
                        return isset($tour->user_property_rel->prop_rel->rent)?$tour->user_property_rel->prop_rel->rent:0;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('net_amount', function (UserPropertyFullAmountDocument $tour) {
                    $service=0;
                    if ($tour->user_property_rel->prop_rel) {
                        if (isset($tour->accounts_rel)) {
                            if ($tour->accounts_rel->payment_type == 1) {
                                $service = isset($tour->accounts_rel->amount)?$tour->accounts_rel->amount:0;
                            }
                        }
                                $rent = $tour->user_property_rel->prop_rel->rent;
                                $security_deposit = $tour->user_property_rel->prop_rel->security_deposit;
                                $net_amount = $rent + $security_deposit + $service;
                                return isset($net_amount)?number_format($net_amount,2):0;

                    }
                    else {
                        return "NIL";
                    }
                })
                ->addColumn('advance_payment', function (UserPropertyFullAmountDocument $tour) {
                    if (isset($tour->accounts_rel)) {
                        $id = AdminAccount::select()->where('user_property_id')->where('payment_type', '2')->first();

                        return isset($id->amount)? number_format($id->amount,2):0;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('received_payment', function (UserPropertyFullAmountDocument $tour) {
                    if (isset($tour->accounts_rel)) {
                        $id = AdminAccount::select()->where('user_property_id')->where('payment_type', '2')->first();

                        return isset($id->amount)?number_format($id->amount,2):0;
                    } else {
                        return "NIL";
                    }
                })
                ->addColumn('balance_amount', function (UserPropertyFullAmountDocument $tour) {
                    return (isset($tour->user_property_rel->rent)) ? $tour->user_property_rel->rent : 'Nil';
                })
                ->addColumn('imageurl', function ($data) {
                    $url = $data->document;
                    return $url;
                })
                ->addColumn('action', function ($row) {
                    $button = '<a class="fad fa-check  tx-20 accept-button mr-1 btn btn-primary mb-1 bt_cus" href="javascript:;" onclick="AcceptFunction(' . $row->id . ')"  data-property="' . $row->id . '" da title="Accept"></a>';
                    $button .= '<a class="fad fa-ban   tx-20 accept-button mr-1 btn btn-danger mb-1 bt_cus" href="javascript:;" onclick="rejectFunction(' . $row->id . ')"   data-property="' . $row->id . '" da title="Reject"></a>';
                    return $button;
                })
                ->make(true);
        }
        return view('admin.user.approval_complete_amount');
    }

    public function approveCompleteUserTour($id)
    {
        if ($id) {
            UserPropertyFullAmountDocument::where('id', $id)->update(['payment_check' => '1']);

            $id = UserPropertyFullAmountDocument::find($id);

            if ($id->id) {
                if (AgentTrackingUserTour::where('user_property', $id->user_property_id)->update(['status' => 5])) {
                    $msg = 'Payment Approval status updated to completed';
                }
            } else {
                $msg = "Invaild Data";
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function rejectCompleteUserTour($id)
    {
        if ($id) {
            UserPropertyFullAmountDocument::where('id', $id)->update(['payment_check' => '2']);

            $msg = 'Payment Approval status updated to rejected';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
    public function rejectContract($id)
    {
        if ($id) {
            AgentRequestingContract::where('id', $id)->update(['upload_status' => '2']);

            $msg = 'status updated to rejected';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
}
