<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FaqDetails;
use Hash;
use Validator;
use File;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;

class FaqController extends Controller
{


    public function showFaq(Request $request)
    {
        $faq = FaqDetails::get();
        $data = array(
            'faq' => $faq
        );
        return view('admin.faq.index', $data);
    }
    public function editFaq($id)
    {
        $faq = FaqDetails::find($id);
        $data = array(
            'faq' => $faq
        );
        return view('admin.faq.edit', $data);
    }
    public function addFaq(Request $request)
    {
        $faq = FaqDetails::get();
        $data = array(
            'faq' => $faq
        );
        return view('admin.faq.add', $data);
    }

    public function addFaqDetails(Request $request)
    {
        $rules = [
            'english_question' => 'required',
            'arabic_question' => 'required',
            'english_answer' => 'required',
            'arabic_answer' => 'required',
        ];
        $messages = [
            'english_question.required' => 'Question is required',
            'arabic_question.required' => 'Question is required',
            'english_answer.required' => 'Answer is required',
            'arabic_answer.required' => 'Answer is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()]);
        }
        if ($request->id) {
            $faq = FaqDetails::find($request->id);
            $msg = 'Successfully updated Faq';
        } else {
            $faq = new FaqDetails();
            $msg = 'Successfully added Faqq';
        }
        $faq->english_question = $request->english_question;
        $faq->arabic_question = $request->arabic_question;
        $faq->english_answer = $request->english_answer;
        $faq->arabic_answer = $request->arabic_answer;
        try {
            $faq->save();
            return Redirect::to('show-faq');
        } catch (\Ecxeption $e) {
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }
    public function deleteFaq($id)
    {
        $faq = FaqDetails::findOrFail($id);
        $faq->delete();
        return Redirect::to('show-faq');
    }
}
