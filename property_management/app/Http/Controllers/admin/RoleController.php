<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

use DB;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{

    // function __construct()
    // {
    //      $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
    //      $this->middleware('permission:role-create', ['only' => ['create','store']]);
    //      $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
    //      $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {

        $users = Admin::with('permissions')->get();
        $roles = Role::where('type' ,'!=', 2 )->paginate(5);
        return view('admin.roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get();
        return view('admin.roles.create',compact('permission'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|unique:roles,name',
                'permission' => 'required',
            ]);
            $role_name = $request->input('name');

            $permissions = $request->input('permission');
            $count = Role::where('name', $role_name)->count();
            if ($count == 0) {
                $role = Role::create(['name' => $role_name]);
                if (!empty($permissions))
                {
                    $role->syncPermissions($permissions);
                }
                return redirect()->back()->with('success', 'Role created successfully');

            } else {
                return redirect()->back()->withInput()->with('error', 'Role already exist');


            }
        } catch (\Exception $e)
        {
            return redirect()->back()->withInput()->with('error', 'Something Went Wrong');
        }
    }

    public function addPerimission($id)
    {

        return view('admin.roles.add_permission')->with('id',$id);

    }


    public function saveAdminPerimission(Request $request)
    {

       try{
                $admin_id = $request->admin_id;
                $permissions = $request->input('permissions');
                $role_name = Str::random(10);
                $count = Role::where('name', $role_name)->count();
                if ($count == 0) {
                    $role = Role::create([
                                'name' => $role_name ,
                                'type' => '2'
                            ]);
                    if (!empty($permissions))
                    {
                        $role->syncPermissions($permissions);
                        $data = Admin::where('id', $admin_id)->first();
                        $data->role = $role->id ;
                        $data->assignRole($role->id);
                        $data->save();
                    }
                    return redirect()->back()->with('success', 'Successfully added!');

                } else {
                    return redirect()->back()->withInput()->with('error', 'Already Added');
                }
       }
       catch(\Exception $e)
       {
        return redirect()->back()->withInput()->with('error', 'Something Went Wrong');
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show(Request $request,$id)
    // {

    //     $admin_id = $request->id;
    //     return view('admin.roles.add_permission',$admin_id);

    //     // $role = Role::find($id);
    //     // $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
    //     //     ->where("role_has_permissions.role_id",$id)
    //     //     ->get();
    //     // return view('admin.roles.show',compact('role','rolePermissions'));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::select('id','name','parent')->get();
        $role = Role::with(['permissions'])->find($id);
        $role_permissions = [];
        foreach ($role->permissions as $role_perm) {
            $role_permissions[] = $role_perm->name;
        }
        return view('admin.roles.edit')->with(compact('role', 'role_permissions','permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',


        ]);
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
                        ->with('success','Role deleted successfully');

    }
}
