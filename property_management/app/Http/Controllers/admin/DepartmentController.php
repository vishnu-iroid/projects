<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

use App\Models\Department;

class DepartmentController extends Controller
{
    public function showDepartment(){
        $department = Department::paginate(10);
        return view('admin.department.department',['department' => $department]);
    }
    public function addDepartment(Request $request){
        $validation = Validator::make($request->all(),['name' => 'required|unique:departments,name'],['name.required' => 'designation name is required']);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()->all()]);
        }
        if($request->id){
            $department = Department::find($request->id);
            $msg = 'Successfully updated department';
        }else{
            $department = new Department;
            $msg = 'Successfully added department';
        }
        $department->name = $request->name;
        try{
            $department->save();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
    public function deleteDepartment($id){
        if($id){
            Department::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted department']);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
    public function viewDepartment($id){
        if($id){
            $dept = Department::find($id);
            return response()->json(['status'=>true,'response'=>$dept]);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
}
