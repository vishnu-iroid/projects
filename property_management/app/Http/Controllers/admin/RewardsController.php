<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CouponTypes;
use App\Models\OwnerProperty;
use App\Models\Rewards;
use Validator;
use DB;

class RewardsController extends Controller
{
    //Coupons section
    public function showCoupon(Request $request)
    {
        
        $coupon  = Rewards::with('property_rel')->paginate(10);
        $properties=OwnerProperty::get();
        $data = [
                    'coupon'  => $coupon,
                    'properties'=>$properties,
               ];
        return view('admin.rewards.coupon',$data);
    }
    public function addCoupon(Request $request)
    {
        $num = mt_rand(0, 999);
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $coupon_code = 'COUP'. $num . $randomString ;
        $todayDate = date('d/m/Y');
        $rules =  [
            'coupon_type'  => 'required',
            'coupon_value' => 'required|integer',
            'expiry_date'  => 'required|date:after_or_equal:'.$todayDate,
            'max_count'    => 'required|integer',
        ];
        $messages =  [
            'coupon_type.required'    => 'Enter Coupon Type',
            'coupon_value.required'   => 'Enter An Integer Value',
            'expiry_date.required'    => 'Enter An Expiry Date',
            'max_count.required'       => 'Enter maximum number of users allowed for this coupon',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        if($request->id){
            $coupon = Rewards::find($request->id);
            $msg  = "Coupon  updated Successfully" ;
        }else{
            $coupon = new Rewards;
            $msg  = "Coupon  added Successfully";
        }
        $coupon->coupon_code   = $coupon_code;
        $coupon->coupon_type   = $request->coupon_type;
        if($request->coupon_type == 0){
            $coupon->percentage   = $request->coupon_value;
        }else{
            $coupon->amount       = $request->coupon_value;
        }
        $coupon->expiry_date   = $request->expiry_date;
        $coupon->max_count     = $request->max_count;
        $coupon->status        = $request->status;
        $coupon->property_id=$request->property;
        try{
            DB::beginTransaction();
            $coupon->save();
            DB::commit();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
    public function editCoupon($id)
    {
        if($id){
            $coupon = Rewards::find($id);
            return response()->json(['status'=>true,'response'=> $coupon]); 
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
    public function deleteCoupon($id){
        if($id){
            $coupon = Rewards::where('id',$id)->delete();
            return response()->json(['status'=>true , 'response' => 'Coupon Deleted Successfully' ]);
        }else{
            return response()->json(['status'=>false , 'response' => 'Something went wrong']);
        }
    }
}
