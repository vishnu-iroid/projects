<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserDesiredProperty;
use App\Models\OwnerProperty;
use App\Models\AssignedUserDesiredProperty;
use DB;
use VAlidator;


class DesiredPropertyController extends Controller
{
    public function showDesiredProperty(Request $request){
        $user_desired_property = UserDesiredProperty::get();
        $data = array(
            'user_desired_property' => $user_desired_property
        );
        return view('admin.user_desired_property.index',$data);
    }
    public function assignDesiredPropertyDetails($id){
        $user_desired_property = UserDesiredProperty::where('id',$id)->first();
        $assigned_user_desired_property = AssignedUserDesiredProperty::where('desired_id',$id)->get();
        $property = OwnerProperty::with('assigned_user_desired_property')->select('id','property_reg_no','property_to','category','street_address_1','latitude','longitude',
                                                DB::raw('ifnull(selling_price,"") as selling_price'), DB::raw("6371 * acos(cos(radians(" . $user_desired_property->latitude . "))
                                                 * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $user_desired_property->longitude . "))
                                                 + sin(radians(" .$user_desired_property->latitude. ")) * sin(radians(latitude))) AS distance"))
                                                ->where([
                                                    ['property_to','=',$user_desired_property->property_to],
                                                    ['selling_price','>=',[$user_desired_property->min_price]],
                                                    ['selling_price','<=',[$user_desired_property->max_price]],
                                                    ['category','=',$user_desired_property->category],
                                                    ['type_id','=',$user_desired_property->type_id]
                                                ])
                                                ->get();
 
        $data = array(
            'user_desired_property' => $user_desired_property,
            'property' => $property,
            'assigned_user_desired_property' => $assigned_user_desired_property,
        );
        return view('admin.user_desired_property.assign_desired_property_details',$data);
    }
    public function saveToAssignedList(Request $request){
        $assigned_property_list = new AssignedUserDesiredProperty();
        $assigned_property_list->desired_id = $request->desired_property_id;
        $assigned_property_list->property_id = $request->assign_property_id;
        $assigned_property_list->status = 1;
        try{
            $assigned_property_list->save();
            return response()->json(['status'=>true,'response'=> 'successfully added']);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
    public function deleteAssignedList(Request $request){
        $delete_assigned_property_list =  AssignedUserDesiredProperty::where('property_id',$request->assign_property_id)->delete();
        dd($request->$request->assign_property_id);
        try{
            return response()->json(['status'=>true,'response'=> 'successfully deleted']);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
}
