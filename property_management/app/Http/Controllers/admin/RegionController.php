<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Pincode;

class RegionController extends Controller
{
    public function showRegion(){
        $countries = Country::paginate(10);
        $states  = State::with('country_rel')->paginate(10);
        $data = [
            'countries' => $countries,
            'states'  => $states,
        ];
        return view('admin.region.region',$data);
    }

    public function showPincode(){
        $pincodes = Pincode::with('city_rel')->paginate(10);
        $cities   = City::with('country_rel','state_rel')->paginate(10);
        $states   = State::get();
        $all_cities = City::get();
        $data = [
            'pincodes'=> $pincodes,
            'cities'  => $cities,
            'states'  => $states,
            'all_cities' => $all_cities,
        ];
        return view('admin.region.pincodecity',$data);
    }
    
    public function viewCountry($id){
        if(!$id){
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
        $country = Country::find($id);
        return response()->json(['status'=>true,'response'=>$country,'type'=>'country']);
    }

    public function addCountry(Request $request){
        $rules = [
            'country' => 'required',
        ];
        $messages = [
            'country.required' => 'country is required',
        ];
        $validation = Validator ::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()]);
        }
        try{
            $country       = new Country();
            $country->name = $request->country;
            $country->country_code = $request->country_code;
            $country->save();
            return response()->json(['status'=>true,'response'=>"Successfully edited country"]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }

    public function editCountry(Request $request){
        if(!$request->countryId){
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
        $validation = Validator::make($request->all(),['country_name'=>'required'],['country_name.required'=>'Country is required']);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()]);
        } 
        $country = Country::find($request->countryId);
        $country->name = $request->country_name;
        $country->country_code = $request->country_code;
        try{
            $country->save();
            return response()->json(['status'=>true,'response'=>"Successfully edited country"]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }

    public function addState(Request $request){
        $rules = [
            'state' => 'required',
            'state_country' => 'required',
        ];
        $messages = [
            'state_country.required' => 'country is required',
        ];
        $validation = Validator ::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()]);
        }
        if($request->stateId){
            $state = State::find($request->stateId);
            $msg = 'Successfully updated state';
        }else{
            $state = new State();
            $msg = 'Successfully added state';
        }
        $state->name = $request->state;
        $state->country = $request->state_country;
        try{
            $state->save();
            return response()->json(['status'=>true,'response'=>"Successfully added state"]);
        }catch(\Ecxeption $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }

    public function viewState($id){
        if(!$id){
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
        $state = State::find($id);
        return response()->json(['status'=>true,'response'=>$state,'type'=>'state']);
    }
    public function deleteState($id){
        if($id){
            State::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted State']); 
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }

    public function addCity(Request $request){
        $rules = [
            'city_name' => 'required',
            'city_state' => 'required',
        ];
        $messages = [
            'city_name.required' => 'city name is required',
            'city_state.required' => 'State is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()]);
        }
        $state = State::where('id',$request->city_state)->select('country')->first();
        if($request->cityId){
            $city = City::find($request->cityId);
            $msg = "Successfully updated city";
        }else{
            $city = new City();
            $msg = "Successfully added city";
        }
        $city->name = $request->city_name;
        $city->state = $request->city_state;
        $city->country = $state->country;
        try{
            $city->save();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }

    public function viewCity($id){
        if(!$id){
            return response()->json(['status'=>false,'response'=>"Something went wrong"]);
        }
        $city = City::find($id);
        return response()->json(['status'=>true,'response'=>$city,'type'=>'city']);
    }
    public function deleteCity($id){
        if($id){
            City::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted City']); 
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
    public function addPincodes(Request $request){
        $rules = [
            'pincode' => 'required',
            'city_pin' => 'required',
        ];
        $messages = [
            'pincode.required' => 'pincode is required',
            'city_pin.required' => 'city is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()]);
        }
        if($request->pincodeId){
            $pincode = Pincode::find($request->pincodeId);
            $pincode->pincode = $request->pincode;
            $pincode->city = $request->city_pin;
            try{
                $pincode->save();
                return response()->json(['status'=>true,'response'=>"Successfully updated pincode"]);
            }catch(\Exception $e){
                return response()->json(['status'=>true,'response'=>[$e->getMessage()]]);
            }
        }else{
            $array = explode(', ',$request->pincode);
            $insert = array();
            foreach($array as $key=>$dat){
                foreach($insert as $ins){
                    if($ins['pincode'] == $dat){
                        return response()->json(['status'=>false,'response'=>"pincode must be unique"]);
                    }
                }
                $insert[$key]['pincode'] = $dat;
                $insert[$key]['city'] = $request->city_pin;
                $insert[$key]['created_at'] = now();
                $insert[$key]['updated_at'] = now();
            }
            try{
                Pincode::insert($insert);
                return response()->json(['status'=>true,'response'=>"Successfully added pincodes"]);
            }catch(\Exception $e){
                return response()->json(['status'=>true,'response'=>[$e->getMessage()]]);
            }
        }
    }

    public function viewPincode($id){
        if(!$id){
            return response()->json(['status'=>false,'response'=>"Something went wrong"]);
        }
        $pincode = Pincode::find($id);
        return response()->json(['status'=>true,'response'=>$pincode,'type'=>'pincode']);
    }
    public function deletePin($id){
        if($id){
            Pincode::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted Pincode']); 
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
}
