<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use File;

use App\Models\Service;
use App\Models\ServiceProvider;

class ServiceController extends Controller
{
    public function showService()
    {
        $services = Service::paginate(10);
        $service_provider = ServiceProvider::with('service_rel')->paginate(10);;
        $services_list = Service::get();
        $data = [
            'services' => $services,
            'service_providers' => $service_provider,
            'services_list' => $services_list,
        ];
        return view('admin.services.services', $data);
    }

    public function addService(Request $request)
    {
        $rules = [
            'service_name' => 'required',
        ];
        $messages = [
            'service_name.required' => 'Service name is required',
        ];
        if ($request->serviceId) {
            $service = Service::find($request->serviceId);
            $msg = 'Updated service successfully';
            // 'floor_plans'       => 'nullable|array',
            // 'floor_plans.*'     => 'max:3000|mimes:jpeg,jpg,png',
            $rules['image'] = 'nullable|max:3000|mimes:jpeg,jpg,png';
            $rules['service_name'] = 'unique:services,service,' . $request->serviceId;
        } else {
            $service = new Service();
            $msg = 'Updated service successfully';
            $rules['image'] = 'mimes:jpeg,jpg,png';
            $rules['service_name'] = 'unique:services,service';
            $messages['image.required'] = 'image is required';
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        if ($request->file('image')) {
            if ($request->serviceId) {
                if (File::exists(public_path() . $request->current_image)) {
                    File::delete(public_path() . $request->current_image);
                }
            }
            $image = $request->file('image');
            $imageName = time() . rand() . '.' . $image->getClientOriginalName();
            $image->move(public_path('/uploads/services/'), $imageName);
            $service->image = '/uploads/services/' . $imageName;
        }
        $service->service = $request->service_name;
        try {
            $service->save();
            return response()->json(['status' => true, 'response' => "Successfully added service"]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'response' => $e->getMessage()]);
        }
    }

    public function viewService($id)
    {
        if (!$id) {
            return response()->json(['status' => false, 'response' => 'Something went wrong', 'type' => 'service']);
        }
        $service = Service::find($id);
        return response()->json(['status' => true, 'response' => $service, 'type' => 'service']);
    }
    public function deleteServices($id){
        if($id){
            Service::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted Service']);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }


    public function showProvider()
    {
        $services = Service::paginate(10);
        $service_provider = ServiceProvider::with('service_rel')->paginate(10);;
        $services_list = Service::get();
        $data = [
            'services' => $services,
            'service_providers' => $service_provider,
            'services_list' => $services_list,
        ];
        return view('admin.services.provider', $data);
    }


    public function addServiceProvider(Request $request)
    {
        $rules = [
            'provider' => 'required',
            'technician_name' => 'required',
            'phone' => 'required',
            'service_category' => 'required',
        ];
        $messages = [
            'provider' => 'provider is required',
            'technician_name' => 'technician name is required',
            'phone' => 'phone is required',
            'service_category' => 'service is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        if ($request->providerId) {
            $provider = ServiceProvider::find($request->providerId);
            $msg = "Successfully updated service provider";
        } else {
            $provider = new ServiceProvider();
            $msg = "Successfully added service provider";
        }
        $provider->name = $request->provider;
        $provider->technician_name = $request->technician_name;
        $provider->technician_phone = $request->phone;
        $provider->service = $request->service_category;
        if ($request->company) {
            $provider->company = $request->company;
            $provider->company_phone = $request->company_phone;
        } else {
            $provider->company = "";
            $provider->company_phone = "";
        }
        $provider->approved_by = auth()->user()->id;
        try {
            $provider->save();
            return response()->json(['status' => true, 'response' => $msg]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'response' => $e->getMessage()]);
        }
    }
    public function viewServiceProvider($id)
    {
        if (!$id) {
            return response()->json(['status' => false, 'response' => 'Something went wrong', 'type' => 'provider']);
        }
        $service = ServiceProvider::find($id);
        return response()->json(['status' => true, 'response' => $service, 'type' => 'provider']);
    }
}
