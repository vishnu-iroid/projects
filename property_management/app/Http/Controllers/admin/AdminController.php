<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

use App\Models\Admin;
use App\Models\AdminAccount;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Frequency;
use App\Models\Owner;
use App\Models\OwnerProperty;
use App\Models\Pincode;
use App\Models\UserProperty;
use Spatie\Permission\Models\Role;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
        $data = Owner::get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('status', function ($row) {
                if ($row->status == 1)
                    return  '<span class="badge rounded-pill bg-primary text-white"> Active </span>';
                else
                    return  '<span class="badge rounded-pill bg-danger text-white"> Inactive </span>';
            })
            ->addColumn('actions', function ($row) {

                if ($row->status == 1) {
                    return '<a class="btn btn-primary px-1 pt-0 pb-0 mr-1" href="javascript:0;"
            title="Add property"
            onclick="addPropertyFunction(' . $row->id . ')">Add Property</a>' .
                        '<a class="btn btn-info px-1 pt-0 pb-0 mr-1" href="javascript:0;"
            title="View property"
            onclick="viewPropertyFunction(' . $row->id . ')">View Property</a>' .
                        ' <a class="fad fa-edit tx-20  mr-1" href="javascript:0;"
                        onclick="EditOwnerFunction(' . $row->id . ')" title="Edit"></a>' .
                        ' <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus  mr-1" href="javascript:0;" data-admin=""
            title="Block" style=""
            onclick="blockFunction(' . $row->id . ',0)"> </a>';
                } else {
                    return     ' <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;"
                    onclick="EditOwnerFunction(' . $row->id . ')"title="Edit"></a>' .
                        '<a class="fad fa-unlock-alt tx-20 reject-button mr-1" href="javascript:0;"
               data-admin="" title="Unblock" style="color: green "
               onclick="blockFunction(' . $row->id . ',1)"></a>';
                }
            })

            ->rawColumns(['status', 'actions'])
            ->make(true);
    }

    public function showAdmins()
    {
        $admin   = Admin::orderBy('id', 'DESC')->where('role', '!=', '1')->paginate(10);
        $roles = Role::where('type', '!=', '2')->get();

        $country = Country::orderBy('id', 'DESC')->get();
        $state   = State::orderBy('id', 'DESC')->get();;
        $department  = Department::orderBy('id', 'DESC')->get();
        $designation = Designation::orderBy('id', 'DESC')->get();
        $countries=Country::get();
        $data = [
            'admin'       => $admin,
            'roles'       => $roles,
            'country'     => $country,
            'state'       => $state,
            'countries'       => $countries,
            // 'role_name'   => $role_name,
            'department'  => $department,
            'designation' => $designation,
        ];
        return view('admin.admins.showadmin', $data);
    }
    public function addAdmin(Request $request)
    {


        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'address' => 'required',
            'role' => 'required',
            'dept' => 'required',
            'des' => 'required',
            'status' => 'required',
        ];
        $messages = [
            'first_nam.required' => 'first name is required',
            'last_name.required' => 'last namerequired',
            'country.required' => 'country isrequired',
            'state.required' => 'state is required',
            'city.required' => 'city is required',
            'zipcode.required' => 'zipcode is required',
            'address.required' => 'address is required',
            'role.required' => 'role is required',
            'dept.required' => 'departments is required',
            'des.required' => 'designation is required',
            'status.required' => 'status is required',
        ];
        if (!$request->id) {
            $admin = new Admin;
            $rules['password'] = 'required';
            $rules['email'] = 'required|email|unique:admins,email';
            $rules['phone'] = 'required|unique:admins,phone';
            $messages['password.required'] = 'password is required';
            $messages['email.required'] = 'email is required';
            $messages['phone.required'] = 'phone is required';
            $admin->password = bcrypt($request->password);
            $msg = 'Successfully added Employee';
        } else {
            $admin = Admin::find($request->id);
            $rules['email'] = 'required|email|unique:admins,email,' . $request->id;
            $rules['phone'] = 'required|unique:admins,phone,' . $request->id;
            $messages['email.required'] = 'email is required';
            $messages['phone.required'] = 'phone is required';
            if ($request->password) {
                $admin->password = bcrypt($request->password);
            }
            $msg =  'Successfully updated Employee';
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        $admin->first_name = $request->first_name;
        $admin->last_name  = $request->last_name;
        $admin->country    = $request->country;
        $admin->state      = $request->state;
        $admin->city       = $request->city;
        $admin->zip_code   = $request->zipcode;
        $admin->address    = $request->address;
        $admin->email      = $request->email;
        $admin->phone      = $request->phone;
        $admin->country_code      = $request->countryCode;
        $admin->role       = $request->role;
        $admin->department = $request->dept;
        $admin->designation = $request->des;
        $admin->active     = $request->status;
        $role_id = $request->input('role');

        Role::findOrFail($role_id);
        $admin->assignRole($role_id);

        try {
            $admin->save();
            return response()->json(['status' => true, 'response' => $msg]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }
    public function viewAdmin($id)
    {
        if ($id) {
            $admin = Admin::where('id', $id)->first();
            return response()->json(['status' => true, 'response' => $admin]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }
    public function deleteEmployee($id){
    //    dd($id);
        if ($id) {
            $employee = Admin::find($id);
            $employee->delete();
            return response()->json(['status' => true, 'response' => 'Emplyee Deleted Successfully']);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }
    public function disabledAdmin($id, $action)
    {
        if ($id) {
            $admin = Admin::where('id', $id)->update(['active' => $action]);
            if ($action == 0) {
                $msg = 'Blocked admin successfully';
            } else {
                $msg = 'Unblocked admin successfully';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }

    public function showCommissionReport(Request $request)
    {
        // $data = UserProperty::select('user_properties.*', 'user_properties.id as p_id')
        //     ->join('admin_accounts as a', 'a.id', 'user_properties.property_id')
        //     ->with('user_rel','prop_rel')->get();
        //     dd($data);
        if ($request->ajax()) {
            $data = UserProperty::select('user_properties.*', 'user_properties.id as p_id')
                ->join('admin_accounts as a', 'a.property_id', 'user_properties.property_id')
                ->join('owner_properties as p', 'p.id', 'user_properties.property_id')
                ->where('occupied', '1')
                ->with('user_rel', 'prop_rel', 'user_property_bookings','prop_rel.owner_rel')->groupBy('p_id')->get();

           return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('tenant', function (UserProperty $property) {
                    return (isset($property->user_rel->name)) ? $property->user_rel->name : 'Nil';
                })
                ->addColumn('owner', function (UserProperty $property) {
                    return (isset($property->prop_rel->owner_rel->name)) ? $property->prop_rel->owner_rel->name : 'Nil';
                })
                ->addColumn('property', function (UserProperty $pro) {
                    return (isset($pro->prop_rel->property_name)) ? $pro->prop_rel->property_name : 'Nil';
                })
                ->addColumn('unit', function ($row) {
                    if ($row->prop_rel->builder_id > 0)
                    {
                        $building_image=OwnerProperty::where('id',$row->prop_rel->builder_id)->first();
                        return $building_image->property_name;
                    }
                })

                ->addColumn('management_fee',function(UserProperty $property){
                   if ($property->prop_rel->property_management_fee_type == 'percentage')
                   {
                   if ($property->prop_rel->property_to == '0')
                   {
                            // $rent = $property->rent;
                            $rent = $property->prop_rel->rent;
                            $rent_commssion_percentage = ($property->prop_rel->management_fee / 100) * $rent;
                            $rent_commssion_percentage_balnce = $rent - $rent_commssion_percentage;


                       return  $rent_commssion_percentage_balnce ? $rent_commssion_percentage_balnce : 'Nil' ;


                   }else{
                            $selling_price = $property->prop_rel->selling_price;
                            $selling_price_commssion_percentage = $property->prop_rel->management_fee * $selling_price;
                            $selling_price_commssion_percentage_balnce = $selling_price - $selling_price_commssion_percentage;
                     return    $selling_price_commssion_percentage_balnce ? $selling_price_commssion_percentage_balnce : 'Nil' ;

                    }
                }
               else{
                   if ($property->prop_rel->property_to == '0'){
                            // $rent = $property->rent;
                            $rent = $property->prop_rel->rent;
                            $rent_commssion_amount_balnce = $rent - $property->prop_rel->management_fee;

                        return $rent_commssion_amount_balnce ? $rent_commssion_amount_balnce : 'Nil' ;

                   }else{
                            $selling_price = $property->prop_rel->selling_price;
                            $selling_price_commssion_amount_balnce = $selling_price - $property->prop_rel->management_fee;
                         return $selling_price_commssion_amount_balnce ? $selling_price_commssion_amount_balnce : 'Nil' ;

                    }
                }
                })
                ->addColumn('collected', function($row){
                    if($row->id){
                     //----* SQL    SELECT admin_accounts.user_property_id,sum(admin_accounts.amount) as transactionamount FROM user_properties INNER JOIN admin_accounts ON user_properties.id=admin_accounts.user_property_id where admin_accounts.payment_type = 8; *---//
                        $collected = UserProperty::select('admin_accounts.user_property_id')
                                                    ->leftjoin('admin_accounts','user_properties.id','admin_accounts.user_property_id')
                                                    ->where('admin_accounts.payment_type', 8)
                                                    ->where('user_properties.id',$row->id)
                                                    ->sum('admin_accounts.amount');
                        return  $collected;
                    }
                    else
                    {
                        return '0' ;
                    }
                })
                ->addColumn('action', function ($row) {

                    return  '<a class="fad fa-eye tx-20  mr-1 btn btn-primary mb-1 bt_cus" href="' . route('viewPropertyManagementFee', $row->property_id) . '" data-property-id="' . $row->property_id . '"title="Edit"></a>';
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }
        return view('admin.report.admin_commisson_report');
    }
    public function viewPropertyManagementFee(Request $request, $id)
    {
        // dd($id);
        $properties = UserProperty::select('user_properties.*', 'user_properties.id as p_id', 'user_properties.rent as user_rent')
            ->join('admin_accounts as a', 'a.property_id', 'user_properties.property_id')
            ->join('owner_properties as p', 'p.id', 'user_properties.property_id')
            ->where('occupied', '1')
             ->where('user_properties.property_id',$id)
            ->with('user_rel', 'prop_rel', 'user_property_bookings','rent_rel','prop_rel.frequency_rel','prop_rel.admin_account_type')->groupBy('p_id')->get();
        // dd($properties);

        return view('admin.report.admin_commisson_report_details', compact('properties'));
    }
}
