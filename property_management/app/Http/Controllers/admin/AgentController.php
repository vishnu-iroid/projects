<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use Validator;
use File;

use App\Models\Agent;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Pincode;
use App\Models\AgentCashPayRequest;
use App\Models\AgentCashInHand;
use App\Models\BankDetails;

class AgentController extends Controller
{
    public function showAgent()
    {
        $agents   = Agent::orderBy('id', 'DESC')->paginate(10);
        $country  = Country::orderBy('id', 'DESC')->get();
        $state    = State::orderBy('id', 'DESC')->get();
        $city    = City::orderBy('id', 'DESC')->get();
        $bank      = BankDetails::orderBy('id','DESC')->get();
        $pincode    = Pincode::orderBy('id', 'DESC')->get();
        $data = [
            'agents'  => $agents,
            'country' => $country,
            'state'   => $state,
            'city'   => $city,
            'pincode'   => $pincode,
            'bank'     => $bank

        ];
        return view('admin.agents.agents', $data);
    }
    public function viewAgentDetails($id)
    {
        if ($id) {
            $agents   = $agent = Agent::find($id);
            $country  = Country::orderBy('id', 'DESC')->get();
            $state    = State::orderBy('id', 'DESC')->get();
            $city    = City::orderBy('id', 'DESC')->get();
            $pincode    = Pincode::orderBy('id', 'DESC')->get();
            $data = [
                'agents'  => $agents,
                'country' => $country,
                'state'   => $state,
                'city'   => $city,
                'pincode'   => $pincode
            ];
            return view('admin.agents.view_agent_details', $data);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
    public function addAgent(Request $request)
    {
        $rules = [
            'name'  => 'required',
            'country' => 'required',
            'state' => 'required',
            'city'  => 'required',
            'zipcode' => 'required',
            'address'  => 'required',
            'ifsc' => 'required',
            'bank_name'   => 'required',
            'branch_name' => 'required',
            'status' => 'required',
            // 'lat' => 'required',
            // 'lng' => 'required',
        ];
        $messages = [
            'name.required'  => 'name is required',
            'phone.required' => 'phone is required',
            'image.required' => 'image is required',
            'country.required' => 'country is required',
            'state.required' => 'state is required',
            'city.required'  => 'city is required',
            'zip_code.required' => 'zip code is required',
            'address.required'  => 'address is required',
            'ifsc.required' => 'ifsc is required',
            'bank_name.required'   => 'bank name is required',
            'branch_name.required' => 'branch name required',
            'status.required' => 'status is required',
            // 'lng.required' => 'Latitude is required',
            // 'lat.required' => 'Longitude is required',
        ];
        if ($request->id) {
            $agent = Agent::find($request->id);
            if ($request->file('image')) {
                $rules['image'] = 'nullable|max:3000|mimes:jpg,png,jpeg';
            }
            $rules['email'] = 'required|email|unique:agents,email,' . $request->id;
            $rules['phone'] = 'required|unique:agents,phone,' . $request->id;
            $rules['account_number'] = 'required|unique:agents,account_number,' . $request->id;
            $messages['account_number'] = 'Account number is required';
            $msg = 'Agent updated successfully';
        } else {
            $agent = new Agent;
            $rules['email'] = 'required|email|unique:agents,email';
            $rules['phone'] = 'required|unique:agents,phone';
            $rules['image'] = 'required|max:3000|mimes:jpg,png,jpeg';
            $rules['account_number'] = 'required|unique:agents,account_number';
            $messages['account_number'] = 'Account number is required';
            $msg = 'Agent added successfully';
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        if ($request->file('image')) {
            if ($request->current_image) {
                if (File::exists(public_path() . $request->current_image)) {
                    File::delete(public_path() . $request->current_image);
                }
            }
            $image = $request->file('image');
            $imagname = time() . rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/uploads/agents'), $imagname);
            $agent->image = '/uploads/agents/' . $imagname;
        }
        $agent->name = $request->name;
        $agent->email = $request->email;
        $agent->phone = $request->phone;
        $agent->country = $request->country;
        $agent->state = $request->state;
        $agent->city = $request->city;
        $agent->zip_code = $request->zipcode;
        $agent->password = Hash::make('agentpass');
        $agent->address = $request->address;
        $agent->target_amount = $request->target;
        $agent->account_number = $request->account_number;
        $agent->ifsc = $request->ifsc;
        $agent->bank_name = $request->bank_name;
        $agent->branch_name = $request->branch_name;
        $agent->status = $request->status;
        $agent->latitude = $request->lat ? round($request->lat,10) : 10.00084910;
        $agent->longitude = $request->lng ? round($request->lng, 10) : 76.30134207;
        $agent->approved_by = auth()->user()->id;
        try {
            if(!$request->id){
                //generate password
                $password_gen       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $str_length         = strlen($password_gen);

                $pass               = '';
                for ($i = 0; $i < 12; $i++) {
                    $n = rand(0, $str_length - 1);
                    $pass .= $password_gen[$n];
                }

                $new_password       = $pass;

                $agent->password    = Hash::make($new_password);


                $agent->save();


                $details = [
                    'title'     => 'Welcome Agent !!!',
                    'body'      =>  $new_password,
                ];

                \Mail::to($request->email)->send(new \App\Mail\GeneratePasswordMail($details));
            }else{
                $agent->save();
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }
    public function viewAgent($id)
    {
        if ($id) {
            $agent = Agent::find($id);
            return response()->json(['status' => true, 'response' => $agent]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
    public function listAgent($adminid)
    {
        if ($adminid) {
            $agent = Agent::select('id', 'name')->get();
            return response()->json(['status' => true, 'agent' => $agent, 'property_id' => $adminid]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
    public function disableAgent($id, $action)
    {
        if ($id) {
            $admin = Agent::where('id', $id)->update(['status' => $action]);
            if ($action == 0) {
                $msg = 'Blocked admin successfully';
            } else {
                $msg = 'Unblocked admin successfully';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function showAgentCashPayment(Request $request)
    {
        $cash = AgentCashPayRequest::with('agent_rel:id,name')->orderBy('created_at', 'desc')->paginate(10);
        $status = $request->status;
        if ($status == '')
        {
            $status = 0;
        }
        if ($status == 0)
        {
            $cash = AgentCashPayRequest::where('status', '0')->with('agent_rel:id,name,phone,target_amount')->orderBy('created_at', 'desc')->paginate(10);
        } elseif ($status == 1)
        {
            $cash = AgentCashPayRequest::where('status', '1')->with('agent_rel:id,name,phone,target_amount')->orderBy('created_at', 'desc')->paginate(10);
        } elseif ($status == 2)
        {
            $cash = AgentCashPayRequest::where('status', '2')->with('agent_rel:id,name,phone,target_amount')->orderBy('created_at', 'desc')->paginate(10);
        }
         elseif ($status == 3)
        {
            $cash = AgentCashPayRequest::where('status', '3')->with('agent_rel:id,name,phone,target_amount')->orderBy('created_at', 'desc')->paginate(10);
        }
        else {
            $cash = AgentCashPayRequest::where('status', $status)->with('agent_rel:id,name,phone')->orderBy('created_at', 'desc')->paginate(10);
        }
        $data = [
            'cash' => $cash,
            'status' =>  $status,
        ];
        return view('admin.agents.pay_request', $data);
    }

    public function acceptPaymentRequest($id, $action)
    {
        if ($id) {
            $Totalamount = AgentCashInHand::where([
                'agent_id' => $id,
                'status' => 1
            ])
                ->groupBy('agent_id')
                ->selectRaw('agent_cash_in_hands.*,sum(payment_amount) as amount ,agent_id')
                ->first();
            $requestedAmount = AgentCashPayRequest::where([
                'id' => $action,
                'status' => 0
            ])->select('amount', 'status')->first();

            if ($requestedAmount->amount ==  $Totalamount->amount) {
                $request = AgentCashPayRequest::where('id', $action)->update(['status' => 1]);
                $admin = AgentCashInHand::where('agent_id', $id)->update(['status' => 2]);
                $msg = 'Accepted Successfully';
                return response()->json(['status' => true, 'response' => $msg]);
            } else {
                return response()->json(['status' => true, 'response' => 'Something went wrong']);
            }
        }
    }

    public function rejectPaymentRequest($id)
    {
        if ($id) {
            $request = AgentCashPayRequest::where('id', $id)->update(['status' => 2]);
            $msg = 'Rejected Successfully';
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function revisePaymentRequest($id)
    {
        # code...
        if ($id) {

            $request = AgentCashPayRequest::where('id', $id)->update(['status' => 3]);
            $msg = 'Revise Successfully';
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
}
