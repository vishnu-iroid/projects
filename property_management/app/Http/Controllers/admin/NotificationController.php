<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use App\Models\AgentNotifications;
use App\Models\UserPropertyServiceRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class NotificationController extends Controller
{

   
   
    public function list(){
        try{
            $collection = Notifications::query();
            $collection->select('notifications.id','notification_heading','notification_text','name','notifications.status');
            $collection->leftJoin('users', 'users.id', '=', 'notifications.user_id');
            $data['notifications'] = $collection->simplepaginate(10);

            $collection = AgentNotifications::query();
            $collection->select('agent_notifications.id','notification_heading','notification_text','name','agent_notifications.status');
            $collection->leftJoin('agents', 'agents.id', '=', 'agent_notifications.user_id');
            $data['agent_notifications'] = $collection->simplepaginate(10);
            return view('admin.notifications.list',$data);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function test()
    {
        # code...
        return "sd";
    }
    public function viewNotificationDetails($id){
       
        if($id){
            
           $notifications=DB::table('notifications')
            ->where('notifications.id',$id)
            ->join('users','users.id','notifications.user_id')
            ->paginate(15);
            // dd($notifications);
           $data=['notifications' => $notifications];

            $notification=Notifications::find($id);
            $notification->status='read';
            $notification->save();
            return view('admin.notifications.notification',$data);
        }
        
    }
}
