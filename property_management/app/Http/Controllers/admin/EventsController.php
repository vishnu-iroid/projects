<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventImage;
use App\Models\EventPackages;
use App\Models\PackageFeatures;
use App\Models\PackageImages;
use App\Models\Admin;
use Validator;
use Hash;
use DB;
use Session;
use File;
use Auth;
use Illuminate\Support\Facades\Redirect;



class EventsController extends Controller
{
    public function showEvents(Request $request)
    {
        $events = Event::get();
        $data = array(
            'events' => $events
        );
        return view('admin.events.index', $data);
    }
    public function showAddPage(Request $request)
    {
        // $admins = Admin::get();
        $events = Event::get();
        $logged_admin_id   = Auth::user()->id;
        $data = array(
            'events' => $events,
            // 'admins' => $admins,
            'logged_admin_id' => $logged_admin_id,
        );
        return view('admin.events.add', $data);
    }
    public function showEditPage($id)
    {
        $admins = Admin::get();
        $event_packages = EventPackages::with('event_package_features')->where('event_id', $id)->get();

        //return count($event_packages);

        $events = Event::with('event_priority_image', 'event_packages')->find($id);


        $data = array(
            'events' => $events,
            'admins' => $admins,
            'event_packages' => $event_packages,
        );
        return view('admin.events.edit', $data);
    }
    public function addEvents(Request $request)
    {
        $rules = [
            'name'              => 'required',
            'event_date'        => 'required|date|after:yesterday',
            'admin_id'          => 'required',
            // 'latitude'          => 'required',
            // 'longitude'         => 'required',
            'short_description' => 'required',
            'description'       => 'required',
            'package_name'      => 'required',
        ];
        $messages = [
            'name.required'                 => 'Name is required',
            'event_date.required'           => 'Event Date is required',
            'admin_id.required'             => 'Admin id is required',
            // 'latitude.required'             => 'Latitude is required',
            // 'longitude.required'            => 'Longitude is required',
            'short_description.required'    => 'short description is required',
            'description.required'          => 'description is required',
            'package_name.required'         => 'Package name is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        if ($request->id) {
            $events = Event::find($request->id);
            $msg = 'Successfully updated Events';
        } else {
            $events = new Event();
            $msg = 'Successfully added Events';
        }
        $events->name               = $request->name;
        $events->admin_id           = $request->admin_id;
        $events->event_date         = $request->event_date;
        $events->latitude           = $request->latitude;
        $events->longitude          = $request->longitude;
        $events->short_description  = $request->short_description;
        $events->description        = $request->description;
        $events->status             = 1;
        try {
            $events->save();
            $request_id   = $events->id;
            $images = new EventImage();
            if ($request_id) {
                if ($request->image) {
                    $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                    request()->image->move(public_path('uploads/events'), $imageName);
                    $images->image = '/uploads/events/' . $imageName;
                } else if ($images->image) {
                    $images->image = $images->image;
                } else {
                    $images->image = "placeholder.jpg";
                }
            }
            $images->event_id = $request_id;
            $images->save();

            $event_id   = $request_id;
            $package_name = $request->package_name;
            $price = $request->price;
            $status = 1;
            for ($i = 0; $i < count($package_name); $i++) {
                $rows = [
                    'event_id' => $event_id,
                    'package_name' => $package_name[$i],
                    'price' => $price[$i],
                    'status' => $status
                ];
                DB::table('event_packages')->insert($rows);
            }
            $packageId = DB::getPdo()->lastInsertId();
            $package_id = $packageId;
            $feature_name = $request->feature_name;
            for ($i = 0; $i < count($feature_name); $i++) {
                $rows = [
                    'package_id' => $package_id,
                    'feature_name' => $feature_name[$i]
                ];
                DB::table('package_features')->insert($rows);
            }

            if ($packageId) {
                if ($request->file('package_image')) {
                    foreach ($request->file('package_image') as $key => $row) {
                        $filename = time() . rand() . '.' . $row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/package_images'), $filename);

                        $package_images[$key]['package_id']       = $packageId;
                        $package_images[$key]['package_image']    = '/uploads/package_images/' . $filename;
                        $package_images[$key]['created_at']       = now();
                        $package_images[$key]['updated_at']       = now();
                        DB::table('package_images')->insert($package_images);
                    }
                }
            }
            return Redirect::to('show-events');
        } catch (\Ecxeption $e) {
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }
    public function addSeperateFeature(Request $request)
    {
        $data = new PackageFeatures();
        $data->package_id = $request->n_package_id;
        $data->feature_name = $request->new_feature_name;
        $data->save();
        return back();
    }
    public function deleteEvents($id)
    {
        // $data = DB::table('events')
        //           ->leftJoin('event_images','events.id','=','event_images.event_id')
        //           ->leftJoin('event_packages','events.id','=','event_packages.event_id')
        //           ->where('events.id',$id);
        //         DB::table('event_images')->where('event_id',$id)->delete();
        //         DB::table('event_packages')->where('event_id',$id)->delete();

        // $data->delete();
        $event_packages = EventPackages::with('event_package_image', 'event_package_features')
            ->where('event_id', $id)
            ->first();
        $event_packages->event_package_image()->delete();
        $event_packages->event_package_features()->delete();
        $event_packages->delete();
        $event = Event::with('event_priority_image')->where('id', $id)->first();
        $event->event_priority_image()->delete();
        $event->delete();
        return Redirect::to('show-events');
    }
    public function deleteFeature($id)
    {
        $features = PackageFeatures::findOrFail($id);
        $features->delete();
        return back();
    }
    public function removeEventPackages($id)
    {
        $event_packages = EventPackages::findOrFail($id);
        $event_packages->event_package_image()->delete();
        $event_packages->event_package_features()->delete();
        $event_packages->delete();
        return back();
    }

    public function editEvents(Request $request)
    {
        //////////// old package details //////////////
        $event_id = $request->id;
        $old_pack_name = $request->oldpackage_name;
        $old_pack_ids = $request->package_id;
        $old_prices = $request->oldprice;
        $old_feature_name = $request->oldfeature_name;
        $old_feature_ids  = $request->oldfeature_id;
        $old_package_image =  $request->oldpackage_image;


        return $old_feature_ids;


        //////////// old package details //////////////
        $new_pack_name = $request->package_name;
        $new_pack_price = $request->price;
        $new_feature_name = $request->feature_name;



        exit();

        $rules = [
            'name'              => 'required',
            'event_date'        => 'required|date|after:yesterday',
            'admin_id'          => 'required',
            'latitude'          => 'required',
            'longitude'         => 'required',
            'short_description' => 'required',
            'description'       => 'required',
        ];
        $messages = [
            'name.required'                 => 'Name is required',
            'event_date.required'           => 'Event Date is required',
            'admin_id.required'             => 'Admin id is required',
            'latitude.required'             => 'Latitude is required',
            'longitude.required'            => 'Longitude is required',
            'short_description.required'    => 'short description is required',
            'description.required'          => 'description is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        $events = Event::find($event_id);
        $events->name               = $request->name;
        $events->admin_id           = $request->admin_id;
        $events->event_date         = $request->event_date;
        $events->latitude           = $request->latitude;
        $events->longitude          = $request->longitude;
        $events->short_description  = $request->short_description;
        $events->description        = $request->description;
        $events->status             = 1;
        $events->save();

        $images = EventImage::where('event_id', $event_id)->first();
        if ($event_id) {
            if ($request->image) {
                $imageName = time() . '.' . request()->image->getClientOriginalExtension();
                request()->image->move(public_path('uploads/events'), $imageName);
                $images->image = '/uploads/events/' . $imageName;
            } else if ($images->image) {
                $images->image = $images->image;
            } else {
                $images->image = "placeholder.jpg";
            }
        }
        $images->event_id = $event_id;
        $images->save();


        if ($old_pack_ids) {

            for ($j = 0; $j < count($old_pack_ids); $j++) {

                $rows = [
                    'event_id' => $event_id,
                    'package_name' => $old_pack_name[$j],
                    'price' => $old_prices[$j],
                    'status' => 1
                ];
                DB::table('event_packages')->where('id', $old_pack_ids[$j])->update($rows);


                if ($request->file('old_package_image')) {
                    foreach ($request->file('old_package_image') as $key => $row) {
                        $filename = time() . rand() . '.' . $row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/package_images'), $filename);

                        $package_images[$key]['package_id']       = $old_pack_ids[$j];
                        $package_images[$key]['package_image']    = '/uploads/package_images/' . $filename;
                        $package_images[$key]['created_at']       = now();
                        $package_images[$key]['updated_at']       = now();
                        DB::table('package_images')->where('package_id', $old_pack_ids[$j])->update($package_images);
                    }
                }
            }

            for ($i = 0; $i < count($old_feature_name); $i++) {
                $rows = [
                    'package_id' => $old_pack_ids[$i],
                    'feature_name' => $old_feature_name[$i]
                ];
                DB::table('package_features')->where('id', $old_feature_ids)->update($rows);
            }
        }


        for ($i = 0; $i < count($new_pack_name); $i++) {
            $rows = [
                'event_id' => $event_id,
                'package_name' => $new_pack_name[$i],
                'price' => $new_pack_price[$i],
                'status' => 1,
            ];
            DB::table('event_packages')->insert($rows);
        }


        for ($i = 0; $i < count($new_feature_name); $i++) {
            $rows = [
                'package_id' => $package_id,
                'feature_name' => $old_pack_ids[$i]
            ];
            DB::table('package_features')->insert($rows);
        }
    }
}
