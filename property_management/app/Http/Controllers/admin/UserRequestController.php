<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OwnerRequestController;
use App\Models\Admin;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Storage;
use Response;
use App\Models\Owner;
use App\Models\PropertyType;
use App\Models\OwnerProperty;
use App\Models\UserProperty;
use App\Models\Services;
use App\Models\ServiceProvider;
use App\Models\InsecptionTeam;
use App\Models\UserPropertyServiceRequests;
use App\Models\UserPropertyServiceRequestDocuments;
use App\Models\OwnerPropertyServiceRequestDocuments;
use App\Models\CancelUserServiceRequest;
use App\Models\CancelOwnerServiceRequest;
use App\Models\OwnerPropertyServiceRequests;
use App\Models\User;
use CreateOwnerPropertyDocumentsTable;
use App\Models\UserServiceRequestApprovel;

class UserRequestController extends Controller
{
    //
    public function showuserRequset(Request $request)
    {

        $inspection_Team = InsecptionTeam::where('status', '1')->get();
        $service_provider = ServiceProvider::all();

        $status = $request->status;
        if ($status == "") {
            $status = 0;
        }

        if ($request->status == 0) {
            $user_services_request = $details = UserPropertyServiceRequests::with(
                'user_related:id,name,phone',
                'service_related:id,service',
                'user_doc:id,document,request_id,document_type',
                'service_payemnt_doc:id,document,request_id,document_type,is_verified',
                'user_service_related:id,request_id,status',
                'prop_related','user_property_related.prop_rel'
            )->whereIn('status',[0,1,2,3])->orderBy('id','DESC')->paginate(10);

            // return $user_services_request->u;
        } elseif ($request->status == 1) {

            $user_services_request = OwnerPropertyServiceRequests::select(
                'owner_property_service_requests.id as o_id',
                'service_id',
                'date',
                'time',
                'owner_property_service_requests.status',
                'owner_property_service_requests.description',
                'owner_properties.property_name',
                'owner_property_service_requests.inspection_team',
                'owner_property_service_requests.service_provider'
            )
                ->with('service_related:id,service','service_payemnt_doc:id,document,request_id,document_type,is_verified','property_related')
                ->join('owner_properties', 'owner_properties.id', 'owner_property_service_requests.owner_property_id')
                ->orderBy('owner_property_service_requests.date', 'DESC')
                // ->groupBy('owner_property_service_requests.id')
                ->whereIn('owner_property_service_requests.status',[0,1,2,3])
                ->orderBy('owner_property_service_requests.id','DESC')
                ->paginate(10);
        }

        // return $status;

        $data = [
            'request'  => $user_services_request,
            'inspection_team' => $inspection_Team,
            'service_provider' => $service_provider,
            'status'   =>  $status
           
        ];

        return view('admin.owner.viewServiceRequeset', $data);
    }


    public function userRequestDetails($id)
    {

        $details = UserPropertyServiceRequests::where('id', $id)
            ->with(
                'user_related:id,name,phone',
                'service_related:id,service',
                'user_doc:id,document,request_id',
                'user_service_related:id,request_id,status',
                'prop_related:id,property_name,property_reg_no'
            )->first();


        //return $details;


        $data = [

            'datas' => $details
        ];
        return view('admin.user.service_request_details', $data);
    }


    public function completedRequest(Request $request)
    {


        $status = $request->status;
        if ($status == "") {
            $status = 0;
        }
        if ($request->status == 0) {
            $user_services_request = UserPropertyServiceRequests::select(
                'user_property_service_requests.id',
                'service_id',
                'date',
                'time',
                'user_property_service_requests.status',
                'owner_properties.property_name',
                'user_property_service_requests.send_owner_approval',
                'user_property_service_requests.inspection_team',
                'user_property_service_requests.description'
            )
                ->with('service_related:id,service', 'user_doc:id,document,document_type,request_id')
                ->join('user_properties', 'user_properties.id', 'user_property_id')
                ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
                ->where('user_property_service_requests.status', '3')
                ->orderBy('user_property_service_requests.date', 'DESC')
                ->groupBy('user_property_service_requests.id')
                ->get();

            //return $user_services_request;
        } elseif ($request->status == 1) {
            $user_services_request = OwnerPropertyServiceRequests::select(
                'owner_property_service_requests.id as o_id',
                'service_id',
                'date',
                'time',
                'owner_property_service_requests.status',
                'owner_property_service_requests.description',
                'owner_properties.property_name',
                'owner_property_service_requests.inspection_team'
            )
                ->with('service_related:id,service', 'owner_doc:id,document,request_id')
                ->join('owner_properties', 'owner_properties.id', 'owner_property_service_requests.owner_property_id')
                ->where('owner_property_service_requests.status', '3')
                ->orderBy('owner_property_service_requests.date', 'DESC')
                ->groupBy('owner_property_service_requests.id')
                ->get();
        }
        $data = [
            'request'  => $user_services_request,
            'status'   =>  $status
        ];
        return view('admin.owner.viewServiceRequesetCompleted', $data);
    }

    public function cancelledRequest(Request $request)
    {

        $status = $request->status;
        if ($status == "") {
            $status = 0;
        }

        if ($request->status == 0) {
            $user_services_request = UserPropertyServiceRequests::select(
                'user_property_service_requests.id',
                'service_id',
                'date',
                'time',
                'user_property_service_requests.status',
                'owner_properties.property_name',
                'user_property_service_requests.send_owner_approval',
                'user_property_service_requests.inspection_team',
                'user_property_service_requests.description'
            )
                ->with('service_related:id,service', 'user_doc:id,document,request_id')
                ->join('user_properties', 'user_properties.id', 'user_property_id')
                ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
                ->where('user_property_service_requests.status', '4')
                ->orderBy('user_property_service_requests.date', 'DESC')
                ->groupBy('user_property_service_requests.id')
                ->get();
        } elseif ($request->status == 1) {
            $user_services_request = OwnerPropertyServiceRequests::select(
                'owner_property_service_requests.id as o_id',
                'service_id',
                'date',
                'time',
                'owner_property_service_requests.status',
                'owner_property_service_requests.description',
                'owner_properties.property_name',
                'owner_property_service_requests.inspection_team'
            )
                ->with('service_related:id,service', 'owner_doc:id,document,request_id')
                ->join('owner_properties', 'owner_properties.id', 'owner_property_service_requests.owner_property_id')
                ->where('owner_property_service_requests.status', '4')
                ->orderBy('owner_property_service_requests.date', 'DESC')
                ->groupBy('owner_property_service_requests.id')
                ->get();
        }
    
        $data = [
            'request'  => $user_services_request,
            'status'   =>  $status
        ];

        return view('admin.owner.viewServiceRequesetCancelled', $data);
    }

    public function approvalSent($id, $action)
    {
        if ($id) {
            $admin = UserPropertyServiceRequests::where('id', $id)->update(['send_owner_approval' => 1]);

            $requestDetails = UserPropertyServiceRequests::where('id', $id)
                ->select('user_property_service_requests.*')
                ->first();
            $user_request = UserServiceRequestApprovel::where('request_id',$requestDetails->id)->first();
            if(!$user_request){
                if($requestDetails->user_property_related){
                    if($requestDetails->user_property_related->user_property_related){
                        $approvel = new UserServiceRequestApprovel;
                        $approvel->request_id = $requestDetails->id;
                        $approvel->owner_id = $requestDetails->user_property_related->user_property_related->owner_id;
                        $approvel->status = '0';
                        $approvel->save();

                        $msg = 'Approval Request Send  successfully';
                        $status = true;
                    }else{
                        $msg = 'No property related to this user';
                        $status = false;
                    }
                }else{
                    $msg = "User property doesn't exist";
                    $status = false;
                }
            }else{
                $msg = 'Request already been sent';
                $status = false;
            }


            return response()->json(['status' => $status , 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function assignUserRequestInspection($id, $memberid)
    {
        if ($id) {

            $admin = UserPropertyServiceRequests::where('id', $id)->update(['inspection_team' => $memberid]);

            $msg = 'Assign Inspection Team successfully';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function addrequestEstimate(Request $request)
    {
        // dd($request->all());
        if ($request) {

            $admin = UserPropertyServiceRequests::where('id', $request->request_id)->update(['status' => '1']);
            $doc = new UserPropertyServiceRequestDocuments;
            if ($request->file('image')) {
                if ($request->current_image) {
                    if (File::exists(public_path() . $request->current_image)) {
                        File::delete(public_path() . $request->current_image);
                    }
                }
                $image = $request->file('image');
                $imagname = time() . rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('/uploads/users/estimate-document/'), $imagname);
                $doc->document = '/uploads/users/estimate-document/' . $imagname;
            }
             $doc->document_type = '2';
            //  $doc->document_type = 1;
            $doc->request_id =  $request->request_id;
            // $doc->owner_amount=$request->owner_amount;
            // $doc->customer_amount=$request->customer_amount;
            $doc->save();
            $msg = 'Inspection  successfully';
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function paymentCompleted($id, $provider)
    {

        if ($id) {

            $admin = UserPropertyServiceRequests::where('id', $id)->update(['service_provider' => $provider]);

            $msg = 'Service Provider Assigned Successfully';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }


    public function assignOwnerRequestInspection($id, $memberid)
    {
        if ($id) {

            $admin = OwnerPropertyServiceRequests::where('id', $id)->update(['inspection_team' => $memberid]);

            $msg = 'Assign Owner Inspection Team successfully';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }


    public function addownerrequestEstimate(Request $request)
    {

        if ($request->file('file')) {

            $admin = OwnerPropertyServiceRequests::where('id', $request->ownerrequest_id)->update(['status' => '1']);
            $doc = new OwnerPropertyServiceRequestDocuments;
            if ($request->file('file')) {
                if ($request->owner_current_image) {
                    if (File::exists(public_path() . $request->owner_current_image)) {
                        File::delete(public_path() . $request->owner_current_image);
                    }
                }
                $image = $request->file('file');
                $imagname = time() . rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('/uploads/users/estimate-document/'), $imagname);
                $doc->document = '/uploads/users/estimate-document/' . $imagname;
                $doc->document_type = '2';
                $doc->request_id =  $request->ownerrequest_id;
                // $doc->owner_amount=$request->owner_amount;
                // $doc->customer_amount=$request->customer_amount;
                $doc->save();
                $msg =  "Estimate Send successfully";
                return response()->json(['status' => true, 'response' => $msg]);
            } else {
                return response()->json(['status' => true, 'response' => 'Something went wrong']);
            }
        }
    }

    public function ownerPaymentCompleted($id, $provider)
    {
        if ($id) {

            $admin = OwnerPropertyServiceRequests::where('id', $id)->update(['service_provider' => $provider]);

            $msg = 'Service Provider Assigned successfully';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }




    public function downloadfile($filename)
    {

        $pathToFile = UserPropertyServiceRequestDocuments::where('request_id', $filename)->first();
        $path = $pathToFile->document;
        $file_path = dirname($path);
        $file = basename($path);
        return response()->download(storage_path($file_path . '/' . $file));
    }


    public function markServiceAsCompleted($id)
    {
        if ($id) {

            $admin = OwnerPropertyServiceRequests::where('id', $id)->update(['status' => 3]);

            $msg = 'Service status updated to completed';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

    public function markUserServiceAsCompleted($id)
    {
        if ($id)
        {
            $admin = UserPropertyServiceRequests::where('id', $id)->update(['status' => 3]);
            $msg = 'Service status updated to completed';
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }


    public function requestForCancellation(Request $request)
    {

        // $cash = CancelUserServiceRequest::with('request_rel','user_property_service_requests.service_related:id,service','user_property_service_requests.user_related:id,name')->orderBy('created_at', 'desc')->paginate(10);
        $cash = CancelUserServiceRequest::paginate(10);

        $status = $request->status;
        if ($status == '') {
            $status = 0;
        }
        if ($status == 0) {
            $cash = CancelUserServiceRequest::where('admin_approve', '0')->orderBy('created_at', 'desc')->paginate(10);
        } elseif ($status == 1) {
            $cash = CancelOwnerServiceRequest::where('admin_approve', '0')->orderBy('created_at', 'desc')->paginate(10);
        }

        $data = [
            'request' => $cash,
            'status' => $status,
        ];


        return view('admin.owner.cancel_request_list', $data);
    }


    public function approveCancelRequest(Request $request)
    {
        if ($request->request_id) {
            if($request->user_type == 1){
                $request_cancel = CancelOwnerServiceRequest::where('id', $request->request_id)->first();
                if($request_cancel){
                    $request_cancel->admin_approve = 1;
                    $request_cancel->save();
                    OwnerPropertyServiceRequests::where('id',$request_cancel->request_id)->update(['status' => 4]);
                }


            }else{
                $request_cancel = CancelUserServiceRequest::where('id', $request->request_id)->first();
                if($request_cancel){
                    $request_cancel->admin_approve = 1;
                    $request_cancel->save();
                    UserPropertyServiceRequests::where('id',$request_cancel->request_id)->update(['status' => 4]);
                }
            }

            $msg = 'Cancel request approved successfully';

            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }


    public function showServiceRequest(Request $request,$id,$status_type){
        if($id){
            $inspection_Team = InsecptionTeam::where('status', '1')->get();
            $service_provider = ServiceProvider::all();
            $status = $status_type;

            if ($status == "")
            {
                $status = 0;
            }

            if ($status_type == 0) {
                $user_services_request  = UserPropertyServiceRequests::
                where('id',$id)
                ->with(
                    'user_related:id,name,phone',
                    'service_related:id,service',
                    'user_doc:id,document,request_id,document_type',
                    'service_payemnt_doc:id,document,request_id,document_type,is_verified',
                    'user_service_related:id,request_id,status',
                    'prop_related:id,property_name,property_reg_no'
                )->whereIn('status',[0,1,2,3])->first();
            }
            if ($status_type == 1) {
                // $user_services_request = OwnerPropertyServiceRequests::where('id',$id)
                //     ->with('service_related:id,service','property_related:id,property_name,property_reg_no','service_payemnt_doc:id,document,request_id,document_type,is_verified')
                //     ->whereIn('owner_property_service_requests.status',[0,1,2,3])
                //    ->first();

                   $user_services_request = OwnerPropertyServiceRequests::select(
                    'owner_property_service_requests.id as o_id',
                    'service_id',
                    'date',
                    'time',
                    'owner_property_service_requests.status',
                    'owner_property_service_requests.description',
                    'owner_properties.property_name',
                    'owner_property_service_requests.inspection_team',
                    'owner_property_service_requests.service_provider'
                )
                ->where('owner_property_service_requests.id',$id)
                    ->with('service_related:id,service','property_related:id,property_name,property_reg_no','service_payemnt_doc:id,document,request_id,document_type,is_verified')
                    ->join('owner_properties', 'owner_properties.id', 'owner_property_service_requests.owner_property_id')
                    ->whereIn('owner_property_service_requests.status',[0,1,2,3])
                    ->first();
            }

            $data = [
                'request'  => $user_services_request,
                'inspection_team' => $inspection_Team,
                'service_provider' => $service_provider,
                'status'   =>  $status
            ];
            return response()->json(['status' => true, 'response' => $data]);
        }
        else{
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }

}
