<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\OwnerProperty;
use App\Models\Owner;
use App\Models\Country;
use App\Models\State;
use App\Models\PropertyType;
use App\Models\PropertyTypeDetailsContent;
use App\Models\AmenitiesCategory;
use App\Models\Amenity;
use App\Models\OwnerPropertyDocument;
use App\Models\OwnerPropertyAmenity;
use App\Models\OwnerPropertyDetail;
use App\Models\Frequency;

use DB;
use Auth;
use Validator;

class OwnerBuildingController extends Controller
{
    public function ownerbuildings($id)
    {
        $owner_id   = $id;
        try{
            $owner_buildings    = OwnerProperty::where('owner_id',$owner_id)->where('is_builder',1)->paginate(10);
            $owner_details      = Owner::where('id',$owner_id)->first();
            $countries          = Country::orderBy('id','DESC')->get();
            $states             = State::orderBy('id','DESC')->get();
            $frequency          = Frequency::all();
            $types              = PropertyType::orderBy('id','DESC')->where('category',0)->get();
            if(count($types)>0){
                $amenities_listing = $this->availableAmenities($types[0]->id);
                $details = $this->availableDetails($types[0]->id);
            }else{
                $amenities_listing = array();
                $details = array();
            }
            $data = [
                'building'     => $owner_buildings,
                'owner_details' => $owner_details,
                'countries' => $countries,
                'states'  => $states,
                'amenities_listing' => $amenities_listing,
                'types'   => $types,
                'details' => $details,
                'frequency' =>$frequency
            ];
            return view('admin.owner.building', $data);

        }catch(\Exception $e){
            return ['status'=>400,'response'=>$e->getMessage()];
        }
    }

    public function availableAmenities($type)
    {
        $amenities_listing = [];
        $base_url          = url('/');
        $amenities_listing = AmenitiesCategory::join('property_type_amenity_contents','amenities_categories.id','=','property_type_amenity_contents.amenity_category_id')
                                        ->where('property_type_amenity_contents.type_id',$type)
                                        ->select('amenities_categories.id','amenities_categories.name')
                                        ->orderBy('sort_order','ASC')
                                        ->distinct()
                                        ->get();
        foreach($amenities_listing as $row){
            $data = Amenity::join('property_type_amenity_contents','amenities.id','=','property_type_amenity_contents.amenity_id')
                        ->where('property_type_amenity_contents.type_id',$type)
                        ->where('amenities.amenity_category_id',$row->id)
                        ->select('amenities.id','amenities.name',DB::raw('CONCAT("' . $base_url . '",image) AS image'))
                        ->get();
            $row->amenities = $data;
        }
        return $amenities_listing;
    }

    public function availableDetails($type)
    {
        $details = [];
        $details = PropertyTypeDetailsContent::join('details','details.id','=','property_type_details_contents.detail_id')
                                        ->where('property_type_details_contents.type_id',$type)
                                        ->select('detail_id','name','sort_order','placeholder')
                                        ->orderBy('details.sort_order','ASC')
                                        ->get();
        return $details;
    }

    public function addBuilding(Request $request)
    {
        $property   = new OwnerProperty;
        $maxid      = OwnerProperty::max('property_reg_no');
        if($maxid){
            $exid = explode('-',$maxid)[1] + 1;
            $property->property_reg_no = '#REG'.time().'-'.$exid;
        }else{
            $property->property_reg_no = '#REG'.time().'-'.'100';
        }
        $property->is_builder       = 1;
        $property->occupied         = NULL;
        $property->furnished        = NULL;
        $property->owner_id             = $request->building_owner_id;
        $property->property_name        = $request->building_name?$request->building_name:null;
        $property->property_to          = $request->property_to;
        if($request->category){
            $property->category             = 0;
        }else{
            $property->category             = 1;
        }
        $property->street_address_1     = $request->address1;
        $property->street_address_2     = $request->address2 ? $request->address2 : "";
        $property->country              = $request->property_country;
        $property->city                 = $request->property_city;
        $property->state                = $request->property_state;
        $property->zip_code             = $request->property_zipcode;
        $property->description          = $request->description?$request->description:null;
        $property->type_id              = $request->type_id?$request->type_id : null;
        $property->longitude            = $request->longitude? $request->longitude : 76.30134207;
        $property->latitude             = $request->latitude ? $request->latitude  : 10.00084910; 
        try{
            DB::beginTransaction();
            $property->save();
            $savebuilder = array();

            if($request->file('building_images')){
                foreach($request->file('building_images') as $key=>$row){
                    $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'),$imagename);
                    $savebuilder[$key]['property_id'] = $property->id;
                    $savebuilder[$key]['document']    = '/uploads/property/images/'.$imagename;
                    $savebuilder[$key]['type']        = '0';
                    $savebuilder[$key]['owner_id']    = Auth::user()->id;
                    $savebuilder[$key]['created_at']  = now();
                    $savebuilder[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savebuilder);
            }
            

            DB::commit();
            return ['status'=>200,'response'=>'Building Added Successfully','status'=> 1];
        }catch(\Exception $e){
            DB::rollback();
            return ['status'=>400,'response'=>$e->getMessage()];
        }
    }


    public function addBuildingApartment(Request $request)
    {
        $rules = [
            'building_id' => 'required',
        ];
        $messages = [
            'building_id.required' => 'Building is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }

        try{
            $building_id        = $request->building_id;
            $building_details   = OwnerProperty::where('id',$building_id)->first();
            if($building_details){
                $property   = new OwnerProperty;
                $maxid      = OwnerProperty::max('property_reg_no');
                if($maxid){
                    $exid = explode('-',$maxid)[1] + 1;
                    $property->property_reg_no = '#REG'.time().'-'.$exid;
                }else{
                    $property->property_reg_no = '#REG'.time().'-'.'100';
                }
                $property->owner_id             = $building_details->owner_id;
                $property->builder_id           = $building_details->id;
                $property->property_name        = $request->property_name?$request->property_name:null;
                $property->property_to          = $building_details->property_to;
                $property->category             = $building_details->category;
                $property->street_address_1     = $building_details->street_address_1;
                $property->street_address_2     = $building_details->street_address_2 ? $building_details->street_address_2 : null;
                $property->country              = $building_details->country;
                $property->city                 = $building_details->city;
                $property->state                = $building_details->state;
                $property->zip_code             = $building_details->zip_code;
                $property->description          = $request->description?$request->description:null;
                $property->furnished            = $request->furnished;
                $property->frequency            = $request->frequency? $request->frequency : null;
                $property->status               = 1;
                $property->contract_owner       = '1';
                $property->rent                 = $request->min_rent?$request->min_rent:null;
                $property->selling_price        = $request->min_sellingprice?$request->min_sellingprice:null;
                $property->mrp                  = $request->max_sellingprice?$request->max_sellingprice:null;
                $property->owner_amount         = $request->owner_amount ? $request->owner_amount  :  '0';
                $property->expected_amount      = $request->expected_amount? $request->expected_amount : null;
                $property->type_id              = $building_details->type_id;
                $property->longitude            = $request->longitude? $request->longitude : $building_details->longitude;
                $property->latitude             = $request->latitude ? $request->latitude : $building_details->latitude;
                $property->is_builder           = 1;
                $property->occupied             = $request->occupied?$request->occupied:0;
                DB::beginTransaction();
                $property->save();
                $savedoc = array(); $savefloor= array(); $savevideo= array(); $amenities= array();

                if($request->file('images')){
                    foreach($request->file('images') as $key=>$row){
                        $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/images'),$imagename);
                        $savedoc[$key]['property_id']   = $property->id;
                        $savedoc[$key]['document']      = '/uploads/property/images/'.$imagename;
                        $savedoc[$key]['type']          = '0';
                        $savedoc[$key]['owner_id']      = $building_details->owner_id;
                        $savedoc[$key]['created_at']    = now();
                        $savedoc[$key]['updated_at']    = now();
                    }
                    OwnerPropertyDocument::insert($savedoc);
                }
                if($request->file('floor_plans')){
                    foreach($request->file('floor_plans') as $key=>$row){
                        $imagename = time().rand().'.'.$row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/floor_plans'),$imagename);
                        $savefloor[$key]['property_id'] = $property->id;
                        $savefloor[$key]['document']    = '/uploads/property/floor_plans/'.$imagename;
                        $savefloor[$key]['type']        = '2';
                        $savefloor[$key]['owner_id']    = $building_details->owner_id;
                        $savefloor[$key]['created_at']  = now();
                        $savefloor[$key]['updated_at']  = now();
                    }
                    OwnerPropertyDocument::insert($savefloor);
                }
                if($request->file('videos')){
                    foreach($request->file('videos') as $key=>$row){
                        $videoname = time().rand().'.'.$row->getClientOriginalExtension();
                        $row->move(public_path('/uploads/property/videos'),$videoname);
                        $savevideo[$key]['property_id'] = $property->id;
                        $savevideo[$key]['document']    = '/uploads/property/videos/'.$videoname;
                        $savevideo[$key]['type']        = '1';
                        $savevideo[$key]['owner_id']    = $building_details->owner_id;
                        $savevideo[$key]['created_at']  = now();
                        $savevideo[$key]['updated_at']  = now();
                    }
                    OwnerPropertyDocument::insert($savevideo);
                }
                if($request->amenities){
                    foreach($request->amenities as $key=>$row){
                        $category_id = Amenity::where('id',$row)->pluck('amenity_category_id');
                        foreach($amenities as $row6){
                            if($row6['property_id'] == $property->id && $row6['amenity_id'] == $row && $row6['amenities_category_id'] == $category_id[0] && $row6['amenities_category_id'] == $building_details->owner_id){
                                DB::rollback();
                                return ['status'=>400,'response'=>'Something went wrong'];
                            }
                        }
                        $amenities[$key]['property_id']             = $property->id;
                        $amenities[$key]['amenity_id']              = $row;
                        $amenities[$key]['amenities_category_id']   = $category_id[0];
                        $amenities[$key]['owner_id']                = $building_details->owner_id;
                        $amenities[$key]['created_at']              = now();
                        $amenities[$key]['updated_at']              = now();
                    }
                    OwnerPropertyAmenity::insert($amenities);
                }

                if($request->detailskey){
                    $max            = sizeof($request->detailskey);
                    $detailsvalue   = $request->detailsvalue;
                    $detailkeyvalue = $request->detailskey;

                    for($i=0; $i < $max; $i++){

                        if($request->detailskey[$i]!=""){
                            //dd($detailkeyvalue[$i]);
                            $details               = new OwnerPropertyDetail();
                            $details['property_id']= $property->id;
                            $details['detail_id']  = $detailkeyvalue[$i];
                            $details['value']      = $detailsvalue[$i];
                            $details['owner_id']   = $building_details->owner_id;
                            $details['created_at'] = now();
                            $details['updated_at'] = now();
                            $details->save();
                        }
                    }
                }
                DB::commit();
                return response()->json(['status' => true, 'response' => "Successfully added apartment"]);
            }else{
                return ['status'=>200,'response'=>'Invalid Building'];
            }
        }catch(\Exception $e){
            DB::rollback();
            return ['status'=>400,'response'=>$e->getMessage()];
        }
    }

    public function addPropertyUnits($id)
    {
        $building_details   = OwnerProperty::where('id',$id)->where('is_builder',1)->first();
        $frequency          = Frequency::all();
        if($building_details){
            $added_unit_count   = OwnerProperty::where('builder_id',$building_details->id)->count();
            if($added_unit_count > 0){
                // $unit_num           = $building_details->unit_num - $added_unit_count;
                $unit_num           =   $added_unit_count;
                if($unit_num > 0){
                    $unit_num           = $unit_num;
                }
            }else{
                $unit_num           = $building_details->unit_num;
            }
            $property_type      = $building_details->type_id;
            $property_to        = $building_details->property_to;
            $property_id        = $building_details->id;

            $amenities_listing = $this->availableAmenities($property_type);
            $property_details  = $this->availableDetails($property_type);
        }else{
            $property_type = 0; $property_to = ""; $property_id = ""; $unit_num           = 0;
            $amenities_listing = ""; $property_details  = "";
        }   

        $data =[
            'unit_count' => $unit_num,
            'frequency' => $frequency,
            'details' => $property_details,
            'amenities_listing' => $amenities_listing,
            'property_to'       => $property_to,
            'property_id'       => $property_id
        ];
        return view('admin.property.add-unit', $data);
    }
}
