<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\LegalModel;
use Hash;
use Validator;
use Session;
use File;
use Illuminate\Support\Facades\Redirect;

class LegalController extends Controller
{


    public function showLegalInformation(Request $request)
    {
        $legal = LegalModel::first();
        $data = array(
            'legal' => $legal
        );
        return view('admin.legal.index', $data);
    }
    public function showUserLegalInformation(Request $request)
    {
        $lang = $request->lang;
        if ($lang == 1) {
            $legal = LegalModel::select('english_description AS description')->first();
        } else {
            $legal = LegalModel::select('arabic_description AS description')->first();
        }
        $data = array(
            'legal' => $legal,
            'lang' => $lang,
        );
        return view('admin.legal.user_legal_view', $data);
    }
    public function addLegal(Request $request)
    {
        $legal =  LegalModel::first();
        if(!$legal){
            $legal =  new LegalModel();
        }
        if ($request->english_description) {
            $legal->english_description = $request->english_description;
        }
        if ($request->arabic_description) {
            $legal->arabic_description = $request->arabic_description;
        }
        try {
            if ($request->id) {
                Session::flash('message', "Updated successfully");
            } else {
                Session::flash('message', "Saved successfully");
            }
            Session::flash('alert-class', 'tx-success');
            Session::flash('alert-icon', 'ion-ios-checkmark-outline');
            $legal->save();
        } catch (\Exception $e) {
            // print_R($e->getMessage());exit;
            Session::flash('message', "Something went wrong");
            Session::flash('alert-class', 'tx-danger');
            Session::flash('alert-icon', 'ion-ios-close-outline');
        }
        return Redirect::to('show-legal');
    }
}
