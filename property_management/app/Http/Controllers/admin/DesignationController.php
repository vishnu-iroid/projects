<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

use App\Models\Designation;

class DesignationController extends Controller
{
    public function showDesignation(){
        $designation = Designation::paginate(10);
        return view('admin.department.designation',['designation' => $designation]);
    }
    public function addDesignation(Request $request){
        // print_r($request->all());exit;
        $validation = Validator::make($request->all(),['name' => 'required'],['name.required' => 'department name is required']);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()->all()]);
        }
        if($request->id){
            $des = Designation::find($request->id);
            $msg = 'Successfully updated designation';
        }else{
            $des = new Designation; 
            $msg = 'Successfully added designation';
        }
        $des->name = $request->name;
        try{
            $des->save();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
    public function deleteDesignation($id){
        if($id){
            Designation::where('id',$id)->delete();
            return response()->json(['status'=>true,'response'=>'Successfully deleted designation']); 
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
    public function viewDesignation($id){
        if($id){
            $des = Designation::find($id);
            return response()->json(['status'=>true,'response'=>$des]); 
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
}
