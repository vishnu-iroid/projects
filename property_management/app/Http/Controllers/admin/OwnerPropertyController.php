<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\OwnerProperty;

class OwnerPropertyController extends Controller
{
    
    public function showProperty(Request $request){
        $property = OwnerProperty::orderBy('id','DESC')->paginate(10);
        return view('admin.property.property',['property' => $property]); 
    }
}
