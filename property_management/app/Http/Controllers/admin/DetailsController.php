<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use File;
//models
use App\Models\Detail;

class DetailsController extends Controller
{
    public function showDetails(){
       $details = Detail::paginate(10);
       return view('admin.property.details',['details' => $details]);
    } 
    public function addDetail(Request $request){
        $rules = [
            'name' => 'required',
            'placeholder' => 'required',
            'sortorder' => 'required',
        ];
        $messages = [
            'name' => 'Detail name i srequired',
            'placeholder' => 'Placeholder is required',
            'sortorder' => 'Sort order is required',
        ];
        if($request->id){
            $rules['icon'] = 'nullable|max:2000|mimes:png';
            $msg = 'Detail updated successfully';
            $detail = Detail::find($request->id);
        }else{
            $rules['icon'] = 'required|max:2000|mimes:png';
            $messages['icon.required'] = 'Icon is required';
            $msg = 'Detail added successfully';
            $detail = new Detail();
        }
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()]);
        }
        if($request->icon){
            if($request->id){
                if(File::exists(public_path('uploads/details/').$detail->icon)){
                    File::delete(public_path('uploads/details/').$detail->icon);
                }
            }
            $image = $request->icon;
            $imagename = time().rand().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('/uploads/details/'),$imagename);
            $detail->icon = '/uploads/details/'.$imagename;
        }
        $detail->name = $request->name;
        $detail->placeholder = $request->placeholder;
        $detail->sort_order = $request->sortorder;
        try{
            $detail->save();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }
    public function viewDetail($id){
        $detail = Detail::find($id);
        return response()->json(['status'=>true,'response'=>$detail]);
    }
}
