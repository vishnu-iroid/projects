<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserFeedbacks;
use Hash;
use Validator;
use File;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;

class FeedbackController extends Controller
{
    public function showFeedbacks(Request $request){
        $feedbacks = UserFeedbacks::with('user_rel')->paginate(10);
        $data = array(
            'feedbacks' => $feedbacks
        );
        return view('admin.feedbacks.index',$data);
    }
    public function editFeedback($id){
        $feedbacks = UserFeedbacks::find($id);
        $data = array(
            'feedbacks' => $feedbacks
        );
        return view('admin.feedbacks.edit',$data);
    }
}
