<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\AdminAccount;
use Illuminate\Http\Request;
use App\Models\Tenant;
use App\Models\UserProperty;
use App\Models\UserPropertyServiceRequests;
use DateInterval;
use DatePeriod;
use DateTime;

class TenantController extends Controller
{

    public function showTenants(Request $request)
    {
        $status = $request->status;

        if ($status == "") {
            $status == "0";
        }
        if ($request->status == 0) {
        // $tenant = UserProperty::with('user_rel:id,name,email,phone', 'prop_rel', 'contract_rel')->where('status',0)->paginate(10);
        $tenant = UserProperty::with('user_rel:id,name,email,phone', 'prop_rel', 'contract_rel','prop_rel.frequency_rel')        
        ->where('status',0)->paginate(10);
        // return  $tenant;

        }elseif($request->status  == 1){
            $tenant = UserProperty::with('user_rel:id,name,email,phone', 'prop_rel', 'contract_rel')->where('status',1)->paginate(10);
        }
        $data = [
            'tenant' => $tenant,
            'status' => $status
        ];
        return view('admin.tenant.tenant', $data);




    }

    public function residentsDetails($id)
    {
        if($id)
        {
            $tenant = UserProperty::with('user_rel:id,name,email,phone', 'prop_rel', 'contract_rel')->where('id',$id)->first();
            $accounts = AdminAccount::with('prop_rel')->where('user_property_id' , $id)->get();
            $service_request = UserPropertyServiceRequests::with('service_related')->where('user_property_id', $id)->get();
        }

        $data = ['tenant'   => $tenant,
                 'accounts' => $accounts,
                 'request'  => $service_request,

                ];

        return view('admin.tenant.show_tenant',$data);
    }

}
