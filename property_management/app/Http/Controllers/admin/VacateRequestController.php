<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserPropertyVacateRequest;

class VacateRequestController extends Controller
{
    public function showVacateRequest(Request $request){
        $status = $request->status;
        if($status == ""){
            $status = 0;
        }
        $vacate_request = UserPropertyVacateRequest::orderBy('id', 'DESC')
                                                    ->where('status',$status)
                                                    ->paginate(10);
        $data = array(
            'vacate_request' => $vacate_request,
        );
        if ($request->status == 0) {
            $vacate_request = UserPropertyVacateRequest::orderBy('id', 'DESC')
                                                       ->where('status', 0)
                                                       ->with('user_property_related')
                                                       ->paginate(10);
                                                    // ->get();
                                                    //    dd( $vacate_request);
            $data['vacate_request'] = $vacate_request;
            return view('admin.vacate_request.index', $data);
        } elseif ($request->status == 1) {
            $vacate_request = UserPropertyVacateRequest::orderBy('id', 'DESC')
                                                       ->where('status', 1)
                                                       ->paginate(10);
            $data['vacate_request'] = $vacate_request;
            return view('admin.vacate_request.approved', $data);
        } else {
            return view('admin.vacate_request.rejected', $data);
        }
    }
    //Approve function
    public function approveRequest($id)
    {
        if($id){
            if (UserPropertyVacateRequest::where('id', $id)->update(['status' => 1])) {
                $msg = 'Approved successfully';
            } else {
                $msg = 'Something went wrong';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }    
    //Reject function
    public function rejectRequest($id)
    {
        if($id){
            if (UserPropertyVacateRequest::where('id', $id)->update(['status' => 2])) {
                $msg = 'Rejected successfully';
            } else {
                $msg = 'Something went wrong';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }    
    //Pending function
    public function pendingRequest($id)
    {
        if($id){
            if (UserPropertyVacateRequest::where('id', $id)->update(['status' => 0])) {
                $msg = 'Added to Pending successfully';
            } else {
                $msg = 'Something went wrong';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }    
}
