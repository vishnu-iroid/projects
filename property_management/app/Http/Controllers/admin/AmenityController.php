<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

use App\Models\Amenity;
use App\Models\AmenitiesCategory;

class AmenityController extends Controller
{
    public function showAmenity(){
        $category = AmenitiesCategory::orderBy('sort_order','ASC');
        $amenity = Amenity::orderBy('id','DESC')->with('category:id,name')->paginate(10);
        $data = [ 
            'amenity' => $amenity, 
            'category' => $category->paginate(10),
            'category_list' => $category->get(),
        ];
        return view('admin.property.amenity',$data);
    }
    public function addAmenityCategory(Request $request){
        $validation = Validator::make($request->all(),['category' => 'required']);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()->all()]);
        }
        if($request->categoryid){
            $category = AmenitiesCategory::find($request->categoryid);
            $msg = "Category updated successfully";
        }else{
            $category = new AmenitiesCategory;
            $msg = "Category saved successfully";
        }
        $category->name = $request->category;
        $category->sort_order = $request->sort ? $request->sort : 0;
        try{
            $category->save();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>$e->getMessage()]);
        }
    }
    public function viewAmenityCategory($id){
        if($id){
            $category = AmenitiesCategory::find($id);        
            return response()->json(['status'=>true,'response'=>$category]);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
    public function addAmenity(Request $request){
        $rules= [
            'amenityname' => 'required',
            'amenity_category' => 'required',
        ];
        $messgaes = [
            'amenityname.required' => 'Amenity name is required',
            'amenity_category.required' => 'Category is required',
        ];
        if($request->amenityId){
            $amenity = Amenity::find($request->amenityId);
            $msg = "Amenity updated successfully";
            $rules['image'] = 'nullable|max:1000|mimes:jpeg,jpg,png'; 
        }else{
            $amenity = new Amenity;
            $msg = "Amenity saved successfully";
            $rules['image'] = 'required|max:1000|mimes:jpeg,jpg,png';
        }
        $validation = Validator::make($request->all(),$rules,$messgaes);
        if($validation->fails()){
            return response()->json(['status'=>false,'response'=>$validation->errors()->all()]);
        }
        if($request->image){
            $image = $request->image;
            $imageName = time().rand().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('/uploads/amenities/'),$imageName);
            $amenity->image = '/uploads/amenities/'.$imageName;
        }
        $amenity->name = $request->amenityname;
        $amenity->amenity_category_id = $request->amenity_category;
        try{
            $amenity->save();
            return response()->json(['status'=>true,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>$e->getMessage()]);
        }
    }
    public function viewAmenity($id){
        if($id){
            $amenity = Amenity::find($id);        
            return response()->json(['status'=>true,'response'=>$amenity]);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
}
