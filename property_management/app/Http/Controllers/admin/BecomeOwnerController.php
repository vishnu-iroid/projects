<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BecomeOwner;
use Validator;
use Auth;
use DB;

use Yajra\DataTables\Facades\DataTables;
class BecomeOwnerController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
      
            $data   = BecomeOwner::get();

            // $data   = OwnerProperty::where('contract_owner', '1')->orderBy('id', 'DESC');
            return DataTables::of($data)
           
                ->addIndexColumn()
                ->addColumn('actions', function ($row) { 
                    return '<a href="'.route('showOwner').'" class="" href="javascript:0;" title="Add as Owner"></a>'.
                    '<a class="fad fa-eye tx-20  mr-1 viewBecomeAnOwner btn btn-primary bt_cus " data-becomeowner-id="' . $row->id . '" href="javascript:0;" title="View" ></a>';
                    
                  
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
    }
    public function showBecomeOwnerList(Request $request){
        $become_owner_list = BecomeOwner::get();
        $data = array(
            'become_owner' => $become_owner_list,
        );
        return view('admin.become_owner.index',$data);
    }
    public function becomeAnOwnerView($id){
        if($id){
        $become_owner_list = DB::table('become_owners as b')
        ->select('b.*','u.name as username')
        ->join('users as u','b.user_id','u.id')
        ->where('b.id',$id)
        ->first();
        return response()->json(['status'=>true,'response'=>$become_owner_list]);
    }else{
        return response()->json(['status'=>false,'response'=>'Something went wrong']);
      }
    }
}
