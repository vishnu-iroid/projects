<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\User;
use App\Models\Owner;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\UserOtp;
use App\Models\Pincode;

class UserController extends Controller
{
    public function showUser(){
        $user      = User::orderBy('id','DESC')->paginate(10);
        $countries = Country::orderBy('id','DESC')->get();
        $states    = State::orderBy('id','DESC')->get();
        $data = [
            'user' => $user,
            'countries' => $countries,
            'states' => $states,
        ];
        return view('admin.user.user',$data);
    }
    public function addUser(Request $request){
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'status' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
        ];
        if($request->id){
            $user = User::find($request->id);
            $rules['email'] = 'required|email|unique:users,email,'.$request->id;
            $rules['phone'] = 'required|numeric|unique:users,phone,'.$request->id;
            $msg = "Successfully updated user";
        }else{
            $user = new User;
            $rules['email'] = 'required|email|unique:users,email';
            $rules['phone'] = 'required|numeric|unique:users,phone,'.$request->id;
            $msg = "Successfully added user";
        }
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>0,'response'=>$validation->errors()->all()]);
        }
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->isOwner = '0';
        $user->country = $request->country;
        $user->state = $request->state;
        $user->city = $request->city;
        $user->zip_code = $request->zipcode;
        $user->address = $request->address;
        $user->status = $request->status;
        $user->latitude  = $request->lat ?  $request->lat : "";
        $user->longitude = $request->lng ?  $request->lng : "";
        $user->referral_code    = $this->generateUserReferalCode();

        try{
            $user->save();
            if(!$request->id){
                $user_otp = new UserOtp();
                $user_otp->user_id = $user->id;
                $user_otp->otp = '1001';
                $user_otp->save();
            }
            return response()->json(['status'=>1,'response'=>$msg]);
        }catch(\Exception $e){
            return response()->json(['status'=>0,'response'=>[$e->getMessage()]]);
        }
    }
    public function disableUser($id,$action){
        if($id){
            $user = User::where('id',$id)->update(['status' => $action]);
            if($action == 0){
                $msg = 'Blocked user successfully';
            }else{
                $msg= 'Unblocked user successfully';
            }
            return response()->json(['status'=>true , 'response' => $msg ]);
        }else{
            return response()->json(['status'=>false , 'response' => 'Something went wrong']);
        }
    }
    public function viewUser($id){
        if($id){
            $user = User::where('id',$id)->first();
            return response()->json(['status'=>true,'response'=>$user]);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
    public function makeUserOwner($id){
        if(!$id){
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
        $user = User::where('id',$id)->first();
        if(!$user){
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
        $user->isOwner = '1';
        try{
            $user->save();
            $owner = new Owner;
            $owner->name = $user->name;
            $owner->email = $user->email;
            $owner->phone = $user->phone;
            $owner->address = $user->address;
            $owner->country = $user->country;
            $owner->state = $user->state;
            $owner->city = $user->city;
            $owner->zip_code = $user->zip_code;
            $owner->status = '1';
            $owner->rental_properties = '0';
            $owner->sale_properties = '0';
            $owner->profile_completed = '0';
            $owner->save();
            return response()->json(['status'=>true,'response'=>'Succeddfully moved to owner']);
        }catch(\Exception $e){
            return response()->json(['status'=>false,'response'=>[$e->getMessage()]]);
        }
    }

    public function generateUserReferalCode()
    {
        $num = mt_rand(0, 999);
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $referral_code = 'SAJID' . $num . $randomString ;
        try {
            $user_data = User::where('referral_code', $referral_code)->first();
            if (!empty($user_data)) {
                $this->generateUserReferalCode();
            } else {
                return $referral_code;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
}
