<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Validator;
use Cookie;
use DB;

use App\Models\Admin;
use  Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    protected $guard = 'admin';

    public function showLogin(){

        // if(Auth()->guard('admin')->user()){
        //     return redirect('/dashboard');
        // }else{
        //     return view('admin.login');
        // }
          return view('admin.login');
    }

    public function login(Request $request){
        $validation = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if($validation->fails())
        {
            return response()->json(['status'=>false,'response'=>$validation->errors()->all()]);
        }
        $remember_me = $request->has('remember_me') ? true : false;
        if(auth()->guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password], $remember_me)){
            $admin = Auth()->guard('admin')->user();
                       Session::put('name',$admin->first_name.' '.$admin->last_name);
            Session::put('email',$admin->email);
            Session::put('id',$admin->id);
            $admin=Admin::find($admin->id);
            $new_sessid   = Session::getId(); //get new session_id after user sign in
            $last_session = Session::getHandler()->read($admin->sessionId); // retrive last session
        
            if ($last_session) {
                if (Session::getHandler()->destroy($admin->sessionId)) {
                    // session was destroyed
                }
            }
            $admin->sessionId = $new_sessid;
            $admin->save();
            $token = $admin->createToken('property')->accessToken;             
            Admin::where('id',$admin->id)->update(['token' => $token]);
            return response()->json(['status'=>true,'response'=>'Logged in successfully']);
        }else{
            return response()->json(['status'=>false,'response'=>['Invalid Email/Password']]);
        }
    }

    public function logout(Request $request){
        Auth()->logout();
        Session::flush();
        return redirect('/');
    }
}
