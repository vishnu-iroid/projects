<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\AboutUs;

use Hash;
use Validator;
use Session;
use File;
use Illuminate\Support\Facades\Redirect;

class AboutController extends Controller
{

    public function showAbout(Request $request)
    {
        $about = AboutUs::first();
        $data = array(
            'about' => $about
        );
        return view('admin.about.index', $data);
    }
    public function showUserAbout(Request $request)
    {
        $lang = $request->lang;
        if ($lang == 1) {
            $about = AboutUs::select('english_description AS description', 'image')->first();
        } else {
            $about = AboutUs::select('arabic_description AS description', 'image')->first();
        }
        $data = array(
            'about' => $about,
            'lang' => $lang,
        );
        return view('admin.about.user_about_view', $data);
    }
    public function addAbout(Request $request)
    {
        try{
            $abouts = AboutUs::count();
                if($abouts=='0'){
                    $about = new AboutUs();
                }else{
                    $about = AboutUs::where('id',$request->id)->first();
                }
                if($request->image){
                    $image = $request->image;
                    $imagename = '/uploads/about/'.time().rand(1,100).'.'.$image->getClientOriginalExtension();
                    $image->move(public_path('/uploads/about'),$imagename);
                    $about->image = $imagename;
                }
                if ($request->english_description) {
                    $about->english_description = $request->english_description;
                }
                if ($request->arabic_description) {
                    $about->arabic_description = $request->arabic_description;
                }
                $about->save();
                 return Redirect::to('show-about' );
        }catch(\Exception $e){
            return response()->json(['status'=>0,'response'=>[$e->getMessage()]]);
        }
        
    }
}
