<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Hash;
//service
use App\Http\Services\OwnerService;
//models

use App\Models\Owner;
use App\Models\Country;
use App\Models\State;
use App\Models\PropertyType;
use App\Models\Frequency;
use App\Models\AmenitiesCategory;
use App\Models\Amenity;
use App\Models\PropertyTypeDetailsContent;
use App\Models\OwnerProperty;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class OwnerController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('owners')->orderBy('id','desc');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    if ($row->status == 1)
                        return  '<span class="badge rounded-pill bg-primary text-white"> Active </span>';
                    else
                        return  '<span class="badge rounded-pill bg-danger text-white"> Inactive </span>';
                    })
                ->addColumn('actions', function ($row) {
                    if ($row->status == 1) {
                        return '<a class="btn btn-primary mb-1 bt_cus fad fa-plus" href="javascript:0;"
                                title="Add property"
                                onclick="addPropertyFunction(' . $row->id . ')"></a>' .
                                                ' <a class="fad fa-eye tx-20 view-button btn btn-primary mb-1 bt_cus"
                                            href="' . route('viewProperty', $row->id) . '"
                                            data-payment="{{ $row->id }}" title="View Property"></a>' .
                                                ' <a class="fad fa-edit tx-20 btn btn-primary mb-1 bt_cus" href="javascript:0;"
                                            onclick="EditOwnerFunction(' . $row->id . ')" title="Edit"></a>' .
                                                ' <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus  mr-1 btn btn-primary mb-1 bt_cus" href="javascript:0;" data-admin=""
                                title="Block" 
                                onclick="blockFunction(' . $row->id . ',0)"> </a>';
                    } else {
                              return ' <a class="fad fa-edit tx-20 edit-button" href="javascript:0;"
                                    onclick="EditOwnerFunction(' . $row->id . ')"title="Edit"></a>' .
                                            '<a class="fad fa-unlock-alt tx-20 reject-button" href="javascript:0;"
                                    data-admin="" title="Unblock" style="color: green "
                                    onclick="blockFunction(' . $row->id . ',1)"></a>';
                            }
                })
                ->rawColumns(['status', 'actions'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('search'))) {
                        $instance->where(function ($w) use ($request) {
                            $owner = $request->get('search');
                            $w->where('name', 'LIKE', "%$owner%");
                        });
                    }
                })
                ->make(true);
        }
    }


    public function showOwner(Request $request)
    {
        $data    = (new OwnerService())->getOwner($request);
        return view('admin.owner.owner', $data);
    }
    public function addOwner(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'status' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'nationalid' => 'required',
            'lat' => 'nullable',
            'lng' => 'nullable',
            'account_number' => 'required',
            'branch' => 'required',
            'bank_name' => 'required',
            'ifsc' => 'required',
        ];
        $messages = [
            'account_number.required' => 'account number is required',
        ];
        if ($request->id) {
            $user = Owner::find($request->id);
            $rules['email'] = 'required|email|unique:owners,email,' . $request->id;
            $rules['phone'] = 'required|numeric|unique:owners,phone,' . $request->id;
            $msg = "Successfully updated owner";
        } else {
            $user = new Owner;
            $rules['email'] = 'required|email|unique:owners,email';
            $rules['phone'] = 'required|numeric|unique:owners,phone';
            $msg = "Successfully added owner";
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 0, 'response' => $validation->errors()->all()]);
        }
        $user->name                 = $request->name;
        $user->email                = $request->email;
        $user->phone                = $request->phone;
        $user->address              = $request->address;
        $user->country              = $request->country;
        $user->state                = $request->state;
        $user->city                 = $request->city;
        $user->zip_code             = $request->zipcode;
        $user->whatsapp_no          = $request->whatsapp_number;
        $user->password             = Hash::make('ownerpass');
        $user->status               = $request->status;
        $user->status               = $request->status;
        $user->rental_properties    = '0';
        $user->sale_properties      = '0';
        $user->profile_completed    = '0';
        $user->account_number       = $request->account_number;
        $user->branch_name          = $request->branch;
        $user->bank_name            = $request->bank_name;
        $user->ifsc                 = $request->ifsc;
        $user->latitude             = $request->lat ? round($request->lat, 10) : 10.00084910;
        $user->longitude            = $request->lng ? round($request->lng, 10) : 76.30134207;
        $user->profile_image        = '/assets/backdrops/profile-dummy.jpg';
        try {
            if (!$request->id) {
                //generate password
                $password_gen       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $str_length         = strlen($password_gen);

                $pass               = '';
                for ($i = 0; $i < 12; $i++) {
                    $n = rand(0, $str_length - 1);
                    $pass .= $password_gen[$n];
                }
                $new_password       = $pass;
                $user->password    = Hash::make($new_password);
                // national id upload
                $exid  = 000;
                if ($request->file('nationalid')) {
                    $random_pass  = mt_rand(1000000000, 9999999999);
                    $id_file_name = '';
                    if ($request->file('nationalid')) {
                        $path = public_path('uploads/property/contracts/');
                        $file          = $request->file('nationalid');
                        $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                        $file_name     = '/uploads/owners/nationalid/' . $id_file_name;
                        $file->move($path, $id_file_name);
                    }
                    $user->national_id        = $file_name;
                }

                $user->save();
                $details = [
                    'title'     => 'Welcome Owner !!!',
                    'body'      =>  $new_password,
                ];
               // \Mail::to($request->email)->send(new \App\Mail\GeneratePasswordMail($details));
            } else {
                $user->save();
            }
            return response()->json(['status' => 1, 'response' => $msg]);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'response' => [$e->getMessage()]]);
        }
    }
    public function viewOwner($id)
    {
        if ($id) {
            $owner = Owner::where('id', $id)->first();
            return response()->json(['status' => true, 'response' => $owner]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }
    public function disableowner($id, $action)
    {
        if ($id) {
            $admin = Owner::where('id', $id)->update(['status' => $action]);
            if ($action == 0) {
                $msg = 'Blocked owner successfully';
            } else {
                $msg = 'Unblocked owner successfully';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
    }
    public function getAvailableAmenities($id)
    {
        if (!$id) {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
        $data = (new OwnerService())->listAmenities($id);
        return response()->json(['status' => true, 'response' => $data]);
    }
    public function getDetails($type)
    {
        if (!$type) {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
        $data = (new OwnerService())->availableDetails($type);
        return response()->json(['status' => true, 'response' => $data]);
    }

    public function showOwnerProperty(Request $request, $id)
    {

        //return $id ;
        $owner     = Owner::orderBy('id', 'DESC')->paginate(10);
        $countries = Country::orderBy('id', 'DESC')->get();
        $states    = State::orderBy('id', 'DESC')->get();
        $types     = PropertyType::orderBy('id', 'DESC')->get();
        $property  = OwnerProperty::select('id', 'property_name')
            ->where('owner_id', $id)
            ->where('is_builder', 1)
            ->get();
        $apartment = OwnerProperty::where('owner_id', $id)->orderBy('id', 'DESC')->get();

        if (count($types) > 0) {
            $amenities_listing = $this->availableAmenities($types[0]->id);
            $details = $this->availableDetails($types[0]->id);
        } else {
            $amenities_listing = array();
            $details = array();
        }
        $data = [
            'owners'  => $owner,
            'countries' => $countries,
            'states'  => $states,
            'amenities_listing' => $amenities_listing,
            'types'   => $types,
            'details' => $details,
            'property' => $property,
            'apartment' => $apartment,
        ];

        return view('admin.owner.addapartment', $data);
    }

    public function availableAmenities($type)
    {
        $amenities_listing = [];
        $amenities_listing = AmenitiesCategory::join('property_type_amenity_contents', 'amenities_categories.id', '=', 'property_type_amenity_contents.amenity_category_id')
            ->where('property_type_amenity_contents.type_id', $type)
            ->select('amenities_categories.id', 'amenities_categories.name')
            ->orderBy('sort_order', 'ASC')
            ->distinct()
            ->get();
        foreach ($amenities_listing as $row) {
            $data = Amenity::join('property_type_amenity_contents', 'amenities.id', '=', 'property_type_amenity_contents.amenity_id')
                ->where('property_type_amenity_contents.type_id', $type)
                ->where('amenities.amenity_category_id', $row->id)
                ->select('amenities.id', 'amenities.name', 'amenities.image')
                ->get();
            $row->amenities = $data;
        }
        return $amenities_listing;
    }

    public function availableDetails($type)
    {
        $details = [];
        $details = PropertyTypeDetailsContent::join('details', 'details.id', '=', 'property_type_details_contents.detail_id')
            ->where('property_type_details_contents.type_id', $type)
            ->select('detail_id', 'name', 'sort_order', 'placeholder')
            ->orderBy('details.sort_order', 'ASC')
            ->get();
        return $details;
    }
   }
