<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OfferPackage;
use App\Models\OfferPackageFeature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Frequency;
use App\Models\OwnerProperty;

class OfferPackageController extends Controller
{
    //
    public function showOfferPackage($id)
    {

        $frequency = Frequency::all();

        $offer = OfferPackage::where('property_id', $id)->orderBy('id', 'desc')->get();
        $property = OwnerProperty::with('frequency_rel')->where('id',$id)->first();

        $data = [
            'result' => $offer,
            'id' => $id,
            'frequency' => $frequency,
            'prop' => $property,
        ];
        return view('admin.offer.show_offer_package', $data);
    }

    public function viewOffer($id){

        if ($id) {
            $agent = OfferPackage::where('id',$id)->with('package_features')->first();
            return response()->json(['status' => true, 'response' => $agent]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }

       }

    public function addOfferPackage(Request $request)
    {
        $rules = [
            'start_date' => 'required|date|after:yesterday',
            'end_date' => 'required|date|after:yesterday',
            'name' => 'required',
            'description' => 'required'
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {

            return response()->json(['status' => 0, 'response' => $validation->errors()->all()]);
        }
        try {

            if ($request->name) {
                $package = new OfferPackage();
                $package->offer_package_name = $request->name;
                $package->start_date = $request->start_date;
                $package->end_date = $request->end_date;
                $package->description = $request->description;
                $package->actual_amount =$request->actual_amount;
                $package->discount_amount = $request->discount_amount;
                $package->frequency_id = $request->frequency_id;
                $package->property_id = $request->package_property_id;
                $package->save();

                $features = $request->title;
                //return $features;

                for ($i = 0; $i < count($features); $i++) {
                    $rows = [
                        'package_id' => $package->id,
                        'package_feature' => $features[$i],
                        'status' => "1"
                    ];
                    DB::table('offer_package_features')->insert($rows);
                }
                return response()->json(['status' => 1, 'response' => "Package Added successfully"]);
            } else {
                return response()->json(['status' => 0, 'response' => "This Package doesn't Added"]);
            }
            return response()->json(['status' => 1, 'response' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'response' => [$e->getMessage()]]);
        }
    }

    public function deactivationPackage($id, $action)
    {


        if ($id) {
            $admin = OfferPackage::where('id', $id)->update(['status' => $action]);
            if ($action == 0) {
                $msg = 'Deactivate  successfully';
            } else {
                $msg = 'Activate  successfully';
            }
            return response()->json(['status' => true, 'response' => $msg]);
        } else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
    }
}
