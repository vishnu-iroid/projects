<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDocument;
use App\Models\Agent;
use App\Models\Amenity;
use App\Models\OwnerPropertyAmenity;
use App\Models\OwnerPropertyDetail;
use App\Models\Frequency;
use App\Models\AgentProperties;
use App\Models\ServiceAllocate;
use App\Models\Service;
use App\Models\Owner;

use Yajra\DataTables\Facades\DataTables;
use App\Models\Country;
use App\Models\State;
use App\Models\PropertyType;

use App\Models\AmenitiesCategory;
use App\Models\City;
use App\Models\Pincode;
use App\Models\PropertyTypeDetailsContent;
use Carbon\Carbon;
use Validator;
use DB;
use Illuminate\Support\Arr;
use PhpParser\Node\Stmt\Else_;

use function PHPUnit\Framework\returnSelf;

class propertyManagementController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data   = DB::table('owner_properties as p')
                ->select('p.*', 'p.id as p_id', 'o.name as owner_name')
                // ->where('p.contract_owner', '1')
                ->join('owners as o', 'o.id', 'p.owner_id')->orderBy('p.id', 'DESC');

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('property_name', function ($row) {
                    if ($row->builder_id > 0) {

                        $building_image=OwnerProperty::where('id',$row->builder_id)->first();
                        return $row->property_name ."(".$building_image->property_name.")";
                    }
                    else{
                        return $row->property_name;
                    }

                })
                ->addColumn('agent',function($row){

                    if($row->id)
                    {
                        $agent = AgentProperties::where('property_id', $row->id)->first();
                        if($agent){
                            $name = Agent::find($agent->agent_id);
                            return $name->name;
                        }
                        else
                        {
                            return 'NIL';
                        }
                    }
                    else
                    {
                        return 'NIL';
                    }

                })
                ->addColumn('status', function ($row) {

                    if (isset($row->contract_end_date) && ($row->contract_end_date < Carbon::now()->format('Y-m-d')))
                        return '<lable class="bg-danger text-white badge p-2"> Expired</lable>';
                    if ($row->occupied == '1')
                        return '<label class="p-2 bg-info badge text-white">Occupied</label>';
                    if ($row->occupied == '0')
                        return '<label class="p-2 bg-success badge text-white">Vacaant</label>';
                })
                ->addColumn('actions', function ($row) {
                    if ($row->property_to == 0) {
                        return  '<a class="fad fa-edit tx-20 btn btn-primary mb-1 bt_cus mr-1" href="' . route('viewEditProperty', $row->id) . '" data-property-id="' . $row->p_id . '"title="Edit"></a>' .
                            '<a class="fad fa-user tx-20 move-button btn btn-info mb-1 bt_cus mr-1" href="javascript:0;"
                             title="Assign Agents" onclick="addPropertyFunction(' . $row->p_id . ')"></a>' .
                            '<a class="fad fa-plus-square btn btn-primary mb-1 bt_cus tx-20 move-button" href="javascript:0;"
                             title="Service Allocate"
                             onclick="addServiceFunction(' . $row->p_id . ')"></a>' . ' <a class="fad fa-eye tx-20 view-button btn btn-primary mb-1 bt_cus mr-1"
                             href="' . route('viewIndividualProperty', $row->id) . '"
                             data-payment="{{ $row->id }}" title="View Property"></a>' . '<a class="fad fa-check tx-20 move-button btn btn-success mb-1 bt_cus" target="_blank "
                             href="' . route('showOfferPackage', $row->p_id) . '"
                           title="Offer Package"></a>';
                    } else {
                        return '<a class="fad mb-1 fa-edit tx-20 btn btn-primary mb-1 bt_cus mr-1" href="' . route('viewEditProperty', $row->id) . '" data-property-id="' . $row->p_id . '"title="Edit"></a>' .
                            '<a class="fad mb-1 fa-user tx-20 move-button btn btn-primary mb-1 bt_cus mr-1" href="javascript:0;"
                title="Assign Agents" onclick="addPropertyFunction(' . $row->p_id . ')"></a>' .
                            '<a class="fad  mb-1 fa-plus-square tx-20 move-button btn btn-primary  bt_cus" href="javascript:0;"
                title="Service Allocate"
                onclick="addServiceFunction(' . $row->p_id . ')"></a>' . ' <a class="fad btn btn-primary mb-1 bt_cus mr-1 fa-eye tx-20 view-button"
                href="' . route('viewIndividualProperty', $row->id) . '"
                data-payment="{{ $row->id }}" title="View Property"></a>';
                    }
                })
                ->rawColumns(['status', 'actions','building'])
                ->filter(function ($instance) use ($request) {
                    if ($request->get('property')) {
                        // dd($request->get('property'));
                        $property = $request->get('property');
                        $instance->where('p.id', 'LIKE', "%$property%");
                    }
                    if ($request->get('owner') == '0') {
                        $instance->where('owner_id', '>=', 1);
                    } else {
                        $instance->where(function ($w) use ($request) {
                            $owner = $request->get('owner');
                            $w->where('owner_id', 'LIKE', "%$owner%");
                        });
                    }
                    if ($request->get('type')) {
                        $type = $request->get('type');
                        $instance->where('type_id', 'LIKE', "%$type%");
                    }
                    if ($request->get('category') == '1') {

                        $instance->where('category', '=', 1);
                    } elseif ($request->get('category') == '3') {

                        $instance->where('category', '=', 0);
                    } elseif ($request->get('category') == '2') {

                        $instance->where('category', '<=', 1);
                    }
                    if ($request->get('status') == '0') {
                        $instance->where('occupied', 0)->where('contract_end_date', '>', Carbon::now()->format('Y-m-d'));
                    }
                    if ($request->get('status') == '1') {
                        $instance->where('occupied', 1)->where('contract_end_date', '>', Carbon::now()->format('Y-m-d'));
                    }
                    if ($request->get('status') == '2') {
                        $instance->where('contract_end_date', '<', Carbon::now()->format('Y-m-d'));
                    }
                })
                ->make(true);
        }
    }
    public function availableAmenities($type)
    {
        $amenities_listing = [];
        $amenities_listing = AmenitiesCategory::join('property_type_amenity_contents', 'amenities_categories.id', '=', 'property_type_amenity_contents.amenity_category_id')
            ->where('property_type_amenity_contents.type_id', $type)
            ->select('amenities_categories.id', 'amenities_categories.name')
            ->orderBy('sort_order', 'ASC')
            ->distinct()
            ->get();
        foreach ($amenities_listing as $row) {
            $data = Amenity::join('property_type_amenity_contents', 'amenities.id', '=', 'property_type_amenity_contents.amenity_id')
                ->where('property_type_amenity_contents.type_id', $type)
                ->where('amenities.amenity_category_id', $row->id)
                ->select('amenities.id', 'amenities.name', 'amenities.image')
                ->get();
            $row->amenities = $data;
        }
        return $amenities_listing;
    }

    public function availableDetails($type)
    {
        $details = [];
        $details = PropertyTypeDetailsContent::join('details', 'details.id', '=', 'property_type_details_contents.detail_id')
            ->where('property_type_details_contents.type_id', $type)
            ->select('detail_id', 'name', 'sort_order', 'placeholder')
            ->orderBy('details.sort_order', 'ASC')
            ->get();
        return $details;
    }
    public function showPropertyManage(Request $request)
    {
        $property   = OwnerProperty::where('contract_owner', '1')->orderBy('id', 'DESC')->paginate(10);
        $agents     = Agent::select('id', 'name')->where('status', '1')->get();
        $service = Service::select('id', 'service')->get();
        $owner     = Owner::orderBy('id', 'DESC')->get();
        $countries = Country::orderBy('id', 'DESC')->get();
        $states    = State::orderBy('id', 'DESC')->get();
        $types     = PropertyType::orderBy('id', 'DESC')->get();

        $frequency = Frequency::select('id', 'type')->get();
        if (count($types) > 0) {
            $amenities_listing = $this->availableAmenities($types[0]->id);
            $details = $this->availableDetails($types[0]->id);
        } else {
            $amenities_listing = array();
            $details = array();
        }
        $data       = [
            'property' => $property,
            'agent' => $agents,
            'service' => $service,
            'owners'  => $owner,
            'countries' => $countries,
            'states'  => $states,
            'amenities_listing' => $amenities_listing,
            'types'   => $types,
            'details' => $details,
            'frequency' => $frequency,

        ];
        return view('admin.property_management.list', $data);
    }

    public function addPropertyManageagent(Request $request)
    {
        // if (!auth()->user()->can('add_property_list')) {
        //             abort(403, 'Unauthorized action.');
        //         }else{
        //             echo 'yes';die;
        //         }
        $rules = [
            'agents' => 'required|array|min:1',
        ];
        $messages = [
            'agents.required' => 'Agent is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $agents = sizeof($request->agents);
        for ($i = 0; $i < $agents; $i++) {
            $agentsProperty                 = new AgentProperties();
            $agentsProperty->property_id    = $request->property_id;
            $agentsProperty->agent_id       = $request->agents[$i];
            $agentsProperty->status         = '1';
            $agentsProperty->save();
        }
        return response()->json(['status' => true, 'response' => 'Successfully added Agent',  'response_data' => $validation->errors()->first()]);
    }
    public function addPropertyManageagentService(Request $request)
    {
        $rules = [
            'service' => 'required|array|min:1',
        ];
        $messages = [
            'service.required' => 'service is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $service = sizeof($request->service);
        for ($i = 0; $i < $service; $i++) {
            $Propertyservice                = new ServiceAllocate();
            $Propertyservice->property_id    = $request->service_property_id;
            $Propertyservice->service_id       = $request->service[$i];
            $Propertyservice->status         = '1';
            $Propertyservice->save();
        }
        return response()->json(['status' => true, 'response' => $validation->errors()->first()]);
    }


    public function addProperty(Request $request)
    {

        if (!$request->ownerslist) {
            return response()->json(['status' => false, 'response' => 'Something went wrong']);
        }
        $rules = [
            'property_name' => 'required',
            'property_to' => 'required',
            'category' => 'required',
            'property_status' => 'required',
            'property_country' => 'required',
            'property_state' => 'required',
            'property_city' => 'required',
            'property_zipcode' => 'required',
            'building_image' => 'array',
            'building_image.*' => 'max:3000|mimes:jpeg,jpg,png',
            'images' => 'array',
            'images.*' => 'max:3000|mimes:jpeg,jpg,png',
            'floor_plans' => 'nullable|array',
            'floor_plans.*' => 'max:3000|mimes:jpeg,jpg,png',
            'videos' => 'nullable|array',
            'videos.*' => 'max:3000|mimes:mp4,mov,ogg,qt,webm',
            'amenities' => 'array',
            'detailsdata' => 'array|min:1',
            'contract_no' => 'required',
            'adsnumber' => 'required',
            'guard_number' => 'required',
            'contract_file' => 'required',
            'ownership_doc' => 'required',
            'commision_as' => 'required',
            'owner_amount' => 'required',
            'property_management_fee_type' => 'required',
            'management_fee' => 'required',
            'contract_start_date' => 'required|date|before:contract_end_date',
            'contract_end_date' => 'required|date|after:contract_start_date',
            'selling_price'   => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'mrp'             => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'rent'            => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'security_deposit' => 'regex:/^\d+(\.\d{1,2})?$/|nullable',
            'token_amount'    => 'regex:/^\d+(\.\d{1,2})?$/|nullable',

        ];
        $messages = [
            'property_name.required' => 'Property Name to is required',
            'property_to.required' => 'property to is required',
            'property_country.required' => 'country is required',
            'property_state.required' => 'state is required',
            'property_city.required' => 'city is required',
            'property_zipcode.required' => 'zipcode is required',
            'images.required' => 'images are required',
            'furnished.required' => 'Furnish is required',
            'detailsdata.required' => 'Details fields are required',
            'contract_no.required' => 'Contract Number is required',
            'ownership_doc.required' => 'Ownership Document is required',
            'contract_file.required' => 'Contract file is required',
            'contract_start_date.required' => 'Contract start date is required',
            'contract_end_date.required' => 'Contract end date is required',
            'token_amount.required' => 'Token amount is required',

        ];

        if (!$request->individual_building) {
            $detailsdata = "";
        } else {
            $detailsdata = $request->detailsdata;
        }

        if ($detailsdata) {
            foreach ($detailsdata as $key => $value) {
                $messages['detailsdata' . $key] = 'Details ' . $key . ' field is required';
            }
        }
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'response' => $validation->errors()->all()]);
        }
        $furnished  = $request->furnished;
        if (!in_array($furnished, array(0, 1, 2))) {
            return response()->json(['status' => false, 'response' => "Please enter a valid furnish type"]);
        }
        $exid  = 000;
        if ($request->propertyedit_id) {

            $property = OwnerProperty::where('id', $request->propertyedit_id)->first();
            if ($property->property_reg_no) {
                $property->property_reg_no = $property->property_reg_no;
            }
        } else {
            $property = new OwnerProperty;

            $maxid = OwnerProperty::max('property_reg_no');
            if ($maxid) {
                $exid = explode('-', $maxid)[1] + 1;
                $property->property_reg_no = '#REG' . time() . '-' . $exid;
            } else {
                $property->property_reg_no = '#REG' . time() . '-' . '100';
            }
        }

        $contract_no                    = $request->contract_no;
        $contract_type                  = $request->contract_type;
        $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
        $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
        $type                           = $request->type;
        $property_to                    = $request->property_to;
        $property_name                  = $request->property_name;
        $unit_num                       = $request->unit_num;
        $floor_num                      = $request->floor_num;
        $commission_in                  = $request->commision_as;
        // property management fee
        $property_management_fee_type = $request->property_management_fee_type;
        $adsnumber = $request->adsnumber;
        $guardnumber = $request->guard_number;
        $property->owner_id             = $request->ownerslist;
        $property->property_name        = $property_name;

        $property->property_to          = $property_to;
        $property->unit_num             = $unit_num;
        $property->floor_num            = $floor_num;
        $property->commission_in        = $commission_in;
        // property management fee
        $property->property_management_fee_type = $property_management_fee_type;
        $property->ads_number = $adsnumber;
        $property->guards_number = $guardnumber;
        $property->type_id              = $type;
        if ($request->category == 1) {
            $property->category             = 0;
        } else {
            $property->category             = 1;
        }
        $property->street_address_1     = $request->address1;
        $property->street_address_2     = $request->address2 ? $request->address2 : "";
        $property->description          = $request->description;
        $property->country              = $request->property_country;
        $property->city                 = $request->property_city;
        $property->state                = $request->property_state;
        $property->zip_code             = $request->property_zipcode;
        $property->occupied             = $request->property_status;
        $property->status               = '1';
        $property->furnished            = $request->furnished ? $request->furnished : '0';
        $property->contract_no          = $contract_no ? $contract_no : null;
        $property->contract_type        = $contract_type ? $contract_type : null;
        $property->contract_start_date  = $contract_start_date ? $contract_start_date : null;
        $property->contract_end_date    = $contract_end_date ? $contract_end_date : null;
        $property->property_to          = $property_to;
        $property->contract_owner       = '1';
        $property->commission           = $request->owner_amount ? $request->owner_amount : null;

        // property management fee
        $property->management_fee = $request->management_fee ? $request->management_fee : null;
        $property->ads_number = $adsnumber ? $adsnumber : null;
        $property->guards_number = $guardnumber ? $guardnumber : null;

        $property->latitude             = $request->lat_prop ? $request->lat_prop : 9.93123300;
        $property->longitude            = $request->lng_prop ? $request->lng_prop : 76.267303;


        if ($request->individual_building  == 1) {
            // dd($request->min_sellingprice);
            $frequency                      = $request->frequency;
            $selling_price                  = $request->min_sellingprice;
            $mrp                            = $request->max_sellingprice;

            $property->occupied             = $request->property_status;
            $property->furnished        = $request->furnished ? $request->furnished : '0';

            $property->selling_price        = $selling_price ? $selling_price : null;
            $property->is_builder           = 0;
            $property->mrp                  = $mrp ? $mrp : null;
            $property->frequency            = $frequency ? $frequency : null;
            $property->rent                 = $request->min_rent ? $request->min_rent : null;
            $property->token_amount         = $request->token_amount ? $request->token_amount : null;
            $property->security_deposit     = $request->security_deposit ? $request->security_deposit : null;
        } else {
            $property->is_builder           = 1;
        }

        if ($request->file('contract_file')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('contract_file')) {
                $path = public_path('uploads/property/contracts/');
                $file          = $request->file('contract_file');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/contracts/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $property->contract_file        = $file_name;
        }
        if ($request->file('ownership_doc')) {
            $random_pass  = mt_rand(1000000000, 9999999999);
            $id_file_name = '';
            if ($request->file('ownership_doc')) {
                $path = public_path('uploads/property/ownership_doc/');
                $file          = $request->file('ownership_doc');
                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                $file_name     = '/uploads/property/ownership_doc/' . $id_file_name;
                $file->move($path, $id_file_name);
            }
            $property->ownership_doc        = $file_name;
        }
        try {
            DB::beginTransaction();
            $property->save();
            $savedoc = array();
            $savefloor = array();
            $savevideo = array();
            $amenities = array();
            $building_image = array();

            if ($request->file('building_image')) {
                foreach ($request->file('building_image') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();

                    $row->move(public_path('/uploads/property/images'), $imagename);
                    $building_image[$key]['property_id']   = $property->id;
                    $building_image[$key]['document']      = '/uploads/property/images/' . $imagename;
                    $building_image[$key]['type']          = '3';
                    $building_image[$key]['owner_id']      = $request->ownerslist;
                    // $building_image[$key]['owner_id']      = isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
                    $building_image[$key]['created_at']    = now();
                    $building_image[$key]['updated_at']    = now();
                }
                OwnerPropertyDocument::insert($building_image);
            }

            if ($request->file('images')) {
                foreach ($request->file('images') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'), $imagename);
                    $savedoc[$key]['property_id']   = $property->id;
                    $savedoc[$key]['document']      = '/uploads/property/images/' . $imagename;
                    $savedoc[$key]['type']          = '0';
                    $savedoc[$key]['owner_id']      = $request->ownerslist;
                    // $savedoc[$key]['owner_id']      = isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
                    $savedoc[$key]['created_at']    = now();
                    $savedoc[$key]['updated_at']    = now();
                }
                OwnerPropertyDocument::insert($savedoc);
            }
            if ($request->file('floor_plans')) {
                foreach ($request->file('floor_plans') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                    $savefloor[$key]['property_id'] = $property->id;
                    $savefloor[$key]['document'] = '/uploads/property/floor_plans/' . $imagename;
                    $savefloor[$key]['type'] = '2';
                    $savefloor[$key]['owner_id'] = $request->ownerslist;
                    // $savefloor[$key]['owner_id'] =isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
                    $savefloor[$key]['created_at'] = now();
                    $savefloor[$key]['updated_at'] = now();
                }
                OwnerPropertyDocument::insert($savefloor);
            }
            if ($request->file('videos')) {
                foreach ($request->file('videos') as $key => $row) {
                    $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/videos'), $videoname);
                    $savevideo[$key]['property_id'] = $property->id;
                    $savevideo[$key]['document'] = '/uploads/property/videos/' . $videoname;
                    $savevideo[$key]['type'] = '1';
                    $savevideo[$key]['owner_id'] = $request->ownerslist;
                    // $savevideo[$key]['owner_id'] = isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
                    $savevideo[$key]['created_at'] = now();
                    $savevideo[$key]['updated_at'] = now();
                }
                OwnerPropertyDocument::insert($savevideo);
            }
            if ($request->amenities) {
                foreach ($request->amenities as $key => $row) {
                    $category_id = Amenity::where('id', $row)->pluck('amenity_category_id');
                    $amenities[$key]['property_id'] = $property->id;
                    $amenities[$key]['amenity_id']  = $row;
                    $amenities[$key]['amenities_category_id'] = $category_id[0];
                    $amenities[$key]['owner_id']   = $request->ownerslist;
                    // $amenities[$key]['owner_id']   = isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
                    $amenities[$key]['created_at'] = now();
                    $amenities[$key]['updated_at'] = now();
                }
            }
            OwnerPropertyAmenity::insert($amenities);
            if ($detailsdata) {
                // dd("dshf");
                foreach ($detailsdata as $key => $detail) {
                    $details[$key]['property_id']   = $property->id;
                    $details[$key]['detail_id']     = $key;
                    $details[$key]['value']         = $detail;
                    $details[$key]['owner_id']      = (int)$request->ownerslist;
                    // $details[$key]['owner_id']      = (int)isset($request->ownerslist)?$request->ownerslist:auth()->user()->id;
                    $details[$key]['created_at']    = now();
                    $details[$key]['updated_at']    = now();
                }
                OwnerPropertyDetail::insert($details);
            }

            DB::commit();

            $return_data = [
                'property_id' => $property->id,
            ];
            // return redirect()->route('showPropertyManage');

            return response()->json(['status' => true, 'response' => 'Successfully added property', 'response_data' => $return_data]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => false, 'response' => [$e->getMessage()]]);
        }
    }


    // View property
    public function viewProperty($id)
    {
        $properties = OwnerProperty::with('type_details', 'country_rel', 'state_rel', 'zipcode_rel', 'city_rel', 'amenity_details', 'owner_rel')
            ->where('owner_id', $id)
            ->paginate(10);
        return view('admin.owner.propertyViewDetail', compact('properties'));
    }
    public function viewEditProperty(Request $request, $id)
    {
        if ($id) {
            $data['property'] = OwnerProperty::with('type_details', 'country_rel', 'state_rel', 'zipcode_rel', 'city_rel', 'amenity_details', 'owner_rel')
                ->where('id', $id)
                ->first();
            $data['types']     = PropertyType::orderBy('id', 'DESC')->get();
            if (count($data['types']) > 0) {
                $data['amenities_listing'] = $this->availableAmenities($data['types'][0]->id);
                $data['details'] = $this->availableDetails($data['types'][0]->id);
            } else {
                $data['amenities_listing'] = array();
                $data['amenities_listing'] = array();
            }
            // dd($data['amenities_listing']);
            $data['property'] = OwnerProperty::with('type_details', 'country_rel', 'state_rel', 'zipcode_rel', 'city_rel', 'amenity_details', 'owner_rel')
                ->where('id', $id)
                ->first();
            $data['details_prop'] = DB::table('owner_property_details')
                ->select('owner_property_details.id', 'details.name', 'owner_property_details.value', 'details.placeholder')
                ->join('details', 'details.id', 'owner_property_details.detail_id')
                ->where('owner_property_details.property_id', $id)
                ->get();
            $data['property_amenities'] =  OwnerPropertyAmenity::where('property_id', $id)->get();
            $data['owners'] = Owner::all();
            $data['countries'] = Country::all();
            $data['states'] = State::all();
            $data['cities'] = City::all();
            $data['zipcodes'] = Pincode::all();
            $data['frequency'] = Frequency::all();
            $data['property_owner'] = Owner::where('id', $data['property']['owner_id'])->first();
            $data['property_country'] = Country::where('id', $data['property']['country'])->first();

            $data['property_state'] = State::where('id', $data['property']['state'])->first();
            $data['property_city'] = City::where('id', $data['property']['city'])->first();
            $data['property_zipcode'] = Pincode::where('id', $data['property']['zip_code'])->first();
            $data['property_frequency'] = Frequency::where('id', $data['property']['frequency'])->first();
            $data['property_image'] = OwnerPropertyDocument::where('property_id', $data['property']['id'])->get();

            $data['state'] = State::where('country', $data['property']['country'])->get();
            $data['city'] = City::where('state', $data['property']['state'])->get();
            $data['zipcode'] = Pincode::where('city', $data['property']['city'])->get();

            return view('admin.property_management.edit', $data);
        }
    }
    public function editPropertyUnits($id)
    {

        $amenities_listing = array();
        $property_frequency = array();
        $property_image = array();
        $property_unit1 = array();
        $details_prop = array();
        $property_amenities = array();

        $building_details   = OwnerProperty::where('id', $id)->where('is_builder', 1)->first();
        $frequency          = Frequency::all();
        if ($building_details) {
            $property_unit1 = OwnerProperty::where('builder_id', $building_details->id)->get();
            // dd($property_unit1);

            $added_unit_count   = OwnerProperty::where('builder_id', $building_details->id)->count();

            if ($added_unit_count > 0) {
                // $unit_num           = $building_details->unit_num - $added_unit_count;
                $unit_num           =  $added_unit_count;
                if ($unit_num > 0) {
                    $unit_num           = $unit_num;
                }
            } else {

                $unit_num           = $building_details->unit_num;
            }
            $property_type      = $building_details->type_id;
            $property_to        = $building_details->property_to;
            $property_id        = $building_details->id;

            //$amenities_listing = $this->availableAmenities($property_type);
            $property_details  = $this->availableDetails($property_type);
            foreach ($property_unit1 as $propkey => $f) {

                $property_frequency[$propkey] = Frequency::where('id', $f->frequency)->first();
                $amentiy_id[$propkey] = OwnerPropertyAmenity::where('property_id', $f->id)->pluck('amenity_id')->toArray();
                $amenities_listing[$propkey] = Amenity::whereIn('id', $amentiy_id[$propkey])->get();
                $property_image_id[$propkey] = OwnerPropertyDocument::where('property_id', $f->id)->pluck('id')->toArray();
                $property_image[$propkey] = OwnerPropertyDocument::whereIn('id', $property_image_id[$propkey])->get();

                $details_prop[$propkey] = DB::table('owner_property_details')
                    ->select('owner_property_details.id', 'details.name', 'owner_property_details.value', 'details.placeholder')
                    ->join('details', 'details.id', 'owner_property_details.detail_id')
                    ->where('owner_property_details.property_id', $f->id)
                    ->get();

                $property_amenities =  OwnerPropertyAmenity::where('property_id', $f->id)->get();
                // dd($property_amenities);
            }
            $data = [
                'unit_count' => $unit_num,
                'frequency' => $frequency,
                'details' => $property_details,
                'amenities_listing' => $amenities_listing,
                'property_to'       => $property_to,
                'property_id'       => $property_id,
                'property_frequency' => $property_frequency,
                'building_details' => $building_details,
                'property_image' => $property_image,
                'property_unit1' => $property_unit1,

                'details_prop' => $details_prop,
                'property_amenities' => $property_amenities,

            ];
            return view('admin.property.add-unit-edit', $data);
        }
        // else {
        //     dd("sdchdsg");
        //     $property_type = 0;
        //     $property_to = "";
        //     $property_id = "";
        //     $unit_num           = 0;
        //     $amenities_listing = "";
        //     $property_details  = "";
        //     $property_frequency = "";
        //     $property_image = "";
        //     $property_unit1 = "";
        //     $details_prop = "";
        //     $property_amenities = "";


        // }


    }
    public function viewIndividualProperty(Request $request, $id)
    {
        if ($id) {
            $data['property'] = OwnerProperty::with('type_details', 'country_rel', 'state_rel', 'zipcode_rel', 'city_rel', 'amenity_details', 'owner_rel', 'frequency_rel')
                ->where('id', $id)
                ->first();
            $data['types']     = PropertyType::orderBy('id', 'DESC')->get();
            if (count($data['types']) > 0) {
                $data['amenities_listing'] = $this->availableAmenities($data['types'][0]->id);
                $data['details'] = $this->availableDetails($data['types'][0]->id);
            } else {
                $data['amenities_listing'] = array();
                $data['amenities_listing'] = array();
            }
            // dd($data['amenities_listing']);
            $data['property'] = OwnerProperty::with('type_details', 'country_rel', 'state_rel', 'zipcode_rel', 'city_rel', 'amenity_details', 'owner_rel')
                ->where('id', $id)
                ->first();
            $data['details_prop'] = DB::table('owner_property_details')
                ->select('owner_property_details.id', 'details.name', 'owner_property_details.value', 'details.placeholder')
                ->join('details', 'details.id', 'owner_property_details.detail_id')
                ->where('owner_property_details.property_id', $id)
                ->get();
            $data['property_amenities'] =  OwnerPropertyAmenity::where('property_id', $id)->get();
            $data['owners'] = Owner::all();
            $data['countries'] = Country::all();
            $data['states'] = State::all();
            $data['cities'] = City::all();
            $data['zipcodes'] = Pincode::all();
            $data['frequency'] = Frequency::all();
            $data['property_owner'] = Owner::where('id', $data['property']['owner_id'])->first();
            $data['property_country'] = Country::where('id', $data['property']['country'])->first();

            $data['property_state'] = State::where('id', $data['property']['state'])->first();
            $data['property_city'] = City::where('id', $data['property']['city'])->first();
            $data['property_zipcode'] = Pincode::where('id', $data['property']['zip_code'])->first();
            $data['property_frequency'] = Frequency::where('id', $data['property']['frequency'])->first();
            $data['property_image'] = OwnerPropertyDocument::where('property_id', $data['property']['id'])->get();

            $data['state'] = State::where('country', $data['property']['country'])->get();
            $data['city'] = City::where('state', $data['property']['state'])->get();
            $data['zipcode'] = Pincode::where('city', $data['property']['city'])->get();
            return view('admin.property_management.property_view', $data);
        }
    }

    public function viewPropertyUnits($id)
    {
        $amenities_listing = array();
        $property_frequency = array();
        $property_image = array();
        $property_unit1 = array();
        $details_prop = array();
        $property_amenities = array();

        $building_details   = OwnerProperty::where('id', $id)->where('is_builder', 1)->first();
        $frequency          = Frequency::all();
        if ($building_details) {
            $property_unit1 = OwnerProperty::where('builder_id', $building_details->id)->with('frequency_rel')->get();
            // dd($property_unit1);

            $added_unit_count   = OwnerProperty::where('builder_id', $building_details->id)->count();

            if ($added_unit_count > 0) {
                // $unit_num           = $building_details->unit_num - $added_unit_count;
                $unit_num           =  $added_unit_count;
                if ($unit_num > 0) {
                    $unit_num           = $unit_num;
                }
            } else {

                $unit_num           = $building_details->unit_num;
            }
            $property_type      = $building_details->type_id;
            $property_to        = $building_details->property_to;
            $property_id        = $building_details->id;

            //$amenities_listing = $this->availableAmenities($property_type);
            $property_details  = $this->availableDetails($property_type);
            foreach ($property_unit1 as $propkey => $f) {

                $property_frequency[$propkey] = Frequency::where('id', $f->frequency)->first();
                $amentiy_id[$propkey] = OwnerPropertyAmenity::where('property_id', $f->id)->pluck('amenity_id')->toArray();
                $amenities_listing[$propkey] = Amenity::whereIn('id', $amentiy_id[$propkey])->get();
                $property_image_id[$propkey] = OwnerPropertyDocument::where('property_id', $f->id)->pluck('id')->toArray();
                $property_image[$propkey] = OwnerPropertyDocument::whereIn('id', $property_image_id[$propkey])->get();

                $details_prop[$propkey] = DB::table('owner_property_details')
                    ->select('owner_property_details.id', 'details.name', 'owner_property_details.value', 'details.placeholder')
                    ->join('details', 'details.id', 'owner_property_details.detail_id')
                    ->where('owner_property_details.property_id', $f->id)
                    ->get();

                $property_amenities =  OwnerPropertyAmenity::where('property_id', $f->id)->get();
                // dd($property_amenities);
            }
            $data = [
                'unit_count' => $unit_num,
                'frequency' => $frequency,
                'details' => $property_details,
                'amenities_listing' => $amenities_listing,
                'property_to'       => $property_to,
                'property_id'       => $property_id,
                'property_frequency' => $property_frequency,
                'building_details' => $building_details,
                'property_image' => $property_image,
                'property_unit1' => $property_unit1,

                'details_prop' => $details_prop,
                'property_amenities' => $property_amenities,

            ];
            return view('admin.property.add-unit-view', $data);
        }

    }

}
