<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//service
use App\Http\Services\DashboardService;
use App\Models\AdminAccount;
use DB;
class DashboardController extends Controller
{
    public function showDashboard(){
        $data = (new DashboardService)->getDashbaordData();
        return view('admin.dashboard',$data);
    }

    public function showChart(){
        $charts=AdminAccount::select(DB::raw('SUM(amount) amount'),'date', DB::raw('YEAR(date) year, MONTH(date) month'))->where('amount_type','1')
                            // ->groupBy('date')
                                ->groupby('year','month')
                                ->get();
       
       $amount =array();
       $date =array();
        foreach($charts  as $chart){
            $amount[]   = $chart['amount'];
            $date[]     = date('M',strtotime($chart['date']));
        }    
        $chart = array('amount'=>$amount,'date'=>$date);                     
        if($charts){
           
        //    $charts['charts_data']=json_encode($charts);
            return response()->json(['status' => true, 'charts' => $chart]);  
        }else {
            return response()->json(['status' => true, 'response' => 'Something went wrong']);
        }
      
        
    }
}
