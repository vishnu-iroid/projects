<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Frequency;

class MasterController extends Controller
{
    public function showFrequency(Request $request){
        $frequency = Frequency::select('id','type','days')->get();
        $data = [
            'frequency' => $frequency
        ];
        return view('admin.master.frequency',$data);
        //return response()->json(['status'=>true,'frequency'=>$frequency]);
    }
    public function addFrequency(Request $request){
        if($request->id!=""){
            $frequency      = Frequency::select('id','type')->where('id',$request->id)->first();
        }else{
            $frequency      = new Frequency();
        }
        $frequency->type    = $request->type;
        $frequency->days    = $request->days;
        $frequency->save();
        return response()->json(['status'=>true,'response'=>'Success']);
    }

    public function editFrequency($id){
        $frequency          = Frequency::select('id','type','days')->where('id',$id)->first();
        return response()->json(['status'=>true,'response'=>$frequency]);
    }
}
