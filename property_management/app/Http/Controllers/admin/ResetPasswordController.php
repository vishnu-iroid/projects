<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\Admin;

class ResetPasswordController extends Controller
{
    public function adminPasswordReset(Request $request){
        $rules = [
            'forgotemail' => 'required|email',
        ];
        $messages = [
            'forgotemail.required' => "Email is required",
            'forgotemail.email'    => "Email address is not valid",
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>0,'response'=>$validation->errors()]);
        }
        $admin  = Admin::where('email',$request->forgotemail)->first();
        if(!$admin){
            return response()->json(['status'=>0,'response'=>['This email is not registered with us']]);
        }
        $token = $admin->createToken('property')->accessToken;
        $admin->token = $token;
        try{
            $admin->save();
            $link = url('password-reset/'.$token.'/'.$request->forgotemail);
            $data = [
                'link' => $link,
                'name' => $admin->name,
                'email'=> $request->forgotemail,
            ];
            Mail::to($request->forgotemail)->send(new forgotPasswordMail($data));
            return response()->json(['status'=>1,'response'=>'Password reset link has been sent to your email']);
        }catch(\Exception $e){
            return response()->json(['status'=>0,'response'=>[$e->getMessage()]]);
        }
    }
}
