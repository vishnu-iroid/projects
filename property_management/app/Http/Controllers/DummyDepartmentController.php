<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Routing\UrlGenerator;

use Session;
use App\Http\Services\DashboardService;



class DummyDepartmentController extends Controller
{
    protected $url;

    public function __construct()
    {
        $default_data = (new DashboardService)->getDashbaordData();
        $this->default_data = $default_data;
    }



    public function showDashboardSuper()
    {
        Session::put('access_mode', 'super');
        $data = $this->default_data;
        return view('admin.dashboard',$data);
    }
    public function showDashboardAccounts()
    {
        Session::put('access_mode', 'accounts');
        $data = $this->default_data;
        return view('admin.dashboard',$data);
    }


    public function showDashboardDocument()
    {
        Session::put('access_mode', 'document');
        $data = $this->default_data;
        return view('admin.dashboard',$data);
    }


    public function showDashboardPropertyManager()
    {
        Session::put('access_mode', 'manager');
        $data = $this->default_data;
        return view('admin.dashboard',$data);
    }


    public function showDashboardService()
    {
        Session::put('access_mode', 'service');
        $data = $this->default_data;
        return view('admin.dashboard',$data);
    }




}
