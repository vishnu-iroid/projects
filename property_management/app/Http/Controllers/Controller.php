<?php

namespace App\Http\Controllers;

use App\Models\UserPropertyServiceRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //   $serviceCount=UserPropertyServiceRequests::where('status',0)->count();
    // public function displayNotification(){
    //     $services=DB::table('user_property_service_requests')
    //         ->where('status',0)
    //         ->join('services','services.id','user_property_service_requests.service_id')
    //         ->get();
          

    //         // dd($serviceCount);
    //     $data=[
    //         'services'=>$services,
    //         'serviceCount'=>$serviceCount
    //     ];
    //         return view('admin.layout.app',$data);
    // }
}
