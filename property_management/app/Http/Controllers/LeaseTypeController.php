<?php

namespace App\Http\Controllers;

use App\Models\AdminAccount;
use App\Models\BookingContract;
use App\Models\Country;
use App\Models\Frequency;
use App\Models\OwnerProperty;
use App\Models\State;
use App\Models\User;
use App\Models\UserProperty;
use App\Models\UserPropertyRentDocuments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use File;
use Validator;


class LeaseTypeController extends Controller
{
    //
    public function showProperty(Request $request)
    {
        $user = User::select('id','name')->get();
        $countries = Country::orderBy('id','DESC')->get();
        $states    = State::orderBy('id','DESC')->get();
        $frequency = Frequency::orderBy('id','DESC')->get();

        if ($request->ajax()) {
            $data = OwnerProperty::whereNotIn('owner_properties.id', function ($q) {
                $q->select('property_id')->from('user_properties');
            })
            ->where('is_builder', 0)
            ->with('owner_rel')
            ->get();
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('property_name', function ($row) {
                if ($row->builder_id > 0) {
                    $building_image=OwnerProperty::where('id',$row->builder_id)->first();
                    return $row->property_name ."(".$building_image->property_name.")";
                }
                else{
                    return $row->property_name;
                }
            })
            ->addColumn('unit', function ($row) {
                if ($row->builder_id > 0) {
                    $building_image=OwnerProperty::where('id',$row->builder_id)->first();
                    return $building_image->property_name;
                }
                else{
                    return "Nil";
                }
            })
            ->addColumn('owner', function (OwnerProperty $row) {
                if (isset($row->owner_rel)) {
                    $name = $row->owner_rel->name;
                    return $name;
                } else {
                    return "NIL";
                }
            })
            ->addColumn('frequency', function (OwnerProperty $row) {
                if (isset($row->frequency_rel)) {
                    $name = $row->frequency_rel->type;
                    return $name;
                } else {
                    return "NIL";
                }
            })
            ->addColumn('actions', function (OwnerProperty $row) {
                return  '<a class="fad fa-user tx-20 move-button btn btn-info mb-1 bt_cus mr-1" href="javascript:0;"   onclick="assignUserFunction(' . $row->id . ')" data-property-id="' . $row->id . '" title="Assign User"> </a>';
            })
            ->rawColumns(['actions'])
            ->make(true);
        }
        $data = [
            'user' => $user,
            'countries' => $countries,
            'states' => $states,
            'frequency' => $frequency
        ];
        return view('admin.lease.show_properties',$data);
    }

    public function addLeaseType(Request $request)
    {
        $existing_tenant     = $request->get('existing_tenant');
        if( $existing_tenant == '1'){
            $rules['tenant_id']  = 'required';
            $rules['contract_start_date']  = 'required';
            $rules['contract_end_date']  = 'required';
            $rules['check_in_date']  = 'required';
            $rules['frequency']  = 'required';
            $rules['min_rent']  = 'required';
            $rules['security_deposit'] = 'required';
            $rules['attachment'] = 'required';

        }else{
            $rules['name']     = 'required';
            $rules['address']  = 'required';
            $rules['status']   = 'required';
            $rules['country']  = 'required';
            $rules['state']    = 'required';
            $rules['city']     = 'required';
            $rules['zipcode']  = 'required';
            $rules['email']    = 'required';
            $rules['phone']    = 'required';
            $rules['contract_start_date']  = 'required';
            $rules['contract_end_date']  = 'required';
            $rules['check_in_date']  = 'required';
            $rules['frequency']  = 'required';
            $rules['min_rent']  = 'required';
            $rules['security_deposit'] = 'required';
            $rules['attachment'] = 'required';

        }
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>0,'response'=>$validation->errors()->all()]);
        }
        DB::beginTransaction();
        try {
                if($request->property_id){
                    $id = $request->property_id ;
                    $property = OwnerProperty::find($id);
                    $property = OwnerProperty::where('id', $request->property_id)->first();
                    if($request->existing_tenant == '1')
                    {
                        $user_id =  $request->tenant_id ;
                        $user = User::find($user_id);
                    }
                    else{
                        $user = new User;
                        $user->name  = $request->name;
                        $user->email = $request->email;
                        $user->phone = $request->phone;
                        $user->isOwner = '0';
                        $user->country = $request->country;
                        $user->state = $request->state;
                        $user->city = $request->city;
                        $user->zip_code = $request->zipcode;
                        $user->address = $request->address;
                        $user->status = $request->status;
                        $user->latitude  = $request->lat ?  $request->lat : "";
                        $user->longitude = $request->lng ?  $request->lng : "";
                        $user->referral_code    = $this->generateUserReferalCode();
                        $user->save();
                    }
                    if($user)
                    {
                        $approve_booking                    = new UserProperty();
                        $approve_booking->user_id           = $user->id;
                        $approve_booking->property_id       = $request->property_id;
                        $approve_booking->check_in          = $request->check_in_date;
                        $approve_booking->check_out         = $request->check_out_date;
                        $approve_booking->status            = 2;
                        $approve_booking->document_verified = 1;
                        $approve_booking->cancel_status     = 0;
                        $approve_booking->rent              =  $property->rent;
                        $pay_date = $request->check_in_date;
                        if($pay_date)
                        {
                            $date =  $pay_date;
                            if($property->frequency)
                            {
                                $no_of_days = Frequency::select('id','days')->where('id',$property->frequency )->first();
                                // Add days to date and display it
                                $approve_booking->due_date = date('Y-m-d', strtotime($date. ' + '. $no_of_days->days. '  days'));
                            }
                        }
                        if($approve_booking->save())
                        {
                            $contract_no                    = $request->contract_no;
                            $user_property_id               = $approve_booking->id;
                            $request_id                     = $request->requestid;
                            $contract_start_date            = date('Y-m-d', strtotime($request->contract_start_date));
                            $contract_end_date              = date('Y-m-d', strtotime($request->contract_end_date));
                            $booking_details                = UserProperty::find($user_property_id);

                            $Contract_details = new BookingContract;
                            $Contract_details->user_property_id =  $user_property_id;
                            $Contract_details->user_id =  $booking_details->user_id;
                            $Contract_details->contract_no = $contract_no;
                            $Contract_details->contract_status = 1;
                            $Contract_details->contract_start_date =  $contract_start_date;
                            $Contract_details->contract_end_date = $contract_end_date;
                            $Contract_details->total_free_maintenance = $request->free_maintenance;
                            $Contract_details->completed_maintenance = 0;
                            $Contract_details->maintenance_charge = $request->maintenance_charge;
                            if ($request->file('contract_file')) {
                                $random_pass  = mt_rand(1000000000, 9999999999);
                                $id_file_name = '';
                                if ($request->file('contract_file')) {
                                    $path = public_path('uploads/property/contracts/');
                                    if (!File::isDirectory($path)) {
                                        File::makeDirectory($path, 0777, true, true);
                                    }
                                    $file          = $request->file('contract_file');
                                    $id_file_name  = $random_pass . '_' . $user_property_id . $file->getClientOriginalName();
                                    $file_name     = '/uploads/property/contracts/' . $id_file_name;
                                    $file->move($path, $id_file_name);
                                }
                                $Contract_details->contract_file = $file_name;
                            }
                            $Contract_details->save();
                        }
                        if ($request->file('attachment')) {
                            $file = $request->file('attachment');
                            $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path('/uploads/users/rent-documents'), $filename);

                            $user_rent_doc                  = new UserPropertyRentDocuments();
                            $user_rent_doc->user_id         = $user->id;
                            $user_rent_doc->user_property_id = $approve_booking->id;
                            $user_rent_doc->property_id     = $request->property_id;
                            $user_rent_doc->document        = '/uploads/users/rent-documents/' . $filename;
                            $user_rent_doc->payment_status  = 1;
                            $user_rent_doc->payment_check  = 1;
                            $user_rent_doc->save();
                            if($request->min_rent)
                            {
                                $payment = new AdminAccount();
                                $payment->amount_type = 1;
                                $payment->amount = (int)$request->min_rent;
                                // dd($request->min_rent);
                                $payment->payment_type = 5;
                                $payment->date = date('Y-m-d');
                                $payment->user_id = $user->id;
                                $payment->property_id = $request->property_id;
                                $payment->user_property_id = $approve_booking->id ;
                                $payment->reference_id = $user_rent_doc->id;
                                $payment->save();
                            }

                            if($request->security_deposit)
                            {
                                $Sercurity_amount = new AdminAccount();
                                $Sercurity_amount->amount_type = 1;
                                $Sercurity_amount->amount = (int)$request->sercurity_amount;
                                //secuirty
                                $Sercurity_amount->payment_type = 3;
                                $Sercurity_amount->date = date('Y-m-d');
                                $Sercurity_amount->user_id = $user->id;
                                $Sercurity_amount->property_id = $request->property_id;
                                $Sercurity_amount->user_property_id = $id ;
                                $Sercurity_amount->save();
                            }


                        }
                    }
                        DB::commit();
                        return response()->json(['status'=>1,'response'=> 'Lease Type Added']);
                    }
                else
                {
                    return response()->json(['status'=>0,'response'=> 'PropertyId is required']);
                }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status'=>0,'response'=>[$e->getMessage()]]);
        }
    }

    public function generateUserReferalCode()
    {
        $num = mt_rand(0, 999);
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $referral_code = 'SAJID' . $num . $randomString ;
        try {
            $user_data = User::where('referral_code', $referral_code)->first();
            if (!empty($user_data)) {
                $this->generateUserReferalCode();
            } else {
                return $referral_code;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

}
