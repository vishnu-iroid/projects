<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use App\Models\AdminAccount;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use App\Models\User;
use App\Models\Detail;
use App\Models\City;
use App\Models\Event;
use App\Models\EventPackages;
use App\Models\BookUserTour;
use App\Models\UserPropertyBookings;
use App\Models\UserPropertyBookingDocuments;
use App\Models\UserPropertyDocument;
use App\Models\UserFavourites;
use App\Models\UserProperty;
use App\Models\AmenitiesCategory;
use App\Models\Amenity;
use App\Models\Services;
use App\Models\CancelUserServiceRequest;
use App\Models\UserPropertyVacateRequest;
use App\Models\UserPropertyServiceRequests;
use App\Models\UserPropertyRentDocuments;
use App\Models\UserPropertyServiceRequestDocuments;
use App\Models\UserFeedbacks;
use App\Models\FaqDetails;
use App\Models\PrivacyPolicy;
use App\Models\LegalInformation;
use App\Models\AgentProperties;
use App\Models\UserAgentProperty;
use App\Models\UserEventBooking;
use App\Models\UserEventBookingDocuments;
use App\Models\UserDesiredProperty;
use App\Models\PropertyType;
use App\Models\AssignedUserDesiredProperty;
use App\Models\BecomeOwner;
use App\Models\ServiceAllocate;
use App\Models\OfferPackage;
use App\Models\Rewards;
use App\Models\UserRewards;
use App\Models\UserPropertyRating;
use App\Models\Agent;
use App\Models\Frequency;
use App\Models\Notifications;
use App\Models\OwnerPropertyAmenity;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use Auth;
// use DB;
// use Validator;

use App\Models\OwnerProperty;

class UserController extends Controller
{

    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function showProperty(Request $request)
    {

        $latitude   = $request->lat;
        $longitude  = $request->lan;
        $wish_list  = $request->favourite_list;
        $page       = $request->page;
        try {
            $details    = Detail::select('id', 'name')->get();
            $base_url   = url('/');
            if ($latitude && $longitude) {

                $properties = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),'property_type','owner_id','rating','property_to',
                                                    'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'));
                $properties = $properties->where('status', '1')
                                        ->where('contract_owner', '1')
                                        ->where('occupied', '0')
                                        ->where('is_builder','!=', 1)
                                        ->where('contract_end_date','>=', date('Y-m-d'))
                                        ->with(array('documents'=>function($query) use($base_url){
                                                    $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                },'property_details:property_id,detail_id,value,name',
                                                'userfavourites:property_id'
                                            )
                                        )
                                        ->orderBy('id', 'DESC')
                                        ->orderBy('is_featured', 'DESC');
                $properties = $this->customPagination($properties,$page);

                if($properties['data']){
                    foreach($properties['data'] as  $each_detail){
                        if ($each_detail->property_details) {
                            $new_property = [];
                            foreach ($details as $key => $each_item) {
                                $count = 0;
                                foreach ($each_detail->property_details as $key => $detail_array) {
                                    if ($each_item->name == $detail_array->name) {
                                        $count = 1;
                                        $type = $detail_array->name;
                                        $each_detail->$type = $detail_array->value;
                                        break;
                                    }
                                }
                                if ($count != 1) {
                                    $detail_data = $each_item->name;
                                    $each_detail->$detail_data = 0;
                                }
                            }
                        }
                        if ($each_detail->userfavourites) {
                            if ($each_detail->userfavourites->property_id == $each_detail->id) {
                                $each_detail->is_favourite = 1;
                            } else {
                                $each_detail->is_favourite = 0;
                            }
                        } else {
                            $each_detail->is_favourite = 0;
                        }
                        unset($each_detail->property_details);
                        unset($each_detail->userfavourites);
                    }
                }
                $properties_data = [
                    'total_page_count'      => $properties['total_page_count'],
                    'current_page'          => $properties['current_page'],
                    'properties'            => $properties['data'],
                ];
                if($page == 1 || empty($page)){
                    $events     = Event::select('id','name','short_description',DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                                + sin(radians(" . $latitude . ")) * sin(radians(latitude))) AS distance"))
                                        ->with(
                                            array(
                                                'event_priority_image' =>
                                                function ($query) {
                                                    $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",image) AS image'), 'event_id');
                                                }
                                            )
                                        )
                                        ->orderBy('distance', 'ASC')
                                        ->where('status', 1)
                                        ->limit(6)
                                        ->get();
                    $events     = $this->allNull2Empty($events);
                    foreach ($events as $event) {
                        if ($event->event_priority_image == "") {
                            $event->event_priority_image = (object)[];
                        }
                        if ($event->distance) {
                            $event->distance = strval($event->distance);
                        }
                    }
                }else{
                    $events     = [];
                }


                return response()->json(['status' => 200, 'property_data' => $properties_data, 'events' => $events]);
            } elseif ($wish_list) {
                $properties = OwnerProperty::select('owner_properties.id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),'property_type','owner_id','rating','property_to',
                                                    'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'))
                                            ->where('owner_properties.status', '1')
                                            ->where('owner_properties.contract_owner', '1')
                                            ->where('owner_properties.is_builder','!=', 1)
                                            ->with(array('documents'=>function($query) use($base_url){
                                                        $query->select('id','property_id','type','document');
                                                    },'property_details:property_id,detail_id,value,name',
                                                    'userfavourites:property_id'
                                                )
                                            )
                                            ->join('user_favourites', 'user_favourites.property_id', 'owner_properties.id')
                                            ->where('user_favourites.status', 1)
                                            ->where('user_favourites.user_id', Auth::user()->id)
                                            ->orderBy('is_featured', 'DESC');

                // $details    = Detail::select('id', 'name')->get();

                $properties = $this->customPagination($properties,$page);

                $properties['data'] = $this->similarPropertyList($properties['data'], $details);

                $faviourate_properties = [
                    'total_page_count'      => $properties['total_page_count'],
                    'current_page'          => $properties['current_page'],
                    'faviourate_properties' => $properties['data'],
                ];


                return response()->json(['status' => 200, 'property_data' => $faviourate_properties]);
            } else {
                return response()->json(['status' => 400, 'response' => 'Sorry, please provide proper params']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function allNull2Empty($array)
    {
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        $response_array = [];
        foreach ($newarray as $key => $each_array) {
            foreach ($each_array as $key => $value) {
                if (is_array($value)) {
                    $this->allNull2Empty($value);
                } else {
                    if (is_null($value)) {
                        $each_array->$key = '';
                    } else {
                        $each_array->$key = $value;
                        if ($key == 'agent_id') {
                            $str_val = strval($value);
                            $each_array->$key = $str_val;
                        }
                    }
                }
            }
            $response_array[] = $each_array;
        }
        return $response_array;
    }


    public function getUserProfile(Request $request)
    {
        if (Auth::user()) {
            $user_id = Auth::user()->id;
            $user_details = User::where('id', $user_id)
                ->select('name', 'email', 'phone', 'address',DB::raw('ifnull(location,"") as location'),
                         DB::raw('CONCAT("' . $this->url->to('/') . '",profile_pic) AS profile_pic'))
                ->first();
            $user_details = $this->eachNull2Empty($user_details);

            // this function is called for make all the null values to empty instead of looping
            // $user_details = $this->setToNull("user_details", $user_details->toArray());

            return response()->json(['status' => 200, 'data' => $user_details]);
        } else {
            return response()->json(['status' => 401, 'response' => 'Invalid user']);
        }
    }

    public function setToNull($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }

    public function eachNull2Empty($array)
    {
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        foreach ($newarray as $key => $value) {
            if (is_array($value)) {
                $this->eachNull2Empty($value);
            } else {
                if (is_null($value)) {
                    $newarray->$key = '';
                } else {
                    $newarray->$key = $value;
                }
            }
        }
        return $newarray;
    }

    public function updateProfile(Request $request)
    {
        $rules = [
            'name'        => 'required',
            'email'       => 'email|required|unique:users,email,' . Auth::user()->id,
            'phone'       => 'required|unique:users,phone,' . Auth::user()->id,
            'address'     => 'required',
            'profile_pic' => 'mimes:jpeg,png'
        ];
        $messages = [
            'name.required' => 'Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Please enter a valid email',
            'phone.required' => 'Phone Number is required',
            'address.required' => 'Address is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_id                   = Auth::user()->id;
        $update_user               = User::where('id', $user_id)->first();
        $update_user->name         = $request->name;
        $update_user->email        = $request->email;
        $update_user->phone        = $request->phone;
        $update_user->address      = $request->address;
        if ($request->file('profile_pic')) {
            if ($update_user->profile_pic) {
                if (file_exists(public_path($update_user->profile_pic))) {
                    unlink(public_path($update_user->profile_pic));
                }
            }

            $file = $request->file('profile_pic');
            $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('/uploads/users/profile'), $filename);

            $update_user->profile_pic        = '/uploads/users/profile/' . $filename;
        }
        $update_user->save();

        $user_details = User::select(DB::raw('ifnull(name,"") AS name'), DB::raw('ifnull(email,"") AS email'), DB::raw('ifnull(phone,"") AS phone'),
                                    DB::raw('ifnull(address,"") AS address'), DB::raw('CONCAT("' . $this->url->to('/') . '",profile_pic) AS profile_pic'))
                            ->where('id', $user_id)
                            ->first();
        if($user_details){
            if($user_details->profile_pic == null){
                $user_details->profile_pic  = "";
            }
        }
        return response()->json(['status' => 200, 'user_data'=>$user_details, 'response' => 'Updated Profile successfully']);
    }

    public  function updatePropertyToFavourites(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id = $request->property_id;
        $user_id     = Auth::user()->id;
        try {
            $user_favourite =  UserFavourites::where('property_id', $property_id)
                ->where('user_id', $user_id)
                ->first();
            if ($user_favourite) {
                if ($user_favourite->status == 0) {
                    $user_favourite->status = 1;
                    $user_favourite->save();
                    return response()->json(['status' => 200, 'response' => 'Property Added to Favourite List']);
                } else {
                    $user_favourite->status = 0;
                    $user_favourite->save();
                    return response()->json(['status' => 200, 'response' => 'Removed Property from Favourite List']);
                }
            } else {
                $user_favourite = new UserFavourites();
                $user_favourite->user_id     = $user_id;
                $user_favourite->property_id = $property_id;
                $user_favourite->status = 1;
                $user_favourite->save();
                return response()->json(['status' => 200, 'response' => 'Property Added to Favourite List']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function showPropertDetails(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id      = $request->property_id;
        try {
            $property_detail = OwnerProperty::select('owner_properties.id','property_reg_no','property_name','property_type','owner_id','agent_id','frequency',
                                                    'rating','property_to','category','selling_price','mrp','is_featured',DB::raw('ifnull(furnished,3) as furnished '),
                                                    'longitude','latitude','rent','city','description','occupied')
                                                    ->where('owner_properties.status', '1')
                                                    ->where('owner_properties.id', $property_id)
                                                    ->with(
                                                        'documents',
                                                        'frequency_rel:id,type',
                                                        'assigned_agent_details:id,agent_id,property_id,status',
                                                        'property_details:property_id,detail_id,value,name',
                                                        'userfavourites:property_id',
                                                        'amenity_details:amenities_category_id,amenity_id,property_id',
                                                        'floor_plans:id,property_id,document,owner_id'
                                                    )
                                                    ->first();
            if ($property_detail) {
                $property_detail = $this->detailsNull2Empty($property_detail);
                $details          = Detail::select('id', 'name')->get();
                $amenity_category = AmenitiesCategory::select('id', 'name')->orderBy('sort_order', 'ASC')->get();
                if ($property_detail->assigned_agent_details) {
                    $agent_id       = $property_detail->assigned_agent_details->agent_id;
                    $property_detail->assigned_agent_details    = Agent::select('id','name','email','phone',DB::raw('CONCAT("'.$this->url->to('/').'", image) AS image'))
                                                                        ->where('id',$agent_id)
                                                                        ->where('status','1')
                                                                        ->first();
                    // $agent_image        = $property_detail->assigned_agent_details;
                    // $agent_image->image = $this->url->to('/') . $agent_image->image;
                    if(!$property_detail->assigned_agent_details){
                        $property_detail->assigned_agent_details    = (object)[];
                    }
                }else{
                    $property_detail->assigned_agent_details    = (object)[];
                }
                if ($property_detail->documents) {
                    foreach ($property_detail->documents as $file_array) {
                        foreach ($file_array as $key => $file) {
                            if ($key == 'document') {
                                $file_array->document = $this->url->to('/') . $file;
                            }
                        }
                    }
                }
                if ($property_detail->frequency_rel) {
                    if ($property_detail->frequency_rel->id == $property_detail->frequency) {
                        $property_detail->frequency = $property_detail->frequency_rel->type;
                    } else {
                        $property_detail->frequency = '';
                    }
                } else {
                    $property_detail->frequency_rel = (object)[];
                }
                if ($property_detail->floor_plans) {
                    foreach ($property_detail->floor_plans as $file_array) {
                        foreach ($file_array as $key => $file) {
                            if ($key == 'document') {
                                $file_array->document = $this->url->to('/') . $file;
                            }
                        }
                    }
                }

                if ($property_detail->property_details) {
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($property_detail->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $property_detail->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $property_detail->$detail_data = 0;
                        }
                    }
                }
                if ($property_detail->userfavourites) {
                    if ($property_detail->userfavourites->property_id == $property_detail->id) {
                        $property_detail->is_favourite = 1;
                    } else {
                        $property_detail->is_favourite = 0;
                    }
                } else {
                    $property_detail->is_favourite = 0;
                }
                if ($property_detail->amenity_details) {
                    $new_property = [];
                    foreach ($amenity_category as $key => $each_category) {
                        $count = 0;
                        $property_amenities = [];
                        foreach ($property_detail->amenity_details as $key => $each_category_detail) {
                            if ($each_category->id == $each_category_detail->amenities_category_id) {
                                $type = $each_category->name;

                                $amenities = OwnerPropertyAmenity::select('amenities.id','amenities.name',DB::raw('CONCAT("'.$this->url->to('/').'", amenities.image) AS image'))
                                                    ->join('amenities','amenities.id','owner_property_amenities.amenity_id')
                                                    // ->where('id',$each_category_detail->amenity_id)
                                                    ->where('owner_property_amenities.amenities_category_id',$each_category->id)
                                                    ->where('owner_property_amenities.property_id',$property_id)
                                                    ->get();
                                $encoded_amenities = json_encode($amenities);
                                $decoded_amenities = json_decode($encoded_amenities);
                                foreach($decoded_amenities as $key => $file_data){
                                    $property_amenities[] = $file_data;
                                }
                                $count = 1;
                                break;
                            }
                        }

                        if ($count == 1) {
                            if (count($property_amenities) > 0) {
                                $amenities_array[] = [
                                    'id'         => $each_category->id,
                                    'name'       => $each_category->name,
                                    'amenity_details' => $property_amenities
                                ];
                            }
                        }
                    }
                    $property_detail->amenity_categories = $amenities_array;
                }
                unset($property_detail->property_details);
                unset($property_detail->amenity_details);
                unset($property_detail->userfavourites);

                if (empty($property_detail->assigned_agent_details)) {
                    $property_detail->assigned_agent_details = (object)[];
                }

                $latitude  = $property_detail->latitude;
                $longitude = $property_detail->longitude;

                $property_city                  = City::where('id', $property_detail->city)->first();
                if ($property_city) {
                    $property_detail->location      = $property_city->name;
                } else {
                    $property_detail->location      = "";
                }
                $property_detail->total_rating_count = UserPropertyRating::where('property_id',$property_id)->count();


                if($longitude && $longitude){
                    $similar_properties = OwnerProperty::select('id','property_reg_no','owner_id','agent_id','rating','property_to',
                                                            'latitude','longitude','category','selling_price','mrp','is_featured','rent',
                                                            DB::raw("6371 * acos(cos(radians(" . $latitude . "))* cos(radians(latitude)) * cos(radians(longitude)
                                                            - radians(" . $longitude . ")) + sin(radians(" . $latitude . ")) * sin(radians(latitude))) AS distance")
                    );
                    $similar_properties = $similar_properties->having('distance', '<', 40)
                                                            ->where('status', '1')
                                                            ->orderBy('distance', 'asc')
                                                            ->with(
                                                                'documents',
                                                                'property_details:property_id,detail_id,value,name',
                                                                'userfavourites:property_id',
                                                                'country_rel',
                                                                'state_rel',
                                                                'city_rel'
                                                            )
                                                            ->orderBy('is_featured', 'DESC')
                                                            ->where('owner_properties.id', '!=', $property_id)
                                                            ->limit(10)
                                                            ->get();

                    $similar_properties = $this->similarPropertyList($similar_properties, $details);
                }else{
                    $similar_properties = [];
                }

                $data = [
                    'property_details' => $property_detail,
                    'similar_properties' => $similar_properties,
                ];
            } else {
                $data = [
                    'property_details' => (object)[],
                    'similar_properties' => [],
                ];
            }
            return response()->json(['status' => 200, 'property_data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function detailsNull2Empty($array)
    {
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        foreach ($newarray as $key => $value) {
            if (is_null($value)) {
                $newarray->$key = '';
            } else {
                $newarray->$key = $value;
                if ($key == 'agent_id') {
                    $str_val = strval($value);
                    $newarray->$key = $str_val;
                }
            }
        }
        return $newarray;
    }


    public function similarPropertyList($property_list, $details)
    {
        $property_list = $this->allNull2Empty($property_list);
        foreach ($property_list as $property_similar) {
            foreach ($property_similar as $key => $each_value) {
                if ($key == 'documents') {
                    foreach ($property_similar->documents as $file_array) {
                        foreach ($file_array as $key => $file) {
                            if ($key == 'document') {
                                $file_array->document = $this->url->to('/') . $file;
                            }
                        }
                    }
                }
                if ($key == 'property_details') {
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($property_similar->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $property_similar->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $property_similar->$detail_data = 0;
                        }
                    }
                }
            }
            if ($property_similar->userfavourites) {
                if ($property_similar->userfavourites->property_id == $property_similar->id) {
                    $property_similar->is_favourite = 1;
                } else {
                    $property_similar->is_favourite = 0;
                }
            } else {
                $property_similar->is_favourite = 0;
            }
            unset($property_similar->property_details);
            unset($property_similar->userfavourites);
        }
        return $property_list;
    }

    public function bookToVisitProperty(Request $request)
    {
        $rules = [
            'property_id' => 'required',
            'date' => 'required|date|after:yesterday',
            'time_range' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
            'date.required' => 'Date is required',
            'date.date'     => 'The date is not a valid date',
            'time_range.required' => 'Time is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $response_message = "";
            if ($request->tour_id) {
                $book_tour = BookUserTour::where('property_id', $request->property_id)
                    ->where('user_id', Auth::user()->id)
                    ->where('booked_date', '>=', date('Y-m-d'))
                    ->first();
                if (!empty($book_tour)) {
                    $book_tour->booked_date      = date('Y-m-d', strtotime($request->date));
                    $book_tour->time_range       = $request->time_range;
                    $book_tour->save();
                    $response_message    = "Tour booking updated successfully";
                } else {
                    return response()->json(['status' => 401, 'response' => "Invalid Tour"]);
                }
            } else {
                $booking_id                 = $this->generateBookingIdTemporary();
                $book_tour                  = new BookUserTour();
                $book_tour->user_id         = Auth::user()->id;
                $book_tour->property_id     = $request->property_id;
                $book_tour->booked_date     = date('Y-m-d', strtotime($request->date));
                $book_tour->time_range      = $request->time_range;
                $book_tour->booking_id      = $booking_id;
                $book_tour->save();

                $response_message           = "Tour booked successfully";

                $property_agents            = AgentProperties::select('agent_id')
                    ->where('property_id', $request->property_id)
                    ->where('status', 1)
                    ->get();
                $property_agents            = json_decode($property_agents);
                if ($property_agents) {
                    $agent_ids              = array_column($property_agents, 'agent_id');
                } else {
                    $agent_ids              = [];
                }
                if ($agent_ids) {
                    $agent_ids          = array_unique($agent_ids);
                    $insert_array   = [];
                    foreach ($agent_ids as $agent_id) {
                        $insert_array[] = [
                            'agent_id'    => $agent_id,
                            'tour_id'     => $book_tour->id,
                            'property_id' => $request->property_id,
                            'user_id'     => Auth::user()->id,
                            'status'      => 0,
                            'created_at'  => date('Y-m-d H:i:s'),
                        ];
                    }
                    UserAgentProperty::insert($insert_array);
                }
            }
            $data = [
                'tour_id'      => $book_tour->id,
                'property_id'  => $book_tour->property_id,
            ];
            return response()->json(['status' => 200, 'response' => $response_message, 'response_data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function generateBookingIdTemporary()
    {
        $otp = mt_rand(100000, 999999);
        $new_booking_id = 'SBID' . $otp;
        try {
            $user_book_data = BookUserTour::where('booking_id', $new_booking_id)->first();
            if (!empty($user_book_data)) {
                $this->generateBookingIdTemporary();
            } else {
                return $new_booking_id;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function confirmTourBookingDetails(Request $request)
    {
        $rules = [
            'tour_id' => 'required',
            'property_id' => 'required',
        ];
        $messages = [
            'tour_id.required' => 'Tour is required',
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $tour_id            = $request->tour_id;
        $property_id        = $request->property_id;
        try {
            $tour_details       = BookUserTour::select('id', 'booked_date', 'time_range')
                ->where('id', $tour_id)
                ->where('property_id', $property_id)
                ->first();
            if (!empty($tour_details)) {
                $property_details   = OwnerProperty::select(
                    'id',
                    'property_to',
                    'property_reg_no',
                    DB::raw('ifnull(property_name,"") AS property_name'),
                    DB::raw('ifnull(selling_price,0.00) AS selling_price'),
                    DB::raw('ifnull(rent,0.00) AS rent'),
                    DB::raw('ifnull(latitude,0.00) AS latitude'),
                    DB::raw('ifnull(longitude,0.00) AS longitude'),
                    'city'
                )
                    ->with(
                        array(
                            'property_priority_image' =>
                            function ($query) {
                                $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",document) AS document'), 'property_id');
                            }
                        )
                    )
                    ->where('id', $property_id)
                    ->first();
                $tour_details       = $this->eachNull2Empty($tour_details);
                $property_details   = $this->eachNull2Empty($property_details);
                $property_city      = City::where('id', $property_details->city)->first();
                if ($property_city) {
                    $property_details->location      = $property_city->name;
                } else {
                    $property_details->location      = "";
                }
            } else {
                $property_details = "";
                $tour_details = "";
            }
            $data = [
                'tour_details'      => $tour_details,
                'property_details'  => $property_details,
            ];

            return response()->json(['status' => 200, 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function viewBookedTourDetails(Request $request)
    {

        $rules = [
            'tour_id' => 'required',
        ];
        $messages = [
            'tour_id.required' => 'Tour is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $tour_id    = $request->tour_id;
        try {
            $tour_details       = BookUserTour::select('id AS tour_id', 'booked_date', 'time_range', 'property_id')
                ->where('id', $tour_id)
                ->first();
            $tour_details       = $this->eachNull2Empty($tour_details);
            return response()->json(['status' => 200, 'tour_data' => $tour_details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function confirmTourBooking(Request $request)
    {
        $rules = [
            'tour_id' => 'required',
        ];
        $messages = [
            'tour_id.required' => 'Tour is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $tour_id            = $request->tour_id;
            $tour_details       = BookUserTour::select('booked_date', 'time_range', 'property_id', 'status')
                ->where('id', $tour_id)
                ->first();
            if ($tour_details->status == 0) {
                BookUserTour::where('id', $tour_id)->update(array('status' => 1));

                return response()->json(['status' => 200, 'response' => "Tour booking confirmed"]);
            } else {
                return response()->json(['status' => 200, 'response' => "This booking is already confirmed"]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function showAllEvents(Request $request)
    {
        $rules = [
            'lat'  => 'required',
            'lan'  => 'required',
            'page' => 'integer',
        ];
        $messages = [
            'lat.required' => 'Latitude is required',
            'lan.required' => 'Longitude is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $latitude   = $request->lat;
        $longitude  = $request->lan;
        $page       = $request->page;
        try {
            $events     = Event::select(
                'id',
                'name',
                'short_description',
                DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                                + sin(radians(" . $latitude . ")) * sin(radians(latitude))) AS distance")
            )
                ->with(
                    array(
                        'event_priority_image' =>
                        function ($query) {
                            $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",image) AS image'), 'event_id');
                        }
                    )
                )
                ->orderBy('distance', 'ASC')
                ->where('status', 1);

            $total_event_count  = $events->count();

            $items_per_page     = 10;
            $total_page_count   = ceil($total_event_count / $items_per_page);
            if ($page) {
                $pages_array        = range(1, $total_page_count);
                if (in_array($page, $pages_array)) {
                    $current_page       = strval($page);
                    $offset_page        = $page - 1;
                    $offset_item_count  = $offset_page * $items_per_page;

                    $events             = $events->offset($offset_item_count)->take($items_per_page)->get();
                } else {
                    $current_page       = strval($page);
                    $events             = [];
                }
            } else {
                if ($page == 0 && $page != null) {
                    $current_page       = strval(0);
                    $events             = [];
                } else {
                    $current_page       = strval(1);
                    $events             = $events->take($items_per_page)->get();
                }
            }

            $events     = $this->allNull2Empty($events);
            foreach ($events as $event) {
                if ($event->event_priority_image == "") {
                    $event->event_priority_image = (object)[];
                }
            }
            $event_data = [
                'total_page_count' => $total_page_count,
                'current_page'     => $current_page,
                'events'           => $events,
            ];

            return response()->json(['status' => 200, 'event_data' => $event_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function showEventDetails(Request $request)
    {
        $rules = [
            'event_id' => 'required|integer',
        ];
        $messages = [
            'event_id.required' => 'Event is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $event_id   = $request->event_id;
        try {
            $event     = Event::select(
                'id',
                'name',
                'short_description',
                'description',
                'event_date',
                'event_date',
                'latitude',
                'longitude',
                'admin_id'
            )
                ->with(
                    array('event_docs' =>
                    function ($query) {
                        $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",image) AS image'), 'event_id');
                    }, 'admin_events:id,first_name,last_name,phone')
                )
                ->where('id', $event_id)
                ->where('status', 1)
                ->first();
            if (!empty($event)) {
                $event     = $this->detailsNull2Empty($event);
                if (empty($event->admin_events)) {
                    $event->admin_events     = (object)[];
                }
            } else {
                $event     = (object)[];
            }


            return response()->json(['status' => 200, 'event_data' => $event]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function getBookPropertyDetails(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id        = $request->property_id;
        try {

            $property_details   = OwnerProperty::select(
                'id',
                'property_to',
                'property_reg_no',
                'frequency',
                DB::raw('ifnull(frequency,"") AS frequency'),
                DB::raw('ifnull(property_name,"") AS property_name'),
                DB::raw('ifnull(selling_price,0.00) AS selling_price'),
                DB::raw('ifnull(token_amount,0.00) AS token_amount'),
                DB::raw('ifnull(rent,0.00) AS rent'),
                'city'
            )
                ->with(
                    array(
                        'property_priority_image' =>
                        function ($query) {
                            $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",document) AS document'), 'property_id');
                        },
                        'property_rent_frequency:id,type'
                    )
                )
                ->where('id', $property_id)
                ->first();

                if ($property_details->property_rent_frequency) {
                    $property_details->frequency = $property_details->property_rent_frequency->type;
                } else {
                    $property_details->property_rent_frequency   = (object)[];
                }
            if ($property_details) {
                $property_details->token_amount   = $property_details->token_amount;
                $property_city      = City::where('id', $property_details->city)->first();
                if ($property_city) {
                    $property_details->location      = $property_city->name;
                } else {
                    $property_details->location      = "";
                }
                $property_details   = $this->eachNull2Empty($property_details);
            } else {
                $property_details   = (object)[];
            }


            return response()->json(['status' => 200, 'property_details'  => $property_details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function bookProperty(Request $request)
    {
        $rules = [
            'property_id' => 'required',
            'check_in' => 'required|date',
            'check_out' => 'date',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
            'check_in.required' => 'Check-in date is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {

            $property_id      = $request->property_id;

            // other package selection
            $package_id       = $request->package_id;

            if($package_id){
                $property_package   = OfferPackage::where('id',$package_id)
                                                    ->where('property_id',$property_id)
                                                    ->where('start_date','<=',date('Y-m-d'))
                                                    ->where('end_date','>=',date('Y-m-d'))
                                                    ->where('status',1)
                                                    ->first();

                if(!$property_package){
                    return response()->json(['status' => 400, 'response' => 'Invalid package']);
                }
            }


            // apply coupon to book property
            $coupon_code = $request->coupon;
            // check coupon exist
            if($coupon_code){
                $check_user_reward   = Rewards::select('rewards.id','rewards.coupon_code',)->join('user_rewards','user_rewards.reward_id','rewards.id')
                                                ->where('user_rewards.user_id', Auth::user()->id)
                                                ->where('user_rewards.expiry_date','>=',date('Y-m-d'))
                                                ->where('user_rewards.status','active')
                                                ->first();
                if($check_user_reward){
                    UserRewards::where('reward_id',$check_user_reward->id)
                                ->where('user_id',Auth::user()->id)
                                ->update(['status' => 'inactive']);
                }else{
                    return response()->json(['status' => 400, 'response' => 'Invalid coupon']);
                }
            }

            $check_in_date    = $request->check_in;
            $check_out_date   = $request->check_out;
            $property_details = OwnerProperty::where('id', $property_id)->where('status',1)->where('contract_owner','1')->where('occupied',0)->first();

            if (!empty($property_details)) {
                $response_message = '';
                $property_for = $property_details->property_to;
                if ($property_details->status == 1) {
                    if ($property_details->occupied == 0) {
                        $booking_id                  = $this->generateBookingIdPermanant();
                        $book_property = UserPropertyBookings::where('property_id', $property_id)
                            ->where('user_id', Auth::user()->id)
                            ->where('check_in', '>=', date('Y-m-d'))
                            ->where('cancel_status', false)
                            ->first();
                        if ($property_for == 0) { #property to rent
                            $rules = [
                                'check_out' => 'required',
                            ];
                            $messages = [
                                'check_out.required' => 'Check-out date is required',
                            ];
                            $validation = Validator::make($request->all(), $rules, $messages);
                            if ($validation->fails()) {
                                return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
                            }
                            if (empty($book_property)) {
                                $book_property                = new UserPropertyBookings();
                                $book_property->property_id  = $property_id;
                                $book_property->booking_id  = $booking_id;
                                $book_property->user_id      = Auth::user()->id;
                                if($coupon_code){
                                    $book_property->coupoun  = $coupon_code;
                                }
                                if($package_id){
                                    $book_property->package_id  = $package_id;
                                }
                                $book_property->check_in      = date('Y-m-d', strtotime($check_in_date));
                                $book_property->check_out     = date('Y-m-d', strtotime($check_out_date));
                                $book_property->save();
                                $response_message    = "Property Booking is under progress";
                                return response()->json(['status' => 200, 'response' => $response_message, 'booking_id' => $booking_id]);
                            } else {
                                $already_check_in = date('d-m-Y', strtotime($book_property->check_in));
                                return response()->json(['status' => 400, 'response' => "Property already booked on " . $already_check_in, 'booking_id' => $book_property->booking_id]);
                            }
                        } elseif ($property_for == 1) { #property to buy
                            if (empty($book_property)) {
                                $book_property                = new UserPropertyBookings();
                                $book_property->property_id  = $property_id;
                                $book_property->booking_id  = $booking_id;
                                $book_property->user_id      = Auth::user()->id;
                                if($coupon_code){
                                    $book_property->coupoun  = $coupon_code;
                                }
                                $book_property->check_in      = date('Y-m-d', strtotime($check_in_date));
                                $book_property->save();
                                $response_message    = "Property Booking is under progress";
                                return response()->json(['status' => 200, 'response' => $response_message, 'booking_id' => $booking_id]);
                            } else {
                                $already_check_in = date('d-m-Y', strtotime($book_property->check_in));
                                return response()->json(['status' => 400, 'response' => "Property already booked on " . $already_check_in, 'booking_id' => $book_property->booking_id]);
                            }
                        }
                    } else {
                        return response()->json(['status' => 400, 'response' => "Property space already occupied"]);
                    }
                } else {
                    return response()->json(['status' => 400, 'response' => "Property is not active"]);
                }
            } else {
                return response()->json(['status' => 401, 'response' => "Invalid Property"]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function generateBookingIdPermanant()
    {
        $otp = mt_rand(100000, 999999);
        $new_booking_id = 'UBID' . $otp;
        try {
            $user_book_data = UserPropertyBookings::where('booking_id', $new_booking_id)->first();
            if (!empty($user_book_data)) {
                $this->generateBookingIdPermanant();
            } else {
                return $new_booking_id;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function uploadBookPropertyDocument(Request $request)
    {
        $rules = [
            'booking_id'        => 'required',
            'document_status'   => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'booking_id.required' => 'Booking id is required',
            'document_status.required' => 'Payment method is required',
            'document.required' => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $has_booking = UserPropertyBookings::where('booking_id', $request->booking_id)->first();
            if (!empty($has_booking)) {
                $already_uploaded = UserPropertyBookingDocuments::where('booking_id', $has_booking->id)->first();
                if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        $file = $request->file('document');
                        $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/users/bill-document'), $filename);

                        $book_doc                  = new UserPropertyBookingDocuments();
                        $book_doc->document        = '/uploads/users/bill-document/' . $filename;
                        $book_doc->user_id         = Auth::user()->id;
                        $book_doc->booking_id      = $has_booking->id;
                        $book_doc->document_status = $request->document_status;
                        $book_doc->save();

                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                return response()->json(['status' => 401, 'response' => 'Invalid Booking']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function userPropertyList(Request $request)
    {
        $rules = [
            'page'                 => 'integer',
            'user_property_status' => 'required|integer',
        ];
        $messages = [
            'page.integer'                  => 'Page number must be an integer',
            'user_property_status.integer'  => 'User property status must be an integer',
            'user_property_status.required' => 'User property status required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $page = $request->page;
            $property_status = $request->user_property_status;
            if (!(in_array($property_status, array(0, 1, 2)))) {
                return response()->json(['status' => 400, 'response' => 'Invalid user property status']);
            }
            $user_id    = Auth::user()->id;
            $properties = UserProperty::select('property_id')
                ->where('user_id', $user_id)
                ->where('cancel_status', false)
                ->where('status', $property_status)
                ->get();
            $decoded_properties     = json_decode($properties);
            $properties_ids         = array_column($decoded_properties, 'property_id');
            $user_properties = OwnerProperty::select('id', 'property_to', 'property_reg_no',  DB::raw('ifnull(frequency,"") AS frequency'),  DB::raw('ifnull(property_name,"") AS property_name'),
                                                        DB::raw('ifnull(selling_price,0.00) AS selling_price'),DB::raw('ifnull(rent,0.00) AS rent'))
                ->with(
                    array('property_priority_image' =>
                    function ($query) {
                        $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",document) AS document'), 'property_id');
                    }, 'user_property_related:id,property_id')
                )
                ->whereIn('id', $properties_ids)
                ->orderBy('id','DESC')
                ->where('status', 1);
                $user_properties->due_date = '';


            $total_count  = $user_properties->count();

            $items_per_page     = 10;
            $total_page_count   = ceil($total_count / $items_per_page);
            if ($page) {
                $pages_array        = range(1, $total_page_count);
                if (in_array($page, $pages_array)) {
                    $current_page              = strval($page);
                    $offset_page               = $page - 1;
                    $offset_item_count         = $offset_page * $items_per_page;

                    $user_properties    = $user_properties->offset($offset_item_count)->take($items_per_page)->get();
                } else {
                    $current_page              = strval($page);
                    $user_properties    = [];
                }
            } else {
                if ($page == 0 && $page != null) {
                    $current_page              = strval(0);
                    $user_properties    = [];
                } else {
                    $current_page              = strval(1);
                    $user_properties                    = $user_properties->take($items_per_page)->get();
                }
            }

            $user_properties = $this->allNull2Empty($user_properties);
            foreach ($user_properties as $property) {
                if ($property->user_property_related == "") {
                    $property->user_property_related = (object)[];
                }
                if ($property->property_priority_image == "") {
                    $property->property_priority_image = (object)[];
                }
                if($property->user_property_related){

                    $payment = AdminAccount::select('payment_type','date')->where('property_id',$property->user_property_related->property_id)->get();

                    if($payment->contains('payment_type', '5'))
                    {
                        $pay_date = AdminAccount::select('payment_type','date')->where('payment_type','5')->where('property_id',$property->user_property_related->property_id)->orderBy('id', 'DESC')->first();
                    }
                    else
                    {
                        $pay_date = AdminAccount::select('payment_type','date')->where('payment_type','2')->where('property_id',$property->user_property_related->property_id)->orderBy('id', 'DESC')->first();

                    }

                    if($pay_date)
                    {
                        $date =  $pay_date->date;
                        if($property->frequency)
                        {
                            $no_of_days = Frequency::select('id','days')->where('id',$property->frequency )->first();
                            // Add days to date and display it
                            $property->due_date = date('Y-m-d', strtotime($date. ' + '. $no_of_days->days. '  days'));
                        }
                    }
                }
            }
            $user_properties_data = [
                'total_page_count' => $total_page_count,
                'current_page'     => $current_page,
                'booked_property'  => $user_properties,
            ];
            return response()->json(['status' => 200, 'booked_data' => $user_properties_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function userBookedPropertyDetails(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id        = $request->property_id;
        try {

            $property_details   = OwnerProperty::select('owner_properties.id','property_to',DB::raw('ifnull(selling_price,"0.00") AS selling_price'),'property_reg_no','city',DB::raw('ifnull(user_properties.rent,"0.00") AS amount'),'user_properties.due_date',DB::raw('ifnull(owner_properties.token_amount,"0.00") as token_amount'))
                                                ->join('user_properties', 'user_properties.property_id', 'owner_properties.id')
                                                ->with(
                                                    array('documents' =>
                                                    function ($query) {
                                                        $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",document) AS document'), 'property_id');
                                                    }, 'user_booked_property:id,property_id,status,rent,due_date,document_verified,cancel_status')
                                                )
                                                ->where('owner_properties.id', $property_id)
                                                ->where('user_properties.user_id', Auth::user()->id)
                                                ->where('owner_properties.status', 1)
                                                ->where('user_properties.cancel_status', false)
                                                ->where('user_properties.status', 2)
                                                ->first();
            if ($property_details) {
                $property_details   = $this->detailsNull2Empty($property_details);
                if ($property_details->user_booked_property) {
                    $property_details->user_booked_property = $this->eachNull2Empty($property_details->user_booked_property);
                } else {
                    $property_details->user_booked_property = (object)[];
                }
                if ($property_details->documents == "") {
                    $property_details->documents = (object)[];
                }
            } else {
                $property_details = (object)[];
            }
            return response()->json(['status' => 200, 'property_details'  => $property_details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function uploadUserPropertyDocuments(Request $request)
    {
        $rules = [
            'user_property_id'  => 'required',
            'document.*'          => 'required|mimes:jpg,jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'user_property_id.required' => 'User property is required',
            'document.required' => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $has_booking = UserProperty::where('id', $request->user_property_id)
                ->where('status', 2)
                ->where('cancel_status', false)
                ->first();
            if (!empty($has_booking)) {
                $already_uploaded = UserPropertyDocument::where('user_property_id', $has_booking->id)
                    ->where('document_type', 2)
                    ->first();
                if (empty($already_uploaded)) {
                    $user_property_doc = [];
                    if ($request->file('document')) {
                        foreach ($request->file('document') as $key => $row) {
                            $filename = time() . rand() . '.' . $row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/users/property-documents'), $filename);

                            $user_property_doc[$key]['user_id']          = Auth::user()->id;
                            $user_property_doc[$key]['user_property_id'] = $has_booking->id;
                            $user_property_doc[$key]['document']         = '/uploads/users/property-documents/' . $filename;
                            $user_property_doc[$key]['document_type']    = 2;
                            $user_property_doc[$key]['created_at']       = now();
                            $user_property_doc[$key]['updated_at']       = now();
                        }
                    }
                    if (UserPropertyDocument::insert($user_property_doc)) {
                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    } else {
                        return response()->json(['status' => 400, 'response' => 'Failed to upload documents']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                return response()->json(['status' => 401, 'response' => 'Invalid User Property']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function payBookedProperty(Request $request)
    {
        $rules = [
            'user_property_id'  => 'required',
            'payment_status'   => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'user_property_id.required'      => 'User property is required',
            'payment_status.required' => 'Payment method is required',
            'document.required'        => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $has_booking = UserProperty::where('id', $request->user_property_id)
                ->where('document_verified', 1)
                ->where('cancel_status', false)
                ->first();
            if (!empty($has_booking)) {
                $already_uploaded = UserPropertyDocument::where('user_property_id', $has_booking->id)
                    ->where('document_type', 1)
                    ->first();
                if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        $file = $request->file('document');
                        $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/users/bill-document'), $filename);

                        $user_property_doc                  = new UserPropertyDocument();
                        $user_property_doc->user_id         = Auth::user()->id;
                        $user_property_doc->user_property_id = $has_booking->id;
                        $user_property_doc->document        = '/uploads/users/bill-document/' . $filename;
                        $user_property_doc->payment_status  = $request->payment_status;
                        $user_property_doc->document_type   = 1;
                        $user_property_doc->save();

                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                $already_uploaded = UserPropertyDocument::where('user_property_id', $request->user_property_id)->first();
                if ($already_uploaded) {
                    return response()->json(['status' => 200, 'response' => 'User document not yet verified']);
                } else {
                    return response()->json(['status' => 400, 'response' => 'Invalid User Property']);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function cancelBookedPropertyDetails(Request $request)
    {
        $rules = [
            'user_property_id' => 'required',
        ];
        $messages = [
            'user_property_id.required' => 'User Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_property_id        = $request->user_property_id;
        try {
            $user_id             = Auth::user()->id;
            $user_property       = UserProperty::where('id', $user_property_id)
                ->where('user_id', $user_id)
                ->where('status', 2)
                ->first();
            if ($user_property) {
                $property_id     = $user_property->property_id;
                $user_property->cancel_status = true;
                $user_property->save();
                $user_booked_property   = UserPropertyBookings::where('user_id', $user_id)
                    ->where('property_id', $property_id)
                    ->where('check_in', '>=', date('Y-m-d'))
                    ->where('cancel_status', false)
                    ->first();
                if ($user_booked_property) {
                    $user_booked_property->cancel_status = true;
                    $user_booked_property->save();
                }
                return response()->json(['status' => 200, 'response' => 'Cancelled Successfully']);
            } else {
                return response()->json(['status' => 400, 'response' => 'Invalid User Property']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function listServices(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $filter_service    = $request->service_name;
        $property_id       = $request->property_id;
        $page              = $request->page;
        try {

            $property_service   = ServiceAllocate::select('service_id')
                                                ->where('property_id',$property_id)
                                                ->where('status',1)
                                                ->distinct('service_id')
                                                ->get();

            $property_service   = json_decode($property_service);
            if($property_service){
                $service_ids       = array_column($property_service,'service_id');
            }else{
                $service_ids       = [0];
            }

            $services           = Services::select('id', 'service', DB::raw('CONCAT("' . $this->url->to('/') . '",image) AS image'))
                                            ->whereIn('id',$service_ids);
            if ($filter_service) {
                $services       = $services->where('service', 'like', '%' . $filter_service . '%');
            }
            $total_count = $services->count();

            $items_per_page     = 10;
            $total_page_count   = ceil($total_count / $items_per_page);
            if ($page) {
                $pages_array        = range(1, $total_page_count);
                if (in_array($page, $pages_array)) {
                    $current_page              = strval($page);
                    $offset_page               = $page - 1;
                    $offset_item_count         = $offset_page * $items_per_page;

                    $services    = $services->offset($offset_item_count)->take($items_per_page)->get();
                } else {
                    $current_page              = strval($page);
                    $services    = [];
                }
            } else {
                if ($page == 0 && $page != null) {
                    $current_page              = strval(0);
                    $services                    = [];
                } else {
                    $current_page              = strval(1);
                    $services                    = $services->take($items_per_page)->get();
                }
            }

            $service_data = [
                'total_page_count' => $total_page_count,
                'current_page'     => $current_page,
                'services'         => $services,
            ];
            return response()->json(['status' => 200, 'service_data' => $service_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function sendServiceRequest(Request $request)
    {
        $rules = [
            'user_property_id' => 'required',
            'service_id'      => 'required',
            'request_date'    => 'required|date|after:yesterday',
            'time'            => 'required|date_format:H:i',
            'document'        => 'required',
            'document.*'      => 'mimes:jpeg,png,doc,docx,pdf',
        ];
        $messages = [
            'user_property_id.required'     => 'User property is required',
            'service_id.required'           => 'Service is required',
            'request_date.required'         => 'Date is required',
            'time.required'                 => 'Time is required',
            'document.required'             => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        // try {
            DB::beginTransaction();
            $user_id            = Auth::user()->id;
            $service_id         = $request->service_id;
            $user_property_id   = $request->user_property_id;
            $has_user_property  = userProperty::where('id', $user_property_id)->first();
            if (!$has_user_property) {
                return response()->json(['status' => 400, 'response' => "User property doesn't exist"]);
            }
            $has_service        = Services::where('id', $service_id)->first();
            if (!$has_service) {
                return response()->json(['status' => 400, 'response' => "Service doesn't exist"]);
            }

            $has_requested      = UserPropertyServiceRequests::where('user_id', $user_id)
                                                            ->where('user_property_id', $user_property_id)
                                                            ->where('service_id', $service_id)
                                                            ->where('date', '>=', $request->request_date)
                                                            ->first();

            if (empty($has_requested)) {
                $service_request                    = new UserPropertyServiceRequests();
                $service_request->user_id           = $user_id;
                $service_request->service_id        = $service_id;
                $service_request->user_property_id  = $user_property_id;
                $service_request->date              = date('Y-m-d', strtotime($request->request_date));
                $service_request->time              = $request->time;
                $service_request->description       = $request->description;
                $service_request->status            = 0;
                $service_request->save();

                $services=DB::table('user_property_service_requests')
                ->where('status',0)
                ->join('services','services.id','user_property_service_requests.service_id')
                ->get();
                foreach($services as $service){
                    $notification=new Notifications();
                    $notification->type_of_notification='Service';
                    $notification->user_id=$service->user_id;
                    $notification->request_id=$service->id;
                    $notification->notification_heading=$service->service;
                    $notification->notification_text=$service->description;
                    // $notification->status=$service->;
                    $notification->save();
        
                }

                $request_id   = $service_request->id;

                 $user_property_service_doc  =[];
                if ($request_id) {
                    if ($request->file('document')) {
                        foreach ($request->file('document') as $key => $row) {
                            $filename = time() . rand() . '.' . $row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/users/service-documents'), $filename);
                            $user_property_service_doc[$key]['request_id']       = $request_id;
                            $user_property_service_doc[$key]['document']         = '/uploads/users/service-documents/' . $filename;
                            $user_property_service_doc[$key]['created_at']       = now();
                            $user_property_service_doc[$key]['updated_at']       = now();
                        }
                    }
                    if (UserPropertyServiceRequestDocuments::insert($user_property_service_doc)) {
                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'Request has been sent successfully']);
                    } else {
                        return response()->json(['status' => 400, 'response' => 'Failed to upload documents']);
                    }
                } else {
                    return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
                }
            } else {
                return response()->json(['status' => 200, 'response' => 'Service already requested']);
            }
        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        // }
    }

    public function listRequestedServices(Request $request)
    {
        $rules = [
            'user_property_id' => 'required',
        ];
        $messages = [
            'user_property_id.required' => 'User Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_property_id    = $request->user_property_id;
        $page                = $request->page;
        try {
            $user_id             = Auth::user()->id;
            $user_services       = UserPropertyServiceRequests::select(
                'user_property_service_requests.id',
                'service_id',
                'date',
                'time',
                'user_property_service_requests.status',
                'owner_properties.property_name',
                'user_property_service_requests.user_property_id'
            )
                ->with('service_related:id,service','service_amount:reference_id,amount')
                ->join('user_properties', 'user_properties.id', 'user_property_id')
                ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
                ->where('user_property_id', $user_property_id)
                ->where('user_property_service_requests.user_id', $user_id)
                ->orderBy('user_property_service_requests.date', 'DESC');

            $total_count = $user_services->count();

            $items_per_page     = 10;
            $total_page_count   = ceil($total_count / $items_per_page);
            if ($page) {
                $pages_array    = range(1, $total_page_count);
                if (in_array($page, $pages_array)) {
                    $current_page      = strval($page);
                    $offset_page       = $page - 1;
                    $offset_item_count = $offset_page * $items_per_page;

                    $user_services     = $user_services->offset($offset_item_count)->take($items_per_page)->get();
                } else {
                    $current_page              = strval($page);
                    $user_services             = [];
                }
            } else {
                if ($page == 0 && $page != null) {
                    $current_page              = strval(0);
                    $user_services             = [];
                } else {
                    $current_page              = strval(1);
                    $user_services             = $user_services->take($items_per_page)->get();
                }
            }
            $user_service_data = [
                'total_page_count' => $total_page_count,
                'current_page'     => $current_page,
                'user_services' => $user_services,
            ];
            return response()->json(['status' => 200, 'user_service_data' => $user_service_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function requestedServiceDetails(Request $request)
    {
        $rules = [
            'request_id' => 'required',

        ];
        $messages = [
            'request_id.required'       => 'Request id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_property_id    = $request->user_property_id;
        $request_id          = $request->request_id;


        try {
            $base_url            = url('/');
            $user_id             = Auth::user()->id;

            $user_services       = UserPropertyServiceRequests::select('user_property_service_requests.id','service_id','user_property_service_requests.date',
                                                                        'time','user_property_service_requests.status','user_property_service_requests.description',
                                                                        'owner_properties.property_name')
                                                                ->with('service_related:id,service')
                                                                ->join('user_properties', 'user_properties.id', 'user_property_id')
                                                                ->join('owner_properties', 'owner_properties.id', 'user_properties.property_id')
                                                                ->where('user_property_service_requests.user_id', $user_id)
                                                                ->where('user_property_service_requests.id', $request_id)
                                                                ->first();
            if (!empty($user_services)) {
                $user_services       = $this->eachNull2Empty($user_services);
                if($user_services){
                    $request_id     = $user_services->id;
                    $estimate       = UserPropertyServiceRequestDocuments::select('id',DB::raw('CONCAT("'.$base_url.'",document) AS document'))
                                                                        ->where('request_id',$request_id)
                                                                        ->where('document_type',2)
                                                                        ->first();
                    if($estimate){
                        $user_services->estimate_file     = $estimate->document;
                    }else{
                        $user_services->estimate_file     = "";
                    }
                }else{
                    $user_services->estimate_file     = "";
                }
            } else {
                $user_services       = (object)[];
            }

            return response()->json(['status' => 200, 'user_service' => $user_services]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function payUserPropertyService(Request $request)
    {
        $rules = [
            'request_id'             => 'required',
            'payment_status'         => 'required',
            'document'               => 'required|mimes:jpeg,png,pdf,doc,docx',
            'owner_payment_checked'  => 'required'
        ];
        $messages = [
            'request_id.required'               => 'Request id is required',
            'payment_status.required'           => 'Payment method is required',
            'document.required'                 => 'Document is required',
            'owner_payment_checked.required'    => 'Owner Payment is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $user_id     = Auth::user()->id;
            DB::beginTransaction();
            $has_request = UserPropertyServiceRequests::where('id', $request->request_id)
                ->where('user_id', $user_id)
                ->where('status', 1)
                ->first();
                // dd( $has_request);
            if (!empty($has_request)) {
                $already_uploaded = UserPropertyServiceRequestDocuments::where('request_id', $has_request->id)
                    ->where('document_type', 1)
                    ->first();
                if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        $file = $request->file('document');
                        $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/users/service-documents'), $filename);

                        $user_service_doc                   = new UserPropertyServiceRequestDocuments();
                        $user_service_doc->request_id       = $has_request->id;
                        $user_service_doc->document         = '/uploads/users/service-documents/' . $filename;
                        $user_service_doc->payment_status   = $request->payment_status;
                        $user_service_doc->owner_payment_checked  = $request->owner_payment_checked;
                        $user_service_doc->document_type   = 1;
                    
                         $user_service_doc->save();

                        // UserPropertyServiceRequests::where('id', $request->request_id)->update(['status'=>2]);

                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                $already_uploaded = UserPropertyServiceRequestDocuments::where('request_id', $request->request_id)->where('document_type', 1)->first();
                if ($already_uploaded) {
                    return response()->json(['status' => 200, 'response' => 'User Request not yet verified']);
                } else {
                    return response()->json(['status' => 400, 'response' => 'Invalid User Property']);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function cancelRequestedService(Request $request)
    {
        $rules = [
            'request_id' => 'required',
        ];
        $messages = [
            'request_id.required' => 'Request id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $request_id        = $request->request_id;
            $user_id             = Auth::user()->id;
            $user_request       = UserPropertyServiceRequests::where('id', $request_id)
                ->where('user_id', $user_id)
                ->where('status', '!=', 3)
                ->first();
            if ($user_request) {
                $has_cancel             = CancelUserServiceRequest::where('request_id', $request_id)
                    ->where('user_id', $user_id)
                    ->first();
                if (empty($has_cancel)) {
                    $cancel_request                 = new CancelUserServiceRequest();
                    $cancel_request->user_id        = $user_id;
                    $cancel_request->request_id     = $request_id;
                    $cancel_request->admin_approve  = 0;
                    $cancel_request->save();
                    return response()->json(['status' => 200, 'response' => 'Cancel request has been processed']);
                } else {
                    return response()->json(['status' => 200, 'response' => 'Cancel request already been processed']);
                }
            } else {
                return response()->json(['status' => 400, 'response' => 'Invalid Service Request']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function userPropertyVacateRequest(Request $request)
    {
        $rules = [
            'user_property_id'     => 'required',
            'vacate_date'          => 'required|date|after:yesterday',
        ];
        $messages = [
            'user_property_id.required' => 'User property is required',
            'vacate_date.required'      => 'Vacate date is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $user_property_id    = $request->user_property_id;
            $user_id             = Auth::user()->id;

            $has_user_property  = UserProperty::where('id', $user_property_id)->first();
            if (!$has_user_property) {
                return response()->json(['status' => 400, 'response' => "User property doesn't exist"]);
            }

            $has_vacate_requested      = UserPropertyVacateRequest::where('user_id', $user_id)
                ->where('user_property_id', $user_property_id)
                ->first();

            if (empty($has_vacate_requested)) {
                $service_request                    = new UserPropertyVacateRequest();
                $service_request->user_id           = $user_id;
                $service_request->user_property_id  = $user_property_id;
                $service_request->vacating_date     = date('Y-m-d', strtotime($request->vacate_date));
                $service_request->status            = 0;

                $service_request->save();

                DB::commit();
                return response()->json(['status' => 200, 'response' => 'Vacate Request has been processed']);
            } else {
                return response()->json(['status' => 200, 'response' => 'Already requested for vacate']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function listUserVacateRequest(Request $request)
    {
        $rules = [
            'user_property_id'     => 'required',
        ];
        $messages = [
            'user_property_id.required' => 'User property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $page                = $request->page;
        try {
            $user_id             = Auth::user()->id;
            $property_id         = $request->user_property_id;
            $user_vacate_request       = UserPropertyVacateRequest::select('id as vacate_request_id', 'user_property_id', 'vacating_date')
                ->with('user_property_related:property_name,property_id')
                ->where('user_property_vacate_request.user_id', $user_id)
                ->where('user_property_id',$property_id)
                ->orderBy('vacating_date', 'DESC');
            $total_count = $user_vacate_request->count();

            $items_per_page     = 10;
            $total_page_count   = ceil($total_count / $items_per_page);
            if ($page) {
                $pages_array    = range(1, $total_page_count);
                if (in_array($page, $pages_array)) {
                    $current_page            = strval($page);
                    $offset_page             = $page - 1;
                    $offset_item_count       = $offset_page * $items_per_page;

                    $user_vacate_request     = $user_vacate_request->offset($offset_item_count)->take($items_per_page)->get();
                } else {
                    $current_page            = strval($page);
                    $user_vacate_request     = [];
                }
            } else {
                if ($page == 0 && $page != null) {
                    $current_page              = strval(0);
                    $user_vacate_request       = [];
                } else {
                    $current_page              = strval(1);
                    $user_vacate_request       = $user_vacate_request->take($items_per_page)->get();
                }
            }
            $user_vacate_request    = $this->allNull2Empty($user_vacate_request);

            if (!empty($user_vacate_request)) {
                foreach ($user_vacate_request as $each_request) {
                    $each_request->user_property_related    = $this->eachNull2Empty($each_request->user_property_related);
                }
            }

            $user_vacate_request_data = [
                'total_page_count' => $total_page_count,
                'current_page'     => $current_page,
                'user_vacate_requests' => $user_vacate_request,
            ];
            return response()->json(['status' => 200, 'user_vacate_data' => $user_vacate_request_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function vacateRequestDetails(Request $request)
    {
        $rules = [
            'vacate_id'          => 'required',
        ];
        $messages = [
            'vacate_id.required' => 'Vacate id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $vacate_id          = $request->vacate_id;
        //try {
            $user_id             = Auth::user()->id;
            $user_vacate_request       = UserPropertyVacateRequest::select('id', 'vacating_date', 'user_property_id', 'user_property_vacate_request.status',)
                ->with('user_property_related:property_name,property_id,property_reg_no,contract_start_date,contract_end_date,owner_properties.status,security_deposit,user_properties.rent as due_amount')
                ->where('user_id', $user_id)
                ->where('id', $vacate_id)
                ->first();

            if (!empty($user_vacate_request)) {
                $user_vacate_request       = $this->eachNull2Empty($user_vacate_request);
                if ($user_vacate_request->user_property_related) {
                    $user_vacate_request->user_property_related = $this->eachNull2Empty($user_vacate_request->user_property_related);
                } else {
                    $user_vacate_request->user_property_related = (object)[];
                }
            } else {
                $user_vacate_request       = (object)[];
            }
            return response()->json(['status' => 200, 'user_vacate_request' => $user_vacate_request]);
        // } catch (\Exception $e) {
        //     return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        // }
    }



    public function userPropertyDetails(Request $request)
    {
        $rules = [
            'user_property_id'     => 'required',
        ];
        $messages = [
            'user_property_id.required' => 'User property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_property_id          = $request->user_property_id;
        try {
            $user_id             = Auth::user()->id;
            $user_property       = UserProperty::select(
                'user_properties.id',
                'user_properties.property_id',
                'user_properties.status',
                'user_properties.cancel_status',
                'user_properties.document_verified',
                'user_property_bookings.check_in',
                'user_property_bookings.check_out',
                DB::raw('CONCAT("' . $this->url->to('/') . '",owner_property_documents.document) AS document'),
                 'owner_property_documents.type'
            )
                ->join('user_property_bookings', 'user_property_bookings.property_id', 'user_properties.property_id')
                ->leftJoin('owner_property_documents', 'owner_property_documents.property_id', 'user_properties.property_id')
                //->with('user_property_related:property_name,id,property_reg_no,status,property_to,rent','prop_rel:id,frequency')
                ->with(array(
                    'contract_rel' =>
                    function ($query) {
                        $query->select('user_property_id', DB::raw('CONCAT("' . $this->url->to('/') . '",contract_file)
                                                        AS contract_file'));
                    },'user_property_related:property_name,id,property_reg_no,status,property_to,rent',
                    'prop_rel:id,frequency'
                    ))
                ->where('user_properties.user_id', $user_id)
                ->where('user_property_bookings.user_id', $user_id)
                 ->where('user_property_bookings.cancel_status', false)
                //->where('user_property_bookings.approve_status', true)
                ->where('user_properties.id', $user_property_id)
                // ->where('user_properties.document_verified', true)
                ->where('user_properties.cancel_status', false)
                ->first();

             $user_property->due_date = '';

            if (!empty($user_property)) {
                $user_property       = $this->eachNull2Empty($user_property);
                if ($user_property->user_property_related) {
                    $user_property->user_property_related = $this->eachNull2Empty($user_property->user_property_related);
                } else {
                    $user_property->user_property_related = (object)[];
                }
                if ($user_property->type) {
                    if ($user_property->type != 0) {
                        $user_property->document = "";
                    }
                }
                if($user_property->prop_rel){
                    $payment = AdminAccount::select('payment_type','date')->where('property_id',$user_property->prop_rel->id)->get();

                    if($payment->contains('payment_type', '5'))
                    {
                        $pay_date = AdminAccount::select('payment_type','date')->where('payment_type','5')->where('property_id',$user_property->prop_rel->id)->orderBy('id', 'DESC')->first();
                    }
                    else
                    {
                        $pay_date = AdminAccount::select('payment_type','date')->where('payment_type','2')->where('property_id',$user_property->prop_rel->id)->orderBy('id', 'DESC')->first();

                    }
                    // $pay_date = AdminAccount::select('payment_type','date')->first();
                    // if($pay_date->payment_type == '5')
                    // {
                    //     $pay_date->where('payment_type','5');
                    // }
                    // else
                    // {
                    //     $pay_date->where('payment_type','2');
                    // }
                  //  $pay_date->orderBy('id', 'DESC')->first();
                    if($pay_date)
                    {
                        $date =  $pay_date->date;
                        if($user_property->prop_rel)
                        {
                            $no_of_days = Frequency::select('id','days')->where('id',$user_property->prop_rel->frequency )->first();
                            // Add days to date and display it
                            $user_property->due_date = date('Y-m-d', strtotime($date. ' + '. $no_of_days->days. '  days'));
                        }
                    }
                }


            } else {
                $user_property       = (object)[];
            }


            unset($user_property->prop_rel);
            return response()->json(['status' => 200, 'user_property' => $user_property]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function payUserPropertyRent(Request $request)
    {
        $rules = [
            'user_property_id'  => 'required',
            'payment_status'    => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'user_property_id.required'      => 'User property is required',
            'payment_status.required'        => 'Payment method is required',
            'document.required'              => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $user_id     = Auth::user()->id;
            DB::beginTransaction();
            $has_booking = UserProperty::where('id', $request->user_property_id)
                ->where('user_id', $user_id)
                //->where('document_verified', 1)
                ->where('cancel_status', false)
                ->first();
            if (!empty($has_booking)) {
                $property_id      = $has_booking->property_id;
                $property_details = OwnerProperty::where('id', $property_id)->first();

                if (empty($property_details)) {
                    return response()->json(['status' => 400, 'response' => 'Invalid Property']);
                }

                if ($property_details->rent == null) {
                    return response()->json(['status' => 400, 'response' => 'This property is not for rent']);
                }

                $already_uploaded = UserPropertyRentDocuments::where('user_property_id', $has_booking->id)
                    ->where('property_id', $property_id)
                    ->where('payment_check', 0)
                    ->first();
                 if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        $file = $request->file('document');
                        $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/users/rent-documents'), $filename);

                        $user_rent_doc                  = new UserPropertyRentDocuments();
                        $user_rent_doc->user_id         = Auth::user()->id;
                        $user_rent_doc->user_property_id = $has_booking->id;
                        $user_rent_doc->property_id     = $property_id;
                        $user_rent_doc->document        = '/uploads/users/rent-documents/' . $filename;
                        $user_rent_doc->payment_status  = $request->payment_status;
                        $user_rent_doc->save();

                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                return response()->json(['status' => 400, 'response' => 'Invalid User Property']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function sendFeedback(Request $request)
    {
        $rules = [
            'feedback'  => 'required',
        ];
        $messages = [
            'feedback.required'      => 'Feedback is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $user_id     = Auth::user()->id;
            $feedback    = $request->feedback;
            DB::beginTransaction();

            $user_feedback              = new UserFeedbacks();
            $user_feedback->user_id     = $user_id;
            $user_feedback->feedback    = $feedback;
            $user_feedback->save();

            DB::commit();
            return response()->json(['status' => 200, 'response' => 'Feedback sent successfully']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function showRentalDetails(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $property_id    = $request->property_id;
            $rent_details   = OwnerProperty::select('rent', 'security_deposit', 'frequency')
                                            ->with('property_rent_frequency:id,type,status')
                                            ->where('id', $property_id)
                                            ->where('status', 1)
                                            // ->where('contract_owner', '1')
                                            ->where('property_to', 0)
                                            ->first();

            if ($rent_details) {
                $rent_details   = $this->detailsNull2Empty($rent_details);
                if (!$rent_details->property_rent_frequency) {
                    $rent_details->property_rent_frequency = (object)[];
                }
            } else {
                $rent_details = (object)[];
            }
            return response()->json(['status' => 200, 'rent_details' => $rent_details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function removeUserProfilePic(Request $request)
    {
        try {
            $user_id                   = Auth::user()->id;
            $update_user               = User::where('id', $user_id)->first();
            if ($update_user->profile_pic) {
                if (file_exists(public_path($update_user->profile_pic))) {
                    unlink(public_path($update_user->profile_pic));
                }
            }
            $update_user->profile_pic = NULL;
            $update_user->save();
            return response()->json(['status' => 200, 'response' => "Removed profile successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function listFAQ(Request $request)
    {
        try {
            $page   = $request->page;
            $lang   = $request->lang;
            if ($lang == 1) {
                $faqs   = FaqDetails::select('id', 'english_question as question', 'english_answer as answer');
            } else {
                $faqs   = FaqDetails::select('id', 'arabic_question as question', 'arabic_answer as answer');
            }

            $faqs   = $this->customPagination($faqs, $page);

            $faq_data = [
                'total_page_count' => $faqs['total_page_count'],
                'current_page'     => $faqs['current_page'],
                'faqs'             => $faqs['data'],
            ];

            return response()->json(['status' => 200, 'faq_data' => $faq_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function listPrivacyPolicy(Request $request)
    {
        // try {
        //     $page   = $request->page;
        //     $privacy_policies   = PrivacyPolicy::select('id', 'title', 'description')
        //         ->where('status', true);


        //     $privacy_policies   = $this->customPagination($privacy_policies, $page);

        //     $privacy_policy_data = [
        //         'total_page_count' => $privacy_policies['total_page_count'],
        //         'current_page'     => $privacy_policies['current_page'],
        //         'privacy_policies' => $privacy_policies['data'],
        //     ];

        //     return response()->json(['status' => 200, 'privacy_policy_data' => $privacy_policy_data]);
        // } catch (\Exception $e) {
        //     return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        // }
    }

    public function listLegalInformations(Request $request)
    {
        // try {
        //     $page   = $request->page;
        //     $legal_informations   = LegalInformation::select('id', 'title', 'description')->where('status', true);


        //     $legal_informations   = $this->customPagination($legal_informations, $page);

        //     $legal_information_data = [
        //         'total_page_count'      => $legal_informations['total_page_count'],
        //         'current_page'          => $legal_informations['current_page'],
        //         'legal_informations'    => $legal_informations['data'],
        //     ];

        //     return response()->json(['status' => 200, 'legal_information_data' => $legal_information_data]);
        // } catch (\Exception $e) {
        //     return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        // }
    }


    public function customPagination($data_array, $page)
    {
        $total_event_count  = $data_array->count();

        $items_per_page     = 10;
        $total_page_count   = ceil($total_event_count / $items_per_page);
        if ($page) {
            $pages_array        = range(1, $total_page_count);
            if (in_array($page, $pages_array)) {
                $current_page       = strval($page);
                $offset_page        = $page - 1;
                $offset_item_count  = $offset_page * $items_per_page;

                $data_array         = $data_array->offset($offset_item_count)->take($items_per_page)->get();
            } else {
                $current_page       = strval($page);
                $data_array         = [];
            }
        } else {
            if ($page == 0 && $page != null) {
                $current_page       = strval(0);
                $data_array         = [];
            } else {
                $current_page       = strval(1);
                $data_array         = $data_array->take($items_per_page)->get();
            }
        }

        if (count($data_array) > 0) {
            $data_array  = $this->allNull2Empty($data_array);
        }

        $return_array = [
            'current_page'      => $current_page,
            'total_page_count'  => $total_page_count,
            'data'              => $data_array
        ];

        return $return_array;
    }


    //Event Packages

    public function showEventPackages(Request $request)
    {
        $rules = [
            'event_id' => 'required|integer',
        ];
        $messages = [
            'event_id.required' => 'Event is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $event_id   = $request->event_id;
        try {
            $event     = Event::select('id')
                ->with(array(
                    'event_packages.event_package_features' =>
                    function ($query) {
                        $query->select('package_id', 'feature_name', 'feature_description');
                    }
                ))
                ->with(array(
                    'event_packages.event_package_image' =>
                    function ($query) {
                        $query->select('package_id', DB::raw('CONCAT("' . $this->url->to('/') . '",package_images.package_image)
                                        AS package_image'));
                    }
                ))
                ->with('event_packages:id,event_id,package_name,price')
                ->where('id', $event_id)
                ->first();

            if ($event) {
                if ($event->event_packages) {
                    foreach ($event->event_packages as $package) {
                        if ($package->event_package_image == null) {
                            unset($package->event_package_image);
                            $package->event_package_image  = (object)[];
                        }
                    }
                }
            }


            return response()->json(['status' => 200, 'event_packages_data' => $event]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
    public function eventBookingDetails(Request $request)
    {
        $rules = [
            'package_id' => 'required|integer',
        ];
        $messages = [
            'package_id.required' => 'Package id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $package_id   = $request->package_id;
        try {
            $package_details    = EventPackages::select('id', 'package_name', 'price')
                                                ->with(array(
                                                    'event_package_image' =>
                                                    function ($query) {
                                                        $query->select('package_id', DB::raw('CONCAT("' . $this->url->to('/') . '",package_image)
                                                                                        AS package_image'));
                                                    }
                                                ))
                                                ->where('id', $package_id)
                                                ->first();

            if ($package_details) {
                if (!$package_details->event_package_image) {
                    unset($package_details->event_package_image);
                    $package_details->event_package_image  = (object)[];
                }
            }

            $user_details       = User::select('name', 'email', 'phone')
                                        ->where('id', Auth::user()->id)
                                        ->first();

            $data               = [
                'event_packages_data'   => $package_details,
                'user_data'             => $user_details,
            ];


            return response()->json(['status' => 200, 'response_data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function bookEventPackage(Request $request)
    {
        $rules = [
            'package_id'        => 'required|integer',
            'name'              => 'required',
            'email'             => 'required|email',
            'phone'             => 'required',
            'amount'            => 'required',
            'person_count'      => 'required|integer',
        ];
        $messages = [
            'package_id.required'   => 'Package id is required',
            'name.required'         => 'Name is required',
            'email.required'        => 'Email is required',
            'phone.required'        => 'Phone number is required',
            'amount.required'       => 'Amount is required',
            'person_count.required' => 'Number of person is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $package_id   = $request->package_id;
        $user_id      = Auth::user()->id;
        try {
            DB::beginTransaction();
            $package_details    = EventPackages::select('id', 'event_id', 'package_name', 'price')
                                                ->where('id', $package_id)
                                                ->first();
            $already_booked    = UserEventBooking::where('user_id', $user_id)
                                                ->where('package_id', $package_id)
                                                ->first();
            // if($already_booked){
            //     dd($already_booked);
            // }

            if ($package_details) {
                $user_event_booking                 = new UserEventBooking();
                $user_event_booking->package_id     = $package_id;
                $user_event_booking->user_id        = $user_id;
                $user_event_booking->name           = $request->name;
                $user_event_booking->email          = $request->email;
                $user_event_booking->phone          = $request->phone;
                $user_event_booking->price          = $request->amount;
                $user_event_booking->person_count   = $request->person_count;
                $user_event_booking->save();

                DB::commit();
                return response()->json([
                    'status' => 200, 'response' => 'Event booking is under progress', 'event_booking_id' => $user_event_booking->id
                ]);
            } else {
                return response()->json(['status' => 400, 'response' => "This package doesn't exist"]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }



    public function uploadEventBookingDocument(Request $request)
    {
        $rules = [
            'event_booking_id'  => 'required',
            'payment_status'    => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'event_booking_id.required' => 'Event Booking id is required',
            'payment_status.required'   => 'Payment method is required',
            'document.required'         => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $has_booking = UserEventBooking::where('id', $request->event_booking_id)->first();
            if (!empty($has_booking)) {
                $already_uploaded = UserEventBookingDocuments::where('event_booking_id', $has_booking->id)->first();
                if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        $file = $request->file('document');
                        $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/users/event-document'), $filename);

                        $book_doc                   = new UserEventBookingDocuments();
                        $book_doc->document         = '/uploads/users/event-document/' . $filename;
                        $book_doc->user_id          = Auth::user()->id;
                        $book_doc->event_booking_id = $has_booking->id;
                        $book_doc->payment_status   = $request->payment_status;
                        $book_doc->save();

                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                return response()->json(['status' => 400, 'response' => 'Invalid Booking']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function fetchDataToRequestDesiredProperty(Request $request)
    {
        try{
            $property_category = PropertyType::select('id','type','category')->get();

            $price_range    =[
                'min_price'     => 0,
                'max_price'     => 100000000000,
            ];

            $data  = [
                'property_category' => $property_category,
                'price_range' => $price_range,
            ];

            return response()->json(['status'=>200,'response_data'=>$data]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function userRequestingDesiredProperty(Request $request)
    {
        $rules = [
            'property_to'       => 'required',
            'category'          => 'required',
            'type_id'           => 'required',
            'area'              => 'required',
            'min_price'         => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'max_price'         => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'latitude'          => 'required',
            'longitude'         => 'required',
        ];
        $messages = [
            'property_to.required'      => 'Property to is required',
            'category.required'         => 'Building type is required',
            'type_id.required'          => 'Category is required',
            'area.required'             => 'Area is required',
            'min_price.required'        => 'Min. Price is required',
            'max_price.required'        => 'Max. Price is required',
            'latitude.required'         => 'Latitude is required',
            'longitude.required'        => 'Longitude is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $user_id      = Auth::user()->id;
        try{
            DB::beginTransaction();

            $maxid      = UserDesiredProperty::max('request_id_no');
            if($maxid){
                $exid = explode('-',$maxid)[1] + 1;
                $property_request_no = '#DPR'.time().'-'.$exid;
            }else{
                $property_request_no = '#DPR'.time().'-'.'100';
            }

            $request_property                        = new UserDesiredProperty();
            $request_property->request_id_no         = $property_request_no;
            $request_property->user_id               = $user_id;
            $request_property->property_to           = $request->property_to;
            $request_property->category              = $request->category;
            $request_property->type_id               = $request->type_id;
            $request_property->area                  = $request->area;
            $request_property->request_date          = date('Y-m-d');
            $request_property->min_price             = $request->min_price;
            $request_property->max_price             = $request->max_price;
            $request_property->latitude              = $request->latitude;
            $request_property->longitude             = $request->longitude;
            $request_property->location             = $request->location?$request->location:null;
            $request_property->description           = $request->description;
            $request_property->save();

            DB::commit();
            return response()->json(['status'=>200,'response'=>'Your request have been sent successfully']);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function listDesiredPropertyRequest(Request $request)
    {
        $page       = $request->page;
        $user_id    = Auth::user()->id;
        try{
            $requested_list     = UserDesiredProperty::select('id','request_id_no')
                                                    ->orderBy('id','DESC')
                                                    ->where('user_id',$user_id);
            $requested_list     = $this->customPagination($requested_list,$page);

            $desired_request_data = [
                'total_page_count' => $requested_list['total_page_count'],
                'current_page'     => $requested_list['current_page'],
                'requested_list'   => $requested_list['data'],
            ];

            return response()->json(['status'=>200,'data'=>$desired_request_data]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function sendBecomeAnOwnerRequest(Request $request)
    {
        $rules = [
            'name'                    => 'required',
            'phone'                   => 'required',
            'email'                   => 'required|email',
            'no_of_rental_properties' => 'required',
            'no_of_sale_properties'   => 'required',
            'property_rel'            => 'required',
        ];
        $messages = [
            'name.required'                       => 'Name is required',
            'phone.required'                      => 'Phone is required',
            'email.required'                      => 'Email is required',
            'no_of_rental_properties.required'    => 'No of Rental Properties is required',
            'no_of_sale_properties.required'      => 'No of Sale Properties is required',
            'property_rel.required'               => 'Property Relation is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $user_id                    = Auth::user()->id;
            $name                       = $request->name;
            $phone                      = $request->phone;
            $email                      = $request->email;
            $no_of_rental_properties    = $request->no_of_rental_properties;
            $no_of_sale_properties      = $request->no_of_sale_properties;
            $property_rel               = $request->property_rel;
            DB::beginTransaction();

            $become_owner                           = new BecomeOwner();
            $become_owner->user_id                  = $user_id;
            $become_owner->name                     = $name;
            $become_owner->phone                    = $phone;
            $become_owner->email                    = $email;
            $become_owner->no_of_rental_properties  = $no_of_rental_properties;
            $become_owner->no_of_sale_properties    = $no_of_sale_properties;
            $become_owner->property_rel             = $property_rel;
            $become_owner->save();

            DB::commit();
            return response()->json(['status'=>200,'response'=>'Request sent successfully']);

        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }



    public function desiredPropertyRequestDetails(Request $request)
    {
        $rules = [
            'request_id'  => 'required',
        ];
        $messages = [
            'request_id.required' => 'Request id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_id    = Auth::user()->id;
        $request_id = $request->request_id;
        try{
            $request_details     = UserDesiredProperty::select('id','request_id_no','property_to','category','type_id','area','request_date','min_price','max_price',
                                                                DB::raw('ifnull(latitude,"0.00") AS latitude'),DB::raw('ifnull(longitude,"0.00") AS longitude'),
                                                                DB::raw('ifnull(location,"") AS location'),'description','status')
                                                    ->with('property_type:id,type')
                                                    ->where('id',$request_id)
                                                    ->where('user_id',$user_id)
                                                    ->first();
            $request_details     = $this->detailsNull2Empty($request_details);

            $assigned_property   = AssignedUserDesiredProperty::select('property_id')
                                                                ->join('user_desired_properties','user_desired_properties.id','assigned_user_desired_properties.desired_id')
                                                                ->where('desired_id',$request_id)
                                                                ->where('user_id',$user_id)
                                                                ->where('assigned_user_desired_properties.status',true)
                                                                ->get();
            $assigned_property   = json_decode($assigned_property);
            if($assigned_property){
                $desired_property_ids       = array_column($assigned_property,'property_id');
            }else{
                $desired_property_ids       = [];
            }

            $desired_properties = OwnerProperty::select('id', 'property_to','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),DB::raw('ifnull(selling_price,"") as selling_price'),
                                                    DB::raw('ifnull(mrp,"") as mrp'),DB::raw('ifnull(latitude,"0.00") AS latitude'),DB::raw('ifnull(longitude,"0.00") AS longitude'),
                                                    DB::raw('ifnull(property_type,"") as property_type'),DB::raw('ifnull(rent,"") as rent'),'rating','is_featured',)
                                            ->with(
                                                array('property_priority_image' =>
                                                    function ($query) {
                                                        $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",document) AS document'), 'property_id');
                                                    },'property_details:property_id,detail_id,value,name',
                                                       'userfavourites:property_id'
                                                )
                                            )
                                            ->whereIn('id', $desired_property_ids)
                                            // ->where('contract_owner', 1)
                                            ->where('status', 1)
                                            ->get();
            if($desired_properties){
                foreach($desired_properties as $key => $property){
                    if(!$property->property_priority_image){
                        unset($property->property_priority_image);
                        $property->property_priority_image = (object)[];
                    }
                }
            }

            $details    = Detail::select('id', 'name')->get();

            $desired_properties = $this->similarPropertyList($desired_properties, $details);

            $data                = [
                    'request_details'       => $request_details,
                    'desired_properties'    => $desired_properties,

            ];

            return response()->json(['status'=>200,'data'=>$data]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function listAllAvailableProperties(Request $request)
    {
        $latitude   = $request->lat;
        $longitude  = $request->lan;
        $wish_list  = $request->favourite_list;
        $page       = $request->page;
        try {
            $base_url   = url('/');
            $details    = Detail::select('id', 'name')->get();

            if ($latitude && $longitude) {
                // $first_properties = OwnerProperty::'id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),'property_type','owner_id','rating','property_to',
                                                    // 'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent')
                //                                     , DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                //                                     * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                //                                     + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
                $properties = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),'property_type','owner_id','rating','property_to',
                                                    'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'));
                $first_properties = $properties->where('status', '1')
                                        ->where('contract_owner', '1')
                                        // ->orderBy('distance', 'asc')
                                        ->with(array('documents'=>function($query) use($base_url){
                                                    $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                },'property_details:property_id,detail_id,value,name',
                                                'userfavourites:property_id'
                                            )
                                        )
                                        ->orderBy('is_featured', 'DESC')
                                        ->limit(10)
                                        ->get();
                $first_properties = $this->similarPropertyList($first_properties, $details);

                $events     = Event::select('id','name',DB::raw('ifnull(short_description,"") as short_description'),
                                            DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                            * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                            + sin(radians(" . $latitude . ")) * sin(radians(latitude))) AS distance"))
                                    ->with(
                                        array(
                                            'event_priority_image' =>
                                            function ($query) {
                                                $query->select(DB::raw('CONCAT("' . $this->url->to('/') . '",image) AS image'), 'event_id');
                                            }
                                        )
                                    )
                                    ->orderBy('distance', 'ASC')
                                    ->where('status', 1)
                                    ->limit(6)
                                    ->get();

                foreach ($events as $event) {
                    if ($event->event_priority_image == "") {
                        $event->event_priority_image = (object)[];
                    }
                }

                // $properties = OwnerProperty::select('id','property_reg_no','property_name','property_type','owner_id','agent_id','rating',
                //                                     'property_to','category','selling_price','mrp','is_featured','rent'
                //                                     , DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                //                                     * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                //                                     + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
                $properties = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),DB::raw('ifnull(property_type,"") as property_type'),'owner_id','rating','property_to',
                                                    'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'));
                $last_properties = $properties->where('status', '1')
                                        ->where('contract_owner', '1')
                                        // ->orderBy('distance', 'asc')
                                        ->with(array('documents'=>function($query) use($base_url){
                                                    $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                },'property_details:property_id,detail_id,value,name',
                                                'userfavourites:property_id'
                                            )
                                        )
                                        ->orderBy('is_featured', 'DESC');
                $total_property_count  = $last_properties->count();

                $items_per_page     = 10;
                $total_page_count   = ceil($total_property_count-10/ $items_per_page);
                if ($page) {
                    $pages_array        = range(1, $total_page_count);
                    if (in_array($page, $pages_array)) {
                        $current_page       = strval($page);
                        $offset_page        = $page - 1;
                        $offset_item_count  = $offset_page * $items_per_page;

                        $last_properties             = $last_properties->offset($offset_item_count+10)->take($items_per_page)->get();
                    } else {
                        $current_page       = strval($page);
                        $last_properties             = [];
                    }
                } else {
                    if ($page == 0 && $page != null) {
                        $current_page       = strval(0);
                        $last_properties             = [];
                    } else {
                        $current_page       = strval(1);
                        $last_properties    = $last_properties->offset(10)->take($items_per_page)->get();
                    }
                }

                $last_properties = $this->similarPropertyList($last_properties, $details);

                $last_property_data = [
                    'total_page_count' => $total_page_count,
                    'current_page'     => $current_page,
                    'last_properties'  => $last_properties,
                ];

                $data = [
                    'first_property_data' => $first_properties,
                    'events'              => $events,
                    'last_property_data' => $last_property_data,
                ];



                return response()->json(['status' => 200, 'data' => $data]);
            } elseif ($wish_list) {
                $properties = OwnerProperty::select('owner_properties.id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),DB::raw('ifnull(property_type,"") as property_type'),'owner_id','rating','property_to',
                                                    'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'))
                                            ->where('owner_properties.status', '1')
                                            ->with(array('documents'=>function($query) use($base_url){
                                                        $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                    },'property_details:property_id,detail_id,value,name',
                                                    'userfavourites:property_id'
                                                )
                                            )
                                            ->join('user_favourites', 'user_favourites.property_id', 'owner_properties.id')
                                            ->where('user_favourites.status', 1)
                                            ->orderBy('is_featured', 'DESC');

                                            // $details    = Detail::select('id', 'name')->get();


                $properties = $this->customPagination($properties,$page);

                $properties['data'] = $this->similarPropertyList($properties['data'], $details);

                $faviourate_properties = [
                    'total_page_count'      => $properties['total_page_count'],
                    'current_page'          => $properties['current_page'],
                    'faviourate_properties' => $properties['data'],
                ];



                return response()->json(['status' => 200, 'property_data' => $faviourate_properties]);
            } else {
                return response()->json(['status' => 400, 'response' => 'Sorry, please provide proper params']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }



    public function generateUserReferalCode()
    {
        $num = mt_rand(0, 999);
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $referral_code = 'SAJID' . $num . $randomString ;
        try {
            $user_data = User::where('referral_code', $referral_code)->first();
            if (!empty($user_data)) {
                $this->generateUserReferalCode();
            } else {
                return $referral_code;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function getReferalCode(Request $request)
    {
        $user_id    = Auth::user()->id;
        try{
            $user_referral     = User::select('referral_code')->where('id',$user_id)->first();
            if($user_referral->referral_code){
                $referral_code  = $user_referral->referral_code;
            }else{
                $referral_code       = $this->generateUserReferalCode();
                User::where('id',$user_id)->update(['referral_code' => $referral_code]);
            }

            return response()->json(['status'=>200,'referral_code' => $referral_code]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function showListingPropertyLocation(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id        = $request->property_id;
        try {
            $base_url       = url('/');
            $property       = OwnerProperty::select('id',DB::raw('ifnull(property_name,"") AS property_name'),DB::raw('ifnull(property_type,"") AS property_type')
                                                    ,'rating','property_to',DB::raw('ifnull(latitude, 0.0) AS latitude'), DB::raw('ifnull(longitude, 0.0) AS longitude')
                                                    ,DB::raw('ifnull(rent,0.0) AS rent'),DB::raw('ifnull(selling_price,0.0) AS selling_price'))
                                            ->with(array('property_priority_image'=>function($query) use($base_url){
                                                                    $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                                },'property_details:property_id,detail_id,value,name',
                                                            )
                                            )
                                            ->where('id',$property_id)
                                            ->where('status','1')
                                            ->where('contract_owner','1')
                                            ->first();

            $details          = Detail::select('id', 'name')->get();
            if($property){
                if($property->property_details){
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($property->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $property->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $property->$detail_data = 0;
                        }
                    }
                    unset($property->property_details);
                }
            }else{
                $property       = (object)[];
            }
            return response()->json(['status' => 200, 'property_details' => $property]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function listAllNearByPropertyLocations(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
            'latitude'  => 'required',
            'longitude'  => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
            'latitude.required'  => 'Latitude is required',
            'longitude.required'  => 'Longitude is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id        = $request->property_id;
        $latitude           = $request->latitude;
        $longitude          = $request->longitude;
        try {
            $base_url       = url('/');
            $property       = OwnerProperty::select('id',DB::raw('ifnull(property_name,"") AS property_name'),DB::raw('ifnull(property_type,"") AS property_type')
                                                    ,'rating','property_to',DB::raw('ifnull(latitude, 0.0) AS latitude'), DB::raw('ifnull(longitude, 0.0) AS longitude')
                                                    ,DB::raw('ifnull(rent,0.0) AS rent'),DB::raw('ifnull(selling_price,0.0) AS selling_price'))
                                            ->with(array('property_priority_image'=>function($query) use($base_url){
                                                                    $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                                },'property_details:property_id,detail_id,value,name',
                                                            )
                                            )
                                            ->where('id',$property_id)
                                            ->where('status',1)
                                            ->where('contract_owner','1')
                                            ->first();
            $details          = Detail::select('id', 'name')->get();
            if($property){
                if($property->property_details){
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($property->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $property->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $property->$detail_data = 0;
                        }
                    }
                    unset($property->property_details);
                }
            }else{
                $property       = (object)[];
            }


            // code to get near by property
            if($latitude && $longitude){
                $near_property      = OwnerProperty::select('id',DB::raw('ifnull(latitude,0.0) AS latitude'), DB::raw('ifnull(longitude,0.0) AS longitude'),'property_to'
                                                            ,DB::raw('ifnull(rent,0.0) AS rent'),DB::raw('ifnull(selling_price,0.0) AS selling_price')
                                                            , DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                                            * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                                            + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"))
                                                    ->having('distance', '<', 10)
                                                    ->where('id','!=',$property_id)
                                                    ->where('status',1)
                                                    ->where('contract_owner','1')
                                                    ->limit(10)
                                                    ->get();
            }else{
                $near_property      = [];
            }

            $data       = [
                'main_property'  => $property,
                'near_properties'  => $near_property,
            ];
            return response()->json(['status' => 200, 'data' => $data ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function listAllPropertyLocations(Request $request)
    {
        $rules = [
            'latitude'  => 'required',
            'longitude'  => 'required',
        ];
        $messages = [
            'latitude.required'  => 'Latitude is required',
            'longitude.required'  => 'Longitude is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id        = $request->property_id;
        $latitude           = $request->latitude;
        $longitude          = $request->longitude;
        try {

            // code to get near by property
            if($latitude && $longitude){
                $list_property      = OwnerProperty::select('id',DB::raw('ifnull(latitude,0.0) AS latitude'), DB::raw('ifnull(longitude,0.0) AS longitude'),'property_to'
                                                            ,DB::raw('ifnull(rent,0.0) AS rent'),DB::raw('ifnull(selling_price,0.0) AS selling_price')
                                                            , DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                                            * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                                            + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"))
                                                    ->having('distance', '<', 10)
                                                    ->where('status',1)
                                                    ->where('contract_owner','1')
                                                    ->limit(10)
                                                    ->get();
            }else{
                $list_property      = [];
            }

            $data       = [
                'properties'  => $list_property,
            ];
            return response()->json(['status' => 200, 'data' => $data ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function showEachPropertyLocationDeatils(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id        = $request->property_id;
        try {
            $base_url       = url('/');
            $property       = OwnerProperty::select('id',DB::raw('ifnull(property_name,"") AS property_name'),DB::raw('ifnull(property_type,"") AS property_type')
                                                    ,'rating','property_to',DB::raw('ifnull(rent,0.0) AS rent'),DB::raw('ifnull(selling_price,0.0) AS selling_price'))
                                            ->with(array('property_priority_image'=>function($query) use($base_url){
                                                                    $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                                },'property_details:property_id,detail_id,value,name',
                                                            )
                                            )
                                            ->where('id',$property_id)
                                            ->where('status','1')
                                            ->where('contract_owner','1')
                                            ->first();

            $details          = Detail::select('id', 'name')->get();
            if($property){
                if($property->property_details){
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($property->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $property->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $property->$detail_data = 0;
                        }
                    }
                    unset($property->property_details);
                }
            }else{
                $property       = (object)[];
            }
            return response()->json(['status' => 200, 'property_details' => $property]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function scheduledTourList(Request $request)
    {
        $page       = $request->page;
        try {
            $user_id        = Auth::user()->id;
            $base_url       = url('/');

            $booked_tours = BookUserTour::select('id','user_id','property_id','booked_date','time_range','status')
                                        ->with(array('property_details' => function($query) {
                                                        $query->select('id','property_reg_no',DB::raw('ifnull(property_name,"") AS property_name'));
                                                    },'property_priority_image' => function($query) use ($base_url) {
                                                        $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                                    }
                                                )
                                            )
                                        ->where('user_id',$user_id)
                                        ->orderBy('id','DESC')
                                        ->where('booked_date','>=',date('Y-m-d'));
            $booked_tours = $this->customPagination($booked_tours,$page);
            $booked_tours_data = [
                'total_page_count'      => $booked_tours['total_page_count'],
                'current_page'          => $booked_tours['current_page'],
                'booked_tours'             => $booked_tours['data'],
            ];

            if($booked_tours['data']){
                foreach($booked_tours['data'] as $each_tour){
                    if($each_tour->property_priority_image == ""){
                        $each_tour->property_priority_image = (object)[];
                    }
                    if($each_tour->property_details == ""){
                        $each_tour->property_details   = (object)[];
                    }
                }

            }
            return response()->json(['status' => 200,  'data' => $booked_tours_data]);

            return response()->json(['status' => 200, 'property_details' => $property]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


}
