<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;

use App\Http\Controllers\api\user\UserRegisterController;

use Validator;
use Hash;
use Session;
use Auth;
use Storage;
use File;
use DB;

use App\Models\User;
use App\Models\UserOtp;

class UserLoginController extends Controller
{

    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function phoneVerification(Request $request){
        $rules = [
            'phone' => 'required',
        ];
        $messages = [
            'phone.required' => 'Phone number is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $user = User::where('phone',$request->phone)->first();
        if(!$user){
            return response()->json(['status'=>401,'response'=>"User not found"]);
        }
        if($user->profile_status == 0){
            $userOtp = UserOtp::where('user_id',$user->id)->first();
            if(!$userOtp){
                UserOtp::insert(['user_id'=>$user->id,
                                'otp'=>1001,
                                'created_at'=>now(),
                                'updated_at'=>now()
                            ]);
            }else{
                UserOtp::where('user_id',$user->id)->update(array('otp'=>1001));
            }
        }else{
            $user->otp = 1001;
            $user->save();
        }
        return response()->json(['status'=>200,'phone'=>$request->phone,'otp'=>1001,'response'=>"OTP has been sent to your phone"]);
    }

    public function resendOTP(Request $request){
        $rules = [
            'phone' => 'required',
        ];
        $messages = [
            'phone.required' => 'Phone number is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $user = User::where('phone',$request->phone)->first();
        if(!$user){
            return response()->json(['status'=>401,'response'=>"User not found"]);
        }
        if($user->profile_status == 0){
            $userOtp = UserOtp::where('user_id',$user->id)->first();
            if(!$userOtp){
                UserOtp::insert(['user_id'=>$user->id,
                                'otp'=>1001,
                                'created_at'=>now(),
                                'updated_at'=>now()
                            ]);
            }else{
                UserOtp::where('user_id',$user->id)->update(array('otp'=>1001));
            }
        }else{
            $user->otp = 1001;
            $user->save();
        }
        return response()->json(['status'=>200,'phone'=>$request->phone,'otp'=>1001,'response'=>"OTP has been re-sent to your phone"]);
    }

    public function userLogin(Request $request){
        $rules = [
            'phone' => 'required',
            'otp' => 'required',
        ];
        $messages = [
            'phone.required' => 'Phone number is required',
            'otp.required' => 'OTP is required'
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $user = User::select('id',DB::raw('ifnull(name,"") as name'),DB::raw('ifnull(email,"") as email'),
                             DB::raw('ifnull(phone,"") as phone'),DB::raw('ifnull(latitude,"") as latitude'),
                             DB::raw('ifnull(longitude,"") as longitude'),DB::raw('ifnull(location,"") as location'),
                             'profile_pic',DB::raw('ifnull(api_token,"") as api_token'),'status')
                    ->where('phone',$request->phone)->first();
        if($user){
            if($user->status == 0){
                return response()->json(['status'=>400,'response'=>"Profile is inactive/blocked"]);
            }
            if($user->profile_status == 0){
                $userData = User::join('user_otps','users.id','=','user_otps.user_id')
                ->where('users.id',$user->id)
                ->select('users.id','users.name','user_otps.otp')
                ->first();
                if(!$userData){
                    return response()->json(['status'=>400,'response'=>"Soemthing went wrong"]);
                }
                if($userData->otp == $request->otp){
                    Session::put('id',$user->id);
                    Session::put('name',$user->name);
                    Session::put('phone',$user->phone);
                    $token = $user->createToken('usertoken')->accessToken;
                    $user->api_token = $token;
                    $user->profile_status = 1; //1-completed
                    $user->save();

                    // get profile
                    if($user->profile_pic){
                        $profile_url  = $this->url->to('/').$user->profile_pic;
                    }else{
                        $profile_url  = "";
                    }

                    $user_array=[
                        'api_token'   => $user->api_token,
                        'email'       => $user->email,
                        'name'        => $user->name,
                        'profile_pic' => $profile_url,
                        'location'    => $user->location,
                    ];
                    if($user->latitude){
                        $user_array['latitude'] = $user->latitude;
                    }
                    if($user->longitude){
                        $user_array['longitude'] = $user->longitude;
                    }
                    return response()->json(['status'=>200,'response'=>"Logged in successfully",'data'=>$user_array]);
                }
                return response()->json(['status'=>400,'response'=>"Invalid otp"]);
            }else{
                if($user->otp == $request->otp){
                    Session::put('id',$user->id);
                    Session::put('name',$user->name);
                    Session::put('phone',$user->phone);
                    $token = $user->createToken('usertoken')->accessToken;
                    $user->api_token = $token;
                    // $user->otp = NULL;
                    // $user->latitude  = $request->lat ?  $request->lat : "" ;
                    // $user->longitude = $request->lng ?  $request->lng : "" ;
                    $user->save();

                    // get profile
                    if($user->profile_pic){
                        $profile_url  = $this->url->to('/').$user->profile_pic;
                    }else{
                        $profile_url  = "";
                    }

                    $user_array=[
                        'api_token'     => $user->api_token,
                        'email'         => $user->email,
                        'name'          => $user->name,
                        'profile_pic'   => $profile_url,
                    ];
                    if($user->latitude){
                        $user_array['latitude'] = $user->latitude;
                    }
                    if($user->longitude){
                        $user_array['longitude'] = $user->longitude;
                    }
                    return response()->json(['status'=>200,'response'=>"Logged in successfully",'data'=>$user_array]);
                }
                return response()->json(['status'=>400,'response'=>"Invalid otp"]);
            }
        }else{
            return response()->json(['status'=>401,'response'=>"Invalid phone number"]);
        }
    }

    public function userLogout(Request $request){
        $owner = Auth::user()->token();
        $owner->revoke();
        Session::flush();
        return response()->json(['status'=>200 , 'response'=>"Logged out successfully"]);
    }

    public function loginWithOther(Request $request)
    {
        $rules = [
            'name'          => 'nullable',
            'email'         => 'nullable|email',
            'phone'         => 'nullable',
            'google_id'     => 'nullable',
            'facebook_id'   => 'nullable',
            'apple_id'      => 'nullable',
            'login_type'    => 'required',
            'profile_pic'   => 'nullable'
        ];
        $messages = [
            // 'email.required' => 'Email is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $user = User::where('email',$request->email)->first();
        if(!$user){
            if($request->login_type == 3 && $request->apple_id){
                $user = User::where('apple_id',$request->apple_id)->first();
            }elseif($request->login_type == 2 && $request->facebook_id){
                $user = User::where('facebook_id',$request->facebook_id)->first();
            }elseif($request->login_type == 1 && $request->google_id){
                $user = User::where('google_id',$request->google_id)->first();
            }
        }
        $status     = 0;
        if($user){
            if($request->login_type == 1){
                if($user->google_id){
                    if($user->google_id == $request->google_id){
                        $status     = 1;
                    }
                }else{
                    $user->google_id = $request->google_id;
                    $user->save();
                    $status     = 1;
                }
            }elseif($request->login_type == 2){
                if($user->facebook_id){
                    if($user->facebook_id == $request->facebook_id){
                        $status     = 1;
                    }
                }else{
                    $user->facebook_id  = $request->facebook_id;
                    $user->save();
                    $status     = 1;
                }
            }else{
                if($user->apple_id){
                    if($user->apple_id == $request->apple_id){
                        $status     = 1;
                    }
                }else{
                    $user->apple_id = $request->apple_id;
                    $user->save();
                    $status     = 1;
                }
            }
        }else{
            $user                   = new User();
            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->phone            = $request->phone;
            if ($request->profile_pic) {
                // $url = $request->profile_pic;

                // $info = pathinfo($url);
                // $contents = file_get_contents($url);
                // $name = substr($url, strrpos($url, '/') + 1);
                
                // $user->profile_pic        = '/uploads/users/profile/' . $info['basename'];
                
                
                // // Storage::disk('uploads/users/profile/')->put($name, $contents);
                // $path = Storage::putFile('image', new File($user->profile_pic), 'public');
                // return $contents; 
    
            }
            $user->referral_code    = (new UserRegisterController())->generateUserReferalCode();
            $user->status   = 1;
            if($request->login_type == 1){
                if($request->google_id){
                    $user->google_id    = $request->google_id;
                }else{
                    return response()->json(['status'=>400,'response'=>"Google id is required"]);
                }
            }elseif($request->login_type == 2){
                if($request->facebook_id){
                    $user->facebook_id  = $request->facebook_id;
                }else{
                    return response()->json(['status'=>400,'response'=>"Facebook id is required"]);
                }
            }else{
                if($request->apple_id){
                    $user->apple_id     = $request->apple_id;
                }else{
                    return response()->json(['status'=>400,'response'=>"Apple id is required"]);
                }
            }
            $user->save();
            $status     = 1;
        }
        if($status){
            Session::put('id',$user->id);
            Session::put('name',$user->name);
            Session::put('phone',$user->phone);
            $token = $user->createToken('usertoken')->accessToken;
            $user->api_token = $token;
            // $user->otp = NULL;
            // $user->latitude  = $request->lat ?  $request->lat : "" ;
            // $user->longitude = $request->lng ?  $request->lng : "" ;
            $user->save();

            // get profile
            if($user->profile_pic){
                $profile_url  = $this->url->to('/').$user->profile_pic;
            }else{
                $profile_url  = "";
            }
            
            $user_array=[
                'api_token'     => $user->api_token,
                'email'         => $user->email,
                'name'          => $user->name,
                'profile_pic'   => $profile_url,
            ];
            if($user->latitude){
                $user_array['latitude'] = $user->latitude?$user->latitude:"0.00";
            }
            if($user->longitude){
                $user_array['longitude'] = $user->longitude?$user->longitude:"0.00";
            }
            return response()->json(['status'=>200,'response'=>"Logged in successfully",'data'=>$user_array]);
        }else{
            return response()->json(['status'=>401,'response'=>"Login Failed"]);
        }
    }
}
