<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\User;
use App\Models\UserOtp;
use App\Models\UserReferral;
use DB;

use Illuminate\Http\Request;

class UserRegisterController extends Controller
{
    public function signUp(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'phone' => 'required|unique:users',
            'lat' => 'required',
            'lan' => 'required',
            'location' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Please enter a valid email',
            'phone.required' => 'Phone Number is required',
            'lat.required' => 'Latitude is required',
            'lan.required' => 'Longitude is required',
            'location.required' => 'Location is required'
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            DB::beginTransaction();
            if($request->referral_code){
                $exist_referral     = User::where('referral_code',$request->referral_code)->first();
                if(!$exist_referral){
                    return response()->json(['status'=>400,'response'=>'Invalid referral code']);
                }
            }   

            $new_user = new User();
            $new_user->name             = $request->name;
            $new_user->email            = $request->email;
            $new_user->phone            = $request->phone;
            $new_user->latitude         = $request->lat;
            $new_user->longitude        = $request->lan;
            $new_user->location         = $request->location;
            $new_user->referral_code    = $this->generateUserReferalCode();
            $new_user->save();
            if($new_user){
                UserOtp::insert(['user_id'=>$new_user->id,
                                    'otp'=>1001,
                                    'created_at'=>now(),
                                    'updated_at'=>now()
                                ]);
                $new_user->otp = 1001;
                $new_user->save();
            }

            if($request->referral_code){
                $referred_user_id                   = $exist_referral->id;
                $user_referral                      = new UserReferral();
                $user_referral->user_id             = $new_user->id;
                $user_referral->referred_user_id    = $referred_user_id;
                $user_referral->status              = 0;
                $user_referral->save();
            }   

            DB::commit();
            return response()->json(['status'=>200,'phone'=>$new_user->phone,'otp'=>1001,'response'=>"OTP has been sent to your phone"]);
    
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function generateUserReferalCode()
    {
        $num = mt_rand(0, 999);
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        $referral_code = 'SAJID' . $num . $randomString ;
        try {
            $user_data = User::where('referral_code', $referral_code)->first();
            if (!empty($user_data)) {
                $this->generateUserReferalCode();
            } else {
                return $referral_code;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
}
