<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use App\Models\AdminAccount;
use App\Models\AgentFeedbacks;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use App\Models\Detail;
use App\Models\Event;
use App\Models\Notifications;
use App\Models\OwnerPropertyTermsOfStay;
use App\Models\RentalPackages;
use App\Models\Help_and_supports;
use App\Models\Rewards;
use App\Models\UserRewards;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDetail;
use App\Models\PropertyType;
use App\Models\Frequency;
use App\Models\Amenity;
use App\Models\OfferPackage;
use App\Models\UserProperty;
use App\Models\UserPropertyRating;
use App\Models\AgentTrackingUserTour;
use App\Models\BookUserTour;
use App\Models\AmenitiesCategory;
use App\Models\UserPropertyServiceRequestDocuments;

use Validator;
use PDF;

class PropertyController extends Controller
{

    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }


    public function showProperty(Request $request)
    {
        $page         = $request->page;
        $property_name= $request->property_name;
        $property_to  = $request->property_to;
        $category     = $request->category;
        $type_id      = $request->type_id;
        $min_price    = $request->min_price;
        $max_price    = $request->max_price;
        $bed_id       = $request->bed_id;
        $bed          = $request->bed;
        $max_bed      = $request->max_bed;
        try {
            $details    = Detail::select('id', 'name')->get();
            $base_url   = url('/');
            $properties = OwnerProperty::select('owner_properties.id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),'property_type','owner_properties.owner_id','rating','property_to',
                                                'category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'))
                                        ->where('owner_properties.is_builder','!=', 1);
            if($property_name){
                $properties = $properties->where('property_name', 'like','%'.$property_name.'%');
            }
            if($property_to != ""){
                if($property_to == 0 ||$property_to == 1){
                    $properties = $properties->where('property_to', $property_to);
                }
            }
            if($category != null){
                if($category == 0 || $category == 1 ){
                    $properties = $properties->where('category', $category);
                }
            }
            if(is_array($type_id)){
                $properties = $properties->whereIn('type_id', $type_id);
            }
            if($bed_id){
                if($max_bed){
                    $max_val    = OwnerPropertyDetail::where('detail_id',$bed_id)->max('value');
                    $new_bed    = [];
                    for($i = $max_bed+1; $i <= $max_val;$i++){
                        $new_bed[]    = (int)$i;
                    }
                    if($bed){
                        $bed    = array_merge($bed,$new_bed);
                    }else{
                        $bed    = $new_bed;
                    }
                }
                if($bed){
                    $properties = $properties->leftjoin('owner_property_details','owner_property_details.property_id','owner_properties.id');
                    $properties = $properties->where('detail_id',$bed_id);
                    $properties = $properties->whereIn('owner_property_details.value', $bed);
                }
            }
            if($min_price && $max_price){
                if($property_to == 0){
                    $properties = $properties->whereBetween('rent', [$min_price, $max_price]);
                }elseif($property_to == 1){
                    $properties = $properties->whereBetween('selling_price', [$min_price, $max_price]);
                }else{
                    $properties = $properties->whereBetween('selling_price', [$min_price, $max_price])->orWhereBetween('rent', [$min_price, $max_price]);
                }
            }
            $properties = $properties->where('status', '1')
                                    ->where('contract_owner', '1')
                                    ->where('occupied', 0)
                                    // ->orderBy('distance', 'asc')
                                    ->with(array('documents'=>function($query) use($base_url){
                                                $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                            },'property_details:property_id,detail_id,value,name',
                                            'userfavourites:property_id'
                                        )
                                    )
                                    ->orderBy('is_featured', 'DESC');
            $properties = $this->customPagination($properties,$page);
            // $properties['data'] = $this->similarPropertyList($properties['data'], $details);


            if($properties['data']){
                foreach($properties['data'] as  $each_detail){
                    if ($each_detail->property_details) {
                        $new_property = [];
                        foreach ($details as $key => $each_item) {
                            $count = 0;
                            foreach ($each_detail->property_details as $key => $detail_array) {
                                if ($each_item->name == $detail_array->name) {
                                    $count = 1;
                                    $type = $detail_array->name;
                                    $each_detail->$type = $detail_array->value;
                                    break;
                                }
                            }
                            if ($count != 1) {
                                $detail_data = $each_item->name;
                                $each_detail->$detail_data = 0;
                            }
                        }
                    }
                    if ($each_detail->userfavourites) {
                        if ($each_detail->userfavourites->property_id == $each_detail->id) {
                            $each_detail->is_favourite = 1;
                        } else {
                            $each_detail->is_favourite = 0;
                        }
                    } else {
                        $each_detail->is_favourite = 0;
                    }
                    unset($each_detail->property_details);
                    unset($each_detail->userfavourites);
                }
            }


            $properties_data = [
                'total_page_count'      => $properties['total_page_count'],
                'current_page'          => $properties['current_page'],
                'properties'            => $properties['data'],
            ];


            return response()->json(['status' => 200, 'property_data' => $properties_data, 'events' => array()]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function allNull2Empty($array)
    {
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        $response_array = [];
        foreach ($newarray as $key => $each_array) {
            foreach ($each_array as $key => $value) {
                if (is_array($value)) {
                    $this->allNull2Empty($value);
                } else {
                    if (is_null($value)) {
                        $each_array->$key = '';
                    } else {
                        $each_array->$key = $value;
                        if ($key == 'agent_id') {
                            $str_val = strval($value);
                            $each_array->$key = $str_val;
                        }
                    }
                }
            }
            $response_array[] = $each_array;
        }
        return $response_array;
    }
    public function customPagination($data_array, $page)
    {
        $total_count        = $data_array->get();

        $total_event_count  = count($total_count);

        $items_per_page     = 10;
        $total_page_count   = ceil($total_event_count / $items_per_page);
        if ($page) {

            $pages_array        = range(1, $total_page_count);
            if (in_array($page, $pages_array)) {
                $current_page       = strval($page);
                $offset_page        = $page - 1;
                $offset_item_count  = $offset_page * $items_per_page;

                $data_array         = $data_array->offset($offset_item_count)->take($items_per_page)->get();
            } else {
                $current_page       = strval($page);
                $data_array         = [];
            }
        } else {
            if ($page == 0 && $page != null) {
                $current_page       = strval(0);
                $data_array         = [];
            } else {
                $current_page       = strval(1);
                $data_array         = $data_array->take($items_per_page)->get();
            }
        }

        if (count($data_array) > 0) {
            $data_array  = $this->allNull2Empty($data_array);
        }

        $return_array = [
            'current_page'      => $current_page,
            'total_page_count'  => $total_page_count,
            'data'              => $data_array
        ];

        return $return_array;
    }


    public function helpAndSupports(Request $request)
    {
        $rules = [
            'problem_description'  => 'required',
        ];
        $messages = [
            'problem_description.required'      => 'Feedback is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $data = array(
                'problem_description'=>$request->problem_description,
                'user_id'=>Auth::user()->id
            );
            $insert = Help_and_supports::create($data);
            return response()->json(['status' => 200, 'response' => 'Support Request successful']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function user_payment_history(Request $request){
        $rules = [
            'user_property_id' => 'required',
        ];
        $messages = [
            'user_property_id.required' => 'User Property id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $user_property_id       = $request->user_property_id;
        $page                   = $request->page;
        try{
            $user_id            = Auth::user()->id;
            $payment_history    = AdminAccount::select('id','date','amount','amount_type','payment_type','reference_id','property_id')
                                                ->where('user_id','=',Auth::user()->id)
                                                ->where('user_property_id',$user_property_id)
                                                ->orderBy('date','DESC');
            $payment_history    = $this->customPagination($payment_history,$page);

            if($payment_history['data']){
                foreach($payment_history['data'] as $each_payment){
                    if($each_payment->payment_type == 1){
                        $reference_id   = $each_payment->reference_id;
                        $service_payment= UserPropertyServiceRequestDocuments::select('id','request_id')
                                                                                ->with(array('requested_service'=>function($query){
                                                                                            $query->select('id','service_id','time',DB::raw('ifnull(description,"") AS description'));
                                                                                        }
                                                                                    )
                                                                                )
                                                                                ->where('id',$reference_id)
                                                                                ->where('document_type',1)
                                                                                ->first();
                        if($service_payment){
                            if($service_payment->requested_service){
                                if($service_payment->requested_service->service_related){
                                    if($service_payment->requested_service->service_related->service){
                                        $each_payment->service             = $service_payment->requested_service->service_related->service;
                                        $each_payment->service_description = $service_payment->requested_service->description;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $payment_history_data = [
                'total_page_count'      => $payment_history['total_page_count'],
                'current_page'          => $payment_history['current_page'],
                'payment_history'            => $payment_history['data'],
            ];

            return response()->json(['status' => 200,  'data' => $payment_history_data]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function user_notifications(request $request){
        $page       = $request->page;
        try{
            $notifications = Notifications::select('id','notification_heading','notification_text',DB::raw('ifnull(status,"unread") as status'),DB::raw('ifnull(created_at,"1970-01-01 00:00:00") as date'))
            ->orderBy('created_at','DESC')
            ->where('user_id','=',Auth::user()->id);

                $notifications = $this->customPagination($notifications,$page);
                $notifications_data = [
                    'total_page_count'      => $notifications['total_page_count'],
                    'current_page'          => $notifications['current_page'],
                    'notifications'            => $notifications['data'],
                ];
                return response()->json(['status' => 200,  'data' => $notifications_data, 'message' => 'Success']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
    public function terms_of_stay(request $request){
        try{
            $data = OwnerPropertyTermsOfStay::select('id','property_id','terms_of_stay')->where('property_id','=',$request->segment(4))->first();
            return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);


            // if(!empty($data)){
            //     return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);
            // }else{
            //     return response()->json(['status' => 404, 'message' => 'No Data Found']);
            // }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
    public function user_rewards(request $request){
        try{
            $data = UserRewards::select('rewards.id','coupon_code','coupon_type',DB::raw('ifnull(percentage,"") AS percentage'),DB::raw('ifnull(amount,"") AS amount'),'rewards.expiry_date')
                                        ->join('rewards','rewards.id','=','user_rewards.reward_id')
                                        ->where('user_rewards.expiry_date','>=',date('Y-m-d'))
                                        ->where('user_id','=',Auth::user()->id)
                                        ->get();
            return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);

            // if($data->isNotEmpty()){
            //     return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);
            // }else{
            //     return response()->json(['status' => 400, 'message' => 'No Data Found']);
            // }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function read_notifications(Request $request)
    {
        $rules = [
            'notification_id'  => 'required',
        ];
        $messages = [
            'notification_id.required'      => 'Notification ID Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            Notifications::where('id',$request->notification_id)->update(['status' => 'read']);
            return response()->json(['status'=>200,'message' => 'updated']);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function rental_packages(request $request){
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property ID Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            $data = RentalPackages::select('rental_packages.id','property_id','type','rent','start_date','end_date','descriptions')
                    ->leftjoin('frequencies','frequencies.id','=','rental_packages.frequency_id')
                    ->where('property_id','=',$request->property_id)->get();
            if($data->isNotEmpty()){
                return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);
            }else{
                return response()->json(['status' => 204, 'message' => 'No Data Found']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function showPropertyFullPageFilter(Request $request)
    {
        $page         = $request->page;
        $latitude     = $request->lat;
        $longitude    = $request->lan;
        $property_to  = $request->property_to;
        $furnished    = $request->furnished;
        $category     = $request->category;
        $min_price    = $request->min_price;
        $max_price    = $request->max_price;
        $bed_id       = $request->bed_id;
        $bed          = $request->bed;
        $max_bed      = $request->max_bed;
        $bathroom_id  = $request->bathroom_id;
        $bathroom     = $request->bathroom;
        $max_bathroom = $request->max_bathroom;
        $area_id      = $request->area_id;
        $min_area     = $request->min_area;
        $max_area     = $request->max_area;
        $amenities    = $request->amenities;
        $type_id      = $request->type_id;

        try {
            $details    = Detail::select('id', 'name')->get();
            $base_url   = url('/');
            if($latitude && $longitude){
                $properties = OwnerProperty::select('owner_properties.id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),DB::raw('ifnull(property_type,"") as property_type'),'owner_properties.owner_id',
                                                    'rating','property_to','category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'),
                                                    DB::raw('ifnull(type_id,"") as type_id'),DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                                                    * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                                                    + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"));
                $properties = $properties->having('distance','<',10)->orderBy('distance', 'ASC');
            }else{
                $properties = OwnerProperty::select('owner_properties.id','property_reg_no',DB::raw('ifnull(property_name,"") as property_name'),DB::raw('ifnull(property_type,"") as property_type'),'owner_properties.owner_id',
                                                    'rating','property_to','category',DB::raw('ifnull(selling_price,"") as selling_price'),DB::raw('ifnull(mrp,"") as mrp'),'is_featured',DB::raw('ifnull(rent,"") as rent'),
                                                    DB::raw('ifnull(type_id,"") as type_id'));
            }
            $properties = $properties->where('owner_properties.is_builder','!=',1);

            if($property_to != ""){
                if($property_to == 0 ||$property_to == 1){
                    $properties = $properties->where('property_to', $property_to);
                }
            }
            if($category != ""){
                if($category == 0 ||$category == 1){
                    $properties = $properties->where('category', $category);
                    if($category == 1){
                        $bed_id = "";
                        // $bathroom_id = "";
                    }
                }
            }
            if(is_array($furnished) && empty($furnished)){
                $properties = $properties->whereIn('furnished', $furnished);
            }
            if($bed_id){
                if($max_bed){
                    $max_val    = OwnerPropertyDetail::where('detail_id',$bed_id)->max('value');
                    $new_bed    = [];
                    for($i = $max_bed; $i <= $max_val;$i++){
                        $new_bed[]    = (int)$i;
                    }
                    if($bed){
                        $bed    = array_merge($bed,$new_bed);
                    }else{
                        $bed    = $new_bed;
                    }
                }
            }
            if($bathroom_id){
                if($max_bathroom){
                    $max_bathroom_val    = OwnerPropertyDetail::where('detail_id',$bathroom_id)->max('value');
                    $new_bathroom    = [];
                    for($i = $max_bathroom; $i <= $max_bathroom_val;$i++){
                        $new_bathroom[]    = (int)$i;
                    }
                    if($bathroom){
                        $bathroom    = array_merge($bathroom,$new_bathroom);
                    }else{
                        $bathroom    = $new_bathroom;
                    }
                }
            }
            $properties = $properties->join('owner_property_details','owner_property_details.property_id','owner_properties.id');
            $property_ids   = "";
            if($bed  || $bathroom || $min_area || $max_area){
                $duplicated_property_ids = [];
                $bed_property_ids = [];
                $bathroom_property_ids = [];
                $area_property_ids = [];
                $duplicated_property_ids['array_1'] = [];
                $duplicated_property_ids['array_2'] = [];
                $duplicated_property_ids['array_3'] = [];
                //fetch bed property
                $count = 0;

                if($bed && $bed_id){
                    $bed_property_ids   = OwnerPropertyDetail::select('property_id')
                                                                ->where('detail_id',$bed_id)
                                                                ->whereIn('value', $bed)
                                                                ->distinct('property_id')
                                                                ->get();
                    $bed_property_ids           = json_decode($bed_property_ids);
                    $duplicated_property_ids['array_1']  =  $this->keyValuePairForProperty($bed_property_ids);
                    $count++;

                }
                //fetch bathroom property
                if($bathroom && $bathroom_id){
                    $bathroom_property_ids  = OwnerPropertyDetail::select('property_id')
                                                                        ->where('detail_id',$bathroom_id)
                                                                        ->whereIn('owner_property_details.value', $bathroom)
                                                                        ->distinct('property_id')
                                                                        ->get();
                    $bathroom_property_ids   = json_decode($bathroom_property_ids);
                    $duplicated_property_ids['array_2']  =  $this->keyValuePairForProperty($bathroom_property_ids);
                    $count++;

                }

                //fetch area property
                if($min_area && $max_area && $area_id){
                    $area_property_ids  = OwnerPropertyDetail::select('property_id')
                                                                        ->where('detail_id',$area_id)
                                                                        ->whereBetween('owner_property_details.value',[$min_area , $max_area])
                                                                        ->distinct('property_id')
                                                                        ->get();
                    $area_property_ids   = json_decode($area_property_ids);
                    $duplicated_property_ids['array_3']  =  $this->keyValuePairForProperty($area_property_ids);
                    $count++;
                }

                if($count > 1){
                    $property_ids = call_user_func_array('array_intersect', $duplicated_property_ids);
                }else{
                    if($duplicated_property_ids['array_1']){
                        $property_ids = $duplicated_property_ids['array_1'];
                    }elseif($duplicated_property_ids['array_2']){
                        $property_ids = $duplicated_property_ids['array_2'];
                    }elseif($duplicated_property_ids['array_3']){
                        $property_ids = $duplicated_property_ids['array_3'];
                    }
                }

            }
            if($property_ids){
                $properties = $properties->whereIn('owner_properties.id',$property_ids);
            }

            if(is_array($type_id) && empty($type_id)){
                $properties = $properties->whereIn('owner_properties.type_id',$type_id);
            }

            if(is_array($amenities) && empty($amenities)){
                $properties = $properties->join('owner_property_amenities','owner_property_amenities.property_id','owner_properties.id');
                $properties = $properties->whereIn('owner_property_amenities.amenity_id',$amenities);
            }

            if($min_price && $max_price){
                if($property_to == 0){
                    $properties = $properties->whereBetween('rent', [$min_price, $max_price]);
                }elseif($property_to == 1){
                    $properties = $properties->whereBetween('selling_price', [$min_price, $max_price]);
                }else{
                    $properties = $properties->whereBetween('selling_price', [$min_price, $max_price])->orWhereBetween('rent', [$min_price, $max_price]);
                }
            }
            $properties = $properties->where('status', '1')
                                    ->where('contract_owner', '1')
                                    ->where('occupied', 0)
                                    // ->orderBy('distance', 'asc')
                                    ->with(array('documents'=>function($query) use($base_url){
                                                $query->select('id','property_id','type',DB::raw('CONCAT("' . $base_url. '",document) AS document'));
                                            },'property_details:property_id,detail_id,value,name',
                                            'userfavourites:property_id'
                                        )
                                    )
                                    ->orderBy('is_featured', 'DESC');


            $properties = $properties->groupBy('owner_properties.id');

            $properties = $this->customPagination($properties,$page);
            // $properties['data'] = $this->similarPropertyList($properties['data'], $details);


            if($properties['data']){
                foreach($properties['data'] as  $each_detail){
                    if ($each_detail->property_details) {
                        $new_property = [];
                        foreach ($details as $key => $each_item) {
                            $count = 0;
                            foreach ($each_detail->property_details as $key => $detail_array) {
                                if ($each_item->name == $detail_array->name) {
                                    $count = 1;
                                    $type = $detail_array->name;
                                    $each_detail->$type = $detail_array->value;
                                    break;
                                }
                            }
                            if ($count != 1) {
                                $detail_data = $each_item->name;
                                $each_detail->$detail_data = 0;
                            }
                        }
                    }
                    if ($each_detail->userfavourites) {
                        if ($each_detail->userfavourites->property_id == $each_detail->id) {
                            $each_detail->is_favourite = 1;
                        } else {
                            $each_detail->is_favourite = 0;
                        }
                    } else {
                        $each_detail->is_favourite = 0;
                    }
                    unset($each_detail->property_details);
                    unset($each_detail->userfavourites);
                }
            }


            $properties_data = [
                'total_page_count'      => $properties['total_page_count'],
                'current_page'          => $properties['current_page'],
                'properties'            => $properties['data'],
            ];


            return response()->json(['status' => 200, 'property_data' => $properties_data, 'events' => array()]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function keyValuePairForProperty($duplicated_property_ids){
        $new_property_id    = [];
        foreach($duplicated_property_ids as $each_property){
            $new_property_id[]   = $each_property->property_id;
        }
        return $new_property_id;
    }


    public function categoryFilter(Request $request){
        try{
            $category   = $request->category;
            $categories = PropertyType::select('id','type','category');
            if($category == 1){
                $categories = $categories->where('category',$category)->get();
            }elseif($category == 0){
                if($category == null){
                    $categories = $categories->get();
                }else{
                    $categories = $categories->where('category',$category)->get();
                }

            }else{
                $categories = [];
            }
            return response()->json(['status'=>200,'categories' => $categories]);

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function frequencyForFilter(Request $request){
        try{
            $frequencies    = Frequency::select('id','type','status')->where('status',1)->get();

            return response()->json(['status'=>200,'frequencies' => $frequencies]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function propertyDetailsFilter(Request $request)
    {
        try{
            $base_url   = url('/');

            $details    = Detail::select('id','name',DB::raw('CONCAT("' . $base_url. '",icon) AS icon'))
                                ->orderBy('sort_order','ASC')
                                ->get();
            return response()->json(['status'=>200,'details' => $details]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function propertyTypesForFilter(Request $request)
    {
        try{
            $base_url   = url('/');

            // amenities list
            $amenities  = Amenity::select('id','name',DB::raw('CONCAT("' . $base_url. '",image) AS image'))
                                        ->orderBy('amenity_category_id','ASC')
                                        ->get();
            // frequencies list
            $frequencies    = Frequency::select('id','type','status')->where('status',1)->get();

            // details list
            $details    = Detail::select('id','name',DB::raw('CONCAT("' . $base_url. '",icon) AS icon'))
                                ->orderBy('sort_order','ASC')
                                ->get();

            // category list
            $categories = PropertyType::select('id','type','category')->get();

            $data       =[
                'category'         =>  $categories,
                'frequencies'      =>  $frequencies,
                'details'          =>  $details,
                'amenities'        =>  $amenities,
            ];


            return response()->json(['status'=>200,'data' => $data]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }



    public function agent_feedback(Request $request)
    {
        $rules = [
            'agent_id'=>'required',
            'rating'=>'required',
            'comments'=>'required'
        ];
        $messages = [
            'agent_id.required'      => 'Agent ID Required',
            'rating.required'      => 'Rating Required',
            'comments.required'      => 'Comments Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            $insert_array = [
                'agent_id'      => $request->agent_id,
                'comments'      => $request->comments,
                'rating'        => $request->rating,
                'user_id'       => Auth::user()->id,
                'created_at'    => now(),
                'updated_at'    => now(),
            ];

            $feedback = AgentFeedbacks::insert($insert_array);
            return response()->json(['status'=>200,'message'=>'Feedback Submitted']);

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function propertyOtherPackages(Request $request){
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property Id is Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            $packages       = OfferPackage::select('id','offer_package_name','property_id',DB::raw('ifnull(discount_amount,"") AS discount_amount'),
                                                    DB::raw('ifnull(actual_amount,"") AS actual_amount'),'start_date','end_date','description','frequency_id')
                                            ->with('package_features:package_feature,package_id')
                                            ->where('property_id',$request->property_id)
                                            ->where('start_date','<=',date('Y-m-d'))
                                            ->where('end_date','>=',date('Y-m-d'))
                                            ->where('status',1)
                                            ->get();
            return response()->json(['status'=>200,'packages'=> $packages]);

        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentFeedbackAndPropertyRating(Request $request)
    {
        $rules = [
            'property_id'       => 'required',
            'rating'            => 'nullable',
            'agent_feedback'    => 'nullable',
        ];
        $messages = [
            'property_id.required'      => 'Property Id is Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            $property_id    = $request->property_id;
            $rating         = $request->rating;
            $agent_feedback = $request->agent_feedback;
            $user_id        = Auth::user()->id;

            $property     = OwnerProperty::where('id',$property_id)->first();
            if(!$property){
                return response()->json(['status'=>400, 'response' =>'Invalid Property']);
            }

            // save property rating
            if($rating){
                $user_rating                = new UserPropertyRating();
                $user_rating->user_id       = $user_id;
                $user_rating->property_id   = $property_id;
                $user_rating->rating        = $rating;
                $user_rating->save();

                $average_rating             = UserPropertyRating::where('property_id',$property_id)->avg('rating');
                // $property_rating            = sprintf(round($average_rating, 2) == intval($average_rating) ? "%d" : "%.1f", $average_rating);
                $property_rating            = round((float)$average_rating, 1);


                $property_details           = OwnerProperty::where('id',$property_id)->first();
                $property_details->rating   = $property_rating;
                $property_details->save();
            }

            // save agent feedback
            if($agent_feedback){
                $user_tour  =  BookUserTour::where('user_id',$user_id)->where('property_id',$property_id)->orderBy('booked_date','DESC')->first();
                if($user_tour){
                    $tour_id        = $user_tour->id;
                    $agent_details  = AgentTrackingUserTour::where('tour_id',$tour_id)->first();
                    if($agent_details){
                        $agent_id       = $agent_details->agent_id;
                        $insert_array = [
                            'agent_id'      => $agent_id,
                            'comments'      => $agent_feedback,
                            'rating'        => $rating?$rating:0,
                            'user_id'       => $user_id,
                            'created_at'    => now(),
                            'updated_at'    => now(),
                        ];
                        $feedback = AgentFeedbacks::insert($insert_array);
                        // send push notification  to agent while updating feedback should confirm
                    }
                }
            }


            return response()->json(['status'=>200, 'response' =>'Successfully Updated']);

        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function downloadPropertyDetailsPDF(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $property_id      = $request->property_id;
        try {
            $property_detail = OwnerProperty::select('owner_properties.id','property_reg_no','property_name','property_type','owner_id','agent_id',
                                                    'rating','property_to','category','selling_price','mrp','is_featured','furnished','longitude','latitude',
                                                    'rent','city','description','occupied')
                                                    ->where('owner_properties.status', '1')
                                                    ->where('owner_properties.id', $property_id)
                                                    ->with(
                                                        'documents',
                                                        'agent_details:id,name,email,phone,image',
                                                        'property_details:property_id,detail_id,value,name',
                                                        'userfavourites:property_id',
                                                        'amenity_details:amenities_category_id,amenity_id,property_id',
                                                        'floor_plans:id,property_id,document,owner_id'
                                                    )
                                                    ->first();
            if ($property_detail) {
                $details          = Detail::select('id', 'name')->get();
                $amenity_category = AmenitiesCategory::select('id', 'name')->orderBy('sort_order', 'ASC')->get();
                if ($property_detail->agent_details) {
                    $agent_image        = $property_detail->agent_details;
                    $agent_image->image = $this->url->to('/') . $agent_image->image;
                }
                if ($property_detail->documents) {
                    foreach ($property_detail->documents as $file_array) {
                        foreach ($file_array as $key => $file) {
                            if ($key == 'document') {
                                $file_array->document = $this->url->to('/') . $file;
                            }
                        }
                    }
                }
                if ($property_detail->floor_plans) {
                    foreach ($property_detail->floor_plans as $file_array) {
                        foreach ($file_array as $key => $file) {
                            if ($key == 'document') {
                                $file_array->document = $this->url->to('/') . $file;
                            }
                        }
                    }
                }
                if ($property_detail->property_details) {
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($property_detail->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $property_detail->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $property_detail->$detail_data = 0;
                        }
                    }
                }
                if ($property_detail->userfavourites) {
                    if ($property_detail->userfavourites->property_id == $property_detail->id) {
                        $property_detail->is_favourite = 1;
                    } else {
                        $property_detail->is_favourite = 0;
                    }
                } else {
                    $property_detail->is_favourite = 0;
                }
                if ($property_detail->amenity_details) {
                    $new_property = [];
                    foreach ($amenity_category as $key => $each_category) {
                        $count = 0;
                        $property_amenities = [];
                        foreach ($property_detail->amenity_details as $key => $each_category_detail) {
                            if ($each_category->id == $each_category_detail->amenities_category_id) {
                                $type = $each_category->name;
                                $amenities = Amenity::select('id', 'name', 'image')
                                    ->where('amenity_category_id', $each_category->id)
                                    ->get();
                                $encoded_amenities = json_encode($amenities);
                                $decoded_amenities = json_decode($encoded_amenities);
                                foreach ($decoded_amenities as $key => $file_data) {
                                    $file_data->image = $this->url->to('/') . $file_data->image;
                                    $property_amenities[] = $file_data;
                                }
                                $count = 1;
                                break;
                            }
                        }

                        if ($count == 1) {
                            if (count($property_amenities) > 0) {
                                $amenities_array[] = [
                                    'id'         => $each_category->id,
                                    'name'       => $each_category->name,
                                    'amenity_details' => $property_amenities
                                ];
                            }
                        }
                    }
                    $property_detail->amenity_categories = $amenities_array;
                }
                unset($property_detail->property_details);
                unset($property_detail->amenity_details);
                unset($property_detail->userfavourites);
            }
            if($property_detail){
                $data   =[
                    'property' => $property_detail
                ];
                $filename = time() . rand().'pdf.pdf';

                $content    = view('admin.pdf.property_details',$data);

                $pdf        = PDF::loadHTML(' '.$content.' ');
                // // Save file to the directory
                $pdf->save('uploads/pdf/'.$filename);

                $pdffile = url('/').'/uploads/pdf/'.$filename;
                $data =[
                    'pdf' => $pdffile,
                ];
                return ['status' => 200, 'data' => $data];
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
}
