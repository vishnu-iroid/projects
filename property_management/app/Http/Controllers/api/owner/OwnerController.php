<?php

namespace App\Http\Controllers\api\owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Auth;
use Session;
use Validator;
use DB;
use PDF;

//service
use App\Http\Services\OwnerService;
use App\Http\Services\api\owner\PropertyService;
use File;
// models
use App\Models\Owner;
use App\Models\State;
use App\Models\City;
use App\Models\PropertyType;
use App\Models\OwnerProperty;
use App\Models\Services;
use App\Models\OwnerPropertyServiceRequests;
use App\Models\UserProperty;
use App\Models\UserPropertyServiceRequests;
use App\Models\OwnerPropertyServiceRequestDocuments;
use App\Models\CancelOwnerServiceRequest;
use App\Models\UserServiceRequestApprovel;
use App\Models\User;
use App\Models\ServiceAllocate;
use App\Models\AdminAccount;
use App\Models\OwnerPaymentReceived;
use App\Models\OwnerNotification;
use App\Models\AgentProperties;
use App\Models\Frequency;

class OwnerController extends Controller
{
    public function Logout(Request $request){
        $owner = Auth::user()->token();
        $owner->revoke();
        Session::flush();
        return response()->json(['status'=>200 , 'response'=>"Logged out successfully"]);
    }
    public function ownerProfile(){
        $owner = Owner::findOrFail(Auth::user()->id);
        if(!$owner){
            return response()->json(['status'=>404,'response'=>'Owner not found']);
        }else{
            $base_url       = url('/');
            if(!$owner->profile_image){
                $owner->profile_image   = "";
            }else{
                $owner->profile_image   = $base_url.$owner->profile_image;
            }
            if(!$owner->id_image){
                $owner->id_image   = "";
            }else{
                $owner->id_image        = $base_url.$owner->id_image;
            }
            $details = [
                'name'              => $owner->name,
                'email'             => $owner->email,
                'phone'             => $owner->phone,
                'profile_image'     => $owner->profile_image,
                'id_image'          => $owner->id_image,
                'account_number'    => $owner->account_number,
                'branch_name'       => $owner->branch_name,
                'ifsc'              => $owner->ifsc,
            ];
            return response()->json(['status'=>200,'response'=> $details]);
        }
    }
    public function addProperty(Request $request){
        $rules = [
            'property_to'       => 'required',
            'is_builder'       => 'required',
            'property_name'     => 'required',
            'type_id'           => 'required',
            'expected_amount'   => 'nullable',
            'category'          => 'required',
            'country'           => 'required',
            'state'             => 'required',
            'city'              => 'required',
            'zipcode'           => 'required',
            'address1'          => 'required',
            'address2'          => 'nullable',
            'images'            => 'array',
            'building_images.*' => 'max:3000|mimes:jpeg,jpg,png',
            'images.*'          => 'max:3000|mimes:jpeg,jpg,png',
            'floor_plans'       => 'nullable|array',
            'floor_plans.*'     => 'max:3000|mimes:jpeg,jpg,png',
            'amenities'         => 'array',
            'detailsdata'       => 'array|min:1',
        ];
        $messages = [
            'property_to.required'  => 'property to is required',
            'images.required'       => 'images are required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $data = (new PropertyService())->addProperty($request);
        return response()->json($data);
    }

    public function addApartment(Request $request){
        $rules = [
            'building_id'       => 'required',
            'images'            => 'required|array',
            'images.*'          => 'max:3000|mimes:jpeg,jpg,png',
            'floor_plans'       => 'nullable|array',
            'floor_plans.*'     => 'max:3000|mimes:jpeg,jpg,png',
            // 'videos'            => 'nullable|array',
            // 'videos.*'          => 'max:3000|mimes:mp4,mov,ogg,qt',
            'amenities'         => 'required|array',
            'detailsdata'       => 'array|min:1',
            'detailsdata.*'     => 'required',
        ];
        $messages = [
            'images.required'       => 'images are required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $data = (new PropertyService())->addBuildingApartment($request);
            return response()->json($data);
        }catch(\Exception $e){
            return response()->json(['status'=>400,'response'=>'Internal Server Error']);
        }
    }


    public function profileUpdate(Request $request){
        $rules = [
            'email' => 'required|unique:owners,email,' . Auth::user()->id,
            'phone' => 'required|unique:owners,phone,' . Auth::user()->id,
        ];
        if($request->id_image){
            $rules['id_image'] = 'max:4000|mimes:jpeg,jpg,png,doc,docx,pdf';
        }
        if($request->profile_image){
            $rules['profile_image'] = 'max:4000|mimes:jpeg,jpg,png';
        }
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $base_url   =   url('/');

        // $owner      = Owner::select('id','name','email','phone',DB::raw('CONCAT("'.$base_url.'",profile_image) AS profile_image'),'account_number',
        //                             'branch_name','ifsc',DB::raw('CONCAT("'.$base_url.'",id_image) AS id_image'))
        //                     ->where('id',Auth::user()->id)->first();
        $owner      = Owner::select('id','name','email','phone',DB::raw('CONCAT("'.$base_url.'",profile_image) AS profile_image'),'account_number',
                                    'branch_name','ifsc',DB::raw('CONCAT("'.$base_url.'",id_image) AS id_image'))
                            ->where('id',Auth::user()->id)->first();
        if(!$owner){
            return response()->json(['status'=>404,'response'=>'Owner not found']);
        }
        $owner->email = $request->email;
        $owner->phone = $request->phone;
        if($request->id_image){
            if(File::exists(public_path('/uploads/owners/id_images/').$owner->id_image)){
                File::delete(public_path('/uploads/owners/id_images/').$owner->id_image);
            }
            $id_image = $request->id_image;
            $imagename = time().rand().'.'.$id_image->getClientOriginalExtension();
            $id_image->move(public_path('/uploads/owners/id_images/'),$imagename);
            $owner->id_image = '/uploads/owners/id_images/'.$imagename;
        }
        if($request->profile_image){
            if(File::exists(public_path('/uploads/owners/profile_images/').$owner->profile_image)){
                File::delete(public_path('/uploads/owners/profile_images/').$owner->profile_image);
            }
            $profile_image          = $request->profile_image;
            $imagename              = time().rand().'.'.$profile_image->getClientOriginalExtension();
            $profile_image->move(public_path('/uploads/owners/profile_images/'),$imagename);
            $owner->profile_image   = '/uploads/owners/profile_images/'.$imagename;
        }
        try{
            $owner->save();
            if($owner){
                if($owner->profile_image == null){
                    $owner->profile_image   = "";
                }
                if($owner->id_image == null){
                    $owner->id_image   = "";
                }
                if($request->profile_image){
                    $owner->profile_image   = $base_url.$owner->profile_image;
                }
                if($request->id_image){
                    $owner->id_image   = $base_url.$owner->id_image;
                }
            }
            $details = [
                'name'              => $owner->name,
                'email'             => $owner->email,
                'phone'             => $owner->phone,
                'profile_image'     => $owner->profile_image,
                'id_image'          => $owner->id_image,
                'account_number'    => $owner->account_number,
                'branch_name'       => $owner->branch_name,
                'ifsc'              => $owner->ifsc,
            ];
            return response()->json(['status'=>200,'response'=> $details]);
        }catch(\Exception $e){
            return response()->json(['status'=>400,'response'=>$e->getMessage()]);
        }
    }
    public function typeOnCategory(Request $request){
        $type = PropertyType::where('category',$request->category)
                            ->select('id','type','category')
                            ->get();
        return response()->json(['status'=>200,'response'=>$type]);
    }

    public function propertyList(Request $request)
    {
        try{
            $base_url       = url('/');
            $page           = $request->page;
            $category       = $request->category;
            $property_name  = $request->property_name;
            $filter_id      = $request->list_id;
            $is_verified    = $request->is_verified;
            $type_id        = $request->type_id;
            $owner          = OwnerProperty::select('owner_properties.id','property_reg_no','property_name','property_type','owner_properties.owner_id','property_to',
                                            'category',DB::raw('ifnull(frequency,0) AS frequency'),DB::raw('ifnull(is_builder,2) AS is_builder'),DB::raw('ifnull(occupied,2) AS occupied'),'owner_amount',
                                            DB::raw('ifnull(rent,0.00) AS rent'),DB::raw('ifnull(selling_price,0.00) AS selling_price'))
                                    ->with(array('property_priority_image'=> function($query) use($base_url){
                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                    }
                                                )
                                            )
                                    ->orderBy('id','DESC')
                                    ->where('owner_properties.owner_id',Auth::user()->id);
            if($filter_id){
                $property_name  ="";
                if($filter_id == 1){
                    $owner = $owner->whereIn('is_builder',[0,1]);
                }elseif($filter_id == 2){
                    $occupied_properties    = OwnerProperty::select('id','is_builder','builder_id')
                                                            ->where('owner_id',Auth::user()->id)
                                                            // ->where('is_builder','!=',1)
                                                            ->where('occupied',1)
                                                            ->get();
                    $occupied_prop_ids  =[];
                    if($occupied_properties){
                        $property_ids   = [];
                        foreach($occupied_properties as $property){
                            if($property->builder_id != null){
                                $property_ids[] = $property->builder_id;
                            }else{
                                $property_ids[] = $property->id;
                            }
                        }
                        $occupied_prop_ids  = array_unique($property_ids);
                    }
                    $owner = $owner->whereIn('is_builder',[0,1])->whereIn('owner_properties.id',$occupied_prop_ids);
                }elseif($filter_id == 3){
                    $vacated_properties    = OwnerProperty::select('id','is_builder','builder_id')
                                                            ->where('owner_id',Auth::user()->id)
                                                            ->where(function($query){
                                                                        return $query->where('is_builder',0)->orWhereNull('is_builder');
                                                                    }
                                                                )
                                                            ->where('occupied','0')
                                                            ->get();
                    $vacated_prop_ids   =[];
                    if($vacated_properties){
                        $property_ids   = [];
                        foreach($vacated_properties as $property){
                            if($property->builder_id != null){
                                $property_ids[] = $property->builder_id;
                            }else{
                                $property_ids[] = $property->id;
                            }
                        }
                        $vacated_prop_ids  = array_unique($property_ids);
                    }
                    $owner = $owner->whereIn('is_builder',[0,1])->whereIn('owner_properties.id',$vacated_prop_ids);
                }elseif($filter_id == 4){
                    $maintenance_properties = OwnerPropertyServiceRequests::select('owner_properties.id','owner_properties.is_builder','owner_properties.builder_id')
                                                                            ->where('owner_properties.owner_id',Auth::user()->id)
                                                                            ->join('owner_properties','owner_properties.id','owner_property_service_requests.owner_property_id')
                                                                            ->whereIn('owner_property_service_requests.status',[0,1,2])
                                                                            ->get();
                    $maintenance_prop_ids   =[];
                    if($maintenance_properties){
                        $property_ids   = [];
                        foreach($maintenance_properties as $property){
                            if($property->builder_id != null){
                                $property_ids[] = $property->builder_id;
                            }else{
                                $property_ids[] = $property->id;
                            }
                        }
                        $maintenance_prop_ids  = array_unique($property_ids);
                    }
                    $owner = $owner->whereIn('is_builder',[0,1])->whereIn('owner_properties.id',$maintenance_prop_ids);

                }
            }else{
                $owner = $owner->whereIn('is_builder',[0,1]);
            }

            if($property_name){
                $owner = $owner->where('property_name', 'like','%'.$property_name.'%');
            }
            if($type_id){
                $owner = $owner->where('type_id',$type_id);
            }
            if($is_verified){
                $owner = $owner->where('status',1);
            }
            if($category != null){
                if($category == 0 || $category == 1 ){
                    $owner = $owner->where('category', $category);
                }
            }

            $owner = $this->customPagination($owner,$page);

            if($owner['data']){
                $new_owner  = $owner['data'];
                foreach($new_owner as $key=>$own){
                    // if($own->agent_id){
                    //     $own->agent_id = strval($own->agent_id);
                    // }
                    if(!$own->property_priority_image){
                        $own->property_priority_image   = (object)[];
                    }
                    if($own->id){
                        $own->net_worth                 = "0.00";// Total amount from the stay
                        $own->income                    = "0.00";// Total Received
                        $own->outstanding_due           = "0.00";// Missed Amount
                        $own->pending                   = "0.00";// Pending Total
                        if($own->is_builder == 1){
                            $bulding_appartments        = OwnerProperty::select('id','occupied','type_id')
                                                                        ->with(array('property_priority_image'=> function($query) use($base_url){
                                                                                            $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                                        },'type_details:id,type'
                                                                                    )
                                                                                )
                                                                        ->where('owner_id',Auth::user()->id)
                                                                        ->where('builder_id',$own->id);
                            if($filter_id == 2){
                                $bulding_appartments = $bulding_appartments->where('occupied',1)->get();
                            }elseif($filter_id == 3){
                                $bulding_appartments = $bulding_appartments->where('occupied',0)->get();
                            }else{
                                $bulding_appartments = $bulding_appartments->get();
                            }

                            $own->bulding_appartments   = $bulding_appartments;
                            $total_appartments_count    = OwnerProperty::where('owner_id',Auth::user()->id)->where('builder_id',$own->id)->count();
                            $vacated_appartments_count  = OwnerProperty::where('owner_id',Auth::user()->id)->where('builder_id',$own->id)->where('occupied',0)->count();
                            $occupied_appartments_count = OwnerProperty::where('owner_id',Auth::user()->id)->where('builder_id',$own->id)->where('occupied',1)->count();
                            $own->appartments_count     = $total_appartments_count;
                            $own->vacated               = $vacated_appartments_count;
                            $own->occupied              = $occupied_appartments_count;
                            $building_ids   =[];
                            if($bulding_appartments){
                                foreach($bulding_appartments as $each_apartment){
                                    $building_ids[] = $each_apartment->id;
                                }
                            }
                            if($building_ids){
                                if($own->property_to == 0){
                                    $user_property      = UserProperty::select('user_properties.*','owner_properties.frequency','owner_properties.owner_amount',DB::raw('ifnull(owner_properties.rent,0.00) AS prop_rent'))
                                                                        ->whereIn('property_id',$building_ids)
                                                                        ->join('owner_properties','owner_properties.id','user_properties.property_id')
                                                                        ->where('check_in','<=',date('Y-m-d'))
                                                                        ->where('check_out','>=',date('Y-m-d'))
                                                                        ->where('cancel_status',0)
                                                                        ->whereIn('user_properties.status',[0,1])
                                                                        ->get();
                                    if($user_property){
                                        $revenue_details         = $this->revenueBuildingDetails($user_property);
                                        $own->net_worth          = $revenue_details['net_worth'];
                                        $own->pending            = $revenue_details['pending'];
                                        $own->income             = $revenue_details['income'];
                                        $own->outstanding_due    = $revenue_details['outstanding_due'];
                                    }


                                }else{
                                    $user_property      = UserProperty::where('property_id',$building_ids)
                                                                        ->where('check_in','>=',date('Y-m-d'))
                                                                        ->get();

                                    $user_property_ids  =[];
                                    if($user_property){
                                        foreach($user_property as $each_user_apartment){
                                            $user_building_ids = $each_user_apartment->property_id;
                                        }
                                    }
                                }
                            }


                        }else{
                            if($own->property_to == 0){
                                $user_property      = UserProperty::select('user_properties.*','owner_properties.frequency')
                                                                    ->where('property_id',$own->id)
                                                                    ->join('owner_properties','owner_properties.id','user_properties.property_id')
                                                                    ->where('check_in','<=',date('Y-m-d'))
                                                                    ->where('check_out','>=',date('Y-m-d'))
                                                                    ->whereIn('user_properties.status',[0,1])
                                                                    ->where('cancel_status',0)
                                                                    ->first();

                                if($user_property){
                                    //function to calculate the amounts
                                    $revenue_details         = $this->revenuePropertyDetails($user_property,$own->rent);
                                    $own->net_worth          = $revenue_details['net_worth'];
                                    $own->pending            = $revenue_details['pending'];
                                    $own->income             = $revenue_details['income'];
                                    $own->outstanding_due    = $revenue_details['outstanding_due'];

                                }

                            }else{
                                $user_property      = UserProperty::where('property_id',$own->id)
                                                                    ->where('check_in','>=',date('Y-m-d'))
                                                                    ->first();
                            }
                        //     // $revenue_amount     = AdminAccount::where('property_id',$own->id)->where('owner_id',$own->owner_id)->where('account_type',2)->sum('amount');
                        //     // $expense_amount     = AdminAccount::where('property_id',$own->id)->where('owner_id',$own->owner_id)->where('account_type',1)->sum('amount');
                        }
                    }
                }
            }
            $owner_data =[
                'total_page_count' => $owner['total_page_count'],
                'current_page'     => $owner['current_page'],
                'owner'            => $owner['data'],
            ];

            return response()->json(['status'=>200,'response'=>$owner_data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function customPagination($data_array, $page)
    {
        $total_event_count  = $data_array->count();

        $items_per_page     = 10;
        $total_page_count   = ceil($total_event_count/$items_per_page);
        if($page){
            $pages_array        = range(1,$total_page_count);
            if(in_array($page,$pages_array)){
                $current_page       = strval($page);
                $offset_page        = $page - 1;
                $offset_item_count  = $offset_page * $items_per_page;

                $data_array         = $data_array->offset($offset_item_count)->take($items_per_page)->get();
            }else{
                $current_page       = strval($page);
                $data_array         = [];
            }
        }else{
            if($page == 0 && $page != null){
                $current_page       = strval(0);
                $data_array         = [];
            }else{
                $current_page       = strval(1);
                $data_array         = $data_array->take($items_per_page)->get();
            }
        }

        if(count($data_array) > 0){
            $data_array  = $this->allNull2Empty($data_array);
        }

        $return_array = [
            'current_page'      => $current_page,
            'total_page_count'  => $total_page_count,
            'data'              => $data_array
        ];

        return $return_array;
    }


    public function propertySubmittingForVerification(Request $request)
    {
        $rules = [
            'property_id'       => 'required',
        ];
        $messages = [
            'property_id.required'  => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id    = $request->property_id;
            $owner_id      = Auth::user()->id;
            $property       = OwnerProperty::where('id',$property_id)
                                            ->where('owner_id',$owner_id)
                                            ->first();
            if(OwnerProperty::where('id',$property_id)->where('owner_id',$owner_id)->update(array('owner_confirmation' => true))){
                return response()->json(['status'=>200,'response' => 'Property submitted for verification']);
            }else{
                return response()->json(['status'=>200,'response' => "Invalid Property"]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function setToNull($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }

    public function allNull2Empty($array){
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        $response_array=[];
        foreach ($newarray as $key => $value) {
            array_walk_recursive($value, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $response_array[] = $value;
        }
        return $response_array;
    }


    public function maintenanceList(Request $request){
        $page     = $request->page;
        $base_url = url('/');
        $services = Services::select('id','service',DB::raw('CONCAT("'.$base_url.'",image) AS image'));
        $services = $this->customPagination($services,$page);

        $services_data  =[
            'total_page_count' => $services['total_page_count'],
            'current_page'     => $services['current_page'],
            'services'   => $services['data'],
        ];

        return response()->json(['status'=>200,'services_data'=>$services_data]);
    }

    public function sendOwnerServiceRequest(Request $request)
    {
        $rules = [
            'owner_property_id' => 'required',
            'service_id'        => 'required',
            'request_date'      => 'required|date|after:yesterday',
            'time'              => 'required|date_format:H:i',
            'document'          => 'required',
            'document.*'        => 'mimes:jpeg,png,doc,docx,pdf',
        ];
        $messages = [
            'owner_property_id.required'    => 'Owmer property is required',
            'service_id.required'           => 'Service is required',
            'request_date.required'         => 'Date is required',
            'time.required'                 => 'Time is required',
            'document.required'             => 'Document is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            DB::beginTransaction();
            $owner_id           = Auth::user()->id;
            $service_id         = $request->service_id;
            $owner_property_id  = $request->owner_property_id;
            $has_user_property  = OwnerProperty::where('id',$owner_property_id)
                                                ->where('owner_id',$owner_id)
                                                ->first();
            if(!$has_user_property){
                return response()->json(['status'=>400,'response'=>"Owner property doesn't exist"]);
            }
            if($has_user_property){
                if($has_user_property->status == 0){
                    return response()->json(['status'=>400,'response'=>"Owner property not yet verfied"]);
                }elseif($has_user_property->status == 2){
                    return response()->json(['status'=>400,'response'=>"This property is rejected by admin"]);
                }
            }
            $has_service        = Services::where('id',$service_id)->first();
            if(!$has_service){
                return response()->json(['status'=>400,'response'=>"Service doesn't exist"]);
            }

            $has_requested      = OwnerPropertyServiceRequests::where('owner_id',$owner_id)
                                        ->where('owner_property_id',$owner_property_id)
                                        ->where('service_id',$service_id)
                                        ->where('date','>=',$request->request_date)
                                        ->first();

            if(empty($has_requested)) {
                $service_request                    = new OwnerPropertyServiceRequests();
                $service_request->owner_id          = $owner_id;
                $service_request->service_id        = $service_id;
                $service_request->owner_property_id = $owner_property_id;
                $service_request->date              = date('Y-m-d',strtotime($request->request_date));
                $service_request->time              = $request->time;
                $service_request->description       = $request->description;
                $service_request->status            = 0;

                $service_request->save();
                $request_id   = $service_request->id;
                if($request_id){
                    if($request->file('document')){
                        foreach($request->file('document') as $key=>$row){
                            $filename = time().rand().'.'.$row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/owners/service-documents'),$filename);

                            $user_property_service_doc[$key]['request_id']       = $request_id;
                            $user_property_service_doc[$key]['document']         = '/uploads/owners/service-documents/'.$filename;
                            $user_property_service_doc[$key]['created_at']       = now();
                            $user_property_service_doc[$key]['updated_at']       = now();
                        }
                    }
                    if($user_property_service_doc){
                        if(OwnerPropertyServiceRequestDocuments::insert($user_property_service_doc)){
                            DB::commit();
                            return response()->json(['status'=>200,'response'=>'Request has been sent successfully']);
                        }else{
                            return response()->json(['status'=>400,'response'=>'Failed to upload documents']);
                        }
                    }else{
                        return response()->json(['status'=>400,'response'=>'Failed to upload documents']);
                    }
                }else{
                    return response()->json(['status'=>500,'response' => 'Internal Server Error']);
                }
            }else{
                return response()->json(['status'=>200,'response'=>'Service already requested']);
            }

        }catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function listOwnerRequestedServices(Request $request)
    {
        $page                = $request->page;
        try{
            $owner_id           = Auth::user()->id;
            $owner_services     = OwnerPropertyServiceRequests::select('owner_property_service_requests.id','service_id','date','time'
                                                                        ,'owner_property_service_requests.status','owner_properties.property_name')
                                                            ->with('service_related:id,service')
                                                            ->join('owner_properties','owner_properties.id','owner_property_service_requests.owner_property_id')
                                                            ->where('owner_property_service_requests.owner_id',$owner_id)
                                                            ->orderBy('owner_property_service_requests.date','DESC');

            $owner_services      = $this->customPagination($owner_services,$page);
            $owner_service_data  =[
                'total_page_count' => $owner_services['total_page_count'],
                'current_page'     => $owner_services['current_page'],
                'owner_services'   => $owner_services['data'],
            ];
            return response()->json(['status'=>200,'owner_service_data'=>$owner_service_data]);

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function ownerRequestedServiceDetails(Request $request)
    {
        $rules = [
            'request_id' => 'required',

        ];
        $messages = [
            'request_id.required'       => 'Request id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        $owner_property_id    = $request->owner_property_id;
        $request_id          = $request->request_id;
        try{
            $base_url       = url('/');
            $owner_id            = Auth::user()->id;
            $owner_services       = OwnerPropertyServiceRequests::select('owner_property_service_requests.id','service_id','owner_property_service_requests.date','time'
                                                                        ,'owner_property_service_requests.status','owner_property_service_requests.description', 'owner_properties.property_name' )
                                                            ->with('service_related:id,service')
                                                            ->join('owner_properties','owner_properties.id','owner_property_service_requests.owner_property_id')
                                                            ->where('owner_property_service_requests.owner_id',$owner_id)
                                                            ->where('owner_property_service_requests.id',$request_id)
                                                            ->first();
            if(!empty($owner_services)){
                $owner_services       = $this->setToNull('owner_service',$owner_services);
                if($owner_services['owner_service']){
                    $request_id     = $owner_services['owner_service']->id;
                    $estimate       = OwnerPropertyServiceRequestDocuments::select('id',DB::raw('CONCAT("'.$base_url.'",document) AS document'))
                                                                        ->where('request_id',$request_id)
                                                                        ->where('document_type',2)
                                                                        ->first();
                    if($estimate){
                        $owner_services['owner_service']->estimate_file     = $estimate->document;
                    }else{
                        $owner_services['owner_service']->estimate_file     = "";
                    }
                }else{
                    $owner_services['owner_service']->estimate_file     = "";
                }
            }else{
                $owner_services       = (object)[];
            }

            return response()->json(['status'=>200,'data'=>$owner_services]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function ownerPropertyListForServiceRequest(Request $request)
    {
        $rules = [
            'service_id'            => 'required',

        ];
        $messages = [
            'service_id.required'   => 'Service id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $service_id          = $request->service_id;
            $owner_id            = Auth::user()->id;

            $property_service   = ServiceAllocate::select('property_id')
                                                ->where('service_id',$service_id)
                                                ->where('status',1)
                                                ->distinct('service_id')
                                                ->get();

            $property_service   = json_decode($property_service);
            if($property_service){
                $property_ids       = array_column($property_service,'property_id');
            }else{
                $property_ids       = [0];
            }

            $owner_peoperties    = OwnerProperty::select('id as owner_property_id', 'property_name')
                                                ->where('owner_id',$owner_id)
                                                ->whereIn('id',$property_ids)
                                                ->where('status',1)
                                                ->get();
            $owner_peoperties    = $this->allNull2Empty($owner_peoperties);

            return response()->json(['status'=>200,'data'=>$owner_peoperties]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function payOwnerPropertyService(Request $request)
    {
        $rules = [
            'request_id'        => 'required',
            'payment_status'    => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'request_id.required'           => 'Request id is required',
            'payment_status.required'       => 'Payment method is required',
            'document.required'             => 'Document is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            DB::beginTransaction();
            $owner_id    = Auth::user()->id;
            $request_id  = $request->request_id;
            $has_request = OwnerPropertyServiceRequests::where('id',$request_id)
                                                        ->where('owner_id',$owner_id)
                                                        ->where('status',1)
                                                        ->first();
            if(!empty($has_request)){
                $already_uploaded = OwnerPropertyServiceRequestDocuments::where('request_id',$has_request->id)
                                                        ->where('document_type',1)
                                                        ->first();
                if(empty($already_uploaded)){
                    if($request->file('document')){
                        $file = $request->file('document');
                        $filename = time().rand().'.'.$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/owners/service-documents'),$filename);

                        $user_service_doc                  = new OwnerPropertyServiceRequestDocuments();
                        $user_service_doc->request_id      = $has_request->id;
                        $user_service_doc->document        = '/uploads/owners/service-documents/'.$filename;
                        $user_service_doc->payment_status  = $request->payment_status;
                        $user_service_doc->document_type   = 1;
                        $user_service_doc->save();

                        DB::commit();
                        return response()->json(['status'=>200,'response'=>'File Upload Successful']);
                    }
                }else{
                    return response()->json(['status'=>200,'response'=>'File already updated']);
                }
            }else{
                $request_exist = OwnerPropertyServiceRequests::where('id',$request->request_id)->where('owner_id',$owner_id)->first();
                if($request_exist){
                    return response()->json(['status'=>200,'response'=>'This request is not ready for payment']);
                }else{
                    return response()->json(['status'=>400,'response'=>'Invalid requested service']);
                }
            }
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function cancelOwnerRequestedService(Request $request)
    {
        $rules = [
            'request_id' => 'required',
        ];
        $messages = [
            'request_id.required' => 'Request id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $request_id        = $request->request_id;
            $owner_id          = Auth::user()->id;
            $owner_request     = OwnerPropertyServiceRequests::where('id',$request_id)
                                                        ->where('owner_id',$owner_id)
                                                        ->where('status','!=',3)
                                                        ->first();
            if($owner_request){
                $has_cancel             = CancelOwnerServiceRequest::where('request_id',$request_id)
                                                                    ->where('owner_id',$owner_id)
                                                                    ->whereIn('admin_approve',[0,1])
                                                                    ->first();
                if(empty($has_cancel)){
                    $cancel_request                 = new CancelOwnerServiceRequest();
                    $cancel_request->owner_id       = $owner_id;
                    $cancel_request->request_id     = $request_id;
                    $cancel_request->admin_approve  = 0;
                    $cancel_request->save();
                    return response()->json(['status'=>200,'response'=>'Cancel request has been processed']);
                }else{
                    return response()->json(['status'=>200,'response'=>'Cancel request already been processed']);
                }
            }else{
                return response()->json(['status'=>400,'response'=>'Invalid Service Request']);
            }
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function getPricePercentage($amount, $percentage)
    {
        $percentInDecimal = $percentage / 100;

        $percent = $percentInDecimal * $amount;
        return $percent;
    }

    public function ownerPropertyDetails(Request $request)
    {
        $rules = [
            'owner_property_id' => 'required',
        ];
        $messages = [
            'owner_property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id    = $request->owner_property_id;
            $owner_id       = Auth::user()->id;
            $base_url       = url('/');

            $properties     = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,0) AS property_name'),'property_to',DB::raw('ifnull(frequency,0) as frequency'),
                                                    DB::raw('CONCAT("'.$base_url.'",contract_file) AS contract_file'), 'is_featured',DB::raw('ifnull(owner_amount,0) as owner_amount'),
                                                    DB::raw('ifnull(selling_price,0.00) as selling_price'),DB::raw('ifnull(rent,0.00) as rent'),'commission_in','commission','status')
                                                ->with(array('documents'=> function($query) use($base_url){
                                                                    $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id','type');
                                                                },'property_rent_frequency:id,type'
                                                            )
                                                        )
                                                ->where('id',$property_id)
                                                ->where('owner_id',$owner_id)
                                                ->first();

            if(!empty($properties)){
                $properties->management_charge    = "0.00";// property pricing
                if($properties->commission_in == 'percentage'){
                    if($properties->property_to == 0){
                        if($properties->rent && $properties->commission){
                            $properties->management_charge = $this->getPricePercentage($properties->rent,$properties->commission);
                        }
                    }else{
                        if($properties->selling_price && $properties->commission){
                            $properties->management_charge = $this->getPricePercentage($properties->selling_price,$properties->commission);
                        }
                    }
                }else{
                    $properties->management_charge = $properties->commission?$properties->commission:'0.00';
                }
                $properties->net_worth                 = "0.00";// Total amount from the stay
                $properties->income                    = "0.00";// Total Received
                $properties->outstanding_due           = "0.00";// Missed Amount
                $properties->pending                   = "0.00";// Pending Total

                $properties       = $this->setToNull('property',$properties);
                if($properties['property']){
                    //calculating revenue
                    if($properties['property']->property_to == 0){
                        $user_property      = UserProperty::select('user_properties.*','owner_properties.frequency','owner_properties.owner_amount',DB::raw('ifnull(owner_properties.rent,0.00) as prop_rent'))
                                                            ->where('property_id',$property_id)
                                                            ->join('owner_properties','owner_properties.id','user_properties.property_id')
                                                            ->where('check_in','<=',date('Y-m-d'))
                                                            ->where('check_out','>=',date('Y-m-d'))
                                                            ->whereIn('user_properties.status',[0,1])
                                                            ->where('cancel_status',0)
                                                            ->first();

                        if($user_property){

                            //function to calculate the amounts
                            $revenue_details         = $this->revenuePropertyDetails($user_property,$user_property->prop_rent);
                            $properties['property']->net_worth          = $revenue_details['net_worth'];
                            $properties['property']->pending            = $revenue_details['pending'];
                            $properties['property']->income             = $revenue_details['income'];
                            $properties['property']->outstanding_due    = $revenue_details['outstanding_due'];
                        }
                    }


                    $property_agent     = AgentProperties::select('agent_properties.id','agents.name','agents.phone',
                                                                DB::raw('CONCAT("'.$base_url.'",agents.image) AS image'),
                                                                'agent_properties.agent_id','agent_properties.property_id')
                                                    ->join('agents','agents.id','agent_properties.agent_id')
                                                    ->where('agent_properties.status',1)
                                                    ->where('agent_properties.property_id',$property_id)
                                                    ->where('agents.status',1)
                                                    ->first();
                    if($property_agent){
                        $properties['property']->property_agent     = $property_agent;
                    }else{
                        $properties['property']->property_agent     = (object)[];
                    }
                }
            }else{
                $properties       = (object)[];
            }

            return response()->json(['status'=>200,'data'=>$properties]);

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function requestedServiceForApproval(Request $request)
    {
        try{
            $page               = $request->page;
            $owner_id           = Auth::user()->id;
            $base_url           = url('/');

            $approval_requests  = UserServiceRequestApprovel::where('status',0)
                                                            ->where('owner_id',$owner_id)
                                                            ->get();
            $requests           = json_decode($approval_requests);
            if($requests){
                $request_ids        = array_column($requests,'request_id');
                $request_details    = UserPropertyServiceRequests::select('id','date','time',DB::raw('ifnull(description,"") as description'),'service_id','status','user_property_id','user_id')
                                                                ->whereIn('id',$request_ids)
                                                                ->with(array('service_related'=> function($query) use($base_url){
                                                                                     $query->select('id','service',DB::raw('CONCAT("'.$base_url.'",image) AS image'));
                                                                                 },'user_property_related:id,property_id','user_service_related:id,request_id,status'
                                                                             )
                                                                        );
                $request_details    = $this->customPagination($request_details,$page);
                if($request_details['data']){
                    foreach($request_details['data'] as $each_request){
                        if($each_request->user_property_related){
                            $property_id    = $each_request->user_property_related->property_id;
                            if($property_id){
                                $each_request->property_details = OwnerProperty::select('id','property_reg_no','property_name','owner_id','property_to','category',
                                                                                    'city','state','country','zip_code',DB::raw('ifnull(street_address_1,"") as street_address_1')
                                                                                    ,DB::raw('ifnull(street_address_2,"") as street_address_2'),DB::raw('ifnull(latitude,"") as latitude')
                                                                                    ,DB::raw('ifnull(longitude,"") as longitude'))
                                                                            ->with(array('property_priority_image'=> function($query) use($base_url){
                                                                                                    $query->select('property_id',DB::raw('CONCAT("'.$base_url.'",document) AS document'));
                                                                                            },'country_rel:id,name','state_rel:id,name','city_rel:id,name','zipcode_rel:id,pincode')
                                                                                    )
                                                                            ->where('id',$property_id)
                                                                            ->first();
                            }else{
                                $each_request->property_details = (object)[];
                            }
                        }
                    }
                }
                $request_data     = [
                    'total_page_count' => $request_details['total_page_count'],
                    'current_page'     => $request_details['current_page'],
                    'request_details'  => $request_details['data'],
                ];
                return response()->json(['status'=>200,'data'=>$request_data]);
            }else{
                $request_data     = [
                    'total_page_count' => 0,
                    'current_page'     => '0',
                    'request_details'  => [],
                ];
                return response()->json(['status'=>200,'data'=>$request_data]);
            }


        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function ownerAcceptingServiceRequest(Request $request)
    {
        $rules = [
            'request_id' => 'required',
        ];
        $messages = [
            'request_id.required' => 'Request id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }

        try{
            $request_id    = $request->request_id;
            $owner_id      = Auth::user()->id;
            $request       = UserServiceRequestApprovel::where('request_id',$request_id)
                                                        ->where('owner_id',$owner_id)
                                                        ->first();

            if($request){
                $update_request     = UserServiceRequestApprovel::where('request_id',$request_id)
                                                                ->where('owner_id',$owner_id)
                                                                ->update(array('status' => 1));
                return response()->json(['status'=>200,'response' => 'Accepted service request successfully']);
            }else{
                return response()->json(['status'=>400,'response' => "Service request doesn't exist"]);
            }
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function ownerRejectingServiceRequest(Request $request)
    {
        $rules = [
            'request_id' => 'required',
        ];
        $messages = [
            'request_id.required' => 'Request id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }

        try{
            $request_id    = $request->request_id;
            $owner_id      = Auth::user()->id;
            $request       = UserServiceRequestApprovel::where('request_id',$request_id)
                                                        ->where('owner_id',$owner_id)
                                                        ->first();
            if($request){
                $update_request     = UserServiceRequestApprovel::where('request_id',$request_id)
                                                                ->where('owner_id',$owner_id)
                                                                ->update(array('status' => 2));
                return response()->json(['status'=>200,'response' => 'Rejected service request successfully']);
            }else{
                return response()->json(['status'=>400,'response' => "Service request doesn't exist"]);
            }
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function listPaymentPropertyAccordingToFilter(Request $request)
    {
        try{
            $property_to    = $request->property_to;
            $page           = $request->page;
            $base_url       = url('/');
            $owner_id       = Auth::user()->id;
            $properties     = OwnerProperty::select('id',DB::raw('ifnull(property_name,"") AS property_name'),'property_reg_no',DB::raw('ifnull(latitude,0.0) AS latitude')
                                                    ,DB::raw('ifnull(longitude,0.0) AS longitude'),'property_to',DB::raw('ifnull(owner_amount,0.00) AS expected_amount')
                                                    ,DB::raw('ifnull(selling_price,0.00) as selling_price'),DB::raw('ifnull(rent,0.00) as rent'),DB::raw('ifnull(frequency,0) AS frequency'))
                                            ->with(array('property_priority_image'=> function($query) use($base_url){
                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                    },'first_owner_payament:id,property_id,amount,date,amount_type,payment_type'
                                                    , 'last_owner_payament:id,property_id,amount,date,amount_type,payment_type'
                                                    ,'property_rent_frequency:id,type'
                                                )
                                            )
                                            ->where('owner_id',$owner_id)
                                            ->where('status',1)
                                            ->where('occupied',1)
                                            ->where('contract_owner','1');
            if($property_to == 1 || $property_to == 0){
                $properties = $properties->where('property_to',$property_to);
            }
            $properties     = $this->customPagination($properties,$page);

            foreach($properties['data'] as $prop){
                if(!$prop->property_priority_image){
                    $prop->property_priority_image = (object)[];
                }
                if(!$prop->first_owner_payament){
                    $prop->first_owner_payament = (object)[];
                }
                if(!$prop->last_owner_payament){
                    $prop->last_owner_payament = (object)[];
                }
                if(!$prop->property_rent_frequency){
                    $prop->property_rent_frequency = (object)[];
                }
            }

            $property_data =[
                'total_page_count' => $properties['total_page_count'],
                'current_page'     => $properties['current_page'],
                'properties'       => $properties['data'],
            ];
            return response()->json(['status'=>200,'property_data' => $property_data ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function paymentPropertyDetails(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id    = $request->property_id;
            $base_url       = url('/');
            $owner_id       = Auth::user()->id;
            $property       = OwnerProperty::select('id',DB::raw('ifnull(property_name,"") AS property_name'),'property_reg_no',DB::raw('ifnull(latitude,0.0) AS latitude')
                                                    ,DB::raw('ifnull(longitude,0.0) AS longitude'),'property_to',DB::raw('ifnull(owner_amount,0.00) AS expected_amount')
                                                    ,DB::raw('ifnull(frequency,0) AS frequency'),DB::raw('ifnull(selling_price,0.00) AS selling_price'),DB::raw('ifnull(rent,0.00) AS rent'))
                                            ->with(array('property_priority_image'=> function($query) use($base_url){
                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                    },'first_owner_payament:id,property_id,amount,date,amount_type,payment_type'
                                                    , 'last_owner_payament:id,property_id,amount,date,amount_type,payment_type'
                                                    ,'property_rent_frequency:id,type'
                                                )
                                            )
                                            ->where('owner_id',$owner_id)
                                            ->where('id',$property_id)
                                            ->where('status',1)
                                            ->where('contract_owner','1')
                                            ->first();
            if($property){
                if(!$property->property_priority_image){
                    unset($property->property_priority_image);
                    $property->property_priority_image = (object)[];
                }
                if(!$property->first_owner_payament){
                    unset($property->first_owner_payament);
                    $property->first_owner_payament = (object)[];
                }
                if(!$property->last_owner_payament){
                    unset($property->last_owner_payament);
                    $property->last_owner_payament = (object)[];
                }
                if(!$property->property_rent_frequency){
                    unset($property->property_rent_frequency);
                    $property->property_rent_frequency = (object)[];
                }
            }

            $payment_data =[
                'property_details'  => $property,
            ];
            return response()->json(['status'=>200,'payment_data' => $payment_data ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function listPaymentPropertyHistory(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id    = $request->property_id;
            $page           = $request->page;
            $owner_id       = Auth::user()->id;
            $check_payment_type     = [6,7];
            $property_payment   = AdminAccount::select('id','amount','amount_type','date','payment_type')
                                                ->where('property_id',$property_id)
                                                ->where('owner_id',$owner_id)
                                                ->whereIn('payment_type',$check_payment_type)
                                                ->orderBy('date','DESC');
            $property_payment     = $this->customPagination($property_payment,$page);

            $property_payment_data =[
                'total_page_count' => $property_payment['total_page_count'],
                'current_page'     => $property_payment['current_page'],
                'payments'       => $property_payment['data'],
            ];

            return response()->json(['status'=>200,'property_payment'  => $property_payment_data]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function eachPaymentHistoryDetails(Request $request)
    {
        $rules = [
            'payment_id' => 'required',
            'property_id' => 'required',
        ];
        $messages = [
            'payment_id.required' => 'Payment id is required',
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $payment_id     = $request->payment_id;
            $property_id    = $request->property_id;
            $base_url       = url('/');
            $owner_id       = Auth::user()->id;
            $service_details= "";
            $check_payment_type     = [6,7];
            $payment_account        = AdminAccount::select('id','amount','amount_type','date','payment_type','property_id','reference_id')
                                                    ->where('owner_id',$owner_id)
                                                    ->with('payment_received:id,payment_id,status')
                                                    ->where('property_id',$property_id)
                                                    ->where('id',$payment_id)
                                                    ->whereIn('payment_type',$check_payment_type)
                                                    ->first();

            if($payment_account){
                if(!$payment_account->payment_received){
                    unset($payment_account->payment_received);
                    $payment_account->payment_received  = (object)[];
                }
                if($payment_account->payment_type == 6){#property service by owner
                    $service_request_doc    = OwnerPropertyServiceRequestDocuments::where('id',$payment_account->reference_id)
                                                                                    ->where('document_type',1)
                                                                                    ->first();
                    if($service_request_doc){
                        $service_details        = OwnerPropertyServiceRequests::select('id','service_id',DB::raw('ifnull(description,"") AS description'))
                                                                                ->with('service_related:id,service')
                                                                                ->where('id',$service_request_doc->request_id)
                                                                                ->where('owner_property_id',$property_id)
                                                                                ->where('owner_id',$owner_id)
                                                                                ->first();
                    }
                }
            }
            if(!$service_details){
                $service_details        = (object)[];
            }
            $payment_data =[
                'payment_details'  => $payment_account,
                'service_details'  => $service_details,
            ];
            return response()->json(['status'=>200,'payment_data' => $payment_data ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }



    public function monthlyAccountsList(Request $request)
    {
        try{
            $owner_id           = Auth::user()->id;
            if($request->year){
                $year           = $request->year;
            }else{
                $year               = date('Y');
            }

            $months             =['01','02','03','04','05','06','07','08','09','10','11','12'];
            $revenue_array      = [];
            $expense_array      = [];
            foreach($months as $each_month){
                $month_revenue  = AdminAccount::where('owner_id',$owner_id)
                                                ->where('date','like',$year.'-'.$each_month.'-%')
                                                ->where('amount_type',2)
                                                ->sum('amount');
                // $associate_revenue  = [ $each_month =>  $month_revenue ];
                $month_revenue  = strval($month_revenue);
                array_push($revenue_array,$month_revenue);

                $month_expense  = AdminAccount::where('owner_id',$owner_id)
                                                ->where('date','like',$year.'-'.$each_month.'-%')
                                                ->where('amount_type',1)
                                                ->sum('amount');
                // $associate_expense  = [ $each_month =>  $month_expense ];
                $month_expense  = strval($month_expense);
                array_push($expense_array,$month_expense);
            }

            $data       = [
                'revenue'   => $revenue_array,
                'expense'   => $expense_array
            ];
            return response()->json(['status'=>200,'data' => $data ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function monthWisePropertyAccounts(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id        = $request->property_id;
            $owner_id           = Auth::user()->id;
            if($request->date){
                $date           = date('Y-m',strtotime($request->date));
            }else{
                $date           = date('Y-m');
            }

            $check_property     = OwnerProperty::where('owner_id',$owner_id)
                                                ->where('id',$property_id)
                                                ->where('contract_owner','1')
                                                ->where('status',1)
                                                ->first();
            if(!$check_property){
                return response()->json(['status'=>400,'response' => 'Invalid Property' ]);
            }

            $month_revenue  = AdminAccount::where('owner_id',$owner_id)
                                            ->where('date','like',$date.'-%')
                                            ->where('property_id',$property_id)
                                            ->where('amount_type',2)
                                            ->sum('amount');
            $month_expense  = AdminAccount::where('owner_id',$owner_id)
                                            ->where('date','like',$date.'-%')
                                            ->where('property_id',$property_id)
                                            ->where('amount_type',1)
                                            ->sum('amount');

            $data       = [
                'revenue'   => strval($month_revenue),
                'expense'   => strval($month_expense)
            ];
            return response()->json(['status'=>200,'data' => $data ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function listPropertyForViewingAccountsOwnerHome(Request $request)
    {
        try{
            $owner_id           = Auth::user()->id;
            $properties     = OwnerProperty::select('id',DB::raw('ifnull(property_name,"") AS property_name'))
                                            ->where('owner_id',$owner_id)
                                            ->where('contract_owner','1')
                                            ->where('status',1)
                                            ->get();

            return response()->json(['status'=>200,'properties' => $properties ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function setPaymentReceived(Request $request)
    {
        $rules = [
            'payment_id' => 'required',
            'property_id' => 'required',
        ];
        $messages = [
            'payment_id.required' => 'Payment id is required',
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $owner_id           = Auth::user()->id;
            $payment_id         = $request->payment_id;
            $property_id        = $request->property_id;

            $check_account      = AdminAccount::where('id',$payment_id)
                                                ->where('property_id',$property_id)
                                                ->first();

            if(!$check_account){
                return response()->json(['status'=>400,'response'=>'Invalid Payment']);
            }

            $pay_received               = new OwnerPaymentReceived();
            $pay_received->payment_id   = $payment_id;
            $pay_received->property_id  = $property_id;
            $pay_received->owner_id     = $owner_id;
            $pay_received->status       = 1;
            $pay_received->save();

            return response()->json(['status'=>200,'response' => 'Payment Received' ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function ownerHomeCounts(Request $request)
    {
        try{
            $property_count     = OwnerProperty::where('owner_id',Auth::user()->id)->where(function ($query){
                                                            return $query->where('is_builder','0')->orWhereNull('is_builder');
                                                        }
                                                )->count();
            $occupied_count     = OwnerProperty::where('owner_id',Auth::user()->id)->where('occupied',1)->count();
            $vacant_count       = OwnerProperty::where('owner_id',Auth::user()->id)->where('occupied','0')->count();
            $maintenance_count  = OwnerPropertyServiceRequests::where('owner_id',Auth::user()->id)->whereIn('status',[0,1,2])->distinct('owner_property_id')->count();
            $data   =[
                'property_count'    => $property_count,
                'occupied_count'    => $occupied_count,
                'vacant_count'      => $vacant_count,
                'maintenance_count' => $maintenance_count,
            ];
            return response()->json(['status'=>200,'data' => $data ]);
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function ownerPropertyBuildingDetails(Request $request)
    {
        $rules = [
            'owner_property_id' => 'required',
        ];
        $messages = [
            'owner_property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id    = $request->owner_property_id;
            $owner_id       = Auth::user()->id;
            $base_url       = url('/');
            $building       = OwnerProperty::select('id','property_reg_no','property_name','property_to', 'is_featured','is_builder','city','type_id','status','contract_owner','owner_confirmation')
                                                ->with(array('documents'=> function($query) use($base_url){
                                                                    $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                },'city_rel:id,name'
                                                            )
                                                        )
                                                ->where('id',$property_id)
                                                ->where('owner_id',$owner_id)
                                                ->where('is_builder',1)
                                                ->first();
            if($building){
                $building->net_worth                 = "0.00";// Total amount from the stay
                $building->income                    = "0.00";// Total Received
                $building->outstanding_due           = "0.00";// Missed Amount
                $building->pending                   = "0.00";// Pending Total

                $building_apartments   = OwnerProperty::select('id','property_reg_no','property_name','property_to','occupied',DB::raw('ifnull(frequency,0) AS frequency'),'type_id')
                                                        ->with(array('property_priority_image'=> function($query) use($base_url){
                                                                    $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                },'type_details:id,type'
                                                            )
                                                        )
                                                        ->where('builder_id',$building->id)
                                                        ->where('owner_id',$owner_id)
                                                        ->where('is_builder',NULL)
                                                        ->get();

                if($building->property_to == 0){
                    if($building_apartments){
                        foreach($building_apartments as $appartments){
                            if(!$appartments->type_details){
                                unset($appartments->type_details);
                                $appartments->type_details  = (object)[];
                            }
                            if($appartments->occupied == 1){
                                $user_current_property  = UserProperty::where('property_id',$appartments->id)
                                                                        ->where('check_in','>=',date('Y-m-d'))
                                                                        ->where('check_out','<=',date('Y-m-d'))
                                                                        ->whereIn('status',[0,1,2])
                                                                        ->where('cancel_status',0)
                                                                        ->first();
                                if($user_current_property){
                                    $appartments->occupied_from    = $user_current_property->check_in;
                                    $appartments->occupied_to      = $user_current_property->check_out;
                                }else{
                                    $appartments->occupied_from    = date('Y-m-d');
                                    $appartments->occupied_to      = date('Y-m-d');
                                }
                            }else{
                                $vacated_since  = UserProperty::select('id','check_out')
                                                                ->where('property_id',$appartments->id)
                                                                ->where('check_out','>=',date('Y-m-d'))
                                                                ->where('status',3)
                                                                ->where('cancel_status',0)
                                                                ->orderBy('check_out','DESC')
                                                                ->first();
                                $rent_out_count = UserProperty::where('property_id',$appartments->id)
                                                                ->where('status',3)
                                                                ->where('cancel_status',0)
                                                                ->count();
                                if($vacated_since){
                                    $appartments->vacated_since    = $vacated_since;
                                    $appartments->rent_out_count   = $rent_out_count;
                                }else{
                                    $appartments->vacated_since    = date('Y-m-d');
                                    $appartments->rent_out_count   = 0;
                                }
                            }
                        }
                    }
                    $user_property      = UserProperty::select('user_properties.*','owner_properties.frequency','owner_properties.owner_amount',DB::raw('ifnull(owner_properties.rent,0.00) AS prop_rent'))
                                                        // ->whereIn('property_id',$building_ids)
                                                        ->join('owner_properties','owner_properties.id','user_properties.property_id')
                                                        ->where('owner_properties.is_builder',NULL)
                                                        ->where('owner_properties.builder_id',$building->id)
                                                        ->where('check_in','<=',date('Y-m-d'))
                                                        ->where('check_out','>=',date('Y-m-d'))
                                                        ->where('cancel_status',0)
                                                        ->whereIn('user_properties.status',[0,1])
                                                        ->get();
                    if($user_property){
                        $revenue_details                = $this->revenueBuildingDetails($user_property);
                        $building->net_worth          = $revenue_details['net_worth'];
                        $building->pending            = $revenue_details['pending'];
                        $building->income             = $revenue_details['income'];
                        $building->outstanding_due    = $revenue_details['outstanding_due'];
                    }
                }
                $building->building_apartments   = $building_apartments;
                $total_appartments_count    = OwnerProperty::where('owner_id',$owner_id)->where('builder_id',$building->id)->count();
                $vacated_appartments_count  = OwnerProperty::where('owner_id',$owner_id)->where('builder_id',$building->id)->where('occupied',0)->count();
                $occupied_appartments_count = OwnerProperty::where('owner_id',$owner_id)->where('builder_id',$building->id)->where('occupied',1)->count();
                $building->appartments_count     = $total_appartments_count;
                $building->vacated               = $vacated_appartments_count;
                $building->occupied              = $occupied_appartments_count;
            }else{
                $building           = (object)[];
            }

            return response()->json(['status'=>200,'data'=>$building]);

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    //owner-notification
    public function ownerNotifications(request $request){
        $page       = $request->page;
        try{
            $notifications = OwnerNotification::select('id','title','description',DB::raw('ifnull(status,0) as status'),DB::raw('ifnull(created_at,"1970-01-01 00:00:00") as date'))
                                                ->where('owner_id',Auth::user()->id);

            $notifications = $this->customPagination($notifications,$page);
            $notifications_data = [
                'total_page_count'      => $notifications['total_page_count'],
                'current_page'          => $notifications['current_page'],
                'notifications'            => $notifications['data'],
            ];
            return response()->json(['status' => 200,  'data' => $notifications_data]);

        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    //update notifaction status
    public function updateNotificationStatus(Request $request)
    {
        $rules = [
            'notification_id'               => 'required',
        ];
        $messages = [
            'notification_id.required'      => 'Notification is Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            $owner_notification     = OwnerNotification::where('id',$request->notification_id)->first();
            if($owner_notification){
                OwnerNotification::where('id',$request->notification_id)->update(['status' => 1]);
                return response()->json(['status'=>200,'response' => 'Status Updated']);
            }else{
                return response()->json(['status'=>400,'response' => 'Invalid Notification']);
            }
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function downloadPropertyReport(Request $request)
    {
        $rules = [
            'property_id'               => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property id is Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try{
            $owner_id               = Auth::user()->id;
            $base_url               = url('/');
            $property_id            = $request->property_id;
            $properties             = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,0) AS property_name'),'property_to',DB::raw('ifnull(frequency,0) as frequency'),
                                                        DB::raw('CONCAT("'.$base_url.'",contract_file) AS contract_file'), 'is_featured',DB::raw('ifnull(owner_amount,0) as owner_amount'))
                                                    ->with(array('documents'=> function($query) use($base_url){
                                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                    },'property_rent_frequency:id,type'
                                                                )
                                                            )
                                                    ->where('id',$property_id)
                                                    ->where('owner_id',$owner_id)
                                                    ->first();

            if($properties){
                $service_payment_amount     = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',1)->where('payment_type',6)->sum('amount');
                $total_payment_amount       = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->sum('amount');
                $total_payment_count        = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->count();
                $data   =[
                    'property' => $properties,
                    'service_payment_amount' => $service_payment_amount,
                    'total_payment_amount' => $total_payment_amount,
                    'total_payment_count' => $total_payment_count,
                ];
                $filename = time() . rand().'pdf.pdf';

                $content    = view('admin.pdf.owner_download_report',$data);
                // return $content;

                $pdf        = PDF::loadHTML(' '.$content.' ');
                // // Save file to the directory
                $pdf->save('uploads/pdf/'.$filename);

                $pdffile = $base_url.'/uploads/pdf/'.$filename;
                $data =[
                    'pdf' => $pdffile,
                ];
                return ['status' => 200, 'data' => $data];
            }

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function downloadAllPropertyReport(Request $request)
    {
        try{
            $owner_id               = Auth::user()->id;
            $base_url               = url('/');
            $properties             = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,0) AS property_name'),'property_to',DB::raw('ifnull(frequency,0) as frequency'),
                                                        DB::raw('CONCAT("'.$base_url.'",contract_file) AS contract_file'), 'is_featured',DB::raw('ifnull(owner_amount,0) as owner_amount'),'type_id',
                                                        'contract_start_date','contract_end_date','occupied','status')
                                                    ->with(array('documents'=> function($query) use($base_url){
                                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                    },'property_rent_frequency:id,type'
                                                                )
                                                            )
                                                    ->where('is_builder','!=',1)
                                                    ->where('owner_id',$owner_id)
                                                    ->get();

            if($properties){
                $data   =[
                    'properties' => $properties,
                ];
                foreach($properties as $property){
                    $property_id   = $property->id;
                    $service_payment_amount     = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',1)->where('payment_type',6)->sum('amount');
                    $total_payment_amount       = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->sum('amount');
                    $total_payment_count        = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->count();


                    $property->service_payment_amount   = $service_payment_amount;
                    $property->total_payment_amount     = $total_payment_amount;
                    $property->total_payment_count      = $total_payment_count;
                }
                $filename = time() . rand().'pdf.pdf';

                $content    = view('admin.pdf.owner_common_report',$data);
                // return $content;

                $pdf        = PDF::loadHTML(' '.$content.' ');
                // // Save file to the directory
                $pdf->save('uploads/pdf/'.$filename);

                $pdffile = $base_url.'/uploads/pdf/'.$filename;
                $data =[
                    'pdf' => $pdffile,
                ];
                return ['status' => 200, 'data' => $data];
            }

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function overAllYearlyReport(Request $request)
    {
        try{
            $owner_id           = Auth::user()->id;
            $base_url            = url('/');
            if($request->year){
                $year           = $request->year;
            }else{
                $year               = date('Y');
            }

            $month_name         = ['January','February','March','April','May','June','July','August','September','October','November','December'];
            $month_num          = ['01','02','03','04','05','06','07','08','09','10','11','12'];
            $revenue_array      = [];
            $expense_array      = [];
            $new_month          = [];
            foreach($month_num as $key => $each_month){
                $month_revenue  = AdminAccount::where('owner_id',$owner_id)
                                                ->where('date','like',$year.'-'.$each_month.'-%')
                                                ->where('amount_type',2)
                                                ->sum('amount');
                // $associate_revenue  = [ $each_month =>  $month_revenue ];
                $month_revenue  = strval($month_revenue);
                // array_push($revenue_array,$month_revenue);

                $month_expense  = AdminAccount::where('owner_id',$owner_id)
                                                ->where('date','like',$year.'-'.$each_month.'-%')
                                                ->where('amount_type',1)
                                                ->sum('amount');
                // $associate_expense  = [ $each_month =>  $month_expense ];
                $month_expense  = strval($month_expense);
                // array_push($expense_array,$month_expense);

                $monthly_array     = array(
                        'revenue' => $month_revenue,
                        'expense' => $month_expense,
                );
                $new_month[$month_name[$key]]   = $monthly_array;
            }

            $data       = [
                'months'    => $new_month,
                'year'      => $year,
            ];

            $filename = time() . rand().'pdf.pdf';

            $content    = view('admin.pdf.owner_yearly_report',$data);

            $pdf        = PDF::loadHTML(' '.$content.' ');
            // // Save file to the directory
            $pdf->save('uploads/pdf/'.$filename);

            $pdffile = $base_url.'/uploads/pdf/'.$filename;
            $data =[
                'pdf' => $pdffile,
            ];
            return ['status' => 200, 'data' => $data];
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function eachMonthPropertyReport(Request $request)
    {
        $rules = [
            'property_id' => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(),$rules,$messages);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            $property_id        = $request->property_id;
            $owner_id           = Auth::user()->id;
            $base_url           = url('/');
            if($request->date){
                $date           = date('Y-m',strtotime($request->date));
            }else{
                $date           = date('Y-m');
            }

            $check_property     = OwnerProperty::where('owner_id',$owner_id)
                                                ->where('id',$property_id)
                                                ->where('contract_owner','1')
                                                ->where('status',1)
                                                ->first();
            if(!$check_property){
                return response()->json(['status'=>400,'response' => 'Invalid Property' ]);
            }

            $month_revenue  = AdminAccount::where('owner_id',$owner_id)
                                            ->where('date','like',$date.'-%')
                                            ->where('property_id',$property_id)
                                            ->where('amount_type',2)
                                            ->sum('amount');
            $month_expense  = AdminAccount::where('owner_id',$owner_id)
                                            ->where('date','like',$date.'-%')
                                            ->where('property_id',$property_id)
                                            ->where('amount_type',1)
                                            ->sum('amount');

            //propertydetails
            $property_details   = OwnerProperty::select('id','property_name','property_to','property_reg_no')
                                                ->where('id',$property_id)
                                                ->first();

            $data       = [
                'property'  => $property_details,
                'revenue'   => strval($month_revenue),
                'expense'   => strval($month_expense),
                'month_year'=> date('F Y',strtotime($date.'-01')),
            ];
            $filename = time() . rand().'pdf.pdf';

            $content    = view('admin.pdf.owner_property_month_report',$data);

            $pdf        = PDF::loadHTML(' '.$content.' ');
            // // Save file to the directory
            $pdf->save('uploads/pdf/'.$filename);

            $pdffile = $base_url.'/uploads/pdf/'.$filename;
            $data =[
                'pdf' => $pdffile,
            ];
            return ['status' => 200, 'data' => $data];
        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function downloadOwnerPropertyOccupiedReport(Request $request)
    {
        try{
            $owner_id               = Auth::user()->id;
            $base_url               = url('/');
            $properties             = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,0) AS property_name'),'property_to',DB::raw('ifnull(frequency,0) as frequency'),
                                                        DB::raw('CONCAT("'.$base_url.'",contract_file) AS contract_file'), 'is_featured',DB::raw('ifnull(owner_amount,0) as owner_amount'),'type_id',
                                                        'contract_start_date','contract_end_date','occupied','status')
                                                    ->with(array('documents'=> function($query) use($base_url){
                                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                    },'property_rent_frequency:id,type'
                                                                )
                                                            )
                                                    ->where('is_builder','!=',1)
                                                    ->where('occupied',1)
                                                    ->where('owner_id',$owner_id)
                                                    ->get();

            if($properties){
                $data   =[
                    'properties' => $properties,
                ];
                foreach($properties as $property){
                    $property_id   = $property->id;
                    $service_payment_amount     = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',1)->where('payment_type',6)->sum('amount');
                    $total_payment_amount       = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->sum('amount');
                    $total_payment_count        = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->count();


                    $property->service_payment_amount   = $service_payment_amount;
                    $property->total_payment_amount     = $total_payment_amount;
                    $property->total_payment_count      = $total_payment_count;
                }
                $filename = time() . rand().'pdf.pdf';

                $content    = view('admin.pdf.owner_common_report',$data);
                // return $content;

                $pdf        = PDF::loadHTML(' '.$content.' ');
                // // Save file to the directory
                $pdf->save('uploads/pdf/'.$filename);

                $pdffile = $base_url.'/uploads/pdf/'.$filename;
                $data =[
                    'pdf' => $pdffile,
                ];
                return ['status' => 200, 'data' => $data];
            }

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }

    public function downloadOwnerPropertyVacantReport(Request $request)
    {
        try{
            $owner_id               = Auth::user()->id;
            $base_url               = url('/');
            $properties             = OwnerProperty::select('id','property_reg_no',DB::raw('ifnull(property_name,0) AS property_name'),'property_to',DB::raw('ifnull(frequency,0) as frequency'),
                                                        DB::raw('CONCAT("'.$base_url.'",contract_file) AS contract_file'), 'is_featured',DB::raw('ifnull(owner_amount,0) as owner_amount'),'type_id',
                                                        'contract_start_date','contract_end_date','occupied','status')
                                                    ->with(array('documents'=> function($query) use($base_url){
                                                                        $query->select(DB::raw('CONCAT("'.$base_url.'",document) AS document'),'property_id');
                                                                    },'property_rent_frequency:id,type'
                                                                )
                                                            )
                                                    ->where('is_builder','!=',1)
                                                    ->where('occupied',0)
                                                    ->where('owner_id',$owner_id)
                                                    ->get();

            if($properties){
                $data   =[
                    'properties' => $properties,
                ];
                foreach($properties as $property){
                    $property_id   = $property->id;
                    $service_payment_amount     = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',1)->where('payment_type',6)->sum('amount');
                    $total_payment_amount       = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->sum('amount');
                    $total_payment_count        = AdminAccount::where('property_id',$property_id)->where('owner_id',$owner_id)->where('amount_type',2)->where('payment_type',7)->count();


                    $property->service_payment_amount   = $service_payment_amount;
                    $property->total_payment_amount     = $total_payment_amount;
                    $property->total_payment_count      = $total_payment_count;
                }
                $filename = time() . rand().'pdf.pdf';

                $content    = view('admin.pdf.owner_common_report',$data);
                // return $content;

                $pdf        = PDF::loadHTML(' '.$content.' ');
                // // Save file to the directory
                $pdf->save('uploads/pdf/'.$filename);

                $pdffile = $base_url.'/uploads/pdf/'.$filename;
                $data =[
                    'pdf' => $pdffile,
                ];
                return ['status' => 200, 'data' => $data];
            }

        }catch (\Exception $e) {
            return response()->json(['status'=>500,'response' => 'Internal Server Error']);
        }
    }


    public function revenuePropertyDetails($user_property,$owner_amount)
    {
        $check_in_date      = $user_property->check_in;
        $check_out_date     = $user_property->check_out;

        $date_diff = abs(strtotime($check_in_date) - strtotime($check_out_date));
        $diff_days = $date_diff/(60*60*24);

        $frequencies         = Frequency::where('id',$user_property->frequency)->first();

        if($user_property->frequency){
            $frequency_days     = $frequencies->days?$frequencies->days:0;
            $count_frequency_payments  = $diff_days/$frequency_days;
            $owner_amount  = $owner_amount?$owner_amount:0;
            $net_worth     = $owner_amount * round($count_frequency_payments);

            $accounts_rel       = AdminAccount::where('owner_id',Auth::user()->id)->where('user_property_id',$user_property->id)->where('payment_type',7)->where('amount_type',2);
            $income        = $accounts_rel->sum('amount');
            $next_due_date = $user_property->due_date;
            $current_date  = date('Y-m-d');
            if($next_due_date < $current_date && !empty($next_due_date)){
                $out_diff = abs(strtotime($current_date) - strtotime($next_due_date));
                $out_days = $out_diff/(60*60*24);
                $count_payable      = ($out_days/$frequency_days) +1;
                $outstanding_due     = $owner_amount * round($count_payable);
            }else{
                $outstanding_due = '0';
            }
            $pending       = $net_worth - $income;


            $data['net_worth']         = strval(number_format($net_worth,2));
            $data['pending']           = strval(number_format($pending,2));
            $data['income']            = strval(number_format($income,2));
            $data['outstanding_due']   = strval(number_format($outstanding_due,2));

            return $data;
        }
    }


    public function revenueBuildingDetails($user_property)
    {
        $static_net_worth           = 0;
        $static_pending             = 0;
        $static_income              = 0;
        $static_outstanding_due     = 0;
        foreach($user_property as $each_user_property){
            $check_in_date      = $each_user_property->check_in;
            $check_out_date     = $each_user_property->check_out;

            $date_diff = abs(strtotime($check_in_date) - strtotime($check_out_date));

            $diff_days = $date_diff/(60*60*24);

            $frequencies         = Frequency::where('id',$each_user_property->frequency)->first();

            if($each_user_property->frequency){
                $frequency_days     = $frequencies->days?$frequencies->days:0;
                $count_frequency_payments  = $diff_days/$frequency_days;

                $owner_amount       = $each_user_property->prop_rent?$each_user_property->prop_rent:0;

                $net_worth     = $owner_amount * round($count_frequency_payments);

                $accounts_rel       = AdminAccount::where('owner_id',Auth::user()->id)->where('user_property_id',$each_user_property->id)->where('payment_type',7)->where('amount_type',2);
                $income        = $accounts_rel->sum('amount');
                $next_due_date = $each_user_property->due_date;
                $current_date  = date('Y-m-d');
                if($next_due_date < $current_date && !empty($next_due_date)){
                    $out_diff = abs(strtotime($current_date) - strtotime($next_due_date));
                    $out_days = $out_diff/(60*60*24);
                    $count_payable      = ($out_days/$frequency_days) +1;
                    $outstanding_due     = $owner_amount * round($count_payable);
                }else{
                    $outstanding_due = 0;
                }
                $pending       = $net_worth - $income;

                $static_net_worth         += $net_worth;
                $static_pending           += $pending;
                $static_income            += $income;
                $static_outstanding_due   += $outstanding_due;

            }

        }
        if($static_net_worth){
            $data['net_worth']         = number_format($static_net_worth,2);
        }else{
            $data['net_worth']         = '0.00';
        }
        if($static_pending){
            $data['pending']           = number_format($static_pending,2);
        }else{
            $data['pending']           = '0.00';
        }
        if($static_income){
            $data['income']            = number_format($static_income,2);
        }else{
            $data['income']            = '0.00';
        }
        if($static_outstanding_due){
            $data['outstanding_due']   = number_format($static_outstanding_due,2);
        }else{
            $data['outstanding_due']   = '0.00';
        }

        return $data;
    }


    public function changePassword(Request $request){
        $userid = Auth::user()->id;
        $user   = Owner::where('id',$userid)->first();
        if($user){
            if (Hash::check($request->current_password, $user->password)){
                if($request->new_password == $request->confirm_new_password){
                    $user->password     = Hash::make($request->new_password);
                    $user->save();
                    return response()->json(['status'=>200 , 'response'=>"Password Changed"]);
                }else{
                    return response()->json(['status'=>400 , 'response'=>"New password and Confirm Password does not match"]);
                }
            }else{
                return response()->json(['status'=>400,'response'=>'Current Password does not matches']);
            }
        }
    }


}
