<?php

namespace App\Http\Controllers\api\agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use App\Models\Agent;
use App\Models\OwnerProperty;
use App\Models\OwnerPropertyDocument;
use App\Models\OwnerPropertyAmenity;
use App\Models\Amenity;
use App\Models\OwnerPropertyDetail;
use App\Models\BookUserTour;
use App\Models\AgentTrackingUserTour;
use App\Models\User;
use App\Models\UserPropertyBookings;
use App\Models\UserPropertyBookingDocuments;
use App\Models\UserProperty;
use App\Models\AgentRequestingContract;
use App\Models\UserPropertyDocument;
use App\Models\Detail;
use App\Models\AmenitiesCategory;
use App\Models\AgentTask;
use App\Models\PendingPropertyAgent;
use App\Models\PendingPropertyForVerification;
use App\Models\UserAgentProperty;
use App\Models\AgentAppointment;
use App\Models\AgentTrackingOwnerTour;
use App\Models\Owner;
use App\Models\AgentCashInHand;
use App\Models\AgentFeedbacks;
use App\Models\AgentCashPayRequest;
use App\Models\AdminAccount;
use App\Models\OwnerPropertyTermsOfStay;
use App\Models\AgentNotifications;
use App\Models\PropertyEmiDetails;
use App\Models\OtherPropertyCharges;
use App\Models\BookingContract;
use App\Models\AgentEarnings;
use App\Models\RequestAgentEarnings;
use App\Models\OfferPackage;
use App\Models\UserPropertyRating;
use App\Models\City;
use App\Models\UserPropertyFullAmountDocument;
use Validator;
use Session;
use Auth;
use DB;
use PDF;
use Storage;

class AgentController extends Controller
{
    public function Logout(Request $request)
    {
        $owner = Auth::user()->token();
        $owner->revoke();
        Session::flush();
        return response()->json(['status' => 200, 'response' => "Logged out successfully"]);
    }
    public function agentProfile(Request $request)
    {
        $agentId    = Auth::user()->id;
        $agents     = Agent::select('id', 'name', 'email', 'phone', 'account_number', 'bank_name', 'ifsc', 'ifsc', 'image')->where('id', $agentId)->first();
        $agents['image']  = URL::to('/') . $agents->image;
        return response()->json(['status' => 200, 'response' => $agents]);
    }
    public function updateAgentProfile(Request $request)
    {
        $agentId                = Auth::user()->id;
        $agent                  = Agent::where('id', $agentId)->first();
        $agent->name            = $request->name;
        $agent->email           = $request->email;
        $agent->phone           = $request->phone;
        $agent->account_number  = $request->account_number;
        $agent->bank_name       = $request->bank_name;
        $agent->ifsc            = $request->ifsc;
        $id_file_name           = '';
        if ($request->file('image')) {
            $path = public_path('uploads/agents/');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $file                   = $request->file('image');
            $id_file_name           = time() . rand() . '.' . $file->getClientOriginalName();
            $file->move($path, $id_file_name);
            $agent->image           = '/uploads/agents/' . $id_file_name;
        }
        $agent->save();
        return response()->json(['status' => 200, 'response' => "Agent SuccessFully Updated"]);
    }


    public function assignedPropertyofAgent(Request $request)
    {
        $agentId    = Auth::user()->id;
        $page        = $request->page;
        $base_url   = url('/');
        $properties = OwnerProperty::select('owner_properties.id', 'property_name', 'property_to', 'city', 'state', 'country', 'latitude', 'longitude', DB::raw('ifnull(is_builder,2) as is_builder'))
            ->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
            ->with(
                array(
                    'property_priority_image' =>
                    function ($query) use ($base_url) {
                        $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                    }, 'country_rel:id,name', 'city_rel:id,name', 'state_rel:id,name'
                )
            )
            ->where('agent_properties.agent_id', $agentId)
            ->distinct('owner_properties.id')
            ->orderBy('owner_properties.id', 'DESC')
            ->where('agent_properties.status', 1);
        // ->get();


        $properties       = $this->customPagination($properties, $page);


        if ($properties['data']) {
            foreach ($properties['data'] as $property) {
                if ($property->property_priority_image == "") {
                    $property->property_priority_image = (object)[];
                }
                if ($property->country_rel == "") {
                    $property->country_rel   = (object)[];
                }
                if ($property->city_rel == "") {
                    $property->city_rel   = (object)[];
                }
                if ($property->state_rel == "") {
                    $property->state_rel   = (object)[];
                }
            }
        }


        $property_data  = [
            'total_page_count' => $properties['total_page_count'],
            'current_page'     => $properties['current_page'],
            'user_properties'         => $properties['data'],
        ];
        //$properties->toArray();
        return response()->json(['status' => 200, 'response' => $property_data]);
    }


    public function agentPropertyDetails(Request $request)
    {


        $agentId        = Auth::user()->id;
        $base_url       = url('/');
        $property_id    = $request->property_id;
        $verfication_property    = PendingPropertyForVerification::where('property_id', $property_id)
            ->where('agent_id', $agentId)
            ->first();
        // dd($verfication_property);
        $properties     = OwnerProperty::select(
            'owner_properties.id',
            'property_reg_no',
            'property_name',
            DB::raw('ifnull(property_type,"") AS property_type'),
            'owner_id',
            'rating',
            'property_to',
            'category',
            'selling_price',
            'mrp',
            'is_featured',
            'furnished',
            'longitude',
            'latitude',
            'rent',
            'city',
            'state',
            'country',
            'description',
            'occupied',
            'zip_code',
            'street_address_1',
            'street_address_2',
            DB::raw('ifnull(type_id,0) as type_id'),
            'expected_amount',
            DB::raw('ifnull(is_builder,2) AS is_builder')
        );
        $properties     = $properties->with(
            array(
                'documents' => function ($query) use ($base_url) {
                    $query->select(DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'id', 'property_id', 'type', 'owner_id');
                },
                'amenity_details' => function ($query) {
                    $query->select('id', 'property_id', 'amenity_id', 'owner_id', 'amenities_category_id');
                },
                'property_details' => function ($query) {
                    $query->select('property_id', 'detail_id', 'value', 'name');
                },
                'owner_rel' => function ($query) use ($base_url) {
                    $query->select('id', 'name', 'phone', DB::raw('CONCAT("' . $base_url . '", profile_image) AS profile_image'));
                },
                'country_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'state_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'city_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'zipcode_rel' => function ($query) {
                    $query->select('id', 'pincode', 'city');
                },
                'floor_plans' => function ($query) use ($base_url) {
                    $query->select('id', 'property_id', DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'owner_id');
                }
            )
        );

        if (!$verfication_property) {
            $properties         =   $properties->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1);
        }
        $properties         =   $properties->where('owner_properties.id', $property_id)
            ->first();


        if ($properties) {
            $properties     = $this->setToNull('property', $properties);

            $details          = Detail::select('id', 'name')->get();




            $amenity_category = AmenitiesCategory::select('id', 'name')->orderBy('sort_order', 'ASC')->get();

            $prop = $properties['property'];
            if (!$prop->owner_rel) {
                $prop->owner_rel = (object)[];
            }
            if (!$prop->country_rel) {
                $prop->country_rel = (object)[];
            }
            if (!$prop->state_rel) {
                $prop->state_rel = (object)[];
            }
            if (!$prop->city_rel) {
                $prop->city_rel = (object)[];
            }
            if (!$prop->zipcode_rel) {
                $prop->zipcode_rel = (object)[];
            }
            if (!$prop->floor_plans) {
                $prop->floor_plans = (object)[];
            }

            if ($prop->property_details) {
                $new_property = [];
                foreach ($details as $key => $each_detail) {
                    $count = 0;
                    foreach ($prop->property_details as $key => $detail_array) {
                        if ($each_detail->name == $detail_array->name) {
                            $count = 1;
                            $type = $detail_array->name;
                            $prop->$type = $detail_array->value;
                            break;
                        }
                    }
                    if ($count != 1) {
                        $detail_data = $each_detail->name;
                        $prop->$detail_data = 0;
                    }
                }
            }

            if ($prop->amenity_details) {
                $new_property = [];
                $assigned_amenities   = [];
                foreach ($amenity_category as $key => $each_category) {
                    $count = 0;
                    $property_amenities = [];
                    foreach ($prop->amenity_details as $key_new => $each_category_detail) {
                        if ($each_category->id == $each_category_detail->amenities_category_id) {

                            $type = $each_category->name;

                            $amenities = OwnerPropertyAmenity::select('amenities.id', 'amenities.name', DB::raw('CONCAT("' . $base_url . '", amenities.image) AS image'))
                                ->join('amenities', 'amenities.id', 'owner_property_amenities.amenity_id')
                                // ->where('id',$each_category_detail->amenity_id)
                                ->where('owner_property_amenities.amenities_category_id', $each_category->id)
                                ->where('owner_property_amenities.property_id', $property_id)
                                ->get();
                            $encoded_amenities = json_encode($amenities);
                            $decoded_amenities = json_decode($encoded_amenities);
                            foreach ($decoded_amenities as $key => $file_data) {
                                $property_amenities[] = $file_data;
                            }
                            $count = 1;
                            break;
                        }
                    }
                    if ($count == 1) {
                        if (count($property_amenities) > 0) {
                            $amenities_array = [
                                'id'         => $each_category->id,
                                'name'       => $each_category->name,
                                'amenity_details' => $property_amenities
                            ];
                            $assigned_amenities[]  = $amenities_array;
                            // $prop->amenity_categories = $amenities_array;

                        }
                    }
                }
                $prop->amenity_categories   = $assigned_amenities;
            }
            // unset($prop->property_details);
            unset($prop->amenity_details);

            // count of feedback as rating
            $prop->total_rating_count = UserPropertyRating::where('property_id', $property_id)->count();
        } else {
            $properties['property'] = (object)[];
        }

        return response()->json(['status' => 200, 'response' => $properties]);
    }


    public function assignedPropertydetailsUpdateofAgent(Request $request)
    {
        $rules = [
            'property_id'        => 'required',
        ];
        $messages = [
            'property_id.required' => 'Property id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        $agentId                        = Auth::user()->id;
        $property_id                    = $request->property_id;

        $verfication_property   = PendingPropertyForVerification::where('property_id', $property_id)
            ->where('agent_id', $agentId)
            ->first();

        $property               = OwnerProperty::select('owner_properties.*')
            ->where('owner_properties.id', $property_id);
        if (!$verfication_property) {
            $property           =   $property->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1)
                ->first();
        } else {
            $property           =   $property->join('pending_property_for_verifications', 'pending_property_for_verifications.property_id', 'owner_properties.id')
                ->where('pending_property_for_verifications.agent_id', $agentId)
                // ->where('pending_property_for_verifications.status',1)
                ->first();
        }

        if (!$property) {
            return response()->json(['status' => 400, 'response' => "Invalid property"]);
        }

        $property->property_to          = $request->property_to;
        $property->furnished            = $request->furnished;
        $property->category             = $request->category;
        if ($request->property_name) {
            $property->property_name        = $request->property_name;
        }
        if ($request->address1) {
            $property->street_address_1     = $request->address1;
        }
        if ($request->address2) {
            $property->street_address_2     = $request->address2;
        }
        if ($request->country) {
            $property->country              = $request->country;
        }
        if ($request->city) {
            $property->city                 = $request->city;
        }
        if ($request->state) {
            $property->state                = $request->state;
        }
        if ($request->zip_code) {
            $property->zip_code             = $request->zipcode;
        }
        if ($request->description) {
            $property->description          = $request->description;
        }
        if ($request->frequency) {
            $property->frequency            = $request->frequency;
        }
        if ($request->rent) {
            $property->rent                 = $request->rent;
        }
        if ($request->expected_amount) {
            $property->expected_amount      = $request->expected_amount;
        }
        if ($request->type_id) {
            $property->type_id              = $request->type_id;
        }
        if ($request->longitude) {
            $property->longitude            = $request->longitude;
        }
        if ($request->latitude) {
            $property->latitude             = $request->latitude;
        }
        // $property->occupied             = '0';
        $property->status               = '1';

        try {
            DB::beginTransaction();
            $property->save();
            $savedoc = array();
            $savefloor = array();
            $savevideo = array();
            $amenities = array();
            if ($request->file('images')) {
                // OwnerPropertyDocument::where('property_id',$property_id)->where('type','0')->delete();
                foreach ($request->file('images') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'), $imagename);
                    $savedoc[$key]['property_id']   = $property_id;
                    $savedoc[$key]['document']      = '/uploads/property/images/' . $imagename;
                    $savedoc[$key]['type']          = '0';
                    $savedoc[$key]['owner_id']      = $property->owner_id;
                    $savedoc[$key]['created_at']    = now();
                    $savedoc[$key]['updated_at']    = now();
                }
            }
            OwnerPropertyDocument::insert($savedoc);
            if ($request->file('floor_plans')) {
                // OwnerPropertyDocument::where('property_id',$property_id)->where('type','2')->delete();
                foreach ($request->file('floor_plans') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                    $savefloor[$key]['property_id'] = $property_id;
                    $savefloor[$key]['document']    = '/uploads/property/floor_plans/' . $imagename;
                    $savefloor[$key]['type']        = '2';
                    $savefloor[$key]['owner_id']    = $property->owner_id;
                    $savefloor[$key]['created_at']  = now();
                    $savefloor[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savefloor);
            }
            if ($request->file('videos')) {
                // OwnerPropertyDocument::where('property_id',$property_id)->where('type','1')->delete();
                foreach ($request->file('videos') as $key => $row) {
                    $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/videos'), $videoname);
                    $savevideo[$key]['property_id'] = $property_id;
                    $savevideo[$key]['document']    = '/uploads/property/videos/' . $videoname;
                    $savevideo[$key]['type']        = '1';
                    $savevideo[$key]['owner_id']    = $property->owner_id;
                    $savevideo[$key]['created_at']  = now();
                    $savevideo[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savevideo);
            }
            if ($request->amenities) {
                OwnerPropertyAmenity::where('property_id', $property_id)->delete();
                foreach ($request->amenities as $key => $row) {
                    $category_id = Amenity::where('id', $row)->pluck('amenity_category_id');
                    foreach ($amenities as $row6) {
                        if ($row6['property_id'] == $property->id && $row6['amenity_id'] == $row && $row6['amenities_category_id'] == $category_id[0] && $row6['amenities_category_id'] == Auth::user()->id) {
                            DB::rollback();
                            return ['status' => 400, 'response' => 'Something went wrong'];
                        }
                    }
                    $amenities[$key]['property_id']             = $property_id;
                    $amenities[$key]['amenity_id']              = $row;
                    $amenities[$key]['amenities_category_id']   = $category_id[0];
                    $amenities[$key]['owner_id']                = $property->owner_id;
                    $amenities[$key]['created_at']              = now();
                    $amenities[$key]['updated_at']              = now();
                }
                OwnerPropertyAmenity::insert($amenities);
            }
            if ($request->detailskey) {
                $max            = sizeof($request->detailskey);
                $detailsvalue   = $request->detailsvalue;
                $detailkeyvalue = $request->detailskey;
                OwnerPropertyDetail::where('property_id', $property_id)->delete();
                for ($i = 0; $i < $max; $i++) {
                    if ($request->detailskey[$i] != "") {
                        //dd($detailkeyvalue[$i]);
                        $details               = new OwnerPropertyDetail();
                        $details['property_id'] = $property_id;
                        $details['detail_id']  = $detailkeyvalue[$i];
                        $details['value']      = $detailsvalue[$i];
                        $details['owner_id']   = $property->owner_id;
                        $details['created_at'] = now();
                        $details['updated_at'] = now();
                        $details->save();
                    }
                }
            }

            $agent_tour         = PendingPropertyForVerification::select('pending_property_for_verifications.id')
                ->join('agent_tracking_owner_tours', 'agent_tracking_owner_tours.tour_id', 'pending_property_for_verifications.id')
                ->where('pending_property_for_verifications.agent_id', $agentId)
                ->where('pending_property_for_verifications.property_id', $property_id)
                ->where('agent_tracking_owner_tours.status', 1)
                ->first();
            if ($agent_tour) {
                AgentTrackingOwnerTour::where('tour_id', $agent_tour->id)
                    ->where('agent_id', $agentId)
                    ->update(['status' => 2]);

                //code to remove the perticular owner tour from home appointment
                AgentAppointment::where('agent_id', $agentId)
                    ->where('request_id', $agent_tour->id)
                    ->where('property_id', $property_id)
                    ->where('type', 2)
                    ->update(['status' => 0]);
            }


            DB::commit();
            return ['status' => 200, 'response' => 'Successfully Updated property'];
        } catch (\Exception $e) {
            DB::rollback();
            return ['status' => 400, 'response' => $e->getMessage()];
        }
    }
    public function changePassword(Request $request)
    {
        $userid = Auth::user()->id;
        $user   = Agent::where('id', $userid)->first();
        if ($user) {
            if (Hash::check($request->current_password, $user->password)) {
                if ($request->new_password == $request->confirm_new_password) {
                    $user->password     = Hash::make($request->new_password);
                    $user->save();
                    return response()->json(['status' => 200, 'response' => "Password Changed"]);
                } else {
                    return response()->json(['status' => 400, 'response' => "New password and Confirm Password does not match"]);
                }
            } else {
                return response()->json(['status' => 400, 'response' => 'Current Password does not matches']);
            }
        }
    }


    public function setToNull($key, $value)
    {
        array_walk_recursive($value, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
        $this->data[$key] = $value;
        return $this->data;
    }

    public function allNull2Empty($array)
    {
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        $response_array = [];
        foreach ($newarray as $key => $value) {
            array_walk_recursive($value, function (&$item, $key) {
                $item = null === $item ? '' : $item;
            });
            $response_array[] = $value;
        }
        return $response_array;
    }

    public function eachNull2Empty($array)
    {
        $listarray = json_encode($array);
        $newarray  = json_decode($listarray);
        foreach ($newarray as $key => $value) {
            if (is_array($value)) {
                $this->eachNull2Empty($value);
            } else {
                if (is_null($value)) {
                    $newarray->$key = '';
                } else {
                    $newarray->$key = $value;
                }
            }
        }
        return $newarray;
    }


    public function agentHome(Request $request)
    {
        try {

            $agentId                       = Auth::user()->id;
            $base_url                      = url('/');
            $agent_assigned_property_count = OwnerProperty::join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1)
                ->distinct('agent_properties.property_id')
                ->count();
            $agent_city                    = City::select('name')->where('id', Auth::user()->city)->first();
            $agent_city_name               = $agent_city->name;

            $user_tour_bookings            = AgentAppointment::select('id', 'property_id', 'date', 'time', DB::raw('ifnull(user_id,0) AS user_id'), DB::raw('ifnull(owner_id,0) AS owner_id'), 'request_id as tour_id', 'type')
                ->with(
                    array(
                        'property_details' =>
                        function ($query) use ($base_url) {
                            $query->select(
                                'id',
                                DB::raw('ifnull(property_name,"") as property_name'),
                                DB::raw('ifnull(latitude,"") as latitude'),
                                DB::raw('ifnull(longitude,"") as longitude')
                            );
                        }, 'user_details' => function ($query) use ($base_url) {
                            $query->select(
                                'id',
                                DB::raw('ifnull(name,"") as name'),
                                DB::raw('CONCAT("' . $base_url . '",profile_pic) AS profile_pic'),
                                'phone'
                            );
                        }, 'owner_details' => function ($query) use ($base_url) {
                            $query->select(
                                'id',
                                DB::raw('ifnull(name,"") as name'),
                                DB::raw('CONCAT("' . $base_url . '",profile_image) AS profile_image'),
                                'phone'
                            );
                        }
                    )
                )
                ->orderBy('date', 'ASC')
                ->orderBy('time', 'ASC')
                ->where('agent_id', $agentId)
                ->where('status', 1)
                ->where('date', date('Y-m-d'));
            $todays_appointment            = $user_tour_bookings->count();
            $user_tour_bookings            = $user_tour_bookings->get();
            $user_tour_bookings            = $this->allNull2Empty($user_tour_bookings);
            if ($user_tour_bookings) {
                foreach ($user_tour_bookings as $appointment) {
                    if (!$appointment->user_details) {
                        $appointment->user_details  = (object)[];
                    } else {
                        if (!$appointment->user_details->profile_pic) {
                            $appointment->user_details->profile_pic = "";
                        }
                    }
                    if ($appointment->owner_details == null) {
                        $appointment->owner_details  = (object)[];
                    } else {
                        if (!$appointment->owner_details->profile_image) {
                            $appointment->owner_details->profile_image = "";
                        }
                    }
                }
            }

            $rent_over_due                 = UserProperty::select('user_properties.id', 'user_id', 'due_date', 'property_id', 'rent')
                ->join('agent_tracking_user_tour', 'agent_tracking_user_tour.user_property', 'user_properties.id')
                ->with(
                    array(
                        'user_rel' => function ($query) use ($base_url) {
                            $query->select('id', DB::raw('ifnull(name,"") as name'), DB::raw('CONCAT("' . $base_url . '",profile_pic) AS profile_pic'), 'phone');
                        }, 'prop_rel:id,property_reg_no'
                    )
                )
                ->where('agent_tracking_user_tour.agent_id', $agentId)
                ->where('user_properties.due_date', '<', date('Y-m-d'))
                ->where('user_properties.status', 0)
                ->get();

            if ($rent_over_due) {
                foreach ($rent_over_due as $rent) {
                    if ($rent->user_rel == null) {
                        $rent->user_rel  = (object)[];
                    } else {
                        if (!$rent->user_rel->profile_pic) {
                            $rent->user_rel->profile_pic = "";
                        }
                    }
                    if ($rent->prop_rel == null) {
                        $rent->prop_rel  = (object)[];
                    }
                }
            }

            $data = [
                'total_assigned_count'  => $agent_assigned_property_count,
                'zone'                  => $agent_city_name,
                'appointment_count'     => $todays_appointment,
                'appoinments'           => $user_tour_bookings,
                'rent_over_due'         => $rent_over_due,
            ];
            return response()->json(['status' => 200, 'response' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function apointmentUserTourDetails(Request $request)
    {
        $rules = [
            'tour_id'        => 'required',
        ];
        $messages = [
            'tour_id.required' => 'Start tour id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $agent_id       = Auth::user()->id;
            $tour_id        = $request->tour_id;
            $base_url       = url('/');
            $user_tour      = BookUserTour::where('id', $tour_id)->first();
            if ($user_tour) {
                $user_id            = $user_tour->user_id;
                $property_id        = $user_tour->property_id;

                $agent_tour_exist   = AgentTrackingUserTour::where('tour_id', $tour_id)
                    ->where('agent_id', '!=', $agent_id)
                    ->first();
                if ($agent_tour_exist) {
                    return response()->json(['status' => 400, 'response' => 'Already Being Processed']);
                }

                // check user agent tour  exist
                $agent_tour         = AgentTrackingUserTour::select('id', 'tour_id', 'agent_id', 'status')
                    ->where('tour_id', $tour_id)
                    ->where('agent_id', $agent_id)
                    ->first();

                if (!$agent_tour) {

                    // insert  start tour
                    $agent_tour                   = new AgentTrackingUserTour();
                    $agent_tour->tour_id          = $user_tour->id;
                    $agent_tour->agent_id         = $agent_id;
                    $agent_tour->status           = 1;
                    $agent_tour->save();

                    $agent_tour                   = AgentTrackingUserTour::select('id', 'tour_id', 'agent_id', 'status')
                        ->where('tour_id', $tour_id)
                        ->where('agent_id', $agent_id)
                        ->first();
                }

                if ($agent_tour->status > 1) {
                    $agent_commission           = AgentEarnings::select('id', 'amount')
                        ->where('tour_id', $agent_tour->id)
                        ->where('agent_id', $agent_id)
                        ->first();
                    if (!$agent_commission) {
                        $agent_commission           = (object)[];
                    }
                } else {
                    $agent_commission           = (object)[];
                }
                $agent_touring              = $this->setToNull('agent_tour', $agent_tour);
                $property_details           = OwnerProperty::select(
                    'id',
                    'property_reg_no',
                    'property_name',
                    'property_to',
                    'rent',
                    'selling_price',
                    'mrp',
                    'security_deposit',
                    'token_amount'
                )
                    ->with(
                        array(
                            'property_priority_image' =>
                            function ($query) use ($base_url) {
                                $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                            }
                        )
                    )
                    ->where('id', $property_id)
                    ->first();
                if ($property_details) {
                    $property_details   = $this->setToNull('property_details', $property_details);
                } else {
                    $property_details['property_details']   = (object)[];
                }
                $user_details               = User::select(
                    'id',
                    'name',
                    'email',
                    'phone',
                    DB::raw('CONCAT("' . $base_url . '",profile_pic) AS profile_pic')
                )
                    ->where('id', $user_id)
                    ->first();
                if ($user_details) {
                    $user_details   = $this->setToNull('user_details', $user_details);
                } else {
                    $user_details['user_details']   = (object)[];
                }
                $data = [
                    'agent_tour'       => $agent_touring['agent_tour'],
                    'property_details' => $property_details['property_details'],
                    'user_details'     => $user_details['user_details'],
                    'agent_commission' => $agent_commission,
                ];
            } else {
                return response()->json(['status' => 400, 'response' => "This tour doesn't exist"]);
            }
            DB::commit();
            return response()->json(['status' => 200, 'response' => $data]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function proceedToBookDetailsForAgent(Request $request)
    {
        $rules = [
            'tour_id'        => 'required',
        ];
        $messages = [
            'tour_id.required' => 'Tour id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $agent_id       = Auth::user()->id;
            $tour_id        = $request->tour_id;
            $base_url       = url('/');
            $user_tour      = BookUserTour::where('id', $tour_id)->first();
            if ($user_tour) {
                $user_id            = $user_tour->user_id;
                $property_id        = $user_tour->property_id;

                $property_details   = OwnerProperty::select(
                    'id',
                    'property_reg_no',
                    'property_to',
                    'rent',
                    'selling_price',
                    'security_deposit',
                    'token_amount'
                )
                    ->where('id', $property_id)
                    ->first();
                if ($property_details) {
                    $token_amount       = $property_details->token_amount;
                    if ($property_details->property_to == 0) {
                        $pending_amount     = $property_details->security_deposit  + ($property_details->rent - $token_amount);
                    } else {
                        $pending_amount     = $property_details->selling_price - $token_amount;
                    }
                    $has_user_property  = UserProperty::where('user_id', $user_id)
                        ->where('property_id', $property_id)
                        ->where('check_in', '>', date('Y-m-d'))
                        ->where('cancel_status', false)
                        ->first();
                    if ($has_user_property) {
                        $user_property_id   = $has_user_property->id;
                    } else {
                        $user_property_id   = 0;
                    }

                    $data = [
                        'token_amount'      =>  strval($token_amount ? $token_amount : ""),
                        'pending_amount'    =>  strval($pending_amount ? $pending_amount : ""),
                        'user_id'           =>  $user_id,
                        'property_id'       =>  $property_id,
                        'tour_id'           =>  $tour_id,
                        'user_property_id'  =>  $user_property_id,
                    ];
                    return response()->json(['status' => 200, 'response' => $data]);
                } else {
                    return response()->json(['status' => 400, 'response' => "This property doesn't exist"]);
                }
            } else {
                return response()->json(['status' => 400, 'response' => "This tour doesn't exist"]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function removePropertyDocument(Request $request)
    {
        $rules = [
            'document_id'          => 'required',
        ];
        $messages = [
            'document_id.required' => 'Document id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $property_document_id       = $request->document_id;
            $update_doc                 = OwnerPropertyDocument::where('id', $property_document_id)->first();
            if ($update_doc) {
                if ($update_doc->document) {
                    if (file_exists(public_path($update_doc->document))) {
                        unlink(public_path($update_doc->document));
                    }
                }
                $update_doc->delete();
                return response()->json(['status' => 200, 'response' => "Removed Property document successfully"]);
            } else {
                return response()->json(['status' => 400, 'response' => "This document doesn't exist"]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function agentProceedToBook(Request $request)
    {
        $rules = [
            'user_id'           => 'required',
            'property_id'       => 'required',
            'check_in'          => 'required|date|after:yesterday',
            'check_out'         => 'nullable',
            'payment_status'    => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
            'payment_count'     => 'integer|required',
            'tour_id'           => 'required',

        ];
        $messages = [
            'user_id.required'          => 'User id is required',
            'property_id.required'      => 'Property id is required',
            'check_in.required'         => 'Check-in date is required',
            'payment_status.required'   => 'Payment method is required',
            'document.required'         => 'Document is required',
            'payment_count.required'    => 'No. of payment is required',
            'tour_id'                   => 'Tour is required',

        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $agent_id         = Auth::user()->id;
            // Payment count is not used in this function to be clarified

            DB::beginTransaction();
            $user_id          = $request->user_id;
            $tour_id          = $request->tour_id;
            $property_id      = $request->property_id;
            $check_in_date    = $request->check_in;
            $check_out_date   = $request->check_out;
            $property_details = OwnerProperty::where('id', $property_id)->where('status', 1)->where('contract_owner', '1')->where('occupied', 0)->first();

            // other package selection
            $package_id       = $request->package_id;

            if ($package_id) {
                $property_package   = OfferPackage::where('id', $package_id)
                    ->where('property_id', $property_id)
                    ->where('start_date', '<=', date('Y-m-d'))
                    ->where('end_date', '>=', date('Y-m-d'))
                    ->where('status', 1)
                    ->first();
                if (!$property_package) {
                    return response()->json(['status' => 400, 'response' => 'Invalid package']);
                }
            }

            //code to remove the perticular property from home appointment
            $tour_details           = BookUserTour::where('id', $tour_id)->first();
            if ($tour_details) {
                $user_id            = $tour_details->user_id;
                $property_id        = $tour_details->property_id;
                AgentAppointment::where('user_id', $user_id)
                    ->where('agent_id', $agent_id)
                    ->where('date', date('Y-m-d'))
                    ->where('property_id', $property_id)
                    ->update(['status' => 0]);
            }

            if (!empty($property_details)) {
                $property_for = $property_details->property_to;
                if ($property_details->status == 1) {
                    if ($property_details->occupied == 0) {
                        $booking_id                  = $this->generateBookingIdPermanant();
                        $book_property = UserPropertyBookings::where('property_id', $property_id)
                            ->where('user_id', $user_id)
                            ->where('check_in', '>=', date('Y-m-d'))
                            ->where('cancel_status', false)
                            ->first();
                        if ($property_for == 0) { #property to rent
                            $rules = [
                                'check_out' => 'required',
                            ];
                            $messages = [
                                'check_out.required' => 'Check-out date is required',
                            ];
                            $validation = Validator::make($request->all(), $rules, $messages);
                            if ($validation->fails()) {
                                return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
                            }
                            if (empty($book_property)) {
                                $book_property                = new UserPropertyBookings();
                                $book_property->property_id   = $property_id;
                                $book_property->booking_id    = $booking_id;
                                $book_property->user_id       = $user_id;
                                $book_property->assigned_agent = $agent_id;
                                if ($package_id) {
                                    $book_property->package_id = $package_id;
                                }
                                $book_property->check_in      = date('Y-m-d', strtotime($check_in_date));
                                $book_property->check_out     = date('Y-m-d', strtotime($check_out_date));
                                $book_property->save();
                            } else {
                                $already_check_in = date('d-m-Y', strtotime($book_property->check_in));
                                return response()->json(['status' => 200, 'response' => "Property already booked on " . $already_check_in]);
                            }
                        } elseif ($property_for == 1) { #property to buy
                            if (empty($book_property)) {
                                $book_property                = new UserPropertyBookings();
                                $book_property->property_id   = $property_id;
                                $book_property->booking_id    = $booking_id;
                                $book_property->user_id       = $user_id;
                                $book_property->assigned_agent = $agent_id;
                                $book_property->check_in      = date('Y-m-d', strtotime($check_in_date));
                                $book_property->save();
                            } else {
                                $already_check_in = date('d-m-Y', strtotime($book_property->check_in));
                                return response()->json(['status' => 200, 'response' => "Property already booked on " . $already_check_in]);
                            }
                        }
                        $has_booking = UserPropertyBookings::where('id', $book_property->id)->first();
                        if (!empty($has_booking)) {
                            $agent_tour = AgentTrackingUserTour::select('id', 'tour_id', 'agent_id', 'status')
                                ->where('tour_id', $tour_id)
                                ->where('agent_id', $agent_id)
                                ->first();
                            if ($agent_tour) {
                                $agent_tour->status     = 2; //status updating to proceed-to-booking
                                $agent_tour->booking_id = $book_property->id;
                                $agent_tour->save();
                            }

                            $already_uploaded = UserPropertyBookingDocuments::where('booking_id', $has_booking->id)->first();
                            if (empty($already_uploaded)) {
                                if ($request->file('document')) {
                                    $file = $request->file('document');
                                    $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                                    $file->move(public_path('/uploads/users/bill-document'), $filename);

                                    $book_doc                   = new UserPropertyBookingDocuments();
                                    $book_doc->user_id          = $user_id;
                                    $book_doc->booking_id       = $has_booking->id;
                                    $book_doc->document         = '/uploads/users/bill-document/' . $filename;
                                    $book_doc->document_status  = $request->payment_status;
                                    $book_doc->save();


                                    if ($request->payment_status == 2) {
                                        if ($request->pay_amount) {
                                            $pay_amount         = $request->pay_amount;
                                        } else {
                                            $pay_amount         = $property_details->token_amount;
                                        }
                                        $agent_cash                 = new AgentCashInHand();
                                        $agent_cash->agent_id       = $agent_id;
                                        $agent_cash->user_id        = $user_id;
                                        $agent_cash->property_id    = $property_id;
                                        $agent_cash->payment_id     = $book_doc->id;
                                        $agent_cash->payment_amount = $pay_amount;
                                        $agent_cash->payment_type   = 1; // agent paid for property booking
                                        $agent_cash->status         = 1; // cash in hand status
                                        $agent_cash->save();
                                    }

                                    DB::commit();
                                    return response()->json(['status' => 200, 'response' => 'Property Booking is under progress']);
                                }
                            } else {
                                return response()->json(['status' => 200, 'response' => 'File already updated']);
                            }
                        } else {
                            return response()->json(['status' => 401, 'response' => 'Invalid Booking']);
                        }
                    } else {
                        return response()->json(['status' => 200, 'response' => "Property space already occupied"]);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => "Property is not active"]);
                }
            } else {
                return response()->json(['status' => 401, 'response' => "Invalid Property"]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function generateBookingIdPermanant()
    {
        $otp = mt_rand(100000, 999999);
        $new_booking_id = 'UBID' . $otp;
        try {
            $user_book_data = UserPropertyBookings::where('booking_id', $new_booking_id)->first();
            if (!empty($user_book_data)) {
                $this->generateBookingIdPermanant();
            } else {
                return $new_booking_id;
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function uploadUserPropertyDocumentsByAgent(Request $request)
    {
        $rules = [
            'user_property_id'  => 'required',
            'document'          => 'required',
            'document.*'        => 'mimes:jpg,jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'user_property_id.required' => 'User property is required',
            'document.required'         => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $has_booking = UserProperty::where('id', $request->user_property_id)
                ->where('status', 2)
                ->where('cancel_status', false)
                ->first();
            if (!empty($has_booking)) {
                $already_uploaded = UserPropertyDocument::where('user_property_id', $has_booking->id)
                    ->where('document_type', 2)
                    ->first();
                if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        foreach ($request->file('document') as $key => $row) {
                            $filename = time() . rand() . '.' . $row->getClientOriginalExtension();
                            $row->move(public_path('/uploads/users/property-documents'), $filename);

                            $user_property_doc[$key]['user_id']          = $has_booking->user_id;
                            $user_property_doc[$key]['user_property_id'] = $has_booking->id;
                            $user_property_doc[$key]['document']         = '/uploads/users/property-documents/' . $filename;
                            $user_property_doc[$key]['document_type']    = 2;
                            $user_property_doc[$key]['created_at']       = now();
                            $user_property_doc[$key]['updated_at']       = now();
                        }
                    }
                    if (UserPropertyDocument::insert($user_property_doc)) {
                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    } else {
                        return response()->json(['status' => 400, 'response' => 'Failed to upload documents']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                return response()->json(['status' => 401, 'response' => 'Invalid User Property']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function requestForContractByAgent(Request $request)
    {
        $rules = [
            'user_property_id'  => 'required',
            'tour_id'           => 'required'
        ];
        $messages = [
            'user_property_id.required' => 'User property is required',
            'tour_id.required'          => 'Tour is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $user_property_id   = $request->user_property_id;
            $tour_id            = $request->tour_id;
            $agent_id           = Auth::user()->id;

            $user_exist         = UserProperty::where('id', $user_property_id)
                ->first();

            if (!$user_exist) {
                return response()->json(['status' => 200, 'response' => 'Invalid Booking']);
            }

            $has_requested      = AgentRequestingContract::where('user_property_id', $user_property_id)
                ->where('status', 1)
                ->first();
            if (!empty($has_requested)) {
                return response()->json(['status' => 200, 'response' => 'You have already requested']);
            } else {
                $request_contract                     = new AgentRequestingContract();
                $request_contract->user_property_id   = $user_property_id;
                $request_contract->agent_id           = $agent_id;
                $request_contract->status             = "1";
                $request_contract->upload_status      = "0";
                $request_contract->requested_date     = date('Y-m-d H:i:s');
                $request_contract->save();

                $agent_tour = AgentTrackingUserTour::select('id', 'tour_id', 'agent_id', 'status')
                    ->where('tour_id', $tour_id)
                    ->where('agent_id', $agent_id)
                    ->first();
                if ($agent_tour) {
                    $agent_tour->status     = 4; //status updating to request-for-contract
                    $agent_tour->save();
                }

                if ($user_exist) {
                    $pending_amount     = $user_exist->rent;
                    if ($pending_amount > 0) {
                        if ($agent_tour) {
                            $agent_tour->status     = 4; //status updating to full amount paid
                            $agent_tour->save();
                        }
                    }
                }
                return response()->json(['status' => 200, 'response' => 'Your request sent successfully']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function updatingUserNotInterestedStatusByAgent(Request $request)
    {
        $rules = [
            'tour_id'           => 'required'
        ];
        $messages = [
            'tour_id.required'          => 'Tour is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $tour_id            = $request->tour_id;
            $agent_id           = Auth::user()->id;
            $agent_tour = AgentTrackingUserTour::select('id', 'tour_id', 'agent_id', 'status')
                ->where('tour_id', $tour_id)
                ->where('agent_id', $agent_id)
                ->first();
            if ($agent_tour) {
                $agent_tour->status     = 6; //status updating to Not Intrested
                $agent_tour->save();

                $tour_details           = BookUserTour::where('id', $tour_id)->first();
                if ($tour_details) {
                    $user_id            = $tour_details->user_id;
                    $property_id        = $tour_details->property_id;
                    AgentAppointment::where('user_id', $user_id)
                        ->where('agent_id', $agent_id)
                        ->where('date', date('Y-m-d'))
                        ->where('property_id', $property_id)
                        ->update(['status' => 0]);
                }

                return response()->json(['status' => 200, 'response' => 'Status Updated']);
            } else {
                return response()->json(['status' => 200, 'response' => 'Invalid Tour']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function myUserProperties(Request $request)
    {
        try {
            $page               = $request->page;
            $completed          = $request->completed;
            $agent_id           = Auth::user()->id;
            $base_url           = url('/');
            $agent_tour         = AgentTrackingUserTour::select('tour_id')
                ->where('agent_id', $agent_id)
                ->where('status', '!=', 6);
            if ($completed) {
                $agent_tours    = $agent_tour->where('status', 5)
                    ->get();
            } else {
                $agent_tours    = $agent_tour->where('status', '!=', 5)
                    ->get();
            }
            $tours              = json_decode($agent_tours);
            if ($tours) {
                $tour_ids       = array_column($tours, 'tour_id');
            } else {
                $tour_ids       = [0];
                // return response()->json(['status'=>200,'tour_data'=>(object)[]]);
            }

            $tour_details       = BookUserTour::select('id', 'property_id', 'user_id')
                ->with(
                    array(
                        'property_priority_image' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                        }, 'user_rel' => function ($query) use ($base_url) {
                            $query->select('id', 'name', 'phone', 'email', DB::raw('CONCAT("' . $base_url . '",profile_pic) AS profile_pic'));
                        }, 'property_details' => function ($query) {
                            $query->select('id', DB::raw('ifnull(property_name,"") as property_name'), 'property_reg_no');
                        }
                    )
                )
                ->orderBy('id', 'DESC')
                ->whereIn('id', $tour_ids);

            $tour_details       = $this->customPagination($tour_details, $page);

            if ($tour_details['data']) {
                foreach ($tour_details['data'] as $each_tour) {
                    if ($each_tour->property_priority_image == "") {
                        $each_tour->property_priority_image = (object)[];
                    }
                    if ($each_tour->user_rel == "") {
                        $each_tour->user_rel   = (object)[];
                    } else {
                        if (!$each_tour->user_rel->profile_pic) {
                            $each_tour->user_rel->profile_pic = "";
                        }
                    }
                    if ($each_tour->property_details == "") {
                        $each_tour->property_details   = (object)[];
                    }
                }
            }

            $tour_data  = [
                'total_page_count' => $tour_details['total_page_count'],
                'current_page'     => $tour_details['current_page'],
                'user_properties'         => $tour_details['data'],
            ];

            return response()->json(['status' => 200, 'tour_data' => $tour_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function customPagination($data_array, $page)
    {
        $total_event_count  = $data_array->count();

        $items_per_page     = 10;
        $total_page_count   = ceil($total_event_count / $items_per_page);
        if ($page) {
            $pages_array        = range(1, $total_page_count);
            if (in_array($page, $pages_array)) {
                $current_page       = strval($page);
                $offset_page        = $page - 1;
                $offset_item_count  = $offset_page * $items_per_page;

                $data_array         = $data_array->offset($offset_item_count)->take($items_per_page)->get();
            } else {
                $current_page       = strval($page);
                $data_array         = [];
            }
        } else {
            if ($page == 0 && $page != null) {
                $current_page       = strval(0);
                $data_array         = [];
            } else {
                $current_page       = strval(1);
                // $data_array         = $data_array->take($items_per_page)->get();
                $data_array         = $data_array->get();
            }
        }

        if (count($data_array) > 0) {
            $data_array  = $this->allNull2Empty($data_array);
        }

        $return_array = [
            'current_page'      => $current_page,
            'total_page_count'  => $total_page_count,
            'data'              => $data_array
        ];

        return $return_array;
    }


    public function agentOngoingAndCompletedPropertyDetails(Request $request)
    {
        $rules = [
            'tour_id'               => 'required',
        ];
        $messages = [
            'tour_id.required'      => 'Tour is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $tour_id            = $request->tour_id;
            $agent_id           = Auth::user()->id;
            $base_url           = url('/');
            $agent_tour         = AgentTrackingUserTour::select('tour_id', 'status', DB::raw('ifnull(user_property,"") AS user_property_id'))
                ->where('agent_id', $agent_id)
                ->where('tour_id', $tour_id)
                ->where('status', '!=', 6)
                ->first();

            if (!$agent_tour) {
                $data = (object)[];
                return response()->json(['status' => 200, 'date' => $data]);
            }
            if ($agent_tour->user_property_id) {
                $full_payment_check = UserPropertyFullAmountDocument::where('user_property_id', $agent_tour->user_property_id)->first();
                if ($full_payment_check) {
                    $agent_tour->payment_check = 1;
                } else {
                    $agent_tour->payment_check = 0;
                }
            } else {
                $agent_tour->payment_check = 0;
            }

            $tour_details       = BookUserTour::select('id', 'property_id', 'user_id')
                ->with(
                    array(
                        'property_priority_image' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                        }, 'user_rel' => function ($query) use ($base_url) {
                            $query->select('id', 'name', 'phone', 'email', DB::raw('CONCAT("' . $base_url . '",profile_pic) AS profile_pic'));
                        }
                    )
                )
                ->where('id', $tour_id)
                ->first();
            if ($tour_details) {
                $property_id                    = $tour_details->property_id;
                $user_id                        = $tour_details->user_id;
                $tour_details['agent_tour']     = $agent_tour;

                $tour_details['user_booking_data'] = UserPropertyBookings::select('id', 'check_in', DB::raw('ifnull(check_out,"") as check_out'), 'is_verified', 'approve_status')
                    ->where('user_id', $user_id)
                    ->where('property_id', $property_id)
                    ->where('cancel_status', false)
                    ->first();
                if (!$tour_details['user_booking_data']) {
                    $tour_details['user_booking_data'] = (object)[];
                }
                $tour_details['property_data']  = OwnerProperty::select(
                    'id',
                    DB::raw('ifnull(property_name,"") as property_name'),
                    'property_reg_no',
                    'property_to',
                    'rating',
                    DB::raw('ifnull(selling_price,0.00) as selling_price'),
                    DB::raw('ifnull(mrp,0.00) as mrp'),
                    DB::raw('ifnull(rent,0.00) as rent'),
                    DB::raw('ifnull(furnished,"") as furnished'),
                    'city',
                    DB::raw('ifnull(contract_start_date,"") AS contract_start_date'),
                    DB::raw('ifnull(contract_end_date,"") AS contract_end_date')
                )
                    ->with('property_details:detail_id,value,name', 'city_rel:id,name')
                    ->where('id', $property_id)
                    ->first();
                if (!$tour_details['property_data']) {
                    $tour_details['property_data'] = (object)[];
                }
                if ($tour_details->user_rel) {
                    if (!$tour_details->user_rel->profile_pic) {
                        $tour_details->user_rel->profile_pic = "";
                    }
                }

                if ($tour_details['property_data']) {
                    $tour_details['property_data']->rating_count = UserPropertyRating::where('property_id', $property_id)->count();
                }
            }
            $tour       = $this->setToNull('details', $tour_details);

            $details          = Detail::select('id', 'name')->get();

            $dummy_object     = (object)[];

            if ($tour['details']->property_data != $dummy_object) {
                if ($tour['details']->property_data->property_details) {
                    $prop = $tour['details']->property_data;
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($prop->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $prop->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $prop->$detail_data = 0;
                        }
                    }
                }
            }


            if ($agent_tour->status > 1) {
                $agent_commission           = AgentEarnings::select('id', 'amount')
                    ->where('tour_id', $agent_tour->id)
                    ->where('agent_id', $agent_id)
                    ->first();
                if ($agent_commission) {
                    $tour['details']->agent_commission           = $agent_commission;
                } else {
                    $tour['details']->agent_commission           = (object)[];
                }
            } else {
                $tour['details']->agent_commission           = (object)[];
            }


            return response()->json(['status' => 200, 'data' => $tour['details']]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function addTask(Request $request)
    {
        $rules = [
            'title'               => 'required',
            'task_date'           => 'required|date|after:yesterday',
            'task_time'           => 'required|date_format:H:i',
        ];
        $messages = [
            'title.required'      => 'Title is required',
            'task_date.required'    => 'Task date is required',
            'task_time.required'    => 'Task time is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $agent_id                = Auth::user()->id;

            $agent_task              = new AgentTask();
            $agent_task->agent_id    = $agent_id;
            $agent_task->title       = $request->title;
            $agent_task->task_date   = date('Y-m-d', strtotime($request->task_date));
            $agent_task->task_time   = $request->task_time;
            $agent_task->completed   = 0;
            $agent_task->save();

            return response()->json(['status' => 200, 'response' => 'Task added successfully']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentCalendarTaskCount(Request $request)
    {
        $rules = [
            'date'               => 'required|date_format:d-m-Y',
        ];
        $messages = [
            'date.required'      => 'Date is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $date               = date('Y-m', strtotime($request->date));
            $agent_id           = Auth::user()->id;
            $agent_task_count   = AgentTask::select('task_date', DB::raw('count(*) as task_count'))
                ->where('agent_id', $agent_id)
                ->where('task_date', 'like', $date . '-%')
                ->orderBy('task_date', 'ASC')
                ->groupBy('task_date')
                ->get();

            return response()->json(['status' => 200, 'agent_task' => $agent_task_count]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentCalendarTaskList(Request $request)
    {
        $rules = [
            'date'               => 'required|date_format:d-m-Y',
        ];
        $messages = [
            'date.required'      => 'Date is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $page               = $request->page;
            $date               = date('Y-m', strtotime($request->date));
            $agent_id           = Auth::user()->id;
            $agent_task         = AgentTask::select('id', 'title', 'task_time', 'completed', 'task_date')
                ->where('agent_id', $agent_id)
                ->where('task_date', 'like', $date . '-%')
                ->orderBy('task_date', 'ASC')
                ->orderBy('task_time', 'ASC');
            $agent_task         = $this->customPagination($agent_task, $page);
            $task_data  = [
                'total_page_count' => $agent_task['total_page_count'],
                'current_page'     => $agent_task['current_page'],
                'task'             => $agent_task['data'],
            ];

            return response()->json(['status' => 200, 'task_data' => $task_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function agentTaskList(Request $request)
    {
        try {
            $page               = $request->page;
            $completed          = $request->completed;
            $agent_id           = Auth::user()->id;
            $agent_task         = AgentTask::select('id', 'title', 'task_time', 'completed', 'task_date')
                ->where('agent_id', $agent_id)
                ->orderBy('task_date', 'DESC')
                ->orderBy('task_time', 'DESC');
            if ($completed) {
                $agent_task     = $agent_task->where('completed', 1);
            } else {
                $agent_task     = $agent_task->where('completed', 0);
            }
            $agent_task         = $this->customPagination($agent_task, $page);
            $task_data  = [
                'total_page_count' => $agent_task['total_page_count'],
                'current_page'     => $agent_task['current_page'],
                'task'             => $agent_task['data'],
            ];

            return response()->json(['status' => 200, 'task_data' => $task_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function updateTaskCompleteStatus(Request $request)
    {
        $rules = [
            'task_id'            => 'required',
        ];
        $messages = [
            'task_id.required'   => 'Task is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $agent_id                = Auth::user()->id;
            $task_id                 = $request->task_id;
            $complete                = $request->completed;

            $agent_task              = AgentTask::where('id', $task_id)
                ->where('agent_id', $agent_id)
                ->first();
            if ($agent_task) {
                if ($complete) {
                    if (AgentTask::where('id', $task_id)->update(['completed' => 1])) {
                        return response()->json(['status' => 200, 'response' => 'Task updated successfully']);
                    }
                } else {
                    if (AgentTask::where('id', $task_id)->update(['completed' => 0])) {
                        return response()->json(['status' => 200, 'response' => 'Task updated successfully']);
                    }
                }
            } else {
                return response()->json(['status' => 400, 'response' => 'Invalid Task']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function myRequest(Request $request)
    {
        try {
            $base_url                = url('/');
            $agent_id                = Auth::user()->id;
            // owner property verification request
            $check_property             = PendingPropertyAgent::select('property_id')
                ->where('status', 1)
                ->get();
            $properties                 = json_decode($check_property);
            if ($properties) {
                $property_ids       = array_column($properties, 'property_id');
            } else {
                $property_ids       = [];
            }
            $requested_property         = PendingPropertyAgent::select('property_id')
                ->whereNotIn('property_id', $property_ids)
                ->where('agent_id', $agent_id)
                ->where('status', 0)
                ->get();
            $requested_property         = json_decode($requested_property);
            if ($requested_property) {
                $requested_property_ids       = array_column($requested_property, 'property_id');
            } else {
                $requested_property_ids       = [];
            }

            $property_verification    = PendingPropertyForVerification::select(
                'pending_property_for_verifications.id',
                'pending_property_for_verifications.verification_date as date',
                'pending_property_for_verifications.time',
                'pending_property_for_verifications.property_id',
                'owner_properties.owner_id'
            )
                ->with(
                    array(
                        'user_property_related' => function ($query) {
                            $query->select(
                                'id',
                                DB::raw('ifnull(property_name,"") as property_name'),
                                DB::raw('ifnull(latitude,"") as latitude'),
                                DB::raw('ifnull(longitude,"") as longitude'),
                                'property_reg_no',
                                'property_to'
                            );
                        }, 'property_priority_image' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                        }, 'owner_rel' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",profile_image) AS profile_image'), 'id', 'name', 'phone', 'email');
                        }
                    )
                )
                ->join('owner_properties', 'owner_properties.id', 'pending_property_for_verifications.property_id')
                ->where('pending_property_for_verifications.agent_id', null)
                ->whereIn('property_id', $requested_property_ids)
                ->where('pending_property_for_verifications.verification_date', '>=', date('Y-m-d'))
                ->orderBy('pending_property_for_verifications.verification_date', 'ASC')
                ->orderBy('pending_property_for_verifications.time', 'ASC')
                ->get();
            $type_owner     = "Owner";
            if ($property_verification) {
                foreach ($property_verification as $item) {
                    $item->type   = $type_owner;
                }
            }

            // user tour booking  request

            $check_tour         = UserAgentProperty::select('tour_id')
                ->where('status', 1)
                ->get();
            $tours              = json_decode($check_tour);
            if ($tours) {
                $tour_ids       = array_column($tours, 'tour_id');
            } else {
                $tour_ids       = [];
            }

            $requested_tour = UserAgentProperty::select('tour_id')
                ->whereNotIn('tour_id', $tour_ids)
                ->where('agent_id', $agent_id)
                ->where('status', 0)
                ->get();
            $requested_tour = json_decode($requested_tour);
            if ($requested_tour) {
                $requested_tour_ids       = array_column($requested_tour, 'tour_id');
            } else {
                $requested_tour_ids       = [];
            }

            $tour_bookings              = BookUserTour::select('id', 'booked_date as date', 'time_range as time', 'property_id', 'user_id')
                ->with(
                    array(
                        'property_priority_image' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                        }, 'user_rel' => function ($query) use ($base_url) {
                            $query->select('id', 'name', 'phone', 'email', DB::raw('CONCAT("' . $base_url . '",profile_pic) AS profile_pic'));
                        }, 'property_details' => function ($query) {
                            $query->select(
                                'id',
                                DB::raw('ifnull(property_name,"") as property_name'),
                                DB::raw('ifnull(latitude,"") as latitude'),
                                DB::raw('ifnull(longitude,"") as longitude'),
                                'property_reg_no',
                                'property_to'
                            );
                        }
                    )
                )
                ->whereIn('id', $requested_tour_ids)
                ->where('booked_date', '>=', date('Y-m-d'))
                ->orderBy('booked_date', 'DESC')
                ->get();

            $tour_bookings              = $this->allNull2Empty($tour_bookings);
            $type_user = "User";
            if ($tour_bookings) {
                foreach ($tour_bookings as $item) {
                    $item->type   = $type_user;
                    if (!$item->property_priority_image) {
                        $item->property_priority_image = (object)[];
                    }
                    if (!$item->user_rel) {
                        $item->user_rel = (object)[];
                    } else {
                        if (!$item->user_rel->profile_pic) {
                            $item->user_rel->profile_pic = "";
                        }
                    }
                    if (!$item->property_details) {
                        $item->property_details = (object)[];
                    }
                }
            }

            if (count($tour_bookings) > 0 && count($property_verification) > 0) {
                $verification = json_decode($property_verification);
                foreach ($tour_bookings as $item) {
                    array_push($verification, $item);
                }
                $requested_list = $verification;
            } elseif (count($tour_bookings) > 0) {
                $requested_list = $tour_bookings;
            } elseif (count($property_verification) > 0) {
                $requested_list = $property_verification;
            } else {
                $requested_list   = [];
            }
            $data  = [
                'requested_list'         => $requested_list,
            ];

            return response()->json(['status' => 200, 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function acceptingMyRequest(Request $request)
    {
        $rules = [
            'request_id'         => 'required',
        ];
        $messages = [
            'request_id.required'      => 'Request id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $base_url                = url('/');
            $agent_id                = Auth::user()->id;

            $type                    = $request->type;
            $request_id              = $request->request_id;

            if ($type == 1) { #owner
                $check_exist             = PendingPropertyForVerification::where('id', $request_id)->where('agent_id', null)->first();

                if (!$check_exist) {
                    return response()->json(['status' => 400, 'response' => 'Already assigned to agent']);
                }
                $property_id        = $check_exist->property_id;
                $owner_details           = OwnerProperty::select('owner_id')->where('id', $property_id)->first();

                if (!$owner_details) {
                    return response()->json(['status' => 400, 'response' => "Property doesn't exist"]);
                }

                $owner_id        = $owner_details->owner_id;

                if (PendingPropertyForVerification::where('id', $request_id)->update(['agent_id' => $agent_id])) {
                    if (PendingPropertyAgent::where('property_id', $property_id)->where('agent_id', $agent_id)->update(['status' => 1])) {
                        $agent_appointment               = new AgentAppointment();
                        $agent_appointment->agent_id     = $agent_id;
                        $agent_appointment->date         = $check_exist->verification_date;
                        $agent_appointment->time         = $check_exist->time;
                        $agent_appointment->type         = 2;
                        $agent_appointment->owner_id     = $owner_id;
                        $agent_appointment->property_id  = $property_id;
                        $agent_appointment->request_id   = $request_id;
                        $agent_appointment->save();
                        return response()->json(['status' => 200, 'response' => 'Request accepted successfully']);
                    }
                }
            } else {
                $check_exist     = BookUserTour::where('id', $request_id)->where('booked_date', '>=', date('Y-m-d'))->first();

                if (!$check_exist) {
                    return response()->json(['status' => 400, 'response' => 'Already assigned to agent']);
                }

                $property_id        = $check_exist->property_id;
                $user_id            = $check_exist->user_id;
                $time_range         = explode('-', $check_exist->time_range);
                $time               = $time_range[0];
                if (UserAgentProperty::where('tour_id', $request_id)
                    ->where('agent_id', $agent_id)
                    ->where('property_id', $property_id)->update(['status' => 1])
                ) {

                    $agent_appointment               = new AgentAppointment();
                    $agent_appointment->agent_id     = $agent_id;
                    $agent_appointment->date         = $check_exist->booked_date;
                    $agent_appointment->time         = $time;
                    $agent_appointment->type         = 1;
                    $agent_appointment->user_id      = $user_id;
                    $agent_appointment->property_id  = $property_id;
                    $agent_appointment->request_id   = $request_id;
                    $agent_appointment->save();
                    return response()->json(['status' => 200, 'response' => 'Request accepted successfully']);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function rejectingMyRequest(Request $request)
    {
        $rules = [
            'request_id'         => 'required',
        ];
        $messages = [
            'request_id.required'      => 'Request id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $base_url                = url('/');
            $agent_id                = Auth::user()->id;

            $type                    = $request->type;
            $request_id              = $request->request_id;

            if ($type == 1) { #owner
                $check_exist             = PendingPropertyForVerification::where('id', $request_id)->where('agent_id', null)->first();

                if (!$check_exist) {
                    return response()->json(['status' => 400, 'response' => 'Already assigned to agent']);
                }

                $property_id        = $check_exist->property_id;

                if (PendingPropertyAgent::where('property_id', $property_id)->where('agent_id', $agent_id)->update(['status' => 2])) {
                    return response()->json(['status' => 200, 'response' => 'Request rejected successfully']);
                }
            } else {
                $check_exist             = BookUserTour::where('id', $request_id)->where('booked_date', '>=', date('Y-m-d'))->first();

                if (!$check_exist) {
                    return response()->json(['status' => 400, 'response' => 'Already assigned to agent']);
                }

                $property_id        = $check_exist->property_id;
                $user_id            = $check_exist->user_id;
                if (UserAgentProperty::where('tour_id', $request_id)
                    ->where('agent_id', $agent_id)
                    ->where('property_id', $property_id)->update(['status' => 2])
                ) {
                    return response()->json(['status' => 200, 'response' => 'Request rejected successfully']);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function startOwnerTourApointment(Request $request)
    {
        $rules = [
            'tour_id'        => 'required',
        ];
        $messages = [
            'tour_id.required' => 'Start tour id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $agent_id           = Auth::user()->id;
            $verification_id    = $request->tour_id;
            $base_url           = url('/');
            $owner_tour          = PendingPropertyForVerification::where('id', $verification_id)
                ->where('agent_id', $agent_id)
                ->first();
            if ($owner_tour) {
                // $owner_id            = $owner_tour->owner_id;
                $property_id        = $owner_tour->property_id;

                $agent_tour_exist   = AgentTrackingOwnerTour::where('tour_id', $verification_id)
                    ->where('agent_id', '!=', $agent_id)
                    ->first();
                if ($agent_tour_exist) {
                    return response()->json(['status' => 400, 'response' => 'Already Being Processed']);
                }

                // check user agent tour  exist
                $agent_tour         = AgentTrackingOwnerTour::select('id', 'tour_id', 'agent_id', 'status')
                    ->where('tour_id', $verification_id)
                    ->where('agent_id', $agent_id)
                    ->first();
                if (!$agent_tour) {
                    // insert  start tour
                    $agent_tour                   = new AgentTrackingOwnerTour();
                    $agent_tour->tour_id          = $owner_tour->id;
                    $agent_tour->agent_id         = $agent_id;
                    $agent_tour->status           = 1;
                    $agent_tour->save();

                    $agent_tour                   = AgentTrackingOwnerTour::select('id', 'tour_id', 'agent_id', 'status')
                        ->where('tour_id', $verification_id)
                        ->where('agent_id', $agent_id)
                        ->first();
                }

                $agent_touring              = $this->setToNull('agent_tour', $agent_tour);

                $property_details           = OwnerProperty::select(
                    'id',
                    'property_reg_no',
                    'property_name',
                    'property_to',
                    'owner_id',
                    'rent',
                    'selling_price',
                    'mrp',
                    'security_deposit',
                    'token_amount',
                    DB::raw('ifnull(furnished,3) as furnished'),
                    'latitude',
                    'longitude',
                    DB::raw('ifnull(is_builder,2) as is_builder')
                )
                    ->with(
                        array(
                            'property_priority_image' =>
                            function ($query) use ($base_url) {
                                $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                            }, 'property_details:detail_id,value,name',
                        )
                    )
                    ->where('id', $property_id)
                    ->first();

                $details          = Detail::select('id', 'name')->get();


                if ($property_details) {
                    if ($property_details->property_details) {
                        $property_details->details    = $property_details->property_details;
                        $property_spec = $property_details->property_details;
                        foreach ($details as $key => $each_detail) {
                            $count = 0;
                            foreach ($property_spec as $key => $detail_array) {
                                if ($each_detail->name == $detail_array->name) {
                                    $count = 1;
                                    $type = $detail_array->name;
                                    $property_details->$type = $detail_array->value;
                                    break;
                                }
                            }
                            if ($count != 1) {
                                $detail_data = $each_detail->name;
                                $property_details->$detail_data = 0;
                            }
                        }
                        unset($property_details->property_details);
                    }
                    if (!$property_details->property_priority_image) {
                        $property_details->property_priority_image   = (object)[];
                    }
                    $owner_id           = $property_details->owner_id;
                    $property_details   = $this->setToNull('property_details', $property_details);
                } else {
                    $owner_id           = 0;
                    $property_details['property_details']   = (object)[];
                }
                $owner_details               = Owner::select(
                    'id',
                    'name',
                    'email',
                    'phone',
                    DB::raw('CONCAT("' . $base_url . '",profile_image) AS profile_image')
                )
                    ->where('id', $owner_id)
                    ->first();
                if ($owner_details) {
                    $owner_details   = $this->setToNull('owner_details', $owner_details);
                } else {
                    $owner_details['owner_details']   = (object)[];
                }

                $data = [
                    'agent_tour'       => $agent_touring['agent_tour'],
                    'property_details' => $property_details['property_details'],
                    'owner_details'     => $owner_details['owner_details'],
                ];

                DB::commit();
                return response()->json(['status' => 200, 'response' => $data]);
            } else {
                return response()->json(['status' => 400, 'response' => "This tour doesn't exist"]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function myOwnerProperties(Request $request)
    {
        try {
            $page               = $request->page;
            $completed          = $request->completed;
            $agent_id           = Auth::user()->id;
            $base_url           = url('/');
            $agent_tour         = AgentTrackingOwnerTour::select('tour_id')
                ->where('agent_id', $agent_id);
            if ($completed) {
                $agent_tours    = $agent_tour->where('status', 3)
                    ->orderBy('id', 'DESC')
                    ->get();
            } else {
                $agent_tours    = $agent_tour->where('status', '!=', 3)
                    ->get();
            }
            $tours              = json_decode($agent_tours);
            if ($tours) {
                $tour_ids       = array_column($tours, 'tour_id');
            } else {
                $tour_ids       = [0];
                // return response()->json(['status'=>200,'tour_data'=>(object)[]]);
            }

            $tour_details       = PendingPropertyForVerification::select('id', 'property_id')
                ->with(
                    array(
                        'property_priority_image' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                        }, 'user_property_related' => function ($query) {
                            $query->select('id', DB::raw('ifnull(property_name,"") as property_name'), 'property_reg_no', 'owner_id');
                        }
                    )
                )
                ->orderBy('id', 'DESC')
                ->whereIn('id', $tour_ids);



            $tour_details       = $this->customPagination($tour_details, $page);


            if ($tour_details['data']) {
                foreach ($tour_details['data'] as $each_tour) {
                    if ($each_tour->property_priority_image == "") {
                        $each_tour->property_priority_image = (object)[];
                    }
                    if ($each_tour->user_property_related == "") {
                        $each_tour->user_property_related   = (object)[];
                    } else {
                        $owner_id = $each_tour->user_property_related->owner_id;
                        $each_tour->owner_rel   = Owner::select('id', 'name', 'phone', 'email', DB::raw('CONCAT("' . $base_url . '",profile_image) AS profile_image'))
                            ->where('id', $owner_id)
                            ->first();
                        if (!$each_tour->owner_rel->profile_image) {
                            $each_tour->owner_rel->profile_image = "";
                        }
                    }
                }
            }

            $tour_data  = [
                'total_page_count' => $tour_details['total_page_count'],
                'current_page'     => $tour_details['current_page'],
                'user_properties'  => $tour_details['data'],
            ];

            return response()->json(['status' => 200, 'tour_data' => $tour_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function agentOwnerPropertyDetails(Request $request)
    {
        $rules = [
            'tour_id'               => 'required',
        ];
        $messages = [
            'tour_id.required'      => 'Tour is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $tour_id            = $request->tour_id;
            $agent_id           = Auth::user()->id;
            $base_url           = url('/');
            $agent_tour         = AgentTrackingOwnerTour::select('tour_id', 'status')
                ->where('agent_id', $agent_id)
                ->where('tour_id', $tour_id)
                // ->where('status',3)
                ->first();
            $agent_touring              = $this->setToNull('agent_tour', $agent_tour);

            $tour_details       = PendingPropertyForVerification::select('id', 'property_id', 'agent_id')
                ->with(
                    array(
                        'property_priority_image' => function ($query) use ($base_url) {
                            $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                        }
                    )
                )
                ->where('id', $tour_id)
                ->where('agent_id', $agent_id)
                ->first();
            if ($tour_details) {
                $property_id                    = $tour_details->property_id;
                $owner_id                        = $tour_details->owner_id;
                $property_details           = OwnerProperty::select(
                    'id',
                    'property_reg_no',
                    DB::raw('ifnull(property_name,"") AS property_name'),
                    'property_to',
                    'owner_id',
                    DB::raw('ifnull(rent,0.00) AS rent'),
                    DB::raw('ifnull(selling_price,0.00) AS selling_price'),
                    DB::raw('ifnull(mrp,0.00) AS mrp'),
                    DB::raw('ifnull(security_deposit,0.00) AS security_deposit'),
                    DB::raw('ifnull(token_amount,0.00) AS token_amount'),
                    DB::raw('ifnull(furnished,0.00) AS furnished'),
                    DB::raw('ifnull(is_builder,2) AS is_builder'),
                    DB::raw('ifnull(latitude,0.00) AS latitude'),
                    DB::raw('ifnull(longitude,0.00) AS longitude'),
                    DB::raw('ifnull(contract_start_date,"") AS contract_start_date'),
                    DB::raw('ifnull(contract_end_date,"") AS contract_end_date')
                )
                    ->with(
                        array(
                            'property_priority_image' =>
                            function ($query) use ($base_url) {
                                $query->select(DB::raw('CONCAT("' . $base_url . '",document) AS document'), 'property_id');
                            }, 'property_details:detail_id,value,name'
                        )
                    )
                    ->where('id', $property_id)
                    ->first();


                $details          = Detail::select('id', 'name')->get();


                if ($property_details) {
                    if ($property_details->property_details) {
                        $property_details->details    = $property_details->property_details;
                        $property_spec = $property_details->property_details;
                        foreach ($details as $key => $each_detail) {
                            $count = 0;
                            foreach ($property_spec as $key => $detail_array) {
                                if ($each_detail->name == $detail_array->name) {
                                    $count = 1;
                                    $type = $detail_array->name;
                                    $property_details->$type = $detail_array->value;
                                    break;
                                }
                            }
                            if ($count != 1) {
                                $detail_data = $each_detail->name;
                                $property_details->$detail_data = 0;
                            }
                        }
                        unset($property_details->property_details);
                    }
                    if (!$property_details->property_priority_image) {
                        $property_details->property_priority_image   = (object)[];
                    }
                    $owner_id           = $property_details->owner_id;
                    $property_details   = $this->setToNull('property_details', $property_details);
                } else {
                    $owner_id           = 0;
                    $property_details['property_details']   = (object)[];
                }
                $owner_details               = Owner::select(
                    'id',
                    'name',
                    'email',
                    'phone',
                    DB::raw('CONCAT("' . $base_url . '",profile_image) AS profile_image')
                )
                    ->where('id', $owner_id)
                    ->first();
                if ($owner_details) {
                    $owner_details   = $this->setToNull('owner_details', $owner_details);
                } else {
                    $owner_details['owner_details']   = (object)[];
                }

                $data = [
                    'agent_tour'       => $agent_touring['agent_tour'],
                    'property_details' => $property_details['property_details'],
                    'owner_details'     => $owner_details['owner_details'],
                ];
            } else {
                return response()->json(['status' => 400, 'data' => "This tour doesn't exist"]);
            }


            return response()->json(['status' => 200, 'response' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function removeAgentProfilePic(Request $request)
    {
        try {
            $agent_id                   = Auth::user()->id;
            $update_agent               = Agent::where('id', $agent_id)->first();
            if ($update_agent->image) {
                if (file_exists(public_path($update_agent->image))) {
                    unlink(public_path($update_agent->image));
                }
            }
            $update_agent->image = NULL;
            $update_agent->save();
            return response()->json(['status' => 200, 'response' => "Removed profile image successfully"]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentCashInHandList(Request $request)
    {
        try {
            $page               = $request->page;
            $agent_id           = Auth::user()->id;
            $agent_cash         = AgentCashInHand::select('id', 'user_id', 'property_id', DB::raw('ifnull(payment_amount,0) as payment_amount'))
                ->with('property_rel:id,property_reg_no', 'user_rel:id,name')
                ->where('agent_id', $agent_id)
                ->where('status', 1)
                ->orderBy('created_at', 'DESC');
            $agent_cash         = $this->customPagination($agent_cash, $page);

            if ($agent_cash['data']) {
                foreach ($agent_cash['data'] as $each_cash) {
                    if ($each_cash->property_rel == "") {
                        $each_cash->property_rel = (object)[];
                    }
                }
            }
            $cash_data  = [
                'total_page_count' => $agent_cash['total_page_count'],
                'current_page'     => $agent_cash['current_page'],
                'cash_data'        => $agent_cash['data'],
            ];

            return response()->json(['status' => 200, 'agent_cash_data' => $cash_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentTotalCashInHand(Request $request)
    {
        try {
            $agent_id           = Auth::user()->id;
            $cash_amount        = AgentCashInHand::where('agent_id', $agent_id)
                ->where('status', 1)
                ->sum('payment_amount');
            $cash_amount        = strval($cash_amount);
            $check_cash         = AgentCashPayRequest::where('agent_id', $agent_id)
                ->where('amount', $cash_amount)
                ->where('status', 0)
                ->first();
            $can_request        = 1;
            if ($check_cash) {
                $can_request    = 0;
            }

            return response()->json(['status' => 200, 'total_amount' => $cash_amount, 'can_pay' => $can_request]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function listPropertyDocument(Request $request)
    {
        $rules = [
            'property_id'               => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $property_id        = $request->property_id;
            $base_url           = url('/');
            //all type document

            // images
            $images         = OwnerPropertyDocument::select('id', DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'property_id', 'type')
                ->where('property_id', $property_id)
                ->where('type', 0)
                ->get();
            // videos
            $videos         = OwnerPropertyDocument::select('id', DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'property_id', 'type')
                ->where('property_id', $property_id)
                ->where('type', 1)
                ->get();
            // floor plans
            $floor_plans    = OwnerPropertyDocument::select('id', DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'property_id', 'type')
                ->where('property_id', $property_id)
                ->where('type', 2)
                ->get();

            $data       = [
                'images'        => $images,
                'videos'        => $videos,
                'floor_plans'   => $floor_plans,
            ];

            return response()->json(['status' => 200, 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentPayCashInHandToAdmin(Request $request)
    {
        try {
            $agent_id               = Auth::user()->id;

            $cash_amount        = AgentCashInHand::where('agent_id', $agent_id)
                ->where('status', 1)
                ->sum('payment_amount');

            $pay_request            = new AgentCashPayRequest();
            $pay_request->agent_id  = $agent_id;
            $pay_request->amount    = $cash_amount;
            $pay_request->status    = 0;
            $pay_request->save();

            return response()->json(['status' => 200, 'response' => 'Successfully requested']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentPropertyContractDetails(Request $request)
    {
        $rules = [
            'property_id'               => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $agent_id    = Auth::user()->id;
        $base_url    = url('/');
        try {
            $property_id       = $request->property_id;
            $details           = OwnerProperty::select(DB::raw('ifnull(contract_start_date,"") AS contract_start_date'), DB::raw('ifnull(contract_end_date,"") AS contract_end_date'), DB::raw('CONCAT("' . $base_url . '",contract_file) AS contract_file'))
                ->where('id', $property_id)
                ->first();

            if ($details) {
                if (!$details->contract_file) {
                    $details->contract_file = "";
                }
            } else {
                $details    = (object)[];
            }

            return response()->json(['status' => 200, 'data' => $details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentUserPropertyContractDetails(Request $request)
    {
        $rules = [
            'user_id'                   => 'required',
            'property_id'               => 'required',
        ];
        $messages = [
            'user_id.required'      => 'User is required',
            'property_id.required'      => 'Property is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $agent_id    = Auth::user()->id;
        $base_url    = url('/');
        try {
            $property_id       = $request->property_id;
            $user_id           = $request->user_id;
            $details           = BookingContract::select(DB::raw('CONCAT("' . $base_url . '",contract_file) AS contract_file'))
                ->join('user_properties', 'user_properties.id', 'booking_contracts.user_property_id')
                ->where('booking_contracts.user_id', $user_id)
                ->where('user_properties.id', $property_id)
                ->where('user_properties.status', '!=', 3)
                ->first();

            if ($details) {
                if (!$details->contract_file) {
                    $details->contract_file = "";
                }
            } else {
                $details    = (object)[];
            }

            return response()->json(['status' => 200, 'data' => $details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function showTourLocationDetails(Request $request)
    {
        $rules = [
            'tour_id'                 => 'required',
            'user_type'               => 'required',
        ];
        $messages = [
            'tour_id.required'      => 'Tour id is required',
            'user_type.required'      => 'User type is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $agent_id    = Auth::user()->id;
        $base_url    = url('/');
        try {
            $tour_id       = $request->tour_id;
            $user_type     = $request->user_type;
            if ($user_type == 1) { #user
                $tour_details           = BookUserTour::select('property_id')
                    ->where('id', $tour_id)
                    ->first();
            } else { #owner
                $tour_details           = PendingPropertyForVerification::select('property_id')
                    ->where('id', $tour_id)
                    ->first();
            }

            if ($tour_details) {
                $property_id    = $tour_details->property_id;
                $base_url       = url('/');
                $property       = OwnerProperty::select(
                    'id',
                    DB::raw('ifnull(property_name,"") AS property_name'),
                    DB::raw('ifnull(property_type,"") AS property_type'),
                    'rating',
                    'property_to',
                    DB::raw('ifnull(latitude, 0.0) AS latitude'),
                    DB::raw('ifnull(longitude, 0.0) AS longitude'),
                    DB::raw('ifnull(rent,0.0) AS rent'),
                    DB::raw('ifnull(selling_price,0.0) AS selling_price')
                )
                    ->with(
                        array(
                            'property_priority_image' => function ($query) use ($base_url) {
                                $query->select('id', 'property_id', 'type', DB::raw('CONCAT("' . $base_url . '",document) AS document'));
                            }, 'property_details:property_id,detail_id,value,name',
                        )
                    )
                    ->where('id', $property_id)
                    // ->where('status',1)
                    // ->where('contract_owner','1')
                    ->first();

                $details          = Detail::select('id', 'name')->get();
                if ($property) {
                    if ($property->property_details) {
                        $new_property = [];
                        foreach ($details as $key => $each_detail) {
                            $count = 0;
                            foreach ($property->property_details as $key => $detail_array) {
                                if ($each_detail->name == $detail_array->name) {
                                    $count = 1;
                                    $type = $detail_array->name;
                                    $property->$type = $detail_array->value;
                                    break;
                                }
                            }
                            if ($count != 1) {
                                $detail_data = $each_detail->name;
                                $property->$detail_data = 0;
                            }
                        }
                        unset($property->property_details);
                    }
                    if (!$property->property_priority_image) {
                        unset($property->property_priority_image);
                        $property->property_priority_image      = (object)[];
                    }

                    $propert_details    = $property;
                } else {
                    $propert_details       = (object)[];
                }
            } else {
                $propert_details    = (object)[];
            }

            return response()->json(['status' => 200, 'property_details' => $propert_details]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function agentEarnings(Request $request)
    {
        $agent_id    = Auth::user()->id;
        $page        = $request->page;
        try {
            $agent_payments    = AgentEarnings::select('id', 'amount', 'property_id', 'status')
                ->with('property_rel:id,property_reg_no')
                ->where('agent_id', $agent_id)
                ->where('status', 0)
                ->orderBy('created_at', 'DESC');

            $agent_payments     = $this->customPagination($agent_payments, $page);

            $agent_payment_data = [
                'total_page_count' => $agent_payments['total_page_count'],
                'current_page'     => $agent_payments['current_page'],
                'payments'         => $agent_payments['data'],
            ];

            return response()->json(['status' => 200, 'data' => $agent_payment_data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentTotalEarnings(Request $request)
    {
        $agent_id    = Auth::user()->id;
        try {
            $agent_payments     = AgentEarnings::where('agent_id', $agent_id)
                ->where('status', 0)
                ->sum('amount');

            $can_request        = 0;
            $agent_target       = Agent::select('id', 'target_amount')->where('id', $agent_id)->first();
            $agent_target_amount = (int)$agent_target->target_amount;
            if ($agent_payments >= $agent_target_amount) {
                $can_request    = 1;
            }
            $agent_payments     = strval($agent_payments);

            $data = [
                'total_earnings'    => $agent_payments,
                'can_request'       => $can_request,
            ];

            return response()->json(['status' => 200, 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }



    //terms of stay
    public function terms_of_stay(request $request)
    {
        try {
            $data = OwnerPropertyTermsOfStay::select('id', 'property_id', 'terms_of_stay')->where('property_id', '=', $request->segment(4))->first();
            // if(!empty($data)){
            //     return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);
            // }else{
            //     return response()->json(['status' => 200, 'data' => (object)[] , 'message' => 'No Data Found']);
            // }
            if (!$data) {
                $data   = (object)[];
            }
            return response()->json(['status' => 200,  'data' => $data, 'message' => 'Success']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    //agent-notification
    public function agent_notifications(request $request)
    {
        $page       = $request->page;
        try {
            $notifications = AgentNotifications::select('id', 'notification_heading', 'notification_text', DB::raw('ifnull(status,"unread") as status'), DB::raw('ifnull(created_at,"1970-01-01 00:00:00") as date'))
                ->where('user_id', '=', Auth::user()->id);

            $notifications = $this->customPagination($notifications, $page);
            $notifications_data = [
                'total_page_count'      => $notifications['total_page_count'],
                'current_page'          => $notifications['current_page'],
                'notifications'            => $notifications['data'],
            ];
            return response()->json(['status' => 200,  'data' => $notifications_data, 'message' => 'Success']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
    public function read_notifications(Request $request)
    {
        $rules = [
            'notification_id'  => 'required',
            'status'  => 'required',
        ];
        $messages = [
            'status.required'      => 'Status Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            AgentNotifications::where('id', $request->notification_id)->update(['status' => $request->status]);
            return response()->json(['status' => 200, 'message' => 'updated']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function price_details(request $request)
    {
        $rules = [
            'property_id'  => 'required',
            'type'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property ID Required',
            'type.required'      => 'Property Type Required',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            if ($request->type == 'sale') {
                $sale_data = OwnerProperty::select('id', DB::raw('ifnull(selling_price,"0.00") AS selling_price'))->where('id', '=', $request->property_id)->first();

                if (!empty($sale_data)) {
                    $data = PropertyEmiDetails::select('id', 'period_type', 'emi_count', 'emi_amount', 'interest_rate', 'descriptions',)
                        ->where('property_id', '=', $request->property_id)
                        ->get();
                    return response()->json(['status' => 200, 'selling_price' => $sale_data->selling_price, 'emi_details' => $data, 'message' => 'Success']);
                } else {
                    return response()->json(['status' => 204, 'message' => 'No Data Found']);
                }
            } else {
                $rent_data = OwnerProperty::select('owner_properties.id', DB::raw('ifnull(rent,"0.00") as rent'), DB::raw('ifnull(security_deposit,"0.00") as security_deposit'), 'property_to')
                    ->leftjoin('frequencies', 'frequencies.id', '=', 'owner_properties.frequency')
                    ->where('owner_properties.id', '=', $request->property_id)
                    ->first();
                if (!empty($rent_data)) {
                    $data = OtherPropertyCharges::select('id', 'charge_name', DB::raw('ifnull(amount,"0.00") AS amount'), DB::raw('ifnull(descriptions,"") AS descriptions'))
                        ->where('property_id', '=', $request->property_id)
                        ->get();
                    return response()->json(['status' => 200, 'rent_data' => $rent_data, 'other_charge_data' => $data, 'message' => 'Success']);
                } else {
                    return response()->json(['status' => 204, 'message' => 'No Data Found']);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function feedbacks(request $request)
    {
        $page       = $request->page;
        try {
            $feedbacks = AgentFeedbacks::select('agent_feedbacks.id', 'rating', DB::raw('ifnull(comments,"") as comments'), DB::raw('ifnull(name,"user") as name'), DB::raw('ifnull(email,"") as email'), DB::raw('ifnull(phone,"") as phone'))
                ->leftjoin('users', 'users.id', '=', 'agent_feedbacks.user_id')
                ->where('agent_id', '=', Auth::user()->id);
            $feedbacks = $this->customPagination($feedbacks, $page);
            $feedbacks_data = [
                'total_page_count'      => $feedbacks['total_page_count'],
                'current_page'          => $feedbacks['current_page'],
                'feedbacks'             => $feedbacks['data'],
            ];
            return response()->json(['status' => 200,  'data' => $feedbacks_data, 'message' => 'Success']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function addAgentEarnings(Request $request)
    {
        $rules = [
            'tour_id'          => 'required|unique:agent_earnings,tour_id',
            'user_id'          => 'required',
            'property_id'      => 'required',
            'amount'           => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];
        $messages = [
            'tour_id.required'      => 'Tour id is required',
            'user_id.required'      => 'User id is required',
            'property_id.required'  => 'Property id is required',
            'amount.required'       => 'Amount is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $tour_id        = $request->tour_id;
        $user_id        = $request->user_id;
        $property_id    = $request->property_id;
        $amount         = $request->amount;
        try {
            DB::beginTransaction();
            $agent_id       = Auth::user()->id;
            $tour_details   = AgentTrackingUserTour::where('id', $tour_id)
                ->where('agent_id', $agent_id)
                ->first();
            if (!$tour_details) {
                return response()->json(['status' => 400, 'response' => 'Invalid Tour']);
            }

            $agent_earnings                 = new AgentEarnings();
            $agent_earnings->tour_id        = $tour_id;
            $agent_earnings->user_id        = $user_id;
            $agent_earnings->agent_id       = $agent_id;
            $agent_earnings->property_id    = $property_id;
            $agent_earnings->amount         = $amount;
            $agent_earnings->status         = 0;
            $agent_earnings->save();

            DB::commit();
            return response()->json(['status' => 200, 'response' => 'Commission added successfully']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function requestAgentEarnings(Request $request)
    {
        try {
            DB::beginTransaction();
            $agent_id       = Auth::user()->id;

            $agent_earning  = AgentEarnings::where('agent_id', $agent_id)->where('status', 0)->sum('amount');
            //check request amount equal to target amount

            //check if already requested
            $check_request  = RequestAgentEarnings::where('agent_id', $agent_id)->where('status', 0)->get();
            if ($check_request) {
                RequestAgentEarnings::where('agent_id', $agent_id)->where('status', 0)->delete();
            }

            $request_agent_earnings                     = new RequestAgentEarnings();
            $request_agent_earnings->agent_id           = $agent_id;
            $request_agent_earnings->requested_amount   = $agent_earning;
            $request_agent_earnings->status             = 0; //set to active request
            $request_agent_earnings->save();

            DB::commit();
            return response()->json(['status' => 200, 'response' => 'Requested Successfully']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function editAgentEarnings(Request $request)
    {
        $rules = [
            'tour_id'    => 'required',
            'amount'     => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];
        $messages = [
            'tour_id.required'          => 'Tour id is required',
            'amount.required'           => 'Amount is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $tour_id            = $request->tour_id;
        $amount             = $request->amount;
        $agent_id           = Auth::user()->id;
        try {
            DB::beginTransaction();

            $agent_earnings                 = AgentEarnings::where('tour_id', $tour_id)->where('agent_id', $agent_id)->first();
            if (!$agent_earnings) {
                return response()->json(['status' => 400, 'response' => 'Invalid commission']);
            }

            $agent_earnings->amount         = $amount;
            $agent_earnings->save();

            DB::commit();
            return response()->json(['status' => 200, 'response' => 'Commission updated successfully']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    //show property other  pakages api at agent propety details
    public function propertyOtherPackages(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property Id is Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $packages       = OfferPackage::select(
                'id',
                'offer_package_name',
                'property_id',
                DB::raw('ifnull(discount_amount,"") AS discount_amount'),
                DB::raw('ifnull(actual_amount,"") AS actual_amount'),
                'start_date',
                'end_date',
                'description',
                'frequency_id'
            )
                ->with('package_features:package_feature,package_id')
                ->where('property_id', $request->property_id)
                ->where('start_date', '<=', date('Y-m-d'))
                ->where('end_date', '>=', date('Y-m-d'))
                ->where('status', 1)
                ->get();
            return response()->json(['status' => 200, 'packages' => $packages]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function propertyOtherPackagesForDropdown(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property Id is Required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $packages       = OfferPackage::select('id', 'offer_package_name')
                ->where('property_id', $request->property_id)
                ->where('start_date', '<=', date('Y-m-d'))
                ->where('end_date', '>=', date('Y-m-d'))
                ->where('status', 1)
                ->get();
            return response()->json(['status' => 200, 'packages' => $packages]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }


    public function downloadPropertyDetailsPDF(Request $request)
    {
        $rules = [
            'property_id'  => 'required',
        ];
        $messages = [
            'property_id.required'      => 'Property ID Required',
        ];

        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $agentId        = Auth::user()->id;
            $base_url       = url('/');
            $property_id    = $request->property_id;
            $verfication_property    = PendingPropertyForVerification::where('property_id', $property_id)
                ->where('agent_id', $agentId)
                ->first();

            $properties     = OwnerProperty::select(
                'owner_properties.id',
                'property_reg_no',
                'property_name',
                DB::raw('ifnull(property_type,"") AS property_type'),
                'owner_id',
                'rating',
                'property_to',
                'category',
                'selling_price',
                'mrp',
                'is_featured',
                'furnished',
                'longitude',
                'latitude',
                'rent',
                'city',
                'state',
                'country',
                'description',
                'occupied',
                'zip_code',
                'street_address_1',
                'street_address_2',
                DB::raw('ifnull(type_id,0) as type_id'),
                'expected_amount'
            );
            $properties     = $properties->with(
                array(
                    'documents' => function ($query) use ($base_url) {
                        $query->select(DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'id', 'property_id', 'type', 'owner_id');
                    },
                    'amenity_details' => function ($query) {
                        $query->select('id', 'property_id', 'amenity_id', 'owner_id', 'amenities_category_id');
                    },
                    'property_details' => function ($query) {
                        $query->select('property_id', 'detail_id', 'value', 'name');
                    },
                    'owner_rel' => function ($query) use ($base_url) {
                        $query->select('id', 'name', 'phone', DB::raw('CONCAT("' . $base_url . '", profile_image) AS profile_image'));
                    },
                    'country_rel' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'state_rel' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'city_rel' => function ($query) {
                        $query->select('id', 'name');
                    },
                    'zipcode_rel' => function ($query) {
                        $query->select('id', 'pincode', 'city');
                    },
                    'floor_plans' => function ($query) use ($base_url) {
                        $query->select('id', 'property_id', DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'owner_id');
                    }
                )
            );

            if (!$verfication_property) {
                $properties         =   $properties->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                    ->where('agent_properties.agent_id', $agentId)
                    ->where('agent_properties.status', 1);
            }
            $properties         =   $properties->where('owner_properties.id', $property_id)
                ->first();
            if ($properties) {
                $properties     = $this->setToNull('property', $properties);

                $details          = Detail::select('id', 'name')->get();
                $amenity_category = AmenitiesCategory::select('id', 'name')->orderBy('sort_order', 'ASC')->get();

                $prop = $properties['property'];
                if (!$prop->owner_rel) {
                    $prop->owner_rel = (object)[];
                }
                if (!$prop->country_rel) {
                    $prop->country_rel = (object)[];
                }
                if (!$prop->state_rel) {
                    $prop->state_rel = (object)[];
                }
                if (!$prop->city_rel) {
                    $prop->city_rel = (object)[];
                }
                if (!$prop->zipcode_rel) {
                    $prop->zipcode_rel = (object)[];
                }
                if (!$prop->floor_plans) {
                    $prop->floor_plans = (object)[];
                }

                if ($prop->property_details) {
                    $new_property = [];
                    foreach ($details as $key => $each_detail) {
                        $count = 0;
                        foreach ($prop->property_details as $key => $detail_array) {
                            if ($each_detail->name == $detail_array->name) {
                                $count = 1;
                                $type = $detail_array->name;
                                $prop->$type = $detail_array->value;
                                break;
                            }
                        }
                        if ($count != 1) {
                            $detail_data = $each_detail->name;
                            $prop->$detail_data = 0;
                        }
                    }
                }

                if ($prop->amenity_details) {
                    $new_property = [];
                    $assigned_amenities   = [];
                    foreach ($amenity_category as $key => $each_category) {
                        $count = 0;
                        $property_amenities = [];
                        foreach ($prop->amenity_details as $key => $each_category_detail) {
                            if ($each_category->id == $each_category_detail->amenities_category_id) {
                                $type = $each_category->name;
                                $amenities = Amenity::select('id', 'name', DB::raw('CONCAT("' . $base_url . '", image) AS image'))
                                    ->where('amenity_category_id', $each_category->id)
                                    ->get();
                                $encoded_amenities = json_encode($amenities);
                                $decoded_amenities = json_decode($encoded_amenities);
                                foreach ($decoded_amenities as $key => $file_data) {
                                    $property_amenities[] = $file_data;
                                }
                                $count = 1;
                                break;
                            }
                        }
                        if ($count == 1) {
                            if (count($property_amenities) > 0) {
                                $amenities_array = [
                                    'id'         => $each_category->id,
                                    'name'       => $each_category->name,
                                    'amenity_details' => $property_amenities
                                ];
                                $assigned_amenities[]  = $amenities_array;
                                // $prop->amenity_categories = $amenities_array;

                            }
                        }
                    }
                    $prop->amenity_categories   = $assigned_amenities;
                }
                // unset($prop->property_details);
                unset($prop->amenity_details);

                // count of feedback as rating
                $prop->total_rating_count = UserPropertyRating::where('property_id', $property_id)->count();
            } else {
                $properties['property'] = (object)[];
            }
            if ($properties['property']) {
                $filename = time() . rand() . 'pdf.pdf';

                $content    = view('admin.pdf.property_details', $properties);
                // return $content;

                $pdf        = PDF::loadHTML(' ' . $content . ' ');
                // // Save file to the directory
                $pdf->save('uploads/pdf/' . $filename);

                $pdffile = url('/') . '/uploads/pdf/' . $filename;
                $data = [
                    'pdf' => $pdffile,
                ];
                return ['status' => 200, 'data' => $data];
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function agentPropertyBuildingDetails(Request $request)
    {
        $agentId        = Auth::user()->id;
        $base_url       = url('/');
        $property_id    = $request->property_id;
        $verfication_property    = PendingPropertyForVerification::where('property_id', $property_id)
            ->where('agent_id', $agentId)
            ->first();

        $properties     = OwnerProperty::select(
            'owner_properties.id',
            'property_reg_no',
            DB::raw('ifnull(property_name,"") AS property_name'),
            DB::raw('ifnull(property_type,"") AS property_type'),
            'owner_id',
            'rating',
            'property_to',
            'category',
            'is_featured',
            DB::raw('ifnull(furnished,"") AS furnished'),
            DB::raw('ifnull(longitude,"") AS longitude'),
            DB::raw('ifnull(latitude,"") AS latitude'),
            DB::raw('ifnull(rent,"") AS rent'),
            'city',
            'state',
            'country',
            DB::raw('ifnull(description,"") AS description'),
            DB::raw('ifnull(occupied,0) AS occupied'),
            'zip_code',
            DB::raw('ifnull(street_address_1,"") AS street_address_1'),
            DB::raw('ifnull(street_address_2,"") AS street_address_2'),
            DB::raw('ifnull(type_id,0) as type_id')
        );
        $properties     = $properties->with(
            array(
                'documents' => function ($query) use ($base_url) {
                    $query->select(DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'id', 'property_id', 'type', 'owner_id');
                },
                'owner_rel' => function ($query) use ($base_url) {
                    $query->select('id', 'name', 'phone', DB::raw('CONCAT("' . $base_url . '", profile_image) AS profile_image'));
                },
                'country_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'state_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'city_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'zipcode_rel' => function ($query) {
                    $query->select('id', 'pincode', 'city');
                }
            )
        );

        if (!$verfication_property) {
            $properties         =   $properties->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1);
        }
        $properties         =   $properties->where('owner_properties.id', $property_id)
            ->first();

        if ($properties) {
            $all_appartment    = OwnerProperty::select(
                'id',
                DB::raw('ifnull(property_name,"") as unit_name'),
                'property_reg_no',
                DB::raw('ifnull(expected_amount,0.00) as expected_amount'),
                DB::raw('ifnull(furnished,3) as furnished'),
                DB::raw('ifnull(occupied,2) as occupied')
            )
                ->where('builder_id', $property_id)
                ->with(array('property_priority_image' => function ($query) use ($base_url) {
                    $query->select(DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'id', 'property_id', 'type', 'owner_id');
                }))
                ->get();

            $total_appartments_count    = OwnerProperty::where('builder_id', $property_id)->count();
            $vacated_appartments_count  = OwnerProperty::where('builder_id', $property_id)->where('occupied', 0)->count();
            $occupied_appartments_count = OwnerProperty::where('builder_id', $property_id)->where('occupied', 1)->count();
            $properties->appartments_count     = $total_appartments_count;
            $properties->vacated_count        = $vacated_appartments_count;
            $properties->occupied_count        = $occupied_appartments_count;
            $data = [
                'building_details' => $properties,
                'units' => $all_appartment,
            ];
        } else {
            $data = [
                'building_details' => (object)[],
                'units' => [],
            ];
        }

        return response()->json(['status' => 200, 'response' => $data]);
    }

    public function updateBuildingDetails(Request $request)
    {
        $rules = [
            'building_id'        => 'required',
        ];
        $messages = [
            'building_id.required' => 'Building id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        $agentId                        = Auth::user()->id;
        $property_id                    = $request->building_id;

        $verfication_property   = PendingPropertyForVerification::where('property_id', $property_id)
            ->where('agent_id', $agentId)
            ->first();

        $property               = OwnerProperty::select('owner_properties.*')
            ->where('owner_properties.id', $property_id);
        if (!$verfication_property) {
            $property           =   $property->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1)
                ->first();
        } else {
            $property           =   $property->join('pending_property_for_verifications', 'pending_property_for_verifications.property_id', 'owner_properties.id')
                ->where('pending_property_for_verifications.agent_id', $agentId)
                // ->where('pending_property_for_verifications.status',1)
                ->first();
        }

        if (!$property) {
            return response()->json(['status' => 400, 'response' => "Invalid property"]);
        }

        $property->property_to   = $request->property_to;
        $property->category      = $request->category;
        if ($request->property_name) {
            $property->property_name        = $request->property_name;
        }
        if ($request->address1) {
            $property->street_address_1     = $request->address1;
        }
        if ($request->address2) {
            $property->street_address_2     = $request->address2;
        }
        if ($request->country) {
            $property->country              = $request->country;
        }
        if ($request->city) {
            $property->city                 = $request->city;
        }
        if ($request->state) {
            $property->state                = $request->state;
        }
        if ($request->zip_code) {
            $property->zip_code             = $request->zipcode;
        }
        if ($request->type_id) {
            $property->type_id              = $request->type_id;
        }
        // try{
        DB::beginTransaction();

        $property->save();

        $savedoc = array();

        if ($request->file('building_images')) {
            foreach ($request->file('building_images') as $key => $row) {
                $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                $row->move(public_path('/uploads/property/images'), $imagename);
                $savedoc[$key]['property_id']   = $property->id;
                $savedoc[$key]['document']      = '/uploads/property/images/' . $imagename;
                $savedoc[$key]['type']          = '0';
                $savedoc[$key]['owner_id']      = $property->owner_id;
                $savedoc[$key]['created_at']    = now();
                $savedoc[$key]['updated_at']    = now();
            }
            OwnerPropertyDocument::insert($savedoc);
        }

        $data['property_to'] = $request->property_to;
        $data['category']    = $request->category;
        if ($request->property_name) {
            $data['property_name'] = $request->property_name;
        }
        if ($request->address1) {
            $data['street_address_1']     = $request->address1;
        }
        if ($request->address2) {
            $data['street_address_2']     = $request->address2;
        }
        if ($request->country) {
            $data['country']              = $request->country;
        }
        if ($request->city) {
            $data['city']                 = $request->city;
        }
        if ($request->state) {
            $data['state']                = $request->state;
        }
        if ($request->zip_code) {
            $data['zip_code']             = $request->zipcode;
        }
        if ($request->type_id) {
            $data['type_id']              = $request->type_id;
        }
        OwnerProperty::where('builder_id', $property_id)->update($data);

        $agent_tour         = PendingPropertyForVerification::select('pending_property_for_verifications.id')
            ->join('agent_tracking_owner_tours', 'agent_tracking_owner_tours.tour_id', 'pending_property_for_verifications.id')
            ->where('pending_property_for_verifications.agent_id', $agentId)
            ->where('pending_property_for_verifications.property_id', $property_id)
            ->where('agent_tracking_owner_tours.status', 1)
            ->first();
        if ($agent_tour) {
            AgentTrackingOwnerTour::where('tour_id', $agent_tour->id)
                ->where('agent_id', $agentId)
                ->update(['status' => 2]);

            //code to remove the perticular owner tour from home appointment
            AgentAppointment::where('agent_id', $agentId)
                ->where('request_id', $agent_tour->id)
                ->where('property_id', $property_id)
                ->where('type', 2)
                ->update(['status' => 0]);
        }



        DB::commit();

        return ['status' => 200, 'response' => 'Successfully updated building'];

        // } catch (\Exception $e) {
        //     DB::rollback();
        //     return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        // }

    }

    public function updateBuildingUnitsDetails(Request $request)
    {
        $rules = [
            'unit_id'        => 'required',
            'building_id'    => 'required'
        ];
        $messages = [
            'unit_id.required' => 'Unit id is required',
            'building_id.required' => 'Building id is required'
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }

        $agentId                        = Auth::user()->id;
        $property_id                    = $request->building_id;

        $verfication_property   = PendingPropertyForVerification::where('property_id', $property_id)
            ->where('agent_id', $agentId)
            ->first();

        $property               = OwnerProperty::select('owner_properties.*')
            ->where('owner_properties.id', $property_id);
        if (!$verfication_property) {
            $property           =   $property->join('agent_properties', 'agent_properties.property_id', 'owner_properties.id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1)
                ->first();
        } else {
            $property           =   $property->join('pending_property_for_verifications', 'pending_property_for_verifications.property_id', 'owner_properties.id')
                ->where('pending_property_for_verifications.agent_id', $agentId)
                // ->where('pending_property_for_verifications.status',1)
                ->first();
        }

        if (!$property) {
            return response()->json(['status' => 400, 'response' => "Invalid property"]);
        }

        $unit_id = $request->unit_id;

        $unit_details = OwnerProperty::where('id', $unit_id)->first();


        $unit_details->furnished            = $request->furnished;
        $unit_details->occupied             = $request->occupied;
        if ($request->property_name) {
            $unit_details->property_name        = $request->property_name;
        }
        if ($request->frequency) {
            $unit_details->frequency            = $request->frequency;
        }
        if ($request->expected_amount) {
            $unit_details->expected_amount      = $request->expected_amount;
        }
        if ($request->description) {
            $unit_details->description      = $request->description;
        }
        // if($request->type_id){
        //     $unit_details->type_id              = $request->type_id;
        // }

        try {
            DB::beginTransaction();
            $property->save();
            $savedoc = array();
            $savefloor = array();
            $savevideo = array();
            $amenities = array();
            if ($request->file('images')) {
                // OwnerPropertyDocument::where('property_id',$unit_id)->where('type','0')->delete();
                foreach ($request->file('images') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/images'), $imagename);
                    $savedoc[$key]['property_id']   = $unit_id;
                    $savedoc[$key]['document']      = '/uploads/property/images/' . $imagename;
                    $savedoc[$key]['type']          = '0';
                    $savedoc[$key]['owner_id']      = $unit_details->owner_id;
                    $savedoc[$key]['created_at']    = now();
                    $savedoc[$key]['updated_at']    = now();
                }
            }
            OwnerPropertyDocument::insert($savedoc);
            if ($request->file('floor_plans')) {
                // OwnerPropertyDocument::where('property_id',$unit_id)->where('type','2')->delete();
                foreach ($request->file('floor_plans') as $key => $row) {
                    $imagename = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/floor_plans'), $imagename);
                    $savefloor[$key]['property_id'] = $unit_id;
                    $savefloor[$key]['document']    = '/uploads/property/floor_plans/' . $imagename;
                    $savefloor[$key]['type']        = '2';
                    $savefloor[$key]['owner_id']    = $unit_details->owner_id;
                    $savefloor[$key]['created_at']  = now();
                    $savefloor[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savefloor);
            }
            if ($request->file('videos')) {
                // OwnerPropertyDocument::where('property_id',$property_id)->where('type','1')->delete();
                foreach ($request->file('videos') as $key => $row) {
                    $videoname = time() . rand() . '.' . $row->getClientOriginalExtension();
                    $row->move(public_path('/uploads/property/videos'), $videoname);
                    $savevideo[$key]['property_id'] = $unit_id;
                    $savevideo[$key]['document']    = '/uploads/property/videos/' . $videoname;
                    $savevideo[$key]['type']        = '1';
                    $savevideo[$key]['owner_id']    = $unit_details->owner_id;
                    $savevideo[$key]['created_at']  = now();
                    $savevideo[$key]['updated_at']  = now();
                }
                OwnerPropertyDocument::insert($savevideo);
            }
            if ($request->amenities) {
                OwnerPropertyAmenity::where('property_id', $unit_id)->delete();
                foreach ($request->amenities as $key => $row) {
                    $category_id = Amenity::where('id', $row)->pluck('amenity_category_id');
                    foreach ($amenities as $row6) {
                        if ($row6['property_id'] == $unit_details->id && $row6['amenity_id'] == $row && $row6['amenities_category_id'] == $category_id[0] && $row6['amenities_category_id'] == Auth::user()->id) {
                            DB::rollback();
                            return ['status' => 400, 'response' => 'Something went wrong'];
                        }
                    }
                    $amenities[$key]['property_id']             = $unit_id;
                    $amenities[$key]['amenity_id']              = $row;
                    $amenities[$key]['amenities_category_id']   = $category_id[0];
                    $amenities[$key]['owner_id']                = $unit_details->owner_id;
                    $amenities[$key]['created_at']              = now();
                    $amenities[$key]['updated_at']              = now();
                }
                OwnerPropertyAmenity::insert($amenities);
            }
            if ($request->detailskey) {
                $max            = sizeof($request->detailskey);
                $detailsvalue   = $request->detailsvalue;
                $detailkeyvalue = $request->detailskey;
                OwnerPropertyDetail::where('property_id', $unit_id)->delete();
                for ($i = 0; $i < $max; $i++) {
                    if ($request->detailskey[$i] != "") {
                        //dd($detailkeyvalue[$i]);
                        $details               = new OwnerPropertyDetail();
                        $details['property_id'] = $unit_id;
                        $details['detail_id']  = $detailkeyvalue[$i];
                        $details['value']      = $detailsvalue[$i];
                        $details['owner_id']   = $unit_details->owner_id;
                        $details['created_at'] = now();
                        $details['updated_at'] = now();
                        $details->save();
                    }
                }
            }

            return ['status' => 200, 'response' => 'Successfully updated building unit'];
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public function propertyUnitDetails(Request $request)
    {
        $agentId        = Auth::user()->id;
        $base_url       = url('/');
        $property_id    = $request->property_id;
        $building_id    = $request->building_id;
        $verfication_property    = PendingPropertyForVerification::where('property_id', $building_id)
            ->where('agent_id', $agentId)
            ->first();

        $properties     = OwnerProperty::select(
            'owner_properties.id',
            'property_reg_no',
            'property_name',
            DB::raw('ifnull(property_type,"") AS property_type'),
            'owner_id',
            'rating',
            'property_to',
            'category',
            'selling_price',
            'mrp',
            'is_featured',
            'furnished',
            'longitude',
            'latitude',
            'rent',
            'city',
            'state',
            'country',
            'description',
            'occupied',
            'zip_code',
            'street_address_1',
            'street_address_2',
            DB::raw('ifnull(type_id,0) as type_id'),
            'expected_amount',
            DB::raw('ifnull(is_builder,2) AS is_builder')
        );
        $properties     = $properties->with(
            array(
                'documents' => function ($query) use ($base_url) {
                    $query->select(DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'id', 'property_id', 'type', 'owner_id');
                },
                'amenity_details' => function ($query) {
                    $query->select('id', 'property_id', 'amenity_id', 'owner_id', 'amenities_category_id');
                },
                'property_details' => function ($query) {
                    $query->select('property_id', 'detail_id', 'value', 'name');
                },
                'owner_rel' => function ($query) use ($base_url) {
                    $query->select('id', 'name', 'phone', DB::raw('CONCAT("' . $base_url . '", profile_image) AS profile_image'));
                },
                'country_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'state_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'city_rel' => function ($query) {
                    $query->select('id', 'name');
                },
                'zipcode_rel' => function ($query) {
                    $query->select('id', 'pincode', 'city');
                },
                'floor_plans' => function ($query) use ($base_url) {
                    $query->select('id', 'property_id', DB::raw('CONCAT("' . $base_url . '", document) AS document_image'), 'owner_id');
                }
            )
        );

        if (!$verfication_property) {
            $properties         =   $properties->join('agent_properties', 'agent_properties.property_id', 'owner_properties.builder_id')
                ->where('agent_properties.agent_id', $agentId)
                ->where('agent_properties.status', 1);
        }
        $properties         =   $properties->where('owner_properties.id', $property_id)
            ->where('owner_properties.builder_id', $building_id)
            ->first();
        if ($properties) {
            $properties     = $this->setToNull('property', $properties);

            $details          = Detail::select('id', 'name')->get();
            $amenity_category = AmenitiesCategory::select('id', 'name')->orderBy('sort_order', 'ASC')->get();

            $prop = $properties['property'];
            if (!$prop->owner_rel) {
                $prop->owner_rel = (object)[];
            }
            if (!$prop->country_rel) {
                $prop->country_rel = (object)[];
            }
            if (!$prop->state_rel) {
                $prop->state_rel = (object)[];
            }
            if (!$prop->city_rel) {
                $prop->city_rel = (object)[];
            }
            if (!$prop->zipcode_rel) {
                $prop->zipcode_rel = (object)[];
            }
            if (!$prop->floor_plans) {
                $prop->floor_plans = (object)[];
            }

            if ($prop->property_details) {
                $new_property = [];
                foreach ($details as $key => $each_detail) {
                    $count = 0;
                    foreach ($prop->property_details as $key => $detail_array) {
                        if ($each_detail->name == $detail_array->name) {
                            $count = 1;
                            $type = $detail_array->name;
                            $prop->$type = $detail_array->value;
                            break;
                        }
                    }
                    if ($count != 1) {
                        $detail_data = $each_detail->name;
                        $prop->$detail_data = 0;
                    }
                }
            }

            if ($prop->amenity_details) {
                $new_property = [];
                $assigned_amenities   = [];
                foreach ($amenity_category as $key => $each_category) {
                    $count = 0;
                    $property_amenities = [];
                    foreach ($prop->amenity_details as $key_new => $each_category_detail) {
                        if ($each_category->id == $each_category_detail->amenities_category_id) {

                            $type = $each_category->name;

                            $amenities = OwnerPropertyAmenity::select('amenities.id', 'amenities.name', DB::raw('CONCAT("' . $base_url . '", amenities.image) AS image'))
                                ->join('amenities', 'amenities.id', 'owner_property_amenities.amenity_id')
                                // ->where('id',$each_category_detail->amenity_id)
                                ->where('owner_property_amenities.amenities_category_id', $each_category->id)
                                ->where('owner_property_amenities.property_id', $property_id)
                                ->get();
                            $encoded_amenities = json_encode($amenities);
                            $decoded_amenities = json_decode($encoded_amenities);
                            foreach ($decoded_amenities as $key => $file_data) {
                                $property_amenities[] = $file_data;
                            }
                            $count = 1;
                            break;
                        }
                    }
                    if ($count == 1) {
                        if (count($property_amenities) > 0) {
                            $amenities_array = [
                                'id'         => $each_category->id,
                                'name'       => $each_category->name,
                                'amenity_details' => $property_amenities
                            ];
                            $assigned_amenities[]  = $amenities_array;
                            // $prop->amenity_categories = $amenities_array;

                        }
                    }
                }
                $prop->amenity_categories   = $assigned_amenities;
            }
            // unset($prop->property_details);
            unset($prop->amenity_details);

            // count of feedback as rating
            $prop->total_rating_count = UserPropertyRating::where('property_id', $property_id)->count();
        } else {
            $properties['property'] = (object)[];
        }

        return response()->json(['status' => 200, 'response' => $properties]);
    }



    public function fetchPendingAmount(Request $request)
    {
        $rules = [
            'tour_id'               => 'required',
            'user_property_id'      => 'required',
        ];
        $messages = [
            'tour_id.required'          => 'Tour is required',
            'user_property_id.required' => 'User property id is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            $tour_id            = $request->tour_id;
            $user_property_id   = $request->user_property_id;
            $agent_id           = Auth::user()->id;

            $base_url           = url('/');
            $agent_tour         = AgentTrackingUserTour::where('agent_id', $agent_id)
                ->where('tour_id', $tour_id)
                ->where('user_property', $user_property_id)
                ->where('status', '!=', 6)
                ->first();

            if (!$agent_tour) {
                return response()->json(['status' => 400, 'response' => "Invalid Booking"]);
            }

            $user_property      = UserProperty::select(DB::raw('ifnull(rent,"") as pending_amount'), 'id as user_property_id',)->where('id', $user_property_id)->first();


            return response()->json(['status' => 200, 'data' => $user_property]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }

    public   function userPropertyFullAmountPayment(Request $request)
    {
        $rules = [
            'tour_id'           => 'required',
            'pending_amount'   => 'required',
            'document_status'   => 'required',
            'document'          => 'required|mimes:jpeg,png,pdf,doc,docx',
        ];
        $messages = [
            'tour_id.required' => 'Tour id is required',
            'document_status.required' => 'Payment method is required',
            'document.required' => 'Document is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        try {
            DB::beginTransaction();
            $tour_id            = $request->tour_id;
            $agent_id           = Auth::user()->id;

            $agent_tour         = AgentTrackingUserTour::where('agent_id', $agent_id)
                ->where('tour_id', $tour_id)
                ->where('status', '!=', 6)
                ->first();


            if (!empty($agent_tour)) {

                if (!$agent_tour->user_property) {

                    return response()->json(['status' => 400, 'response' => "Invalid Booking"]);
                }
                $user_tour  = BookUserTour::where('id', $agent_tour->tour_id)->first();
                if (!$user_tour) {
                    return response()->json(['status' => 400, 'response' => "Invalid Booking"]);
                }
                $already_uploaded = UserPropertyFullAmountDocument::where('user_property_id', $agent_tour->user_property)->first();
                if (empty($already_uploaded)) {
                    if ($request->file('document')) {
                        $file = $request->file('document');
                        $filename = time() . rand() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/users/bill-document'), $filename);

                        $book_doc                  = new UserPropertyFullAmountDocument();
                        $book_doc->document        = '/uploads/users/bill-document/' . $filename;
                        $book_doc->user_id         = $user_tour->user_id;
                        $book_doc->user_property_id = $agent_tour->user_property;
                        $book_doc->document_status = $request->document_status;
                        $book_doc->save();


                        if ($request->document_status == 2) {
                            if ($request->pending_amount) {
                                $pending_amount         = $request->pending_amount;
                                $agent_cash                 = new AgentCashInHand();
                                $agent_cash->agent_id       = $agent_id;
                                $agent_cash->user_id        = $user_tour->user_id;
                                $agent_cash->property_id    = $user_tour->property_id;
                                $agent_cash->payment_id     = $book_doc->id;
                                $agent_cash->payment_amount = $pending_amount;
                                $agent_cash->payment_type   = 1; // agent paid for user property full amount
                                $agent_cash->status         = 1; // cash in hand status
                                $agent_cash->save();
                            }
                        }

                        DB::commit();
                        return response()->json(['status' => 200, 'response' => 'File Upload Successful']);
                    }
                } else {
                    return response()->json(['status' => 200, 'response' => 'File already updated']);
                }
            } else {
                return response()->json(['status' => 401, 'response' => 'Invalid Booking']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 500, 'response' => 'Internal Server Error']);
        }
    }
}
