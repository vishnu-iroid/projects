<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use Hash;
use Auth;
use DB;
use Mail;

//service
use App\Http\Services\OwnerService;
use App\Http\Services\api\owner\PropertyService;
//models
use App\Models\State;
use App\Models\City;
use App\Models\Owner;
use App\Models\Agent;
use App\Models\Country;
use App\Models\Pincode;
use App\Models\PropertyType;
use App\Models\Service;
use App\Models\FaqDetails;
use App\Models\PrivacyPolicy;
use App\Models\AboutUs;
use App\Models\LegalModel;
use Illuminate\Support\Facades\URL;

class ApiController extends Controller
{
    public function Login(Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required',
            'type'      => 'required'
        ];
        $messages = [
            'email.required'    => 'Email is required',
            'password.required' => 'Password is required',
            'type.required'     => 'Type is required'
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        if ($request->type == 1) {
            $data           = Owner::select('id', 'name', 'email', 'phone', 'profile_image', 'status', 'password')->where('email', $request->email)->first();
        } else {
            $data           = Agent::select('id', 'name', 'email', 'phone', 'image as profile_image', 'status', 'password')->where('email', $request->email)->first();
        }
        if ($data) {
            if ($data->status != 1) {
                return response()->json(['status' => 400, 'response' => "Account is inactive/blocked"]);
            }
            if (Hash::check($request->password, $data->password)) {
                Session::put('id', $data->id);
                Session::put('name', $data->name);
                Session::put('email', $data->email);
                Session::put('role', $request->type);
                $token = $data->createToken('ownertoken')->accessToken;
                $data->api_token = $token;
                $data->save();
                $data['profile_image'] = URL::to('/') . $data->profile_image;
                $data->type = $request->type;
                return response()->json(['status' => 200, 'response' => "Logged in successfully", 'data' => $data]);
            }
            return response()->json(['status' => 400, 'response' => "Invalid password"]);
        } else {
            return response()->json(['status' => 401, 'response' => "Invalid email"]);
        }
    }
    public function countries()
    {
        $country = Country::select('id', 'name')->get();
        return response()->json(['status' => 200, 'response' => $country]);
    }
    public function states(Request $request)
    {
        if($request->country){
            $states = State::select('id', 'name', 'country')->where('country',$request->country)->get();
        }else{
            $states = State::select('id', 'name', 'country')->get();
        }
        return response()->json(['status' => 200, 'response' => $states]);
    }

    public function zipcodes(Request $request)
    {
        $zipcodes = Pincode::select('id', 'pincode')
            ->where('city', $request->city)
            ->get();
        return response()->json(['status' => 200, 'response' => $zipcodes]);
    }
    public function getAvailableAmenities(Request $request)
    {
        if (!$request->typeId) {
            return response()->json(['status' => 400, 'response' => 'Someting went wrong']);
        }
        $data = (new OwnerService())->amenitiesAndDetails($request->typeId);
        return response()->json(['status' => 200, 'response' => $data]);
    }
    public function types()
    {
        $types = PropertyType::select('id', 'type')->get();
        return response()->json(['status' => 200, 'response' => $types]);
    }
    public function maintenance()
    {
        $base_url   = url('/');
        $services = Service::select('id', 'service', DB::raw('CONCAT("' . $base_url . '",image) AS image'))->get();
        return response()->json(['status' => 200, 'response' => $services]);
    }

    public function getfaq()
    {
        $faq = FaqDetails::select('english_question', 'english_answer')->get();
        return response()->json(['status' => 200, 'response' => $faq]);
    }

    public function getabout()
    {
        $about = AboutUs::select('image', 'english_description')->get();
        return response()->json(['status' => 200, 'response' => $about]);
    }

    public function getprivacy(Request $request, $lang)
    {
       // $lang = $request->lang;
        if ($lang == 1) {
            $privacy = PrivacyPolicy::select('english_description AS description')->first();
        } else {
            $privacy = PrivacyPolicy::select('arabic_description AS description')->first();
        }
       // $privacy = PrivacyPolicy::select('english_description')->get();
        return response()->json(['status' => 200, 'response' => $privacy]);
    }

    public function getterms($lang)
    {
        if ($lang == 1) {
            $terms = LegalModel::select('english_description AS description')->first();
        } else {
            $terms = LegalModel::select('arabic_description AS description')->first();
        }
       // $terms = PrivacyPolicy::select('english_description')->get();
        return response()->json(['status' => 200, 'response' => $terms]);
    }

    public function sendResetPasswordMail(Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'user_type' => 'required',
        ];

        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>400,'response'=>$validation->errors()->first()]);
        }
        try{
            //generate password
            $password_gen       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $str_length         = strlen($password_gen);

            $pass               = '';
            for ($i = 0; $i < 12; $i++) {
                $n = rand(0, $str_length - 1);
                $pass .= $password_gen[$n];
            }

            $new_password       = $pass;


            if($request->user_type == 1){
                $user              = Owner::where('email',$request->email)->first();
            }elseif($request->user_type == 2){
                $user              = Agent::where('email',$request->email)->first();
            }else{
                return response()->json(['status'=>400,'response'=>'Invalid Email']);
            }

            if($user){
                $user->password    = Hash::make($new_password);
                $user->save();
            }else{
                return response()->json(['status'=>400,'response'=>'Invalid Email']);
            }


            $details = [
                'title'     => 'Mail from Siaaha',
                'body'      =>  $new_password,
            ];

            \Mail::to($request->email)->send(new \App\Mail\ForgotPasswordMail($details));

            return response()->json(['status'=>200,'response'=>'New password successfully sent to your mail']);
        }catch(\Exception $e){
            return response()->json(['status'=>500,'response'=>'Internal Server Error']);
        }
    }
}
