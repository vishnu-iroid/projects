<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Country;
use Illuminate\Http\Request;
use Validator;
use DB;
use DataTables;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['companies'] = Company::latest()->get();
        $data['countries'] = Country::latest()->get();
        if ($request->ajax()) {
            return Datatables::of($data['companies'])
                ->addIndexColumn()
          
                ->addColumn('actions', function($row){
                    $actionBtn = '<a href="' . route('viewCompany', $row->id) . '" class="edit-company btn btn-success btn-sm">Edit</a> 
                    <a href="javascript:void(0)" class="delete btn btn-danger btn-sm"  onclick="deleteFunction('.$row->id.')" title="delete">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['actions'])
                ->make(true);
        }
        return view('admin.company.companyList', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $rules = [
            'logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'company' => 'required',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'email' => 'required|email|unique:companies',
                'alternativePhone'=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
      ];

      $validator = Validator::make($request->all(), $rules);
      if ($validator->passes()) {
            try {
                  $result = null;
                  DB::transaction(function () use ($request, &$result) {
                        $company = new Company();
                        $company->name = $request->company;
                        $company->email = $request->email;
                        $company->phone  = $request->phone;
                        $company->country_code  = $request->countryCode;
                        $company->address = $request->address;
                        $company->contact_person = $request->alternativePhone;
                        $company->website  = $request->website;
                        $company->register_no  = $request->registeration_no;
                        $company->vat  = $request->vat;
                        $company->other_info = $request->other_info;
                        $exid  = 000;
                        if ($request->hasfile('logo')) 
                        {
                              $file =  $request->file('logo');
                              $random_pass  = mt_rand(1000000000, 9999999999);
                              $id_file_name = '';
                              if ($request->file('logo')) {
                                $path = public_path('uploads/company/logo/');
                                $file          = $request->file('logo');
                                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                                $file_name     = 'uploads/company/logo/' . $id_file_name;
                                $file->move($path, $id_file_name);
                            }
                            $company->logo        = $file_name;
                             
                        }
                        $company->save();
                      
                  });

                  
                  return response()->json(['status' => true, 'response' => 'Successfully added Company']);
            } catch (\Illuminate\Database\QueryException $e) {
                  return response()->json(['databaseerror' => $e->errorInfo]);
            }
      }

      return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $company=Company::find($id);
       $countries=Country::get();
       $data=[
           'company'=>$company,
           'countries'=>$countries
       ];
       return view('admin.company.companyView',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
     {
        $id=$request->companyId;
        $rules = [];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
              try {
                    $result = null;

                    DB::transaction(function () use ($request, &$result, $id) {
                          $company = Company::find($id);
                          $company->name = $request->company;
                        $company->email = $request->email;
                        $company->phone  = $request->phone;
                        $company->country_code  = $request->countryCode;
                        $company->address = $request->address;
                        $company->contact_person = $request->alternativePhone;
                        $company->website  = $request->website;
                        $company->register_no  = $request->registeration_no;
                        $company->vat  = $request->vat;
                        $company->other_info = $request->other_info;
                        $exid  = 000;
                        if ($request->hasfile('logo')) 
                        {
                              $file =  $request->file('logo');
                              $random_pass  = mt_rand(1000000000, 9999999999);
                              $id_file_name = '';
                              if ($request->file('logo')) {
                                $path = public_path('uploads/company/logo/');
                                $file          = $request->file('logo');
                                $id_file_name  = $random_pass . '_' . $exid . $file->getClientOriginalName();
                                $file_name     = 'uploads/company/logo/' . $id_file_name;
                                $file->move($path, $id_file_name);
                            }
                            $company->logo        = $file_name;
                             
                        }
                          $company->save();
                        
                    });
                  
                    
                    return response()->json(['status' => true, 'response' => 'Successfully Updated Company'
                ]);
              } catch (\Illuminate\Database\QueryException $e) {
                    return response()->json(['databaseerror' => $e->errorInfo]);
              }
        } 
        return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id){
            $company = Company::find($id); 
            $company->delete();     
            return response()->json(['status'=>true,'response'=>'Successfully Deleted Company']);
        }else{
            return response()->json(['status'=>false,'response'=>'Something went wrong']);
        }
    }
}
