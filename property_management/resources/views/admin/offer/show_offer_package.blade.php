@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Offer Packages</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Offer Package</h4>
                        <p class="card-category">List of all offers</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspPackages</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Description</th>
                            <th class="wd-20p">Start Date</th>
                            <th class="wd-20p">End Date</th>
                            <th class="wd-20p">Status</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($result as $row)
                        <tr>
                            <td>{{ $loop->iteration  }}</td>
                            <td>{{ $row->offer_package_name  }}</td>
                            <td>{{ $row->description  }}</td>
                            <td>{{ $row->start_date  }}</td>
                            <td>{{ $row->end_date  }}</td>
                            <td>
                                @if($row->status == 0)
                                <span class="badge rounded-pill bg-danger text-white"> Blocked </span>
                                @else
                                <span class="badge rounded-pill bg-primary text-white"> Active </span>
                                @endif
                            </td>
                            <td>
                                @if($row->status == 0)
                                <a class="fad fa-unlock-alt tx-20 reject-button" href="javascript:0;" data-admin="" title="Unblock" style="color: green " onclick="blockFunction('{{$row->id}}','1')">
                                </a>
                                @else
                                <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus " href="javascript:0;" data-admin="" title="Block" style="" onclick="blockFunction('{{$row->id}}','0')">
                                </a>
                                @endif
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" onclick="edit({{ $row->id }})" title="Edit"></a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">

                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center tx-primary" id="myModalLabel">Add Offer Package</h4>
            </div>

            <form class="form-admin" id="form-add" action="javascript:;">
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Package Name <span class="tx-danger">*</span></label>
                                        <input type="hidden" id="id" name="id">
                                        <input type="hidden" value="{{ $id }}" id="package_property_id" name="package_property_id">
                                        <input class="form-control" id="name" name="name" type="text" placeholder="Enter Package name" value="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Description <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="description" name="description" type="text" placeholder="Enter Description">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Start Date <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="start_date" name="start_date" type="date" placeholder="Enter Start Date">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>End Date<span class="tx-danger">*</span></label>
                                        <input class="form-control" id="end_date" name="end_date" type="date" placeholder="Enter End Date">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Actual  Amount <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="no_of_days" name="actual_amount" type="number"  value= @if($prop){{  $prop->rent  }} @endif placeholder="Enter Amount">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Discount  Amount <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="no_of_days" name="discount_amount" type="number" placeholder="Enter Amount">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Frequency<span class="tx-danger">*</span></label>
                                        {{-- <select class="form-control select2 zipcode-select " name="frequency_id" id="frequency_id">
                                            <option value="">Select</option>
                                            @foreach($frequency as $row)
                                            <option value="{{ $row->id}}">{{ $row->type}}</option>
                                            @endforeach
                                        </select><br> --}}
                                        <input type="text" name="frequency_name" class="form-control" readonly id="frequency_name" value= @if($prop) @if ($prop->frequency_rel)  {{ $prop->frequency_rel->type }}  @endif @endif readonly>
                                        <input type="hidden" name="frequency_id" id="frequency_id" value= @if($prop) @if ($prop->frequency_rel)  {{ $prop->frequency_rel->id }}  @endif @endif >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="inputFormRow">
                                        <div class="form-group mb-3">
                                            <input type="text" name="title[]" class="form-control m-input" placeholder="Enter Feature" autocomplete="off">
                                            <div class="input-group-append">
                                                <button id="removeRow" type="button" class="btn btn-danger">Remove</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="newRow"></div>
                                    <button id="addRow" type="button" class="btn btn-info">Add Row</button>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();"> Close </button>
                    <button type="submit" class="btn btn-primary addBtn" id="propAddBtn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Inactivation Package</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to Inactive this Package?
                    <input type="hidden" name="packageId" id="packageId">
                    <input type="hidden" name="action" id="action">
                    <!--Action -  1-unblock , 0- block -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class deletetBtn">Inactive</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Offer Package`);
    $("#package_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    function blockFunction(id, action) {


        $('#packageId').val(id);
        $('#action').val(action);
        if (action == 1) {
            $('.reject-modal-title').text('Activate Package');
            $('.deletetBtn').text('Activate');
        }
        $('#rejectModal').modal();
    }
</script>

<script>
    $('.deletetBtn').click(function() {
        var id = $('#packageId').val()
        var action = $('#action').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/offerpackage-deactivation/" + id + '/' + action,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".addBtn").text('Processing..');
            },
            success: function(data) {
                if (action == 0) {
                    $(".deletetBtn").text('Block');
                } else {
                    $(".deletetBtn").text('Unblock');
                }
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })
</script>
<script type="text/javascript">
    // add row
    $("#addRow").click(function() {
        var html = '';
        html += '<div id="inputFormRow">';
        html += '<div class="input-group mb-3">';
        html += '<input type="text" name="title[]" class="form-control m-input" placeholder="Enter Feature" autocomplete="off">';
        html += '<div class="input-group-append">';
        html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
        html += '</div>';
        html += '</div>';

        $('#newRow').append(html);
    });

    // remove row
    $(document).on('click', '#removeRow', function() {
        $(this).closest('#inputFormRow').remove();
    });
</script>
<script>
    function edit(id){

        $.ajax({
            url: url + "/offer/view/" + id,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status === true) {
                    var html = '';
                    $('#name').val(data.response.offer_package_name);
                    $('#description').val(data.response.description);
                    $('#start_date').val(data.response.start_date);
                    $('#end_date').val(data.response.end_date);
                    $('#actual_ampount').val(data.response.actual_amount);
                    $('#discount_amount').val(data.response.discount_amount);
                    $('#frequency_id').val(data.response.frequency_id).trigger("change");
                    $('#tableid').val(data.response.id);

                    if (data.response.package_features.length > 0) {

                        $('#inputFormRow').html('');
                        data.response.package_features.forEach((val, key) => {

                       html += '<div id="inputFormRow">';
                       html += '<div class="input-group mb-3">';
                       html += '<input type="text" name="title[]" class="form-control m-input" value= "' + val.package_feature + '" placeholder="Enter Feature" autocomplete="off">';
                       html += '<div class="input-group-append">';
                       html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
                       html += '</div>';
                       html += '</div>';
                       $('#newRow').append(html);
                    })

                    }

                    $('#addModal').modal()
                } else {
                    toastr["error"](data.response);
                }
            }
        })
    }


    </script>
<script>
    $("#form-add").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            name: {
                required: true,
            },
            description: {
                required: true,
            },
            start_date: {
                required: true,
            },
            end_date: {
                required: true,
            },
            no_of_days: {
                required: true,
            },
            no_of_deduction_days: {
                required: true,
            },


        },
        messages: {
            name: "Name is required",

        },
        submitHandler: function(form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addOfferPackage')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
