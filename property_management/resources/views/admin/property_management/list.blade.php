@extends('admin.layout.app')


<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .pac-container{
        z-index:9999;
    }

    #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    top: 9px !important;
    font-weight: 300;
    margin-left: 10px !important;
    left: 174px !important;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 380px;
    }

    #pac-container {
    padding-bottom: 12px;
    margin-right: 12px;
    }

    .pac-controls {
    display: inline-block;
    padding: 5px 11px;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 500 !important;
    }

    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height: 20px;
        border-radius: 5px !important
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }


    .pac-card {
  background-color: #fff;
  border: 0;
  border-radius: 2px;
  box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
  margin: 10px;
  padding: 0 0.5em;
  font: 400 18px Roboto, Arial, sans-serif;
  overflow: hidden;
  font-family: Roboto;
  padding: 0;
}

#pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
}

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding: 10px;
        border-radius: 10px;
    }

    .error {
        color: red !important;
    }

    .ui-datepicker {
        position: fixed;
        top: 270.797px;
        left: 172.5px;
        z-index: 5001 !important;
        display: block;
    }

    #ui-datepicker-div {
        z-index: 5001 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Property Management</span>
            <span class="breadcrumb-item active">Property List</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                   <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                       <i class="fas fa-plus-square"></i>&nbsp Add Client
                                   </button>
                           </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property List</h4>
                            <p class="card-category">List of all Properties</p>
                        </div>
                        {{-- <div class="pd-5 mg-r-20">
                           <a href="#" class="nav-link card-active tx-white d-flex align-items-center"  data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Owner</a>
                       </div> --}}

                        <div class="pd-5 mg-r-20">
                            <a href="" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal"
                                data-target="#addPropertyModal_property"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd
                                Property</a>
                        </div>

                    </div>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Property</label>
                            {{-- <input type="text" class="form-control" name="property_name" placeholder="Property Name" id="property_name_filter"> --}}
                            <select class="form-control filter " id="property_name_filter">
                                <option selected disabled>Select One</option>
                                @foreach ($property as $t)
                                    <option value="{{ $t->id }}"> {{ $t->property_name }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Catagory</label><select class="form-control filter" id="categoryfilter">
                                <option selected disabled>Select One</option>
                                <option value="2">ALL</option>
                                <option value="3">RESIDENTIAL</option>
                                <option value="1">COMMERICIAL</option>
                            </select>
                        </div>
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Type</label> <select class="form-control filter " id="type_id">
                                <option selected disabled>Select One</option>
                                @foreach ($types as $t)
                                    <option value="{{ $t->id }}"> {{ $t->type }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Owner</label>
                            <select class="form-control filter" id="owner_id">
                                <option selected disabled>Select One</option>
                                <option value="0">All</option>
                                @foreach ($owners as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Status</label>
                            <select class="form-control filter" id="status">
                                <option selected disabled>Select One</option>
                                <option value="3">All</option>
                                <option value="0">Vacant</option>
                                <option value="1">Occupied</option>
                                <option value="2">Expired</option>

                            </select>
                        </div>
                        {{-- <div id="datatableclient_filter" class="dataTables_filter col-md-3"><center><label>AREA</label><input type="search" class="filter" placeholder="" aria-controls="datatableclient"></div> --}}



                    </div>
                </div>

                <div class="card-body">
                    <table id="datatableproperty" class="table data-table printTable" style="width:100%">

                        <thead>
                            <tr>
                                <th class="">Id</th>
                                {{-- <th class="">Property Code</th> --}}
                                <th class="">Property Name</th>
                                {{-- <th class="">Building Name</th> --}}
                                <th class="">Owner</th>
                                <th class="">Contract start date</th>
                                <th class="">Contract End Date</th>
                                <th class="">Agent</th>
                                <th class="">Status</th>
                                <th class="">Actions</th>
                                {{-- <th class="">Builing</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    {{-- edit property --}}


    <!-- ----owner add modal---- -->
    <div class="modal fade contractUpload" id="addPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Add Multiple Agents</h4>
                </div>
                <form action="javascript:;" id="multiple-agent-form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Agents<span class="tx-danger">*</span></label>
                                    <select class="form-control select2 agent-select" name="agents[]" id="agents" multiple>
                                        {{-- <option value="">Select</option> --}}
                                        @foreach ($agent as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select><br>
                                    <label for="agents" class="error"></label>
                                    <input type="hidden" name="property_id" id="property_id">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class addBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- service allocation modal -->

    <div class="modal fade contractUpload" id="addServiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Allocate Multiple Service</h4>
                </div>
                <form action="javascript:;" id="multiple-service-form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Service<span class="tx-danger">*</span></label>
                                    <select class="form-control select2 service-select" name="service[]" id="service"
                                        multiple>
                                        {{-- <option value="">Select</option> --}}
                                        @foreach ($service as $row)
                                            <option value="{{ $row->id }}">{{ $row->service }}</option>
                                        @endforeach
                                    </select><br>
                                    <label for="service" class="error"></label>
                                    <input type="hidden" name="service_property_id" id="service_property_id">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class addBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myModalLabel">Add Offer Package</h4>
                </div>

                <form class="form-admin" id="form-add" action="javascript:;">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Package Name <span class="tx-danger">*</span></label>
                                            <input type="hidden" id="id" name="id">
                                            <input type="hidden" id="package_property_id" name="package_property_id">
                                            <input class="form-control" id="name" name="name" type="text"
                                                placeholder="Enter Package name" value="" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Description <span class="tx-danger">*</span></label>
                                            <input class="form-control" id="description" name="description" type="text"
                                                placeholder="Enter Description">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Start Date <span class="tx-danger">*</span></label>
                                            <input class="form-control" id="start_date" name="start_date" type="date"
                                                placeholder="Enter Start Date">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>End Date<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="end_date" name="end_date" type="date"
                                                placeholder="Enter End Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label> Amount <span class="tx-danger">*</span></label>
                                            <input class="form-control" id="no_of_days" name="no_of_days" type="number"
                                                placeholder="Enter Amount">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Frequency<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 zipcode-select " name="frequency_id"
                                                id="frequency_id">
                                                <option value="">Select</option>
                                                <option value="2">Monthly</option>
                                                <option value="1">Daily</option>

                                            </select><br>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="inputFormRow">
                                            <div class="form-group mb-3">
                                                <input type="text" name="title[]" class="form-control m-input"
                                                    placeholder="Enter Feature" autocomplete="off">
                                                <div class="input-group-append">
                                                    <button id="removeRow" type="button"
                                                        class="btn btn-danger">Remove</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="newRow"></div>
                                        <button id="addRow" type="button" class="btn btn-info">Add Row</button>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();"> Close </button>
                        <button type="submit" class="btn btn-primary addBtn" id="propAddBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Add property model --}}

    <div class="modal fade" id="addPropertyModal_property" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="overflow: hidden">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myModalLabelProperty">Add Property</h4>
                    {{-- <a href="" class="btn btn-primary float-right"  onclick="addOwnerBuilding()">Add as Building</a> --}}
                </div>
                <div class="col-md-12">
                    <div id="tabs">
                        <ul class="nav nav-tabs property-tabs mt-1" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details"
                                    role="tab" aria-controls="details" aria-selected="true">Details</a>
                            </li>
                            <li class="nav-item hide-element">
                                <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab"
                                    aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                            </li>
                            <li class="nav-item hide-element">
                                <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities"
                                    role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contract"
                                    role="tab" aria-controls="contract" aria-selected="false">Contract Details</a>
                            </li>
                            <li class="nav-item hide-element">
                                <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents"
                                    role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                            </li>
                            <li class="nav-item">
                                <a id="location-prop-tab" class="nav-link propertynav" data-toggle="tab"
                                    href="#location-prop" role="tab" aria-controls="location-prop"
                                    aria-selected="false">Location</a>
                            </li>
                        </ul>


                        <form class="form-admin" id="form-add-property" action="javascript:;">
                            <input type="hidden" id="propertyedit_id" name="propertyedit_id">
                            <div class="modal-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="details" role="tabpanel"
                                        aria-labelledby="details-tab">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Property Type<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 " name="category" id="category_id">
                                                        <option value="">select</option>
                                                        <option value="0">Residential</option>
                                                        <option value="1">Commercial</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Category<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 category-select" name="type"
                                                        id="type">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Owners<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 owners-select" name="ownerslist"
                                                        id="ownerslist">
                                                        <option value="">select</option>
                                                        @foreach ($owners as $owner)
                                                            <option value="{{ $owner->id }}">{{ $owner->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="hidden" id="property_owner_id" name="property_owner_id">
                                                <div class="form-group">
                                                    <label>Property For<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2" name="property_to"
                                                        id="property_to">
                                                        <option value="">select</option>
                                                        <option value="0">Rent</option>
                                                        <option value="1">Sell</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Property Name<span class="tx-danger">*</span></label>
                                                    <input class="form-control" id="property_name" name="property_name"
                                                        type="text" placeholder="Enter Property Name">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Property Status<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2" name="property_status"
                                                        id="property_status">
                                                        <option value="">status</option>
                                                        <option value="0">Vacant</option>
                                                        <option value="1">Occupied</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Country<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2" name="property_country"
                                                        id="property_country">
                                                        <option value="">country</option>
                                                        @foreach ($countries as $country)
                                                            <option value="{{ $country->id }}">{{ $country->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>State<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 state-select" name="property_state"
                                                        id="property_state">
                                                        {{-- @foreach ($states as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach --}}
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>City<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 city-select" name="property_city"
                                                        id="property_city_id">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Zipcode<span class="tx-danger">*</span></label>
                                                    <select class="form-control select2 zipcode-select "
                                                        id="property_zipcode" name="property_zipcode">


                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>National Address <span class="tx-danger">*</span></label>
                                                    <textarea class="form-control" id="address1" name="address1"
                                                        type="text" placeholder="Enter address"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Secondary Address</label>
                                                    <textarea class="form-control" id="address2" name="address2"
                                                        type="text" placeholder="Enter address"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Description <span class="tx-danger">*</span></label>
                                                    <textarea class="form-control" id="description" name="description"
                                                        type="text" placeholder="Description"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Individual Property<span class="tx-danger">*</span></label>
                                                    <input type="checkbox" name="individual_building" id="is_builder"
                                                        value="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6 is-building">
                                                <div class="form-group">
                                                    <label>Images <span class="tx-danger">*</span></label><br>
                                                    <input id="building_image" name="building_image[]" type="file"
                                                        class="form-control file" multiple data-show-upload="true"
                                                        data-show-caption="true">
                                                    {{-- <input type="file" class="w-75 form-control" id="editlogo" name="logo"
                                                placeholder="logo" accept="logo/*" onchange="readURL(this);"> --}}
                                                    <input type="hidden" name="hidden_image" id="hidden_image">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row is-appartment">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No. of Units<span class="tx-danger">*</span></label>
                                                    <input class="form-control" id="unit_num" name="unit_num"
                                                        type="number" placeholder="Enter Unit">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No. of floors<span class="tx-danger">*</span></label>
                                                    <input class="form-control" id="floor_num" name="floor_num"
                                                        type="number" placeholder="Enter Floor">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="amenities-row">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Furnished <span class="tx-danger">*</span></label>
                                                        <select class="form-control select2" name="furnished"
                                                            id="furnished">
                                                            <option value="">Select</option>
                                                            <option value="0">Not Furnished</option>
                                                            <option value="1">Semi Furnished</option>
                                                            <option value="2">Fully Furnished</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-warning tx-white mb-3"> &nbspDetails</div>
                                            <div class="row" id="details-section">
                                                @if ($details)
                                                    @foreach ($details as $det)
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>{{ $det->name }}<span
                                                                        class="tx-danger">*</span></label>
                                                                <input type="text" class="form-control"
                                                                    name="detailsdata[{{ $det->detail_id }}]"
                                                                    id="det-{{ $det->detail_id }}"
                                                                    placeholder="{{ $det->placeholder }}">
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>


                                        </div>
                                        {{-- <hr> --}}
                                        <div class="float-right">
                                            <a class="btn btn-primary text-white tabto1">Next</a>
                                        </div><br>
                                    </div>
                                    <!-- end of tab1 -->
                                    <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                                        <div class="rentdetails">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Lease Type<span class="tx-danger">*</span></label>
                                                        <select class="form-control select2 rentfrequency" name="frequency"
                                                            id="frequency">
                                                            <option value="">select</option>
                                                            @foreach ($frequency as $freq)
                                                                <option value="{{ $freq->id }}">{{ $freq->type }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Rent<span class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control" id="min_rent"
                                                            name="min_rent" placeholder="Enter rent">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Security Deposit<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control" name="security_deposit"
                                                            id="security_deposit" placeholder="Enter Security deposit">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sellingdetails">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Min Selling price<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control" id="min_sellingprice"
                                                            name="min_sellingprice"
                                                            placeholder="Enter minimum selling price">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Max Selling price<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control" id="max_sellingprice"
                                                            name="max_sellingprice"
                                                            placeholder="Enter maximum selling price">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="token-amount">
                                            <div class="row">
                                                <div class="col-md-6 token-hold">
                                                    <div class="form-group">
                                                        <label>Token Amount<span class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control" name="token_amount"
                                                            id="token_amount" placeholder="Enter Token amount">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <hr> --}}
                                        <div class="float-right">
                                            <a class="btn btn-primary text-white tabto2">Next</a>
                                        </div>
                                        <br>

                                    </div>
                                    <div class="tab-pane fade" id="amenities" role="tabpanel"
                                        aria-labelledby="amenities-tab">
                                        <div id="amenties_available">
                                            @if ($amenities_listing)
                                                @foreach ($amenities_listing as $row)
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="card-warning tx-white mb-3"> &nbsp
                                                                    {{ $row->name }} </div>
                                                                <div class="row mg-l-1">
                                                                    @foreach ($row->amenities as $row2)
                                                                        <label class="ckbox ml-2">
                                                                            <input type="checkbox"
                                                                                value="{{ $row2->id }}"
                                                                                name="amenities[]" id="amenities_id">
                                                                            <span>{{ $row2->name }}</span>
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="float-right">
                                            <a class="btn btn-primary text-white tabto3">Next</a>
                                        </div><br>

                                    </div>
                                    <!-- end of tab3 -->
                                    <!-- start tab 4 -->
                                    <div class="tab-pane fade" id="contract" role="tabpanel"
                                        aria-labelledby="contract-tab">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Contract No</label>
                                                    <input type="text"
                                                        class="form-control @error('contract_no') is-invalid @enderror"
                                                        name="contract_no" id="contract_no">
                                                    @error('contract_no')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Contract File</label>
                                                    <input type="file" class="form-control" name="contract_file"
                                                        id="contract_file">
                                                    {{-- <input type="file" class="w-75 form-control" id="editlogo" name="logo"
                                                placeholder="logo" accept="logo/*" onchange="readURL(this);"> --}}
                                                    <input type="hidden" name="hidden_image" class="hidden_image">
                                                </div>

                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Ownership Doc<span class="tx-danger">*</span></label>
                                                    <input type="file" class="form-control" name="ownership_doc"
                                                        id="ownership_doc">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Contract Start Date<span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                        name="contract_start_date" id="contract_start_date"
                                                        autocomplete="off">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Contract End Date<span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control fc-datepicker"
                                                        name="contract_end_date" id="contract_end_date" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Commission in terms of :-<span
                                                            class="tx-danger">*</span></label><br>
                                                    <label>Amount</label>
                                                    <input type="radio" class="commission_as" checked name="commision_as"
                                                        id="commission_as" value="amount">
                                                    <label>Percentage</label>
                                                    <input type="radio" class="commission_as" name="commision_as"
                                                        id="commission_as" value="percentage">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Commission<span class="tx-danger"
                                                            style="font-size:12px;">*</span></label>
                                                    <input type="number" class="form-control" name="owner_amount"
                                                        id="owner_amount" placeholder="Enter Commission">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Property Management fee:-<span
                                                            class="tx-danger">*</span></label><br>
                                                    <label>Amount</label>
                                                    <input type="radio" class="property_management_fee_type"
                                                        name="property_management_fee_type"
                                                        id="property_management_fee_type" value="amount" checked>
                                                    <label>Percentage</label>
                                                    <input type="radio" class="property_management_fee_type"
                                                        name="property_management_fee_type"
                                                        id="property_management_fee_type" value="percentage">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Management Fee<span class="tx-danger"
                                                            style="font-size:12px;">*</span></label>
                                                    <input type="number" class="form-control" name="management_fee"
                                                        id="management_fee" placeholder="Enter Management Fee">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Ads Number:-<span class="tx-danger">*</span></label><br>
                                                    <input type="text" class="form-control" name="adsnumber"
                                                        id="adsnumber" placeholder="Enter Ads Number">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Contact Person No<span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control" name="guard_number"
                                                        id="guard_number" placeholder="Enter Contact Person No">
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <hr> --}}
                                        <div class="float-right">
                                            <a class="btn btn-primary text-white tabto4">Next</a>
                                        </div>
                                        <br>
                                    </div>
                                    <!-- end of tab4 -->
                                    <!-- start tab 5 -->
                                    <div class="tab-pane fade" id="documents" role="tabpanel"
                                        aria-labelledby="documents-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Images <span class="tx-danger">*</span></label><br>
                                                    <input id="images" name="images[]" type="file"
                                                        class="form-control file" multiple data-show-upload="true"
                                                        data-show-caption="true">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                                    <input id="floor_plans" name="floor_plans[]" type="file"
                                                        class="form-control file" multiple data-show-upload="true"
                                                        data-show-caption="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Videos <span class="tx-danger"></span></label><br>
                                                    <input id="videos" name="videos[]" type="file"
                                                        class="form-control file" multiple data-show-upload="true"
                                                        data-show-caption="true">
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <hr> --}}
                                        <div class="float-right">
                                            <a class="btn btn-primary text-white tabto5">Next</a>
                                        </div>
                                        <br>


                                    </div>
                                    <div class="tab-pane fade" id="location-prop" role="tabpanel"
                                        aria-labelledby="location-prop-tab">

                                        <div>
                                            <input id="pac-input" class="controls form-control" type="text" placeholder="Search Box" style="z-index:9999;">
                                        </div>

                                        <div class="container" id="map-canvas" style="height:300px;z-index:9995;"></div>

                                        <div>
                                            <input type="hidden" id="lat-prop" name="lat_prop">
                                            <input type="hidden" id="lng-prop" name="lng_prop">
                                        </div>
                                        <br>
                                        {{-- <hr> --}}
                                        <div class="float-right"> <button type="submit"
                                                class="btn btn-primary addBtn nextBtn" id="addPropertyBtn">Submit</button>
                                        </div> <br>



                                    </div>

                                </div>
                                <div class="errorTxt shadow"></div>
                                <div class="modal-footer">

                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>





    @endsection
    @section('scripts')
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

        <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>

        <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
        <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU"></script>



        {{-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> --}}



        <script>
            $(document).on('click', '.tabto1', function() {
                if ($("#is_builder").is(':checked')) {
                    $('#rent-tab').trigger('click');
                } else {
                    $('#contract-tab').trigger('click');
                }
            });

            $(document).on('click', '.tabto2', function() {
                $('#amenities-tab').trigger('click');
            });
            $(document).on('click', '.tabto3', function() {
                $('#contract-tab').trigger('click');
            });
            $(document).on('click', '.tabto4', function() {

                if ($("#is_builder").is(':checked')) {

                    $('#documents-tab').trigger('click');
                } else {
                    $('#location-prop-tab').trigger('click');
                }
            });
            $(document).on('click', '.tabto5', function() {
                $('#location-prop-tab').trigger('click');
            });
            $(document).on('change', '#contract_start_date', function(e) {
                var dt = new Date($("#contract_start_date").val());
                var year = dt.getFullYear();
                var month = dt.getMonth();
                var day = dt.getDate();
                var updateDate = new Date(year + 1, month, day);
                var dd = updateDate.getDate();
                var mm = updateDate.getMonth();
                var y = updateDate.getFullYear();

                var someFormattedDate = mm + '/'+ dd + '/'+ y;
                $('#contract_end_date').val(someFormattedDate);
            });

        </script>
        <script>
            var cityIds, zipids, startTimer;

            $(document).ready(function() {
                $("#site_title").html(`Property | Property Management`);
                $("#property_manage_nav").addClass("active");


                //check contractno
                $('#contract_no').on('keyup', function() {

                    $('#contract_no-error').remove();
                    clearTimeout(startTimer);
                    let contract_no = $(this).val();

                    startTimer = setTimeout(checkContractno, 500, contract_no);
                });



                $('#contract_no').on('keydown', function() {
                    clearTimeout(startTimer);
                });

                function checkContractno(contract_no) {
                    $('#contract_no-error').remove();
                    if (contract_no.length > 1) {
                        $.ajax({
                            type: 'post',
                            url: "{{ route('checkContractno') }}",
                            data: {
                                contract_no: contract_no,
                                _token: "{{ csrf_token() }}"
                            },
                            success: function(data) {

                                if (data.success == false) {
                                    $('#contract_no').after(
                                        '<div id="contract_no-error" class="text-danger" <strong>' +
                                        data.message + '<strong></div>');
                                } else {
                                    $('#contract_no').after(
                                        '<div id="contract_no-error" class="text-success" <strong>' +
                                        data.message + '<strong></div>');
                                }

                            }
                        });
                    } else {
                        $('#contract_no').after(
                            '<div id="phone-error" class="text-danger" <strong>Contract no can not be empty.<strong></div>'
                        );
                    }
                    $('#contract_no-error').remove();

                }

            });

            // for add agent modal
            function addPropertyFunction(id) {
                $('#property_id').val(id);
                //  alert(id);
                $('#addPropertyModal').modal();
                $('#agents').select2();
            }
            // agent modal submittion
            $("#multiple-agent-form").validate({
                normalizer: function(value) {
                    return $.trim(value);
                },
                ignore: [],
                rules: {
                    'agents[]': {
                        required: true,
                    },
                },
                submitHandler: function(form) {
                    var form = document.getElementById("multiple-agent-form");
                    var formData = new FormData(form);
                    $(".addBtn").prop("disabled", true);
                    $(".closeBtn").prop("disabled", true);
                    $.ajax({
                        data: formData,
                        type: "post",
                        url: "{{ route('addPropertyManageagent') }}",
                        processData: false,
                        dataType: "json",
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        async: true,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        },
                        beforeSend: function() {
                            $(".addBtn").text('Processing..');
                            var value = $('#property_management_fee_type').val()
                            console.log(value);
                        },
                        success: function(data) {
                            $(".addBtn").text('Submit');
                            $(".addBtn").prop("disabled", false);
                            $(".closeBtn").prop("disabled", false);
                            if (data.status == 1) {
                                toastr["success"](data.response);
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            } else {
                                var html = "";
                                $.each(data.response, function(key, value) {
                                    html += value + "</br>";
                                });
                                toastr["error"](html);
                            }
                        },
                    });
                },
            });
        </script>
        <script>
            function addPackageFunction(id) {

                $('#package_property_id').val(id);
                $('#addModal').modal();
            }
        </script>
        <script type="text/javascript">
            // add row
            $("#addRow").click(function() {
                var html = '';
                html += '<div id="inputFormRow">';
                html += '<div class="input-group mb-3">';
                html +=
                    '<input type="text" name="title[]" class="form-control m-input" placeholder="Enter Feature" autocomplete="off">';
                html += '<div class="input-group-append">';
                html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
                html += '</div>';
                html += '</div>';

                $('#newRow').append(html);
            });

            // remove row
            $(document).on('click', '#removeRow', function() {
                $(this).closest('#inputFormRow').remove();
            });
        </script>
        <script>
            $("#form-add").validate({
                normalizer: function(value) {
                    return $.trim(value);
                },
                ignore: [],
                rules: {
                    name: {
                        required: true,
                    },
                    description: {
                        required: true,
                    },
                    start_date: {
                        required: true,
                    },
                    end_date: {
                        required: true,
                    },
                    no_of_days: {
                        required: true,
                    },
                    no_of_deduction_days: {
                        required: true,
                    },
                },
                messages: {
                    name: "Name is required",

                },
                submitHandler: function(form) {
                    var form = document.getElementById("form-add");
                    var formData = new FormData(form);
                    $(".addBtn").prop("disabled", true);
                    $(".closeBtn").prop("disabled", true);
                    $.ajax({
                        data: formData,
                        type: "post",
                        url: "{{ route('addOfferPackage') }}",
                        processData: false,
                        dataType: "json",
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        async: true,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        },
                        beforeSend: function() {
                            $(".addBtn").text('Processing..');
                        },
                        success: function(data) {
                            $(".addBtn").text('Submit');
                            $(".addBtn").prop("disabled", false);
                            $(".closeBtn").prop("disabled", false);
                            if (data.status == 1) {
                                toastr["success"](data.response);
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            } else {
                                var html = "";
                                $.each(data.response, function(key, value) {
                                    html += value + "</br>";
                                });
                                toastr["error"](html);
                            }
                        },
                    });
                },
            });
        </script>

        <script>
            // for add service modal
            function addServiceFunction(id) {
                $('#service_property_id').val(id);
                $('#addServiceModal').modal();
            }
            // service modal submission
            $("#multiple-service-form").validate({
                normalizer: function(value) {
                    return $.trim(value);
                },
                ignore: [],
                rules: {
                    'service[]': {
                        required: true,
                    },

                },
                submitHandler: function(form) {
                    var form = document.getElementById("multiple-service-form");
                    var formData = new FormData(form);
                    $(".addBtn").prop("disabled", true);
                    $(".closeBtn").prop("disabled", true);
                    $.ajax({
                        data: formData,
                        type: "post",
                        url: "{{ route('addPropertyManageagentService') }}",
                        processData: false,
                        dataType: "json",
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        async: true,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        },
                        beforeSend: function() {
                            $(".addBtn").text('Processing..');
                        },
                        success: function(data) {
                            $(".addBtn").text('Submit');
                            $(".addBtn").prop("disabled", false);
                            $(".closeBtn").prop("disabled", false);
                            if (data.status == 1) {
                                toastr["success"](data.response);
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            } else {
                                var html = "";
                                $.each(data.response, function(key, value) {
                                    html += value + "</br>";
                                });
                                toastr["error"](html);
                            }
                        },
                    });
                },
            });
        </script>
        <script>
            const url = '{{ url('/') }}';
            $('.sellingdetails').hide()
            $('.hide-element').hide();
            $('.amenities-row').hide();
            $('.errorTxt').hide()
            $('.modalnav').click(function() {
                var id = $(this).attr('id')
                if (id != 'location-tab') {
                    $('#ownerSubmitBtn').attr('disabled', true)
                } else {
                    $('#ownerSubmitBtn').attr('disabled', false)
                }
            })
            $('.propertynav').click(function() {
                var id = $(this).attr('id')
                if (id != 'location-prop-tab') {
                    $('#addPropertyBtn').attr('disabled', true)
                } else {
                    $('#addPropertyBtn').attr('disabled', false)
                }
            })


            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                container: '#contractUploadModal modal-body'
            });

            $('#property_to').change(function() {
                if ($('#property_to').val() == 1) {
                    // buy -> 1 / rent->0
                    $('.rentdetails').hide();
                    $('.sellingdetails').show()

                } else {
                    $('.sellingdetails').hide()
                    $('.rentdetails').show();
                }
            });
            $(document).on("click", "#is_builder", function() {

                var check_val = $(this).prop("checked");
                if (check_val) {
                    $('.hide-element').show();
                    $('.amenities-row').show();
                    $('.is-building').hide();
                    $('.is-appartment').hide();
                } else {
                    $('.hide-element').hide();
                    $('.amenities-row').hide();
                    $('.is-building').show();
                    $('.is-appartment').show();
                }
            });
        </script>
        <script>
            $(document).ready(function(){

                google.maps.event.addDomListener(window, 'load', init);


                function init() {
                    const uluru = {
                    lat: 24.466667,
                    lng: 54.366669
                    };

                if ($('#lat-prop').val() != "" && $('#lng-prop').val() != "") {
                    const uluru = {
                        lat: $('#lat-prop').val(),
                        lng: $('#lng-prop').val()
                    };
                }
                const map = new google.maps.Map(document.getElementById("map-canvas"), {
                    zoom: 10,
                    center: uluru,
                });

                const marker = new google.maps.Marker({
                    position: uluru,
                    map: map,
                    draggable: true
                });
                navigator.geolocation.getCurrentPosition(
                    function(position) { // success cb
                        if ($('#lat-prop').val() == "" && $('#lng-prop').val() == "") {
                            $('#lat-prop').val(parseFloat(position.coords.latitude))
                            $('#lng-prop').val(parseFloat(position.coords.longitude))
                        }
                        var lat = $('#lat-prop').val()
                        var lng = $('#lng-prop').val()
                        map.setCenter(new google.maps.LatLng(lat, lng));
                        var latlng = new google.maps.LatLng(lat, lng);
                        marker.setPosition(latlng);
                    },
                    function() {}
                );

                    var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
                    map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
                    google.maps.event.addListener(searchBox, 'places_changed', function() {
                    searchBox.set('map', null);
                    var places = searchBox.getPlaces();
                    var bounds = new google.maps.LatLngBounds();
                    var i, place;
                    for (i = 0; place = places[i]; i++) {
                        (function(place) {
                        var marker = new google.maps.Marker({

                            position: place.geometry.location
                        });
                        marker.bindTo('map', searchBox, 'map');
                        google.maps.event.addListener(marker, 'map_changed', function() {
                            if (!this.getMap()) {
                            this.unbindAll();
                            $('#lat').val(marker.position.lat())
                            $('#lng').val(marker.position.lng())
                            }

                        });
                        bounds.extend(place.geometry.location);
                        }(place));

                    }
                    map.fitBounds(bounds);
                    searchBox.set('map', map);
                    map.setZoom(Math.min(map.getZoom(),12));

                    });
                }
            })
        </script>

        <script>

            // function initMap() {
            //     const uluru = {
            //         lat: 24.466667,
            //         lng: 54.366669
            //     };
            //     if ($('#lat').val() != "" && $('#lng').val() != "") {
            //         const uluru = {
            //             lat: $('#lat').val(),
            //             lng: $('#lng').val()
            //         };
            //     }

            //     const map = new google.maps.Map(document.getElementById("map"), {
            //         zoom: 10,
            //         center: uluru,
            //     });
            //     const marker = new google.maps.Marker({
            //         position: uluru,
            //         map: map,
            //         draggable: true
            //     });
            //     // navigator.geolocation.getCurrentPosition(
            //     //     function(position) { // success cb
            //     //         if ($('#lat').val() == "" && $('#lng').val() == "") {
            //     //             $('#lat').val(parseFloat(position.coords.latitude))
            //     //             $('#lng').val(parseFloat(position.coords.longitude))
            //     //         }
            //     //         var lat = $('#lat').val()
            //     //         var lng = $('#lng').val()
            //     //         map.setCenter(new google.maps.LatLng(lat, lng));
            //     //         var latlng = new google.maps.LatLng(lat, lng);
            //     //         marker.setPosition(latlng);
            //     //     },
            //     //     function() {}
            //     // );
            //     google.maps.event.addListener(marker, 'dragend', function() {
            //         $('#lat').val(marker.position.lat())
            //         $('#lng').val(marker.position.lng())
            //     })
            // }

            // // Initialize and add the map
            // function initMap() {
            // // The location of Uluru
            // const uluru = { lat: 24.466667, lng: 54.366669 };
            // // The map, centered at Uluru
            // const map = new google.maps.Map(document.getElementById("map"), {
            //     zoom: 4,
            //     center: uluru,
            // });
            // // The marker, positioned at Uluru
            // const marker = new google.maps.Marker({
            //     position: uluru,
            //     map: map,
            // });
            // }

            // function initPropertyMap() {
            //     const uluru = {
            //         lat: 24.466667,
            //         lng: 54.366669
            //     };
            //     console.log('lat', $('#lat-prop').val())
            //     if ($('#lat-prop').val() != "" && $('#lng-prop').val() != "") {
            //         const uluru = {
            //             lat: $('#lat-prop').val(),
            //             lng: $('#lng-prop').val()
            //         };
            //     }
            //     const map = new google.maps.Map(document.getElementById("map-2"), {
            //         zoom: 10,
            //         center: uluru,
            //     });
            //     const marker = new google.maps.Marker({
            //         position: uluru,
            //         map: map,
            //         draggable: true
            //     });
            //     navigator.geolocation.getCurrentPosition(
            //         function(position) { // success cb
            //             if ($('#lat-prop').val() == "" && $('#lng-prop').val() == "") {
            //                 $('#lat-prop').val(parseFloat(position.coords.latitude))
            //                 $('#lng-prop').val(parseFloat(position.coords.longitude))
            //             }
            //             var lat = $('#lat-prop').val()
            //             var lng = $('#lng-prop').val()
            //             map.setCenter(new google.maps.LatLng(lat, lng));
            //             var latlng = new google.maps.LatLng(lat, lng);
            //             marker.setPosition(latlng);
            //         },
            //         function() {}
            //     );
            //     google.maps.event.addListener(marker, 'dragend', function() {
            //         $('#lat-prop').val(marker.position.lat())
            //         $('#lng-prop').val(marker.position.lng())
            //     })
            // }
        </script>


        <script>
            $('#property_country').change(function() {
                var countryId = $(this).val()
                //  alert(countryId);
                $.ajax({
                    url: url + "/get-states/" + countryId,
                    type: "GET",
                    processData: false,
                    async: true,
                    header: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        $('.state-select').html('')
                        var html = '<option value=""></option>';
                        if (data.status === true) {
                            if (data.response.length > 0) {
                                data.response.forEach((val, key) => {
                                    html += `<option value=` + val.id + `>` + val.name + `</option>`
                                })
                                $('.state-select').append(html);

                            } else {
                                $('.state-select').html('')
                            }
                        } else {
                            $('.state-select').html('')
                        }
                    }
                });
                return false
            });
            $('.state-select').change(function() {
                var stateId = $(this).val()
                $.ajax({
                    url: url + "/get-cities/" + stateId,
                    type: "GET",
                    processData: false,
                    async: true,
                    header: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        $('.city-select').html('')
                        var html = '<option value=""></option>';
                        if (data.status === true) {
                            if (data.response.length > 0) {
                                data.response.forEach((val, key) => {
                                    html += `<option value=` + val.id + `>` + val.name + `</option>`
                                })
                                $('.city-select').append(html);

                            } else {
                                $('.city-select').html('')
                            }
                        } else {
                            $('.city-select').html('')
                        }
                    }
                });
                return false
            });
        </script>

        <script>
            $('#filtercategory').change(function() {
                var categoryId = $(this).val();
                console.log(categoryId);
                categoryTypeFilter(categoryId);
            });


            $(document).ready(function() {
                categoryTypeFilter(1);
            });

            function categoryTypeFilter(categoryId) {
                $.ajax({
                    url: url + "/get-type/" + categoryId,
                    type: "GET",
                    processData: false,
                    async: true,
                    header: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        $('.category-select').html('')
                        var html = '<option value=""></option>';
                        if (data.status === true) {

                            if (data.response.length > 0) {
                                data.response.forEach((val, key) => {
                                    html += `<option value=` + val.id + `>` + val.type + `</option>`
                                })
                                $('.category-select').append(html)
                            } else {
                                $('.v-select').html('')
                            }
                        } else {
                            $('.category-select').html('')
                        }
                    }
                })
                return false
            }
        </script>
        <script>
            $('.city-select').change(function() {
                var cityId = $(this).val()
                if (cityId != null) {
                    $.ajax({
                        url: url + "/get-pincodes/" + cityId,
                        type: "GET",
                        processData: false,
                        async: true,
                        header: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            //  $('.zipcode-select').html('')
                            var html = '<option value=""></option>';
                            if (data.status === true) {
                                if (data.response.length > 0) {
                                    data.response.forEach((val, key) => {
                                        html += `<option value=` + val.id + `>` + val.pincode +
                                            `</option>`
                                    })
                                    $('.zipcode-select').append(html)
                                } else {
                                    $('.zipcode-select').html('')
                                }
                            } else {
                                $('.zipcode-select').html('')
                            }
                        }
                    });
                }
                return false
            })
            $('#type').change(function() {
                var id = $(this).val()
                $.ajax({
                    url: url + "/owner/available-amenities/" + id,
                    method: 'GET',
                    async: true,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == true) {
                            $('#amenties_available').html('')
                            $('#amenties_available').append(data.response)
                        } else {
                            toastr['error'](data.response)
                        }
                    },
                })
                return false
            })

            $("#form-add").validate({
                normalizer: function(value) {
                    return $.trim(value);
                },
                ignore: [],
                rules: {
                    name: {
                        required: true,

                    },
                    country: {
                        required: true,
                    },
                    state: {
                        required: true,
                    },
                    city: {
                        required: true,
                    },
                    zipcode: {
                        required: true,
                    },
                    address: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    phone: {
                        required: true,
                        maxlength: 10,
                    },
                    status: {
                        required: true,
                    },
                    account_number: {
                        required: true,
                    },
                    branch: {
                        required: true,
                    },
                    ifsc: {
                        required: true,
                    },

                },
                submitHandler: function(form) {
                    var form = document.getElementById("form-add");
                    var formData = new FormData(form);
                    $(".addBtn").prop("disabled", true);
                    $(".closeBtn").prop("disabled", true);
                    $.ajax({
                        data: formData,
                        type: "post",
                        url: "{{ route('addOwner') }}",
                        processData: false,
                        dataType: "json",
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        async: true,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        },
                        beforeSend: function() {
                            $(".addBtn").text('Processing..');
                        },
                        success: function(data) {
                            $(".addBtn").text('Submit');
                            $(".addBtn").prop("disabled", false);
                            $(".closeBtn").prop("disabled", false);
                            if (data.status == 1) {
                                toastr["success"](data.response);
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            } else {
                                var html = "";
                                $.each(data.response, function(key, value) {
                                    html += value + "</br>";
                                });
                                toastr["error"](html);
                            }
                        },
                    });
                },
            });

            $('#form-add-property').validate({
                normalizer: function(value) {
                    return $.trim(value);
                },
                ignore: [],
                rules: {
                    property_name: {
                        required: true,
                    },
                    property_to: {
                        required: true,
                    },
                    category: {
                        required: true
                    },
                    property_country: {
                        required: true,
                    },
                    property_state: {
                        required: true,
                    },
                    property_city: {
                        required: true,
                    },
                    property_zipcode: {
                        required: true,
                    },
                    address1: {
                        required: true,
                    },
                    property_status: {
                        required: true,
                    },
                    // furnished : {
                    //     required : true,
                    // },
                    is_builder: {
                        required: function() {
                            if ($('#is_builder').prop(":checked")) {
                                return true;
                                // min_sellingprice : {
                                //     required : function (){
                                //         if($('#property_to').val() == 1){
                                //             return true
                                //         }
                                //         return false
                                //     },
                                // },
                                // max_sellingprice : {
                                //     required : function (){
                                //         if($('#property_to').val() == 1){
                                //             return true
                                //         }
                                //         return false
                                //     },
                                // },
                                // 'amenities[]' : {
                                //     required :true
                                // },
                                // 'images[]' : {
                                //     required : true,
                                // },
                                // 'detailsdata[]' : {
                                //     required : true,
                                // }

                                // min_rent : {
                                //     required : function (){
                                //         if($('#property_to').val() == 0){
                                //             return true
                                //         }
                                //         return false
                                //     },
                                // },
                                // token_amount : {
                                //     required : true,
                                // },

                                // security_deposit : {
                                //     required : function (){
                                //         if($('#property_to').val() == 0){
                                //             return true
                                //         }
                                //         return false
                                //     },
                                // },
                            }
                            return false
                        },
                    },

                    contract_no: {
                        required: true,
                    },

                    contract_file: {
                        required: true,
                    },

                    contract_start_date: {
                        required: true,
                    },

                    contract_end_date: {
                        required: true,
                    },

                },
                messages: {
                    property_name: "Property Name is required",
                    property_status: "Property Status is required",
                    property_to: "Property to is required",
                    category: "Category is required",
                    property_country: "Country is required",
                    property_state: "State is required",
                    property_city: "City is required",
                    property_zipcode: "Zipcode is required",
                    address1: "Address is required",
                    frequency: "Frequency is required",
                    contract_no: 'Contract number is required',
                    contract_file: 'Contract file is required',
                    contract_start_date: 'Contract start date is required',
                    contract_end_date: 'Contract end date is required',
                },
                errorElement: 'div',
                errorLabelContainer: '.errorTxt',
                showErrors: function(errorMap, errorList) {
                    if (!this.numberOfInvalids()) {
                        $('.errorTxt').hide()
                    } else {
                        $('.errorTxt').show()
                    }
                    this.defaultShowErrors();
                },
                submitHandler: function(form) {
                    var form = document.getElementById("form-add-property");
                    var formData = new FormData(form);
                    console.log(formData);
                    $(".addBtn").prop("disabled", true);
                    $(".closeBtn").prop("disabled", true);
                    $.ajax({
                        data: formData,
                        type: "post",
                        url: "{{ url('/property/management/add-property') }}",
                        processData: false,
                        dataType: "json",
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        async: true,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        },
                        beforeSend: function() {
                            $(".addBtn").text('Processing..');
                        },
                        success: function(data) {
                            $(".addBtn").text('Submit');
                            $(".addBtn").prop("disabled", false);
                            $(".closeBtn").prop("disabled", false);
                            console.log(data);
                            if (data.status == 1) {
                                toastr["success"](data.response);
                                if (data.response_data) {
                                    if (data.response_data.property_id) {
                                        var responseId = data.response_data.property_id;
                                        if ($('#is_builder').prop(":checked")) {
                                            setTimeout(function() {
                                                window.location.href = "";
                                            }, 1000);
                                        } else {
                                            setTimeout(function() {
                                                window.location.href = "/add-property-unit/" +
                                                    responseId;
                                            }, 1000);
                                        }
                                    }
                                } else {
                                    setTimeout(function() {
                                        window.location.href = "";
                                    }, 1000);
                                }
                            } else {
                                var html = "";
                                $.each(data.response, function(key, value) {
                                    html += value + "</br>";
                                });
                                toastr["error"](html);
                            }
                            $("#form-add-property").trigger("reset");
                        },
                    });
                },
            })

            $('#type').change(function() {
                var id = $(this).val()
                $.ajax({
                    url: url + "/owner/details-on-type-change/" + id,
                    method: 'GET',
                    async: true,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        if (data.status == true) {
                            var html = ''
                            data.response.forEach((value, key) => {
                                html += ` <div class="col-md-4">
                                   <div class="form-group">
                                       <label>` + value.name + `<span class="tx-danger">*</span></label>
                                       <input type="text" class="form-control" name="detailsdata[` + value.detail_id +
                                    `]" id="det-` + value.detail_id + `" placeholder=` + value
                                    .placeholder + `>
                                   </div>
                               </div>`
                            })
                            $('#details-section').html('')
                            $('#details-section').append(html)

                        } else {
                            toastr['error'](data.response)
                        }
                    },
                })
                return false
            })


            $(function() {
                var table = $('.data-table').DataTable({
                    processing: true,
                    searching: false,
                    serverSide: true,
                    dom: 'Bfrtip',
                    buttons: [{
                        extend: 'print',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4],
                            modifer: {
                                page: 'all',
                            }
                        }

                    }],
                    ajax: {
                        url: "{{ route('propertyListing') }}",
                        data: function(d){
                            d.search = $('input[type="search"]').val();
                            d.type = $('#type_id').val();

                            d.category = $('#categoryfilter').val();
                            d.property = $('#property_name_filter').val();

                            d.owner = $('#owner_id').val();
                            d.status = $('#status').val();
                        }
                    },
                    columns: [{
                            data: 'id',
                            name: 'id',
                            orderable: true
                        },
                        // {
                        //     data: 'property_reg_no',
                        //     name: 'property_reg_no'
                        // },
                        {
                            data: 'property_name',
                            name: 'property_name'
                        },
                        // {
                        //     data: 'building_name',
                        //     name: 'building_name'
                        // },

                        {
                            data: 'owner_name',
                            name: 'owner_name'
                        },
                        {
                            data: 'contract_start_date',
                            name: 'contract_start_date'
                        },
                        {
                            data: 'contract_end_date',
                            name: 'contract_end_date'
                        },

                        {
                            data: 'agent',
                            name: 'agent'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        {
                            data: 'actions',
                            name: 'actions',
                            orderable: false,
                            searchable: false
                        },
                    ]
                });
                $("#property_name_filter").change(function() {
                    table.draw();
                });
                $('.filter').change(function() {
                    table.draw();
                });
            });
        </script>
        <script>
          </script>
          {{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap"></script> --}}
          {{-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> --}}
          {{-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap"></script> --}}


    @endsection
