@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .vgreen {
        background: #65e715 !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .close {
        cursor: pointer;
        position: absolute;
        top: 15%;
        right: 12%;
        padding: 4px 8px 4px 8px;
        transform: translate(0%, -50%);
        color: white;
        opacity: 1;
    }

    .close:hover {
        background: #bbb;
    }

    .styles {
        border: 1px solid #495057;
        height: 43px;
        padding: 11px
    }

    .styles1 {
        border: 1px solid #495057;
        height: 80px;
        padding: 11px
    }

    .lightbox-gallery {
        background-image: linear-gradient(#4A148C, #E53935);
        background-repeat: no-repeat;
        color: #000;
        overflow-x: hidden
    }

    .lightbox-gallery p {
        color: #fff
    }

    .lightbox-gallery h2 {
        font-weight: bold;
        margin-bottom: 40px;
        padding-top: 40px;
        color: #fff
    }

    @media (max-width:767px) {
        .lightbox-gallery h2 {
            margin-bottom: 25px;
            padding-top: 25px;
            font-size: 24px
        }
    }

    .lightbox-gallery .intro {
        font-size: 16px;
        max-width: 500px;
        margin: 0 auto 40px
    }

    .lightbox-gallery .intro p {
        margin-bottom: 0
    }

    .lightbox-gallery .photos {
        padding-bottom: 20px
    }

    .lightbox-gallery .item {
        padding-bottom: 30px
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <a class="breadcrumb-item" href="{{ route('userBookings') }}">Property Management</a>
        <span class="breadcrumb-item active"> View</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
                                                                                                <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                                                                    <i class="fas fa-plus-square"></i>&nbsp Add Client
                                                                                                </button>
                                                                                        </div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between align-items-center">
                    <div class="col-lg-6">
                        <h4 class="card-title h4-card">{{ $property->property_name }} Property View</h4>
                        {{-- <p class="card-category"></p> --}}
                    </div>
                    @if($property->is_builder=='1')
                    <div class="col-lg-6 justify-content-end d-flex">
                        <a href="{{route('viewPropertyUnits',$property->id)}}" class="btn btn-success">Units View</a>
                    </div>
                    @endif

                </div>
            </div>

            <div class="col-md-12 mt-3">
                <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details"
                            role="tab" aria-controls="details" aria-selected="true">Details</a>
                    </li>
                    {{-- @if ($property->is_builder == 0) --}}
                    <li class="nav-item hide-element">
                        <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab"
                            aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                    </li>

                    <li class="nav-item hide-element">
                        <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities"
                            role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contract" role="tab"
                            aria-controls="contract" aria-selected="false">Contract Details</a>
                    </li>
                    @if ($property->is_builder == 0)
                    <li class="nav-item hide-element">
                        <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents"
                            role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a id="location-prop-tab" class="nav-link propertynav" data-toggle="tab" href="#location-prop"
                            role="tab" aria-controls="location-prop" aria-selected="false">Location</a>
                    </li>
                </ul>
            </div>


            {{-- <form class="form-admin" id="form-add-property"
                    action="{{ route('editOwnerProperty', [$property->id]) }}">
            @csrf --}}

            {{-- <input type="hidden" name="propertyId" value="{{ $property->id }}"> --}}


            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class=""
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Property Type :</h6>
                                    </div>
                                    <div class="right">
                                        @if ($property->category == '0')
                                        <p class="mb-0">Residential</p>
                                        @else
                                        <p class="mb-0">Commercial</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class=""
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Category :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->type_details->type }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Owners :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->owner_rel->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Property For :</h6>
                                    </div>
                                    <div class="right">
                                        @if ($property->property_to == '1')
                                        <p class="mb-0">Buy</p>
                                        @else
                                        <p class="mb-0">Rent</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Property Name :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->property_name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Property Status :</h6>
                                    </div>
                                    <div class="right">
                                        @if ($property->occupied == '0')
                                        <p class="mb-0">Vacant</p>
                                        @else
                                        <p class="mb-0">Occupied</p>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Country :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->country_rel->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">State :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->state_rel->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">City :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->city_rel->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Zipcode :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->zipcode_rel->pincode }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Address 1 :</h6>
                                    </div>
                                    <div class="right mt-2">
                                        <p class="mb-0">{{ $property->street_address_1 }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Address 2 :</h6>
                                    </div>
                                    <div class="right mt-2">
                                        <p class="mb-0">{{ $property->street_address_2 }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Zipcode :</h6>
                                    </div>
                                    <div class="right">
                                        <p class="mb-0">{{ $property->zipcode_rel->pincode }}</p>
                                    </div>
                                </div>
                                </div>
                               

                                <div class="col-lg-6">

                                <div class="mt-2"
                                    style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                    <div class="left mb-2">
                                        <h6 class="mb-0">Description :</h6>
                                    </div>
                                    <div class="right mt-2">
                                        <p class="mb-0">{{ $property->description }}</p>
                                    </div>
                                </div>
                                </div>

                               
                            @if ($details_prop)
                                    @foreach ($details_prop as $detail)    
                            <div class="col-lg-6 mt-2">
                                <div class="" id="details-section">

                                    
                                    <div class="mb-2"
                                        style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                        <div class="left mb-2">
                                            <h6 class="mb-0">{{ str_replace('_', ' ', ucfirst($detail->placeholder)) }}
                                            </h6>
                                        </div>
                                        <div class="right">
                                            <p class="mb-0" name="details_prop_data[{{ $detail->id }}]"
                                                id="det-{{ $detail->id }}">{{ $detail->value }}</p>
                                        </div>

                                    </div>
                                   
                                </div>
                            </div>
                            
                            @endforeach
                            @endif

                            <div class="col-lg-6">
                                <div class=" is-appartment">
                                    @if ($property->is_builder == '1')
                                    <div class="mt-2"
                                        style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">

                                        <div class="left mb-2">
                                            <h6 class="mb-0">Units</h6>
                                        </div>
                                        <div class="right">
                                            <p class="mb-0">{{ $property->unit_num }}</p>
                                        </div>

                                    </div> 
                                    @endif
                                    <div class="mt-2"
                                        style="border: 1px solid #f1f1f1; background: #f9f9f9; padding: 15px; border-radius: 5px;">
                                        <div class="left mb-2">
                                            <h6 class="mb-0">floors</h6>
                                        </div>
                                        <div class="right">
                                            <p class="mb-0">{{ $property->floor_num }}</p>
                                        </div>

                                    </div>
                                </div>

                            </div>




                            <!-- <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Property Type</label>
                                        @if ($property->category == '0')
                                            <p class="styles">Residential</p>
                                        @else
                                            <p class="styles">Commercial</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <p class="styles">{{ $property->type_details->type }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Owners</label>
                                        <p class="styles">{{ $property->owner_rel->name }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Property For</label>
                                        @if ($property->property_to == '1')
                                            <p class="styles">Buy</p>
                                        @else
                                            <p class="styles">Rent</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Property Name</label>
                                        <p class="styles">{{ $property->property_name }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Property Status</label>
                                        @if ($property->occupied == '0')
                                            <p class="styles">Vacant</p>
                                        @else
                                            <p class="styles">Occupied</p>
                                        @endif
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="row">
                            </div> -->
                            <!-- <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <p class="styles">{{ $property->country_rel->name }}</p>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>State</label>
                                        <p class="styles">{{ $property->state_rel->name }}</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City</label>
                                        <p class="styles">{{ $property->city_rel->name }}</p>ch
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Zipcode</label>
                                        <p class="styles">{{ $property->zipcode_rel->pincode }}</p>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address 1 </label>
                                        <p class="styles1">{{ $property->street_address_1 }}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address 2 </label>
                                        <p class="styles1">{{ $property->street_address_2 }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description </label>
                                        <p class="styles1">{{ $property->description }}</p>
                                    </div>
                                </div>

                            </div> -->

                            <div class="col-md-12">
                                <div class="form-group mt-3">
                                    <label>Individual Property</label>
                                    <input type="checkbox" name="individual_building" id="is_builder" value="1"
                                        {{ $property->is_builder == 0 ? ' checked' : '' }}>
                                </div>
                            </div>
                            @if ($property->is_builder == '1')
                            <div class="col-md-6 is-building">
                                <div class="form-group">
                                    <label>Images </label><br>

                                    {{-- <input id="building_image" name="building_image[]" type="file"
                                                    class="form-control file" multiple data-show-upload="true"
                                                    data-show-caption="true"> --}}
                                    @if (count($property_image))

                                    <div class="photos">
                                        <div class="row p-2">
                                            @foreach ($property_image as $image)
                                            @if ($image->type == '3')

                                            <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                                <img class="p-2" src="{{ $image->document }}" alt="" width="100px"
                                                    height="100px">
                                            </a>

                                            @endif
                                            @endforeach
                                        </div>
                                    </div>

                                    @endif
                                </div>
                            </div>
                            @endif

                            <!-- <div class="row is-appartment">
                                @if ($property->is_builder == '1')
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. of Units</label>
                                        <p class="styles">{{ $property->unit_num }}</p>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. of floors</label>
                                        <p class="styles">{{ $property->floor_num }}</p>
                                    </div>
                                </div>
                            </div> -->

                            <div class="amenities-row" style="display: none">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            @if ($property->is_builder == 1)
                                            <label>Furnished </label>
                                            @if ($property->furnished == '0')
                                            <p class="styles"> Not Furnished</p>
                                            @elseif ($property->furnished == '1')
                                            <p class="styles"> Semi Furnished</p>
                                            @elseif ($property->furnished == '2')
                                            <p class="styles"> Fully Furnished</p>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-warning tx-white mb-3"> &nbspDetails</div>

                                <div class="row" id="details-section">
                                    {{-- @if ($details)
                                            @foreach ($details as $det)
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>{{ $det->name }}<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="detailsdata[{{ $det->detail_id }}]"
                                        id="det-{{ $det->detail_id }}" placeholder="{{ $det->placeholder }}">
                                </div>
                            </div>
                            @endforeach
                            @endif --}}


                            @if ($details_prop)
                            @foreach ($details_prop as $detail)
                            <div class="col-md-4 d-none">
                                <div class="form-group">
                                    <label>{{ str_replace('_', ' ', ucfirst($detail->placeholder)) }}<span
                                            class="tx-danger">*</span></label>
                                    <p class="styles" name="details_prop_data[{{ $detail->id }}]"
                                        id="det-{{ $detail->id }}">{{ $detail->value }}</p>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <!-- end of tab1 -->
            <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                <div class="rentdetails">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group back">
                                <h6>Lease Type</h6>
                                @if ($property_frequency)
                                <p class="mb-0">
                                    {{ $property_frequency->type }}</p>
                                @else
                                <p class="mb-0">Not Mentioned</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group back">
                                <h6>Rent</h6>
                                <p class="mb-0">
                                    @if ($property->rent)
                                    {{ $property->rent }}@else 0
                                    @endif
                                </p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group back">
                                <h6>Security Deposit</h6>
                                <p class="mb-0">
                                    @if ($property->security_deposit)
                                    {{ $property->security_deposit }}@else 0
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                @if ($property->property_to == '1')
                <div class="sellingdetails">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Min Selling price</label>
                                <p class="styles">
                                    @if ($property->selling_price)
                                    {{ $property->selling_price }}@else 0
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Max Selling price</label>
                                <p class="styles">
                                    @if ($property->mrp)
                                    {{ $property->mrp }}@else 0
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="token-amount">
                    <div class="row">
                        <div class="col-md-6 token-hold">
                            <div class="form-group back">
                                <h6>Token Amount</h6>
                                <p class="mb-0">
                                    @if ($property->token_amount)
                                    {{ $property->token_amount }} @else 0
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="amenities" role="tabpanel" aria-labelledby="amenities-tab">
                <div id="amenties_available">
                    <div class="row">
                        
                    @if ($amenities_listing)
                    @foreach ($property_amenities as $row)
                    <div class="col-md-6">
                            <div class="form-group">
                                <div class="tx-dark mb-3 p-2" style="background-color: #ebebeb; border-radius: 2px;"> &nbsp
                                    {{ $row->amenity_category->name }}
                                </div>
                                <div class="mg-l-1">
                                    {{-- @foreach ($row->amenity_category as $row2) --}}
                                    <label class="ckbox ml-2">
                                        <input type="checkbox" value="{{ $row->amenity->id }}" name="amenities[]"
                                            checked>
                                        <span>{{ $row->amenity->name }}</span>
                                        {{-- <input type="hidden" name="amenityId" value="{{$row->id}}"> --}}
                                    </label>
                                    {{-- @endforeach --}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @endif
             
           
        </div>
                </div>
            </div>
            <!-- end of tab3 -->
            <!-- start tab 4 -->
            <div class="tab-pane fade" id="contract" role="tabpanel" aria-labelledby="contract-tab">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Contract No</h6>
                            <p class="mb-0">{{ $property->contract_no }}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Contract File</h6>
                            {{-- <input type="file" class="form-control" name="contract_file" id="contract_file"> --}}
                            <img src="{{ $property->contract_file }}" alt="" width="100px" height="100px">

                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Contract Start Date</h6>
                            <p class="mb-0">
                                {{ $property->contract_start_date }}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Contract End Date</h6>
                            <p class="mb-0">
                                {{ $property->contract_end_date }}</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Commission in terms of :-<span class="tx-danger">*</span></h6>
                          
                            @if ($property->commission_in == 'amount')
                            <p>Amount</p>
                            <input type="radio" checked name="commision_as" id="commission_as" value="amount">
                            @else
                            <h6>Percentage</h6>
                            <input type="radio" checked name="commision_as" id="commission_as" value="percentage">
                            @endif
                           
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Commission<span class="tx-danger" style="font-size:12px;">*</span></h6>
                            @if ($property->commission_in == 'amount')
                            <p class="mb-0">{{ $property->commission }}</p>
                            @else
                            <p class="mb-0">{{ $property->commission }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Property Management fee:-<span class="tx-danger">*</span></h6>
                            @if ($property->property_management_fee_type == 'amount')
                            <p>Amount</p>
                            <input type="radio" class="property_management_fee_type" name="property_management_fee_type"
                                id="property_management_fee_type" value="amount" checked>
                            @else
                            <h6>Percentage</h6>
                            <input type="radio" class="property_management_fee_type" name="property_management_fee_type"
                                id="property_management_fee_type" value="percentage">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Management Fee<span class="tx-danger" style="font-size:12px;">*</span></h6>
                            @if ($property->property_management_fee_type == 'amount')
                            <p class="mb-0">{{ $property->management_fee }}</p>
                            @else
                            <p class="mb-0">{{ $property->management_fee }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Ads Number:-</h6>
                            <p class="mb-0">{{ $property->ads_number }}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Guard Number</h6>
                            <p class="mb-0">{{ $property->guards_number }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of tab4 -->
            <!-- start tab 5 -->
            <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6 class="mb-0">Images </h6>

                            @if (count($property_image))
                            <div class="photos">
                                <div class=" row p-2">
                                    @foreach ($property_image as $image)
                                    @if ($image->type == '0')
                                    <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                        <img class="p-2" src="{{ $image->document }}" alt="" width="100px"
                                            height="100px">
                                    </a>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6 class="mb-0">Floor Plans </h6>

                            @if (count($property_image))
                            <div class="photos">
                                <div class="row p-2">
                                    @foreach ($property_image as $floor)
                                    @if ($floor->type == '2')

                                    <a target="_blank" href="{{ $floor->document }}" data-lightbox="photos">
                                        <img class="p-2" src="{{ $floor->document }}" alt="" width="100px"
                                            height="100px">
                                    </a>

                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group back">
                            <h6>Videos <span class="tx-danger"></span></h6><br>
                            <div class="  photos">
                                <div class="row p-2">
                                    @foreach ($property_image as $image)
                                    @if ($image->type == '1')
                                    <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                        <img class="p-2" src="{{ $image->document }}" alt="" width="100px"
                                            height="100px">
                                    </a>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="location-prop" role="tabpanel" aria-labelledby="location-prop-tab">
                <div id="map">

                </div>
                <div>

                    <input type="hidden" id="lat-prop" name="lat_prop">
                    <input type="hidden" id="lng-prop" name="lng_prop">
                </div>
            </div>
        </div>
        <div class="errorTxt shadow"></div>
        <div class="modal-footer">
            {{-- <a href="{{route('addPropertyUnits')}}" class="btn btn-primary addBtn" id="addPropertyBtn"
            disabled>Submit</a> --}}
            {{-- <button type="submit" class="btn btn-primary addBtn" id="addPropertyBtn">Submit</button> --}}
        </div>
        {{-- </form> --}}
    </div>
</div>
<!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="overflow: hidden">
</div>



@endsection @section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly"
    async></script>
<script>
    const url = '{{ url(' / ') }}';
    // var deletegalleryimageurl = "{{ route('deletegalleryimage', $property->id) }}";
    $('.sellingdetails').hide()
    $('.hide-element').hide();
    $('.amenities-row').hide();

</script>

{{-- <script>
        $(document).ready(function(){
            $('.hide-element').css('display', 'none');
        });
    </script> --}}

<script>
    $('#property_country').change(function () {
        var countryId = $(this).val()
        //  alert(countryId);
        $.ajax({
            url: url + "/get-states/" + countryId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('.state-select').html('')
                var html = '<option value="">--Select--</option>';
                if (data.status === true) {
                    if (data.response.length > 0) {

                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.name + `</option>`
                        })
                        $('.state-select').append(html);

                    } else {
                        $('.state-select').html('')
                    }
                } else {
                    $('.state-select').html('')
                }
            }
        });
        // return false
    });

    $('.state-select').change(function () {
        var stateId = $(this).val()
        $.ajax({
            url: url + "/get-cities/" + stateId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('.city-select').html('')
                var html = '<option value="">--Select--</option>';
                if (data.status === true) {
                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.name + `</option>`
                        })
                        $('.city-select').append(html)
                    } else {
                        $('.city-select').html('')
                    }
                } else {
                    $('.city-select').html('')
                }
            }
        });
        // return false
    });

    $('.city-select').change(function () {
        var cityId = $(this).val();
        $.ajax({
            url: url + "/get-pincodes/" + cityId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('.zipcode-select').html('')
                var html = '<option value="">--select--</option>';
                if (data.status === true) {
                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.pincode +
                                `</option>`
                        })
                        $('.zipcode-select').append(html)
                    } else {
                        $('.zipcode-select').html('')
                    }
                } else {
                    $('.zipcode-select').html('')
                }
            }
        });

        return false
    });
    $('#category').change(function () {
        var categoryId = $(this).val();
        categoryTypeFilter(categoryId);
    });

    function categoryTypeFilter(categoryId) {
        $.ajax({
            url: url + "/get-type/" + categoryId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('.category-select').html('')
                var html = '<option value=""></option>';
                if (data.status === true) {

                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.type + `</option>`
                        })
                        $('.category-select').append(html)
                    } else {
                        $('.v-select').html('')
                    }
                } else {
                    $('.category-select').html('')
                }
            }
        })
        return false
    }

    $('.fc-datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        container: '#contractUploadModal modal-body'
    });
    $('#property_to').change(function () {
        if ($('#property_to').val() == 1) {
            // buy -> 1 / rent->0
            $('.rentdetails').hide();
            $('.sellingdetails').show()

        } else {
            $('.sellingdetails').hide()
            $('.rentdetails').show();
        }
    });
    // $(document).on("click", "#is_builder", function() {

    //     var check_val = $(this).prop("checked");
    //     if (check_val) {
    //         $('.hide-element').show();
    //         $('.amenities-row').show();
    //         $('.is-building').hide();
    //     } else {
    //         $('.hide-element').hide();
    //         $('.amenities-row').hide();
    //         $('.is-building').show();
    //     }
    // });

    if ($('#is_builder').is(':checked')) {
        $('.hide-element').show();
        $('.amenities-row').show();
        $('.is-building').hide();
        $('.is-appartment').hide();
    } else {
        $('.hide-element').hide();
        $('.amenities-row').hide();
        $('.is-building').show();
        $('.is-appartment').show();
    }

</script>
<script>
    function initMap() {
        const uluru = {
            lat: 24.466667,
            lng: 54.366669
        };
        if ($('#lat').val() != "" && $('#lng').val() != "") {
            const uluru = {
                lat: $('#lat').val(),
                lng: $('#lng').val()
            };
        }

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            center: uluru,
        });
        const marker = new google.maps.Marker({
            position: uluru,
            map: map,
            draggable: true
        });
        // navigator.geolocation.getCurrentPosition(
        //     function(position) { // success cb
        //         if ($('#lat').val() == "" && $('#lng').val() == "") {
        //             $('#lat').val(parseFloat(position.coords.latitude))
        //             $('#lng').val(parseFloat(position.coords.longitude))
        //         }
        //         var lat = $('#lat').val()
        //         var lng = $('#lng').val()
        //         map.setCenter(new google.maps.LatLng(lat, lng));
        //         var latlng = new google.maps.LatLng(lat, lng);
        //         marker.setPosition(latlng);
        //     },
        //     function() {}
        // );
        google.maps.event.addListener(marker, 'dragend', function () {
            $('#lat').val(marker.position.lat())
            $('#lng').val(marker.position.lng())
        })
    }

    function initPropertyMap() {
        const uluru = {
            lat: 24.466667,
            lng: 54.366669
        };
        console.log('lat', $('#lat-prop').val())
        if ($('#lat-prop').val() != "" && $('#lng-prop').val() != "") {
            const uluru = {
                lat: $('#lat-prop').val(),
                lng: $('#lng-prop').val()
            };
        }
        const map = new google.maps.Map(document.getElementById("map-2"), {
            zoom: 10,
            center: uluru,
        });
        const marker = new google.maps.Marker({
            position: uluru,
            map: map,
            draggable: true
        });
        navigator.geolocation.getCurrentPosition(
            function (position) { // success cb
                if ($('#lat-prop').val() == "" && $('#lng-prop').val() == "") {
                    $('#lat-prop').val(parseFloat(position.coords.latitude))
                    $('#lng-prop').val(parseFloat(position.coords.longitude))
                }
                var lat = $('#lat-prop').val()
                var lng = $('#lng-prop').val()
                map.setCenter(new google.maps.LatLng(lat, lng));
                var latlng = new google.maps.LatLng(lat, lng);
                marker.setPosition(latlng);
            },
            function () {}
        );
        google.maps.event.addListener(marker, 'dragend', function () {
            $('#lat-prop').val(marker.position.lat())
            $('#lng-prop').val(marker.position.lng())
        })
    }
    $('#form-add-property').validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            property_name: {
                required: true,
            },
            property_to: {
                required: true,
            },
            category: {
                required: true
            },
            property_country: {
                required: true,
            },
            property_state: {
                required: true,
            },
            property_city: {
                required: true,
            },
            property_zipcode: {
                required: true,
            },
            address1: {
                required: true,
            },
            // furnished : {
            //     required : true,
            // },
            is_builder: {
                required: function () {
                    if ($('#is_builder').prop(":checked")) {
                        return true;
                        // min_sellingprice : {
                        //     required : function (){
                        //         if($('#property_to').val() == 1){
                        //             return true
                        //         }
                        //         return false
                        //     },
                        // },
                        // max_sellingprice : {
                        //     required : function (){
                        //         if($('#property_to').val() == 1){
                        //             return true
                        //         }
                        //         return false
                        //     },
                        // },
                        // 'amenities[]' : {
                        //     required :true
                        // },
                        // 'images[]' : {
                        //     required : true,
                        // },
                        // 'detailsdata[]' : {
                        //     required : true,
                        // }

                        // min_rent : {
                        //     required : function (){
                        //         if($('#property_to').val() == 0){
                        //             return true
                        //         }
                        //         return false
                        //     },
                        // },
                        // token_amount : {
                        //     required : true,
                        // },

                        // security_deposit : {
                        //     required : function (){
                        //         if($('#property_to').val() == 0){
                        //             return true
                        //         }
                        //         return false
                        //     },
                        // },
                    }
                    return false
                },
            },

            //  contract_no: {
            //      required: true,
            //  },

            //  contract_file: {
            //      required: true,
            //  },

            //  contract_start_date: {
            //      required: true,
            //  },

            //  contract_end_date: {
            //      required: true,
            //  },

        },
        messages: {
            property_name: "Property Name is required",
            property_to: "Property to is required",
            category: "Category is required",
            property_country: "Country is required",
            property_state: "State is required",
            property_city: "City is required",
            property_zipcode: "Zipcode is required",
            address1: "Address is required",
            frequency: "Frequency is required",
            // min_rent : "Min rent is required",
            // max_rent : "Max rent is required",
            // furnished : "Furnished is required",
            // security_deposit : "Security deposit is required",
            // token_amount : "Token Amount is required",
            //  contract_no: 'Contract number is required',
            //  contract_file: 'Contract file is required',
            //  contract_start_date: 'Contract start date is required',
            //  contract_end_date: 'Contract end date is required',
            // min_sellingprice : "Min Selling Price is required",
            // max_sellingprice : "Max Selling Price is required",
            // 'amenities[]' : 'Amenities are required',
            // 'images[]' : 'Images are required',
            // 'detailsdata[]': 'Details are required',
        },
        errorElement: 'div',
        errorLabelContainer: '.errorTxt',
        showErrors: function (errorMap, errorList) {
            if (!this.numberOfInvalids()) {
                $('.errorTxt').hide()
            } else {
                $('.errorTxt').show()
            }
            this.defaultShowErrors();
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-property");
            var formData = new FormData(form);
            console.log(formData);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{ url('/property/editOwner-property') }}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    console.log(data);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        if (data.response_data) {
                            if (data.response_data.property_id) {
                                var responseId = data.response_data.property_id;
                                console.log(responseId);
                                if ($('#is_builder').is(':checked')) {

                                    setTimeout(function () {
                                        window.location.href =
                                            "/property/management/list";
                                    }, 1000);
                                } else {
                                    // alert("fdiu");
                                    setTimeout(function () {
                                        window.location.href = "/edit-property-unit/" +
                                            responseId;
                                    }, 1000);
                                }
                            }
                        } else {
                            setTimeout(function () {
                                window.location.href = "";
                            }, 1000);
                        }
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });

    // $("document").ready(function() {
    //     $("#floor_plans").change(function() {
    //         // alert('changed!');

    //     });
    // });


    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    function deleteImage(element, path, pid) {
        var deleteimageblock = $(element);

        if (path != "") {
            var form_data = new FormData();
            form_data.append("deletegalleryimagename", path)
            // form_data.append("_token", $('meta[name=_token]').attr('content'));
            $.ajax({
                url: "/deleteimage/" + pid,
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                success: function (data) {
                    // console.log(data);
                    if (data.success) {
                        deleteimageblock.closest('div').remove();
                        // getuploadedanduploadablegalleryimages();
                    }

                },
                error: function (ts) {
                    alert(ts.responseText);
                }
            });
        }


    }

</script>
@endsection
