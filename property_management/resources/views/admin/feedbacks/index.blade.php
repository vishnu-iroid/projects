   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
   </style>
   @section('content')
    
        <div class="br-pageheader">
          <nav class="breadcrumb pd-0 mg-0 tx-12">
              <a class="breadcrumb-item" href="http://127.0.0.1:8000/dashboard">Home</a>
              <span class="breadcrumb-item active">Feedback</span>
          </nav>
   
      
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Feedback</h4>
                           
                        </div>
                    </div>
                </div>
                <div class="card-body">
        <div class="bd bd-gray-300 table-responsive">
            <table class="table table-striped table-bordered mg-b-0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>User Name</th>
                  <th>Feedbacks</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                  @php $i =1; @endphp
                  @foreach($feedbacks as $row)
                  <tr>
                    <th scope="row">{{ $i }} @php $i++; @endphp</th>
                    <td>{{ $row->user_rel->name }}</td>
                    <td>{{ $row->feedback }}</td>
                    <td>
                        <a href="{{ url('edit-feedbacks/'.$row->id) }}" class="btn btn-primary btn-icon" title="view"><div><i class="fa fa-eye"></i></div></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- bd -->
          <div>
            {{$feedbacks->links()}}
          </div>
      </div>
      </div>
      </div>
      </div>
      <!-- br-pagebody -->


    @endsection
