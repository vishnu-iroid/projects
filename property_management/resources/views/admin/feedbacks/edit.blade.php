   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
   </style>
   @section('content')
      <div class="br-pagetitle">
     
        <div>
          <h4>View Feedbacks</h4>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
                <div class="br-section-wrapper mg-t-10">
                        <div class="form-layout form-layout-1">
                            <div class="col-lg-12">
                                    <input type="hidden" name="id" value="{{ $feedbacks->id }}">
                                <div class="form-group">
                                    <label class="form-control-label">User Id: <span class="tx-danger">*</span></label>
                                    <input type="text" name="user_id" class="form-control" value="{{ $feedbacks->user_id }}" disabled>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Description: <span class="tx-danger">*</span></label>
                                    <textarea class="form-control " type="text" rows="5" name="feedback" disabled>{{ $feedbacks->feedback }}</textarea>
                                </div>
                            </div><!-- col-4 -->

                        </div><!-- form-layout -->
                </div>
      </div><!-- br-pagebody -->
    @endsection
  