<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Siaha</title>
    <style>
        body{
            font-family: 'Segoe UI', sans-serif;

        }
        .wrap{
            /* border: 1px solid rgba(219, 219, 219, 0.623); */
            margin: 5px;
            /* width: 800px; */
        }
        p{
            line-height: 30px;
            margin-top: 0;
        }
        .wrap_txt{
            margin: 0 auto;
        }
        .discrip h4{
            margin-bottom: 5px;  
        }
        h4{
            
        }
        ul.features{
            padding-left: 30px;
            line-height: 30px;
        }
        h3.name{
            padding: 10px;
            border: 1px solid rgba(219, 219, 219, 0.623);
            text-align: center;
            font-size: 22px;
            font-weight: 500;

        }
        h4 span{
            font-size: 14px;
            color: darkgray
            
        }
        h4{
            font-size: 20px;
            font-weight: 500;
        }

        table, td,th {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .text-center{
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="wrap">
    <div class="" style="">
        
   
    <h3 class="name">Property Report</span></h3>
    <div style="overflow-x:auto;">
        <table style="margin-top:5%">
            <tr>
                <th>Property Code</th>
                <th>Property Name</th>
                <th>Property Type</th>
                {{-- <th>Contract Start</th>
                <th>Contract End</th> --}}
                <th>Property Status</th>
                <th>Occupied Status</th>
                <th>Total Service Charge</th>
                <th>Total Amount Received</th>
                <th>No. of Payments Received</th>
            </tr>
            @foreach($properties as $property)
            <tr>
                <td>
                    @if($property->property_reg_no)
                        <span class="text-center">{{$property->property_reg_no}}</span>
                    @else
                        <span class="text-center">--</span>
                    @endif
                </td>
                <td>
                    @if($property->property_name)
                        <span class="text-center">{{$property->property_name}}</span>
                    @else
                        <span class="text-center">--</span>
                    @endif
                </td>
                <td>
                @if($property->type_details)
                    @if($property->type_details->type)
                        <span class="text-center">{{$property->type_details->type}}</span>
                    @else
                        <span class="text-center">--</span>
                    @endif
                @else
                    <span class="text-center">--</span>
                @endif
                </td>
                {{-- <td>
                @if($property->contract_start_date)
                    <span class="text-center">{{ date('d M Y',strtotime($property->contract_start_date))}}</span>
                @else
                    <span class="text-center">--</span>
                @endif
                </td> --}}
                {{-- <td> --}}
                {{-- @if($property->contract_start_date)
                    <span class="text-center">{{ date('d M Y',strtotime($property->contract_start_date))}}</span>
                @else
                    <span class="text-center">--</span>
                @endif --}}
                {{-- </td> --}}
                <td>
                    @if($property->status == 1)
                        <span class="text-center" style="color:green"> Active </span>
                    @else
                        <span class="text-center" style="color:red"> Inactive </span>
                    @endif
                </td>
                <td>
                    @if($property->occupied == 1)
                        <span class="text-center" style="color:green"> Occupied </span>
                    @else
                        <span class="text-center" style="color:red"> Vacant </span>
                    @endif
                </td>
                <td>
                    @if($property->service_payment_amount)
                        <span class="text-center"> {{$property->service_payment_amount}}  </span>
                    @else
                        <span class="text-center"> 0.00 </span>
                    @endif
                </td>
                <td>
                    @if($property->total_payment_amount)
                        <span class="text-center"> {{$property->total_payment_amount}}  </span>
                    @else
                        <span class="text-center"> 0.00 </span>
                    @endif
                </td>
                <td>
                    @if($property->total_payment_count)
                        <span class="text-center"> {{$property->total_payment_count}}  </span>
                    @else
                        <span class="text-center"> 0 </span>
                    @endif
                </td>
            </tr>
            @endforeach
        </table>

</div>
</div>
   
</div>
</body>