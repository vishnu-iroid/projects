<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Siaha</title>
    <style>
        body{
            font-family: 'Segoe UI', sans-serif;

        }
        .wrap{
            /* border: 1px solid rgba(219, 219, 219, 0.623); */
            margin: 5%;
            /* width: 800px; */
        }
        p{
            line-height: 30px;
            margin-top: 0;
        }
        .wrap_txt{
            margin: 0 auto;
        }
        .discrip h4{
            margin-bottom: 5px;  
        }
        h4{
            
        }
        ul.features{
            padding-left: 30px;
            line-height: 30px;
        }
        h3.name{
            padding: 10px;
            border: 1px solid rgba(219, 219, 219, 0.623);
            text-align: center;
            font-size: 22px;
            font-weight: 500;

        }
        h4 span{
            font-size: 14px;
            color: darkgray
            
        }
        h4{
            font-size: 20px;
            font-weight: 500;
        }
        table, td,th {
            border: 1px solid black;
            border-collapse: collapse;
        }

        .text-center{
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="wrap">
    <div class="" style="padding: 30px;">
        
   
    <div class="wrap_txt">
        <h3 class="name">Yearly Accounts - {{$year}}</span></h3>
        <div style="overflow-x:auto;">
            <table style="margin-top:5%">
                <tr>
                    <th width="45%">Month</th>
                    <th width="10%">Revenue</th>
                    <th width="10%">Expense</th>
                </tr>
                @foreach($months as $key=>$month)
                <tr col="2">
                    <td class="text-center">
                        <span >{{$key}}</span>
                    </td>
                    <td  class="text-center">
                        <span>{{$month['revenue']}}</span>
                    </td>
                    <td class="text-center">
                        <span>{{$month['expense']}}</span>
                    </td>
                </tr>
                
                @endforeach
            </table>

        </div>
    </div>
</div>
   
</div>
</body>