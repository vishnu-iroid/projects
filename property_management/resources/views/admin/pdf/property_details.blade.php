<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Siaha</title>
    <style>
        body{
            font-family: 'Segoe UI', sans-serif;

        }
        .wrap{
            /* border: 1px solid rgba(219, 219, 219, 0.623); */
            margin: 5%;
            /* width: 800px; */
        }
        p{
            line-height: 30px;
            margin-top: 0;
        }
        .wrap_txt{
            margin: 0 auto;
        }
        .discrip h4{
            margin-bottom: 5px;  
        }
        h4{
            
        }
        ul.features{
            padding-left: 30px;
            line-height: 30px;
        }
        h3.name{
            padding: 10px;
            border: 1px solid rgba(219, 219, 219, 0.623);
            text-align: center;
            font-size: 22px;
            font-weight: 500;

        }
        h4 span{
            font-size: 14px;
            color: darkgray
            
        }
        h4{
            font-size: 20px;
            font-weight: 500;
        }
    </style>
</head>
<body>
    <div class="wrap">
    <div class="" style="padding: 30px;">
        
   
    <div class="wrap_txt">
        @if($property->property_name)
            <h3 class="name">Property Name : <span>{{$property->property_name}}</span></h3>
        @else
            <h3 class="name">Property Code : <span>{{$property->property_reg_no}}</span></h3>
        @endif
        @if($property->property_name)
            <h4>Property Code : {{$property->property_reg_no}}</h4>
        @endif
        @if($property->property_to == 1)
            @if($property->selling_price)
                <h4> Discount Price : <span>{{$property->selling_price}}</span></h4>
            @endif
            @if($property->mrp)
                <h4> Actual Price : <span>{{$property->mrp}}</span></h4>
            @endif
            <h6 style="color: darkred;">Appartment for Sale  </h6>
        @else
            @if($property->rent)
                <h4> Rent : <span>{{$property->rent}}</span></h4>
            @endif
            <h6 style="color: darkred;">Appartment for Rent  </h6>
        @endif

        <h4>Features</h4>
        <ul class="features">
            <li>Beds : @if($property->Beds) {{$property->Beds}} @endif</li>
            <li>bathroom : @if($property->Bathroom) {{$property->Bathroom}} @endif</li>
            <li>Sq.t : @if($property->area) {{$property->area}} @endif</li>
            <li>Furnished : @if($property->furnished == 0) Not Furnished @elseif($property->furnished == 1) Semi Furnished @else Fully Furnished @endif</li>
        </ul>
    
        @if($property->description)
            <div class="discrip">
                <h4>Description</h4>
                <p>{{$property->description}}</p>
            </div>
        @endif

</div>
</div>
   
</div>
</body>

@if($property->documents)
@php
    $status =0;
    foreach ($property->documents as  $item){
        if($item->type == 0){
            $status =1;
        }
    } 
@endphp
@if($status == 1)
<body>
    <h3>Images</h3>
    <div class="images" style="text-align: center;">
        @foreach ($property->documents as $item)
        @if($item->type == 0)
        <img src="{{$item->document_image}}" alt="" width="350" height="300" style="margin:5%;"> 
        @endif
        @endforeach 
    </div>
</body>
@endif
@endif