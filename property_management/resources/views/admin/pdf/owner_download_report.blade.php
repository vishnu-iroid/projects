<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Siaha</title>
    <style>
        body{
            font-family: 'Segoe UI', sans-serif;

        }
        .wrap{
            /* border: 1px solid rgba(219, 219, 219, 0.623); */
            margin: 5%;
            /* width: 800px; */
        }
        p{
            line-height: 30px;
            margin-top: 0;
        }
        .wrap_txt{
            margin: 0 auto;
        }
        .discrip h4{
            margin-bottom: 5px;  
        }
        h4{
            
        }
        ul.features{
            padding-left: 30px;
            line-height: 30px;
        }
        h3.name{
            padding: 10px;
            border: 1px solid rgba(219, 219, 219, 0.623);
            text-align: center;
            font-size: 22px;
            font-weight: 500;

        }
        h4 span{
            font-size: 14px;
            color: darkgray
            
        }
        h4{
            font-size: 20px;
            font-weight: 500;
        }
    </style>
</head>
<body>
    <div class="wrap">
    <div class="" style="padding: 30px;">
        
   
    <div class="wrap_txt">
        @if($property->property_name)
            <h3 class="name">Property Name : <span>{{$property->property_name}}</span></h3>
        @else
            <h3 class="name">Property Code : <span>{{$property->property_reg_no}}</span></h3>
        @endif
        @if($property->property_name)
            <h4>Property Code : {{$property->property_reg_no}}</h4>
        @endif
        @if($property->property_to == 1)
            
            @if($property->owner_amount)
                <h4> Amount : <span>{{$property->owner_amount}}</span></h4>
            @endif

            <h5 style="color: darkred;">Appartment for Sale  </h5>
        @else
            @if($property->owner_amount)
                <h4> Amount : <span>{{$property->owner_amount}} @if($property->property_rent_frequency) /{{$property->property_rent_frequency->type}} @endif</span></h4>
            @endif
            <h5 style="color: darkred;">Appartment for Rent  </h5>
        @endif

        <h4>Property Status : @if($property->status == 1) <b style="color: #339933">Active</b> @else <b style="color: #e90c1e">Inactive</b> @endif</h4>
        <h4>Occupied Status : @if($property->occupied == 1) <b style="color: #339933">Occupied</b>  @else <b style="color: #e90c1e">Vacant</b> @endif</h4>
        @if($service_payment_amount)
            <h4>Total Service Cost  :  {{$service_payment_amount}}</h4>
        @endif
        @if($total_payment_amount)
            <h4>Total Amount Received  :  {{$total_payment_amount}}</h4>
        @endif
        @if($total_payment_count)
            <h4>No. of Payment Received  :  {{$total_payment_count}}</h4>
        @endif

        @if($property->contract_start_date)
            <h4>Contract Start Date : {{date('d M Y',strtotime($property->contract_start_date))}}</h4>
        @endif
        @if($property->contract_end_date)
            <h4>Contract End Date : {{date('d M Y',strtotime($property->contract_end_date))}}</h4>
        @endif

</div>
</div>
   
</div>
</body>