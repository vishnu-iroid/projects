<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tenant Report</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .tblcolor{
            background-color: #3a579e;
        }
        body{
            font-size: 12px;
        }
    </style>
</head>

<body>
    <h1 class="text-center">{{ $title }}</h1>
    <h2 class="text-center">{{ $report }}</h2>
    <p class="text-center">{{ $date }}</p>
    <div class="p-1 mb-2 tblcolor text-white">Tenant Details</div>
    <div>
        <p>Tenant Name:{{ $properties->user_rel->name }}</p>
        <p>Contact Number:{{ $properties->user_rel->phone }}</p>
        <p>Email:{{ $properties->user_rel->email }}</p>
        <p>Address:{{ $properties->user_rel->address }}</p>
    </div>
    <div>
        <div class="p-1 mb-2 tblcolor text-white">Property Details</div>
        <div>
            <p>Property Name:{{ $properties->prop_rel->property_name }}</p>
            @if ($properties->prop_rel->category == 0)
                <p>Property Type:Residential</p>
            @else
                <p>Property Type:Commercial</p>
            @endif
            @if ($properties->prop_rel->builder_id != null)
                @php
                    $data = DB::table('owner_properties')
                        ->select('id', 'property_name')
                        ->where('id', $properties->prop_rel->builder_id)
                        ->first();
                @endphp
                @if (isset($data->property_name))
                    <p>Unit:{{ $data->property_name }}</p>
                @else
                    <p>Unit: No Units</p>
                @endif
            @else
                <p>Unit: No Units</p>

            @endif
            {{-- <p>Location: </p>
                <img src="" alt=""> --}}
        </div>
    </div>
    <div>
        <div class="p-1 mb-2 tblcolor text-white">Contract Details</div>
        <p> Contract Start Date {{ $properties->prop_rel->contract_start_date }}</p>
        <p>Contract End Date:{{ $properties->prop_rel->contract_end_date }}</p>
        <p>Lease Type:
            @if ($properties->prop_rel->freequecy_rel)
            {{ $properties->prop_rel->freequecy_rel->type }}</p>
            @endif

        {{-- <p>Contract Value: {{ $properties->prop_rel->contract_end_date }}</p> --}}
        <p>Deposit: {{ $properties->prop_rel->security_deposit }}</p>
        @if (isset($properties->rent_report_rel->payment_type) == 1)
            <p>Service Charges: {{ $properties->rent_report_rel->amount }}</p>
        @endif
        {{-- <p>No. of payments: 2</p>
       <p>No. Of Services: 10</p> --}}
        {{-- <p>Service Lists: AC, ELECTRIC, PLUMBING</p> --}}
    </div>
    <div>
        <div class="p-1 mb-2 tblcolor text-white">Due Details</div>
        <div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th>Due Date</th>
                    <th>Due Amount</th>
                    <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    @if ($properties->accounts_rel)
                    @foreach ($properties->accounts_rel as $account)
                        <tr>
                               <td>{{$account->date}}</td>
                                @if ($account->payment_type == '5')
                                    <td>{{ $account->amount }}</td>
                                    <td>Rent</td>
                                @elseif ($account->payment_type == '1')
                                    <td>{{ $account->amount }}</td>
                                    <td>Service</td>
                                @elseif ($account->payment_type == '8')
                                    <td>{{ $account->amount }}</td>
                                    <td>Maintenance</td>
                                @endif
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="m-2">
        <div class="p-1 mb-2 tblcolor text-white">Payment Details</div>
        <div>
            <table class="table table-bordered table-stripped">
                <tbody>
                    <tr>
                        <td>Receivables</td>
                        <td>--</td>
                    </tr>
                    <tr>
                        <td>Collection</td>
                        {{-- @if ($properties->rent_report_rel)
                            @foreach ($properties->rent_report_rel as $account)
                                @if ($account->payment_type == '5')
                                    <td>{{ $account->amount }}</td>
                                    <td>Rent</td>
                                @elseif ($account->payment_type == '1')
                                    <td>{{ $account->amount }}</td>
                                    <td>Service</td>
                                @elseif ($account->payment_type == '8')
                                    <td>{{ $account->amount }}</td>
                                    <td>Maintenance</td>
                                @endif
                            @endforeach
                        @endif --}}
                        <td>Balance</td>
                        <td>--</td>
                    </tr>

                </tbody>

            </table>
        </div>
    </div>
    <div class="m-2">
        <div class="p-1 mb-2 tblcolor text-white">Payment Reports</div>
        <div>
            <table class="table table-bordered table-stripped">

                <thead>
                    <tr>
                    <th>Due Date</th>
                    <th>Due amount</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Attachment</th>
                    <th>Balance</th>
                    </tr>
                </thead>
                <tbody>

                    @if ($properties->accounts_rel)
                       @foreach ($properties->accounts_rel as $row)
                       <tr>
                           <td> {{ $row->date }} </td>
                           @if ($account->payment_type == '5')
                           <td>{{ $account->amount }}</td>
                           <td>Rent</td>
                       @elseif ($account->payment_type == '1')
                           <td>{{ $account->amount }}</td>
                           <td>Service</td>
                       @elseif ($account->payment_type == '8')
                           <td>{{ $account->amount }}</td>
                           <td>Maintenance</td>
                       @endif
                       <td> {{ $row->date }} </td>
                       <td>{{ $row->amount }} </td>
                       <td> <a href="">view</a> </td>
                       <td> 0 </td>
                       </tr>
                       @endforeach
                    @endif

                </tbody>

            </table>
        </div>
    </div>
    <div class="m-2">
        <div class="p-1 mb-2 tblcolor text-white">Service Details</div>
        <div>
            <table class="table table-bordered table-stripped">
                <tbody>
                    <tr>
                        <td>Total Service</td>
                        <td>
                            @if($properties->contract_rel)
                            {{ $properties->contract_rel->total_free_maintenance }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Ordered</td>
                        <td>
                        @if ($service_count)
                            {{ $service_count }}
                        @endif
                    </td>
                    </tr>
                    <tr>
                        <td>Remaining</td>
                        <td>
                            @if($properties->contract_rel && $service_count)
                            {{ $properties->contract_rel->total_free_maintenance - $service_count }}
                            @endif

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="m-2">
        <div class="p-1 mb-2 tblcolor text-white">Service Reports</div>
        <div>
            <table class="table table-bordered table-stripped">

                <thead>
                    <tr>
                    <th>Ordered Date</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>More Details</th>
                    </tr>


                </thead>
                <tbody>

                        @if ($services)
                            @foreach ($services as $service)
                                <tr>
                                    <td> {{ $service->date }} </td>
                                    <td> {{ isset( $service->service_related)? $service->service_related->service : "--"}}</td>
                                    <td>
                                        @if($service->status)
                                        @if($service->status == '0' )
                                        {{'Requested'}}
                                        @endif
                                        @if($service->status == '2' )
                                        {{ 'Paid' }}
                                        @endif
                                        @if($service->status == '3' )
                                        {{ 'Completed' }}
                                        @endif
                                        @if($service->status == '4' )
                                        {{ 'Cancelled' }}
                                        @endif
                                        @endif
                                    </td>
                                    <td>
                                        {{ $service->date }}
                                    </td>
                                    <td>
                                        @if($service->service_payemnt_doc)
                                         <a href="{{ $service->service_payemnt_doc->document }}">view</a>

                                         @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif


                </tbody>

            </table>
        </div>
    </div>
</body>

</html>
