<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vacant Report</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .tblcolor{
            background-color: #3a579e;
        }
        body{
            font-size: 12px;
        }
    </style>
</head>
<body>
    <h2 class="text-center">Vacant Report</h2>
    <table class="table table-bordered table-striped">
        <thead>
            <th>Id</th>
            <th>Property Name</th>
            <th>Unit Name</th>
            <th>Furnished</th>
            <th>Category</th>
            <th>Frequency</th>
            <th>City|State</th>
          
        </thead>
        <tbody>
            @foreach ($proprtyvacateReport as $property)
            <tr>
                <td>{{$property->id}}</td>
                @if ($property->builder_id != null)
                            @php
                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $property->builder_id)->first();
                            @endphp
                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                        <td>{{ $property->property_name }}</td>
                    @else
                        <td>{{ $property->property_name }}</td>
                        <td>--</td>
                    @endif
                    @if($property->furnished== '0')
                    <td>Not Furnished</td>
                    @elseif($property->furnished== '1')
                    <td>Semi Furnished</td>
                    @elseif($property->furnished== '2')
                    <td>Fully Furnished</td>
                    @else
                    <td>--</td>
                    @endif
                    @if ($property->category=='0')
                    @if($property->property_to=='0')
                    <td><span class="badge badge-danger">Residential</span>| <span class="badge badge-warning">Rent</span></td>
                    @else
                    <td><span class="badge badge-danger">Residential</span>|<span class="badge badge-warning"> Buy</span></td>
                    @endif
                       
                       @elseif ($property->category=='1')
                       @if($property->property_to=='0')
                       <td><span class="badge badge-danger">Commercial</span>|<span class="badge badge-warning">Rent</span></td>
                       @else
                       <td><span class="badge badge-danger">Commercial</span><span class="badge badge-warning">|Buy</span></td>
                       @endif
                      
                       @else
                       <td>---</td> 
                    @endif
                   
                    <td>{{isset($property->frequency_rel->type)?$property->frequency_rel->type:"--"}}</td>
                    <td>{{$property->city_rel->name}}|{{$property->state_rel->name}}</td>
                   
            </tr>
            @endforeach
            
        </tbody>

    </table>
</body>
</html>