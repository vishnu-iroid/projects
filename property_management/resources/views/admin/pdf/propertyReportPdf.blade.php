<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Property Report</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .tblcolor {
            background-color: #3a579e;
        }

        body {
            font-size: 12px;
        }

    </style>
</head>

<body>
    <h1 class="text-center">Property Report</h1>
    {{-- <table class="table table-bordered table-striped">
        <thead>
            <th>Id</th>
            <th>Property Name</th>
            <th>Unit Name</th>
            <th>Furnished</th>
            <th>Category</th>
            <th>Status</th>
            <th>Frequency</th>
            <th>City|State</th>
           
        </thead>
        <tbody>
           
            <tr>
                <td>{{$property->id}}</td>
                @if ($property->builder_id != null)
                            @php
                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $property->builder_id)->first();
                            @endphp
                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                        <td>{{ $property->property_name }}</td>
                    @else
                        <td>{{ $property->property_name }}</td>
                        <td>--</td>
                    @endif
                    @if ($property->furnished == '0')
                    <td>Not Furnished</td>
                    @elseif($property->furnished== '1')
                    <td>Semi Furnished</td>
                    @elseif($property->furnished== '2')
                    <td>Fully Furnished</td>
                    @else
                    <td>--</td>
                    @endif
                    @if ($property->category == '0')
                    @if ($property->property_to == '0')
                    <td><span class="badge badge-danger">Residential</span>| <span class="badge badge-warning">Rent</span></td>
                    @else
                    <td><span class="badge badge-danger">Residential</span>|<span class="badge badge-warning"> Buy</span></td>
                    @endif
                       
                       @elseif ($property->category=='1')
                       @if ($property->property_to == '0')
                       <td><span class="badge badge-danger">Commercial</span>|<span class="badge badge-warning">Rent</span></td>
                       @else
                       <td><span class="badge badge-danger">Commercial</span><span class="badge badge-warning">|Buy</span></td>
                       @endif
                      
                       @else
                       <td>---</td> 
                    @endif
                    @if ($property->occupied == '0')
                    <td><span class="badge badge-success">Vacant</span></td>
                    @else
                    <td><span class="badge badge-info">Occupied</span></td>
                    @endif
                    <td>{{isset($property->frequency_rel->type)?$property->frequency_rel->type:"--"}}</td>
                    <td>{{$property->city_rel->name}}|{{$property->state_rel->name}}</td>
                    <td></td>
            </tr>
          
            
        </tbody>

    </table> --}}


    <div class="row">
        <div class="col-sm-5 col-md-6">
            <p>Property Id:{{ $property->id }}</p>
            <p>Property Register Number:{{ $property->property_reg_no }}</p>
            <p>Property Name:{{ $property->property_name }}</p>
            <p>Owner Name:{{ $property->owner_rel->name }}</p>
            <p>Owner Phone:{{ $property->owner_rel->phone }}</p>
            <p>Owner Email:{{ $property->owner_rel->email }}</p>
            <p>Owner Address:{{ $property->owner_rel->address }}</p>
        </div>
    </div>

    <div>
        
        @if ($property->is_builder > 0 && $property->unit_num > 0)
        <h1>Building Units</h1>
           
            <table class="table table-bordered">
                <thead>
                    <th>Id</th>
                    {{-- <th>Registeration No.</th> --}}
                    <th>Property Name</th>
                    <th>Property To</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Furnished</th>
                    <th>Lease Type</th>
                    <th>Token Amount</th>
                    <th>Rent</th>
                    <th>Deposit</th>
                    
                </thead>
                <tbody>
            @foreach ( $units as $unit)
                <tr>
                    <td>{{$unit->id}}</td>
                    {{-- <td>{{$unit->property_reg_no }}</td> --}}
                    <td>{{$unit->property_name}}</td>
                    @if($unit->property_to==0)
                    <td>Rent</td>
                    @else
                    <td>Buy</td>
                    @endif
                    @if($unit->category==0)
                    <td>Residential</td>
                    @else
                    <td>Commercial</td>
                    @endif
                    @if($unit->occupied==0)
                    <td>Vaccant</td>
                    @else
                    <td>Occupied</td>
                    @endif
                    @if($unit->furnished==0)
                    <td>Not furnished</td>
                    @elseif($unit->furnished==1)
                    <td>Semi Furnished</td>
                    @else
                    <td>Fully furnished</td>
                    @endif
                    <td>{{$unit->frequency_rel->type}}</td>
                    <td>{{$unit->token_amount}}</td>
                    <td>{{$unit->rent}}</td>
                    <td>{{$unit->security_deposit}}</td>
                    
                </tr>
            @endforeach
        </tbody>
        </table>
        @else
            <h2>It's an Individual Property</h2>
        @endif
    </div>

</body>

</html>
