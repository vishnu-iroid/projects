<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rent Report</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .tblcolor {
            background-color: #3a579e;
        }

    </style>
</head>

<body>
    <div class="p-1 mb-2 tblcolor text-white">Property Details</div>
    <div>
        @if ($details->prop_rel->builder_id != null)
            @php
                $data = DB::table('owner_properties')
                    ->select('id', 'property_name')
                    ->where('id', $details->prop_rel->builder_id)
                    ->first();
            @endphp
            <p>Property Name:{{ isset($data->property_name) ? $data->property_name : '---' }}</p>
            <p>Property Name:{{ $details->prop_rel->property_name }}</p>
        @else
            <p>Property Name:{{ $details->prop_rel->property_name }}</p>
            <p>Unit:--</p>
        @endif


    </div>
    <div class="p-1 mb-2 tblcolor text-white">Owner Details</div>
    <div>
        <p>Owner Name:{{ $details->prop_rel->owner_rel->name }}</p>
        <p>Contact Number:{{ $details->prop_rel->owner_rel->phone }}</p>
        <p>Email:{{ $details->prop_rel->owner_rel->email }}</p>
        <p>Address:{{ $details->prop_rel->owner_rel->address }}</p>

    </div>
    <div class="p-1 mb-2 tblcolor text-white">Property Accounts Details</div>
    <div>
        <p>Lease Type:{{$details->prop_rel->freequecy_rel->type}}</p>
        @if($details->prop_rel->furnished== '0')
        <p>Furnished:Not Furnished</p>
        @elseif($details->prop_rel->furnished== '1')
        <p>Furnished:Semi Furnished</p>
        @elseif($details->prop_rel->furnished== '2')
        <p>Furnished:Fully Furnished</p>
        @endif
        
        <p>Security Deposit:{{$details->prop_rel->security_deposit}}</p>
        <p>Token Amount:{{$details->prop_rel->token_amount}}</p>
        <p>Contract Start Date:{{$details->prop_rel->contract_start_date}}</p>
        <p>Contract End Date:{{$details->prop_rel->contract_end_date}}</p>
        <p>Check In:{{$details->check_in}}</p>
        <p>Check Out:{{$details->check_out}}</p>
        <p>Received Rent:{{$details->rent1_rel->amount}}</p>
        <p>Net Amount:{{$netamount}}</p>
        <p>Balance:{{$balance}}</p>
    </div>
</body>

</html>
