<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Siaha</title>
    <style>
        body{
            font-family: 'Segoe UI', sans-serif;

        }
        .wrap{
            /* border: 1px solid rgba(219, 219, 219, 0.623); */
            margin: 5%;
            /* width: 800px; */
        }
        p{
            line-height: 30px;
            margin-top: 0;
        }
        .wrap_txt{
            margin: 0 auto;
        }
        .discrip h4{
            margin-bottom: 5px;  
        }
        h4{
            
        }
        ul.features{
            padding-left: 30px;
            line-height: 30px;
        }
        h3.name{
            padding: 10px;
            border: 1px solid rgba(219, 219, 219, 0.623);
            text-align: center;
            font-size: 22px;
            font-weight: 500;

        }
        h4 span{
            font-size: 14px;
            color: darkgray
            
        }
        h4{
            font-size: 20px;
            font-weight: 500;
        }
        .text-center{
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="wrap">
    <div class="" style="padding: 30px;">
        
   
    <div class="wrap_txt">
        <h3 class="name text-center">Property Report - {{$month_year}}</span></h3>

        @if($property->property_name)
            <h3 class="text-center">Property Name : <span>{{$property->property_name}}</span></h3>
        @endif
        @if($property->property_reg_no)
            <h4 class="text-center">Property Code : {{$property->property_reg_no}}</h4>
        @endif
        @if($property->property_to == 1)
            <h5 class="text-center" style="color: darkred;">Appartment for Sale  </h5>
        @else
            <h5 class="text-center" style="color: darkred;">Appartment for Rent  </h5>
        @endif
        <h3 class="text-center">Revenue : <span>{{$revenue}}</span></h3>
        <h3 class="text-center">Expense : {{$expense}}</h3>

</div>
</div>
   
</div>
</body>