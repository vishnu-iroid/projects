@extends('admin.layout.app') 
<style>
    .modal-busy {
        position:fixed;
        z-index:999;
        height:100%;
        width:100%;
        top:0;
        left:0;
        background-color:black;
        filter:alpha(opacity=60);
        opacity:0.6;
        -moz-opacity:0.8;
    }
    .center-busy {
        z-index:1000;
        margin:300px auto;
        padding:0px;
        width:130px;
        background-color:black;
        filter:alpha(opacity=100);
        opacity:1;
        -moz-opacity:1;
    }
    .center-busy img {
        height:130px;
        width:130px;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Assign Property Page</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Assign Property  Page</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table table-bordered table-hover col-md-12 col-lg-12 col-sm-12">
                        <thead>
                            <th class="wd-5p">Id</th>
                            <th class="wd-5p">Property Reg No</th>
                            <th class="wd-5p">Property Type</th>
                            <th class="wd-5p">Building Type</th>
                            <th class="wd-5p">Address</th>
                            <th class="wd-5p">Price</th>
                            <th class="wd-5p">Action</th>
                        </thead>
                        @foreach ($property as $row)
                        <tbody>
                            <input type="hidden" value="{{ $row->id }}" id="assign_property_id" />
                            <td class="wd-5p">{{ $loop->iteration }}</td>
                            <td class="wd-5p">{{ $row->property_reg_no }}</td>
                            <td class="wd-5p">{{ $row->property_to }}</td>
                            <td class="wd-5p">{{ $row->category }}</td>
                            <td class="wd-5p">{{ $row->street_address_1 }}</td>
                            <td class="wd-5p">{{ $row->selling_price }}</td>
                            <td class="wd-5p"><input type="checkbox"  value="{{ $row->id }}"  class="assign-check" @if ($row->assigned_user_desired_property !== null) checked @endif /></td>
                        </tbody>
                            <input type="hidden" value="{{ request()->route('id') }}" id="desired_property_id" />
                        @endforeach
                    </table>
                    <div class="modal-busy" style="display:none">
                        <div class="center-busy">
                         <img src="{{ asset('gif/spinner.gif') }}" />
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                    </div>
                </div>
                <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
@endsection
@section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script>
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
$(document).ready(function(){
    $('.assign-check').on('change',function () {
        let desired_property_id = $("#desired_property_id").val();
        let assign_property_id = $(this).val();
        if ($(this).prop('checked')==false){ 
            $.ajax({
                    type: "post",
                    url: "{{route('deleteAssignedList')}}",
                    data: { assign_property_id : assign_property_id },
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                success:function(data){
                    alert('Deleted Successfully');
                }
            });
        }else{
            $.ajax({
                    type: "post",
                    url: "{{route('saveToAssignedList')}}",
                    data: { desired_property_id : desired_property_id , assign_property_id : assign_property_id },
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                beforeSend:function(){
                    $(".modal-busy").show();
                },
                success:function(data){
                    $(".modal-busy").hide();
                }
            });
        }
    });
});
   
    
</script>
@endsection