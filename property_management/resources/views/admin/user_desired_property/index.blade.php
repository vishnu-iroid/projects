@extends('admin.layout.app') 
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Desired Property Details</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
                    <div class="card-header card-header-blueheader">
                        <div class="row d-flex justify-content-between">
                            <div class="col-md-4">
                                <h4 class="card-title h4-card">Desired Property List</h4>
                                <p class="card-category">List of all Requested Users</p>
                            </div>
                        </div>
                    </div>
                    <table id="datatable" class="table table-hover table-bordered display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-10p">Request Id no</th>
                                <th class="wd-5p">User Id</th>
                                <th class="wd-5p">Property Type</th>
                                <th class="wd-5p">Building Type</th>
                                <th class="wd-5p">Category</th>
                                <th class="wd-5p">Area</th>
                                <th class="wd-10p">Requested Date</th>
                                <th class="wd-10p">Min Price</th>
                                <th class="wd-10p">Max Price</th>
                                <th class="wd-10p">Latitude</th>
                                <th class="wd-10p">Longitude</th>
                                <th class="wd-20p">Description</th>
                                <th class="wd-5p">Status</th>
                                <th class="wd-15p">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($user_desired_property as $row)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$row->request_id_no}}</td>
                                <td>{{$row->user_id}}</td>
                                <td>{{$row->property_to}}</td>
                                <td>{{$row->category}}</td>
                                <td>{{$row->type_id}}</td>
                                <td>{{$row->area}}</td>
                                <td>{{$row->request_date}}</td>
                                <td>{{$row->min_price}}</td>
                                <td>{{$row->max_price}}</td>
                                <td>{{$row->latitude}}</td>
                                <td>{{$row->longitude}}</td>
                                <td>{{$row->description}}</td>
                                <td>{{$row->status}}</td>
                                <td>
                                    <a href="{{ url('assign-desired-property/'.$row->id) }}" class="" href="javascript:0;" title="Assign a property" style="color: blue "></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    </div>
                <!-- table-wrapper -->
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
@endsection