@extends('admin.layout.app')
<link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
<style>
    .ct-series-a .ct-line{
     stroke: #ffffff !important;
    }
    .ct-area {
   stroke: none;
   fill-opacity: 2.1;
}
.ct-series-a .ct-area{
 fill: none !important;
}
.ct-series-a .ct-point{
 stroke: #ffffff;
}

    .card {
       position: relative;
       display: flex;
       flex-direction: column;
       min-width: 0;
       word-wrap: break-word;
       background-color: #f8f9fa !important;
       background-clip: border-box;
       border: 1px solid #eee;
       border-radius: 3px !important;
   }
   .card-header-b{
     height : 300px !important;
   }
   .properties-details{
    height: 55px;
     border-radius: 3px;
     position: relative;
   }
   .property-total-count{


   }
   .property-total-count p{
     margin: 0;
   }
   .all-property-btn{
     color: #434ba0;
   }
   .fa-chevron-right{
     font-size: 10px;
   }
   .count-seperation{
     background: #e6f3ff8f;
    
   }
   .count-card{
     border-right: solid 1px rgb(224, 224, 224);
     padding: 10px;
     text-align: center;
   }
   .count-card-last{
     text-align: center;
     padding: 10px;
   }
   .count-card p {
   margin: 0;
}
.count-card-last p {
   margin: 0;
}
.download-report-btn{
 background: #b3b9f633;
 padding: 10px;
 border-radius: 5px;
}
.nav-tabs{
 border: none !important;
}
.nav-link{
 border: none !important;
}
.nav-tabs .nav-link.active {
   color: #363636 !important;
   font-weight: 800;
   background-color: transparent !important;
}
.nav-tabs a{
 color: #a9a9a9;
 font-weight: 800;
}
.tab-details{
 border: solid 1px #b4baff;
 border-radius: 5px;
 padding: 15px;
 position: relative;
 margin-left: 20px;
 margin-right: 20px;
}
.tab-detailstwo{
 border: solid 1px #ffd9b4;
 border-radius: 5px;
 padding: 15px;
 position: relative;
 margin-left: 20px;
 margin-right: 20px;
}
.tab-detailstwo h4{
 color: #ec8116;
}
.tab-details h4{
 color: #434ba0;
}
.tab-arrow{
 position: absolute;
   top: 8px;
   /* left: 0; */
   right: 15px;
}
.property-section {
   padding: 30px 0px;
}
.DeepRed.rounded.overflow-hidden.shadow {
   background: linear-gradient(to right, #cb112e, #db25fc);
}
.properties-background{
 padding: 20px;

}
.all-property-btn{
 position:absolute;
 right: 15px;
}
.pie-chart{
 /* padding:  20px;
 height: 280px; */
}
.line-chart{
 padding: 0;
}
#canvas{
 width: 100%;
 height: 380px !important;
 padding: 20px;
}
.line-chart-main h4{
 
}
.card-title{
 padding: 15px;
}
  </style>
@section('content')
   <div class="br-pagetitle">
     <!-- <i class="icon fad fa-home tx-70 lh-0"></i> -->
     <div>
       <!-- <h4>Dashboard</h4> -->
       <!-- <p class="mg-b-0">Do bigger things with Bracket plus, the responsive bootstrap 4 admin template.</p> -->
     </div>
   </div><!-- d-flex -->

   <div class="br-pagebody">
     <div class="row row-sm mg-b-20">
       <div class="col-sm-6 col-xl-3">
         <div class="MalibuBeach rounded overflow-hidden shadow " style="height: 130px;">
         <!-- <div class="rounded overflow-hidden shadow" style="background-color: #1C658C; height: 130px;"> -->
           <a href="{{route('showUser')}}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <img src="{{asset('assets/images/group.png')}}" width="55" alt="">

               <!-- <i class="fad fa-plus-hexagon tx-60 lh-0 tx-white"></i> -->
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Users</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $users }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $users_active }} active users</span>
               </div>
             </div>
           </a>
           <div id="ch1" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="80">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
       <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
         <div class=" rounded overflow-hidden shadow" style="background-color: #00B4D8; height: 130px;">
           <a href="{{route('showOwner')}}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <!-- <i class="fad fa-user tx-60 lh-0 tx-white"></i> -->
               <img src="{{asset('assets/images/owner.png')}}" width="50" alt="">
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Owners</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $owners }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $owners_active }} active owners</span>
               </div>
             </div>
           </a>
           <div id="ch3" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="80">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
       <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
         <div class="HappyAcid rounded overflow-hidden shadow" style="height: 130px;">
         <!-- <div class= rounded overflow-hidden shadow" style="background-color: #203239; height: 130px;"> -->
           <a href="{{ route('showAgent') }}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <!-- <i class="fad fa-chevron-right tx-60 lh-0 tx-white"></i> -->
               <img src="{{asset('assets/images/owner (1).png')}}" width="50" alt="">
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Agents</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $agents }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $agents_active }} active agents</span>
               </div>
             </div>
           </a>
           <div id="ch2" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="80">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
       <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
         <!-- <div class= "rounded overflow-hidden shadow" style="background-color: #E04DB0; height: 130px;"> -->

         <div class="DeepBlue rounded overflow-hidden shadow" style="height: 130px;">
           
           <a href="{{ route('showProvider') }}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <!-- <i class="fad fa-handshake tx-60 lh-0 tx-white"></i> -->
               <img src="{{asset('assets/images/customer-service.png')}}" width="45" alt="">
               <div class="mg-l-20">
                 <p class="tx-12 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Service Providers</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $service_provider }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $service_provider }} active</span>
               </div>
             </div>
           </a>
           <div id="ch4" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="80">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->


     </div><!-- row -->

     <div class="row mg-t-40">
       <div class="col-md-6">
         <div class="card card-chart shadow" data-count="0">
             <div data-header-animation="true" class="card-header-b card-header-success">
               <div id="dailySalesChart" class="ct-chart">

               </div>
             </div>
             <div class="card-body">
               <h4 class="">Total Income</h4>
               <p>{{ $total_income}}</p>
               {{-- <p class="card-category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> --}}
             </div>
             {{-- <div class="card-footer">
               <div class="stats"><i class="fad fa-clock"></i> updated 4 minutes ago </div>
             </div> --}}
         </div>
       </div>

       <div class="col-md-6">
        {{-- <div class="card card-chart shadow" data-count="0">
            <div data-header-animation="true" class="card-header-b card-header-success">
              <div id="dailySalesChart" class="ct-charttwo">

              </div>
            </div>

            <div class="card-body">
              <h4 class="">Property Reservation Fee</h4>
              <!-- <p class="card-category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p> -->
            </div>

            <div class="card-footer">
              <div class="stats"><i class="fad fa-clock"></i> updated 4 minutes ago </div>
            </div>
        </div> --}}
        <div class="line-chart-main bg-light shadow" style="border-radius: 5px;">
        <div class="line-chart ">

          <canvas id="canvas"></canvas>

        </div>
        <div class="">
        <h4 class=""  style="padding: 0 0 10px 20px;">Property Reservation Fee</h4>
      </div>
        </div>
      </div> 

       <div class="col-md-12 mt-4">
           <div class="row">
             {{-- <div class="col-md-6    w-full">
               <div class="property-section bg-light rounded ">
               <div class="properties-details-section">
                 <div class="properties-details d-flex">


                   <div class="download-report-btn  mg-l-20 tx-bold"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download Reports</div>
                 </div>




             </div>


             <ul class="nav nav-tabs" id="myTab" role="tablist">
               <li class="nav-item" role="presentation">
                 <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Week</a>
               </li>
               <li class="nav-item" role="presentation">
                 <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Month</a>
               </li>
               <li class="nav-item" role="presentation">
                 <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Year</a>
               </li>
             </ul>
             <div class="tab-content" id="myTabContent">
               <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                 <div class="tab-details mt-4">
                     <h4>$46,960</h4>
                     <h5>Money in <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #1caf9a; font-size: 20px; transform: rotate(30deg);"></i> <span style="color: #1caf9a;">5.8%</span></h5>
                     <h5 style="color: #c50505;">COMMISSION / SERVICE FEE</h5>
                     <div class="tab-arrow">
                       <i class="fas fa-chevron-right" style="color: #434ba0; font-size: 18px;"></i>
                     </div>
                 </div>
                 <div class="tab-detailstwo mt-3">
                   <h4>$46,960</h4>
                   <h5>Money out <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #ee3f0a; font-size: 20px; transform: rotate(140deg);"></i> <span style="color: #ee3f0a;">5.8%</span></h5>
                   <h5 style="color: #c50505;">COMMISSION / SERVICE FEE</h5>
                   <div class="tab-arrow">
                     <i class="fas fa-chevron-right" style="color: #ec8116; font-size: 18px;"></i>
                   </div>
                 </div>

               </div>
               <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="tab-details mt-4">
                   <h4>$46,960</h4>
                   <h5>Money in <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #1caf9a; font-size: 20px; transform: rotate(30deg);"></i> <span style="color: #1caf9a;">5.8%</span></h5>
                   <h5 style="color: #c50505;">COMMISSION / SERVICE FEE</h5>
                   <div class="tab-arrow">
                     <i class="fas fa-chevron-right" style="color: #434ba0; font-size: 18px;"></i>
                   </div>
               </div>
               <div class="tab-detailstwo mt-3">
                 <h4>$46,960</h4>
                 <h5>Money out <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #ee3f0a; font-size: 20px; transform: rotate(140deg);"></i> <span style="color: #ee3f0a;">5.8%</span></h5>
                 <h5 style="color: #c50505;">COMMISSION / SERVICE FEE</h5>
                 <div class="tab-arrow">
                   <i class="fas fa-chevron-right" style="color: #ec8116; font-size: 18px;"></i>
                 </div>
               </div>
               </div>
               <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                 <div class="tab-details mt-4">
                   <h4>$46,960</h4>
                   <h5>Money in <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #1caf9a; font-size: 20px; transform: rotate(30deg);"></i> <span style="color: #1caf9a;">5.8%</span></h5>
                   <h5 style="color: #c50505;">COMMISSION / SERVICE FEE</h5>
                   <div class="tab-arrow">
                     <i class="fas fa-chevron-right" style="color: #434ba0; font-size: 18px;"></i>
                   </div>
               </div>
               <div class="tab-detailstwo mt-3">
                 <h4>$46,960</h4>
                 <h5>Money out <i class="fa fa-arrow-circle-up" aria-hidden="true" style="color: #ee3f0a; font-size: 20px; transform: rotate(140deg);"></i> <span style="color: #ee3f0a;">5.8%</span></h5>
                 <h5 style="color: #c50505;">COMMISSION / SERVICE FEE</h5>
                 <div class="tab-arrow">
                   <i class="fas fa-chevron-right" style="color: #ec8116; font-size: 18px;"></i>
                 </div>
               </div>
               </div>
             </div>

               </div>
             </div> --}}


             <div class="col-lg-4 mb-3 col-md-6">
               <div class="properties-background bg-light">
               <div class="properties-details d-flex align-items-center">
                 <img src="/uploads/amenities/properties.png" style="height: 40px;">
                 <div class="property-total-count mg-l-15">
                     <p class="tx-22 text-dark tx-bold">{{$properties}}</p>
                     <p class="">Properties</p>
                 </div>
                 <div class="all-property-btn  mg-l-20"><a href="{{route('showPropertyManage')}}">See all Properties <i class="fas fa-chevron-right"></i></a></div>
               </div>
               <hr>

               <div class="count-seperation mt-3">
                
                   <div class="count-card">
                     <p class="tx-24 text-dark tx-bold">{{$propertie_vacant}}</p>
                     <p>Vacant</p>
                   </div>
                  
                     <div class="count-card">
                       <p class="tx-24 text-dark tx-bold">{{$propertie_occupied}}</p>
                       <p>Occupied</p>
                     </div>
                  

                  
                       <div class="count-card-last">
                         <p class="tx-24 text-dark tx-bold">{{$properties-($propertie_vacant+$propertie_occupied)}}</p>
                         <p>Unlisted</p>
                       </div>
                       </div>
                
               


               </div>
           </div>



          
           <div class="col-lg-4 mb-3 col-md-6  rounded ">
             <div class="properties-background bg-light">

             <div class="properties-details d-flex align-items-center">

               <div class="d-flex align-items-center">
                <img src="/uploads/amenities/employees.png" style="height: 40px">
                <div class="property-total-count mg-l-15">
                    <p class="tx-24 text-dark tx-bold">{{$tenant}}</p>
                    <p class="">Tenants</p>
                </div>
               </div>

               <div class="all-property-btn  mg-l-20"><a href="{{route('showTenants')}}">See all Tenanats <i class="fas fa-chevron-right"></i></a></div>
             </div>

             <hr>

             <div class="count-seperation mt-3">
  
                 <div class="count-card">
                   <p class="tx-24 text-dark tx-bold">{{$tenant_rental}}</p>
                   <p>Rental</p>
                 </div>
             

               
                   <div class="count-card">
                     <p class="tx-24 text-dark tx-bold">{{$tenant_owner}}</p>
                     <p>Owned</p>
                   </div>
             
                     {{-- <div class="count-card-last">
                       <p class="tx-24 text-dark tx-bold ">{{$employee_doc}}</p>
                       <p class="">Documentation</p>
                     </div> --}}
                    
               </div>
             </div>
             </div>
         

         <div class="col-lg-4 mb-3 col-md-12"> 
          <div class="properties-background bg-light">
            <div class="properties-details d-flex align-items-center">
              <img src="/uploads/amenities/realtor.png" style="height: 40px;">
              <div class="property-total-count mg-l-15">
                  <p class="tx-15">Agents Pay Requests</p>
              
              </div>
            </div>
            <hr>
           <div class="pie-chart">
             <h5 class="pb-2 tx-primary"></h5>
             <canvas id="myChart"  ></canvas>
           </div>
           </div>
           </div>

           </div>
       </div>
     </div>
   </div>
     </div>
   </div><!-- br-pagebody -->
 @endsection
 @section('scripts')
 <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
 <script>
       let Lbls ;
      //  ['2021-09-08', '2021-09-08', '2021-09-08', '2021-09-08', '2021-09-08','2021-09-08','0000-00-00','aug','sep','oct','nov','dec'];
      let Amounts ;
      // [120, 90, 105, 85, 65, 90, 78, 110, 40, 65, 55, 40];

      $.ajax({
            url:  "/dashboard/chart",
            method: 'GET',
            async: true,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
              console.log(data);
               Lbls = data.charts.date;
               Amounts = data.charts.amount;
                var myChart = new Chartist.Line('.ct-chart',{
                    labels: Lbls, 
                    series: [
                      Amounts,
                    ]
                },  
                {
                  low: 0,
                  showArea: true,
                  showPoint: true,
                  fullWidth: true
                });
                myChart.on('draw', function(data) {
                  if(data.type === 'line' || data.type === 'area') {
                    data.element.animate({
                      d: {
                        begin: 2000 * data.index,
                        dur: 2000,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: Chartist.Svg.Easing.easeOutQuint
                      }
                    });
                  }
                });
            },
        });

 </script>


{{-- <script>
var myChart = new Chartist.Line('.ct-charttwo',{
   labels: ['jan', 'feb', 'mar', 'apr', 'may','jun','jul','aug','sep','oct','nov','dec'],
   series: [
     [120, 90, 105, 85, 65, 90, 78, 110, 40, 65, 55, 40]
   ]
},{
 low: 0,
 showArea: true,
 showPoint: true,
 fullWidth: true
});
myChart.on('draw', function(data) {
 if(data.type === 'line' || data.type === 'area') {
   data.element.animate({
     d: {
       begin: 2000 * data.index,
       dur: 2000,
       from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
       to: data.path.clone().stringify(),
       easing: Chartist.Svg.Easing.easeOutQuint
     }
   });
 }
});
</script> --}}


 <script>
     $('#site_title').html(`Property | Dashboard`)
     $('#dashboard').addClass(`active`)
 </script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
type: 'pie',
data: {
 labels: ["Requested", "Accepted", "Rejected"],
 datasets: [{
   backgroundColor: [
     "#34495e",
     "#2ecc71",
     "#e74c3c"
   ],
   data: [12, 19,  7]
 }]
}
});
</script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
<script>
  var lineChartData = {
    labels: ["Data 1", "Data 2", "Data 3", "Data 4", "Data 5", "Data 6", "Data 7"],
    datasets: [{
        fillColor: "rgba(151,187,205,0)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        data: [20, 30, 80, 20, 40, 10, 60]
    },]

}

Chart.defaults.global.animationSteps = 50;
Chart.defaults.global.tooltipYPadding = 16;
Chart.defaults.global.tooltipCornerRadius = 0;
Chart.defaults.global.tooltipTitleFontStyle = "normal";
Chart.defaults.global.tooltipFillColor = "rgba(0,160,0,0.8)";
Chart.defaults.global.animationEasing = "easeOutBounce";
Chart.defaults.global.responsive = true;
Chart.defaults.global.scaleLineColor = "gray";
Chart.defaults.global.scaleFontSize = 16;

var ctx = document.getElementById("canvas").getContext("2d");
var LineChartDemo = new Chart(ctx).Line(lineChartData, {
    pointDotRadius: 10,
    bezierCurve: false,
    scaleShowVerticalLines: false,
    scaleGridLineColor: "gray"
});
</script>

 @endsection
