@extends('admin.layout.app')
 <style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .select2-container {
      z-index:10050; width: 100% !important; padding: 0;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Set Up Management</span>
        <span class="breadcrumb-item active">City & Pincode</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
<div class="row">
    <div class="col-md-9">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">City</h4>
                        <p class="card-category">List of all city</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive ">
                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-15p">City</th>
                            <th class="wd-15p">State</th>
                            <th class="wd-15p">Country</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cities as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->state_rel->name }}</td>
                            <td>{{ $row->country_rel->name }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-dataid="{{$row->id}}" data-url="/city/view/" title="Edit"></a>
                                <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deleteCityFunction('{{$row->id}}')"></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $cities->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <div class="col-md-3">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-12">
                        <h2 class="card-title" style="font-size: 1.125rem;font-weight: 300 !important;">Add City</h2>
                        <!-- <p class="card-category"></p> -->
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form class="form-clients" id="form-add-city" action="javascript:;">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name <span class="tx-danger">*</span></label>
                            <input class="form-control" id="cityId" name="cityId" type="hidden"/>
                            <input class="form-control" id="city_name" name="city_name" type="text" placeholder="Enter city name"/>
                        </div>
                        <div class="form-group">
                            <label>State <span class="tx-danger">*</span></label>
                            <select name="city_state" id="city_state" class="form-control select2">
                            @foreach($states as $row5)
                                <option value="{{ $row5->id }}">{{$row5->name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary addBtn" id="cityBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Pincodes</h4>
                        <p class="card-category">List of all pincode</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive ">
                    <thead>
                        <tr>`
                            <th class="wd-5p">Id</th>
                            <th class="wd-15p">Pincode</th>
                            <th class="wd-15p">City</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pincodes as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->pincode }}</td>
                            <td>{{ $row->city_rel->name }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-dataid="{{$row->id}}" data-url="/pincodes/view/" title="Edit"></a>
                                <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deletePincodeFunction('{{$row->id}}')"></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $pincodes->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <div class="col-md-4">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-12">
                        <h2 class="card-title" style="font-size: 1.125rem;font-weight: 300 !important;">Add Pincode</h2>
                        <!-- <p class="card-category"></p> -->
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form class="form-clients" id="form-add-pincode" action="javascript:;">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Pincode <span class="tx-danger">*</span></label>
                            <input class="form-control" id="pincodeId" name="pincodeId" type="hidden"/>
                            <input class="form-control" id="pincode" name="pincode" type="text" placeholder="Enter pincode"/>
                        </div>
                        <div class="form-group">
                            <label>City <span class="tx-danger">*</span></label>
                            <select name="city_pin" id="city_pin" class="form-control select2">
                            <option value=""></option>
                            @foreach($all_cities as $row6)
                                <option value="{{ $row6->id }}">{{$row6->name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary addBtn" id="btnPincode">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Reject Modal city -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger">Delete City</h4>
                </div>
                <form action="javascript:;" id="form-delete-city">
                    <div class="modal-body">
                        Are you sure you want to delete this City?
                        <input type="hidden" name="cId" id="cId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary closeBtn" onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtncity">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Reject Modal pincode -->
    <div class="modal fade" id="deleteModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger">Delete Pincode</h4>
                </div>
                <form action="javascript:;" id="form-delete-pin">
                    <div class="modal-body">
                        Are you sure you want to delete this Pincode?
                        <input type="hidden" name="pinId" id="pinId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary closeBtn" onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtnpin">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
</script>
<script>
    function deleteCityFunction(id){
        $('#cId').val(id);
        $('#deleteModal').modal()
    }
    function deletePincodeFunction(id){
        $('#pinId').val(id);
        $('#deleteModal2').modal()
    }
</script>
<script>
    $("#site_title").html(`Property | Region Mangement`);
    $("#region_nav").addClass("active");
    $("#city_pincode_nav").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
    $(function () {
        "use strict";
        $("#datatable2").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>

    $('tbody').on('click','.edit-button',function(){
        var dataId = $(this).data('dataid')
        var toURL =  $(this).data('url')
        $.ajax({
            url : url+toURL+dataId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    if(data.type == "city"){
                        $('#cityId').val(data.response.id)
                        $('#city_name').val(data.response.name)
                        $('#city_state').val(data.response.state).trigger('change')
                    }else{
                        $('#pincodeId').val(data.response.id)
                        $('#pincode').val(data.response.pincode)
                        $('#city_pin').val(data.response.city).trigger('change')
                    }
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })
    //Delete Function City
    $('.deletetBtncity').on('click',function(){
        var id = $('#cId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url+"/city/delete/"+id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                $(".deletetBtn").html('Deleting..');
            },
            success: function (data) {
                $(".deletetBtn").text('Delete');
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function () {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })
    //Delete Function Pin
    $('.deletetBtnpin').on('click',function(){
        var id = $('#pinId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url+"/pincode/delete/"+id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                $(".deletetBtn").html('Deleting..');
            },
            success: function (data) {
                $(".deletetBtn").text('Delete');
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function () {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    $("#form-add-city").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            city_name: {
                required: true,
            },
            city_state: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-city");
            var formData = new FormData(form);
            $("#cityBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{ route('addCity') }}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $("#cityBtn").text('Processing');
                },
                success: function (data) {
                    $("#cityBtn").text('Submit');
                    $("#cityBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });

    $("#form-add-pincode").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            pincode: {
                required: true,
            },
            city_pin: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-pincode");
            var formData = new FormData(form);
            $("#btnPincode").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{ route('addPincodes') }}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $("#btnPincode").text('Processing');
                },
                success: function (data) {
                    $("#btnPincode").text('Submit');
                    $("#btnPincode").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
