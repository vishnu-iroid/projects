@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .ui-datepicker {
        position: fixed;
        top: 270.797px;
        left: 172.5px;
        z-index: 5001 !important;
        display: block;
    }

    #ui-datepicker-div {
        z-index: 5001 !important;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00)!important;
        height: 20px;
        border-radius: 5px !important
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding: 10px;
        border-radius: 10px;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Property Management</span>
            <span class="breadcrumb-item active">Add Landloards|Owners</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Add Landloards|Owners</h4>
                            <p class="card-category">List of all owner</p>
                        </div>
                        <div class="pd-5 mg-r-10">
                            <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal"
                                data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Owner</a>
                        </div>
                    </div>
                </div>
                {{-- <div class="card-body">
                    <div class="row">
                        <div id="datatableclient_filter" class="dataTables_filter col-md-6">
                            <center><label>PROPERTY</label></center>
                            <select class="form-control filter" id="property_id">
                                <option selected disabled>SELECT ONE</option>
                                @foreach ($types as $pr)
                                    <option value="{{ $pr->id }}">{{ $pr->type }}</option>
                                @endforeach
                            </select>

                        </div>
                        <div id="datatableclient_filter" class="dataTables_filter col-md-6">
                            <center><label>OWNER</label></center>
                            <select class="form-control filter" id="owner_id">
                                <option selected disabled>SELECT ONE</option>
                                @foreach ($owners as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div> --}}

                <div class="card-body">
                    <table id="datatableclient" class="table data-table" style="width:100%">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-20p">Name</th>
                                <th class="wd-15p">Email</th>
                                <th class="wd-10p">Phone</th>
                                <th class="wd-10p">Status</th>
                                <th class="wd-25p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>

                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>

    <!-- ----owner add modal---- -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myModalLabel">Add Owner</h4>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link modalnav active" id="profile-tab" data-toggle="tab" href="#profile"
                                role="tab" aria-controls="profile" aria-selected="true">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a id="bank-tab" class="nav-link modalnav" data-toggle="tab" href="#bank" role="tab"
                                aria-controls="bank" aria-selected="false">Bank Details</a>
                        </li>
                    </ul>
                </div>
                <form class="form-admin" id="form-add" action="javascript:;">
                    <input type="hidden" id="owner_id" name="owner_id">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name <span class="tx-danger">*</span></label>
                                            <input type="hidden" id="id" name="id">
                                            <input class="form-control" id="name" name="name" type="text"placeholder="Enter name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email <span class="tx-danger">*</span></label>
                                            <input class="form-control email" id="email" name="email" type="text"placeholder="Enter email" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Country<span class="tx-danger">*</span></label>
                                            <select class="form-control  country" name="country" >
                                                <option value="" selected disabled>Choose one</option>
                                                @foreach ($countries as $key => $country)
                                                    <option value="{{ $country->id }}">
                                                        {{ $country->name }}</option>
                                                    {{-- <option value="{{ $country->id }}" @if ($key == 0) selected @endif>
                                                        {{ $country->name }}</option> --}}
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>State<span class="tx-danger">*</span></label>
                                            <select class="form-control  state-select" name="state" id="state">
                                                <option value="" selected disabled>Choose one</option>
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City<span class="tx-danger">*</span></label>
                                            <select class="form-control  city-select city" name="city">
                                                <option value="" selected disabled>Choose one</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Zipcode
                                                {{-- <span class="tx-danger">*</span> --}}
                                            </label>
                                            <select class="form-control  zipcode-select zipcode" name="zipcode">
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address <span class="tx-danger">*</span></label>
                                    <textarea class="form-control" id="address" name="address" type="text"
                                        placeholder="Enter address"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone <span class="tx-danger">*</span></label>
                                            <input class="form-control" id="phone" name="phone" type="text"
                                                placeholder="Enter phone">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="status" id="status">
                                                <option value="0">Inactive</option>
                                                <option value="1" selected>Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>National ID <span class="tx-danger">*</span></label>
                                            <input class="form-control" id="nationalid" name="nationalid" type="file">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>WhatsApp Number
                                                {{-- <span class="tx-danger">*</span> --}}
                                            </label>
                                            <input class="form-control" id="whatsapp_number" name="whatsapp_number"
                                                type="text" placeholder="Enter WhatsApp Number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end-of-tab-1 -->
                            <!-- tab2 -->
                            <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Account Number <span class="tx-danger">*</span></label>
                                            <input class="form-control" id="account_number" name="account_number"
                                                type="text" placeholder="Enter account number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Bank Name<span class="tx-danger">*</span></label>
                                            <select name="bank_name" class="form-control " id="bank_name">
                                                <option></option>
                                                @foreach ($banks as $row)
                                                    <option value="{{ $row->id }}">{{ $row->bank_name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Branch Name<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="branch" name="branch" type="text"
                                                placeholder="Enter branch name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>IBAN<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="ifsc" name="ifsc" type="text"
                                                placeholder="Enter IBAN code">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();"> Close </button>
                        <button type="submit" class="btn btn-primary addBtn" id="ownerSubmitBtn" disabled>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- Reject Modal -->
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Block Owner</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to block this owner?
                        <input type="hidden" name="ownerId" id="ownerId">
                        <input type="hidden" name="action" id="action">
                        <!--Action -  1-unblock , 0- block -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtn">Block</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    {{--  property modal --}}

    <div class="modal fade" id="addPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="overflow: hidden">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myPropertyModalLabel">Add Property</h4>
                    {{-- <a href="" class="btn btn-primary float-right"  onclick="addOwnerBuilding()">Add as Building</a> --}}
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#pdetails"
                                role="tab" aria-controls="details" aria-selected="true">Details</a>
                        </li>
                        <li class="nav-item hide-element">
                            <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab"
                                aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                        </li>
                        <li class="nav-item hide-element">
                            <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities"
                                role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contracts"
                                role="tab" aria-controls="contract" aria-selected="false">Contract Details</a>
                        </li>
                        <li class="nav-item hide-element">
                            <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents"
                                role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a id="location-prop-tab" class="nav-link propertynav" data-toggle="tab" href="#location-prop"
                                role="tab" aria-controls="location-prop" aria-selected="false">Location</a>
                        </li>
                    </ul>
                </div>

                <form class="form-admin" id="form-add-property" action="javascript:;">

                    {{-- <form id="editform">
                        @csrf

                        <input type="hidden" name="" id="editing_id"> --}}
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pdetails" role="tabpanel"
                                aria-labelledby="details-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property Type<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="category" id="category">
                                               <option value="">select</option>
                                                <option value="1">Residential</option>
                                                <option value="2">Commercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Category<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 category-select" name="type" id="type">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="hidden" id="property_owner_id" name="property_owner_id">
                                        <div class="form-group">
                                            <label>Property For<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_to" id="property_to">
                                                <option value="">selct</option>
                                                <option value="0">Rent</option>
                                                <option value="1">Buy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property Name<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="property_name" name="property_name"
                                                type="text" placeholder="Enter Property Name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property Status<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_status" id="property_status">
                                                <option value="">status</option>
                                                <option value="0">Vacant</option>
                                                <option value="1">Occupied</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Country<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 country" name="property_country"
                                               ><option value="">Country</option>
                                                @foreach ($countries as $country)
                                                    <option value="{{ $country->id }}" >{{ $country->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>State<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 state-select" name="property_state"
                                                id="property_state">
                                                <option disabled selected value="">SELECT ONE</option>
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 city-select" name="property_city"
                                                id="property_city">
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Zipcode<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 zipcode-select" name="property_zipcode"
                                                id="property_zipcode">
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>National Address <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address1" name="address1" type="text"
                                                placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Secondary Address <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address2" name="address2" type="text"
                                                placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="description" name="description" type="text"
                                                placeholder="Description"></textarea>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Individual Property<span class="tx-danger">*</span></label>
                                            <input type="checkbox" name="individual_building" id="is_builder" value="1">
                                        </div>
                                    </div>
                                    <div class="col-md-6 is-building">
                                        <div class="form-group">
                                            <label>Images <span class="tx-danger">*</span></label><br>
                                            <input id="building_image" name="building_image[]" type="file"
                                                class="form-control file" multiple data-show-upload="true"
                                                data-show-caption="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row is-appartment">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. of Units<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="unit_num" name="unit_num" type="number"
                                                placeholder="Enter Unit">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. of floors<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="floor_num" name="floor_num" type="number"
                                                placeholder="Enter Floor">
                                        </div>
                                    </div>
                                </div>
                                <div class="amenities-row ">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Furnished <span class="tx-danger">*</span></label>
                                                <select class="form-control select2" name="furnished" id="furnished">
                                                    <option value="">select</option>
                                                    <option value="0">Not Furnished</option>
                                                    <option value="1">Semi Furnished</option>
                                                    <option value="2">Fully Furnished</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-warning tx-white mb-3"> &nbspDetails</div>
                                    <div class="row" id="details-section">
                                        @if ($details)
                                            @foreach ($details as $det)
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>{{ $det->name }}<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control"
                                                            name="detailsdata[{{ $det->detail_id }}]"
                                                            id="det-{{ $det->detail_id }}"
                                                            placeholder="{{ $det->placeholder }}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                                 <div class="float-right">
                                 <a class="btn btn-primary text-white tabto1" >Next</a>
                                 </div>
                                 <br>
                            </div>
                            <!-- end of tab1 -->
                            <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                                <div class="rentdetails">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Frequency<span class="tx-danger">*</span></label>
                                                <select class="form-control select2 rentfrequency" name="frequency"
                                                    id="frequency">
                                                    <option value="">Frequency</option>
                                                    @foreach ($frequency as $freq)
                                                        <option value="{{ $freq->id }}">{{ $freq->type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Rent<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="min_rent" name="min_rent"
                                                    placeholder="Enter rent">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Security Deposit<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="security_deposit"
                                                    id="security_deposit" placeholder="Enter Security deposit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sellingdetails">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Min Selling price<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="min_sellingprice"
                                                    name="min_sellingprice" placeholder="Enter minimum selling price">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Max Selling price<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="max_sellingprice"
                                                    name="max_sellingprice" placeholder="Enter maximum selling price">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="token-amount">
                                    <div class="row">
                                        <div class="col-md-6 token-hold">
                                            <div class="form-group">
                                                <label>Token Amount<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="token_amount"
                                                    id="token_amount" placeholder="Enter Token amount">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="float-right">
                                <a class="btn btn-primary text-white tabto2" >Next</a>
                                </div>
                                <br>
                            </div>
                            <div class="tab-pane fade" id="amenities" role="tabpanel" aria-labelledby="amenities-tab">
                                <div id="amenties_available">
                                    @if ($amenities_listing)
                                        @foreach ($amenities_listing as $row)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="card-warning tx-white mb-3"> &nbsp {{ $row->name }}
                                                        </div>
                                                        <div class="row mg-l-1">
                                                            @foreach ($row->amenities as $row2)
                                                                <label class="ckbox ml-2">
                                                                    <input type="checkbox" value="{{ $row2->id }}"
                                                                        name="amenities[]">
                                                                    <span>{{ $row2->name }}</span>
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="float-right">
                                <a class="btn btn-primary text-white tabto3" >Next</a>
                                </div>
                                <br>
                            </div>
                            <!-- end of tab3 -->
                            <!-- start tab 4 -->
                            <div class="tab-pane fade" id="contracts" role="tabpanel" aria-labelledby="contract-tab">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contract No</label>
                                            <input type="text" class="form-control" name="contract_no" id="contract_no">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contract File<span class="tx-danger">*</span></label>
                                            <input type="file" class="form-control" name="contract_file"
                                                id="contract_file">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ownership Doc<span class="tx-danger">*</span></label>
                                            <input type="file" class="form-control" name="ownership_doc"
                                                id="ownership_doc">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract Start Date<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker"
                                                name="contract_start_date" id="contract_start_date" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract End Date<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker" name="contract_end_date"
                                                id="contract_end_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Commission in terms of :-<span
                                                    class="tx-danger">*</span></label><br>
                                            <label>Amount</label>
                                            <input type="radio" checked name="commision_as" id="commission_as"
                                                value="amount">
                                            <label>Percentage</label>
                                            <input type="radio" name="commision_as" id="commission_as" value="percentage">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Commission<span class="tx-danger"
                                                    style="font-size:12px;">*</span></label>
                                            <input type="number" class="form-control" name="owner_amount"
                                                id="owner_amount" placeholder="Enter Commission">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property Management fee:-<span
                                                    class="tx-danger">*</span></label><br>
                                            <label>Amount</label>
                                            <input type="radio" checked name="property_management_fee_type"
                                                id="property_management_fee_type" value="amount">
                                            <label>Percentage</label>
                                            <input type="radio" name="property_management_fee_type"
                                                id="property_management_fee_type" value="percentage">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Management Fee<span class="tx-danger"
                                                    style="font-size:12px;">*</span></label>
                                            <input type="number" class="form-control" name="management_fee"
                                                id="management_fee" placeholder="Enter Management Fee">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Ads Number:-<span class="tx-danger">*</span></label><br>
                                            <input type="text" class="form-control" name="adsnumber" id="adsnumber"
                                                placeholder="Enter Ads Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Guard Number<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="guard_number" id="guard_number"
                                                placeholder="Enter Guard Number">
                                        </div>
                                    </div>
                                </div>

                                <div class="float-right">
                                <a class="btn btn-primary text-white tabto4" >Next</a>
                                </div>
                                <br>

                            </div>
                            <!-- end of tab4 -->
                            <!-- start tab 5 -->
                            <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Images <span class="tx-danger">*</span></label><br>
                                            <input id="images" name="images[]" type="file" class="form-control file"
                                                multiple data-show-upload="true" data-show-caption="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                            <input id="floor_plans" name="floor_plans[]" type="file"
                                                class="form-control file" multiple data-show-upload="true"
                                                data-show-caption="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Videos <span class="tx-danger"></span></label><br>
                                            <input id="videos" name="videos[]" type="file" class="form-control file"
                                                multiple data-show-upload="true" data-show-caption="true">
                                        </div>
                                    </div>
                                </div>

                                <div class="float-right">
                                <a class="btn btn-primary text-white tabto5" >Next</a>
                                </div>
                                <br>
                            </div>
                            <div class="tab-pane fade" id="location-prop" role="tabpanel"
                                aria-labelledby="location-prop-tab">
                                <div id="map-2"></div>
                                <div>
                                    <input type="hidden" id="lat-prop" name="lat_prop">
                                    <input type="hidden" id="lng-prop" name="lng_prop">
                                </div>
                                <div>

                                    <div class="float-right">
                                         <button type="submit" class="btn btn-primary addBtn addPropertyUnits" id="addPropertyBtn"disabled>Submit</button>
                                     </div>
                            </div>
                            </div>
                        </div>
                        <div class="errorTxt shadow"></div>
                        {{-- <div class="modal-footer">
                            <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                                onclick="window.location.reload();"> Close </button>
                            {{-- <a href="{{route('addPropertyUnits')}}"  class="btn btn-primary addBtn" id="addPropertyBtn"  disabled>Submit</a> -}}

                        </div> --}}
                </form>
            </div>
        </div>
    </div>





    @endsection @section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
   <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
   {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"async></script> --}}
   <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly" async></script>

    <script>
        $(document).on('click','.tabto1',function(){
            if($("#is_builder").is(':checked'))
            {
                $('#rent-tab').trigger('click');
            }else
            {
                $('#contract-tab').trigger('click');
            }
            });
        $(document).on('click','.tabto2',function(){
            $('#amenities-tab').trigger('click');
        });
        $(document).on('click','.tabto3',function(){
            $('#contract-tab').trigger('click');
        });
        $(document).on('click','.tabto4',function(){
            if($("#is_builder").is(':checked'))
            {
                $('#documents-tab').trigger('click');
            }else
            {
                $('#location-prop-tab').trigger('click');
            }
        });

        $(document).on('click','.tabto5',function(){
            $('#location-prop-tab').trigger('click');
        });
    </script>
    <script>
        const url = '{{ url('/') }}';
        $('.sellingdetails').hide()
        $('.hide-element').hide();
        $('.amenities-row').hide();
        $('.errorTxt').hide()
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id != 'bank-tab') {
                $('#ownerSubmitBtn').attr('disabled', true)
            } else {
                $('#ownerSubmitBtn').attr('disabled', false)
            }
        })
        $('.propertynav').click(function() {
            var id = $(this).attr('id')
            if (id != 'location-prop-tab') {
                $('#addPropertyBtn').attr('disabled', true)
            } else {
                $('#addPropertyBtn').attr('disabled', false)
            }
        })

        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            container: '#contractUploadModal modal-body'
        });

        $('#property_to').change(function() {
            if ($('#property_to').val() == 1) {
                // buy -> 1 / rent->0
                $('.rentdetails').hide();
                $('.sellingdetails').show()

            } else {
                $('.sellingdetails').hide()
                $('.rentdetails').show();
            }
        });

        $('#is_builder').click(function() {
            var check_val = $(this).prop("checked");

            if (check_val) {
                $('.hide-element').show();
                $('.amenities-row').show();
                $('.is-building').hide();
                 $('.is-appartment').hide();
            } else {
                $('.hide-element').hide();
                $('.amenities-row').hide();
                $('.is-building').show();
                $('.is-appartment').show();
            }
        });
    </script>
    <script>
        function initMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            if ($('#lat').val() != "" && $('#lng').val() != "") {
                const uluru = {
                    lat: $('#lat').val(),
                    lng: $('#lng').val()
                };
            }
            const map = new google.maps.Map(document.getElementById("map-2"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#lat').val() == "" && $('#lng').val() == "") {
                        $('#lat').val(parseFloat(position.coords.latitude))
                        $('#lng').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#lat').val()
                    var lng = $('#lng').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat').val(marker.position.lat())
                $('#lng').val(marker.position.lng())
            });
        }

        function initPropertyMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            if ($('#lat-prop').val() != "" && $('#lng-prop').val() != "") {
                const uluru = {
                    lat: $('#lat-prop').val(),
                    lng: $('#lng-prop').val()
                };
            }
            const map = new google.maps.Map(document.getElementById("map-2"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#lat-prop').val() == "" && $('#lng-prop').val() == "") {
                        $('#lat-prop').val(parseFloat(position.coords.latitude))
                        $('#lng-prop').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#lat-prop').val()
                    var lng = $('#lng-prop').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat-prop').val(marker.position.lat())
                $('#lng-prop').val(marker.position.lng())
            });
        }
    </script>
    <script>
        $("#site_title").html(`Property | Owner Management`);
        $("#owner_nav").addClass("active");
    </script>
    <script>
        function blockFunction(id, action) {
            $('#ownerId').val(id);
            $('#action').val(action);
            if (action == 1) {
                $('.reject-modal-title').text('Unblock Owner');
                $('.deletetBtn').text('Unblock');
            }
            $('#rejectModal').modal()
        }

        function addPropertyFunction(id) {
            $('#property_owner_id').val(id);
            $('#addPropertyModal').modal();
            initPropertyMap();
        }
        // view property
        // Edit Property
        function EditOwnerFunction(id) {

            $('#owner_id').val(id);
            $.ajax({
                url: url + "/owner/view/" + id,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data.response);
                    if (data.status === true) {
                        $('#myModalLabel').text('Edit Owner')
                        $('#id').val(data.response.id);
                        $('#name').val(data.response.name);
                        $('.email').val(data.response.email);
                        $('.country').val(data.response.country).trigger("change");
                        $('#state').val(data.response.state).trigger("change");
                        $('.city-select').val(data.response.city).trigger("change");
                        $('.zipcode').val(data.response.zip_code).trigger("change");
                        $('#address').val(data.response.address);
                        $('#phone').val(data.response.phone);
                        $('#whatsapp_number').val(data.response.whatsapp_no);
                        $('#status').val(data.response.status).trigger("change");
                        $('#bank_name').val(data.response.bank_name).attr("selected", "selected");
                        $('#account_number').val(data.response.account_number);
                        $('#branch').val(data.response.branch_name);
                        $('#ifsc').val(data.response.ifsc);
                        $('#lat').val(data.response.latitude);
                        $('#lng').val(data.response.longitude);
                        initMap()
                        $('#addModal').modal()
                    } else {
                        toastr["error"](data.response);
                    }
                }
            });


            // $('#addModal').modal();
            // initPropertyMap();
        }

        function addOwnerBuilding() {
            event.preventDefault();
            var ownerId = $('#property_owner_id').val();
            var link = 'manage-owner-buildings/' + ownerId;
            window.location.href = link;

        }
    </script>
    <script>
        $('.country').change(function() {
            var countryId = $(this).val()
            $.ajax({
                url: url + "/get-states/" + countryId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.state-select').html('')
                    var html = '';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.state-select').append(html);

                        } else {
                            $('.state-select').html('')
                        }
                    } else {
                        $('.state-select').html('')
                    }
                }
            });
            return false
        });

    </script>

    <script>
        $('.state-select').change(function() {
            var stateId = $(this).val()
            $.ajax({
                url: url + "/get-cities/" + stateId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.city-select').html('')
                    var html = '';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.city-select').append(html)
                        } else {
                            $('.city-select').html('')
                        }
                    } else {
                        $('.city-select').html('')
                    }
                }
            })
            return false
        });
    </script>
    <script>
        $('.city-select').change(function() {
            var cityId = $(this).val()
            $.ajax({
                url: url + "/get-pincodes/" + cityId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data)
                    $('.zipcode-select').html('')
                    var html = '';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.pincode +
                                    `</option>`
                            })
                            $('.zipcode-select').append(html)
                        } else {
                            $('.zipcode-select').html('')
                        }
                    } else {
                        $('.zipcode-select').html('')
                    }
                }
            });
            return false
        });
    </script>

    <script>
        $('#category').change(function() {
            var categoryId = $(this).val();
            categoryTypeFilter(categoryId);
        });


        $(document).ready(function() {
            categoryTypeFilter(1);
        });

        function categoryTypeFilter(categoryId) {
            $.ajax({
                url: url + "/get-type/" + categoryId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.category-select').html('')
                    var html = '<option  disabled selected value="">SELECT ONE</option>';
                    if (data.status === true) {

                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.type + `</option>`
                            })
                            $('.category-select').append(html)
                        } else {
                            $('.v-select').html('')
                        }
                    } else {
                        $('.category-select').html('')
                    }
                }
            });
            return false
        }
    </script>

    <script>
        $('.deletetBtn').on('click', function() {
            var id = $('#ownerId').val()
            var action = $('#action').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/owner/block/" + id + '/' + action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    if (action == 0) {
                        $(".deletetBtn").text('Block');
                    } else {
                        $(".deletetBtn").text('Unblock');
                    }
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
        });



        $('#type').change(function() {
            var id = $(this).val()
            $.ajax({
                url: url + "/owner/available-amenities/" + id,
                method: 'GET',
                async: true,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == true) {
                        $('#amenties_available').html('')
                        $('#amenties_available').append(data.response)
                    } else {
                        toastr['error'](data.response)
                    }
                },
            });
            return false
        });


        $("#form-add").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                name: {
                    required: true,
                },
                country: {
                    required: true,
                },
                state: {
                    required: true,
                },
                city: {
                    required: true,
                },
                address: {
                    required: true,
                },
                email: {
                    required: true,
                },
                phone: {
                    required: true,
                    maxlength: 10,
                },
                status: {
                    required: true,
                },
                account_number: {
                    required: true,
                },
                branch: {
                    required: true,
                },
                ifsc: {
                    required: true,
                },
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "/owner/add",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

        $('#form-add-property').validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                property_name: {
                    required: true,
                },
                property_to: {
                    required: true,
                },
                category: {
                    required: true
                },
                property_country: {
                    required: true,
                },
                property_state: {
                    required: true,
                },
                property_city: {
                    required: true,
                },
                property_zipcode: {
                    required: true,
                },
                address1: {
                    required: true,
                },
                // furnished : {
                //     required : true,
                // },
                is_builder: {
                    required: function() {
                        if ($('#is_builder').prop(":checked")) {
                            return true;
                            // min_sellingprice : {
                            //     required : function (){
                            //         if($('#property_to').val() == 1){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                            // max_sellingprice : {
                            //     required : function (){
                            //         if($('#property_to').val() == 1){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                            // 'amenities[]' : {
                            //     required :true
                            // },
                            // 'images[]' : {
                            //     required : true,
                            // },
                            // 'detailsdata[]' : {
                            //     required : true,
                            // }

                            // min_rent : {
                            //     required : function (){
                            //         if($('#property_to').val() == 0){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                            // token_amount : {
                            //     required : true,
                            // },

                            // security_deposit : {
                            //     required : function (){
                            //         if($('#property_to').val() == 0){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                        }
                        return false
                    },
                },

                contract_no: {
                    required: true,
                },

                contract_file: {
                    required: true,
                },
                ownership_doc: {
                    required: true,
                },

                contract_start_date: {
                    required: true,
                },

                contract_end_date: {
                    required: true,
                },

            },
            messages: {
                property_name: "Property Name is required",
                property_to: "Property to is required",
                category: "Category is required",
                property_country: "Country is required",
                property_state: "State is required",
                property_city: "City is required",
                property_zipcode: "Zipcode is required",
                address1: "Address is required",
                frequency: "Frequency is required",
                contract_no: 'Contract number is required',
                contract_file: 'Contract file is required',
                ownership_doc: 'Ownership Document is required',
                contract_start_date: 'Contract start date is required',
                contract_end_date: 'Contract end date is required',

            },
            errorElement: 'div',
            errorLabelContainer: '.errorTxt',
            showErrors: function(errorMap, errorList) {
                if (!this.numberOfInvalids()) {
                    $('.errorTxt').hide()
                } else {
                    $('.errorTxt').show()
                }
                this.defaultShowErrors();
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-property");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "/property/add",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            if (data.response_data) {
                                if (data.response_data.property_id) {
                                    var responseId = data.response_data.property_id;
                                    if ($('#is_builder').prop(":checked")) {
                                        setTimeout(function() {
                                            window.location.href = "";
                                        }, 1000);
                                    } else {
                                        setTimeout(function() {
                                            window.location.href = "/add-property-unit/" +
                                                responseId;
                                        }, 1000);
                                    }
                                }
                            } else {
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            }
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

        $('#type').change(function() {
            var id = $(this).val()
            $.ajax({
                url: url + "/owner/details-on-type-change/" + id,
                method: 'GET',
                async: true,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == true) {
                        var html = ''
                        data.response.forEach((value, key) => {
                            html += ` <div class="col-md-4">
                                    <div class="form-group">
                                        <label>` + value.name + `<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="detailsdata[` + value.detail_id +
                                `]" id="det-` + value.detail_id + `" placeholder=` + value
                                .placeholder + `>
                                    </div>
                                </div>`
                        })
                        $('#details-section').html('')
                        $('#details-section').append(html)

                    } else {
                        toastr['error'](data.response)
                    }
                },
            });
            return false
        });


        // yajra datatable
        $(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ url('/owners-list') }}",
                    data: function(d) {
                        d.search = $('input[type="search"]').val();
                        d.type = $('#property_id').val();
                        d.status = $('#owner_id').val();
                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        orderable: false,
                        // searchable: false
                    }
                ]
            });
            $('.filter').change(function(){
          table.draw();
             });
        });


        $(document).on('click', '.property_view_button', function() {
            var ownerId = $(this).data('owner-id');
            alert(ownerId);
            $.ajax({
                url: url + "/property-list/view/" + ownerId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {


                    if (data.status === true) {
                        console.log(data.response);
                        if (data.response.category == 0)
                            $("#ptype").html('Residential');
                        else
                            $("#ptype").html('Commercial');
                        $("#pregisterno").html(data.response.property_reg_no)
                        if (data.response.type_details)
                            $('#pcategory').html(data.response.type_details.type);
                        else
                            $('#pcategory').html('');
                        if (data.response.property_to == '0')
                            $('#propertyFor').html('Rent');
                        else
                            $('#propertyFor').html('Buy');


                        $('#pname').html(data.response.property_name);
                        if (data.response.country_rel)
                            $('#countryname').html(data.response.country_rel.name);
                        else
                            $('#countryname').html('');
                        if (data.response.state_rel)
                            $('#statename').html(data.response.state_rel.name);
                        else
                            $('#statename').html('');
                        if (data.response.city_rel)
                            $('#cityName').html(data.response.city_rel.name);
                        else
                            $('#cityName').html('');

                        if (data.response.zipcode_rel)
                            $('#zipCode').html(data.response.zipcode_rel.pincode);
                        else
                            $('#zipCode').html('');
                        $('#address1').html(data.response.street_address_1);
                        $('#address2').html(data.response.street_address_2);;
                        $('.contarctno').html(data.response.contract_no);
                        $('.startdate').html(data.response.contract_start_date);
                        $('.enddate').html(data.response.contract_end_date);
                        if (data.response.commission_in == 'percentage') {
                            $('.commission').html(data.response.commission + '%')
                        } else {
                            $('.commission').html(data.response.commission)
                        }
                        if (data.response.property_management_fee_type == 'percentage') {
                            $('.managementamount').html(data.response.management_fee + '%')
                        } else {
                            $('.managementamount').html(data.response.management_fee)
                        }
                        $('.adsNumber').html(data.response.ads_number);
                        $('.guardNumber').html(data.response.guards_number);

                        $('#propertyViewModal').modal();
                    } else {
                        toastr["error"](data.response);
                    }
                }
            });

        });
    </script>
@endsection
