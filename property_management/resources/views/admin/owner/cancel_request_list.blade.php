@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Service Management</span>
    </nav>
</div>

<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Cancel Requests </h4>
                        <p class="card-category">List of all cancel Requests</p>
                    </div>
                    <div class="col-md-6 row d-flex justify-content-center">
                        <a href="{{ route('showCancelRequest',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center @if($status == 0) card-active @endif">
                            &nbspUser
                        </a>
                        <a href="{{ route('showCancelRequest',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center @if($status == 1) card-active @endif">
                            &nbspOwner
                        </a>
                    </div>
                    @if($status == 1)
                    <input type="hidden" id="user-type" value="1">
                    @else
                    <input type="hidden" id="user-type" value="0">
                    @endif

                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Property Name</th>
                            <th class="wd-15p">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($request as $row)
                        @if($row->request_rel->status != 3 || $row->request_rel->status != 4)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            @if($status == 1)
                            <td>@if($row->request_rel)@if($row->request_rel->owner_related){{$row->request_rel->owner_related->name ? $row->request_rel->owner_related->name : "" }}@endif @endif</td>
                            <td>@if($row->request_rel)@if($row->request_rel->service_related){{$row->request_rel->service_related->service?$row->request_rel->service_related->service:""}}@endif @endif</td>
                             @else
                            <td>@if($row->request_rel)@if($row->request_rel->user_related){{$row->request_rel->user_related->name ? $row->request_rel->user_related->name : "" }}@endif @endif</td>
                            <td>@if($row->request_rel)@if($row->request_rel->service_related){{$row->request_rel->service_related->service?$row->request_rel->service_related->service:""}}@endif @endif</td>
                            @endif
                            <td>
                                @if($row->status == 0)
                                <a class="fad fa-check  tx-20 accept-button" href="javascript:0;" data-property="" title="Accept" style="color: green " onclick="AcceptFunction('{{$row->id}}','{{$status}}')">
                                <!-- <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus " href="javascript:0;" data-admin="" title="Block" style="" onclick="RejectFunction('{{$row->id}}')"> -->
                                </a>
                                @endif
                            </td>

                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $request->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<!-- Accept Modal -->
<div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-success reject-modal-title">Service Cancel Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to accept this cancel request?
                    <input type="hidden" name="requestId" id="requestId">
                    <input type="hidden" name="id" id="id">
                    <!--Action -  1-accept , 0- reject -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-success reject-class AcceptBtn">Accept</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Reject Pay Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to Reject this Agent Pay Request ?
                    <input type="hidden" name="Id" id="Id">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class RejectBtn">Reject</button>
                </div>
            </form>
        </div>
    </div>
</div>




@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Service Management`);
    $("#service_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    function AcceptFunction(id, action) {
        $('#requestId').val(id);
        $('#id').val(action);

        $('#acceptModal').modal()
    }

    $('.AcceptBtn').click(function() {
        var id = $('#requestId').val();
        var user_type = $('#user-type').val();
        $(".AcceptBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "POST",
            url: "{{route('approveCancelRequest')}}",
            data:{
                request_id : id,
                user_type  : user_type,
            },
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".AcceptBtn").text('Processing..');
            },
            success: function(data) {
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    function RejectFunction(id) {

        $('#Id').val(id);

        $('#rejectModal').modal()
    }

    $('.rejectBtn').click(function() {
        var id = $('#Id').val()
        var action = $('#id').val()
        $(".RejectBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/agent_pay_request/reject/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".AcceptBtn").text('Processing..');
            },
            success: function(data) {
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })
</script>


@endsection
