@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Facility Management</span>
        <span class="breadcrumb-item active">Service Request</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-6">
                        <h4 class="card-title h4-card">Cancelled Requests</h4>
                        <p class="card-category">List of Cancelled Requests</p>
                    </div>

                    <div class="col-md-6 d-flex justify-content-end">
                        @if($status == '1')
                        <a href="{{ route('cancelledRequest',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                        </a>
                        <a href="{{ route('cancelledRequest',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-check-user tx-20"></i>&nbspUser
                        </a>
                        @elseif($status == '0')
                        <a href="{{ route('cancelledRequest',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                        </a>

                        <a href="{{ route('cancelledRequest',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-check-user tx-20"></i>&nbspUser
                        </a>
                        @endif

                    </div>
                    <!-- <div class="col-md-8 row d-flex justify-content-center">

                    </div> -->
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-10p">Date</th>
                            <th class="wd-10p">Property</th>
                            <th class="wd-5">Service</th>
                            <th class="wd-20">Description</th>
                            {{-- <th class="wd-10">Docs</th>
                            <th class="wd-20p">Staus</th> --}}
                            <th class="wd-20p">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if($status == 0)
                        @foreach($request as $row )
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->date}}</td>
                            <td>{{$row->property_name}}</td>
                            <td>{{$row->service_related->service}}</td>
                            <td>{{$row->description}}</td>
                            <td>No Actions</td>
                            {{-- <td>

                                @foreach($row->user_doc as $keys)
                                <i class="far fa-file-alt"></i>
                                @endforeach

                            </td> --}}

                        </tr>
                        @endforeach
                        @elseif($status == '1')
                        @foreach($request as $row )
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->date}}</td>
                            <td>{{$row->property_name}}</td>
                            <td>{{$row->service_related->service}}</td>
                            <td>{{ $row->description }}</td>
                            <td>No Actions</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                <!-- <div class="d-flex justify-content-end">

                </div> -->
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->





@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Sernice Management`);
    $("#service_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>

@endsection
