@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
    .ui-datepicker { position: fixed; top: 270.797px; left: 172.5px; z-index: 5001 !important; display: block; }
    #ui-datepicker-div { z-index: 5001 !important;  }

    .errorTxt{
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding : 10px;
        border-radius :10px;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Apartment</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Apartment</h4>
                        <p class="card-category">List of all Apartment {{ count( $property ) }} </p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addPropertyModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAparment</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Property Registration No</th>
                            <th class="wd-15p">Property  Name</th>
                            <th class="wd-15p">Builders</th>
                            <th class="wd-20p">Description</th>

                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($apartment as $row )
                        <tr>
                            @if($row->builder_id != '')
                           <td> {{ $loop->iteration  }}</td>
                            <td> {{ $row->property_reg_no }}</td>
                            <td> {{ $row->owner_id}}
                            <td>{{ $row->property_name }}</td>
                            <td>{{ $row->description }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">

                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<div class="modal fade" id="addPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        @if(count($property) !== 0)
        <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myPropertyModalLabel">Add Apartment</h4>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                        </li>
                        <li class="nav-item">
                            <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab" aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities" role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contract" role="tab" aria-controls="contract" aria-selected="false">Contract Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                        </li>
                    </ul>
                </div>

                <form class="form-admin" id="form-add-property" action="javascript:;">
                    <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Builder Name<span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="builder_id" id="builder_id">
                                        <option value=""></option>
                                        @foreach($property as $row)
                                            <option value="{{$row->id}}" >{{$row->property_name}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Property Name<span class="tx-danger">*</span></label>
                                    <input class="form-control" id="property_name" name="property_name" type="text" placeholder="Enter Property Name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden" id="property_owner_id" name="property_owner_id">
                                    <div class="form-group">
                                        <label>property to <span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="property_to" id="property_to">
                                            <option value="0">Rent</option>
                                            <option value="1">Buy</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Category <span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="category" id="category">
                                            <option value="1">Residential</option>
                                            <option value="2">Commercial</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" id="description" name="description" placeholder="description">
                                </div>
                        </div>
                        </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Type<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 category-select" name="type" id="type">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Furnished<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="furnished" id="furnished">
                                            <option value="0">Not Furnished</option>
                                            <option value="1">Semi Furnished</option>
                                            <option value="2">Fully Furnished</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-warning tx-white mb-3"> &nbspDetails</div>
                            <div class="row" id="details-section">
                            @if($details)
                                @foreach($details as $det)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{$det->name}}<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="detailsdata[{{$det->detail_id}}]" id="det-{{$det->detail_id}}" placeholder="{{$det->placeholder}}">
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            </div>
                        </div>
                        <!-- end of tab1 -->
                        <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                            <div class="rentdetails">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Frequency<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 rentfrequency" name="frequency" id="frequency">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Bimonthly</option>
                                                <option value="4">Biweekly</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Rent<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="min_rent" name="min_rent" placeholder="Enter rent">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Security Deposit<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="security_deposit" id="security_deposit" placeholder="Enter Security deposit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sellingdetails">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Min Selling price<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="min_sellingprice" name="min_sellingprice" placeholder="Enter minimum selling price">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Max Selling price<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="max_sellingprice" name="max_sellingprice" placeholder="Enter maximum selling price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="token-amount">
                                <div class="row">
                                    <div class="col-md-6 token-hold">
                                        <div class="form-group">
                                            <label>Token Amount<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="token_amount" id="token_amount" placeholder="Enter Token amount">
                                        </div>
                                    </div>
                                    <div class="col-md-6 token-hold">
                                        <div class="form-group">
                                            <label>Owner Amount<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="owner_amount" id="owner_amount" placeholder="Enter Owner amount">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="amenities" role="tabpanel" aria-labelledby="amenities-tab">
                            <div id="amenties_available">
                                @if($amenities_listing)
                                    @foreach($amenities_listing as $row)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="card-warning tx-white mb-3"> &nbsp {{$row->name}}  </div>
                                                <div class="row mg-l-1">
                                                    @foreach($row->amenities as $row2)
                                                        <label class="ckbox ml-2">
                                                            <input type="checkbox" value="{{$row2->id}}" name="amenities[]">
                                                            <span>{{ $row2->name }}</span>
                                                        </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <!-- end of tab3 -->
                        <!-- start tab 4 -->
                        <div class="tab-pane fade" id="contract" role="tabpanel" aria-labelledby="contract-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract No</label>
                                        <input type="text" class="form-control" name="contract_no" id="contract_no">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract File</label>
                                        <input type="file" class="form-control" name="contract_file" id="contract_file">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract Start Date<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker" name="contract_start_date" id="contract_start_date" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract End Date<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker" name="contract_end_date" id="contract_end_date" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of tab4 -->
                        <!-- start tab 5 -->
                        <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Images <span class="tx-danger">*</span></label><br>
                                        <input id="images" name="images[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                        <input id="floor_plans" name="floor_plans[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Videos <span class="tx-danger"></span></label><br>
                                        <input id="videos" name="videos[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="errorTxt shadow"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();" > Close </button>
                        <button type="submit" class="btn btn-primary addBtn" id="addPropertyBtn" disabled>Submit</button>
                    </div>
                </form>
        </div>
        @else
        <div class="modal-content">
        <p class="text-danger"><b> There is No Properties Belongs to this Owner </b> </p>
        </div>
        @endif
    </div>
</div>


<!--  Modal -->

@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
    $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          changeMonth: true,
          changeYear: true,
          container: '#contractUploadModal modal-body'
        });

    $('.propertynav').click(function(){
            var id= $(this).attr('id')
            if(id != 'documents-tab'){
                $('#addPropertyBtn').attr('disabled',true)
            }else{
                $('#addPropertyBtn').attr('disabled',false)
            }
        })

    $('#property_to').change(function(){
            if($('#property_to').val() == 1){
                // buy -> 1 / rent->0
                $('.rentdetails').hide();
                $('.sellingdetails').show()

            }else{
                $('.sellingdetails').hide()
                $('.rentdetails').show();
            }
        })
</script>
<script>

    $('#category').change(function(){
            var categoryId = $(this).val()

            $.ajax({
                url : url+"/get-type/"+categoryId,
                type : "GET",
                processData : false,
                async : true,
                header : {
                    "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
                },
                success : function(data){
                    $('.category-select').html('')
                    var html = '<option value=""></option>';
                    if(data.status === true){

                        if(data.response.length > 0){
                            data.response.forEach((val,key)=>{
                                html += `<option value=`+val.id+`>`+val.type+`</option>`
                            })
                            $('.category-select').append(html)
                        }else{
                            $('.v-select').html('')
                        }
                    }else{
                        $('.category-select').html('')
                    }
                }
            })
            return false
        })


</script>
<script>
    $("#site_title").html(`Property | Offer Package`);
    $("#package_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });

    $('#type').change(function(){
        var id = $(this).val()
        $.ajax({
            url : url+"/owner/available-amenities/"+id,
            method : 'GET',
            async : true,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status == true){
                    $('#amenties_available').html('')
                    $('#amenties_available').append(data.response)
                }else{
                    toastr['error'](data.response)
                }
            },
        })
        return false
    })
    $('#form-add-property').validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules : {
            builder_id :{
             required:true
            },
            property_name : {
                required: true,
            },
            property_to : {
                required: true,
            },
            category : {
                required : true
            },
            furnished : {
                required : true,
            },
            frequency : {
                required : function (){
                    if($('#property_to').val() == 0){
                        return true
                    }
                    return false
                },
            },
            min_rent : {
                required : function (){
                    if($('#property_to').val() == 0){
                        return true
                    }
                    return false
                },
            },
            token_amount : {
                required : true,
            },

            security_deposit : {
                required : function (){
                    if($('#property_to').val() == 0){
                        return true
                    }
                    return false
                },
            },

            contract_no : {
                required : true,
            },

            contract_file : {
                required : true,
            },

            contract_start_date : {
                required : true,
            },

            contract_end_date : {
                required : true,
            },

            min_sellingprice : {
                required : function (){
                    if($('#property_to').val() == 1){
                        return true
                    }
                    return false
                },
            },
            max_sellingprice : {
                required : function (){
                    if($('#property_to').val() == 1){
                        return true
                    }
                    return false
                },
            },
            'amenities[]' : {
                required :true
            },
            'images[]' : {
                required : true,
            },
            'detailsdata[]' : {
                required : true,
            }
        },
        messages : {
            builder_id : "buider Name is Required",
            property_name : "Property Name is required",
            property_to : "Property to is required",
            category : "Category is required",
            frequency : "Frequency is required",
            min_rent : "Min rent is required",
            max_rent : "Max rent is required",
            furnished : "Furnished is required",
            security_deposit : "Security deposit is required",
            token_amount : "Token Amount is required",
            contract_no : 'Contract number is required',
            contract_file : 'Contract file is required',
            contract_start_date : 'Contract start date is required',
            contract_end_date : 'Contract end date is required',
            min_sellingprice : "Min Selling Price is required",
            max_sellingprice : "Max Selling Price is required",
            'amenities[]' : 'Amenities are required',
            'images[]' : 'Images are required',
            'detailsdata[]': 'Details are required',
        },
        errorElement : 'div',
        errorLabelContainer: '.errorTxt',
        showErrors: function (errorMap, errorList) {
            if(!this.numberOfInvalids()){
                $('.errorTxt').hide()
            }else{
                $('.errorTxt').show()
            }
            this.defaultShowErrors();
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-property");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addApartment')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    })

    $('#type').change(function(){
        var id = $(this).val()
        $.ajax({
            url : url+"/owner/details-on-type-change/"+id,
            method : 'GET',
            async : true,
            processData : false,
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status == true){
                    var html = ''
                    data.response.forEach((value,key)=>{
                        html += ` <div class="col-md-4">
                                    <div class="form-group">
                                        <label>`+value.name+`<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="detailsdata[`+value.detail_id+`]" id="det-`+value.detail_id+`" placeholder=`+value.placeholder+`>
                                    </div>
                                </div>`
                    })
                    $('#details-section').html('')
                    $('#details-section').append(html)

                }else{
                    toastr['error'](data.response)
                }
            },
        })
        return false
    })
</script>





@endsection

