@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .error {
        color: red;
    }

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding : 10px;
        border-radius :10px;
    }
    
    .errorBuildingTxt{
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding : 10px;
        border-radius :10px;
    }
    #map { height: 450px; width: 100%; }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Property</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="card-title h4-card">Owner Buildings</h4>
                        <p class="card-category"><b>Owner :- </b> {{ $owner_details->name }}</p>
                    </div>
                    {{-- <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-right"  data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Building</a>
                    </div> --}}
                    <div class="col-md-6">
                        <div class="pd-5 col-md-4 d-flex justify-content-end float-right">
                                <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addBuildingModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspBuilding</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Register no:</th>
                            <th class="wd-20p">Building Name</th>
                            <th class="wd-20p">Category</th>
                            <th class="wd-20p">City|State</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($building as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->property_reg_no}}</td>
                            <td>{{$row->property_name}}</td>
                            <td>
                                <span class="badge rounded-pill bg-warning text-dark">
                                    @if($row->category == 0)
                                    Residential
                                    @elseif($row->role == 1)
                                    Commercial
                                    @endif
                                </span> 
                                @if($row->propert_to == 0)
                                <span class="badge rounded-pill bg-info text-white"> Rent </span>
                                @else
                                <span class="badge rounded-pill bg-primary text-white"> Buy </span>
                                @endif
                            </td>
                            <td>{{$row->city_rel->name}} | {{$row->state_rel->name}}</td>
                            <td>
                                <!-- <a class="fad fa-plus-square tx-20 move-button ml-1" href="javascript:0;" data-property="{{$row->id}}" title="Add property" onclick="addPropertyFunction('{{$row->id}}')"></a> -->

                                <a class="btn btn-primary px-1 pt-0 pb-0 move-button" href="javascript:0;" data-target="#addPropertyModal" onclick="addPropertyFunction('{{$row->id}}','{{$row->property_to}}')" data-property="{{$row->id}}" >Add Appartment</a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $building->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<!-- Reject Modal -->
<div class="modal fade" id="addBuildingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myPropertyModalLabel">Add Building</h4>
                </div>

                <form class="form-admin" id="form-add-building" action="javascript:;">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>Building Name<span class="tx-danger">*</span></label>
                                        <input class="form-control" id="building_name" name="building_name" type="text" placeholder="Enter Building Name">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="building_owner_id" value="{{$owner_details->id}}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" id="property_owner_id" name="property_owner_id">
                                        <div class="form-group">
                                            <label>Property to <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_to" id="property_to">
                                                <option value="0">Rent</option>
                                                <option value="1">Buy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Category <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="category" id="category">
                                                <option value="1">Residential</option>
                                                <option value="2">Commercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 category-select" name="type" id="type">
                                                <option value="" selected></option>
                                                @foreach($types as $type)
                                                    <option value="{{$type->id}}">{{$type->type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Country<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_country" id="property_country">
                                            <option value="" selected></option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>State<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 state-select" name="property_state" id="property_state">
                                            <option value=""></option>
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 city-select" name="property_city" id="property_city">
                                            <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Zipcode<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 zipcode-select" name="property_zipcode" id="property_zipcode">
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label>Address 1 <span class="tx-danger">*</span></label>
                                        <textarea class="form-control" id="address1" name="address1" type="text" placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address 2 <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address2" name="address2" type="text" placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Images <span class="tx-danger">*</span></label><br>
                                            <input id="building_images" name="building_images[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="location-tab">
                                <div id="map"></div>
                                <div>
                                   <input type="text" id="lat" name="lat">
                                   <input type="text" id="lng" name="lng">
                                </div>
                            </div>
                        </div>
                        <div class="errorBuildingTxt shadow pl-2"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();" > Close </button>
                        <button type="submit" class="btn btn-primary addBtn" id="addBuildingApartmentBtn" >Submit</button>
                    </div>
                </form>
        </div>
    </div>
</div>

<div class="modal fade" id="addPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myPropertyModalLabel">Add Apartment</h4>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                        </li>
                        <li class="nav-item">
                            <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab" aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities" role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contract" role="tab" aria-controls="contract" aria-selected="false">Contract Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                        </li>
                    </ul>
                </div>

                <form class="form-admin" id="form-add-apartment" action="javascript:;">
                    <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Property Name<span class="tx-danger">*</span></label>
                                    <input class="form-control" id="property_name" name="property_name" type="text" placeholder="Enter Property Name">
                                    </div>
                                    <input type="hidden" id="building_id" name="building_id">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Furnished<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="furnished" id="furnished">
                                            <option value="0">Not Furnished</option>
                                            <option value="1">Semi Furnished</option>
                                            <option value="2">Fully Furnished</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-warning tx-white mb-3"> &nbspDetails</div>
                            <div class="row" id="details-section">
                            @if($details)
                                @foreach($details as $det)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{$det->name}}<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="detailsdata[{{$det->detail_id}}]" id="det-{{$det->detail_id}}" placeholder="{{$det->placeholder}}">
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            </div>
                        </div>
                        <!-- end of tab1 -->
                        <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                            <div class="rentdetails">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Frequency<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 rentfrequency" name="frequency" id="frequency">
                                                <option value="1">Weekly</option>
                                                <option value="2">Monthly</option>
                                                <option value="3">Bimonthly</option>
                                                <option value="4">Biweekly</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Rent<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="min_rent" name="min_rent" placeholder="Enter rent">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Security Deposit<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="security_deposit" id="security_deposit" placeholder="Enter Security deposit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sellingdetails">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Min Selling price<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="min_sellingprice" name="min_sellingprice" placeholder="Enter minimum selling price">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Max Selling price<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="max_sellingprice" name="max_sellingprice" placeholder="Enter maximum selling price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="token-amount">
                                <div class="row">
                                    <div class="col-md-6 token-hold">
                                        <div class="form-group">
                                            <label>Token Amount<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="token_amount" id="token_amount" placeholder="Enter Token amount">
                                        </div>
                                    </div>
                                    <div class="col-md-6 token-hold">
                                        <div class="form-group">
                                            <label>Owner Amount<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="owner_amount" id="owner_amount" placeholder="Enter Owner amount">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="amenities" role="tabpanel" aria-labelledby="amenities-tab">
                            <div id="amenties_available">
                                @if($amenities_listing)
                                    @foreach($amenities_listing as $row)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="card-warning tx-white mb-3"> &nbsp {{$row->name}}  </div>
                                                <div class="row mg-l-1">
                                                    @foreach($row->amenities as $row2)
                                                        <label class="ckbox ml-2">
                                                            <input type="checkbox" value="{{$row2->id}}" name="amenities[]">
                                                            <span>{{ $row2->name }}</span>
                                                        </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <!-- end of tab3 -->
                        <!-- start tab 4 -->
                        <div class="tab-pane fade" id="contract" role="tabpanel" aria-labelledby="contract-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract No</label>
                                        <input type="text" class="form-control" name="contract_no" id="contract_no">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract File</label>
                                        <input type="file" class="form-control" name="contract_file" id="contract_file">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract Start Date<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker" name="contract_start_date" id="contract_start_date" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract End Date<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control fc-datepicker" name="contract_end_date" id="contract_end_date" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of tab4 -->
                        <!-- start tab 5 -->
                        <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Images <span class="tx-danger">*</span></label><br>
                                        <input id="images" name="images[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                        <input id="floor_plans" name="floor_plans[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Videos <span class="tx-danger"></span></label><br>
                                        <input id="videos" name="videos[]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="errorTxt shadow"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();" > Close </button>
                        <button type="submit" class="btn btn-primary addBtn disabled" id="addPropertyBtn" >Submit</button>
                    </div>
                </form>
        </div>
    </div>
</div>




@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
    const url = '{{url("/")}}';
</script>
<script>
    $('#details-tab').click(function(){
        $('.tab-pane').removeClass('active show');
        $('.tab-pane').addClass('fade');
        $('.tab-pane#details').addClass('active show');
    });

    
    $('.propertynav#documents-tab').click(function(){
        $('#addPropertyBtn').removeClass('disabled');
    });
</script>

<script>
    $('.errorTxt').hide();
    $('.errorBuildingTxt').hide();

    $("#site_title").html(`Property | Property`);
    $("#property_nav").addClass("active");
    $("#property_sub_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });

    function addPropertyFunction(buildingId,propertyTo){
        $('#building_id').val(buildingId);
        $('#addPropertyModal').modal();

        if(propertyTo == 1){
            // buy -> 1 / rent->0
            $('.rentdetails').hide();
            $('.sellingdetails').show()

        }else{
            $('.sellingdetails').hide()
            $('.rentdetails').show();
        }
    }
</script>

<script>
    function blockFunction(id, action) {
        $('#adminId').val(id);
        $('#action').val(action);
        if (action == 1) {
            $('.reject-modal-title').text('Unblock Admin');
            $('.deletetBtn').text('Unblock');
        }
        $('#rejectModal').modal()
    }
</script>
<script>
    $('.deletetBtn').click(function() {
        var id = $('#adminId').val()
        var action = $('#action').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/admin/block/" + id + '/' + action,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".addBtn").text('Processing..');
            },
            success: function(data) {
                if (action == 0) {
                    $(".deletetBtn").text('Block');
                } else {
                    $(".deletetBtn").text('Unblock');
                }
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    $('.edit-button').click(function() {
        var adminid = $(this).data('admin')
        $.ajax({
            url: url + "/admin/view/" + adminid,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status === true) {
                    $('#myModalLabel').text('Edit Admin')
                    $('#id').val(data.response.id);
                    $('#first_name').val(data.response.first_name);
                    $('#last_name').val(data.response.last_name);
                    $('#country').val(data.response.country).trigger("change");
                    $('#state').val(data.response.state).trigger("change");
                    $('#city').val(data.response.city).trigger("change");
                    $('#dept').val(data.response.department).trigger("change");
                    $('#des').val(data.response.designation).trigger("change");
                    $('#zipcode').val(data.response.zip_code);
                    $('#address').val(data.response.address);
                    $('#email').val(data.response.email);
                    $('#phone').val(data.response.phone);
                    $('#role').val(data.response.role).trigger("change");
                    $('#status').val(data.response.active).trigger("change");
                    $('#password').attr('placeholder', 'change password');
                    $('#addModal').modal()
                } else {
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })


   function multiplAgent(id){

        $('#property_id').val(id);

        $.ajax({
            url: url + "/propertyagent/list/" + id,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                console.log(data.agent.length);
                $('.agent-select').html('')
                var html = '<option value=""></option>';
                if (data.status === true) {
                    if (data.agent.length > 0) {
                        data.agent.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.name + `</option>`
                        })
                        $('.agent-select').append(html)
                        $('#addPropertyModal').modal();
                    } else {
                        $('.agent-select').html('')
                    }
                } else {
                    $('.agent-select').html('')
                }
            }

        })
        return false

    }


    // agent modal submittion
    $("#multiple-agent-form").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            'agents[]': {
                required: true,
            },
            date: {
                required: true,
            },
            fromtime: {
                required: true,
            }

        },
        submitHandler: function(form) {
            var form = document.getElementById("multiple-agent-form");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addAgentinProperty')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
                error: function(xhr, data) {
                    console.log(xhr);
                }
            });
        },
    });






    $('#form-add-building').validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules : {
            building_name : {
                required: true,
            },
            property_to : {
                required: true,
            },
            category : {
                required : true
            },
            property_country: {
                required: true,
            },
            type:{
                required: true,
            },
            property_state: {
                required: true,
            },
            property_city: {
                required: true,
            },
            property_zipcode: {
                required: true,
            },
            building_owner_id : {
                required: true,
            },
            address1: {
                required: true,
            },
            'building_images[]' : {
                required : true,
            }
        },
        messages : {
            building_name : "Building Name is required",
            property_to : "Property to is required",
            type : "Property type is required",
            building_owner_id : "Owner is required",
            category : "Category is required",
            property_country : "Country is required",
            property_state : "State is required",
            property_city :"City is required",
            property_zipcode : "Zipcode is required",
            address1 : "Address is required",
            'building_images[]' : 'Images are required',
        },
        errorElement : 'div',
        errorLabelContainer: '.errorBuildingTxt',
        showErrors: function (errorMap, errorList) {
            if(!this.numberOfInvalids()){
                $('.errorBuildingTxt').hide()
            }else{
                $('.errorBuildingTxt').show()
            }
            this.defaultShowErrors();
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-building");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addBuilding')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });


    $('#form-add-apartment').validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules : {
            property_name : {
                required: true,
            },
            building_id : {
                required : true,
            },
            furnished : {
                required : true,
            },
            frequency : {
                required : function (){
                    if($('#property_to').val() == 0){
                        return true
                    }
                    return false
                },
            },
            min_rent : {
                required : function (){
                    if($('#property_to').val() == 0){
                        return true
                    }
                    return false
                },
            },
            token_amount : {
                required : true,
            },

            security_deposit : {
                required : function (){
                    if($('#property_to').val() == 0){
                        return true
                    }
                    return false
                },
            },

            contract_no : {
                required : true,
            },

            contract_file : {
                required : true,
            },

            contract_start_date : {
                required : true,
            },

            contract_end_date : {
                required : true,
            },

            min_sellingprice : {
                required : function (){
                    if($('#property_to').val() == 1){
                        return true
                    }
                    return false
                },
            },
            max_sellingprice : {
                required : function (){
                    if($('#property_to').val() == 1){
                        return true
                    }
                    return false
                },
            },
            'amenities[]' : {
                required :true
            },
            'images[]' : {
                required : true,
            },
            'detailsdata[]' : {
                required : true,
            }
        },
        messages : {
            property_name : "Property Name is required",
            building_id    : "Building is required",
            frequency : "Frequency is required",
            min_rent : "Min rent is required",
            furnished : "Furnished is required",
            security_deposit : "Security deposit is required",
            token_amount : "Token Amount is required",
            contract_no : 'Contract number is required',
            contract_file : 'Contract file is required',
            contract_start_date : 'Contract start date is required',
            contract_end_date : 'Contract end date is required',
            min_sellingprice : "Min Selling Price is required",
            max_sellingprice : "Max Selling Price is required",
            'amenities[]' : 'Amenities are required',
            'images[]' : 'Images are required',
            'detailsdata[]': 'Details are required',
        },
        errorElement : 'div',
        errorLabelContainer: '.errorTxt',
        showErrors: function (errorMap, errorList) {
            if(!this.numberOfInvalids()){
                $('.errorTxt').hide()
            }else{
                $('.errorTxt').show()
            }
            this.defaultShowErrors();
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-apartment");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addBuildingApartment')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });

    $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          changeMonth: true,
          changeYear: true,
          container: '#contractUploadModal modal-body'
    });

    $('#category').change(function(){
        var categoryId = $(this).val();

        $.ajax({
            url : url+"/get-type/"+categoryId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                console.log(data);
                $('.category-select').html('')
                var html = '<option value=""></option>';
                if(data.status === true){

                    if(data.response.length > 0){
                        data.response.forEach((val,key)=>{
                            html += `<option value=`+val.id+`>`+val.type+`</option>`
                        })
                        $('.category-select').append(html)
                    }else{
                        $('.v-select').html('')
                    }
                }else{
                    $('.category-select').html('')
                }
            }
        })
        return false
    })
</script>
<script>
    $('.state-select').change(function(){
        var stateId = $(this).val()
        $.ajax({
            url : url+"/get-cities/"+stateId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                $('.city-select').html('')
                var html = '<option value=""></option>';
                if(data.status === true){
                    if(data.response.length > 0){
                        data.response.forEach((val,key)=>{
                            html += `<option value=`+val.id+`>`+val.name+`</option>`
                        })
                        $('.city-select').append(html)
                    }else{
                        $('.city-select').html('')
                    }
                }else{
                    $('.city-select').html('')
                }
            }
        })
        return false
    })
</script>
<script>
    $('.city-select').change(function(){
        var cityId = $(this).val()
        $.ajax({
            url : url+"/get-pincodes/"+cityId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                $('.zipcode-select').html('')
                var html = '<option value=""></option>';
                if(data.status === true){
                    if(data.response.length > 0){
                        data.response.forEach((val,key)=>{
                            html += `<option value=`+val.id+`>`+val.pincode+`</option>`
                        })
                        $('.zipcode-select').append(html)
                    }else{
                        $('.zipcode-select').html('')
                    }
                }else{
                    $('.zipcode-select').html('')
                }
            }
        })
        return false
    })
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    function initMap() {
        const uluru = { lat: 24.466667, lng: 54.366669};
        if($('#lat').val() != "" && $('#lng').val() != ""){
            const uluru = { lat: $('#lat').val(), lng: $('#lng').val()};
        }
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            center: uluru,
        });
        const marker = new google.maps.Marker({
            position: uluru,
            map: map,
            draggable: true
        });
        navigator.geolocation.getCurrentPosition(
            function( position ){ // success cb
                if($('#lat').val() == "" && $('#lng').val() == ""){
                    $('#lat').val(parseFloat(position.coords.latitude))
                    $('#lng').val(parseFloat(position.coords.longitude))
                }
                var lat = $('#lat').val()
                var lng = $('#lng').val()
                map.setCenter(new google.maps.LatLng(lat,lng));
                var latlng = new google.maps.LatLng(lat, lng);
                marker.setPosition(latlng);
            },
            function(){  }
        );
        google.maps.event.addListener(marker, 'dragend', function(){
            $('#lat').val(marker.position.lat())
            $('#lng').val(marker.position.lng())
        })
    }

    // initMap();

</script>

@endsection
