@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="{{ route('showOwner') }}">Property Management</a>
            <span class="breadcrumb-item active">Owner Property Details</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                                                                                            <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                                                                                <i class="fas fa-plus-square"></i>&nbsp Add Client
                                                                                                            </button>
                                                                                                    </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Owner Property Details</h4>
                            <p class="card-category"></p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                   
                    @if (count($properties) > 0)
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th scope="col">Id</th>
                                <th scope="col">Property Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Propety For</th>
                                <th scope="col">Owner</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </thead>
                            <tbody>
                                @foreach ($properties as $property )
                                <tr>
                                    <td>{{ $property->id}}</td>
                                    <td>{{ $property->property_name}}</td>
                                    @if($property->category == '0')
                                    <td>Residential</td>
                                    @else
                                    <td>Commercial</td>
                                    @endif
                                    @if($property->property_to=='0')
                                    <td>Rent</td>
                                    @else
                                    <td>Buy</td>
                                    @endif
                                    <td>{{$property->owner_rel->name}}</td>
                                    @if($property->occupied=='0')
                                    <td>Vaccant</td>
                                    @else
                                    <td>Occupied</td>
                                    @endif
                                    <td><a class="fad fa-eye tx-20 btn btn-primary bt_cus" href="{{route('viewIndividualProperty',$property->id)}}" title="detailed view"></a></td>
                                </tr>
                                @endforeach
                               

                            </tbody>
                        </table>

                    @else
                        <h4 class="text-center tx-danger">There is no Property to view</h4>
                    @endif
                    <div class="d-flex justify-content-end">
                        {!! $properties->links() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

    <script>
        const url = '{{ url('/') }}';
    </script>
@endsection
