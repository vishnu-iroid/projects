@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Facility Management</span>
            <span class="breadcrumb-item active">Service Request</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Service Request</h4>
                            <p class="card-category">List of all Service Requests</p>
                        </div>

                        <div class="col-md-6 d-flex justify-content-end">
                            @if ($status == '1')
                                <a href="{{ route('showUserRequest', ['status' => 1]) }}"
                                    class="nav-link tx-white d-flex align-items-center card-active">
                                    <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                                </a>

                                <a href="{{ route('showUserRequest', ['status' => 0]) }}"
                                    class="nav-link tx-white d-flex align-items-center ">
                                    <i class="fad fa-check-user tx-20"></i>&nbspUser
                                </a>
                            @elseif($status == '0')
                                <a href="{{ route('showUserRequest', ['status' => 1]) }}"
                                    class="nav-link tx-white d-flex align-items-center ">
                                    <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                                </a>

                                <a href="{{ route('showUserRequest', ['status' => 0]) }}"
                                    class="nav-link tx-white d-flex align-items-center card-active">
                                    <i class="fad fa-check-user tx-20"></i>&nbspUser
                                </a>
                            @endif

                        </div>
                        <!-- <div class="col-md-8 row d-flex justify-content-center">

                                                                                            </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive wrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-10p">Date</th>
                                <th class="wd-10p">Property Name</th>
                                <th class="wd-10p">Unit Name</th>
                                <th class="wd-5">Service</th>
                                <th class="wd-20">Description</th>
                                <th class="wd-10">Attachments</th>
                                <th class="wd-20p">Status</th>
                                <th class="wd-20p">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if ($status == 0)
                                @foreach ($request as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->date }}</td>
                                        <td>{{ $row->user_property_related->prop_rel->property_name }}</td>
                                        @if ($row->user_property_related->prop_rel->builder_id != null)
                                            @php
                                                $data = DB::table('owner_properties')
                                                    ->select('id', 'property_name')
                                                    ->where('id', $row->user_property_related->prop_rel->builder_id)
                                                    ->first();
                                            @endphp

                                            <td>{{ $row->user_property_related->prop_rel->property_name }}</td>
                                        @else
                                            <td>---</td>
                                        @endif
                                        <td>{{ $row->service_related->service }}</td>
                                        <td>{{ $row->description }}</td>
                                        <td>

                                            @foreach ($row->user_doc as $keys)
                                                @if ($keys->document_type == 0)
                                                    <a href="{{ url($keys->document) }}"
                                                        class="btn btn-success btn-xs mrg" data-placement="top"
                                                        data-toggle="tooltip" data-original-title="Download"><i
                                                            class=" far fa-file-alt"></i></a>
                                                    <!-- <a href="{{ url('downloadfile' . $keys->document) }}"> <i class=" far fa-file-alt"></i> </a> -->
                                                @endif
                                            @endforeach

                                        </td>

                                        <td>
                                            @if ($row->status == 0)
                                                @if ($row->send_owner_approval == 1 && $row->inspection_team == null)
                                                    @if ($row->owner_approval_status)
                                                        @if ($row->owner_approval_status->admin_approve != 1)
                                                            <span class="badge rounded-pill bg-danger text-white">Waiting
                                                                for owner confirmation </span>
                                                        @else
                                                            @if ($row->inspection_team != null)
                                                                <span class="badge rounded-pill bg-danger text-white">
                                                                    Inspection Pending </span>
                                                            @else
                                                                <span class="badge rounded-pill bg-info text-white"> Sent
                                                                    for Owner Approval </span>
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if ($row->inspection_team != null)
                                                            <span class="badge rounded-pill bg-danger text-white">
                                                                Inspection Pending </span>
                                                        @else
                                                            <span class="badge rounded-pill bg-info text-white"> Sent for
                                                                Owner Approval </span>
                                                        @endif
                                                    @endif
                                                @else
                                                    @if ($row->inspection_team != null)
                                                        <span class="badge rounded-pill bg-danger text-white"> Inspection
                                                            Pending </span>
                                                    @else
                                                        <span class="badge rounded-pill bg-danger text-white"> Requested
                                                        </span>
                                                    @endif
                                                @endif
                                            @elseif($row->status == 1)
                                                @if ($row->service_provider != null)
                                                    @if (!empty($row->service_payemnt_doc))
                                                        @if (empty($row->service_payemnt_doc->is_verified))
                                                            <span class="badge rounded-pill bg-primary text-white"> Payment
                                                                Not Yet Verified </span>
                                                        @endif
                                                    @else
                                                        @if ($row->service_provider != null)
                                                            <span class="badge rounded-pill bg-primary text-white ">
                                                                Assigned service provider</span><br>
                                                        @endif
                                                        <span class="badge rounded-pill bg-danger text-white mt-2"> Payment
                                                            Pending</span>
                                                    @endif
                                                @else
                                                    <span class="badge rounded-pill bg-danger text-white"> Please assign
                                                        service provider</span>
                                                    <span class="badge rounded-pill bg-danger text-white mt-2"> Payment
                                                        Pending</span>
                                                @endif
                                            @elseif($row->status == 2)
                                                @if ($row->service_provider != null)
                                                    @if (!empty($row->service_payemnt_doc))
                                                        @if (empty($row->service_payemnt_doc->is_verified))
                                                            <span class="badge rounded-pill bg-primary text-white"> Payment
                                                                Not Yet Verified </span>
                                                        @endif
                                                    @else
                                                        @if ($row->service_provider != null)
                                                            <span class="badge rounded-pill bg-primary text-white ">
                                                                Assigned service provider</span><br>
                                                        @endif
                                                        <span class="badge rounded-pill bg-danger text-white mt-2"> Payment
                                                            Pending</span>
                                                    @endif
                                                @else
                                                    <span class="badge rounded-pill bg-danger text-white mt-2"> Please
                                                        assign service provider</span>
                                                @endif
                                                <span class="badge rounded-pill bg-success text-white mt-2"> Payment Done </span>
                                            @elseif($row->status == 3)
                                                <span class="badge rounded-pill bg-success text-white"> Service Completed
                                                </span>
                                            @elseif($row->status == 4)
                                                <span class="badge rounded-pill bg-warning text-white"> Service Cancelled
                                                </span>
                                            @endif


                                        </td>
                                        <td>
                                            @if ($row->status == 0 && $row->inspection_team == '')
                                                @if ($row->send_owner_approval == null)
                                                    <a class="fad fa-unlock-alt tx-20 reject-button btn btn-primary mb-1 bt_cus" href="javascript:0;"
                                                        data-property="" title="send owner approval"
                                                        onclick="ApprovelFunction('{{ $row->id }}','1')"></a>
                                                @endif
                                                <a class="fad fa-user-alt tx-20 reject-button btn btn-info mb-1 bt_cus" href="javascript:0;"
                                                    data-property="" title="Select Inspection Team"
                                                    onclick="Inspection('{{ $row->id }}','1')"></a>
                                            @elseif($row->status == 0 && $row->inspection_team != null)
                                                <a class="fad fa-user-alt tx-20 reject-button btn btn-primary mb-1 bt_cus" href="javascript:0;"
                                                    data-property="" title="Inspection Completeds" 
                                                    onclick="InspectionCompleted('{{ $row->id }}', '1')"></a>
                                            @elseif($row->status == 1 || $row->status == 2)
                                                @if ($row->service_provider != null)
                                                    @if (!empty($row->service_payemnt_doc))
                                                        @if (!empty($row->service_payemnt_doc->is_verified))
                                                            <a class=" tx-20 reject-button" href="javascript:0;"
                                                                data-property="" title="Service Completed"
                                                                style="color: green "
                                                                onclick="UserServiceCompleted('{{ $row->id }}',  '1')"><i
                                                                    class="fas fa-check"></i></a>
                                                        @else
                                                            <!-- <a class=" tx-20 reject-button" href="javascript:0;" data-property="" title="Service Completed" style="color: green " onclick="UserServiceCompleted('{{ $row->id }}',  '1')"><i class="fas fa-check"></i></a> -->
                                                            <!-- <span class="badge rounded-pill bg-primary text-white"> Payment Not Yet Verified </span> -->
                                                        @endif
                                                    @else
                                                        @if ($row->service_provider != null)
                                                            <!-- <span class="badge rounded-pill bg-primary text-white "> Assigned service provider</span><br> -->
                                                        @else
                                                            <a class="fad fa--alt tx-20 reject-button" href="javascript:0;"
                                                                data-property="" title="Assign Service Provider"
                                                                style="color: green "
                                                                onclick="PaymentCompleted('{{ $row->id }}','1')"><i
                                                                    class="fas fa-wallet"></i></a>
                                                        @endif
                                                        <!-- <span class="badge rounded-pill bg-danger text-white mt-2"> Payment Pending</span> -->
                                                    @endif
                                                @else
                                                    <a class="fad fa--alt tx-20 reject-button" href="javascript:0;"
                                                        data-property="" title="Assign Service Provider"
                                                        style="color: green "
                                                        onclick="PaymentCompleted('{{ $row->id }}','1')"><i
                                                            class="fas fa-wallet"></i></a>
                                                    <!-- <span class="badge rounded-pill bg-danger text-white mt-2"> Please assign service provider</span> -->
                                                @endif
                                                <!-- <a class="fad fa--alt tx-20 reject-button" href="javascript:0;" data-property="" title="Assign Service Provider" style="color: green " onclick="PaymentCompleted('{{ $row->id }}','1')"><i class="fas fa-wallet"></i></a> -->
                                            @endif
                                            <a class="fad fa-view tx-20 viewService-button " href="javascript:0;"
                                                data-serviceid="{{ $row->id }}" data-type="0" title="view"><i
                                                    class="fa fa-eye btn btn-primary mb-1 bt_cus" aria-hidden="true"></i></a>
                                        </td>
                                        {{-- onclick="viewService({{ $row->id }})" --}}
                                    </tr>
                                @endforeach
                            @elseif($status == '1')
                                @foreach ($request as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->date }}</td>
                                        <td>{{ $row->property_name }}</td>
                                        @if ($row->builder_id != null)
                                        @php
                                         $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->builder_id)->first();
                                        @endphp
                                  
                                    <td>{{ $row->property_name }}</td>
                                @else
                                    <td>---</td>
                                    
                                @endif
                                        <td>{{ $row->service_related->service }}</td>
                                        <td>{{ $row->description }}</td>
                                        <td>

                                            @foreach ($row->owner_doc as $keys)
                                                <a href="{{ asset($keys->document) }}" class="btn btn-success btn-xs mrg"
                                                    data-placement="top" data-toggle="tooltip"
                                                    data-original-title="Download" download".png><i
                                                        class=" far fa-file-alt"></i></a>
                                            @endforeach

                                        </td>
                                        <td>
                                            @if ($row->status == 0)
                                                @if ($row->inspection_team != null)
                                                    <span class="badge rounded-pill bg-danger text-white"> Inspection
                                                        Pending </span>
                                                @else
                                                    <span class="badge rounded-pill bg-danger text-white"> Request has been
                                                        sent </span>
                                                @endif
                                            @elseif($row->status == 1)
                                                @if ($row->service_provider != null)
                                                    @if (!empty($row->service_payemnt_doc))
                                                        @if (!empty($row->service_payemnt_doc->is_verified))
                                                            <span class="badge rounded-pill bg-primary text-white"> Payment
                                                                Verified </span>
                                                        @else
                                                            <span class="badge rounded-pill bg-primary text-white"> Payment
                                                                Not Yet Verified </span>
                                                        @endif
                                                    @else
                                                        @if ($row->service_provider != null)
                                                            <span class="badge rounded-pill bg-primary text-white ">
                                                                Assigned service provider</span><br>
                                                        @endif
                                                        <span class="badge rounded-pill bg-danger text-white mt-2"> Payment
                                                            Pending</span>
                                                    @endif
                                                @else
                                                    <span class="badge rounded-pill bg-danger text-white mt-2"> Please
                                                        assign service provider</span>
                                                @endif
                                            @elseif($row->status == 2)
                                                <span class="badge rounded-pill bg-success text-white"> Payment Done </span>
                                            @elseif($row->status == 3)
                                                <span class="badge rounded-pill bg-success text-white"> Service Completed
                                                </span>
                                            @elseif($row->status == 4)
                                                <span class="badge rounded-pill bg-warning text-white"> Service Cancelled
                                                </span>
                                            @endif

                                        </td>
                                        <td>
                                            <!-- {{-- <a class="fad fa-eye tx-20 view-button mr-1" href="{{route('bookingDetails', ['id' => $row->id])}}" data-booking="{{$row->id}}" title="View"></a> --}} -->


                                            @if ($row->inspection_team == 0 && $row->status == 0)
                                                <a class="fad fa-user-alt tx-20 reject-button" href="javascript:0;"
                                                    data-property="" title="Select Inspection Team" style="color: green "
                                                    onclick="ownerInspection('{{ $row->o_id }}','1')"></a>
                                            @elseif($row->inspection_team !== 0 && $row->status == 0)
                                                <a class="fad fa-user-alt tx-20 reject-button" href="javascript:0;"
                                                    data-property="" title="Inspection Completed" style=""
                                                    onclick="ownerInspectionCompleted('{{ $row->o_id }}', '1')"></a>
                                            @elseif($row->status == 1 || $row->status == 2)
                                                @if ($row->service_provider != null)
                                                    @if (!empty($row->service_payemnt_doc))
                                                        @if (!empty($row->service_payemnt_doc->is_verified))
                                                            <a class=" tx-20 reject-button" href="javascript:0;"
                                                                data-property="" title="Service Completed"
                                                                style="color: green "
                                                                onclick="OwnerServiceCompleted('{{ $row->o_id }}',  '1')"><i
                                                                    class="fas fa-check"></i></a>
                                                            <!-- <a class=" tx-20 reject-button" href="javascript:0;" data-property="" title="Service Completed" style="color: green " onclick="UserServiceCompleted('{{ $row->id }}',  '1')"><i class="fas fa-check"></i></a> -->
                                                        @else
                                                            <!-- <a class=" tx-20 reject-button" href="javascript:0;" data-property="" title="Service Completed" style="color: green " onclick="UserServiceCompleted('{{ $row->id }}',  '1')"><i class="fas fa-check"></i></a> -->
                                                            <!-- <span class="badge rounded-pill bg-primary text-white"> Payment Not Yet Verified </span> -->
                                                        @endif
                                                    @else
                                                        @if ($row->service_provider != null)
                                                            <!-- <span class="badge rounded-pill bg-primary text-white "> Assigned service provider</span><br> -->
                                                        @else
                                                            <a class="fad  tx-20 reject-button" href="javascript:0;"
                                                                data-property="" title="Assign Service Provider"
                                                                style="color: green "
                                                                onclick="OwnerPaymentCompleted('{{ $row->o_id }}',  '1')"><i
                                                                    class="fas fa-wallet"></i></a>
                                                        @endif
                                                        <!-- <span class="badge rounded-pill bg-danger text-white mt-2"> Payment Pending</span> -->
                                                    @endif
                                                @else
                                                    <a class="fad  tx-20 reject-button" href="javascript:0;"
                                                        data-property="" title="Assign Service Provider"
                                                        style="color: green "
                                                        onclick="OwnerPaymentCompleted('{{ $row->o_id }}',  '1')"><i
                                                            class="fas fa-wallet"></i></a>
                                                    <!-- <span class="badge rounded-pill bg-danger text-white mt-2"> Please assign service provider</span> -->
                                                @endif
                                            @endif

                                            <a class="fad fa-view tx-20 viewService-button mr-1 p-2" href="javascript:0;"
                                                data-serviceid="{{ $row->o_id }}" data-type="1" title="view"><i
                                                    class="fa fa-eye" aria-hidden="true"></i></a>

                                        </td>


                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
{{$request->links()}}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->



    <!-- Request Approval  Modal -->

    <div class="modal fade" id="ApprovelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Send Approvel Request</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Approval Request Send to Owner
                        <input type="hidden" name="ownerId" id="ownerId">
                        <input type="hidden" name="action" id="action">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class sendApprovelBtn">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Assign Inspection Team  Modal -->
    <div class="modal fade" id="addInspection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Inspection Team</h4>
                </div>
                <form action="javascript:;">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" id="memberid" name="memberid" />
                                        <div class="form-group">

                                            <label>Inspection Team <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="memberid" id="memberid">
                                                <option value=""></option>
                                                @foreach ($inspection_team as $ins)
                                                    <option value="{{ $ins->id }}">{{ $ins->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();"> Close </button>
                        <button type="submit" class="btn btn-primary addBtn" id="propAddBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Inspection Completed Modal -->

    <div class="modal fade" id="inspectionCompleted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Inspection Request</h4>
                </div>
                <form id="form-add" action="javascript:;">
                    <div class="modal-body">
                        <div class="tab-content" id="form-add">
                            <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Estimate Bill <span class="tx-danger">*</span></label>
                                            <div class="form-group image-class" style="display: none;">
                                                <img class="img-thumbnail image-display" src="#" alt="agent" width="100">
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="image" name="image">
                                                <label class="custom-file-label" id="file_label">Choose file</label>
                                                <input type="hidden" id="current_image" name="current_image">
                                                <input type="hidden" id="request_id" name="request_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Owner Amount <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="owner_amount" id="owner_amount" placeholder="Owner Amount">
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label>Customer Amount <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="customer_amount" id="customer_amount" placeholder="Customer Amount">
                                        </div>
                                    </div>

                                </div> --}}
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();"> Close </button>
                        <button type="submit" class="btn btn-primary addsubmit" id="propAddBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Payment Completed Modal -->

    <div class="modal fade" id="PaymentCompleted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Assign Service Provider</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">

                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="service_allocate" id="service_allocate">

                        <label>Service Provider Name <span class="tx-danger">*</span></label>
                        <select class="form-control" name="providerid" id="provider_id">
                            <option value="" selected disabled>Choose Company</option>
                            @foreach ($service_provider as $ins)
                                <option value="{{ $ins->id }}">{{ $ins->company }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class payBtn">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!---------------------- Owner Insecption  ------------>

    <!-- Assign Inspection Team  Modal -->
    <div class="modal fade" id="addOwnerInspection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title"> Inspection Team</h4>
                </div>
                <form action="javascript:;">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label>Inspection Team <span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="ownermemberid" id="ownermemberid">
                                                <option value=""></option>
                                                @foreach ($inspection_team as $ins)
                                                    <option value="{{ $ins->id }}">{{ $ins->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                               

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();"> Close </button>
                        <button type="submit" class="btn btn-primary addOwnerBtn" id="propAddBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Owner Inspection Modal -->


    <div class="modal fade" id="ownerinspectionCompleted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Inspection Complete</h4>
                </div>
                <form id="ownerform-add" action="javascript:;">
                    <div class="modal-body">
                        <div class="tab-content" id="form-add">
                            <div class="tab-pane fade show active" id="profile" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Estimate Bill <span class="tx-danger">*</span></label>
                                            <div class="form-group image-class" style="display: none;">
                                                <img class="img-thumbnail image-display" src="#" alt="agent" width="100">
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="file" name="file">
                                                <label class="custom-file-label" id="file_label">Choose file</label>
                                                <input type="hidden" id="owner_current_image" name="current_image">
                                                <input type="hidden" id="ownerrequest_id" name="ownerrequest_id">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Owner Amount <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="owner_amount" id="owner_amount" placeholder="Owner Amount">
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">

                                            <label>Customer Amount <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="customer_amount" id="customer_amount" placeholder="Customer Amount">
                                        </div>
                                    </div>

                                </div> --}}

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();"> Close </button>
                        <button type="submit" class="btn btn-primary addownersubmit" id="propAddBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--------------------- owner InspectionCompleted ------------------->

    <!-------owner Service Provider -->

    <div class="modal fade" id="OwnerPaymentCompleted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Assign Service Provider</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">

                        <input type="hidden" name="requestid" id="requestid">
                        <input type="hidden" name="provider_action_val" id="provider_action">
                        <div class="form-group">

                            <label>Service Provider <span class="tx-danger">*</span></label>
                            <select class="form-control" name="providerid" id="providerid">
                                <option value="" selected disabled>Choose one</option>
                                @foreach ($service_provider as $ins)
                                    <option value="{{ $ins->id }}">{{ $ins->name }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class OwnerpayBtn">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="OwnerServiceCompleted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-service-title">Service Completed</h4>
                </div>
                <form action="javascript:;" id="service-completed-form">
                    <div class="modal-body">

                        <input type="hidden" name="serviceRequestId" id="serviceRequestId">
                        <input type="hidden" name="action_service" id="action_service">
                        <div class="form-group">

                            <label>Submit Service as completed</label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-success reject-class completeServiceBtn">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="modal fade" id="UserServiceCompleted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-service-title">Service Completed</h4>
                </div>
                <form action="javascript:;" id="service-completed-form">
                    <div class="modal-body">

                        <input type="hidden" name="userServiceRequestId" id="userServiceRequestId">
                        <input type="hidden" name="userAction_service" id="userAction_service">
                        <div class="form-group">

                            <label>Submit Service as completed</label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-success reject-class completeUserServiceBtn">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{-- view  Modal --}}
    <div class="modal fade" id="viewServiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title tx-primary" id="exampleModalLabel">View Services</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                            <th>ID</th>
                            <th>DATE</th>
                            <th>PROPERTY</th>
                            <th>SERVICE</th>
                            <th>DESCRIPTION</th>
                            <th>STATUS</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="serviceid"></td>
                                <td class="serviceDate"></td>
                                <td class="serviceProperty"></td>
                                <td class="service_Service"></td>
                                <td class="serviceDescription"></td>
                                <td class="serviceStatus"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>



    <!-------------------------------------------- End ------------------------->


    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
   
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Property | Service Management`);
        $("#service_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: false,
                autoWidth: false,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
          
        });
    </script>
    <script>
        function ApprovelFunction(id, action) {

            $('#ownerId').val(id);
            $('#action').val(action);
            if (action == 1) {
                $('.reject-modal-title').text('Owner Approval Request');
                $('.sendApprovelBtn').text('Submit');
            }
            $('#ApprovelModal').modal()
        }
    </script>
    <script>
        $('.sendApprovelBtn').click(function() {
            var id = $('#ownerId').val()
            var action = $('#action').val()


            $(".sendApprovelBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/owner/approval/" + id + '/' + action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {

                    $(".sendApprovelBtn").text('Processing..');
                },
                success: function(data) {

                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);

                },
                error: function(xhr, data) {
                    console.log(xhr);
                }

            });
        })
    </script>
    <script>
        var RequestId = '';

        function Inspection(requestid, memberid) {

            RequestId = requestid;

            $('#memberid').val(memberid);

            $('#addInspection').modal()
        }

        $('.addBtn').click(function() {

            var id = RequestId;

            var memberid = $('#memberid').val()


            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/user_request/Inspection/" + id + '/' + memberid,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {

                    $(".addBtn").text('Processing..');
                },
                success: function(data) {

                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);

                    RequestId = '';

                },
                error: function(xhr, data) {
                    console.log(xhr);
                }

            });
        })
    </script>
    <script>
        var RequestId = '';

        function InspectionCompleted(requestid, member_id) {

            RequestId = requestid;
            $('#request_id').val(requestid);
            $('#inspectionCompleted').modal()
        }

        $("#form-add").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                image: {
                    required: function() {
                        if ($('#id').val()) {
                            return false
                        }
                        return true
                    },
                },
            },
            messages: {
                lat: "Image is required",

            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addrequestEstimate') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addsubmit").text('Processing..');
                    },
                    success: function(data) {
                        $(".addsubmit").text('Submit');
                        $(".addsubmit").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                    error: function(xhr, data) {
                        console.log(xhr);
                    }

                });
            },
        });
    </script>

    <script>
        function PaymentCompleted(id, action) {

            $('#id').val(id);


            if (action == 1) {
                $('.reject-modal-title').text('Assign Service Provider');
                $('.sendApprovelBtn').text('Submit');
            }
            $('#PaymentCompleted').modal()
        }
    </script>
    <script>
        $('.payBtn').click(function() {
            var id = $('#id').val()
            var action = $('#provider_id').val();


            $(".payBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/userrequest/approval/" + id + '/' + action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {

                    $(".payBtn").text('Processing..');
                },
                success: function(data) {

                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);

                },
                error: function(xhr, data) {
                    console.log(xhr);
                }

            });
        })
    </script>

    <!-----------------------------Owner Insecption Function----------------------->


    <script>
        var RequestId = '';

        function ownerInspection(requestid, memberid) {

            RequestId = requestid;

            $('#memberid').val(memberid);

            $('#addOwnerInspection').modal()
        }

        $('.addownerBtn').click(function() {


            var id = RequestId;
            var memberid = $('#ownermemberid').val()


            $(".addownerBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/owner_request/Inspection/" + id + '/' + memberid,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addownerBtn").text('Processing....');
                },
                success: function(data) {

                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);

                    RequestId = '';

                },
                error: function(xhr, data) {
                    console.log(xhr);
                }

            });
        })
    </script>

    <!-- end OwnerInspection -->
    <!-------- Owner Inspection Complete ------->
    <script>
        var RequestId = '';

        function ownerInspectionCompleted(id, member_id) {

            RequestId = id;
            $('#ownerrequest_id').val(id);
            $('#ownerinspectionCompleted').modal()
        }

        $("#ownerform-add").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                file: {
                    required: function() {
                        if ($('#id').val()) {
                            return false
                        }
                        return true
                    },
                },
            },
            messages: {
                file: "Images is required",

            },
            submitHandler: function(form) {
                var form = document.getElementById("ownerform-add");
                var formData = new FormData(form);
                $(".addownersubmit").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addownerrequestEstimate') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {

                        $(".addownersubmit").text('Processing..');
                    },
                    success: function(data) {
                        $(".addownersubmit").text('Submit');
                        $(".addownersubmit").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                    error: function(xhr, data) {
                        console.log(xhr);
                    }

                });
            },
        });
    </script>

    <script>
        function OwnerPaymentCompleted(id, action) {
            $('#requestid').val(id);
            $('#action').val(action);

            if (action == 1) {
                $('.reject-modal-title').text('Assign Service Provider');
                $('.sendApprovelBtn').text('Submit');
            }
            $('#OwnerPaymentCompleted').modal()
        }

        function OwnerServiceCompleted(id, action) {
            $('#serviceRequestId').val(id);
            if (action == 1) {
                $('.reject-service-title').text('Service Completed');
                $('.completeServiceBtn').text('Submit');
            }
            $('#OwnerServiceCompleted').modal()
        }


        $('.completeServiceBtn').click(function() {
            var id = $('#serviceRequestId').val();

            $(".completeServiceBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/request/mark-completed/" + id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {

                    $(".completeServiceBtn").text('Processing..');
                },
                success: function(data) {

                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);

                },
                error: function(xhr, data) {
                    console.log(xhr);
                }

            });
        });

        function UserServiceCompleted(id, action) {
            $('#userServiceRequestId').val(id);
            if (action == 1) {
                $('.reject-service-title').text('Service Completed');
                $('.completeUserServiceBtn').text('Submit');
            }
            if (id) {
                $('#UserServiceCompleted').modal();
            }
        }
        $('.completeUserServiceBtn').click(function() {
            var id = $('#userServiceRequestId').val();

            $(".completeUserServiceBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "POST",
                url: url + "/request/mark-user-completed/" + id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {

                    $(".completeUserServiceBtn").text('Processing..');
                },
                success: function(data) {

                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);

                },
                error: function(xhr, data) {
                    console.log(xhr);
                }

            });
        });
    </script>
    <script>
        $('#provider_id').change(function() {
            var provider = $(this).val();
            $('#service_allocate').val(provider);
        });

        $('#providerid').change(function() {
            var provider = $(this).val();
            $('#provider_action').val(provider);
        });
        $('.OwnerpayBtn').click(function() {
            var id = $('#requestid').val();
            var provider = $('#provider_action').val();
            $(".OwnerpayBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/request/approval/" + id + '/' + provider,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {

                    $(".OwnerpayBtn").text('Processing..');
                },
                success: function(data) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                },
                error: function(xhr, data) {
                    console.log(xhr);
                }
            });
        });

        // function viewService(id) {
        //     alert(id)
        //     $.ajax({
        //         url: url + "/service-view/" + id,
        //         type: "GET",
        //         processData: false,
        //         async: true,
        //         header: {
        //             "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        //         },
        //         success: function(data) {
        //             console.log(data);
        //             if (data.status === true) {
        //                 toastr["success"](data.response);
        //                 setTimeout(function() {
        //                     window.location.href = "";
        //                 }, 1000);
        //             } else {
        //                 toastr["error"](data.response);
        //             }
        //         }
        //     });
        // }

        //view service on user
        $(document).on('click', '.viewService-button', function() {
            var serviceid = $(this).data('serviceid');
            var status_type = $(this).data('type');
            $.ajax({
                url: url + "/service-view/" + serviceid + "/" + status_type,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {

                    if (data.status === true) {
                        if (data.response.status == 0) {
                            $(".serviceid").html(data.response.request.id);
                            $(".serviceDate").html(data.response.request.date);
                            if (data.response.request.prop_related.property_name)
                                $(".serviceProperty").html(data.response.request.prop_related
                                    .property_name);
                            else
                                $(".serviceProperty").html('');
                            $(".service_Service").html(data.response.request.service_related.service);
                            $(".serviceDescription").html(data.response.request.description);
                            if (data.response.request.status == 0) {
                                if (data.response.request.send_owner_approval == 1 && data.response
                                    .request
                                    .inspection_team == null) {

                                    if (data.response.request.owner_approval_status) {
                                        if (data.response.request.owner_approval_status.admin_approve !=
                                            1)
                                            $(".serviceStatus").html('Waiting for owner confirmation');
                                        else {
                                            if (data.response.request.inspection_team != null)
                                                $(".serviceStatus").html('Inspection Pending');

                                            else
                                                $(".serviceStatus").html('Sent for Owner Approval');
                                        }
                                    } else {
                                        if (data.response.request.inspection_team != null)
                                            $(".serviceStatus").html(' Inspection Pending');
                                        else
                                            $(".serviceStatus").html('Sent for Owner Approval');
                                    }
                                } else {
                                    if (data.response.request.inspection_team != null)
                                        $(".serviceStatus").html('Inspection Pending');
                                    else
                                        $(".serviceStatus").html('Requested');
                                }
                            } else if (data.response.request.status == 1) {
                                if (data.response.request.service_provider != null) {
                                    if (data.response.request.service_payemnt_doc != null) {
                                        if (data.response.request.service_payemnt_doc
                                            .is_verified == null) {
                                            $(".serviceStatus").html('Payment Not Yet Verified');
                                        }
                                    } else {
                                        if (data.response.request.service_provider != null) {
                                            $(".serviceStatus").html('Assigned service provider');
                                        }
                                        $(".serviceStatus").html('Payment pending');
                                    }
                                } else {
                                    $(".serviceStatus").html('Please assign service provider');
                                    $(".serviceStatus").html('Payment Pending');
                                }
                            } else if (data.response.request.status == 2) {
                                if (data.response.request.service_provider != null) {
                                    if (!data.response.request.service_payemnt_doc != null) {
                                        if (data.response.request.service_payemnt_doc.is_verified ==
                                            0) {
                                            $(".serviceStatus").html('Payment Not Yet Verified');
                                        }

                                    } else {
                                        if (data.response.request.service_provider != null) {


                                            $(".serviceStatus").html('Assigned service provider');
                                        }
                                        $(".serviceStatus").html('Payment Pending');
                                    }
                                } else {
                                    $(".serviceStatus").html('Please assign service provider');
                                }

                                $(".serviceStatus").html('Payment Done');

                            } else if (data.response.request.status == 3) {
                                $(".serviceStatus").html('Service Completed');
                            } else if (data.response.request.status == 4) {
                                $(".serviceStatus").html('Service Cancelled');
                            }
                            $('#viewServiceModal').modal()
                        } else if (data.response.status == 1) {
                            $(".serviceid").html(data.response.request.o_id);
                            $(".serviceDate").html(data.response.request.date);
                            if (data.response.request.property_name)
                                $(".serviceProperty").html(data.response.request.property_name);
                            else
                                $(".serviceProperty").html('');
                            $(".service_Service").html(data.response.request.service_related
                                .service);
                            $(".serviceDescription").html(data.response.request.description);
                            if (data.response.request.status == 0) {
                                if (data.response.request.inspection_team != null)
                                    $(".serviceStatus").html('Inspection Pending');
                                else
                                    $(".serviceStatus").html('Request has been sent');

                            } else if (data.response.request.status == 1) {
                                if (data.response.request.service_provider != null) {
                                    if (data.response.request.service_payemnt_doc != null) {
                                        if (data.response.request.service_payemnt_doc.is_verified !=
                                            null)
                                            $(".serviceStatus").html('Payment Verified');
                                        else
                                            $(".serviceStatus").html('Payment Not Yet Verified');
                                    } else {
                                        if (data.response.request.service_provider != null)
                                            $(".serviceStatus").html('Assigned service provider');

                                        $(".serviceStatus").html(' Payment Pending');

                                    }

                                } else
                                    $(".serviceStatus").html('Please assign service provider');

                            } else if (data.response.request.status == 2) {
                                $(".serviceStatus").html('Payment Done');
                            } else if (data.response.request.status == 3) {
                                $(".serviceStatus").html('Service Completed');

                            } else if (data.response.request.status == 4) {
                                $(".serviceStatus").html('Service Cancelled');

                            }
                            $('#viewServiceModal').modal()
                        }
                    } else {
                        toastr["error"](data.response);
                    }

                }
            });
        });
    </script>
@endsection
