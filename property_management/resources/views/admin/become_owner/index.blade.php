@extends('admin.layout.app')
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Property Management</span>
            <span class="breadcrumb-item active">Request For Ownership</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Request For Ownership</h4>
                            <p class="card-category">List of all ownership requests</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="" class="table data-table datatableBecomeowner" style="width:100%">

                        <thead>
                            <tr>
                                <th class="wd-10p">Id</th>
                                <th class="wd-10p">Name</th>
                                <th class="wd-10p">Phone</th>
                                <th class="wd-15p">Email</th>
                                <th class="wd-10p">No Of Rental Properties</th>
                                <th class="wd-10p">No Of Sale Properties</th>
                                <th class="wd-15p">Property Relation</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    {{-- <table id="datatable" class="table table-hover table-bordered display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-15p">Id</th>
                                <th class="wd-15p">User Id</th>
                                <th class="wd-15p">Name</th>
                                <th class="wd-15p">Phone</th>
                                <th class="wd-20p">Email</th>
                                <th class="wd-20p">No Of Rental Properties</th>
                                <th class="wd-20p">No Of Sale Properties</th>
                                <th class="wd-20p">Property Relation</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead> --}}
                    {{-- <tbody>
                        @foreach ($become_owner as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->user_id }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->phone }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->no_of_rental_properties }}</td>
                                <td>{{ $row->no_of_sale_properties }}</td>
                                <td>{{ $row->property_rel }}</td>
                                <td>
                                    <a href="{{ route('showOwner') }}" class="fad fa-arrow-circle-right tx-20  mr-1"
                                        href="javascript:0;" title="Add as Owner" style="color: blue "></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    </div> --}}
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    {{-- view Modal --}}
    <div class="modal fade" id="becomeAnOwnerModal" tabindex="-1" role="dialog" aria-labelledby="becomeAnOwnerModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title tx-primary" id="becomeAnOwnerModalLabel">View Details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <p><label>Id</label>: <label id="ownerId"> </label></p>
               <p><label>UserId</label>: <label id="userId"></label></p>
               <p><label>User Name</label>: <label id="userName"></label></p>
               <p><label>Become An Owner Name</label>: <label id="becomeownerName"></label></p>
               <p><label>Email</label>: <label id="useremail"></label></p>
               <p><label>Phone</label>: <label id="userPhone"></label></p>
               <p><label>No.of Rental Properties</label>: <label id="NoOfRentalProperties"></label></p>
               <p><label>No of Sale Properties</label>: <label id="NoOfSaleProperties"></label></p>
               <p><label>Property Rel</label>: <label id="property_rel"></label></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              
            </div>
          </div>
        </div>
      </div>
@endsection



@section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

    {{-- <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script> --}}



    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script> --}}
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    {{-- <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
     <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script> --}}
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>


    <script>
        $(function() {
            var table = $('.datatableBecomeowner').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ route('becomeAnOwnerListing') }}",
                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'no_of_rental_properties',
                        name: 'no_of_rental_properties'
                    },
                    {
                        data: 'no_of_sale_properties',
                        name: 'no_of_sale_properties'
                    },
                    {
                        data: 'property_rel',
                        name: 'property_rel'
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        orderable: false,
                        searchable: false
                    }
                ]
            });
         

        });

        $(document).on('click', '.viewBecomeAnOwner', function(e) {
             e.preventDefault(); 
             let id = $(this).data('becomeowner-id');
            //  alert(id);
         
             $.ajax({
                 url: "/become-list/view/"+id,
                 type: "GET",
                 processData: false,
                 async: true,
                 header: {
                     "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                 },
                 success: function(data) {
                     if (data.status === true) {
                     $("#ownerId").html(data.response.id);
                    $("#userId").html(data.response.user_id);
                     $("#userName").html(data.response.username);
                     $("#becomeownerName").html(data.response.name);
                     $("#useremail").html(data.response.email);
                     $("#userPhone").html(data.response.phone);
                     $("#NoOfRentalProperties").html(data.response.no_of_rental_properties);
                     $("#NoOfSaleProperties").html(data.response.no_of_sale_properties);
                     $("#property_rel").html(data.response.property_rel);
                         $('#becomeAnOwnerModal').modal()
                     } else {
                         toastr["error"](data.response);
                     }
                 }
             });
         });
    </script>
@endsection
