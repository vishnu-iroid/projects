@extends('admin.layout.app')


<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 500 !important;
    }

    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height: 20px;
        border-radius: 5px !important
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }


    .pac-card {
        background-color: #fff;
        border: 0;
        border-radius: 2px;
        box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
        margin: 10px;
        padding: 0 0.5em;
        font: 400 18px Roboto, Arial, sans-serif;
        overflow: hidden;
        font-family: Roboto;
        padding: 0;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding: 10px;
        border-radius: 10px;
    }

    .error {
        color: red !important;
    }

    .ui-datepicker {
        position: fixed;
        top: 270.797px;
        left: 172.5px;
        z-index: 5001 !important;
        display: block;
    }

    #ui-datepicker-div {
        z-index: 5001 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Set Up Management</span>
            <span class="breadcrumb-item active">Company List</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                               <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                   <i class="fas fa-plus-square"></i>&nbsp Add Client
                                               </button>
                                       </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Company List</h4>
                            <p class="card-category">List of all Company</p>
                        </div>
                        {{-- <div class="pd-5 mg-r-20">
                           <a href="#" class="nav-link card-active tx-white d-flex align-items-center"  data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Owner</a>
                       </div> --}}

                        <div class="pd-5 mg-r-20">
                            <a href="" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal"
                                data-target="#addCompanyModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd
                                Company</a>
                        </div>

                    </div>
                </div>


                <div class="card-body">
                    <table id="datatableCompany" class="table data-table printTable" style="width:100%">

                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-15p">Company Name</th>
                                <th class="wd-15p">Logo</th>
                                <th class="wd-10p">Email</th>
                                <th class="wd-15p">Phone</th>
                                <th class="wd-15p">Website</th>
                                <th class="wd-15p">Registration No</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->


    {{-- Add company model --}}

    <div class="modal fade" id="addCompanyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="overflow: hidden">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myModalLabelProperty">Add Company</h4>
                    {{-- <a href="" class="btn btn-primary float-right"  onclick="addOwnerBuilding()">Add as Building</a> --}}
                </div>
                <div class="col-md-12">
                    <div id="tabs">
                        <ul class="nav nav-tabs property-tabs mt-1" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details"
                                    role="tab" aria-controls="details" aria-selected="true">Details</a>
                            </li>
                        </ul>


                        <form class="form-admin" id="form-add-company" action="javascript:;">
                            <input type="hidden" id="companyedit_id" name="companyedit_id">
                            <div class="modal-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="details" role="tabpanel"
                                        aria-labelledby="details-tab">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Company Name</label>
                                                <input type="text" class="form-control" id="company" name="company"
                                                    placeholder="company name">
                                                    @if ($errors->has('name'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('name') }}
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Email</label>
                                                <input type="email" class="form-control" name="email" id="inputEmail4"
                                                    placeholder="Email">
                                                    @if ($errors->has('email'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('email') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Website</label>
                                                <input type="text" class="form-control" id="website" name="website"
                                                    placeholder="Website">
                                                    @if ($errors->has('website'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('website') }}
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Phone</label>
                                                <!-- <input type="text" class="form-control inner" id="phone" name="phone"
                                                    placeholder="phone"  onkeypress="javascript:return isNumber(event)"> -->
                                                <!-- <select name="countryCode" id="countryCode">
                                                    <option value="">91</option>
                                                    @foreach ($countries as $country )
                                                        <option value="{{$country->country_code}}">{{$country->country_code}}</option>
                                                    @endforeach
                                                </select> -->
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1" class="form-control"> 
                                                            <select name="countryCode" id="countryCode" >
                                                                <option value="">91</option>
                                                                @foreach ($countries as $country )
                                                                    <option value="{{$country->country_code}}">{{$country->country_code}}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" id="phone" name="phone"
                                                    placeholder="phone"  onkeypress="javascript:return isNumber(event)" aria-label="Username" aria-describedby="basic-addon1">
                                                </div>
                                                    @if ($errors->has('phone'))
                                                    <div class="alert alert-danger">
                                                        {{ $errors->first('phone') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Contact Person</label>
                                                <?php /*<input type="text" class="form-control inner" id="alternativePhone"
                                                    name="alternativePhone" placeholder="alternativePhone">
                                                <select name="countryCode" id="countryCode">
                                                    <option value="">91</option>
                                                    @foreach ($countries as $country )
                                                        <option value="{{$country->country_code}}">{{$country->country_code}}</option>
                                                    @endforeach
                                                </select>
                                                
                                                @if ($errors->has('contact_person'))
                                                <div class="alert alert-danger">
                                                    {{ $errors->first('contact_person') }}
                                                </div>
                                                @endif  */ ?>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <select name="countryCode" id="countryCode">
                                                                <option value="">91</option>
                                                                @foreach ($countries as $country )
                                                                    <option value="{{$country->country_code}}">{{$country->country_code}}</option>
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control inner" id="alternativePhone"
                                                    name="alternativePhone" placeholder="alternativePhone">
                                                </div>  
                                                @if ($errors->has('contact_person'))
                                                <div class="alert alert-danger">
                                                    {{ $errors->first('contact_person') }}
                                                </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Registration Number</label>
                                                <input type="text" class="form-control" id="registeration_no"
                                                    name="registeration_no" placeholder="registeration_no">
                                            </div>
                                        </div>
                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">VAT</label>
                                                <input type="text" class="form-control" id="vat" name="vat"
                                                    placeholder="Vat No">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="inputEmail4">Upload Logo</label>
                                                <input type="file" class="form-control" id="logo" name="logo"
                                                    placeholder="Logo">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <label for="inputAddress">Address</label>
                                            <textarea class="form-control" name="address" id="address"></textarea>
                                        </div>
                                        <div class="form-row">
                                            <label for="inputAddress">Other Info</label>
                                            <textarea class="form-control" name="other_info" id="other_info"></textarea>
                                        </div>

                                        {{-- <div class="float-right">
                                            <a class="btn btn-primary text-white tabto1 m-2">Next</a>
                                        </div><br> --}}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                                        onclick="window.location.reload();"> Close </button>
                                    <button type="submit" class="btn btn-primary addBtn" id="addPropertyBtn"
                                        >Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="companyDeleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Remove Comapany</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to delete this company?
                        <input type="hidden" name="adminId" id="bookId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtn">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
{{-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> --}}
<script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
{{-- <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

<script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
    <script>
        $("#form-add-company").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                name: {
                    required: true,
                },
                phone: {
                    required: true,
                },

            },
            messages: {
                name: "Name is required",
                phone: "Phone is required",

            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-company");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addCompany') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            window.location.reload(true);   
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

</script>

<script>
        $(function() {
            var table = $('#datatableCompany').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ route('companyDetails') }}",
                    
                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'logo',
                        name: 'logo',
                        render: function(data, type, full, meta) {
                        return '<a href="' + data +
                            '" class="btn btn-warning btn-xs mrg bt_cus"><i class=" far fa-file-alt"></i></a>';
                        }
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'website',
                        name: 'website'
                    },
                    {
                        data: 'register_no',
                        name: 'register_no'
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        orderable: false,
                        searchable: false
                    }
                ]
            });
        });

        // function deleteRow(id){
        //     var token = $("meta[name='csrf-token']").attr("content");
        //     $.ajax({
        //         url: "/company-delete/"+id,
        //         type: 'DELETE',
        //         data: {
        //             "_token": token,
        //         },
        //         success: function (){
        //             if (data.status == 1) {
        //                     toastr["success"](data.response);
        //                     setTimeout(function() {
        //                         window.location.href = "";
        //                     }, 1000);
        //                 } else {
        //                     var html = "";
        //                     $.each(data.response, function(key, value) {
        //                         html += value + "</br>";
        //                     });
        //                     toastr["error"](html);
        //                 }
        //         }
        //     });
        // }

        function deleteFunction(id) {
           $('#bookId').val(id);
           $('#companyDeleteModal').modal();
       };

       $('.deletetBtn').click(function() {
           var id = $('#bookId').val()
           alert(id);
           $(".deletetBtn").prop("disabled", true);
           $(".closeBtn").prop("disabled", true);
           $.ajax({
               type: "DELETE",
               url:"/company-delete/" + id,
               processData: false,
               async: true,
               headers: {
                   "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
               },
               beforeSend: function() {
                   $(".addBtn").text('Processing..');
               },
               success: function(data) {
                   $(".deletetBtn").prop("disabled", false);
                   $(".closeBtn").prop("disabled", false);
                   toastr["success"](data.response);
                   setTimeout(function() {
                       window.location.href = "";
                   }, 1000);
               },
           });
       });
       function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }
</script>
@endsection
