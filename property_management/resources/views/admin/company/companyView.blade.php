@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .vgreen {
        background: #65e715 !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .close {
        cursor: pointer;
        position: absolute;
        top: 15%;
        right: 12%;
        padding: 4px 8px 4px 8px;
        transform: translate(0%, -50%);
        color: white;
        opacity: 1;
    }

    .close:hover {
        background: #bbb;
    }
    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="{{ route('userBookings') }}">Set Up Management</a>
            <span class="breadcrumb-item active"> Company Edit</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                                                                        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                                                            <i class="fas fa-plus-square"></i>&nbsp Add Client
                                                                                        </button>
                                                                                </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Company Edit</h4>
                            <p class="card-category"></p>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details"
                                role="tab" aria-controls="details" aria-selected="true">Details</a>
                        </li>
                        
                    </ul>
                </div>
                <form class="form-admin" id="form-add-company">
                <input type="hidden" name="companyId" value="{{ $company->id }}">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                aria-labelledby="details-tab">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Company Name</label>
                                        <input type="text" class="form-control" id="company" name="company"
                                            placeholder="company name" value="{{$company->name}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Email</label>
                                        <input type="email" class="form-control" name="email" id="inputEmail4"
                                        value="{{$company->email }}"  placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Website</label>
                                        <input type="text" class="form-control" id="website" name="website"
                                        value="{{$company->website }}" placeholder="Website">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Phone</label>
                                        <select name="countryCode" id="countryCode">
                                            {{-- <option value="">Select</option> --}}
                                            @foreach ($countries as $country )
                                                <option value="{{$country->country_code}}"{{ $country->country_code == $company->country_code ? 'selected' : '' }}>{{$country->country_code}}</option>
                                            @endforeach
                                        </select>
                                        <input type="text" class="form-control" id="phone" name="phone"
                                        value="{{$company->phone }}" placeholder="phone">
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Contact Person</label>
                                        <select name="countryCode" id="countryCode">
                                            {{-- <option value="">Select</option> --}}
                                            @foreach ($countries as $country )
                                            <option value="{{$country->country_code}}"{{ $country->country_code == $company->country_code ? 'selected' : '' }}>{{$country->country_code}}</option>
                                        @endforeach
                                        </select>
                                        <input type="text" class="form-control" id="alternativePhone"
                                        value="{{$company->contact_person }}" name="alternativePhone" placeholder="alternativePhone">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Registration Number</label>
                                        <input type="text" class="form-control" id="registeration_no"
                                        value="{{$company->register_no  }}" name="registeration_no" placeholder="registeration_no">
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">VAT</label>
                                        <input type="text" class="form-control" id="vat" name="vat"
                                        value="{{$company->vat }}" placeholder="Vat No">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">Upload Logo</label>
                                        <input type="file" class="form-control" id="logo" name="logo"
                                            placeholder="Logo">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <label for="inputAddress">Address</label>
                                    <textarea class="form-control" name="address" id="address">{{$company->address }}</textarea>
                                </div>
                                <div class="form-row">
                                    <label for="inputAddress">Other Info</label>
                                    <textarea class="form-control" name="other_info" id="other_info">{{$company->other_info }}</textarea>
                                </div>

                                {{-- <div class="float-right">
                                    <a class="btn btn-primary text-white tabto1 m-2">Next</a>
                                </div><br> --}}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                                onclick="window.location.reload();"> Close </button>
                            <button type="submit" class="btn btn-primary addBtn" id="addPropertyBtn"
                                >Submit</button>
                        </div>
                    </div>
                </form
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>



    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly" async></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
   <script>
        const url = '{{ url('/') }}';
        $('#form-add-company').validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                name: {
                    required: true,
                },
                phone: {
                    required: true,
                },
                email: {
                    required: true
                },

            },
            messages: {
                name: "Company Name is required",
                phone: "Phone is required",
                email: "email is required",   
            },
            errorElement: 'div',
            errorLabelContainer: '.errorTxt',
            showErrors: function(errorMap, errorList) {
                if (!this.numberOfInvalids()) {
                    $('.errorTxt').hide()
                } else {
                    $('.errorTxt').show()
                }
                this.defaultShowErrors();
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-company");
                var formData = new FormData(form);
                console.log(formData);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('editCompany') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "{{route('companyDetails')}}";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

   </script>
@endsection
