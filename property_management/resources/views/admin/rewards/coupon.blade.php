@extends('admin.layout.app') 
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }
    
    .modal { overflow: auto !important; }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height : 20px;
        border-radius: 5px  !important
    }
    #map { height: 450px; width: 100%; }
    .errorTxt{
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding : 10px;
        border-radius :10px;
    }
    .error{
        color : red;
    }
    
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Rewards</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Rewards</h4>
                            <p class="card-category">List of all Rewards</p>
                        </div>
                        <div class="pd-5 mg-r-20">
                            <a href="#" class="nav-link card-active tx-white d-flex align-items-center"  data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Coupon</a> 
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive wrap">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Property</th>
                                <th>Coupon Code</th>
                                <th>Coupon Type</th>
                                <th>Coupon Value</th>
                                <th>Expiry Date</th>
                                <th>Max Count of Users</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = ($coupon->currentpage()-1)* $coupon->perpage() + 1;?>
                        @foreach($coupon as $row)
                            <tr>
                                <td> {{ $i++ }}</td>
                                @if(isset($row->property_rel))
                                <td>{{$row->property_rel->property_name}}</td>
                                @endif
                                <td> {{ $row->coupon_code ?? ''}} </td>
                                @if ($row->coupon_type == 0)
                                    <td> Percentage </td>
                                @elseif ($row->coupon_type == 1)
                                    <td> Amount </td>
                                @endif   
                                @if ($row->coupon_type == 0)
                                    <td> {{ $row->percentage ?? '' }} % </td>
                                @elseif ($row->coupon_type == 1)
                                    <td> {{ $row->amount ?? '' }} </td>
                                @endif    
                                <td> {{ date('d/m/Y', strtotime($row->expiry_date)) ?? '' }} </td>
                                <td> {{ $row->max_count }} </td>
                                <td> 
                                    @if( $row->status == 1 )
                                        <span class="badge rounded-pill bg-primary text-white"> Active </span>
                                    @else
                                        <span class="badge rounded-pill bg-danger text-white"> Inactive </span>
                                    @endif
                                </td>
                                <td>
                                    <a class="fad fa-edit tx-20 edit-button btn btn-primary " href="javascript:0;" data-coupon="{{$row->id}}" title="Edit"></a>
                                    <a class="fad fa-trash tx-20   btn btn-danger " href="javascript:0;" onclick="deleteFunction('{{$row->id}}')"  title="Delete"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    {!! $coupon->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<!-- ----owner add modal---- -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
<div class="modal-dialog modal-lg">
<div class="modal-content">
   <div class="modal-header">
      <h4 class="modal-title text-center tx-primary" id="myModalLabel">Add Coupon</h4>
   </div>
   <form class="form-admin" id="form-add" action="javascript:;">
      <div class="modal-body">
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <input type="hidden" id="id" name="id">
                        <label>Coupons Type<span class="tx-danger">*</span></label>
                        <select class="form-control select2" name="coupon_type" id="coupon_type">
                            <option value="">select</option>
                            <option value="0" >Percentage</option>
                            <option value="1" >Amount</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Coupon  Value <span class="tx-danger">*</span></label>
                        <input class="form-control coupon_value" name="coupon_value" type="text" placeholder="Enter Coupon  Value"/>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Max Count of Users<span class="tx-danger">*</span></label>
                        <input class="form-control" id="max_count" name="max_count" type="text" placeholder="Enter Max count of users"/>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Status<span class="tx-danger">*</span></label>
                        <select class="form-control select2" name="status" id="status">
                           <option value="0">Inactive</option>
                           <option value="1" selected>Active</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                       <label>Property<span class="tx-danger">*</span></label>
                       <select class="form-control select2" name="property" id="property">
                           <option value="">Select Property</option>
                          @foreach ($properties as $property)
                          <option value="{{$property->id}}">{{$property->property_name}}</option>
                              
                          @endforeach
                       </select>
                    </div>
                 </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label>Expiry Date<span class="tx-danger">*</span></label>
                        <input class="form-control" id="expiry_date" name="expiry_date" type="date" placeholder="Enter Expiry Date"/>
                     </div>
                  </div>
               </div>
            </div>
            <!-- end-of-tab-1 -->
         </div>
         </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();" > Close </button>
               <button type="submit" class="btn btn-primary addBtn" id="coupBtn">Submit</button>
            </div>
   </form>
   </div>
   </div>
</div>

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content delete-popup">
         <div class="modal-header">
            <h4 class="modal-title tx-danger reject-modal-title">Delete Coupon</h4>
         </div>
         <form action="javascript:;" id="delete-form">
            <div class="modal-body">
                Are you sure you want to delete this coupon?
               <input type="hidden" name="couponId" id="couponId">
               <!--Action -  1-unblock , 0- block -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
               <button type="submit" class="btn btn-danger reject-class deletetBtn">Delete</button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
    const url = '{{url("/")}}';
    $("#site_title").html(`Property | Coupons`);
    $("#rewards").addClass("active");
    $("#coupons").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        // $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>
    function deleteFunction(id){
        $('#couponId').val(id);
        $('#rejectModal').modal()
    }
</script>
<script>
    $("#form-add").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            coupon_code: {
                required: true,
            },
            coupon_type: {
                required: true,
            },
            coupon_value: {
                required: true,
            },
            expiry_date: {
                required: true,
            },
            max_count: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addCoupon')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
<script>
 $('body').on('click','.edit-button',function(){
     var id = $(this).data('coupon')
        $.ajax({
            url : url+"/coupon/view/"+id,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                console.log(data);
                if(data.status === true){
                    $('.addBtn').html('Update')
                    $('#id').val(data.response.id)
                    $('#coupon_code').val(data.response.coupon_code)
                    $('#coupon_type').val(data.response.coupon_type).trigger("change");
                    if(data.response.percentage){
                        $('.coupon_value').val(data.response.percentage)
                    }else{
                        $('.coupon_value').val(data.response.amount)
                    }
                    $('#max_count').val(data.response.max_count)
                    $('#property').val(data.response.property_id)
                    $('#expiry_date').val(data.response.expiry_date)
                    $('#addModal').modal()

                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
 })
  $('.deletetBtn').click(function(){
        var id = $('#couponId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
                type: "GET",
                url: url+"/delete/coupon/"+id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data){
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
    })
</script>

@endsection
