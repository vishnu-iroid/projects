@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Notifications</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Notifications</h4>
                            <p class="card-category">List of  Notifications</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{-- <a href="{{route('addNotifications')}}">list</a> --}}
                    <table id="datatable1" class="table">
                        <thead>
                            <tr>
                                <th class="">#</th>
                                <th class="">HEADING</th>
                                <th class="">DESCRIPTIONS</th>
                                <th class="">USER</th>
                                {{-- <th class="">STATUS</th> --}}
                                {{-- <th>Action</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifications as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->notification_heading }}</td>
                                <td>{{ $row->notification_text}}</td>
                                <td>{{ $row->name}}</td>
                                {{-- <td>{{ $row->status}}</td> --}}
                                {{-- <td><a class="fa fa-eye" href="{{route('viewNotificationDetails',['id'=>$row->id])}}"></a></td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $notifications->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    </div>


  

    @endsection @section('scripts')
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script>
        const url = '{{url("/")}}';
    </script>
    <script>
        $("#site_title").html(`Property | User Notification`);
        $("#notifications").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                // responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            $("#datatable2").DataTable({
                // responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            // $(".dataTables_length select").select2({
            //     minimumResultsForSearch: Infinity,
            // });
        });
    </script>
    @endsection
