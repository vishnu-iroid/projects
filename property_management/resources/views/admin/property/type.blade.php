@extends('admin.layout.app')
 <style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4, h4 {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00) !important ;
        height : 20px !important ;
        border-radius: 5px  !important ;
    }
    .card-warning-2 {
        background: linear-gradient(60deg, #ffa726, #fb8c00) !important ;
        height : 20px !important ;
        border-radius: 5px  !important ;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Master</span>
        <span class="breadcrumb-item active">Type</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
<div class="row">
    <div class="col-md-3">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-12">
                        <h2 class="card-title" style="font-size: 1.125rem;font-weight: 300 !important;">Add Type</h2>
                        <!-- <p class="card-category"></p> -->
                    </div>
                </div>
        </div>
        <div class="card-body">
            <form class="form-clients" id="form-add" action="javascript:;">
                <div class="modal-body">
                    <div class="form-group">
                        <label> Category<span class="tx-danger">*</span></label>
                        <input class="form-control" id="id" name="id" type="hidden"/>
                        <input class="form-control" id="type" name="type" type="text" placeholder="Enter property type"/>
                    </div>
                    <div class="form-group">
                        <label> Type<span class="tx-danger">*</span></label>
                        <Select class="form-control select2" name="category" id="category">
                            <option value="0">Residential</option>
                            <option value="1">Commercial</option>
                        </Select>
                    </div>
                    <div class="form-group">
                        <label>Amenities<span class="tx-danger">*</span></label>
                        @if(count($amenity_categories ) == 0)
                            <div class="row d-flex justify-content-center tx-danger tx-bold">
                                <label>No amenities found<span class="tx-danger"></span></label>
                            </div>
                        @endif
                        @foreach($amenity_categories as $key=>$row)
                            <div id="accordion-{{$row->id}}" class="accordion" role="tablist" aria-multiselectable="true" val="{{$row->id}}" @if($key ==0 ) style="margin-top:-25px" @endif>
                                <div class="card">
                                    <div class="card-header card-warning-2" role="tab" id="headingOne-{{$row->id}}">
                                        <h6 class="pd-2 px-2">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$row->id}}"
                                            aria-expanded="true" aria-controls="collapseOne-{{$row->id}}" class="tx-white transition">
                                            {{$row->name}}
                                            </a>
                                        </h6>
                                    </div><!-- card-header -->

                                    <div id="collapseOne-{{$row->id}}" class="collapse" role="tabpanel" aria-labelledby="headingOne-{{$row->id}}">
                                    <div class="card-block pd-20">
                                    <div class="row mg-l-1">
                                            @foreach($amenities as $key=>$row2)
                                                @if($row2->amenity_category_id == $row->id)
                                                    <label class="ckbox ml-2">
                                                        <input type="checkbox" value="{{$row2->id}}" id='cb-{{$row2->id}}' name="amenities[]">
                                                        <span>{{ $row2->name }}</span>
                                                    </label>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    </div>
                                </div><!-- card -->
                                <!-- ADD MORE CARD HERE -->
                            </div><!-- accordion -->
                        @endforeach
                    </div>
                    <div class="form-group">
                    <label>Details<span class="tx-danger">*</span></label>
                        @if(count($details) == 0)
                            <div class="row d-flex justify-content-center tx-danger tx-bold">
                                <label>No details found<span class="tx-danger"></span></label>
                            </div>
                        @endif
                        <div class="row d-flex justify-content-center">
                            @foreach($details as $row5)
                                <label class="ckbox ml-2">
                                    <input type="checkbox" value="{{$row5->id}}" id='det-{{$row5->id}}' name="details[]">
                                    <span>{{ $row5->name }}</span>
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary addBtn">Submit</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <div class="col-md-9">
    <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title">Type</h4>
                        <p class="card-category">List of all type</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Category</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($type as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->type }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-type="{{$row->id}}" title="Edit"></a>
                                <!-- <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deleteFunction('{{$row->id}}')"></a> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                        {!! $type->links() !!}
                </div>
            </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
</div>
</div>
<!-- br-pagebody -->

<!-- Reject Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content delete-popup">
         <div class="modal-header">
            <h4 class="modal-title tx-danger">Delete Property Type</h4>
         </div>
         <form action="javascript:;" id="form-delete">
            <div class="modal-body">
                Are you sure you want to delete this property type?
               <input type="hidden" name="departmentId" id="departmentId">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary closeBtn" onclick="window.location.reload();">Cancel</button>
               <button type="submit" class="btn btn-danger reject-class deletetBtn">Delete</button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
        $('.accordion').click(function(){
            $id = $(this).attr('val')
            $('#collapseOne-'+$id).toggleClass('show')
        })
</script>
<script>
    $("#site_title").html(`Property | Property Type`);
    $("#property_nav").addClass("active");
    $("#property_type_nav").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>
    function deleteFunction(id){
        $('#departmentId').val(id);
        $('#deleteModal').modal()
    }
</script>
<script>

    $('.deletetBtn').click(function(){
        var id = $('#departmentId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
                type: "GET",
                url: url+"/department/delete/"+id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".deletetBtn").html('Deleting..');
                },
                success: function (data) {
                    $(".deletetBtn").text('Delete');
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
    })

    $('.edit-button').click(function(){
        var typeId = $(this).data('type')
        $.ajax({
            url : url+"/property-type/view/"+typeId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    // $('#myModalLabel').html('Edit Client')
                    $('#id').val(data.response.type.id)
                    $('#type').val(data.response.type.type)
                    $('#category').val(data.response.type.category).trigger('change')
                    if(data.response.amenities.length > 0){
                        data.response.amenities.forEach((value) => {
                           $('#cb-'+value.amenity_id).prop('checked',true)
                        })
                    }
                    if(data.response.details.length > 0){
                        data.response.details.forEach((value) => {
                           $('#det-'+value.detail_id).prop('checked',true)
                        })
                    }
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $("#form-add").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            type: {
                required: true,
            },
            category: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addType')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
