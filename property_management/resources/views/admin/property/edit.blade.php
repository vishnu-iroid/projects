@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .vgreen {
        background: #65e715 !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .close {
        cursor: pointer;
        position: absolute;
        top: 15%;
        right: 12%;
        padding: 4px 8px 4px 8px;
        transform: translate(0%, -50%);
        color: white;
        opacity: 1;
    }

    .close:hover {
        background: #bbb;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="{{ route('userBookings') }}">Bookings</a>
            <span class="breadcrumb-item active">Details</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                                                                <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                                                    <i class="fas fa-plus-square"></i>&nbsp Add Client
                                                                                </button>
                                                                        </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property Edit</h4>
                            <p class="card-category"></p>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details"
                                role="tab" aria-controls="details" aria-selected="true">Details</a>
                        </li>
                        {{-- @if ($property->is_builder == 0) --}}
                        <li class="nav-item hide-element">
                            <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab"
                                aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                        </li>

                        <li class="nav-item hide-element">
                            <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities"
                                role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contract" role="tab"
                                aria-controls="contract" aria-selected="false">Contract Details</a>
                        </li>
                        @if ($property->is_builder == 0)
                            <li class="nav-item hide-element">
                                <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents"
                                    role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a id="location-prop-tab" class="nav-link propertynav" data-toggle="tab" href="#location-prop"
                                role="tab" aria-controls="location-prop" aria-selected="false">Location</a>
                        </li>
                    </ul>
                </div>
                <form class="form-admin" id="form-add-property"
                    action="{{ route('editOwnerProperty', [$property->id]) }}">
                    @csrf

                    <input type="hidden" name="propertyId" value="{{ $property->id }}">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                aria-labelledby="details-tab">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property Type<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="category" id="category">
                                                <option value="">-select-</option>
                                                <option value="0" {{ $property->category == '0' ? 'selected' : '' }}>
                                                    Residential</option>
                                                <option value="1" {{ $property->category == '1' ? 'selected' : '' }}>
                                                    commercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Category<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 category-select" name="type">
                                                <option value="">-select-</option>
                                                <option value=" {{ $property->type_id }}" selected>
                                                    {{ $property->type_details->type }}
                                                    {{-- {{ $property->type_details->type ? 'selected' : '' }} --}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Owners<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 owners-select" name="ownerslist"
                                                id="ownerslist">
                                                <option value="">select </option>
                                                @foreach ($owners as $owner)
                                                    <option value="{{ $owner->id }} " @if ($property_owner->id == $owner->id) selected @endif>
                                                        {{ $owner->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="hidden" id="property_owner_id" name="property_owner_id">
                                        <div class="form-group">
                                            <label>Property For<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_to" id="property_to">
                                                <option value="">-select-</option>
                                                <option value="0" {{ $property->property_to == '0' ? 'selected' : '' }}>
                                                    Rent</option>
                                                <option value="1" {{ $property->property_to == '1' ? 'selected' : '' }}>
                                                    Buy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property Name<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="property_name" name="property_name"
                                                value="{{ $property->property_name }}" type="text"
                                                placeholder="Enter Property Name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property Status<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_status"
                                                id="property_status">
                                                <option value="">status</option>
                                                <option value="0" {{ $property->occupied == '0' ? 'selected' : '' }}>
                                                    Vacant</option>
                                                <option value="1" {{ $property->occupied == '1' ? 'selected' : '' }}>
                                                    Occupied</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Country<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_country"
                                                id="property_country">
                                                {{-- <option value=""></option> --}}
                                                @foreach ($countries as $country)
                                                    <option value="{{ $country->id }} " @if ($property_country->country == $country->id) selected @endif>
                                                        {{ $country->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>State<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 state-select" name="property_state"
                                                id="property_state">
                                                @foreach ($states as $state)
                                                    <option value="{{ $state->id }} " @if ($property_state->id == $state->id) selected @endif>
                                                        {{ $state->name }}
                                                    </option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 city-select" name="property_city"
                                                id="property_city">
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }} " @if ($property_city->id == $city->id) selected @endif>
                                                        {{ $city->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Zipcode<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 zipcode-select" name="property_zipcode"
                                                id="property_zipcode">
                                                @foreach ($zipcodes as $zip)
                                                    <option value="{{ $zip->id }} " @if ($property_zipcode->id == $zip->id) selected @endif>
                                                        {{ $zip->pincode }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address 1 <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address1" name="address1" type="text"
                                                placeholder="Enter address">{{ $property->street_address_1 }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address 2 <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address2" name="address2" type="text"
                                                placeholder="Enter address">{{ $property->street_address_2 }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="description" name="description" type="text"
                                                placeholder="Description">{{ $property->description }}</textarea>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Individual Property<span class="tx-danger">*</span></label>
                                            <input type="checkbox" name="individual_building" id="is_builder" value="1"
                                                {{ $property->is_builder == 0 ? ' checked' : '' }}>
                                        </div>
                                    </div>
                                    @if ($property->is_builder == '1')
                                        <div class="col-md-6 is-building">
                                            <div class="form-group">
                                                <label>Images <span class="tx-danger">*</span></label><br>

                                                <input id="building_image" name="building_image[]" type="file"
                                                    class="form-control file" multiple data-show-upload="true"
                                                    data-show-caption="true">
                                                @if (count($property_image))
                                                    @foreach ($property_image as $image)
                                                        @if ($image->type == '3')
                                                        <div class=" row m-2">
                                                            <div class="col-lg-4 col-md-4 col-6 ">
                                                                <span title="remove this image"
                                                                    class="vgreen close close-thick"
                                                                    onclick="deleteImage(this,'{{ $image->document }}','{{ $image->id }}')"
                                                                    id="galleryimage_{{ $loop->index }}">x</span>
                                                                <a target="_blank" href="{{ $image->document }}"
                                                                    data-toggle="lightbox" data-gallery="example-gallery" >
                                                                    <img class="img-fluid "
                                                                        src="{{ $image->document }}" alt=""></a>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row is-appartment">
                                    @if ($property->is_builder == '1')
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>No. of Units<span class="tx-danger">*</span></label>
                                                <input class="form-control" id="unit_num" name="unit_num" type="number"
                                                    value="{{ $property->unit_num }}" placeholder="Enter Unit">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. of floors<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="floor_num" name="floor_num" type="number"
                                                value="{{ $property->floor_num }}" placeholder="Enter Floor">
                                        </div>
                                    </div>
                                </div>
                                <div class="amenities-row" style="display: none">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                @if ($property->is_builder == 1)
                                                    <label>Furnished <span class="tx-danger">*</span></label>
                                                    <select class="form-control select2" name="furnished" id="furnished">
                                                        <option value="">-select-</option>
                                                        <option value="0"
                                                            {{ $property->furnished == '0' ? 'selected' : '' }}>
                                                            Not Furnished</option>
                                                        <option value="1"
                                                            {{ $property->furnished == '1' ? 'selected' : '' }}>
                                                            Semi Furnished</option>
                                                        <option value="2"
                                                            {{ $property->furnished == '2' ? 'selected' : '' }}>
                                                            Fully Furnished</option>

                                                    </select>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-warning tx-white mb-3"> &nbspDetails</div>
                                    <div class="row" id="details-section">
                                        {{-- @if ($details)
                                            @foreach ($details as $det)
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>{{ $det->name }}<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control"
                                                            name="detailsdata[{{ $det->detail_id }}]"
                                                            id="det-{{ $det->detail_id }}"
                                                            placeholder="{{ $det->placeholder }}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif --}}


                                        @if ($details_prop)
                                            @foreach ($details_prop as $detail)
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>{{ str_replace('_', ' ', ucfirst($detail->placeholder)) }}<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control"
                                                            name="details_prop_data[{{ $detail->id }}]"
                                                            id="det-{{ $detail->id }}" value="{{ $detail->value }}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <!-- end of tab1 -->
                            <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                                <div class="rentdetails">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Frequency<span class="tx-danger">*</span></label>
                                                <select class="form-control select2 rentfrequency" name="frequency"
                                                    id="frequency">
                                                    <option value="">select</option>
                                                    @foreach ($frequency as $freq)
                                                        @if ($property_frequency)
                                                            <option value="{{ $freq->id }} " @if ($property_frequency->id == $freq->id) selected  @endif>
                                                                {{ $freq->type }}
                                                            </option>
                                                        @else
                                                            <option value="{{ $freq->id }}">{{ $freq->type }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Rent<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="min_rent" name="min_rent"
                                                    value="{{ $property->rent }}" placeholder="Enter rent">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Security Deposit<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="security_deposit"
                                                    value="{{ $property->security_deposit }}" id="security_deposit"
                                                    placeholder="Enter Security deposit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if ($property->property_to == '1')
                                    <div class="sellingdetails">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Min Selling price<span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control" id="min_sellingprice"
                                                        value="{{ $property->selling_price }}" name="min_sellingprice"
                                                        placeholder="Enter minimum selling price">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Max Selling price<span class="tx-danger">*</span></label>
                                                    <input type="text" class="form-control" id="max_sellingprice"
                                                        value="{{ $property->mrp }}" name="max_sellingprice"
                                                        placeholder="Enter maximum selling price">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="token-amount">
                                    <div class="row">
                                        <div class="col-md-6 token-hold">
                                            <div class="form-group">
                                                <label>Token Amount<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="token_amount"
                                                    value="{{ $property->token_amount }}" id="token_amount"
                                                    placeholder="Enter Token amount">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="amenities" role="tabpanel" aria-labelledby="amenities-tab">
                                <div id="amenties_available">
                                    @if ($amenities_listing)
                                        @foreach ($property_amenities as $row)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="bg-warning tx-white mb-3"> &nbsp
                                                            {{ $row->amenity_category->name }}
                                                        </div>
                                                        <div class="row mg-l-1">
                                                            {{-- @foreach ($row->amenity_category as $row2) --}}
                                                            <label class="ckbox ml-2">
                                                                <input type="checkbox" value="{{ $row->amenity->id }}"
                                                                    name="amenities[]" checked>
                                                                <span>{{ $row->amenity->name }}</span>
                                                                {{-- <input type="hidden" name="amenityId" value="{{$row->id}}"> --}}
                                                            </label>
                                                            {{-- @endforeach --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <!-- end of tab3 -->
                            <!-- start tab 4 -->
                            <div class="tab-pane fade" id="contract" role="tabpanel" aria-labelledby="contract-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract No</label>
                                            <input type="text" class="form-control" name="contract_no" id="contract_no"
                                                value="{{ $property->contract_no }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract File</label>
                                            <input type="file" class="form-control" name="contract_file"
                                                id="contract_file">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract Start Date<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker"
                                                value="{{ $property->contract_start_date }}" name="contract_start_date"
                                                id="contract_start_date" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract End Date<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker" name="contract_end_date"
                                                value="{{ $property->contract_end_date }}" id="contract_end_date"
                                                autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Commission in terms of :-<span
                                                    class="tx-danger">*</span></label><br>
                                            <label>Amount</label>
                                            <input type="radio" checked name="commision_as" id="commission_as"
                                                value="amount">
                                            <label>Percentage</label>
                                            <input type="radio" name="commision_as" id="commission_as" value="percentage">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Commission<span class="tx-danger"
                                                    style="font-size:12px;">*</span></label>
                                            <input type="number" class="form-control" name="owner_amount"
                                                value="{{ $property->commission }}" id="owner_amount"
                                                placeholder="Enter Commission">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property Management fee:-<span
                                                    class="tx-danger">*</span></label><br>
                                            <label>Amount</label>
                                            <input type="radio" class="property_management_fee_type"
                                                name="property_management_fee_type" id="property_management_fee_type"
                                                value="amount" checked>
                                            <label>Percentage</label>
                                            <input type="radio" class="property_management_fee_type"
                                                name="property_management_fee_type" id="property_management_fee_type"
                                                value="percentage">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Management Fee<span class="tx-danger"
                                                    style="font-size:12px;">*</span></label>
                                            <input type="number" class="form-control" name="management_fee"
                                                value="{{ $property->management_fee }}" id="management_fee"
                                                placeholder="Enter Management Fee">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Ads Number:-<span class="tx-danger">*</span></label><br>
                                            <input type="text" class="form-control" name="adsnumber" id="adsnumber"
                                                value="{{ $property->ads_number }}" placeholder="Enter Ads Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Guard Number<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="guard_number"
                                                value="{{ $property->guards_number }}" id="guard_number"
                                                placeholder="Enter Guard Number">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of tab4 -->
                            <!-- start tab 5 -->
                            <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Images <span class="tx-danger">*</span></label><br>
                                            <input id="images" name="images[]" type="file" class="form-control file"
                                                value="" multiple data-show-upload="true" data-show-caption="true">
                                            @if (count($property_image))
                                                @foreach ($property_image as $image)
                                                    @if ($image->type == '0')
                                                        <div class=" row m-2">
                                                            <div class="col-lg-4 col-md-4 col-6 ">
                                                                <span title="remove this image"
                                                                    class="vgreen close close-thick"
                                                                    onclick="deleteImage(this,'{{ $image->document }}','{{ $image->id }}')"
                                                                    id="galleryimage_{{ $loop->index }}">x</span>
                                                                <a target="_blank" href="{{ $image->document }}"
                                                                    data-toggle="lightbox" data-gallery="example-gallery" >
                                                                    <img class="img-fluid "
                                                                        src="{{ $image->document }}" alt=""></a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                            <input id="floor_plans" name="floor_plans[]" type="file" value=""
                                                class="form-control file" multiple data-show-upload="true"
                                                data-show-caption="true">
                                            @if (count($property_image))
                                                @foreach ($property_image as $floor)
                                                    @if ($floor->type == '2')
                                                    <div class=" row m-2">
                                                        <div class="col-lg-4 col-md-4 col-6 ">
                                                            <span title="remove this image"
                                                                class="vgreen close close-thick"
                                                                onclick="deleteImage(this,'{{ $floor->document }}','{{ $floor->id }}')"
                                                                id="galleryimage_{{ $loop->index }}">x</span>
                                                            <a target="_blank" href="{{ $floor->document }}"
                                                                data-toggle="lightbox" data-gallery="example-gallery" >
                                                                <img class="img-fluid "
                                                                    src="{{ $floor->document }}" alt=""></a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Videos <span class="tx-danger"></span></label><br>
                                            <input id="videos" name="videos[]" type="file" class="form-control file"
                                                multiple data-show-upload="true" data-show-caption="true">
                                            @foreach ($property_image as $image)
                                                @if ($image->type == '1')
                                                <div class=" row m-2">
                                                    <div class="col-lg-4 col-md-4 col-6 ">
                                                        <span title="remove this image"
                                                            class="vgreen close close-thick"
                                                            onclick="deleteImage(this,'{{ $image->document }}','{{ $image->id }}')"
                                                            id="galleryimage_{{ $loop->index }}">x</span>
                                                        <a target="_blank" href="{{ $image->document }}"
                                                            data-toggle="lightbox" data-gallery="example-gallery" >
                                                            <img class="img-fluid "
                                                                src="{{ $image->document }}" alt=""></a>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="location-prop" role="tabpanel"
                                aria-labelledby="location-prop-tab">
                                <div id="map-2"></div>
                                <div>
                                    <input type="hidden" id="lat-prop" name="lat_prop">
                                    <input type="hidden" id="lng-prop" name="lng_prop">
                                </div>
                            </div>
                        </div>
                        <div class="errorTxt shadow"></div>
                        <div class="modal-footer">
                            {{-- <a href="{{route('addPropertyUnits')}}"  class="btn btn-primary addBtn" id="addPropertyBtn"  disabled>Submit</a> --}}
                            <button type="submit" class="btn btn-primary addBtn" id="addPropertyBtn">Submit</button>
                        </div>
                </form>
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>



    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    
 <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>

    <script>
        const url = '{{ url('/') }}';
        // var deletegalleryimageurl = "{{ route('deletegalleryimage', $property->id) }}";
        $('.sellingdetails').hide()
        $('.hide-element').hide();
        $('.amenities-row').hide();
    </script>

    {{-- <script>
        $(document).ready(function(){
            $('.hide-element').css('display', 'none');
        });
    </script> --}}

    <script>
        $('#property_country').change(function() {
            var countryId = $(this).val()
            //  alert(countryId);
            $.ajax({
                url: url + "/get-states/" + countryId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.state-select').html('')
                    var html = '<option value="">--Select--</option>';
                    if (data.status === true) {
                        if (data.response.length > 0) {

                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.state-select').append(html);

                        } else {
                            $('.state-select').html('')
                        }
                    } else {
                        $('.state-select').html('')
                    }
                }
            });
            // return false
        });

        $('.state-select').change(function() {
            var stateId = $(this).val()
            $.ajax({
                url: url + "/get-cities/" + stateId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.city-select').html('')
                    var html = '<option value="">--Select--</option>';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.city-select').append(html)
                        } else {
                            $('.city-select').html('')
                        }
                    } else {
                        $('.city-select').html('')
                    }
                }
            });
            // return false
        });

        $('.city-select').change(function() {
            var cityId = $(this).val();
            $.ajax({
                url: url + "/get-pincodes/" + cityId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.zipcode-select').html('')
                    var html = '<option value="">--select--</option>';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.pincode +
                                    `</option>`
                            })
                            $('.zipcode-select').append(html)
                        } else {
                            $('.zipcode-select').html('')
                        }
                    } else {
                        $('.zipcode-select').html('')
                    }
                }
            });

            return false
        });
        $('#category').change(function() {
            var categoryId = $(this).val();
            categoryTypeFilter(categoryId);
        });

        function categoryTypeFilter(categoryId) {
            $.ajax({
                url: url + "/get-type/" + categoryId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.category-select').html('')
                    var html = '<option value=""></option>';
                    if (data.status === true) {

                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.type + `</option>`
                            })
                            $('.category-select').append(html)
                        } else {
                            $('.v-select').html('')
                        }
                    } else {
                        $('.category-select').html('')
                    }
                }
            })
            return false
        }

        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            container: '#contractUploadModal modal-body'
        });
        $('#property_to').change(function() {
            if ($('#property_to').val() == 1) {
                // buy -> 1 / rent->0
                $('.rentdetails').hide();
                $('.sellingdetails').show()

            } else {
                $('.sellingdetails').hide()
                $('.rentdetails').show();
            }
        });
        // $(document).on("click", "#is_builder", function() {

        //     var check_val = $(this).prop("checked");
        //     if (check_val) {
        //         $('.hide-element').show();
        //         $('.amenities-row').show();
        //         $('.is-building').hide();
        //     } else {
        //         $('.hide-element').hide();
        //         $('.amenities-row').hide();
        //         $('.is-building').show();
        //     }
        // });

        if ($('#is_builder').is(':checked')) {
            $('.hide-element').show();
            $('.amenities-row').show();
            $('.is-building').hide();
            $('.is-appartment').hide();
        } else {
            $('.hide-element').hide();
            $('.amenities-row').hide();
            $('.is-building').show();
            $('.is-appartment').show();
        }
    </script>
    <script>
        function initMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            if ($('#lat').val() != "" && $('#lng').val() != "") {
                const uluru = {
                    lat: $('#lat').val(),
                    lng: $('#lng').val()
                };
            }


            const map = new google.maps.Map(document.getElementById("map-2"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#lat').val() == "" && $('#lng').val() == "") {
                        $('#lat').val(parseFloat(position.coords.latitude))
                        $('#lng').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#lat').val()
                    var lng = $('#lng').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat').val(marker.position.lat())
                $('#lng').val(marker.position.lng())
            })
        }

        function initPropertyMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            console.log('lat', $('#lat-prop').val())
            if ($('#lat-prop').val() != "" && $('#lng-prop').val() != "") {
                const uluru = {
                    lat: $('#lat-prop').val(),
                    lng: $('#lng-prop').val()
                };
            }
            const map = new google.maps.Map(document.getElementById("map-2"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#lat-prop').val() == "" && $('#lng-prop').val() == "") {
                        $('#lat-prop').val(parseFloat(position.coords.latitude))
                        $('#lng-prop').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#lat-prop').val()
                    var lng = $('#lng-prop').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat-prop').val(marker.position.lat())
                $('#lng-prop').val(marker.position.lng())
            })
        }

        $('#form-add-property').validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                property_name: {
                    required: true,
                },
                property_to: {
                    required: true,
                },
                category: {
                    required: true
                },
                property_country: {
                    required: true,
                },
                property_state: {
                    required: true,
                },
                property_city: {
                    required: true,
                },
                property_zipcode: {
                    required: true,
                },
                address1: {
                    required: true,
                },
                // furnished : {
                //     required : true,
                // },
                is_builder: {
                    required: function() {
                        if ($('#is_builder').prop(":checked")) {
                            return true;
                            // min_sellingprice : {
                            //     required : function (){
                            //         if($('#property_to').val() == 1){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                            // max_sellingprice : {
                            //     required : function (){
                            //         if($('#property_to').val() == 1){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                            // 'amenities[]' : {
                            //     required :true
                            // },
                            // 'images[]' : {
                            //     required : true,
                            // },
                            // 'detailsdata[]' : {
                            //     required : true,
                            // }

                            // min_rent : {
                            //     required : function (){
                            //         if($('#property_to').val() == 0){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                            // token_amount : {
                            //     required : true,
                            // },

                            // security_deposit : {
                            //     required : function (){
                            //         if($('#property_to').val() == 0){
                            //             return true
                            //         }
                            //         return false
                            //     },
                            // },
                        }
                        return false
                    },
                },

                //  contract_no: {
                //      required: true,
                //  },

                //  contract_file: {
                //      required: true,
                //  },

                //  contract_start_date: {
                //      required: true,
                //  },

                //  contract_end_date: {
                //      required: true,
                //  },

            },
            messages: {
                property_name: "Property Name is required",
                property_to: "Property to is required",
                category: "Category is required",
                property_country: "Country is required",
                property_state: "State is required",
                property_city: "City is required",
                property_zipcode: "Zipcode is required",
                address1: "Address is required",
                frequency: "Frequency is required",
                // min_rent : "Min rent is required",
                // max_rent : "Max rent is required",
                // furnished : "Furnished is required",
                // security_deposit : "Security deposit is required",
                // token_amount : "Token Amount is required",
                //  contract_no: 'Contract number is required',
                //  contract_file: 'Contract file is required',
                //  contract_start_date: 'Contract start date is required',
                //  contract_end_date: 'Contract end date is required',
                // min_sellingprice : "Min Selling Price is required",
                // max_sellingprice : "Max Selling Price is required",
                // 'amenities[]' : 'Amenities are required',
                // 'images[]' : 'Images are required',
                // 'detailsdata[]': 'Details are required',
            },
            errorElement: 'div',
            errorLabelContainer: '.errorTxt',
            showErrors: function(errorMap, errorList) {
                if (!this.numberOfInvalids()) {
                    $('.errorTxt').hide()
                } else {
                    $('.errorTxt').show()
                }
                this.defaultShowErrors();
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-property");
                var formData = new FormData(form);
                console.log(formData);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ url('/property/editOwner-property') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        console.log(data);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            if (data.response_data) {
                                if (data.response_data.property_id) {
                                    var responseId = data.response_data.property_id;
                                    console.log(responseId);
                                    if ($('#is_builder').is(':checked')) {

                                        setTimeout(function() {
                                            window.location.href =
                                                "/property/management/list";
                                        }, 1000);
                                    } else {
                                        // alert("fdiu");
                                        setTimeout(function() {
                                            window.location.href = "/edit-property-unit/" +
                                                responseId;
                                        }, 1000);
                                    }
                                }
                            } else {
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            }
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

        // $("document").ready(function() {
        //     $("#floor_plans").change(function() {
        //         // alert('changed!');

        //     });
        // });


        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });

        function deleteImage(element, path,pid) {       
             var deleteimageblock = $(element);
          
            if (path != "") {
                var form_data = new FormData();
                form_data.append("deletegalleryimagename", path)
                // form_data.append("_token", $('meta[name=_token]').attr('content'));
                $.ajax({
                    url: "/deleteimage/" + pid,
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.success) {
                            deleteimageblock.closest('div').remove();
                            // getuploadedanduploadablegalleryimages();
                        }

                    },
                    error: function(ts) {
                        alert(ts.responseText);
                    }
                });
            }


        }
    </script>

@endsection
