@foreach ($details_new as $detail)
<div class="col-md-4">
    <div class="form-group">
        <label>{{ str_replace('_',' ',ucfirst($detail->placeholder)) }}<span
                class="tx-danger">*</span></label>
        <input type="text" class="form-control"
            name="details_prop_data[{{ $detail->id }}]"
            id="det-{{ $detail->id }}"
            value="{{ $detail->value }}" >
    </div>
</div>
@endforeach
