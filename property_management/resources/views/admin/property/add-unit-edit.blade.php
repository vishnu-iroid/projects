@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .ui-datepicker {
        position: fixed;
        top: 270.797px;
        left: 172.5px;
        z-index: 5001 !important;
        display: block;
    }

    #ui-datepicker-div {
        z-index: 5001 !important;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height: 20px;
        border-radius: 5px !important
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #e94e4e;
        padding: 10px;
        border-radius: 10px;
    }

    .vgreen {
        background: #65e715 !important;
    }

    .close {
        cursor: pointer;
        position: absolute;
        top: 24%;
        right: -41%;
        padding: 4px 8px 4px 8px;
        transform: translate(0%, -50%);
        color: white;
        opacity: 1;
    }

    .close:hover {
        background: #bbb;
    }

    .lightbox-gallery {
        background-image: linear-gradient(#4A148C, #E53935);
        background-repeat: no-repeat;
        color: #000;
        overflow-x: hidden
    }

    .lightbox-gallery p {
        color: #fff
    }

    .lightbox-gallery h2 {
        font-weight: bold;
        margin-bottom: 40px;
        padding-top: 40px;
        color: #fff
    }

    @media (max-width:767px) {
        .lightbox-gallery h2 {
            margin-bottom: 25px;
            padding-top: 25px;
            font-size: 24px
        }
    }

    .lightbox-gallery .intro {
        font-size: 16px;
        max-width: 500px;
        margin: 0 auto 40px
    }

    .lightbox-gallery .intro p {
        margin-bottom: 0
    }

    .lightbox-gallery .photos {
        padding-bottom: 20px
    }

    .lightbox-gallery .item {
        padding-bottom: 30px
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="">Property Management</a>
            <span class="breadcrumb-item active">Unit View</span>
        </nav>
    </div>

    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                            <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                <i class="fas fa-plus-square"></i>&nbsp Add Client
                                            </button>
                                    </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            @if ($property_id)

                @if ($unit_count > 0)
                    {{-- {{dd()}} --}}
                    <form action="{{ route('updatePropertyUnits') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" id="unitnum" name="unitnum" value="{{ $unit_count }}">
                        <div class="modal-body">
                            @foreach ($property_unit1 as $propkey => $property)
                                @for ($i = 1; $i <= $unit_count; $i++)
                                    <input type="hidden" id="property_id{{ $propkey + 1 }}"
                                        name="property_id[{{ $propkey + 1 }}]" value="{{ $property->id }}">
                                    <input type="hidden" id="property_to{{ $propkey + 1 }}"
                                        name="property_to[{{ $propkey + 1 }}]" value="{{ $property->property_to }}">
                                    <div class="tab-content" id="myTabContent">
                                        <h4 class="text-dark">Unit {{ $propkey + 1 }}</h4>
                                        @if ($i == 0)
                                            <a href="" class="btn btn-info duplicate-above-unit">Duplicate Above</a>
                                        @endif



                                        {{-- @foreach ($property_unit1 as $property) --}}
                                        <div class="tab-pane fade show active" id="details" role="tabpanel"
                                            aria-labelledby="details-tab">
                                            <div class="row">
                                                <div class="col-md-6">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Unit Name<span class="tx-danger">*</span></label>
                                                        <input class="form-control"
                                                            id="property_name{{ $propkey + 1 }}"
                                                            name="property_name[{{ $propkey + 1 }}]" type="text"
                                                            value="{{ $property->property_name }}"
                                                            placeholder="Enter Unit Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">

                                                        {{-- @if ($property->is_builder == '1') --}}
                                                        <label>Furnished <span class="tx-danger">*</span></label>
                                                        <select class="form-control select2"
                                                            name="furnished[{{ $propkey + 1 }}]"
                                                            id="furnished{{ $propkey + 1 }}">
                                                            <option value="">-select-</option>
                                                            <option value="0"
                                                                {{ $property->furnished == '0' ? 'selected' : '' }}>
                                                                Not Furnished</option>
                                                            <option value="1"
                                                                {{ $property->furnished == '1' ? 'selected' : '' }}>
                                                                Semi Furnished</option>
                                                            <option value="2"
                                                                {{ $property->furnished == '2' ? 'selected' : '' }}>
                                                                Fully Furnished</option>

                                                        </select>
                                                        {{-- @endif --}}
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Vacant\Occupied <span class="tx-danger">*</span></label>
                                                        <select class="form-control select2"
                                                            name="vacant[{{ $propkey + 1 }}]"
                                                            id="vacant{{ $propkey + 1 }}">
                                                            <option value="0"
                                                                {{ $property->occupied == '0' ? 'selected' : '' }}>Vacant
                                                            </option>
                                                            <option value="1"
                                                                {{ $property->occupied == '1' ? 'selected' : '' }}>
                                                                Occupied</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-dark bg-info tx-white mb-3"> &nbspUnit Descriptions</div>
                                            <div class="row">

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Description<span class="tx-danger">*</span></label>
                                                        {{-- <input type="text-area" class="form-control" name="description[]" id="description{{$i}}" placeholder="Description"> --}}
                                                        <textarea class="form-control"
                                                            name="description[{{ $propkey + 1 }}]"
                                                            id="description{{ $propkey + 1 }}">{{ $property->description }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-dark bg-info tx-white mb-3"> &nbspUnit Details</div>
                                            <div class="row" id="details-section">
                                                @if ($details)
                                                    @foreach ($details_prop[$propkey] as $key => $detail)
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>{{ str_replace('_', ' ', ucfirst($detail->placeholder)) }}<span
                                                                        class="tx-danger">*</span></label>
                                                                <input type="text" class="form-control"
                                                                    name="details_prop_data[{{ $propkey + 1 }}][{{ $detail->id }}]"
                                                                    id="det-{{ $detail->id }}"
                                                                    value="{{ $detail->value }}">
                                                            </div>
                                                        </div>
                                                    @endforeach
                                            </div>
                                @endif

                                @if (isset($amenities_listing))
                                    <div class="text-dark bg-info tx-white mb-3"> &nbspAmenities</div>
                                    <div class="row">
                                        @foreach ($amenities_listing[$propkey] as $row)
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {{-- <div class="card-warning tx-white mb-3"> &nbsp {{$row->name}}  </div> --}}
                                                    <div class="row mg-l-1">
                                                        {{-- @foreach ($row->amenities as $row2) --}}
                                                        <label class="ckbox ml-2">
                                                            <input type="checkbox" value="{{ $row->id }}"
                                                                name="amenities[{{ $propkey + 1 }}][]" checked>
                                                            <span>{{ $row->name }}</span>
                                                        </label>
                                                        {{-- @endforeach --}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                        </div>
                @endif
                @if ($property->property_to == 0)
                    <div class="text-dark bg-info tx-white mb-3"> &nbspRental Details</div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Lease Type<span class="tx-danger">*</span></label>
                                <select class="form-control select2 rentfrequency" name="frequency[{{ $propkey + 1 }}]"
                                    id="frequency{{ $propkey + 1 }}">
                                    <option value="">select</option>
                                    @if (!empty($frequency))
                                        @foreach ($frequency as $freq)
                                            @if (!empty($property_frequency[$propkey]))
                                                <option value="{{ $freq->id }} "
                                                    @if ($property_frequency[$propkey]->id == $freq->id) selected @endif>
                                                    {{ $freq->type }}
                                                </option>
                                            @else
                                                <option value="{{ $freq->id }}">{{ $freq->type }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>


                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Rent<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" id="min_rent{{ $propkey + 1 }}"
                                    value="{{ $property->rent }}" name="min_rent[{{ $propkey + 1 }}]"
                                    placeholder="Enter rent">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Security Deposit<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="security_deposit[{{ $propkey + 1 }}]"
                                    id="security_deposit{{ $propkey + 1 }}" value="{{ $property->security_deposit }}"
                                    placeholder="Enter Security deposit">
                            </div>
                        </div>
                    @else
                        <div class="text-dark bg-info tx-white mb-3"> &nbspSelling Details</div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Min Selling price<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" id="min_sellingprice{{ $propkey + 1 }}"
                                        name="min_sellingprice[{{ $propkey + 1 }}]"
                                        value="{{ $property->selling_price }}" placeholder="Enter Amount">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Max Selling price<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" id="max_sellingprice{{ $propkey + 1 }}"
                                        name="max_sellingprice[{{ $propkey + 1 }}]" value="{{ $property->mrp }}"
                                        placeholder="Enter Amount">
                                </div>
                            </div>
                @endif
                <div class="col-md-3 token-hold">
                    <div class="form-group">
                        <label>Token Amount<span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" name="token_amount[{{ $propkey + 1 }}]"
                            id="token_amount{{ $propkey + 1 }}" value="{{ $property->token_amount }}"
                            placeholder="Enter Token amount">
                    </div>
                </div>
                <div class="col-md-3 token-hold">
                    <div class="form-group">
                        <label>Expected Amount<span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" name="expected_amount[{{ $propkey + 1 }}]"
                            id="expected_amount{{ $propkey + 1 }}" value="{{ $property->expected_amount }}"
                            placeholder="Enter Expected amount">
                    </div>
                </div>
        </div>
        <div class="text-dark bg-info tx-white mb-3"> &nbspDocuments</div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Images <span class="tx-danger">*</span></label><br>
                    <input id="images{{ $propkey + 1 }}" name="images[{{ $propkey + 1 }}][]" type="file"
                        class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                    @foreach ($property_image[$propkey] as $image)
                        @if ($image->type == '0')
                            <div class="row photos">
                                {{-- <div class="  m-2"> --}}
                                    <div class="col-sm-6 col-md-4 col-lg-3 item p-2"> <span title="remove this image"
                                            class="vgreen close close-thick"
                                            onclick="deleteUnitImage(this,'{{ $image->document }}','{{ $image->id }}')">x</span>
                                        <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                            <img src="{{ $image->document }}" alt="" width="100px" height="100px">
                                        </a>
                                    </div>
                                {{-- </div> --}}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Floor Plans <span class="tx-danger">*</span></label><br>
                    <input id="floor_plans{{ $propkey + 1 }}" name="floor_plans[{{ $propkey + 1 }}][]" type="file"
                        class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                    @foreach ($property_image[$propkey] as $floor)
                        @if ($floor->type == '2')
                        <div class="row photos">
                            {{-- <div class="  m-2"> --}}
                                <div class="col-sm-6 col-md-4 col-lg-3 item p-2"> <span title="remove this image"
                                        class="vgreen close close-thick"
                                        onclick="deleteUnitImage(this,'{{ $floor->document }}','{{ $floor->id }}')">x</span>
                                    <a target="_blank" href="{{ $floor->document }}" data-lightbox="photos">
                                        <img src="{{ $floor->document }}" alt="" width="100px" height="100px">
                                    </a>
                                </div>
                            {{-- </div> --}}
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Videos <span class="tx-danger"></span></label><br>
                    <input id="videos{{ $propkey + 1 }}" name="videos[{{ $propkey + 1 }}][]" type="file"
                        class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                    @foreach ($property_image[$propkey] as $image)
                        @if ($image->type == '1')
                        <div class="row photos">
                            {{-- <div class="  m-2"> --}}
                                <div class="col-sm-6 col-md-4 col-lg-3 item p-2"> <span title="remove this image"
                                        class="vgreen close close-thick"
                                        onclick="deleteUnitImage(this,'{{ $image->document }}','{{ $image->id }}')">x</span>
                                    <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                        <img src="{{ $image->document }}" alt="" width="100px" height="100px">
                                    </a>
                                </div>
                            {{-- </div> --}}
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <br>
    @break;
    @if ($i == $unit_count)
    <br> @else
        <hr>
    @endif
    @endfor
    @endforeach
    <div class="errorTxt shadow"></div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </form>
@else
    {{-- <h4 class="text-center text-danger">Units where already added</h4> --}}
    <script type="text/javascript">
        window.location = "{{ url('/property/management/list') }}";
    </script>
    @endif
@else
    {{-- <h4 class="text-center text-danger">There is no property units to show</h4> --}}
    <script type="text/javascript">
        window.location = "{{ url('/property/management/list') }}";
    </script>
    @endif
    </div>
    <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->




    @endsection @section('scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    {{-- <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script> --}}
    {{-- <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script> --}}
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>


    <script>
        const url = '{{ url('/') }}';
        // $('.sellingdetails').hide()
        $('.errorTxt').hide()

        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            container: '#contractUploadModal modal-body'
        });

        // $('#property_to').change(function(){
        //     if($('#property_to').val() == 1){
        //         // buy -> 1 / rent->0
        //         $('.rentdetails').hide();
        //         $('.sellingdetails').show()

        //     }else{
        //         $('.sellingdetails').hide()
        //         $('.rentdetails').show();
        //     }
        // })
    </script>
    <script>
        var amenities_list = @json($amenities_listing);
        var details = @json($details);


        function validateUnitForm() {
            var unit_count = $('#unit_count').val();
            // var furnished_array = [];
            var unit_array = [];

            var errorText = "";
            $('.errorTxt').empty();
            for (var i = 1; i <= unit_count; i++) {
                var amenities = "";
                var details = {};
                var furnished = $("#furnished" + i).find(':selected').val();
                if (furnished == "") {
                    // alert('hai');
                    // errorText = '<p></p>'
                }

                var unitName = $("#property_name" + i).val();
                if (unitName == "") {
                    // alert('hai');
                    errorText += '<p>Unit Name of unit ' + i + ' is required </p>';
                }

                if ($('#property_to').val() == 0) {
                    var frequency = $("#frequency" + i).val();
                    if (frequency == "") {
                        // alert('hai');
                        errorText += '<p>Frequency of unit ' + i + ' is required </p>';
                    }
                    var rent = $("#min_rent" + i).val();
                    if (rent == "") {
                        // alert('hai');
                        errorText += '<p>Rent of unit ' + i + ' is required </p>';
                    }
                    var securityDeposit = $("#security_deposit" + i).val();
                    if (securityDeposit == "") {
                        // alert('hai');
                        errorText += '<p>Security Deposit of unit ' + i + ' is required </p>';
                    }
                    var sellingPrice = "";
                    var mrp = "";
                } else {
                    var sellingPrice = $("#min_sellingprice" + i).val();
                    if (sellingPrice == "") {
                        // alert('hai');
                        errorText += '<p>Min. Selling Price of unit ' + i + ' is required </p>';
                    }
                    var mrp = $("#max_sellingprice" + i).val();
                    if (mrp == "") {
                        // alert('hai');
                        errorText += '<p>Max. Selling Price of unit ' + i + ' is required </p>';
                    }
                    var frequency = "";
                    var rent = "";
                    var securityDeposit = "";
                }
                var tokenAmount = $("#token_amount" + i).val();
                if (tokenAmount == "") {
                    // alert('hai');
                    errorText += '<p>Token amount of unit ' + i + ' is required </p>';
                }



                amenities = $("input:checkbox[name='amenities" + i + "[]']:checked").map(function() {
                    return $(this).val();
                }).get();

                if (amenities.length == 0) {
                    errorText += '<p>Amenities of unit ' + i + ' is required </p>';
                }
                detailsdata = $("input[name='detailsdata" + i + "[]']");
                $detail_empty = 0;
                $("#detailsdata" + i + ":input").each(function() {
                    if ($(this).attr("name").length > 0) {
                        if ($(this).val() != '') {
                            details[$(this).attr("name")] = $(this).val();
                        } else {
                            $detail_empty = 1;
                        }
                    }
                });
                if ($detail_empty == 1) {
                    errorText += '<p>Details of unit ' + i + ' is required</p>';
                }


                // var each_unit = {
                //     'unit_name':unitName,
                //     'furnished':furnished,
                //     'amenities':amenities,
                //     'frequency': frequency,
                //     'rent': rent,
                //     'security_deposit': securityDeposit,
                //     'selling_price': sellingPrice,
                //     'mrp': mrp,
                //     'token_amount': tokenAmount,
                //     'images': dataImage,
                //     'floor_plans': dataFloorPlan,
                //     'videos': dataVideo,
                // };
                // unit_array.push(each_unit);
            }
            // if(errorText != ''){
            //     $('.errorTxt').append(errorText);
            //     $('.errorTxt').show();
            //     return "error";
            // }


            // var propertyId = $('#property_owner_id').val();

            // var data = {    
            //         property_id : propertyId,
            //         units : unit_array,
            //     };

        }
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });

        function deleteUnitImage(element, path, pid) {           
            var deleteimageblock = $(element);

            if (path != "") {
                var form_data = new FormData();
                form_data.append("deletegalleryimagename", path)
                // form_data.append("_token", $('meta[name=_token]').attr('content'));
                $.ajax({
                    url: "/deleteimage/" + pid,
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.success) {
                            deleteimageblock.closest('div').remove();
                            // getuploadedanduploadablegalleryimages();
                        }

                    },
                    error: function(ts) {
                        alert(ts.responseText);
                    }
                });
            }


        }
    </script>
@endsection
