@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }

    .ui-datepicker { position: fixed; top: 270.797px; left: 172.5px; z-index: 5001 !important; display: block; }
    #ui-datepicker-div { z-index: 5001 !important;  }

    .modal { overflow: auto !important; }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height : 20px;
        border-radius: 5px  !important
    }
    #map { height: 450px; width: 100%; }
    #map-2 { height: 450px; width: 100%; }
    .errorTxt{
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding : 10px;
        border-radius :10px;
    }

 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <a class="breadcrumb-item" href="">Property Management</a>
            <span class="breadcrumb-item active">Unit</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        @if($property_id)
        
        @if($unit_count > 0)
        <form  action="{{route('savePropertyUnits')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <input type="hidden" id="property_id" name="property_id" value="{{$property_id}}" >
            <input type="hidden" id="property_owner_id" name="property_owner_id" value="{{$property_id}}" >
            <input type="hidden" id="property_to" name="property_to" value="{{$property_to}}">
            <input type="hidden" id="unit_count" name="unit_count" value="{{$unit_count}}">
            @for($i = 1; $i <= $unit_count; $i++)
            <div class="tab-content" id="myTabContent">
                <h4 class="text-dark">Unit {{$i}}</h4>
                @if($i == 0)
                <a href="" class="btn btn-info duplicate-above-unit">Duplicate Above</a>
                @endif 
                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                            <label>Unit Name<span class="tx-danger">*</span></label>
                            <input class="form-control" id="property_name{{$i}}" name="property_name[{{$i}}]" type="text" value="Unit {{$i}}" placeholder="Enter Unit Name">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Furnished <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="furnished[{{$i}}]" id="furnished{{$i}}">
                                    <option value="0">Not Furnished</option>
                                    <option value="1">Semi Furnished</option>
                                    <option value="2">Fully Furnished</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Vacant\Occupied <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="vacant[{{$i}}]" id="vacant{{$i}}">
                                    <option value="0">Vacant</option>
                                    <option value="1">Occupied</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="text-dark bg-info tx-white mb-3"> &nbspUnit Descriptions</div>
                    <div class="row">

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Description<span class="tx-danger">*</span></label>
                                {{-- <input type="text-area" class="form-control" name="description[]" id="description{{$i}}" placeholder="Description"> --}}
                                <textarea class="form-control" name="description[{{$i}}]" id="description{{$i}}" placeholder="Description"></textarea>
                            </div>
                        </div>
                    </div>
                    @if($details)
                    <div class="text-dark bg-info tx-white mb-3"> &nbspUnit Details</div>
                    <div class="row" id="details-section">
                        @foreach($details as $det)
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>{{$det->name}}<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="detailsdata[{{$i}}][{{$det->detail_id}}]" id="detailsdata{{$i}}" placeholder="{{$det->placeholder}}">
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    @if($amenities_listing)
                    <div class="text-dark bg-info tx-white mb-3"> &nbspAmenities</div>
                    <div class="row">
                        @foreach($amenities_listing as $row)
                            <div class="col-md-12">
                                <div class="form-group">
                                    {{-- <div class="card-warning tx-white mb-3"> &nbsp {{$row->name}}  </div> --}}
                                    <div class="row mg-l-1">
                                        @foreach($row->amenities as $row2)
                                            <label class="ckbox ml-2">
                                                <input type="checkbox" value="{{$row2->id}}" name="amenities[{{$i}}][]">
                                                <span>{{ $row2->name }}</span>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    @if($property_to == 0)
                    <div class="text-dark bg-info tx-white mb-3"> &nbspRental Details</div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Lease type<span class="tx-danger">*</span></label>
                                <select class="form-control select2 rentfrequency" name="frequency[{{$i}}]" id="frequency{{$i}}">
                                    <option value="">--Select--</option>
                                    @foreach($frequency as $freq)
                                        <option value="{{$freq->id}}">{{$freq->type}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Rent<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" id="min_rent{{$i}}" name="min_rent[{{$i}}]" placeholder="Enter rent">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Security Deposit<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="security_deposit[{{$i}}]" id="security_deposit{{$i}}" placeholder="Enter Security deposit">
                            </div>
                        </div>
                    @else
                    <div class="text-dark bg-info tx-white mb-3"> &nbspSelling Details</div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Min Selling price<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" id="min_sellingprice{{$i}}" name="min_sellingprice[{{$i}}]" placeholder="Enter Amount">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Max Selling price<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" id="max_sellingprice{{$i}}" name="max_sellingprice[{{$i}}]" placeholder="Enter Amount">
                            </div>
                        </div>
                    @endif
                        <div class="col-md-3 token-hold">
                            <div class="form-group">
                                <label>Token Amount<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="token_amount[{{$i}}]" id="token_amount{{$i}}" placeholder="Enter Token amount">
                            </div>
                        </div>
                        <div class="col-md-3 token-hold">
                            <div class="form-group">
                                <label>Expected Amount<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="expected_amount[{{$i}}]" id="expected_amount{{$i}}" placeholder="Enter Expected amount">
                            </div>
                        </div>
                    </div>
                    <div class="text-dark bg-info tx-white mb-3"> &nbspDocuments</div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Images <span class="tx-danger">*</span></label><br>
                                <input id="images{{$i}}" name="images[{{$i}}][]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                <input id="floor_plans{{$i}}" name="floor_plans[{{$i}}][]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Videos <span class="tx-danger"></span></label><br>
                                <input id="videos{{$i}}" name="videos[{{$i}}][]" type="file" class="form-control file" multiple data-show-upload="true" data-show-caption="true">
                            </div>
                        </div>
                    </div>
                </div>
            <br>
            @if($i == $unit_count) <br> @else <hr> @endif
            @endfor
            <div class="errorTxt shadow"></div>
            <div class="modal-footer">
                {{-- <a href=""  class="btn btn-primary addBtn" id="addPropertyBtn"  >Submit</a> --}}
                <button type="submit" class="btn btn-primary "  >Submit</button>
            </div>
        </form>  
        @else
        {{-- <h4 class="text-center text-danger">Units where already added</h4> --}}
        <script type="text/javascript">
            window.location = "{{ url('/property/management/list') }}";
        </script>
        @endif
        @else
        {{-- <h4 class="text-center text-danger">There is no property units to show</h4> --}}
        <script type="text/javascript">
            window.location = "{{ url('/property/management/list') }}";
        </script>
        @endif
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->




@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
{{-- <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script> --}}
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
        // $('.sellingdetails').hide()
        $('.errorTxt').hide()

        $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          changeMonth: true,
          changeYear: true,
          container: '#contractUploadModal modal-body'
        });

        // $('#property_to').change(function(){
        //     if($('#property_to').val() == 1){
        //         // buy -> 1 / rent->0
        //         $('.rentdetails').hide();
        //         $('.sellingdetails').show()

        //     }else{
        //         $('.sellingdetails').hide()
        //         $('.rentdetails').show();
        //     }
        // })
</script>
<script>
    var amenities_list  = @json($amenities_listing);
    var details         = @json($details);


    // $(function () {
    //     $('form').on('submit', function (e) {
    //         var unit_count = $('#unit_count').val(); 

    //         var validateData = validateUnitForm();
            
    //         if(validateData == 'error'){
    //             return fasle;
    //         }

    //         // var formData = new FormData();
    //         // dataImage     = [];
    //         // dataVideo     = [];
    //         // dataFloorPlan = [];
    //         var formData    = $(this).serializeArray();
            
    //         // var dataImage = new FormData(); 
    //         // var dataVideo = new FormData(); 
    //         // var dataFloorPlan = new FormData(); 
    //         for(var i=1; i <= unit_count; i++ ){
    //         //get form image, videos and floorplans
    //             formData.push({name: 'images['+i+']', value: document.getElementById('images'+i).files });
    //             formData.push({name: 'floor_plans['+i+']', value: document.getElementById('floor_plans'+i).files });
    //             formData.push({name: 'videos['+i+']', value: document.getElementById('videos'+i).files });
    //             // formData.append('images', document.getElementById('images'+i).files);
    //             // formData.append('videos', );
    //             // formData.append('floor_plans', document.getElementById('images'+i).files);
    //         }
    //         console.log(formData);

    //         return false;
    //         // formData.append('images',dataImage);
    //         // formData.append('videos',dataVideo);
    //         // formData.append('floor_plans',dataFloorPlan);


    //         // var images =  document.getElementById('images'+i).files;
    //         // if(images.length == 0){
    //         //     // alert('hai');
    //         //     errorText += '<p>Image of unit '+i+' is required </p>';
    //         // }

    //         // var floorPlans =  document.getElementById('floor_plans'+i).files ;
    //         // hasFileImage = floorPlans.length;
    //         // if(hasFileImage == 0){
    //         //     // alert('hai');
    //         //     errorText += '<p>Floor plan of unit '+i+' is required </p>';
    //         // }

    //         // var videos =  document.getElementById('videos'+i).files;

    //         $.ajax({
    //             type: 'post',
    //             url: '{{route("savePropertyUnits")}}',
    //             headers: {
    //                 "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    //             },
    //             data: formData.serialize(),
    //             success: function () {
    //                 return false;
    //             }
    //         });
    //         e.preventDefault();
    //     });
    // });


    function validateUnitForm(){
        var unit_count = $('#unit_count').val(); 
        // var furnished_array = [];
        var unit_array = [];
        
        var errorText = "";
        $('.errorTxt').empty();
        for(var i=1; i <= unit_count; i++ ){
            var amenities = "";
            var details = {};
            var furnished =  $("#furnished"+i).find(':selected').val();
            if(furnished == ""){
                // alert('hai');
                // errorText = '<p></p>'
            }
            
            var unitName =  $("#property_name"+i).val();
            if(unitName == ""){
                // alert('hai');
                errorText += '<p>Unit Name of unit '+i+' is required </p>';
            }

            if($('#property_to').val() == 0){
                var frequency =  $("#frequency"+i).val();
                if(frequency == ""){
                    // alert('hai');
                    errorText += '<p>Frequency of unit '+i+' is required </p>';
                }
                var rent =  $("#min_rent"+i).val();
                if(rent == ""){
                    // alert('hai');
                    errorText += '<p>Rent of unit '+i+' is required </p>';
                }
                var securityDeposit =  $("#security_deposit"+i).val();
                if(securityDeposit == ""){
                    // alert('hai');
                    errorText += '<p>Security Deposit of unit '+i+' is required </p>';
                }
                var sellingPrice = "";
                var mrp = "";
            }else{
                var sellingPrice =  $("#min_sellingprice"+i).val();
                if(sellingPrice == ""){
                    // alert('hai');
                    errorText += '<p>Min. Selling Price of unit '+i+' is required </p>';
                }
                var mrp     =  $("#max_sellingprice"+i).val();
                if(mrp == ""){
                    // alert('hai');
                    errorText += '<p>Max. Selling Price of unit '+i+' is required </p>';
                }
                var frequency = "";
                var rent = "";
                var securityDeposit = "";
            }
            var tokenAmount =  $("#token_amount"+i).val();
            if(tokenAmount == ""){
                // alert('hai');
                errorText += '<p>Token amount of unit '+i+' is required </p>';
            }



            amenities   = $("input:checkbox[name='amenities"+i+"[]']:checked").map(function(){return $(this).val();}).get();

            if(amenities.length == 0){
                errorText += '<p>Amenities of unit '+i+' is required </p>';
            }
            detailsdata = $("input[name='detailsdata"+i+"[]']");
            $detail_empty = 0;
            $("#detailsdata"+i+":input").each(function() {
                if($(this).attr("name").length > 0) {
                    if($(this).val() != ''){
                        details[$(this).attr("name")] = $(this).val();
                    }else{
                        $detail_empty = 1;
                    }
                }
            });
            if($detail_empty == 1){
                errorText += '<p>Details of unit '+i+' is required</p>';
            }
            

            // var each_unit = {
            //     'unit_name':unitName,
            //     'furnished':furnished,
            //     'amenities':amenities,
            //     'frequency': frequency,
            //     'rent': rent,
            //     'security_deposit': securityDeposit,
            //     'selling_price': sellingPrice,
            //     'mrp': mrp,
            //     'token_amount': tokenAmount,
            //     'images': dataImage,
            //     'floor_plans': dataFloorPlan,
            //     'videos': dataVideo,
            // };
            // unit_array.push(each_unit);
        }
        // if(errorText != ''){
        //     $('.errorTxt').append(errorText);
        //     $('.errorTxt').show();
        //     return "error";
        // }


        // var propertyId = $('#property_owner_id').val();

        // var data = {    
        //         property_id : propertyId,
        //         units : unit_array,
        //     };

    }


</script>
@endsection