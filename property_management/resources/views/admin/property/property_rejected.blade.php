@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }
    .modal { overflow: auto !important; }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Property</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Property</h4>
                            <p class="card-category">List of all property</p>
                        </div>
                        <div class="col-md-6 row justify-content-center">
                                <a href="{{ route('showProperty',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-align-justify tx-20"></i>&nbspPending
                            </a>
                            <a href="{{ route('showProperty',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-check-circle tx-20"></i>&nbspVerified
                            </a>
                            <a href="{{ route('showProperty',['status' => 2]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fad fa-ban tx-20"></i>&nbspRejected
                            </a>
                        </div>
                        <!-- <div class="pd-5 col-md-4 d-flex justify-content-end">
                            <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspProperty</a>
                        </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-15p">Id</th>
                                <th class="wd-15p">Register no:</th>
                                {{-- <th class="wd-20p">Area</th> --}}
                                <th class="wd-20p">Category</th>
                                <th class="wd-20p">City|State</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($property as $row)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$row->property_reg_no}}</td>
                                {{-- <td>{{$row->area}}</td> --}}
                                <td>
                                    <span class="badge rounded-pill bg-warning text-dark">
                                        @if($row->category == 0)
                                            Residential
                                        @elseif($row->category == 1)
                                            Commercial
                                        @endif
                                    </span> |
                                    @if($row->propert_to == 0)
                                        <span class="badge rounded-pill bg-info text-white"> Rent </span>
                                    @else
                                        <span class="badge rounded-pill bg-primary text-white"> Buy </span>
                                    @endif
                                </td>
                                <td>{{$row->city_rel->name}} | {{$row->state_rel->name}}</td>
                                <td>
                                    <a class="fad  fa-unlock tx-20 reject-button" href="javascript:0;" data-property="" title="UnBlock"
                                         onclick="blockFunction('{{$row->id}}','0')">
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    {!! $property->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content delete-popup">
         <div class="modal-header">
            <h4 class="modal-title tx-danger reject-modal-title">Unblock Property</h4>
         </div>
         <form action="javascript:;" id="delete-form">
            <div class="modal-body">
                Are you sure you want to Unblock this Property?
               <input type="hidden" name="propertyId" id="propertyId">
               <input type="hidden" name="action" id="action">
               <!--Action -  1-unblock , 0- block -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
               <button type="submit" class="btn btn-danger reject-class deletetBtn">Block</button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Property`);
    $("#property_nav").addClass("active");
    $("#property_sub_nav").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>
    function blockFunction(id,action){
        $('#propertyId').val(id);
        $('#action').val(action);
        if(action == 1 ){
            $('.reject-modal-title').text('Unblock Property');
            $('.deletetBtn').text('Unblock');
        }
        $('#rejectModal').modal()
    }
    $('.deletetBtn').on('click', function() {
            var id = $('#propertyId').val()
            var action = $('#action').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/property/block/" + id + '/' + action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    if (action == 0) {
                        $(".deletetBtn").text('Block');
                    } else {
                        $(".deletetBtn").text('Unblock');
                    }
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
        });

</script>
<script>

    $('.edit-button').click(function(){
        var adminid = $(this).data('admin')
        $.ajax({
            url : url+"/admin/view/"+adminid,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    $('#myModalLabel').text('Edit Admin')
                    $('#id').val(data.response.id);
                    $('#first_name').val(data.response.first_name);
                    $('#last_name').val(data.response.last_name);
                    $('#country').val(data.response.country).trigger("change");
                    $('#state').val(data.response.state).trigger("change");
                    $('#city').val(data.response.city).trigger("change");
                    $('#dept').val(data.response.department).trigger("change");
                    $('#des').val(data.response.designation).trigger("change");
                    $('#zipcode').val(data.response.zip_code);
                    $('#address').val(data.response.address);
                    $('#email').val(data.response.email);
                    $('#phone').val(data.response.phone);
                    $('#role').val(data.response.role).trigger("change");
                    $('#status').val(data.response.active).trigger("change");
                    $('#password').attr('placeholder','change password');
                    $('#addModal').modal()
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })
</script>
@endsection
