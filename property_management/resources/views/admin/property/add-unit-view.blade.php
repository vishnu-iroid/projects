@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .vgreen {
        background: #65e715 !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .close {
        cursor: pointer;
        position: absolute;
        top: 15%;
        right: 12%;
        padding: 4px 8px 4px 8px;
        transform: translate(0%, -50%);
        color: white;
        opacity: 1;
    }

    .close:hover {
        background: #bbb;
    }

    .styles {
        border: 1px solid #495057;
        height: 43px;
        padding: 11px
    }

    .styles1 {
        border: 1px solid #495057;
        height: 80px;
        padding: 11px;
        width: 1090px;
    }

    .lightbox-gallery {
        background-image: linear-gradient(#4A148C, #E53935);
        background-repeat: no-repeat;
        color: #000;
        overflow-x: hidden
    }

    .lightbox-gallery p {
        color: #fff
    }

    .lightbox-gallery h2 {
        font-weight: bold;
        margin-bottom: 40px;
        padding-top: 40px;
        color: #fff
    }

    @media (max-width:767px) {
        .lightbox-gallery h2 {
            margin-bottom: 25px;
            padding-top: 25px;
            font-size: 24px
        }
    }

    .lightbox-gallery .intro {
        font-size: 16px;
        max-width: 500px;
        margin: 0 auto 40px
    }

    .lightbox-gallery .intro p {
        margin-bottom: 0
    }

    .lightbox-gallery .photos {
        padding-bottom: 20px
    }

    .lightbox-gallery .item {
        padding-bottom: 30px
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="">Property Management</a>
            <span class="breadcrumb-item active">Unit View</span>
        </nav>
    </div>

    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card"> Property Units </h4>
                            {{-- <p class="card-category"></p> --}}
                        </div>

                    </div>
                </div>
                <div class="modal-body">
                    @if ($property_id)
                        @if ($unit_count > 0)
                            <div class="">
                                @foreach ($property_unit1 as $propkey => $property)
                                    @for ($i = 1; $i <= $unit_count; $i++)
                                        <div class="tab-content" id="myTabContent">
                                            <h4 class="text-dark">Unit {{ $propkey + 1 }}</h4>
                                            @if ($i == 0)
                                                <a href="" class="btn btn-info duplicate-above-unit">Duplicate Above</a>
                                            @endif
                                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                                aria-labelledby="details-tab">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Unit Name</label>
                                                            <p class="styles">{{ $property->property_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Furnished </label>
                                                            @if ($property->furnished == '0')
                                                                <p class="styles"> Not Furnished</p>
                                                            @elseif ($property->furnished == '1')
                                                                <p class="styles"> Semi Furnished</p>
                                                            @elseif ($property->furnished == '2')
                                                                <p class="styles"> Fully Furnished</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Property Status </label>
                                                            @if ($property->occupied == '0')
                                                                <p class="styles">Occupied</p>
                                                            @else
                                                                <p class="styles">Vaccant</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-dark bg-info tx-white mb-3 p-2"> &nbspUnit Descriptions</div>
                                                <div class="row">

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>Description</label>
                                                            <p class="styles1">{{ $property->description }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-dark bg-info tx-white mb-3 p-2"> &nbspUnit Details</div>
                                                <div class="row" id="details-section">
                                                    @if ($details)
                                                        @foreach ($details_prop[$propkey] as $key => $detail)
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>{{ str_replace('_', ' ', ucfirst($detail->placeholder)) }}<span
                                                                            class="tx-danger">*</span></label>
                                                                    <p class="styles">{{ $detail->value }}</p>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                </div>
                                    @endif

                                    @if (isset($amenities_listing))
                                        <div class="text-dark bg-info tx-white mb-3 p-2"> &nbspAmenities</div>
                                        <div class="row">
                                            @foreach ($amenities_listing[$propkey] as $row)
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        {{-- <div class="card-warning tx-white mb-3"> &nbsp {{$row->name}}  </div> --}}
                                                        <div class="row mg-l-1">
                                                            {{-- @foreach ($row->amenities as $row2) --}}
                                                            <label class="ckbox ml-2">
                                                                <input type="checkbox" value="{{ $row->id }}"
                                                                    name="amenities[{{ $propkey + 1 }}][]" checked>
                                                                <span>{{ $row->name }}</span>
                                                            </label>
                                                            {{-- @endforeach --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                            </div>
                        @endif
                        @if ($property->property_to == 0)
                            <div class="text-dark bg-info tx-white mb-3 p-2"> &nbspRental Details</div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Lease Type</label>
                                        @if (!empty($property_frequency[$propkey]))
                                            <p class="styles">
                                                @if ($property_frequency[$propkey]->id)
                                                    {{ $property_frequency[$propkey]->type }}
                                                @endif
                                            </p>
                                        @else
                                            <p class="styles">Not Mentioned</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Rent</label>
                                        <p class="styles">{{ $property->rent }}</p>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Security Deposit</label>
                                        <p class="styles">{{ $property->security_deposit }}</p>
                                    </div>
                                </div>
                            @else
                                <div class="text-dark bg-info tx-white mb-3 p-2"> &nbspSelling Details</div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Min Selling price</label>
                                            <p class="styles">{{ $property->selling_price }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Max Selling price</label>
                                            <p class="styles">{{ $property->mrp }}</p>
                                        </div>
                                    </div>
                        @endif
                        <div class="col-md-3 token-hold">
                            <div class="form-group">
                                <label>Token Amount</label>
                                <p class="styles">{{ $property->token_amount }}</p>
                            </div>
                        </div>
                        <div class="col-md-3 token-hold">
                            <div class="form-group">
                                <label>Expected Amount</label>
                                <p class="styles">{{ $property->expected_amount }}</p>
                            </div>
                        </div>
                </div>
                <div class="text-dark bg-info tx-white mb-3 p-2"> &nbspDocuments</div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Images </label><br>
                            <div class="photos">
                                <div class="row p-2">
                                    @foreach ($property_image[$propkey] as $image)
                                        @if ($image->type == '0')
                                            <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                                <img class="p-2" src="{{ $image->document }}" alt=""
                                                    width="100px" height="100px">
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Floor Plans </label><br>
                            <div class="photos">
                                <div class="row p-2">
                                    @foreach ($property_image[$propkey] as $floor)
                                        @if ($floor->type == '2')
                                            <a target="_blank" href="{{ $floor->document }}" data-lightbox="photos">
                                                <img class="p-2" src="{{ $floor->document }}" alt=""
                                                    width="100px" height="100px">
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Videos <span class="tx-danger"></span></label><br>
                            <div class="photos">
                                <div class="row p-2">
                                    @foreach ($property_image[$propkey] as $image)
                                        @if ($image->type == '1')
                                            <a target="_blank" href="{{ $image->document }}" data-lightbox="photos">
                                                <img class="p-2" src="{{ $image->document }}" alt=""
                                                    width="100px" height="100px">
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            @break;
            @if ($i == $unit_count)
            <br> @else
                <hr>
            @endif
            @endfor
            @endforeach
            <div class="errorTxt shadow"></div>

        @else
            {{-- <h4 class="text-center text-danger">Units where already added</h4> --}}
            <script type="text/javascript">
                window.location = "{{ url('/property/management/list') }}";
            </script>
            @endif
        @else
            {{-- <h4 class="text-center text-danger">There is no property units to show</h4> --}}
            <script type="text/javascript">
                window.location = "{{ url('/property/management/list') }}";
            </script>
            @endif
        </div>
    </div>
    <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>

    </div>
@endsection
@section('scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
{{-- <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script> --}}
{{-- <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script> --}}
<script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>


<script>
    const url = '{{ url('/') }}';
</script>
@endsection
