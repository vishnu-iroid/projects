@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }
    .modal { overflow: auto !important; }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .ui-datepicker { position: fixed; top: 270.797px; left: 172.5px; z-index: 5001 !important; display: block; }
 #ui-datepicker-div {
    z-index: 5001 !important;
 }
 </style>


@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Property</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Property</h4>
                            <p class="card-category">List of all property</p>
                        </div>
                        <div class="col-md-6 row justify-content-center">
                                <a href="{{ route('showProperty',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-align-justify tx-20"></i>&nbspPending
                            </a>
                            <a href="{{ route('showProperty',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fad fa-check-circle tx-20"></i>&nbspVerified
                            </a>
                            <a href="{{ route('showProperty',['status' => 2]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-ban tx-20"></i>&nbspRejected
                            </a>
                        </div>
                        <!-- <div class="pd-5 col-md-4 d-flex justify-content-end">
                            <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspProperty</a>
                        </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <div lass="col-md-12">
                        <!-- <div class="col-md-4 d-flex justify-content-center">
                            <a href="" class="btn">Vacant</a>
                            <a href="" class="btn">Occupied</a>
                        </div> -->
                        {{-- <div class="ht-lg-40 bg-gray-200 pd-x-20 d-lg-flex align-items-center justify-content-center mb-2">
                            <ul class="nav nav-effect nav-effect-1 flex-column flex-lg-row" role="tablist">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#" role="tab">Vacant</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#" role="tab">Occupied</a></li>
                            </ul>
                        </div> --}}
                    </div>
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-15p">Register no:</th>
                                {{-- <th class="wd-20p">Area</th> --}}
                                <th class="wd-20p">Category</th>
                                <th class="wd-20p">City|State</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($property as $row)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$row->property_reg_no}}</td>
                                {{-- <td>{{$row->area}}</td> --}}
                                <td>
                                    <span class="badge rounded-pill bg-warning text-dark">
                                        @if($row->category == 0)
                                            Residential
                                        @elseif($row->category == 1)
                                            Commercial
                                        @endif
                                    </span> |
                                    @if($row->propert_to == 0)
                                        <span class="badge rounded-pill bg-info text-white"> Rent </span>
                                    @else
                                        <span class="badge rounded-pill bg-primary text-white"> Buy </span>
                                    @endif
                                </td>
                                <td>{{$row->city_rel->name}} | {{$row->state_rel->name}}</td>
                                <td>
                                    <a class="fad fa-eye tx-20 view-button" href="javascript:0;" data-property="{{$row->id}}" title="View"></a>
                                    <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus  ml-1" style="color:red" href="javascript:0;" data-property="" title="block" onclick="blockFunction('{{$row->id}}','2')"></a>
                                    <a class="fad fa-upload tx-20  ml-1 upload-button" href="" property="{{$row->id}}" data-propertyto="{{$row->property_to}}" style="color:rgb(0, 119, 255)" onclick="uploadPropertyContract({{$row->id}},{{$row->property_to}})" id="upload-contract" title="Upload Contract"></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    {!! $property->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->


{{-- <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ml-2">
                <div class=" ">
                    <label class="font-italic">Images : </label></br>
                    <div class="form-group image-class" style="display: none;">

                    </div>
                </div>
                <div class="row">
                    <label class="font-italic">Register Number : </label>
                    <p id="regno" class="font-italic"></p>
                </div>
        </div>
    </div>
</div> --}}


{{-- View Modal --}}
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title tx-primary" id="exampleModalLabel">View Properties</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <table class="table table-bordered">
                <thead>
                    <th>ID</th>
                    <th>Register Number</th>
                    <th>Property</th>
                    {{-- <th>Image</th> --}}
                    <th>Category</th>
                    <th>City</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <tr>
                        <td class="propertyId"></td>
                        <td class="propertyRegisterNo"></td>
                        {{-- <td class="propertyImage"></td> --}}
                        <td class="propertyName"></td>
                        <td class="propertyCategory"></td>
                        <td class="propertyCity"></td>
                        <td class="status"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        </div>
    </div>
</div>
</div>

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content delete-popup">
         <div class="modal-header">
            <h4 class="modal-title tx-danger reject-modal-title">Block Property</h4>
         </div>
         <form action="javascript:;" id="delete-form">
            <div class="modal-body">
                Are you sure you want to block this property?
               <input type="hidden" name="propertyId" id="propertyId">
               <input type="hidden" name="action" id="action">
               <!--Action -  1-unblock , 0- block -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
               <button type="submit" class="btn btn-danger reject-class deletetBtn">Block</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade contractUpload" id="contractUploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Upload Contract Details</h4>
            </div>
            <form action="javascript:;" id="contract-form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Contract No</label>
                                <input type="text" class="form-control" name="contract_no" id="contract_no">
                            </div>
                            <input type="hidden" name="property_id" id="property_id">
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Contract File</label>
                                <input type="file" class="form-control" name="contract_file" id="contract_file">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ownership Doc<span class="tx-danger">*</span></label>
                                <input type="file" class="form-control" name="ownership_doc"
                                    id="ownership_doc">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract Start Date<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control fc-datepicker" name="contract_start_date" id="contract_start_date" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract End Date<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control fc-datepicker" name="contract_end_date" id="contract_end_date" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Property To<span class="tx-danger">*</span></label>
                                <select class="form-control" name="property_to" id="property_to" onchange="properties(this.value)">
                                    <option value="">Select</option>
                                    <option value="0">Rent</option>
                                    <option value="1">Buy</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 token-hold">
                            <div class="form-group">
                                <label>Token Amount<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="token_amount" id="token_amount">
                            </div>
                        </div>
                    </div>
                    <div class="row frequency_rent" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Frequency</label>
                                <select class="form-control select2 agent-select" name="frequency" id="frequency" >
                                    <option value="">Select</option>
                                    @foreach ($frequency as $row)
                                    <option value="{{ $row->id}}">{{ $row->type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Rent<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="rent" id="rent">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Security Deposit<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="security_deposit" id="security_deposit">
                            </div>
                        </div>
                    </div>
                    <div class="row sellingprice" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Selling Price<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="selling_price" id="selling_price">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>MRP<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="mrp" id="mrp">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Commission in terms of :-<span class="tx-danger">*</span></label><br>
                                <label>Amount</label>
                                <input type="radio"  checked name="commision_as" id="commission_as" value="amount" >
                                <label>Percentage</label>
                                <input type="radio"  name="commision_as" id="commission_as" value="percentage" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Commission<span class="tx-danger" style="font-size:12px;">*</span></label>
                                <input type="number" class="form-control" name="commission" id="commission"  placeholder="Enter Commission">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Property Management fee:-<span
                                        class="tx-danger">*</span></label><br>
                                <label>Amount</label>
                                <input type="radio" class="property_management_fee_type"  name="property_management_fee_type"
                                    id="property_management_fee_type" value="amount" checked>
                                <label>Percentage</label>
                                <input type="radio" class="property_management_fee_type" name="property_management_fee_type"
                                    id="property_management_fee_type" value="percentage">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Management Fee<span class="tx-danger"
                                        style="font-size:12px;">*</span></label>
                                <input type="number" class="form-control" name="management_fee"
                                    id="management_fee" placeholder="Enter Management Fee">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Ads Number:-<span class="tx-danger">*</span></label><br>
                                <input type="text" class="form-control" name="adsnumber" id="adsnumber"
                                    placeholder="Enter Ads Number">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Guard Number<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control" name="guard_number"
                                    id="guard_number" placeholder="Enter Guard Number">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white contractcloseBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class contractBtn">Submit</button>
                </div>
            </form>
        </div>
    </div>
 </div>

@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Property`);
    $("#property_nav").addClass("active");
    $("#property_sub_nav").addClass("active");
     // Datepicker

    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
    function properties(val){
        //alert(val);
        var property = val;
        if(property==0){
            $('.frequency_rent').show();
            $('.sellingprice').hide();
            // $('.token-hold').remove();
        //     $('.frequency_rent').append(`<div class="col-md-6 token-hold">
        //                                     <div class="form-group">
        //                                         <label>Token Amount<span class="tx-danger">*</span></label>
        //                                         <input type="text" class="form-control" name="token_amount" id="token_amount">
        //                                     </div>
        //                                 </div>`);

        }else if(property==1){
            $('.sellingprice').show();
            $('.frequency_rent').hide();
            // $('.token-hold').remove();
        //     $('.sellingprice').append(`<div class="col-md-6 token-hold">
        //                                     <div class="form-group">
        //                                         <label>Token Amount<span class="tx-danger">*</span></label>
        //                                         <input type="text" class="form-control" name="token_amount" id="token_amount">
        //                                     </div>
        //                                 </div>`);
        }
    }
    function blockFunction(id,action){
        $('#propertyId').val(id);
        $('#action').val(action);
        if(action == 1 ){
            $('.reject-modal-title').text('Unblock Property');
            $('.deletetBtn').text('Unblock');
        }
        $('#rejectModal').modal()
    }
    $('.deletetBtn').on('click', function() {
            var id = $('#propertyId').val()
            var action = $('#action').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/property/block/" + id + '/' + action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    if (action == 0) {
                        $(".deletetBtn").text('Block');
                    } else {
                        $(".deletetBtn").text('Unblock');
                    }
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
        });


    $('.edit-button').click(function(){
        var adminid = $(this).data('admin')
        $.ajax({
            url : url+"/admin/view/"+adminid,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    $('#myModalLabel').text('Edit Admin')
                    $('#id').val(data.response.id);
                    $('#first_name').val(data.response.first_name);
                    $('#last_name').val(data.response.last_name);
                    $('#country').val(data.response.country).trigger("change");
                    $('#state').val(data.response.state).trigger("change");
                    $('#city').val(data.response.city).trigger("change");
                    $('#dept').val(data.response.department).trigger("change");
                    $('#des').val(data.response.designation).trigger("change");
                    $('#zipcode').val(data.response.zip_code);
                    $('#address').val(data.response.address);
                    $('#email').val(data.response.email);
                    $('#phone').val(data.response.phone);
                    $('#role').val(data.response.role).trigger("change");
                    $('#status').val(data.response.active).trigger("change");
                    $('#password').attr('placeholder','change password');
                    $('#addModal').modal()
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $('.view-button').click(function(){
        var propertyid = $(this).data('property')
        $.ajax({
            url : url+"/property/view/"+propertyid,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
               
                if(data.status == true){
                     console.log(data.response);
                    
                    $(".propertyId").html(data.response.id);
                    $(".propertyRegisterNo").html(data.response.property_reg_no );
                    $(".propertyName").html(data.response.property_name);
                    if(data.response.category==1){
                    $(".propertyCategory").html('Commercial');
                    }
                    else if(data.response.category==0){
                        $(".propertyCategory").html('Residential');
                    }
                    $(".propertyCity").html(data.response.city_rel.name);
                    if(data.response.status==0){
                    $(".status").html('Pending');}
                    else if(data.response.status==1){
                        $(".status").html('Approved');
                    }
                    else if(data.response.status==2){
                        $(".status").html('Rejected');
                    }
                    
                    $('#viewModal').modal()
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })
    function uploadPropertyContract(property_id,property_to){
        event.preventDefault();
        $('#property_id').val(property_id);
        $('#property_to').val(property_to);
        if(property_to==0){
            $('.frequency_rent').show();
            $('.sellingprice').hide();
        }else if(property_to==1){
            $('.sellingprice').show();
            $('.frequency_rent').hide();
        }
        $('#contractUploadModal').modal();
        $('.fc-datepicker').datepicker({
          showOtherMonths: true,
          selectOtherMonths: true,
          container: '#contractUploadModal modal-body'
        });
    }
    $("#contract-form").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            property_id: {
                required: true,
            },
            property_for: {
                required: true,
            },
            contract_start_date: {
                required: true,
            },
            contract_end_date: {
                required: true,
            },
            property_to:{
                required: true,
            },
            commission_as:{
                required: true,
            },
            commission:{
                required: true,
            }

        },
        submitHandler: function (form) {
            var form        = document.getElementById("contract-form");
            var formData    = new FormData(form);
            $(".contractBtn").prop("disabled", true);
            $(".contractcloseBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addContractDetails')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".contractBtn").text('Processing..');
                },
                success: function (data) {
                    $(".contractBtn").text('Submit');
                    $(".contractBtn").prop("disabled", false);
                    $(".contractcloseBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
