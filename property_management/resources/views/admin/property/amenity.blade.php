@extends('admin.layout.app')
 <style>
    .br-section-wrapper-dept {
        background-color: #fff; padding: 10px 10px; box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%); border-radius: 6px;
    }
    .select2-container {
      z-index:10050; width: 100% !important; padding: 0;
    }
    .card {
        position: relative; display: flex; flex-direction: column; min-width: 0; word-wrap: break-word; background-color: #fff;
        background-clip: border-box; border: 1px solid #eee; border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem; font-weight: 300 !important;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Master</span>
        <span class="breadcrumb-item active">Amenities</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">

<div class="row">
    <div class="col-md-12">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Amenity categories</h4>
                        <p class="card-category">List of all amenity categories</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModalCategory"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAmenity Category</a> 
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive wrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Sort Order</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($category as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->sort_order }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-type="{{$row->id}}" title="Edit"></a>
                                <!-- <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deleteFunction('{{$row->id}}')"></a> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                        {!! $category->links() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                  <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Amenities</h4>
                        <p class="card-category">List of all amenities</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModalAmenity"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAmenity</a> 
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Icon</th>
                            <th class="wd-15p">Category</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($amenity as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td><img src="{{ asset($row->image) }}" alt="Amenity" width="40px" height="35px"></td>
                            <td>{{ $row->category->name }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button-amenity mr-1" href="javascript:0;" data-amenity="{{$row->id}}" title="Edit"></a>
                                <!-- <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deleteFunction('{{$row->id}}')"></a> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                        {!! $amenity->links() !!}
                </div>
            </div>
        </div>
    </div>
    
</div>
</div>
<!-- br-pagebody -->

<!-- Category add Modal -->
<div
    class="modal fade"
    id="addModalCategory"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel"
    aria-hidden="true"
    style="overflow: hidden"
>
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
         <h4 class="modal-title text-center tx-primary" id="myModalLabelCategory">Add Amenity Category</h4>
         </div>
         <form action="javascript:;" id="form-add-category">
            <div class="col-md-12">
                <div class="form-group">
                    <label> Category Name <span class="tx-danger">*</span></label>
                    <input type="hidden" id="categoryid" name="categoryid">
                    <input class="form-control" id="category" name="category" type="text" placeholder="Enter category name"/>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Sort Order <span class="tx-danger">*</span></label>
                    <input class="form-control" id="sort" name="sort" type="text" value="0" placeholder="Enter sort order"/>
                </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Close</button>
               <button type="submit" class="btn btn-primary addBtn">Submit</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Amenity add Modal -->
<div
    class="modal fade"
    id="addModalAmenity"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel"
    aria-hidden="true"
    style="overflow: hidden"
>
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
         <h4 class="modal-title text-center tx-primary" id="myModalLabelAmenity">Add Amenity</h4>
         </div>
         <form action="javascript:;" id="form-add-amenity">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Name <span class="tx-danger">*</span></label>
                    <input type="hidden" id="amenityId" name="amenityId">
                    <input class="form-control" id="amenityname" name="amenityname" type="text" placeholder="Enter amenity name"/>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Image <span class="tx-danger">*</span></label>
                    <div class="form-group image-class" style="display: none;">
                        <img class="img-thumbnail image-display" src="#" alt="agent" width="100">
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image">
                        <label class="custom-file-label" id="file_label">Choose file</label>
                        <input type="hidden" id="current_image" name="current_image">
                     </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Category <span class="tx-danger">*</span></label>
                    <select class="form-control select2" name="amenity_category" id="amenity_category">
                     <option value=""></option>
                     @foreach($category_list as $row5)
                        <option value="{{$row5->id}}">{{$row5->name}}</option>
                     @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Close</button>
               <button type="submit" class="btn btn-primary addBtn">Submit</button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
</script>
<script>
    $('#image').on('change',function(){
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
</script>
<script>
    $("#site_title").html(`Property | Amenity`);
    $("#master").addClass("active");
    $("#property_amenity_nav").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        $("#datatable2").DataTable({
            responsive: true,
            autoWidth:false;
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>

    $('tbody').on('click','.edit-button',function(){
        var typeId = $(this).data('type')
        $.ajax({
            url : url+"/amenity-category/view/"+typeId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    $('#myModalLabelCategory').html('Edit Amenity Category')
                    $('#categoryid').val(data.response.id)
                    $('#category').val(data.response.name)
                    $('#sort').val(data.response.sort_order)
                    $('#addModalCategory').modal()
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $('tbody').on('click','.edit-button-amenity',function(){
        var amenityId = $(this).data('amenity')
        $.ajax({
            url : url+"/amenity/view/"+amenityId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    $('#myModalLabelAmenity').html('Edit Amenity')
                    $('#amenityId').val(data.response.id)
                    $('#amenityname').val(data.response.name)
                    $('.image-class').css('display','block')
                    $('.image-display').prop('src',url+data.response.image)
                    $('#current_image').val(data.response.image)
                    $('#current_image').val(data.response.image)
                    $('#amenity_category').val(data.response.amenity_category_id).trigger('change')
                    $('#addModalAmenity').modal()
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $("#form-add-category").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            category: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-category");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addAmenityCategory')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });

    $("#form-add-amenity").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            amenityname: {
                required: true,
            },
            image: {
              required : function(){
                  if($('#amenityId').val()){
                      return false
                  }
                  return true
              },
            },
            amenity_category: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add-amenity");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addAmenity')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
