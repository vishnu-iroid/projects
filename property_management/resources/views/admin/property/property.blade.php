@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .error {
        color: red;
    }

    .hide-element {
        display: none !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Property Management</span>
            <span class="breadcrumb-item active">Request to List Property</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
            <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                <i class="fas fa-plus-square"></i>&nbsp Add Client
            </button>
    </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Request to List Property</h4>
                            <p class="card-category">List of all property</p>
                        </div>
                        <div class="col-md-6 row d-flex justify-content-center">
                            <a href="{{ route('showProperty', ['status' => 0]) }}"
                                class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fad fa-align-justify tx-20"></i>&nbspPending
                            </a>
                            <a href="{{ route('showProperty', ['status' => 1]) }}"
                                class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-check-circle tx-20"></i>&nbspVerified
                            </a>
                            <a href="{{ route('showProperty', ['status' => 2]) }}"
                                class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-ban tx-20"></i>&nbspRejected
                            </a>
                        </div>
                        <!-- <div class="pd-5 col-md-4 d-flex justify-content-end">
                                <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspProperty</a>
                            </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive wrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-15p">Register no:</th>
                                {{-- <th class="wd-10p">Area</th> --}}
                                <th class="wd-15p">Category</th>
                                <th class="wd-15p">City|States</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($property as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->property_reg_no }}</td>
                                    {{-- <td>{{$row->area}}</td> --}}
                                    <td>
                                        <span class="badge rounded-pill bg-warning text-dark">
                                            @if ($row->category == 0)
                                                Residential
                                            @elseif($row->category == 1)
                                                Commercial
                                            @endif
                                        </span>
                                        @if ($row->propert_to == 1)

                                            <span class="badge rounded-pill bg-info text-white"> Buy </span>
                                        @elseif($row->propert_to == 0)
                                            <span class="badge rounded-pill bg-primary text-white"> Rent </span>
                                        @endif
                                    </td>
                                    <td>{{ $row->city_rel->name }} | {{ $row->state_rel->name }}</td>
                                    <td>

                                        <a class="fad fa-edit tx-20  btn btn-primary mb-1 bt_cus"
                                            href="{{ route('editProperty', ['id' => $row->id]) }}"
                                            data-propertyId="{{ $row->id }}" title="Edit"></a>
                                        {{-- <a class="fad fa-edit tx-20 edit-button" href="javascript:0;" data-property="{{$row->id}}" title="Edit"></a> --}}
                                        @if ($row->status == 0)
                                            {{-- $row->active --}}
                                            <a class="fad fa-ban fa-unlock-alt tx-20 reject-button btn btn-primary mb-1 bt_cus" href="javascript:0;"
                                                data-property="" title="Reject"
                                                onclick="blockFunction('{{ $row->id }}','2')">
                                            </a>
                                        @else
                                            <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus  btn btn-primary mb-1 bt_cus" href="javascript:0;" data-property=""
                                                title="Block"
                                                onclick="blockFunction('{{ $row->id }}','2')">
                                            </a>
                                        @endif
                                        <!-- <a class="fad fa-plus-square tx-20 move-button ml-1" href="javascript:0;" data-property="{{ $row->id }}" title="Add property" onclick="addPropertyFunction('{{ $row->id }}')"></a> -->

                                        <a class="fad fa-plus-square tx-20 move-button btn btn-primary mb-1 bt_cus" href="javascript:0;"
                                            data-property="{{ $row->id }}" title="Add Agents"
                                            onclick="multiplAgent('{{ $row->id }}')"></a>
                                            @if ($row->is_builder == 1)
                                            <a class="fad fa-building tx-20 btn btn-primary mb-1 bt_cus"  href="{{ route('editPropertyUnits', ['id' => $row->id]) }}"
                                             title="Edit Building Units"></a>
                                            @endif
                                    </td>


                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $property->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <!-- Reject Modal -->
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Block Property</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to block this property?
                        <input type="hidden" name="propertyId" id="propertyId">
                        <input type="hidden" name="action" id="action">
                        <!--Action -  1-unblock , 0- block -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtn">Block</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- ----owner add modal---- -->
    <div class="modal fade contractUpload" id="addPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Add Multiple Agents</h4>
                </div>
                <form action="javascript:;" id="multiple-agent-form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Date & Time <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="date" name="date">
                                    <input class="form-control" type="time" name="fromtime">
                                    <input class="form-control propertyId" type="hidden" name="property_id"
                                        id="property_id">
                                    <label for="agent" class="error"></label>
                                </div>
                                <div class="form-group">
                                    <label>Agents<span class="tx-danger">*</span></label>
                                    <select class="form-control select2 agent-select" name="agents[]" id="agents" multiple>
                                        {{-- <option value="">Select</option> --}}

                                    </select><br>
                                    <label for="agents" class="error"></label>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class addBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Edit Property --}}
    <div class="modal fade" id="addPropertyModal_property" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="overflow: hidden">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center tx-primary" id="myModalLabelProperty">Add Property</h4>
                    {{-- <a href="" class="btn btn-primary float-right"  onclick="addOwnerBuilding()">Add as Building</a> --}}
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link propertynav active" id="details-tab" data-toggle="tab" href="#details"
                                role="tab" aria-controls="details" aria-selected="true">Details</a>
                        </li>
                        <li class="nav-item hide-element">
                            <a id="rent-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab"
                                aria-controls="bank" aria-selected="false">Rent/Selling Details</a>
                        </li>
                        <li class="nav-item hide-element">
                            <a class="nav-link propertynav" id="amenities-tab" data-toggle="tab" href="#amenities"
                                role="tab" aria-controls="amenities" aria-selected="false">Amenities</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link propertynav" id="contract-tab" data-toggle="tab" href="#contract" role="tab"
                                aria-controls="contract" aria-selected="false">Contract Details</a>
                        </li>
                        <li class="nav-item hide-element">
                            <a class="nav-link propertynav" id="documents-tab" data-toggle="tab" href="#documents"
                                role="tab" aria-controls="documents" aria-selected="false">Documents</a>
                        </li>
                        <li class="nav-item">
                            <a id="location-prop-tab" class="nav-link propertynav" data-toggle="tab" href="#location-prop"
                                role="tab" aria-controls="location-prop" aria-selected="false">Location</a>
                        </li>
                    </ul>
                </div>

                <form class="form-admin" id="form-add-property" action="javascript:;">
                    <input type="hidden" id="propertyedit_id" name="propertyedit_id">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="details" role="tabpanel"
                                aria-labelledby="details-tab">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property Type<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 " name="category" id="category_id">
                                                <option value="0">Residential</option>
                                                <option value="1">Commercial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Category<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 category-select" name="type" id="type">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Owners<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 owners-select" name="ownerslist"
                                                id="ownerslist">
                                                <option value=""></option>
                                                @foreach ($owners as $owner)
                                                    <option value="{{ $owner->id }}">{{ $owner->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="hidden" id="property_owner_id" name="property_owner_id">
                                        <div class="form-group">
                                            <label>Property For<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_to" id="property_to">
                                                <option value="0">Rent</option>
                                                <option value="1">Buy</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property Name<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="property_name" name="property_name"
                                                type="text" placeholder="Enter Property Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Country<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="property_country"
                                                id="property_country">
                                                <option value=""></option>
                                                @foreach ($countries as $country)
                                                    <option value="{{ $country->id }}">{{ $country->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>State<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 state-select" name="property_state"
                                                id="property_state">
                                                {{-- @foreach ($states as $state)
                                                     <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                 @endforeach --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 city-select" name="property_city"
                                                id="property_city_id">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Zipcode<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 zipcode-select " id="property_zipcode"
                                                name="property_zipcode">


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address 1 <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address1" name="address1" type="text"
                                                placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address 2 <span class="tx-danger">*</span></label>
                                            <textarea class="form-control" id="address2" name="address2" type="text"
                                                placeholder="Enter address"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Individual Property<span class="tx-danger">*</span></label>
                                            <input type="checkbox" name="individual_building" id="is_builder" value="1">
                                        </div>
                                    </div>
                                    <div class="col-md-6 is-building">
                                        <div class="form-group">
                                            <label>Images <span class="tx-danger">*</span></label><br>
                                            <input id="building_image" name="building_image[]" type="file"
                                                class="form-control file" multiple data-show-upload="true"
                                                data-show-caption="true">
                                            {{-- <input type="file" class="w-75 form-control" id="editlogo" name="logo"
                                                 placeholder="logo" accept="logo/*" onchange="readURL(this);"> --}}
                                            <input type="hidden" name="hidden_image" id="hidden_image">
                                        </div>
                                    </div>
                                </div>
                                <div class="row is-appartment">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. of Units<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="unit_num" name="unit_num" type="number"
                                                placeholder="Enter Unit">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. of floors<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="floor_num" name="floor_num" type="number"
                                                placeholder="Enter Floor">
                                        </div>
                                    </div>
                                </div>
                                <div class="amenities-row">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Furnished <span class="tx-danger">*</span></label>
                                                <select class="form-control select2" name="furnished" id="furnished">
                                                    <option value="0">Not Furnished</option>
                                                    <option value="1">Semi Furnished</option>
                                                    <option value="2">Fully Furnished</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-warning tx-white mb-3"> &nbspDetails</div>
                                    <div class="row" id="details-section">
                                        @if ($details)
                                            @foreach ($details as $det)
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>{{ $det->name }}<span
                                                                class="tx-danger">*</span></label>
                                                        <input type="text" class="form-control"
                                                            name="detailsdata[{{ $det->detail_id }}]"
                                                            id="det-{{ $det->detail_id }}"
                                                            placeholder="{{ $det->placeholder }}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <!-- end of tab1 -->
                            <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                                <div class="rentdetails">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Frequency<span class="tx-danger">*</span></label>
                                                <select class="form-control select2 rentfrequency" name="frequency"
                                                    id="frequency">
                                                    <option value=""></option>
                                                    @foreach ($frequency as $freq)
                                                        <option value="{{ $freq->id }}">{{ $freq->type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Rent<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="min_rent" name="min_rent"
                                                    placeholder="Enter rent">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Security Deposit<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="security_deposit"
                                                    id="security_deposit" placeholder="Enter Security deposit">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sellingdetails">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Min Selling price<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="min_sellingprice"
                                                    name="min_sellingprice" placeholder="Enter minimum selling price">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Max Selling price<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" id="max_sellingprice"
                                                    name="max_sellingprice" placeholder="Enter maximum selling price">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="token-amount">
                                    <div class="row">
                                        <div class="col-md-6 token-hold">
                                            <div class="form-group">
                                                <label>Token Amount<span class="tx-danger">*</span></label>
                                                <input type="text" class="form-control" name="token_amount"
                                                    id="token_amount" placeholder="Enter Token amount">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="amenities" role="tabpanel" aria-labelledby="amenities-tab">
                                <div id="amenties_available">
                                    @if ($amenities_listing)
                                        @foreach ($amenities_listing as $row)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="card-warning tx-white mb-3"> &nbsp
                                                            {{ $row->name }} </div>
                                                        <div class="row mg-l-1">
                                                            @foreach ($row->amenities as $row2)
                                                                <label class="ckbox ml-2">
                                                                    <input type="checkbox" value="{{ $row2->id }}"
                                                                        name="amenities[]" id="amenities_id">
                                                                    <span>{{ $row2->name }}</span>
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <!-- end of tab3 -->
                            <!-- start tab 4 -->
                            <div class="tab-pane fade" id="contract" role="tabpanel" aria-labelledby="contract-tab">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contract No</label>
                                            <input type="text" class="form-control" name="contract_no" id="contract_no">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Contract File</label>
                                            <input type="file" class="form-control" name="contract_file"
                                                id="contract_file">
                                            {{-- <input type="file" class="w-75 form-control" id="editlogo" name="logo"
                                                 placeholder="logo" accept="logo/*" onchange="readURL(this);"> --}}
                                            <input type="hidden" name="hidden_image" class="hidden_image">
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ownership Doc<span class="tx-danger">*</span></label>
                                            <input type="file" class="form-control" name="ownership_doc"
                                                id="ownership_doc">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract Start Date<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker"
                                                name="contract_start_date" id="contract_start_date" autocomplete="off">

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract End Date<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control fc-datepicker" name="contract_end_date"
                                                id="contract_end_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Commission in terms of :-<span
                                                    class="tx-danger">*</span></label><br>
                                            <label>Amount</label>
                                            <input type="radio" class="commission_as" checked name="commision_as"
                                                id="commission_as" value="amount">
                                            <label>Percentage</label>
                                            <input type="radio" class="commission_as" name="commision_as"
                                                id="commission_as" value="percentage">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Commission<span class="tx-danger"
                                                    style="font-size:12px;">*</span></label>
                                            <input type="number" class="form-control" name="owner_amount"
                                                id="owner_amount" placeholder="Enter Commission">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Property Management fee:-<span
                                                    class="tx-danger">*</span></label><br>
                                            <label>Amount</label>
                                            <input type="radio" class="property_management_fee_type"
                                                name="property_management_fee_type" id="property_management_fee_type"
                                                value="amount" checked>
                                            <label>Percentage</label>
                                            <input type="radio" class="property_management_fee_type"
                                                name="property_management_fee_type" id="property_management_fee_type"
                                                value="percentage">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Management Fee<span class="tx-danger"
                                                    style="font-size:12px;">*</span></label>
                                            <input type="number" class="form-control" name="management_fee"
                                                id="management_fee" placeholder="Enter Management Fee">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Ads Number:-<span class="tx-danger">*</span></label><br>
                                            <input type="text" class="form-control" name="adsnumber" id="adsnumber"
                                                placeholder="Enter Ads Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Guard Number<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="guard_number" id="guard_number"
                                                placeholder="Enter Guard Number">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- end of tab4 -->
                            <!-- start tab 5 -->
                            <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Images <span class="tx-danger">*</span></label><br>
                                            <input id="images" name="images[]" type="file" class="form-control file"
                                                multiple data-show-upload="true" data-show-caption="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Floor Plans <span class="tx-danger">*</span></label><br>
                                            <input id="floor_plans" name="floor_plans[]" type="file"
                                                class="form-control file" multiple data-show-upload="true"
                                                data-show-caption="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Videos <span class="tx-danger"></span></label><br>
                                            <input id="videos" name="videos[]" type="file" class="form-control file"
                                                multiple data-show-upload="true" data-show-caption="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="location-prop" role="tabpanel"
                                aria-labelledby="location-prop-tab">
                                {{-- <div id="map-2"></div> --}}
                                <div>
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box" style="z-index:9999;">
                                </div>
                                <div class="container" id="map-canvas" style="height:300px;z-index:9995;"></div>
                                <div>
                                    <input type="hidden" id="lat-prop" name="lat_prop">
                                    <input type="hidden" id="lng-prop" name="lng_prop">
                                </div>
                            </div>
                        </div>
                        <div class="errorTxt shadow"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                                onclick="window.location.reload();"> Close </button>
                            {{-- <a href="{{route('addPropertyUnits')}}"  class="btn btn-primary addBtn" id="addPropertyBtn"  disabled>Submit</a> --}}
                            <button type="submit" class="btn btn-primary addBtn" id="addPropertyBtn"
                                disabled>Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>

    @endsection @section('scripts')

    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
    {{-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly"
    async
  ></script> --}}
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU"></script>

    {{-- <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly"
        async></script> --}}

    <script>
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                autoWidth: false,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            // $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
        });

        function multiplAgent(id) {
            $('#property_id').val(id);
            $('#agents').select2();
            $.ajax({
                url: url + "/propertyagent/list/" + id,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data.agent.length);
                    $('.agent-select').html('')
                    $('#addPropertyModal').modal();
                    var html = '<option value=""> Select</option>';
                    if (data.status === true) {
                        if (data.agent.length > 0) {
                            data.agent.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.agent-select').append(html)
                            $('#addPropertyModal').modal();

                        } else {
                            $('.agent-select').html('')
                        }
                    } else {
                        toastr["error"](data.response);
                    }
                }
            });
            return false
        }
        function EditPropertyUnits(id) {
                $.ajax({
                url:  "/edit-property-unit/" + id,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status === true) {
                        if (data.agent.length > 0) {

                        }
                    } else {
                        toastr["error"](data.response);
                    }
                }
            });
            return false
        }


        $('.edit-button').click(function() {
            var id = $(this).data('property');

            $('#propertyedit_id').val(id);
            $.ajax({
                url: url + "/property-edit-request/" + id,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status === true) {
                        console.log(data.response.property)
                        if (data.response.state.length > 0) {
                            var html = '<option value="">SELECT</option>';
                            $.each(data.response.state, function(index, value) {
                                html += '<option value="' + value.id + '">' + value.name +
                                    '</option>';
                            });
                            $('.state-select').append(html);
                        }
                        if (data.response.city.length > 0) {
                            var html = '<option value="">SELECT</option>';
                            $.each(data.response.city, function(index, value) {
                                html += '<option value="' + value.id + '">' + value.name +
                                    '</option>';
                            });
                            $('.city-select').append(html);
                        }
                        if (data.response.zipcode.length > 0) {
                            var html = '<option value="">SELECT</option>';
                            $.each(data.response.zipcode, function(index, value) {
                                html += '<option value="' + value.id + '">' + value.pincode +
                                    '</option>';
                            });
                            $('.zipcode-select').append(html);
                        }
                        alert(data.response.details_prop[0].value);
                        $('#myModalLabelProperty').text('Edit Property')
                        $('#category_id').val(data.response.property.category).trigger("change");
                        $('#type').val(data.response.property.type_id).trigger("change");
                        $('#ownerslist').val(data.response.property.owner_id).trigger("change");
                        $('#property_to').val(data.response.property.property_to).trigger("change");
                        $('#property_name').val(data.response.property.property_name);
                        $('#property_country').val('1').trigger("change");
                        //  $('.state-select').val('');
                        $('.state-select').val(data.response.property.state).trigger("change");

                        $(".city-select").val(data.response.property.city).trigger("change");
                        $('.zipcode-select').val(data.response.property.zip_code).trigger("change");
                        $('#address1').val(data.response.property.street_address_1);
                        $('#address2').val(data.response.property.street_address_2)
                        $('#is_builder').val(data.response.property.is_builder);
                        $('#unit_num').val(data.response.property.unit_num).trigger("change");

                        $('#floor_num').val(data.response.property.floor_num).trigger("change");
                        $('#furnished').val(data.response.property.furnished).trigger("change");
                        $('#frequency').val(data.response.property.frequency).trigger("change")
                        $('#min_rent').val(data.response.property.rent);
                        $('#security_deposit').val(data.response.property.security_deposit)
                        $('#min_sellingprice').val(data.response.property.selling_price);
                        $('#max_sellingprice').val(data.response.property.mrp)
                        $('#token_amount').val(data.response.property.token_amount);
                        $('#contract_no').val(data.response.property.contract_no);



                        if (data.response.property.is_builder == 1) {
                            $('.hide-element').show();
                            $('.amenities-row').show();
                            $('.is-building').hide();

                        } else {
                            $('.hide-element').hide();
                            $('.amenities-row').hide();
                            $('.is-building').show();
                        }
                        //  $.each(data.response.property.amenity_details.id,function(key,val){
                        //     $('#amenities_id_'+val).prop("checked",true);
                        //  });

                        $('#contract_start_date').val(data.response.property.contract_start_date);
                        $('#contract_end_date').val(data.response.property.contract_end_date)

                        if (data.response.property.commission_in == 'amount')
                            $('.commission_as[value="amount"]').prop('checked', true);
                        else
                            $('.commission_as[value="percentage"]').prop('checked', true);
                        //  $('#commission_as').val(data.response.property.commission_in);
                        $('#owner_amount').val(data.response.property.commission)
                        console.log(data.response.details_prop.value);
                        $('#det-3').val(data.response.details_prop[0].value);

                        //  $('#property_management_fee_type').val(data.response.property_management_fee_type);
                        if (data.response.property.property_management_fee_type == 'amount')
                            $('.property_management_fee_type[value="amount"]').prop('checked', true);
                        else
                            $('.property_management_fee_type[value="percentage"]').prop('checked',
                            true);
                        $('#management_fee').val(data.response.property.management_fee)
                        $('#adsnumber').val(data.response.property.ads_number);
                        $('#guard_number').val(data.response.property.guards_number)
                        $('#lat').val(data.response.property.latitude);
                        $('#lng').val(data.response.property.longitude);
                        $('#addPropertyModal_property').modal()


                    } else {
                        toastr["error"](data.response);
                    }
                }
            })
            return false
        });
    </script>


    <script>
        // for add service modal
        function addServiceFunction(id) {
            $('#service_property_id').val(id);
            $('#addServiceModal').modal();
        }

        // service modal submission
        $("#multiple-service-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                'service[]': {
                    required: true,
                },

            },
            submitHandler: function(form) {
                var form = document.getElementById("multiple-service-form");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addPropertyManageagentService') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>




    <script>
        const url = '{{ url('/') }}';
        $('.sellingdetails').hide()
        $('.hide-element').hide();
        $('.amenities-row').hide();
        $('.errorTxt').hide()
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id != 'location-tab') {
                $('#ownerSubmitBtn').attr('disabled', true)
            } else {
                $('#ownerSubmitBtn').attr('disabled', false)
            }
        })
        $('.propertynav').click(function() {
            var id = $(this).attr('id')
            if (id != 'location-prop-tab') {
                $('#addPropertyBtn').attr('disabled', true)
            } else {
                $('#addPropertyBtn').attr('disabled', false)
            }
        })


        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            container: '#contractUploadModal modal-body'
        });

        $('#property_to').change(function() {
            if ($('#property_to').val() == 1) {
                // buy -> 1 / rent->0
                $('.rentdetails').hide();
                $('.sellingdetails').show()

            } else {
                $('.sellingdetails').hide()
                $('.rentdetails').show();
            }
        });
        $(document).on("click", "#is_builder", function() {

            var check_val = $(this).prop("checked");
            if (check_val) {
                $('.hide-element').show();
                $('.amenities-row').show();
                $('.is-building').hide();
            } else {
                $('.hide-element').hide();
                $('.amenities-row').hide();
                $('.is-building').show();
            }
        });
    </script>



    <script>
        $('#property_country').change(function() {
            var countryId = $(this).val()
            //  alert(countryId);
            $.ajax({
                url: url + "/get-states/" + countryId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.state-select').html('')
                    var html = '<option value=""></option>';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.state-select').append(html);

                        } else {
                            $('.state-select').html('')
                        }
                    } else {
                        $('.state-select').html('')
                    }
                }
            });
            return false
        });
        $('.state-select').change(function() {
            var stateId = $(this).val()
            $.ajax({
                url: url + "/get-cities/" + stateId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.city-select').html('')
                    var html = '<option value=""></option>';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.name + `</option>`
                            })
                            $('.city-select').append(html);

                        } else {
                            $('.city-select').html('')
                        }
                    } else {
                        $('.city-select').html('')
                    }
                }
            });
            return false
        });
    </script>

    <script>

        $('#filtercategory').change(function() {
            var categoryId = $(this).val();
            console.log(categoryId);
            categoryTypeFilter(categoryId);
        });


        $(document).ready(function() {
            categoryTypeFilter(1);
        });

        function categoryTypeFilter(categoryId) {
            $.ajax({
                url: url + "/get-type/" + categoryId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    $('.category-select').html('')
                    var html = '<option value=""></option>';
                    if (data.status === true) {

                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.type + `</option>`
                            })
                            $('.category-select').append(html)
                        } else {
                            $('.v-select').html('')
                        }
                    } else {
                        $('.category-select').html('')
                    }
                }
            })
            return false
        }
    </script>
    <script>
        $('.city-select').change(function() {
            var cityId = $(this).val()
            if (cityId != null) {
                $.ajax({
                    url: url + "/get-pincodes/" + cityId,
                    type: "GET",
                    processData: false,
                    async: true,
                    header: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        $('.zipcode-select').html('')
                        var html = '<option value=""></option>';
                        if (data.status === true) {
                            if (data.response.length > 0) {
                                data.response.forEach((val, key) => {
                                    html += `<option value=` + val.id + `>` + val.pincode +
                                        `</option>`
                                })
                                $('.zipcode-select').append(html)
                            } else {
                                $('.zipcode-select').html('')
                            }
                        } else {
                            $('.zipcode-select').html('')
                        }
                    }
                });
            }
            return false
        })
        $('#type').change(function() {
            var id = $(this).val()
            $.ajax({
                url: url + "/owner/available-amenities/" + id,
                method: 'GET',
                async: true,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == true) {
                        $('#amenties_available').html('')
                        $('#amenties_available').append(data.response)
                    } else {
                        toastr['error'](data.response)
                    }
                },
            })
            return false
        })



        $('#form-add-property').validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                property_name: {
                    required: true,
                },
                property_to: {
                    required: true,
                },
                category: {
                    required: true
                },
                property_country: {
                    required: true,
                },
                property_state: {
                    required: true,
                },
                property_city: {
                    required: true,
                },
                property_zipcode: {
                    required: true,
                },
                address1: {
                    required: true,
                },
                // furnished : {
                //     required : true,
                // },
                is_builder: {
                    required: function() {
                        if ($('#is_builder').prop(":checked")) {
                            return true;

                        }
                        return false
                    },
                },

                contract_no: {
                    required: true,
                },

                contract_file: {
                    required: true,
                },

                contract_start_date: {
                    required: true,
                },

                contract_end_date: {
                    required: true,
                },

            },
            messages: {
                property_name: "Property Name is required",
                property_to: "Property to is required",
                category: "Category is required",
                property_country: "Country is required",
                property_state: "State is required",
                property_city: "City is required",
                property_zipcode: "Zipcode is required",
                address1: "Address is required",
                frequency: "Frequency is required",
                contract_no: 'Contract number is required',
                contract_file: 'Contract file is required',
                contract_start_date: 'Contract start date is required',
                contract_end_date: 'Contract end date is required',

            },
            errorElement: 'div',
            errorLabelContainer: '.errorTxt',
            showErrors: function(errorMap, errorList) {
                if (!this.numberOfInvalids()) {
                    $('.errorTxt').hide()
                } else {
                    $('.errorTxt').show()
                }
                this.defaultShowErrors();
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-property");
                var formData = new FormData(form);
                console.log(formData);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ url('/property/management/add-property') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        console.log(data);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            if (data.response_data) {
                                if (data.response_data.property_id) {
                                    var responseId = data.response_data.property_id;
                                    if ($('#is_builder').prop(":checked")) {
                                        setTimeout(function() {
                                            window.location.href = "";
                                        }, 1000);
                                    } else {
                                        setTimeout(function() {
                                            window.location.href = "/add-property-unit/" +
                                                responseId;
                                        }, 1000);
                                    }
                                }
                            } else {
                                setTimeout(function() {
                                    window.location.href = "";
                                }, 1000);
                            }
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

        $('#type').change(function() {

            var id = $(this).val()
            $.ajax({
                url: url + "/owner/details-on-type-change/" + id,
                method: 'GET',
                async: true,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == true) {
                        var html = ''
                        data.response.forEach((value, key) => {
                            html += ` <div class="col-md-4">
                               <div class="form-group">
                                   <label>` + value.name + `<span class="tx-danger">*</span></label>
                                   <input type="text" class="form-control" name="detailsdata[` + value.detail_id +
                                `]" id="det-` + value.detail_id + `" placeholder=` + value
                                .placeholder + `>
                               </div>
                           </div>`
                        })
                        $('#details-section').html('')
                        $('#details-section').append(html)

                    } else {
                        toastr['error'](data.response)
                    }
                },
            })
            return false
        });
        // agent modal submittion
        $("#multiple-agent-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                'agents[]': {
                    required: true,
                },
                date: {
                    required: true,
                },
                fromtime: {
                    required: true,
                }

            },
            submitHandler: function(form) {
                var form = document.getElementById("multiple-agent-form");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addAgentinProperty') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        alert('ds')
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                    error: function(xhr, data) {
                        console.log(xhr);
                    }
                });
            },
        });


        function blockFunction(id, action) {
            $('#propertyId').val(id);
            $('#action').val(action);
            // alert(action);
            if (action == 1) {
                $('.reject-modal-title').text('Unblock Property');
                $('.deletetBtn').text('Unblock');
            }
            $('#rejectModal').modal()
        }
        $('.deletetBtn').on('click', function() {
            var id = $('#propertyId').val()
            var action = $('#action').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/property/block/" + id + '/' + action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    if (action == 0) {
                        $(".deletetBtn").text('Block');
                    } else {
                        $(".deletetBtn").text('Unblock');
                    }
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
        });
    </script>
@endsection
