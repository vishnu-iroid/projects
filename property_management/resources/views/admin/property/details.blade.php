@extends('admin.layout.app')
 <style>
    .card {
        position: relative; display: flex; flex-direction: column; min-width: 0;
        word-wrap: break-word; background-color: #fff; background-clip: border-box; border: 1px solid #eee; border-radius: .25rem;
    }
    .h4, h4 { font-size: 1.125rem; font-weight: 300 !important; }
    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00) !important ; height : 20px !important ;
        border-radius: 5px  !important ;
    }
    .card-warning-2 {
        background: linear-gradient(60deg, #ffa726, #fb8c00) !important ; height : 20px !important ;
        border-radius: 5px  !important ;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Master</span>
        <span class="breadcrumb-item active">Details</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
<div class="row">
    <div class="col-md-3">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-12">
                        <h2 class="card-title" style="font-size: 1.125rem;font-weight: 300 !important;">Add Detail</h2>
                        <!-- <p class="card-category"></p> -->
                    </div>
                </div>
        </div>
        <div class="card-body">
            <form class="form-detail" id="form-add" action="javascript:;">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Name <span class="tx-danger">*</span></label>
                        <input class="form-control" id="id" name="id" type="hidden"/>
                        <input class="form-control" id="name" name="name" type="text" placeholder="Enter detail name"/>
                    </div>
                    <div class="form-group">
                        <label>Placeholder<span class="tx-danger">*</span></label>
                        <input class="form-control" id="placeholder" name="placeholder" type="text" placeholder="Enter placeholder"/>
                    </div>
                    <div class="form-group">
                        <label>Icon <span class="tx-danger">*</span></label>
                        <div class="form-group image-class" style="display: none;">
                            <img class="img-thumbnail image-display" src="#" alt="agent" width="100">
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="icon" name="icon">
                            <label class="custom-file-label" id="file_label">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Sort Order<span class="tx-danger">*</span></label>
                        <input class="form-control" id="sortorder" name="sortorder" type="text" placeholder="Enter sort order"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary addBtn">Submit</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <div class="col-md-9">
    <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title">Details</h4>
                        <p class="card-category">List of all detail</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Placeholder</th>
                            <th class="wd-15p">Sort order</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($details as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->placeholder }}</td>
                            <td>{{ $row->sort_order }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-detail="{{$row->id}}" title="Edit"></a>
                                <!-- <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deleteFunction('{{$row->id}}')"></a> -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                        {!! $details->links() !!}
                </div>
            </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
</div>
</div>
<!-- br-pagebody -->

@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Details`);
    $("#property_nav").addClass("active");
    $("#property_detail_nav").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>

    $('tbody').on('click','.edit-button',function(){
        var typeId = $(this).data('detail')
        $.ajax({
            url : url+"/details/view/"+typeId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                    // $('#myModalLabel').html('Edit Client')
                    $('#id').val(data.response.id)
                    $('#name').val(data.response.name)
                    $('#placeholder').val(data.response.placeholder)
                    $('#sortorder').val(data.response.sort_order)
                    $('.image-class').css('display','block')
                    $('.image-display').prop('src',url+data.response.icon)
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $("#form-add").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            name: {
                required: true,
            },
            placeholder: {
                required: true,
            },
            sortorder: {
                required: true,
            },
            icon: {
                required: function(){
                    if($('#id').val()){
                        return false
                    }
                    return true
                }
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addDetail')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    if (data.status === true) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
