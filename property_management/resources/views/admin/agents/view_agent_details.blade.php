@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>

@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Agent Management</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Agents</h4>
                        <p class="card-category">List of all agents</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAgent</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link modalnav active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a id="bank-tab" class="nav-link modalnav" data-toggle="tab" href="#bank" role="tab" aria-controls="bank" aria-selected="false">Bank Details</a>
                        </li>
                        <li class="nav-item">
                            <a id="location-tab" class="nav-link modalnav" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Location</a>
                        </li>
                    </ul>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name <span class="tx-danger">*</span></label>
                                        <input type="hidden" id="id" name="id">
                                        <input class="form-control" id="name" name="name" type="text" value="{{ $agents->name }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="email" name="email" type="text" value="{{ $agents->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="phone" name="phone" type="text" value="{{ $agents->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="status" id="status" value="{{ $agents->status }}">
                                            <option value="0">Inactive</option>
                                            <option value="1" selected>Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Country<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="country" id="country">
                                            <option value=""></option>
                                            @foreach($country as $con)
                                            <option value="{{$con->id}}" @if($con->id == $agents->country) selected @endif> {{$con->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>State<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 state-select" name="state" id="state">
                                            <option value=""></option>
                                            @foreach($state as $st)
                                            <option value="{{$con->id}}" @if($st->id == $agents->state) selected @endif>{{$st->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 city-select" name="city" id="city">
                                            <option value=""></option>
                                            @foreach($city as $cities)
                                            <option value="{{ $cities->id }}" @if($cities->id == $agents->city) selected @endif>{{ $cities->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Zipcode<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 zipcode-select" name="zipcode" id="zipcode">
                                            <option value=""></option>
                                            @foreach($pincode as $pin)
                                            <option value="{{ $pin->id }}" @if($pin->id == $agents->zip_code) selected @endif>{{ $pin->pincode }}</option>
                                            @endforeach
                                        </select>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Address <span class="tx-danger">*</span></label>
                                <textarea class="form-control" id="address" name="address" type="text">{{ $agents->address }}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Image <span class="tx-danger">*</span></label>
                                        <div class="form-group image-class">
                                            <img class="img-thumbnail image-display" src="{{ asset('uploads/'.$agents->image) }}" alt="agent" width="100">
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                            <label class="custom-file-label" id="file_label">Choose file</label>
                                            <input type="hidden" id="current_image" name="current_image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab1-end -->
                        <!-- tab-2 start -->
                        <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bank Name<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="bank_name" id="bank_name" value="{{ $agents->bank_name }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Branch Name<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="{{ $agents->branch_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Account Number<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="account_number" id="account_number" value="{{ $agents->account_number }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>IFSC<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="ifsc" id="ifsc" value="{{ $agents->ifsc }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-2 end -->
                        <!-- tab-3 start -->
                        <div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="location-tab">
                            <div id="map"></div>
                            <div>
                                <input type="hidden" id="lat" name="lat" value="{{ $agents->lat }}">
                                <input type="hidden" id="lng" name="lng" value="{{ $agents->lng }}">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- br-pagebody -->
@endsection
@section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
</script>
<script>
    function initMap() {
        const uluru = {
            lat: 24.466667,
            lng: 54.366669
        };
        if ($('#lat').val() != "" && $('#lng').val() != "") {
            const uluru = {
                lat: $('#lat').val(),
                lng: $('#lng').val()
            };
        }
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            center: uluru,
        });
        const marker = new google.maps.Marker({
            position: uluru,
            map: map,
            draggable: true
        });
        navigator.geolocation.getCurrentPosition(
            function(position) { // success cb
                if ($('#lat').val() == "" && $('#lng').val() == "") {
                    $('#lat').val(parseFloat(position.coords.latitude))
                    $('#lng').val(parseFloat(position.coords.longitude))
                }
                var lat = $('#lat').val()
                var lng = $('#lng').val()
                map.setCenter(new google.maps.LatLng(lat, lng));
                var latlng = new google.maps.LatLng(lat, lng);
                marker.setPosition(latlng);
            },
            function() {}
        );
        google.maps.event.addListener(marker, 'dragend', function() {
            $('#lat').val(marker.position.lat())
            $('#lng').val(marker.position.lng())
        })
    }
    initMap();
</script>
@endsection
