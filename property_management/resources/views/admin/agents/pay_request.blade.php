@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Accounts Management</span>
        <span class="breadcrumb-item active">Agent Commission Requests</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Agent Commission Requests</h4>
                        <p class="card-category">List of requests</p>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end">
                        <a href="{{ route('showAgentCashPayment',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center @if($status == 0 ) {{ 'card-active' }}  @endif ">
                            <i class="fad fa-align-justify tx-20"></i>&nbspRequests
                        </a>
                        <a href="{{ route('showAgentCashPayment',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center @if($status == 1) {{ 'card-active' }}  @endif  ">
                            <i class="fad fa-check-circle tx-20"></i>&nbspAccepted
                        </a>
                        <a href="{{ route('showAgentCashPayment',['status' => 3]) }}" class="nav-link tx-white d-flex align-items-center @if($status == 3) {{ 'card-active' }}  @endif ">
                            <i class="fad fa-recycle tx-20"></i>&nbspRevised
                        </a>
                        <a href="{{ route('showAgentCashPayment',['status' => 2]) }}" class="nav-link tx-white d-flex align-items-center @if($status == 2) {{ 'card-active' }}  @endif ">
                            <i class="fad fa-ban tx-20"></i>&nbspRejected
                        </a>

                    </div>

                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table table-striped responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-15p">Agent</th>
                            <th class="wd-15p">Contact No</th>
                            <th class="wd-15p">Target Amount</th>
                            <th class="wd-15p">Amount</th>
                            <th class="wd-15p">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cash as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->agent_rel->name}}</td>
                            <td>{{$row->agent_rel->phone}}</td>
                            <td>{{number_format($row->agent_rel->target_amount,2)}}</td>
                            <td>{{$row->amount}}</td>
                            <td>
                                @if($row->status == 0)
                                    <a class="fad fa-check  tx-20 accept-button pr-2 btn btn-primary mb-1 bt_cus mr-1" href="javascript:0;" data-property="" title="Accept" onclick="AcceptFunction('{{$row->agent_id}}','{{$row->id}}')"></a>
                                    <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus  btn btn-primary mb-1 bt_cus mr-1" href="javascript:0;" data-admin="" title="Cancel"  onclick="RejectFunction('{{$row->id}}')"></a>
                                    <a class="fad fa-recycle tx-20 revise-button btn btn-primary bt_cus  btn btn-primary mb-1 bt_cus" href="javascript:0;" data-admin="" title="Revise"  onclick="reviseFunction('{{$row->id}}')"></a>
                                    @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $cash->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<!-- Accept Modal -->
<div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-success reject-modal-title">Accept Pay Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure to want to approve the request?
                    <input type="hidden" name="agentId" id="agentId">
                    <input type="hidden" name="id" id="id">
                    <!--Action -  1-accept , 0- reject -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-success reject-class AcceptBtn">Accept</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Reject Pay Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to Cancel  ?
                    <input type="hidden" name="Id" id="Id">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class RejectBtn">Reject</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reject Modal -->
<div class="modal fade" id="reviseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-primary reject-modal-title">Revise Pay Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to Revise  ?
                    <input type="hidden" name="reviseId" id="reviseId">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-primary reject-class ReviseBtn">Revise</button>
                </div>
            </form>
        </div>
    </div>
</div>





@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Agent Management`);
    $("#agent_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    function AcceptFunction(id, action) {
        $('#agentId').val(id);
        $('#id').val(action);
        $('#acceptModal').modal()
    }

    $('.AcceptBtn').click(function() {
        var id = $('#agentId').val()
        var action = $('#id').val()
        $(".AcceptBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/agent_pay_request/accept/" + id + '/' + action,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".AcceptBtn").text('Processing..');
            },
            success: function(data) {
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    function reviseFunction($id)
    {
        $('#reviseId').val($id);
        $('#reviseModal').modal();

    }

    $('.ReviseBtn').click(function() {
        var id = $('#reviseId').val()

        $(".ReviseBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/agent_pay_request/revise/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".AcceptBtn").text('Processing..');
            },
            success: function(data) {
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    });



    function RejectFunction(id) {

        $('#Id').val(id);

        $('#rejectModal').modal();
    }



    $('.rejectBtn').click(function() {
        var id = $('#Id').val()
        var action = $('#id').val()
        $(".RejectBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/agent_pay_request/reject/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".AcceptBtn").text('Processing..');
            },
            success: function(data) {
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    });
    $(function() {
        "use strict";
        $("#datatable2").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>


@endsection
