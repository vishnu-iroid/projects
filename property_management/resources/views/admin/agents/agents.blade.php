@extends('admin.layout.app')
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .pac-container{
        z-index:9999;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 15px 0 13px;
    text-overflow: ellipsis;
    width: 400px;
    }

    #pac-container {
    padding-bottom: 12px;
    margin-right: 12px;
    }

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Sales</span>
        <span class="breadcrumb-item active">Sale Agent</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Sale Agent</h4>
                        <p class="card-category">List of all agents</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-20"></i>&nbspAgent</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Phone</th>
                            <th class="wd-20p">Email</th>
                            <th class="wd-20p">Status</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($agents as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->phone}}</td>
                            <td>{{$row->email}}</td>
                            <td>
                                @if($row->status == 0)
                                <span class="badge rounded-pill bg-danger text-white"> Blocked </span>
                                @else
                                <span class="badge rounded-pill bg-primary text-white"> Active </span>
                                @endif
                            </td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary mb-1 bt_cus" href="javascript:0;" data-toggle="modal" data-target="#addModal" data-admin="{{$row->id}}" title="Edit"></a>
                                <a class="fad fa-trash tx-20 delete-button btn btn-danger bt_cus mb-1" href="javascript:0;" data-admin="{{$row->id}}" title="View"></a>
                                <a href='{{ url("agent/details",$row->id) }}' class="fad fa-eye tx-20  btn btn-primary mb-1 bt_cus " href="javascript:0;" title="View"></a>
                                @if($row->status == 0)
                                <a class="fad fa-unlock-alt tx-20 reject-button" href="javascript:0;" data-admin="" title="Unblock" onclick="blockFunction('{{$row->id}}','1')">
                                </a>
                                @else
                                <a class="fad fa-ban tx-20 reject-button btn btn-primary bt_cus  btn btn-primary mb-1 bt_cus" href="javascript:0;" data-admin="" title="Block" onclick="blockFunction('{{$row->id}}','0')">
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $agents->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<!-- Add and Edit Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center tx-primary" id="myModalLabel">Add Agent</h4>
            </div>
            <div class="col-md-12">
                <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link modalnav active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a id="bank-tab" class="nav-link modalnav" data-toggle="tab" href="#bank" role="tab" aria-controls="bank" aria-selected="false">Bank Details</a>
                    </li>
                    <li class="nav-item">
                        <a id="location-tab" class="nav-link modalnav" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Location</a>
                    </li>
                </ul>
            </div>
            <form class="form-admin" id="form-add" action="javascript:;">
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name <span class="tx-danger">*</span></label>
                                        <input type="hidden" id="id" name="id">
                                        <input class="form-control" id="name" name="name" type="text" placeholder="Enter first name" value="@if(isset($agents->name)){{ $agents->name }} @endif" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="email" name="email" type="text" placeholder="Enter email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="phone" name="phone" type="text" placeholder="Enter phone">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="status" id="status">
                                            <option value="0">Inactive</option>
                                            <option value="1" selected>Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Country<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="country" id="country">
                                            <option value=""></option>
                                            @foreach($country as $con)
                                            <option value="{{$con->id}}">{{$con->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>State<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 state-select" name="state" id="state">
                                            <option value=""></option>
                                            @foreach($state as $st)
                                            <option value="{{$st->id}}">{{$st->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 city-select" name="city" id="city">
                                            <option value=""></option>
                                            @foreach($city as $cities)
                                            <option value="{{ $cities->id }}">{{ $cities->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Zipcode<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 zipcode-select" name="zipcode" id="zipcode">
                                            <option value=""></option>
                                            @foreach($pincode as $pin)
                                            <option value="{{ $pin->id }}">{{ $pin->pincode }}</option>
                                            @endforeach
                                        </select>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Address <span class="tx-danger">*</span></label>
                                <textarea class="form-control" id="address" name="address" type="text" placeholder="Enter address"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Image <span class="tx-danger">*</span></label>
                                        <div class="form-group image-class" style="display: none;">
                                            <img class="img-thumbnail image-display" src="#" alt="agent" width="100">
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                            <label class="custom-file-label" id="file_label">Choose file</label>
                                            <input type="hidden" id="current_image" name="current_image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Target <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="target" name="target" type="number" placeholder="Enter Target Amount">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab1-end -->
                        <!-- tab-2 start -->
                        <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bank Name<span class="tx-danger">*</span></label>
                                        <select name="bank_name" class="form-control select2" id="bank_name">
                                            @foreach ($bank as $row )
                                                <option value="{{ $row->id }}">{{ $row->bank_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Branch Name<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" placeholder="Enter branch name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Account Number<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="account_number" id="account_number" placeholder="Enter Account number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>IBAN<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="ifsc" id="ifsc" placeholder="Enter IBAN code">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-2 end -->
                        <!-- tab-3 start -->
                        <div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="location-tab">
                            <div>
                                <input id="pac-input" class="controls" type="text" placeholder="Search Box" style="z-index:9999;">
                            </div>
                            <div class="container" id="map-canvas" style="height:300px;z-index:9995;"></div>

                            <div>
                                <input type="hidden" id="lat" name="lat">
                                <input type="hidden" id="lng" name="lng">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();"> Close </button>
                    <button type="submit" class="btn btn-primary addBtn" id="propAddBtn" disabled>Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add and Edit Modal -->
<!-- View modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabe2" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center tx-primary" id="myModalLabe2">Add Agent</h4>
            </div>
            <div class="col-md-12">
                <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link modalnav active" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a id="bank-tab" class="nav-link modalnav" data-toggle="tab" href="#bank2" role="tab" aria-controls="bank" aria-selected="false">Bank Details</a>
                    </li>
                    <li class="nav-item">
                        <a id="location-tab" class="nav-link modalnav" data-toggle="tab" href="#location1" role="tab" aria-controls="location" aria-selected="false">Location</a>
                    </li>
                </ul>
            </div>
            <form class="form-admin" id="form-add" action="javascript:;">
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name <span class="tx-danger">*</span></label>
                                        <input type="hidden" id="id" name="id">
                                        <input class="form-control" id="view_name" name="name" type="text" placeholder="Enter first name" value="@if(isset($agents->name)){{ $agents->name }} @endif" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="view_email" name="email" type="text" placeholder="Enter email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="view_phone" name="phone" type="text" placeholder="Enter phone">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="status" id="view_status">
                                            <option value="0">Inactive</option>
                                            <option value="1" selected>Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Country<span class="tx-danger">*</span></label>
                                        <select class="form-control select2" name="country" id="view_country">
                                            <option value=""></option>
                                            @foreach($country as $con)
                                            <option value="{{$con->id}}">{{$con->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>State<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 state-select" name="state" id="view_state">
                                            <option value=""></option>
                                            @foreach($state as $st)
                                            <option value="{{$con->id}}">{{$st->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>City<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 city-select" name="city" id="view_city">
                                            <option value=""></option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Zipcode<span class="tx-danger">*</span></label>
                                        <select class="form-control select2 zipcode-select" name="zipcode" id="view_zipcode">
                                            <option value=""></option>
                                        </select>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Address <span class="tx-danger">*</span></label>
                                <textarea class="form-control" id="view_address" name="address" type="text" placeholder="Enter address"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Image <span class="tx-danger">*</span></label>
                                        <div class="form-group image-class" style="display: none;">
                                            <img class="img-thumbnail image-display" src="#" alt="agent" width="100">
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="view_image" name="image">
                                            <label class="custom-file-label" id="file_label">Choose file</label>
                                            <input type="hidden" id="view_current_image" name="current_image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Target <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="target" name="target" type="number" placeholder="Enter Target Amount">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab1-end -->
                        <!-- tab-2 start -->
                        <div class="tab-pane fade" id="bank2" role="tabpanel" aria-labelledby="bank-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bank Name<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="bank_name" id="view_bank_name" placeholder="Enter bank name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Branch Name<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="branch_name" id="view_branch_name" placeholder="Enter branch name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Account Number<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="account_number" id="view_account_number" placeholder="Enter Account number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>IBAN<span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="ifsc" id="view_ifsc" placeholder="Enter ifsc code">

                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- tab-2 end -->
                        <!-- tab-3 start -->
                        <div class="tab-pane fade" id="location1" role="tabpanel" aria-labelledby="location-tab">
                                <input type="hidden" id="view_lat" name="lat">
                                <input type="hidden" id="view_lng" name="lng">
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End View modal -->

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Block Agent</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to block this agent?
                    <input type="hidden" name="agentId" id="agentId">
                    <input type="hidden" name="action" id="action">
                    <!--Action -  1-unblock , 0- block -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class deletetBtn">Block</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU"></script>

<script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>

 <script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>

<script>
    $("#site_title").html(`Property | Agent Management`);
    $("#agent_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
                    autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    $(document).ready(function(){

        google.maps.event.addDomListener(window, 'load', init);

        })

        function init() {
                var map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: {
                        lat: 12.9715987,
                        lng: 77.59456269999998
                    },
                zoom: 12
                });

            var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
            google.maps.event.addListener(searchBox, 'places_changed', function() {
            searchBox.set('map', null);
            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;
            for (i = 0; place = places[i]; i++) {
                (function(place) {
                var marker = new google.maps.Marker({

                    position: place.geometry.location
                });
                marker.bindTo('map', searchBox, 'map');
                google.maps.event.addListener(marker, 'map_changed', function() {
                    if (!this.getMap()) {
                    this.unbindAll();
                    }
                });
                bounds.extend(place.geometry.location);
                }(place));

            }
            map.fitBounds(bounds);
            searchBox.set('map', map);
            map.setZoom(Math.min(map.getZoom(),12));

            });
        }



</script>

<script>
    function blockFunction(id, action) {
        $('#agentId').val(id);
        $('#action').val(action);
        if (action == 1) {
            $('.reject-modal-title').text('Unblock Admin');
            $('.deletetBtn').text('Unblock');
        }
        $('#rejectModal').modal()
    }



</script>

<script>
    $('.deletetBtn').click(function() {
        var id = $('#agentId').val()
        var action = $('#action').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/agent/block/" + id + '/' + action,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".addBtn").text('Processing..');
            },
            success: function(data) {
                if (action == 0) {
                    $(".deletetBtn").text('Block');
                } else {
                    $(".deletetBtn").text('Unblock');
                }
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    //Edit
    $('.edit-button').click(function() {
        var adminid = $(this).data('admin')
        $.ajax({
            url: url + "/agent/view/" + adminid,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status === true) {
                    $('#myModalLabel').text('Edit Agent')
                    $('#id').val(data.response.id);
                    $('#name').val(data.response.name);
                    $('.image-display').attr('src', url + data.response.image);
                    $('.image-class').css('display', 'block');
                    $('#current_image').val(data.response.image)
                    $('#country').val(data.response.country).trigger("change");
                    $('#state').val(data.response.state).trigger("change");
                    setTimeout(function() {
                        $('#city').val(data.response.city).trigger("change");
                        setTimeout(function() {
                            $('#zipcode').val(data.response.zip_code).trigger("change");
                        }, 500)
                    }, 200);
                    $('#address').val(data.response.address);
                    $('#email').val(data.response.email);
                    $('#phone').val(data.response.phone);
                    $('#status').val(data.response.status).trigger("change");
                    $('#bank_name').val(data.response.bank_name);
                    $('#branch_name').val(data.response.branch_name);
                    $('#account_number').val(data.response.account_number);
                    $('#ifsc').val(data.response.ifsc);
                    $('#lat').val(data.response.latitude);
                    $('#lng').val(data.response.longitude);
                    $('#target').val(data.response.target_amount);
                    initMap()
                    $('#addModal').modal()
                } else {
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })
    //View
    $('.view-button').click(function() {
        var adminid = $(this).data('admin')
        $.ajax({
            url: url + "/agent/view/" + adminid,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status === true) {
                    $('#myModalLabe2').text('View Agent')
                    $('#id').val(data.response.id);
                    $('#view_name').val(data.response.name);
                    $('.view_image-display').attr('src', url + data.response.image);
                    $('.view_image-class').css('display', 'block');
                    $('#view_current_image').val(data.response.image)
                    $('#view_country').val(data.response.country).trigger("change");
                    $('#view_state').val(data.response.state).trigger("change");
                    setTimeout(function() {
                        $('#view_city').val(data.response.city).trigger("change");
                        setTimeout(function() {
                            $('#view_zipcode').val(data.response.zip_code).trigger("change");
                        }, 500)
                    }, 200);
                    $('#view_address').val(data.response.address);
                    $('#view_email').val(data.response.email);
                    $('#view_phone').val(data.response.phone);
                    $('#view_status').val(data.response.status).trigger("change");
                    $('#view_bank_name').val(data.response.bank_name);
                    $('#view_branch_name').val(data.response.branch_name);
                    $('#view_account_number').val(data.response.account_number);
                    $('#view_ifsc').val(data.response.ifsc);
                    $('#view_lat').val(data.response.latitude);
                    $('#view_lng').val(data.response.longitude);
                    view_initMap()
                    $('#viewModal').modal()
                } else {
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })


    $("#form-add").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            name: {
                required: true,
            },
            country: {
                required: true,
            },
            state: {
                required: true,
            },
            city: {
                required: true,
            },
            image: {
                required: function() {
                    if ($('#id').val()) {
                        return false
                    }
                    return true
                },
            },
            zipcode: {
                required: true,
            },
            address: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
                maxlength: 10,
            },
            bank_name: {
                required: true,
            },
            branch_name: {
                required: true,
            },
            account_number: {
                required: true,
            },
            ifsc: {
                required: true,
            },
            password: {
                required: function() {
                    if ($('#id').val()) {
                        return false
                    }
                    return true
                }
            },
            status: {
                required: true,
            },
            // lat: {
            //     required: true,
            // },
            // lng: {
            //     required: true,
            // },
        },
        messages: {
            // lat: "Latitude is required",
            // lng: "Longitude is required",
        },
        submitHandler: function(form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addAgent')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
<script>
    $('.state-select').change(function() {
        var stateId = $(this).val();
        $.ajax({
            url: url + "/get-cities/" + stateId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $('.city-select').html('')
                var html = '<option value=""></option>';
                if (data.status === true) {
                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.name + `</option>`
                        })
                        $('.city-select').append(html)
                    } else {
                        $('.city-select').html('')
                        $('.zipcode-select').html('')
                    }
                } else {
                    $('.city-select').html('')
                    $('.zipcode-select').html('')
                }
            }
        })
        return false
    })
</script>
<script>
    $('.city-select').change(function() {
        var cityId = $(this).val()
        $.ajax({
            url: url + "/get-pincodes/" + cityId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $('.zipcode-select').html('')
                var html = '<option value=""></option>';
                if (data.status === true) {
                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.pincode + `</option>`
                        })
                        $('.zipcode-select').append(html)
                    } else {
                        $('.zipcode-select').html('')
                    }
                } else {
                    $('.zipcode-select').html('')
                }
            }
        })
        return false
    })
</script>
<script>

</script>
@endsection
