@extends('admin.layout.app')

<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
.check-title{
    background: linear-gradient(
60deg , #099840, #0e9642);
    padding: 10px;
    border-radius: 5px;
    color: #fff;
}
.check-tab {
    padding: 11px;
    font-size: 18px !important;
    border: transparent;
    border-radius: 5px;
    width: 100%;
}
i.fas.fa-chevron-down {
    /* color: aqua; */
    position: absolute;
    /* left: 0; */
    right: 30px;
}

.card{
    padding: 20px;
}



.check-bd{
    background-color: #000000;
    height: 20px;
}
    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 500 !important;
    }

    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height: 20px;
        border-radius: 5px !important
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding: 10px;
        border-radius: 10px;
    }

    .error {
        color: red !important;
    }

    .ui-datepicker {
        position: fixed;
        top: 270.797px;
        left: 172.5px;
        z-index: 5001 !important;
        display: block;
    }

    #ui-datepicker-div {
        z-index: 5001 !important;
    }

</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <a class="breadcrumb-item" href="">Roles</a>
        <span class="breadcrumb-item active">Create Roles</span>
    </nav>
</div>


<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">User Permission</h4>
                        <p class="card-category">Add User Permission</p>
                    </div>

                    <div class="pd-5 mg-r-20">
                        <a href="{{ route('roles.saveAdminPerimission') }}" class="nav-link card-active tx-white d-flex align-items-center"
                           ><i class="fad fa-plus-hexagon tx-20"></i>Back To Lists</a>
                    </div>


                </div>
            </div>
            <div class="br-pagebody">
                @if (session('success'))
                    <div class="col-sm-12">
                        <div class="alert  alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    </div>
                @endif
                @if (isset($error))
                    <div class="col-sm-12">
                        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                        {{$error}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <form method="POST" action="{{route('roles.saveAdminPerimission')}}" accept-charset="UTF-8" id="role_add_form">
                    @csrf
                    <input type="hidden" name="admin_id" value="{{ $id }}">
                    <div class="box-body">

                        <div class="row">
                            {{-- <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">ROLE NAME:*</label>
                                    <input class="form-control" required placeholder="Role Name" name="name" type="text" id="name" autocomplete="off">
                                </div>
                            </div> --}}
                            <br>
                            {{-- <div class="col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>PERMISSION</th>
                                            <th>CHECK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (!empty($permission))
                                            @foreach ($permission as $v)
                                                <tr>
                                                    {{-- <td>{{$v['name']}}</td>
                                                    <td><input class="input-icheck" name="permissions[]" type="checkbox" value="{{$v['name']}}"></td> -}}

                                                </tr>
                                            @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div> --}}
                        </div>

                         {{-- DASHBOARD --}}

                            {{-- <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#demo">Dashboard  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="demo" class="collapse">
                                    <div class="check_group">
                                        <div class="checkbox mt-3 container">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="ViewDashboard">
                                                                VIEW
                                                                </label>
                                                    </div>
                                        </div>
                                        <br>
                                    </div>

                                </div>
                            </div> --}}

                             {{--------- EMPLOYMENT MANAGEMENT ------}}

                             <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#demofive">Employee Management  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="demofive" class="collapse">
                                    <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                <div class="checkbox pl-5">
                                                        <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="View Employmemnt Management"> &nbsp;&nbsp;VIEW
                                                        </label>
                                                </div>

                                                <div class="checkbox pl-5">
                                                    <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="AddEmploymentManagement">&nbsp;&nbsp;ADD
                                                    </label>
                                                </div>

                                                <div class="checkbox pl-5">
                                                    <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="EditEmploymentManagement">&nbsp;&nbsp;EDIT
                                                    </label>
                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="deleteEmploymentManagement">&nbsp;&nbsp;DELETE
                                                    </label>
                                                </div>
                                        </div>
                                        <br>
                                    </div>

                                </div>
                            </div>

                            {{------------ ROLE --------}}

                            {{-- <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#demothree">Role  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="demothree" class="collapse">
                                    <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                            <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="ViewRoles">
                                                        &nbsp;&nbsp;
                                                        VIEW
                                                        </label>
                                            </div>
                                            <div class="checkbox pl-5">
                                                <label>
                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="AddRoles">
                                                &nbsp;&nbsp;
                                                ADD
                                                </label>
                                             </div>
                                             <div class="checkbox pl-5">
                                                <label>
                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="EditRoles">
                                                &nbsp;&nbsp;
                                                EDIT
                                                </label>
                                             </div>
                                             <div class="checkbox pl-5">
                                                <label>
                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="DeleteRoles">
                                                &nbsp;&nbsp;
                                                DELETE
                                                </label>
                                             </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div> --}}

                            {{-- Account Management --}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#account_management">Account Management  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="account_management" class="collapse">
                                    <br>
                                    {{-- <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">

                                                    <div class="checkbox pl-5">

                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="ViewAccountManagement">&nbsp;&nbsp;
                                                                VIEW
                                                                </label>
                                                    </div>

                                        </div>
                                        <br>
                                    </div> --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#property_reservation_fee">Property Reservation Fee  <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="property_reservation_fee" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_property_reservation_fee">&nbsp;&nbsp;
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_property_reservation_fee">&nbsp;&nbsp;
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_property_reservation_fee">&nbsp;&nbsp;
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_property_reservation_fee">&nbsp;&nbsp;
                                                        DELETE
                                                        </label>
                                                    </div>


                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-------- deposte ------}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#deposit">Deposit <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="deposit" class="collapse">

                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_deposite">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_deposite">&nbsp;&nbsp;ADD
                                                        </label>
                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_deposite">&nbsp;&nbsp;EDIT
                                                    </label>
                                                 </div>
                                                 <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_deposite">&nbsp;&nbsp;DELETE
                                                    </label>
                                                 </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{---------- rent -------}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#rent_collection">Rent Collection <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="rent_collection" class="collapse">

                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_rent_collection">&nbsp;&nbsp;VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_rent_collection">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_rent_collection">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_rent_collection">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                     {{---------- Service Payment Request -------}}
                                     <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#service_payment_request">Service Payment Request <i class="fas fa-chevron-down text-left"></i></button>
                                     <div id="service_payment_request" class="collapse">

                                         <div class="check_group">
                                             <div class="checkbox mt-3 ">
                                                 <label>
                                                 <input type="checkbox" class="check_all input-icheck" > Select all
                                                 </label>
                                                 <hr>
                                             </div>
                                             <div class="d-flex">
                                                     <div class="checkbox pl-5">
                                                             <label>
                                                             <input class="input-icheck" name="permissions[]" type="checkbox" value="view_service_payment_request">&nbsp;&nbsp;VIEW
                                                             </label>
                                                     </div>
                                                     <div class="checkbox pl-5">
                                                         <label>
                                                         <input class="input-icheck" name="permissions[]" type="checkbox" value="add_service_payment_request">&nbsp;&nbsp;ADD
                                                         </label>
                                                     </div>
                                                     <div class="checkbox pl-5">
                                                         <label>
                                                         <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_service_payment_request">&nbsp;&nbsp;EDIT
                                                         </label>
                                                     </div>
                                                     <div class="checkbox pl-5">
                                                         <label>
                                                         <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_service_payment_request">&nbsp;&nbsp;DELETE
                                                         </label>
                                                     </div>
                                             </div>
                                             <br>
                                         </div>
                                     </div>

                                        {{---------- Agent Cash  Request -------}}
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#agent_cash_request">Agent Cash Pay Request <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="agent_cash_request" class="collapse">

                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view-agent-cash-request">&nbsp;&nbsp;VIEW
                                                                </label>
                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add-agent-cash-request">&nbsp;&nbsp;ADD
                                                            </label>
                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit-agent-cash-request">&nbsp;&nbsp;EDIT
                                                            </label>
                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete-agent-cash-request">&nbsp;&nbsp;DELETE
                                                            </label>
                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        {{----------- report --------}}
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#report">Report <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="report" class="collapse">

                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="account_management_view">&nbsp;&nbsp;VIEW
                                                                </label>
                                                        </div>

                                                </div>

                                            </div>
                                            <br>
                                                {{----------- rent report --------}}

                                            <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#rent_report">Rent Report<i class="fas fa-chevron-down text-left"></i></button>
                                            <div id="rent_report" class="collapse">

                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_rent_report">&nbsp;&nbsp;VIEW
                                                                </label>
                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_rent_report">&nbsp;&nbsp;ADD
                                                            </label>
                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_rent_report">&nbsp;&nbsp;EDIT
                                                            </label>
                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_rent_report">&nbsp;&nbsp;DELETE
                                                            </label>
                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        </div>

                                </div>
                            </div>

                            {{------ Documentation -----}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#document_verification">Documentation  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="document_verification" class="collapse">
                                    <br>
                                    {{-- <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_document">&nbsp;&nbsp;
                                                                VIEW
                                                                </label>
                                                    </div>
                                        </div>
                                        <br>
                                    </div> --}}
                                    {{-- document verification --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#booking_document_verification">Document Verification <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="booking_document_verification" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_document_verification">
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_document_verification">
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_document_verification">
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_document_verification">
                                                        DELETE
                                                        </label>
                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Reserve unit --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#reserve_unit">Reserve Unit<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="reserve_unit" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_reserve_units">
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_reserve_units">
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_reserve_units">
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_reserve_units">
                                                        DELETE
                                                        </label>
                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{--   Issued Contract   --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#issue_contract">Issues Contract<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="issue_contract" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_issue_contract">
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_issue_contract">
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_issue_contract">
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_issue_contract">
                                                        DELETE
                                                        </label>
                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                    {{-- Pending Contract --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#pending_contract">Pending Contract<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="pending_contract" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_pending_contract">
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_pending_contract">
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_pending_contract">
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_pending_contract">
                                                        DELETE
                                                        </label>
                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                    {{--Terminated  Contract --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#terminated_contract">Terminated Contract<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="terminated_contract" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_terminated_contract">
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_terminated_contract">
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_terminated_contract">
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_terminated_contract">
                                                        DELETE
                                                        </label>
                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{--  Report --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#report">Report<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="report" class="collapse">
                                        {{-- <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="report_menu_view">
                                                            VIEW
                                                            </label>
                                                    </div>
                                            </div> --}}
                                            <br>
                                            {{--Terminated  Contract --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#document_menu_report">Residental Report<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="document_menu_report" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_residents_report">
                                                            VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_residents_report">
                                                        ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_residents_report">
                                                        EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_residents_report">
                                                        DELETE
                                                        </label>
                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                        </div>
                                    </div>




                                </div>
                            </div>

                            {{------ Property Management ------}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#property_management">Property Management  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="property_management" class="collapse">
                                    <br>
                                    {{-- <div class="check_group">
                                        <div class="checkbox mt-3 container">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                            @if (!empty($permission))
                                                @foreach ($permission as $v)
                                                    <div class="checkbox pl-5">
                                                            @if ($v->parent == 'employment_management')
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="{{$v['name']}}">
                                                                {{$v['name']}}
                                                                </label>
                                                            @endif
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <br>
                                    </div> --}}
                                    {{-- OWNER --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#property_reservation_fee">Add Landloards/owners<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="property_reservation_fee" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- RESERVATION REQUEST --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#reservation_request">Reservation Request<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="reservation_request" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_reservation_request">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_reservation_request">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_reservation_request">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_reservation_request">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Property List --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#property_list">Property List<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="property_list" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_property_list">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_property_list">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_property_list">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_property_list">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Request to add property --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#request_to_add_property">Request To Add Property<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="request_to_add_property" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_request_to_add_property">&nbsp;&nbsp;VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_request_to_add_property">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_request_to_add_property">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_request_to_add_property">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Residents --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#residents">Residents<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="residents" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_residents">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_residents">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_residents">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_residents">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Request for ownership --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#request_for_ownership">Request For Ownership<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="request_for_ownership" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_request_for_ownership">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_request_for_ownership">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_request_for_ownership">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_request_for_ownership">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Vacate Request --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#vacate_request">Vacate Request <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="vacate_request" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_vacate_request">&nbsp;&nbsp;VIEW
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_vacate_request">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_vacate_request">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_vacate_request">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- Report --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_report">Reports <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_report" class="collapse">
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_owners_report">Owner <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_owners_report" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_owners_report">&nbsp;&nbsp;VIEW
                                                                </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_owners_report">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_owners_report">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_owners_report">&nbsp;&nbsp;DELETE
                                                            </label>

                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_residents_report">Residents <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_residents_report" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_residents_report">&nbsp;&nbsp;VIEW
                                                                </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_residents_report">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_residents_report">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_residents_report">&nbsp;&nbsp;DELETE
                                                            </label>

                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>

                                    </div>
                                    {{-- Master --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_master">Master <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_master" class="collapse">
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_frequence">Frequence <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_frequence" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_frequence">&nbsp;&nbsp;VIEW
                                                                </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_frequence">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_frequence">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_frequence">&nbsp;&nbsp;DELETE
                                                            </label>

                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_type">Type <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_type" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_type">&nbsp;&nbsp;VIEW
                                                                </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_type">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_type">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_type">&nbsp;&nbsp;DELETE
                                                            </label>

                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>

                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_amentites">Amenities <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_amentites" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="admin_admin_amentites">&nbsp;&nbsp;VIEW
                                                                </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_amentites">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_amentites">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_amentites">&nbsp;&nbsp;DELETE
                                                            </label>

                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>

                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_details">Details <i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_details" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_details">&nbsp;&nbsp;VIEW
                                                                </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_details">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_details">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_details">&nbsp;&nbsp;DELETE
                                                            </label>
                                                        </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            {{-------- sale ----------}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_sale">Sale  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="admin_sale" class="collapse">
                                    <br>
                                    {{-- <div class="check_group">
                                        <div class="checkbox mt-3 container">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                            @if (!empty($permission))
                                                @foreach ($permission as $v)
                                                    <div class="checkbox pl-5">
                                                            @if ($v->parent == 'employment_management')
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="{{$v['name']}}">
                                                                {{$v['name']}}
                                                                </label>
                                                            @endif
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <br>
                                    </div> --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#customer_visit_request">Customer Visit Request  <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="customer_visit_request" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="view_customer_visit_request">&nbsp;&nbsp;VIEW
                                                    </label>

                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="add_customer_visit_request">&nbsp;&nbsp;ADD
                                                    </label>

                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_customer_visit_request">&nbsp;&nbsp;EDIT
                                                    </label>

                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_customer_visit_request">&nbsp;&nbsp;DELETE
                                                    </label>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            {{---- Facility Management ----}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#facility_management">Facility Management  <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="facility_management" class="collapse">
                                    <br>
                                    {{-- <div class="check_group">
                                        <div class="checkbox mt-3 container">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                            @if (!empty($permission))
                                                @foreach ($permission as $v)
                                                    <div class="checkbox pl-5">
                                                            @if ($v->parent == 'employment_management')
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="{{$v['name']}}">
                                                                {{$v['name']}}
                                                                </label>
                                                            @endif
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <br>
                                    </div> --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_service_request">Service Requests  <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_service_request" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_service_request">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_service_request">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_service_request">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_service_request">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_cancelled_request">Cancelled Request<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_cancelled_request" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_cancelled_request">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_cancelled_request">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_cancelled_request">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_cancelled_request">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_reserve_unit">Reserve Unit<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_reserve_unit" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_reserve_unit">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_reserve_unit">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_reserve_unit">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_reserve_unit">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_issued_contract">Issued Contract<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_issued_contract" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_issued_contract">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_issued_contract">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_issued_contract">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_issued_contract">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_terminated_contract">Terminated Contract<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_terminated_contract" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_terminated_contract">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_terminated_contract">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_terminated_contract">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_terminated_contract">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_master">Master<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_master" class="collapse">


                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_master_service">Service Request<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_master_service" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_master_service">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_master_service">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_master_service">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_master_service">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>

                                    </div>

                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_master_service_provider">Service Provider<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_master_service_provider" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_master_service_provider">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_master_service_provider">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_master_service_provider">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_master_service_provider">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>

                                    </div>

                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_master_inspection_team">Inspection Team<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_master_service_provider" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_master_inspection_team">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_master_inspection_team">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_master_inspection_team">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_master_inspection_team">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>

                                    </div>


                                    </div>



                                </div>
                            </div>

                            {{----- Setup Management ------}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#setup_management">Setup Management <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="setup_management" class="collapse">
                                    <br>

                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_region_management">Region Management  <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_region_management" class="collapse">
                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_country">Country/State<i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_country" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_country">&nbsp;&nbsp;VIEW
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_country">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_country">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_country">&nbsp;&nbsp;DELETE
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>

                                        </div>

                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_city">City/Pincode<i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_city" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_city">&nbsp;&nbsp;VIEW
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_city">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_city">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_city">&nbsp;&nbsp;DELETE
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>

                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_department">Departments <i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_department" class="collapse">
                                        <br>

                                        <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_designation">Designation<i class="fas fa-chevron-down text-left"></i></button>
                                        <div id="admin_designation" class="collapse">
                                            <div class="check_group">
                                                <div class="checkbox mt-3 ">
                                                    <label>
                                                    <input type="checkbox" class="check_all input-icheck" > Select all
                                                    </label>
                                                    <hr>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="d-flex">
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_designation">&nbsp;&nbsp;VIEW
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_designation">&nbsp;&nbsp;ADD
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_designation">&nbsp;&nbsp;EDIT
                                                            </label>

                                                        </div>
                                                        <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_designation">&nbsp;&nbsp;DELETE
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_sales_agent">Sales Agent<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_sales_agent" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_sales_agent">&nbsp;&nbsp;VIEW
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_sales_agent">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_sales_agent">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_sales_agent">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{------- Event ------}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_event">Events <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="admin_event" class="collapse">
                                    <br>
                                    <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_events">&nbsp;&nbsp;VIEW
                                                                </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_events">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_events">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_events">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>

                            {{-- notification --}}
                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_notification">Notification <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="admin_notification" class="collapse">
                                    <br>
                                    <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_notification">&nbsp;&nbsp;VIEW
                                                        </label>
                                                </div>
                                                <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_notification">&nbsp;&nbsp;ADD
                                                        </label>
                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_notification">&nbsp;&nbsp;EDIT
                                                    </label>
                                                </div>
                                                <div class="checkbox pl-5">
                                                    <label>
                                                    <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_notification">&nbsp;&nbsp;DELETE
                                                    </label>
                                                </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>

                            {{------ Feedback ------}}
                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_feedback">FeedBack <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="admin_feedback" class="collapse">
                                    <br>
                                    <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_feedback">&nbsp;&nbsp;VIEW
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_feedback">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_feedback">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_feedback">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                        </div>
                                        <br>
                                    </div>




                                </div>
                            </div>

                            {{-- reward --}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_reward">Rewards <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="admin_reward" class="collapse">
                                    <br>
                                    <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="view_rewards">&nbsp;&nbsp;VIEW
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_rewards">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_rewards">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_rewards">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>

                            {{------- CMS ------}}

                            <div class="card">
                                <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_cms">CMS <i class="fas fa-chevron-down text-left"></i></button>
                                <div id="admin_cms" class="collapse">
                                    <br>
                                    {{-- <div class="check_group">
                                        <div class="checkbox mt-3 ">
                                            <label>
                                            <input type="checkbox" class="check_all input-icheck" > Select all
                                            </label>
                                            <hr>
                                        </div>
                                        <div class="d-flex">
                                            @if (!empty($permission))
                                                @foreach ($permission as $v)
                                                    <div class="checkbox pl-5">
                                                            @if ($v->parent == 'employment_management')
                                                                <label>
                                                                <input class="input-icheck" name="permissions[]" type="checkbox" value="{{$v['name']}}">
                                                                {{$v['name']}}
                                                                </label>
                                                            @endif
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <br>
                                    </div> --}}
                                    {{-- About --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_about">About<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_about" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">

                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_about">&nbsp;&nbsp;VIEW&nbsp;
                                                            </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_about">&nbsp;&nbsp;ADD
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_about">&nbsp;&nbsp;EDIT
                                                        </label>

                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_about">&nbsp;&nbsp;DELETE
                                                        </label>

                                                    </div>

                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    {{-- privacy policy --}}
                                    <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_privacy_policy">Privacy Policy<i class="fas fa-chevron-down text-left"></i></button>
                                    <div id="admin_privacy_policy" class="collapse">
                                        <div class="check_group">
                                            <div class="checkbox mt-3 ">
                                                <label>
                                                <input type="checkbox" class="check_all input-icheck" > Select all
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="d-flex">
                                                    <div class="checkbox pl-5">
                                                            <label>
                                                            <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_privacy_policy">&nbsp;&nbsp;VIEW&nbsp;
                                                            </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_privacy_policy">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_privacy_policy">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_privacy_policy">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                     {{-- Legal Information --}}
                                     <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_legal_information">Legal Information<i class="fas fa-chevron-down text-left"></i></button>
                                     <div id="admin_legal_information" class="collapse">
                                         <div class="check_group">
                                             <div class="checkbox mt-3 ">
                                                 <label>
                                                 <input type="checkbox" class="check_all input-icheck" > Select all
                                                 </label>
                                                 <hr>
                                             </div>
                                             <div class="d-flex">
                                                     <div class="checkbox pl-5">
                                                             <label>
                                                             <input class="input-icheck" name="permissions[]" type="checkbox" value="view_admin_legal_information">&nbsp;&nbsp;VIEW&nbsp;
                                                             </label>
                                                     </div>
                                                     <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_admin_legal_information">&nbsp;&nbsp;ADD
                                                        </label>
                                                     </div>
                                                     <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_admin_legal_information">&nbsp;&nbsp;EDIT
                                                        </label>
                                                     </div>
                                                     <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_admin_legal_information">&nbsp;&nbsp;DELETE
                                                        </label>
                                                     </div>
                                             </div>
                                             <br>
                                         </div>
                                     </div>
                                      {{-- FAQ --}}
                                      <button type="button" class="btn btn-success border-0 check-tab check-title text-left" data-toggle="collapse" data-target="#admin_faq">FAQ<i class="fas fa-chevron-down text-left"></i></button>
                                      <div id="admin_faq" class="collapse">
                                          <div class="check_group">
                                              <div class="checkbox mt-3 ">
                                                  <label>
                                                  <input type="checkbox" class="check_all input-icheck" > Select all
                                                  </label>
                                                  <hr>
                                              </div>
                                              <div class="d-flex">
                                                      <div class="checkbox pl-5">
                                                              <label>
                                                              <input class="input-icheck" name="permissions[]" type="checkbox" value="view_faq">&nbsp;&nbsp;VIEW&nbsp;
                                                              </label>
                                                      </div>
                                                      <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="add_faq">&nbsp;&nbsp;ADD
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="edit_faq">&nbsp;&nbsp;EDIT
                                                        </label>
                                                    </div>
                                                    <div class="checkbox pl-5">
                                                        <label>
                                                        <input class="input-icheck" name="permissions[]" type="checkbox" value="delete_faq">&nbsp;&nbsp;DELETE
                                                        </label>
                                                    </div>
                                              </div>
                                              <br>
                                          </div>
                                      </div>

                                </div>
                            </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary ">SAVE</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection
@section('scripts')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> --}}
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>



<script>
    $(document).on('change', '.check_all', function()
    {
        if($(".check_all").is(':checked')){
                    $(this)
            .closest('.check_group')
            .find('.input-icheck')
            .each(function() {
                $(this).iCheck('check');
            });
        }else{
                    $(this)
            .closest('.check_group')
            .find('.input-icheck')
            .each(function() {
                $(this).iCheck('uncheck');
            });
        }

    });

    // $('.check_all').each(function() {
    //     var length = 0;
    //     var checked_length = 0;
    //     $(this)
    //         .closest('.check_group')
    //         .find('.input-icheck')
    //         .each(function() {
    //             length += 1;
    //             if ($(this).iCheck('update')[0].checked) {
    //                 checked_length += 1;
    //             }
    //         });
    //     length = length - 1;
    //     if (checked_length != 0 && length == checked_length) {
    //         $(this).iCheck('check');
    //     }
    // });


</script>



<script>
    var coll = document.getElementsByClassName("collapsible-tab");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
          content.style.maxHeight = null;
        } else {
          content.style.maxHeight = content.scrollHeight + "px";
        }
      });
    }
</script>





@endsection





