@extends('admin.layout.app')
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <a class="breadcrumb-item" href="">Set Up Management</a>
        <span class="breadcrumb-item active">User Role</span>
    </nav>
</div>
<div class="br-pagebody">
    @if(Session::has('message'))
    <div class="alert alert-{{Session::get('status')}}" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      {{Session::get('message')}}
    </div>
    @endif
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">User Roles</h4>
                        <p class="card-category">List of all User Roles</p>
                    </div>

                    <div class="pd-5 mg-r-20">
                        <a href="{{ route('roles.create') }}" class="nav-link card-active tx-white d-flex align-items-center"
                           ><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd
                            User Roles</a>
                    </div>

                </div>
            </div>
            <div class="card-body">

                <table id="datatableproperty" class="table data-table printTable table-striped table-bordered text-center">

                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-15p">Name</th>
                            <th class="wd-15p">Actions</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $key => $role)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $role->name }}</td>
                                <td>
                                    {{-- <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a> --}}
                                    {{-- @can('role-edit') --}}
                                        <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                    {{-- @endcan --}}
                                    {{-- @can('role-delete') --}}
                                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    {{-- @endcan --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $roles->render() !!}
            </div>
        </div>
    </div>
</div>


@endsection
