@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 500 !important;
    }

    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height: 20px;
        border-radius: 5px !important
    }

    #map {
        height: 450px;
        width: 100%;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

    .errorTxt {
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding: 10px;
        border-radius: 10px;
    }

    .error {
        color: red !important;
    }

    .ui-datepicker {
        position: fixed;
        top: 270.797px;
        left: 172.5px;
        z-index: 5001 !important;
        display: block;
    }

    #ui-datepicker-div {
        z-index: 5001 !important;
    }

</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <a class="breadcrumb-item" href="">Roles</a>
        <span class="breadcrumb-item active">Create Roles</span>
    </nav>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">User Roles</h4>
                        <p class="card-category">Create User Roles</p>
                    </div>

                    <div class="pd-5 mg-r-20">
                        <a href="{{ route('roles.index') }}" class="nav-link card-active tx-white d-flex align-items-center"
                           ><i class="fad fa-plus-hexagon tx-20"></i>Back</a>
                    </div>


                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{url('roles').'/'.$role->id}}" accept-charset="UTF-8" id="role_edit_form">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">ROLE NAME:*</label>
                                    <input class="form-control" value="{{$role->name}}" required placeholder="Role Name" name="name" type="text" id="name" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>PERMISSION</th>
                                            <th>CHECK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- <td> Accounts Management </td>
                                        <td>
                                              @foreach ($permission as $v)
                                                    @if ($v->parent == "accounts")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                        </td> --}}
                                        {{-- @if (!empty($permission))
                                            @foreach ($permission as $v)
                                                <tr>


                                                    <td>{{$v['name']}}</td>
                                                    <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif></td>

                                                </tr>
                                            @endforeach
                                        @endif --}}

                                           @if (!empty($permission))
                                            <tr>
                                                <td> Employee Management </td>

                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "employee")
                                                    <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}">
                                                    {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach

                                            </tr>

                                            <tr>
                                                <td> Accounts Management </td>

                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "accounts")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach

                                            </tr>

                                            <tr>
                                                <td> Documentation </td>

                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "documentation")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach

                                            </tr>

                                            <tr>
                                                <td> Property Management </td>

                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "property_management")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                            </tr>
                                            <tr>
                                                <td> Sales </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "sales")
                                                    <td> <input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                            </tr>

                                            <tr>
                                                <td> Facility Management </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "facility_management")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                            </tr>
                                            <tr>
                                                <td> Set Up Management </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "set_up_management")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                            </tr>
                                            <tr>
                                                <td> Events </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "events")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                            </tr>
                                            <tr>
                                                <td> Notifications </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "notification")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}</td>
                                                    @endif
                                                    @endforeach
                                            </tr>
                                            <tr>
                                                <td> Feedbacks </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "feedbacks")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}
                                                    @endif
                                                    @endforeach
                                            </tr>
                                            <tr>
                                                <td> Rewards </td>

                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "rewards")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}
                                                    @endif
                                                    @endforeach

                                            </tr>
                                            <tr>
                                                <td> CMS </td>
                                                    @foreach ($permission as $v)
                                                    @if ($v->parent == "cms")
                                                     <td><input class="input-icheck" name="permission[]" type="checkbox" value="{{$v['name']}}" @if(in_array($v['name'], $role_permissions)) checked @endif> {{$v['name']}}
                                                    @endif
                                                    @endforeach
                                            </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                     <!-- /.box-body -->
                     <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">SAVE</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection

