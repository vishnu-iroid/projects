@extends('admin.layout.app')
 <style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4, h4 {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00) !important ;
        height : 20px !important ;
        border-radius: 5px  !important ;
    }
    .card-warning-2 {
        background: linear-gradient(60deg, #ffa726, #fb8c00) !important ;
        height : 20px !important ;
        border-radius: 5px  !important ;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Master</span>
        <span class="breadcrumb-item active">Lease type</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
<div class="row">
    <div class="col-md-3">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-12">
                        <h2 class="card-title" style="font-size: 1.125rem;font-weight: 300 !important;">Add Lease type</h2>
                        <!-- <p class="card-category"></p> -->
                    </div>
                </div>
        </div>
        <div class="card-body">
            <form class="form-clients" id="form-add" action="javascript:;">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Type <span class="tx-danger">*</span></label>
                        <input class="form-control" id="id" name="id" type="hidden"/>
                        <input class="form-control" id="type" name="type" type="text" placeholder="Enter Lease type"/>
                    </div>
                    <div class="form-group">
                        <label>No. of days <span class="tx-danger">*</span></label>
                        <input class="form-control" id="days" name="days" type="text" placeholder="Days"/>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary addBtn">Submit</button>
                </div>
            </form>
        </div>
        </div>
    </div>
    <div class="col-md-9">
    <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title">Lease type</h4>
                        <p class="card-category">List of all Lease type</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-10p">Id</th>
                            <th class="wd-15p">Type</th>
                            <th class="wd-15p">Actions</th>
                            <th class="wd-5p">Days</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($frequency as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->type }}</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-type="{{$row->id}}" title="Edit"></a>
                                <!-- <a class="fad fa-trash-alt tx-20 reject-button btn btn-primary bt_cus" href="javascript:0;" title="Delete" style="" onclick="deleteFunction('{{$row->id}}')"></a> -->
                            </td>
                            <td>{{ $row->days }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="d-flex justify-content-end">
                        {!! $frequency->links() !!}
                </div> --}}
            </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
</div>
</div>
<!-- br-pagebody -->

<!-- Reject Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content delete-popup">
         <div class="modal-header">
            <h4 class="modal-title tx-danger">Delete Property Type</h4>
         </div>
         <form action="javascript:;" id="form-delete">
            <div class="modal-body">
                Are you sure you want to delete this property type?
               <input type="hidden" name="departmentId" id="departmentId">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary closeBtn" onclick="window.location.reload();">Cancel</button>
               <button type="submit" class="btn btn-danger reject-class deletetBtn">Delete</button>
            </div>
         </form>
      </div>
   </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/js/datepicker.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
        $('.accordion').click(function(){
            $id = $(this).attr('val')
            $('#collapseOne-'+$id).toggleClass('show')
        })

    $("#site_title").html(`Property | Property Type`);
    $("#master").addClass("active");
    $("#master_frequency_nav").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });

    function deleteFunction(id){
        $('#departmentId').val(id);
        $('#deleteModal').modal()
    }

    $('.deletetBtn').on('click',function(){
        var id = $('#departmentId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
                type: "GET",
                url: url+"/department/delete/"+id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".deletetBtn").html('Deleting..');
                },
                success: function (data) {
                    $(".deletetBtn").text('Delete');
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
    })

    $('tbody').on('click','.edit-button',function(){
        var typeId = $(this).data('type')
        $.ajax({
            url : url+"/frequency/edit/"+typeId,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                console.log(data);
                if(data.status === true){
                    $('.addBtn').html('Update')
                    $('#id').val(data.response.id)
                    $('#type').val(data.response.type)
                    $('#days').val(data.response.days)

                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $("#form-add").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            type: {
                required: true,
            },

        },
        submitHandler: function (form) {
            var form        = document.getElementById("form-add");
            var formData    = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addFrequency')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
