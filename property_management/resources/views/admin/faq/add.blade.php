   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
   </style>
   @section('content')
      <div class="br-pagetitle">
       
        <div>
          <h4>Add Faq</h4>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <form action="{{ route('addFaqDetails') }}" method="POST">
                <div class="br-section-wrapper mg-t-10">
                    <h4 class="br-section-label">English</h4>
                        @csrf
                        <div class="form-layout form-layout-1">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Enter Your Question: <span class="tx-danger">*</span></label>
                                    <textarea  class="form-control ckeditor" type="text" rows="5" name="english_question" placeholder="Enter your question"></textarea>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Enter Your Answer: <span class="tx-danger">*</span></label>
                                    <textarea class="form-control ckeditor" type="text" rows="5" name="english_answer" placeholder="Enter your answer"></textarea>
                                </div>
                            </div><!-- col-4 -->

                        </div><!-- form-layout -->
                </div>
                <div class="br-section-wrapper mg-t-2">
                    <h4 class="br-section-label">Arabic</h4>
                        <div class="form-layout form-layout-1">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Enter Your Question: <span class="tx-danger">*</span></label>
                                    <textarea id="editor3" class="form-control rtl ckeditor" type="text" rows="5" name="arabic_question" placeholder="Enter your question"></textarea>
                                </div>
                            </div><!-- col-4 -->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Enter Your Answer: <span class="tx-danger">*</span></label>
                                    <textarea id="editor2" class="form-control  ckeditor" type="text" rows="5" name="arabic_answer" placeholder="Enter your answer"></textarea>
                                </div>
                            </div><!-- col-4 -->

                        <div class="form-layout-footer">
                            <button class="btn btn-info">Submit</button>
                        </div><!-- form-layout-footer -->
                        </div><!-- form-layout -->
                </div>

            </form>
      </div><!-- br-pagebody -->
    @endsection
    @section('scripts')
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
    <script>
    CKEDITOR.replace('editor2', {
      extraPlugins: 'bidi',
      // Setting default language direction to right-to-left.
      contentsLangDirection: 'rtl',
      height: 270,
      scayt_customerId: '1:Eebp63-lWHbt2-ASpHy4-AYUpy2-fo3mk4-sKrza1-NsuXy4-I1XZC2-0u2F54-aqYWd1-l3Qf14-umd',
      scayt_sLang: 'auto'
    });
  </script>
    <script>
    CKEDITOR.replace('editor3', {
      extraPlugins: 'bidi',
      // Setting default language direction to right-to-left.
      contentsLangDirection: 'rtl',
      height: 270,
      scayt_customerId: '1:Eebp63-lWHbt2-ASpHy4-AYUpy2-fo3mk4-sKrza1-NsuXy4-I1XZC2-0u2F54-aqYWd1-l3Qf14-umd',
      scayt_sLang: 'auto'
    });
  </script>
    @endsection