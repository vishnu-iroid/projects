   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
   </style>
   @section('content')
   <div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <a class="breadcrumb-item" href="">CMS</a>
        <span class="breadcrumb-item active">Faq</span>
    </nav>

</div>

      <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 ">
              <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Faq</h4>
                            <p class="card-category">List of all Faq</p>
                        </div>
                        <div class="pd-5 mg-r-20">
                            <a href="{{ url('add-faq-view') }}" class="nav-link card-active tx-white d-flex align-items-center" ><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Faq</a> 
                        </div>

                    </div>
                </div>
            
<div class="card-body">
        <div class="bd bd-gray-300  table-responsive">
            <table class="table mg-b-0">
              <thead>
                <tr>
                  <th width="5%">No.</th>
                  <th width="20%">Question</th>
                  <th width="20%">Answer</th>
                  <th width="20%">Question Arabic</th>
                  <th width="20%">Answer Arabic</th>
                  <th width="15%">Action</th>
                </tr>
              </thead>
              <tbody>
                  @php $i =1; @endphp
                  @foreach($faq as $row)
                  <tr>
                    <th scope="row">{{ $i }} @php $i++; @endphp</th>
                    <td>
                      @php 
                      if (strlen($row->english_question) > 50){
                          $row->english_question = substr($row->english_question, 0, 47) . '...';
                      }
                      @endphp
                      {!! $row->english_question ?? '' !!}</td>
                    <td>
                      @php 
                      if (strlen($row->english_answer) > 50){
                        $row->english_answer = substr($row->english_answer, 0, 47) . '...';
                      }
                      @endphp
                      {!!$row->english_answer ?? '' !!}</td>
                    <td>
                      @php 
                      if (strlen($row->arabic_question) > 50){
                        $row->arabic_question = substr($row->arabic_question, 0, 47) . '...';
                      }
                      @endphp
                      {!! $row->arabic_question ?? '' !!}</td>
                    <td>
                      @php 
                      if (strlen($row->arabic_answer) > 50){
                        $row->arabic_answer = substr($row->arabic_answer, 0, 47) . '...';
                      }
                      @endphp
                      {!! $row->arabic_answer ?? '' !!}</td>
                    <td>
                        <a href="{{ url('edit-faq/'.$row->id) }}" class="btn btn-primary btn-icon"><div><i class="fa fa-edit"></i></div></a>
                        <span><a href="{{ url('delete-faq/'.$row->id) }}" class="btn btn-danger btn-icon"><div><i class="fa fa-trash"></i></div></a></span>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div><!-- bd -->
      </div><!-- br-pagebody -->


    @endsection
    