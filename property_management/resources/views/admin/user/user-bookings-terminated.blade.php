@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Documentation</span>
            <span class="breadcrumb-item active">Terminated Contracts</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                            <i class="fas fa-plus-square"></i>&nbsp Add Client
                                        </button>
                                </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Terminated Contracts</h4>
                            <p class="card-category">List of all Terminated contracts </p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>User Name</th>
                                <th>Contact No</th>
                                <th>Agent</th>
                                <th>Property Name</th>
                                <th>Unit Name</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    {{-- {{dd($row->user_details)}} --}}
                                    @if ($row->user_property_rel->user_rel)
                                        @if ($row->user_property_rel->user_rel->name)
                                            <td>{{ $row->user_property_rel->user_rel->name }}</td>
                                        @endif
                                    @endif
                                    @if ($row->user_property_rel->user_rel)
                                        @if ($row->user_property_rel->user_rel->phone)
                                            <td>{{ $row->user_property_rel->user_rel->phone }}</td>
                                        @endif
                                    @endif
                                    @if ($row->agent_rel)
                                    <td>{{$row->agent_rel->name}}</td>
                                    @endif
                                    {{-- @if ($row->user_property_rel->user_property_related)
                                        <td> {{ $row->user_property_rel->user_property_related->property_name }} </td>
                                    @endif
                                    @php
                                        $building_image = \App\Models\OwnerProperty::where('id', $row->user_property_rel->user_property_related->builder_id)->first();
                                    @endphp
                                    @if ($row->user_property_rel->user_property_related->builder_id > 0)
                                        <td>{{ $building_image->property_name }}</td>

                                    @else
                                        <td>No Units</td>
                                    @endif --}}



                                    @if ($row->user_property_rel->user_property_related->builder_id != null)
                                    @php
                                     $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->user_property_rel->user_property_related->builder_id)->first();
                                    @endphp
                                <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                <td>{{ $row->user_property_rel->user_property_related->property_name }}</td>
                            @else
                                <td>{{ $row->user_property_rel->user_property_related->property_name }}</td>
                                <td>--</td>
                            @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $users->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
                {{-- <a class="fad fa-upload tx-20 upload-button ml-1" href="javascript:0;"style="color:rgb(0, 119, 255)"onClick="contractModal('{{ $row->id }}','{{ $row->prop_rel->property_to }}',{{ $row->rent }})"title="Upload Contract"></a> @endif </td> --}}

            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>
    @endsection @section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
    <script>
        const url = '{{ url('/') }}';
    </script>
    <script>
        $("#site_title").html(`Property | Bookings`);
        $("#user_book").addClass("active");
    </script>
    <script>

    </script>
    <script>
        // function contractModal(bookid, property_to, amount) {


        //     $('#bookid').val(bookid);
        //     if (property_to == 1) {
        //         $('.strat_title').text('Contract Issued Date');
        //         $('.end_date').hide();
        //     }
        //     $('#final_amount').val(amount);
        //     $('#contractUploadModal').modal()
        // }
        function renewContractFunction(bookid, property_to, amount, contract_no) {
            $('#bookid').val(bookid);

            if (property_to == 1) {
                $('.strat_title').text('Contract Issued Date');
                $('.end_date').hide();
            }
            $('#final_amount').val(amount);
            $('#contractUploadModal').modal()
        }


        function verification(bookid) {
            $('#id').val(bookid);
            $('#verification').modal();

        }
    </script>
    <script>
        $('.submitBtn').click(function() {
            var propertyId = $('#id').val()
            $.ajax({
                url: url + "/property_verification/" + propertyId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                }
            })
            return false
        })
    </script>

    <script>
        $('.upload-button').click(function() {
            var booked_id = $(this).data('property');
            $('#bookedid').val(booked_id);
            $('#contractUploadModal').modal();
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                container: '#contractUploadModal modal-body'
            });
        });
        $("#contract-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                contract_start_date: {
                    required: true,
                },
                contract_end_date: {
                    required: true,
                }


            },
            submitHandler: function(form) {
                var form = document.getElementById("contract-form");
                var formData = new FormData(form);
                $(".contractBtn").prop("disabled", true);
                $(".contractcloseBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('editUserBookingContract') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".contractBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".contractBtn").text('Submit');
                        $(".contractBtn").prop("disabled", false);
                        $(".contractcloseBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>
@endsection
