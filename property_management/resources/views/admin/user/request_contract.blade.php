@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Documentation</span>
            <span class="breadcrumb-item active">Contract Request</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Contract Requests</h4>
                            <p class="card-category">List of all Contract Requests</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="data-table table display responsive wrap">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Applicant Name</th>
                                <th>Agent Name</th>
                                <th>Contact no</th>
                                <th>Unit No/Name </th>
                                <th>Property Name</th>
                                <th>Rent Value</th>
                                <th>Attachment</th>
                                <th>Frequncy </th>
                                <th>Floor Count </th>
                                <th>check-in</th>
                                <th>check-out</th>
                                <th>deposit</th>
                                <th>Owner</th>
                                <th>Ownership Doc</th>
                                <th>Requested</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- Reject Modal -->
        <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content delete-popup">
                    <div class="modal-header">
                        <h4 class="modal-title tx-danger reject-modal-title">Reject Request</h4>
                    </div>
                    <form action="javascript:;" id="delete-form">
                        <div class="modal-body">
                            Are you sure you want to reject this request?
                            <input type="hidden" name="documentId" id="documentId">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                                onclick="window.location.reload();">Cancel</button>
                            <button type="submit" class="btn btn-danger reject-class deletetBtn">Reject</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- br-section-wrapper -->
        <div class="modal fade contractUpload" id="contractUploadModal" tabindex="-1" role="dialog"
            aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content delete-popup">
                    <div class="modal-header">
                        <h4 class="modal-title tx-danger reject-modal-title">Upload Contract Details</h4>
                    </div>
                    <form action="javascript:;" id="contract-form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract No</label>
                                        <input type="text" class="form-control" name="contract_no" id="contract_no">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract Type</label>
                                        <input class="form-control" id="contract_type" name="contract_type" type="text" placeholder="contract type"/>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract No</label>
                                        <input type="text" class="form-control" name="contract_no" id="contract_no">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract File</label>
                                        <input type="file" class="form-control" name="contract_file" id="contract_file">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract Start Date<span class="tx-danger">*</span></label>
                                        <input type="hidden" name="bookedid" id="bookid">
                                        <input type="hidden" name="requestid" id="requestid">
                                        <input type="date" class="form-control fc-datepicker" name="contract_start_date"
                                            id="contract_start_date">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contract End Date<span class="tx-danger">*</span></label>
                                        <input type="date" class="form-control fc-datepicker" name="contract_end_date"
                                            id="contract_end_date">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. of Free Maintenance <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="free_maintenance"
                                            id="selling_price">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Maintenance Charge <span class="tx-danger">*</span></label>
                                        <input type="text" class="form-control" name="maintenance_charge" id="mrp">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default bg-purple tx-white contractcloseBtn"
                                onclick="window.location.reload();">Cancel</button>
                            <button type="submit" class="btn btn-danger reject-class contractBtn">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>

    <script>
        const url = '{{url("/")}}';
        $(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ route('requestUserContract') }}",
                    data: function(d) {
                        d.search = $('input[type="search"]').val();
                        d.owner = $('#owner_id').val();
                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },
                    {
                        data: 'user',
                        name: 'user'
                    },
                    {
                        data: 'agent',
                        name: 'agent'
                    },
                    {
                        data: 'agent_contact',
                        name: 'agent_contact'
                    },
                    {
                        data: 'unit',
                        name: 'unit'
                    },
                    {
                        data: 'building',
                        name: 'building'
                    },
                    // {
                    //     data: null,
                    //     render: function ( data, type, row ) {

                    //         if(data.building == 'NIL'){
                    //             return data.building;
                    //         }
                    //         else
                    //         {
                    //             return data.property;
                    //         }

                    //     }

                    // },
                    // {
                    //     data:null,
                    //     render: function ( data, type, row ) {

                    //         if(data.building == 'NIL'){
                    //             return data.property;
                    //         }
                    //         else
                    //         {
                    //             return data.building;
                    //         }

                    //     }

                    // },
                    {
                        data: 'property_rent',
                        name: 'property_rent'
                    },

                    {
                        data: 'attachment',
                        name: 'attachment',
                        render: function(data, type, full, meta) {
                            return '<a href="' + data +
                                '" class="btn btn-warning btn-xs mrg bt_cus"><i class=" far fa-file-alt"></i></a>';
                        }
                    },
                    {
                        data: 'frequency',
                        name: 'frequency'
                    },
                    {
                        data: 'floorNo',
                        name: 'floorNo'
                    },
                    {
                        data: 'check_in',
                        name: 'check_in'
                    },
                    {
                        data: 'check_out',
                        name: 'check_out'
                    },
                    {
                        data: 'security_deposit',
                        name: 'security_deposit'
                    },
                    {
                        data: 'owner',
                        name: 'owner'
                    },
                    {
                        data: 'ownership_doc',
                        name: 'ownership_doc',
                        render: function(data, type, full, meta) {
                            return '<a href="' + data +
                                '" class="btn btn-warning btn-xs mrg "><i class=" far fa-file-alt"></i></a>';
                        }
                    },
                    {
                        data: 'requested_date',
                        name: 'requested_date'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });
            $('.filter').change(function() {
                table.draw();
            });

        });
    </script>
    <script>
        $(document).on('click', '.upload-button', function(e) {
            var property_id = $(this).data('property');
            var request_id = $(this).data('requestid');

            $('#bookid').val(property_id);
            $('#requestid').val(request_id)
            $('#contractUploadModal').modal();
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                container: '#contractUploadModal modal-body'
            });
        });

        $("#contract-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                contract_start_date: {
                    required: true,
                },
                contract_end_date: {
                    required: true,
                }
            },
            submitHandler: function(form) {
                var form = document.getElementById("contract-form");
                var formData = new FormData(form);
                $(".contractBtn").prop("disabled", true);
                $(".contractcloseBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addUserBookingContract') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".contractBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".contractBtn").text('Submit');
                        $(".contractBtn").prop("disabled", false);
                        $(".contractcloseBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });



        function rejectFunction(id) {
            $('#documentId').val(id);
            $('#rejectModal').modal()
        }
        $(document).on('click', '.deletetBtn', function() {
            var id = $('#documentId').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/reject/contract/" + id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    console.log(url);
                    $(".deletetBtn").text('Processing..');
                },
                success: function(data) {
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
        })
    </script>
@endsection
