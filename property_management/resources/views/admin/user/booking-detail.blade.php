@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="{{ route('userBookings') }}">Bookings</a>
            <span class="breadcrumb-item active">Details</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                                                                                <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                                                                                    <i class="fas fa-plus-square"></i>&nbsp Add Client
                                                                                                </button>
                                                                                        </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Booking Details</h4>
                            <p class="card-category"></p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-header card-header-tabs-line">

                        <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" id="main-property" href="#property-status"
                                    role="tab" aria-controls="property-status" aria-selected="true">Property</a>
                            </li>
                            {{-- @if ($user->offer_rel)
                                @if ($user->property_details->property_to == '0')
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" id="main-package" href="#property-offer"
                                            role="tab" aria-selected="false"> Offer Package</a>
                                    </li>
                                @endif
                            @endif --}}

                            {{-- @if ($user->coupon_rel)
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" id="main-coupon" href="#property-coupon"
                                        role="tab" aria-selected="false"> Applied Coupon</a>
                                </li>
                            @endif --}}

                            {{-- <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" id="main-approve" href="#approve-booking" role="tab"
                                    aria-selected="false">Approve Booking</a>
                            </li> --}}
                        </ul>
                    </div>
                    <div class="tab-content pt-3">
                        <div class="tab-pane fade show active" id="property-status" role="tabpanel"
                            aria-labelledby="main-property">
                            <div class="row justify-content-between align-items-center mb-5">
                                <div class="col-md-10 col-12 ">
                                    <div class="col-md-12">
                                        <div class="property-details">
                                            @if ($user->property_details)
                                                @if ($user->property_details->property_reg_no)
                                                    <p>Property Reg No:- <b>{{ $user->property_details->property_reg_no }}</b>
                                                    </p>
                                                @endif
                                                @if ($user->property_details->property_name)
                                                    <p>Property Name :-
                                                        <b>{{ $user->property_details->property_name }}</b>
                                                    </p>
                                                @endif
                                                @if ($user->property_details->occupied == '0')
                                                    <p class="text-success">Currently Available</p>
                                                @else
                                                    <p class="text-danger">Not Available</p>
                                                @endif
                                                @if ($user->property_details->property_to == '0')
                                                    <p class="text-primary">Property for :- <b>Rent</b></p>
                                                @else
                                                    <p class="text-info">Property for :- <b>Sale</b></p>
                                                @endif
                                                @if ($user->property_details->category == '0')
                                                    <p class="text-primary">Property Type:<b>Residential</b></p>
                                                @else
                                                    <p class="text-info">Property Type:<b>Commercial</b></p>
                                                @endif
                                            @endif
                                            @if ($user->coupon_rel)
                                            @if ($user->coupon_rel->id)
                                                <p> Coupon :- <b>{{ $user->coupon_rel->coupon_code }}</b></p>
                                            @endif
                                            @if ($user->coupon_type == 0)
                                                <p> Percentage <b>{{ $user->coupon_rel->percentage }} %</b></p>
                                            @elseif($user->coupon_type == 1)
                                                <p> Amount:- <b>{{ $user->coupon_rel->amount }}</b></p>
                                            @endif
                                            @if ($user->coupon_rel->status == '1')
                                                <p class="text-success">Currently Available</p>
                                            @else
                                                <p class="text-danger">Not Available</p>
                                            @endif
                                            @if ($user->coupon_rel->expiry_date)
                                                <p class="text-primary">expiry Date :-
                                                    <b>{{ date('d-m-Y', strtotime($user->coupon_rel->expiry_date)) }}</b>
                                                </p>
                                            @endif

                                        @else
                                            <p class="text-danger"> There is No Coupon Applied </p>
                                        @endif
                                        @if ($user->offer_rel)
                                        @if ($user->offer_rel->id)
                                            <p>offer Package :- <b>{{ $user->offer_rel->offer_package_name }}</b>
                                            </p>
                                        @endif
                                        @if ($user->offer_rel->start_date)
                                            <p> Start Date:- <b>{{ $user->offer_rel->start_date }}</b></p>
                                        @endif
                                        @if ($user->offer_rel->end_date)
                                            <p> End Date:- <b>{{ $user->offer_rel->end_date }}</b></p>
                                        @endif
                                        @if ($user->offer_rel->status == '1')
                                            <p class="text-success">Currently Available</p>
                                        @else
                                            <p class="text-danger">Not Available</p>
                                        @endif
                                        @if ($user->offer_rel->actual_amount)
                                            <p class="text-primary">Actual Amount :-
                                                <b>{{ $user->offer_rel->actual_amount }}</b>
                                            </p>
                                        @endif
                                        @if ($user->offer_rel_discount_amount)
                                            <p class="text-info">Property for :-
                                                <b>{{ $user->offer_rel->discount_amount }}</b>
                                            </p>
                                        @endif
                                        @if ($user->property_details->category == '0')
                                            <p class="text-primary"><b>Residential</b></p>
                                        @else
                                            <p class="text-info"><b>Commercial</b></p>
                                        @endif
                                    @else
                                        <p class="text-danger"> There is No Package Offer </p>
                                    @endif
                                    <form class="form-admin" id="form-add" action="{{ route('approveBooking') }}" method="POST">
                                        <div class="row">
                                            <input type="hidden" id="booking_id" value="{{ $user->id }}" name="booking_id">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Move-In Expect : </label>
                                                    <input type="hidden" id="check_in" value="{{ $user->check_in }}"
                                                        name="check_in">
                                                    <b>{{ date('d-m-Y', strtotime($user->check_in)) }}</b>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                @if ($user->check_out)
                                                    <div class="form-group">
                                                        <label>Move-Out Date : </label>
                                                        <input type="hidden" id="check_out" value="{{ $user->check_out }}"
                                                            name="check_out">
                                                        <b>{{ date('d-m-Y', strtotime($user->check_out)) }}</b>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                @if ($user->booking_doc)
                                                    <div class="form-group">
                                                        <a href="{{ url($user->booking_doc->document) }}" target="_blank"
                                                            class="btn btn-primary float-right ">View Payment</a>
                                                    </div>
                                                @endif
                                            </div>


                                        </div>

                                        @if ($user->booking_doc || $user->is_verified == 1)
                                            @php
                                                $amount = '';
                                            @endphp
                                            @if ($user->property_details->property_to == '0')
                                                @php
                                                    $amount = $user->property_details->rent;
                                                @endphp
                                                <p class="text-primary"> Rental Amount :-
                                                    <b>{{ $user->property_details->rent }}</b>
                                                </p>
                                                <p class="text-primary"> Security Deposit :-
                                                    <b>{{ $user->property_details->security_deposit }}</b>
                                                </p>
                                                @if ($user->account_rel)
                                                    <p class="text-info"> Token Amount :-
                                                        <b>{{ $user->property_details->token_amount }}</b>
                                                    </p>
                                                @endif
                                                @php
                                                    if ($user->property_details) {
                                                        // if ($user->property_details->amount) {
                                                            $amount = ($user->property_details->rent+$user->property_details->security_deposit) - $user->property_details->token_amount;
                                                        // }
                                                        // dd($amount);
                                                    }
                                                @endphp
                                                @if ($user->offer_rel)
                                                    @php
                                                        if ($user->offer_rel) {
                                                            if ($user->offer_rel->discount_amount) {
                                                                $amount = $user->property_details->token - $user->offer_rel->discount_amount;
                                                            }
                                                        }
                                                    @endphp
                                                    <p class="text-primary"> After Package Applied :- <b>{{ $amount }}</b>
                                                    </p>
                                                @endif
                                                @if ($user->coupon_rel)
                                                    @php
                                                        if ($user->coupon_type == 0) {
                                                            $amount = ($user->coupon_rel->percentage / 100) * $user->property_details->rent;
                                                        } elseif ($user->coupon_type == 1) {
                                                            $amount = $user->property_details->rent - $user->coupon_rel->amount;
                                                        }
                                                    @endphp
                                                    <p class="text-primary"> After Coupon Applied :- <b>{{ $amount }}</b></p>

                                                @endif
                                            @else
                                                @php
                                                    if ($user->property_details) {
                                                        if ($user->property_details->rent) {
                                                            $amount = ($user->property_details->selling_price+$user->property_details->security_deposit) - $user->property_details->rent;
                                                        }
                                                    }
                                                @endphp
                                                @if ($user->coupon_rel)
                                                    @php
                                                        if ($user->coupon_type == 0) {
                                                            $amount = ($user->coupon_rel->percentage / 100) * $user->property_details->rent;
                                                        } elseif ($user->coupon_type == 1) {
                                                            $amount = $user->property_details->selling_price - $user->coupon_rel->amount;
                                                        }
                                                    @endphp
                                                    <p class="text-primary"> After Coupon Applied :- <b>{{ $amount }}</b></p>

                                                @endif
                                                @if ($user->property_details)
                                                    <p class="text-info"> Selling Amount :-
                                                        <b>{{ $user->property_details->selling_price }}</b>
                                                    </p>
                                                @endif

                                                @if ($user->property_details)
                                                    <p class="text-info"> Token Amount :-
                                                        <b>{{ $user->property_details->token_amount }}</b>
                                                    </p>
                                                @endif

                                            @endif
                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label>Balance Amount : </label>
                                                        <input class="form-control address" type="text" id="token"
                                                            value="{{ $amount }}" name="token">
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Last Date to pay balance : <span class="tx-danger">*</span></label>
                                                        <input class="form-control address" required id="due_date" name="due_date"
                                                            value="{{ $user->check_in }}" type="date">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button type="submit"
                                                        class="btn btn-primary float-right approveBtn">Approve</button>
                                                </div>
                                            </div>
                                        @else
                                            @if ($user->is_verified == 0)
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div>Payment not yet verified</div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div>Token amount not yet paid</div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    </form>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        {{-- <div class="tab-pane fade show" id="property-coupon" role="tabpanel" aria-labelledby="main-coupon">
                            <div class="row justify-content-between align-items-center mb-5">
                                <div class="col-md-auto col-12 ">
                                    <div class="col-md-12">
                                        <div class="property-details">

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="tab-pane fade show" id="property-offer" role="tabpanel" aria-labelledby="main-package">
                            <div class="row justify-content-between align-items-center mb-5">
                                <div class="col-md-auto col-12 ">
                                    <div class="col-md-12">
                                        <div class="property-details">

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="tab-pane fade show" id="approve-booking" role="tabpanel" aria-labelledby="main-approve">

                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>



    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

    <script>
        const url = '{{ url('/') }}';

    </script>
    <script>
        $("#form-add").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                due_date: {
                    required: true,
                },
                check_in: {
                    required: true,
                },
                check_out: {
                    required: true,
                },
                booking_id: {
                    required: true,
                },
                token: {
                    required: true,
                }
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add");
                var formData = new FormData(form);
                $(".approveBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('approveBooking') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".approveBtn").text('Approve');
                        $(".approveBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "{{ route('userBookings') }}";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

    </script>

@endsection
