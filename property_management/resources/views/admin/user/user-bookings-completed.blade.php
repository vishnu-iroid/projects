@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Documentation</span>
            <span class="breadcrumb-item active">Issued Contracts</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                                <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                                    <i class="fas fa-plus-square"></i>&nbsp Add Client
                                </button>
                        </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Renew/End Lease</h4>
                            <p class="card-category">List of all Issued Contracts </p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="data-table table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Tenant Name</th>
                                <th>Tenant Contact No</th>
                                <th>Property</th>
                                <th>Unit Name</th>
                                <th>Owner Name</th>
                                <th>Owner Contact No</th>
                                <th>Contract Start Date</th>
                                <th>Contract End Date</th>
                                {{-- <th>Property</th>
                                <th>Check-in</th>
                                <th>Check-Out</th>
                                <th>Property Status</th>
                               <th>Status</th> --}}
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @foreach ($users as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    @if ($row->user_rel)
                                        @if ($row->user_rel->name)
                                        <td>{{ $row->user_rel->name }}</td>
                                        @endif
                                    @endif
                                    @if ($row->user_rel)
                                    @if ($row->user_rel->phone)
                                    <td>{{ $row->user_rel->phone }}</td>
                                    @endif
                                @endif

                                    <td>
                                        @if ($row->prop_rel)
                                            {{ $row->prop_rel->property_name }} @endif
                                    </td>
                                    <td>@if ($row->check_in) {{ date('d-m-Y', strtotime($row->check_in)) }} @endif</td>
                                    <td>@if ($row->check_out)  {{ date('d-m-Y', strtotime($row->check_out)) }} @endif </td>
                                    <td>

                                            @if ($row->prop_rel->occupied == '0')
                                                Vacant
                                            @else
                                                occupied
                                            @endif

                                    </td>
                                   <td> @if ($row->status == '0' || $row->status == '1')
                                        <span class="badge rounded-pill bg-success text-white"> Booking Completed </span>
                                        @else

                                        @endif
                                 </tr>
                            @endforeach --}}
                        </tbody>
                    </table>

                </div>
                <!-- table-wrapper -->
                {{-- <a class="fad fa-upload tx-20 upload-button ml-1" href="javascript:0;"style="color:rgb(0, 119, 255)"onClick="contractModal('{{ $row->id }}','{{ $row->prop_rel->property_to }}',{{ $row->rent }})"title="Upload Contract"></a> @endif </td> --}}

            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>

    <!-- Reject Modal -->
    <div class="modal fade" id="verification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Document Verification</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to Verification?
                        <input type="hidden" name="id" id="id">

                        <!--Action -  1-unblock , 0- block -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class submitBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade contractUpload" id="contractUploadModal" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Upload Contract Details</h4>
                </div>
                <form action="javascript:;" id="contract-form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contract No</label>
                                    <input type="text" class="form-control" name="contract_no" id="contract_no">
                                </div>

                            </div>
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contract Type</label>
                                    <input class="form-control" id="contract_type" name="contract_type" type="text"
                                        placeholder="contract type" />
                                </div>
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="strat_title">Contract Start Date </label><span
                                        class="tx-danger">*</span>
                                    <input type="hidden" name="bookid" id="bookid">
                                    <input type="hidden" name="user_property_id" id="user_property_id">

                                    <input type="date" class="form-control fc-datepicker" name="contract_start_date"
                                        id="contract_start_date">
                                </div>
                            </div>
                            <div class="col-md-6 end_date">
                                <div class="form-group">
                                    <label>Contract End Date<span class="tx-danger">*</span></label>
                                    <input type="date" class="form-control fc-datepicker" name="contract_end_date"
                                        id="contract_end_date">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contract File</label>
                                    <input type="file" class="form-control" name="contract_file" id="contract_file">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contract Value</label>
                                    <input type="text" class="form-control" name="final_amount" id="final_amount">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. of Free Maintenance <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="free_maintenance" id="selling_price">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Maintenance Charge <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="maintenance_charge" id="mrp">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white contractcloseBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class contractBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection @section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('assets/lib/select2/js/select2.min.js') }}"></script>
    <script>
        const url = '{{ url('/') }}';
    </script>
    <script>
        $("#site_title").html(`Property | Bookings`);
        $("#user_book").addClass("active");
    </script>
    <script>
        $(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ route('issuedContract') }}",
                    data: function(d) {
                        d.search = $('input[type="search"]').val();
                        d.owner = $('#owner_id').val();
                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },
                    {
                        data: 'tanent',
                        name: 'tanent'
                    },
                    {
                        data: 'tanent_contact',
                        name: 'tanent_contact'
                    },
                    {
                        data: 'property',
                        name: 'property'
                    },
                    {
                        data: 'unit',
                        name: 'unit'
                    },
                    //  {  data:'agent',   name:'agent'},
                    //  {  data:'agent_contact',name:'agent_contact'},
                    //  {  data:'property',   name:'property'},
                    //  {  data:'frequency',name:'frequency'},
                    //  {  data:'check_in',name:'check_in'},
                    //  {  data:'check_out',name:'check_out'},
                    //  {  data:'security_deposit',name:'security_deposit'},
                    {
                        data: 'owner',
                        name: 'owner'
                    },
                    {
                        data: 'owner_contact',
                        name: 'owner_contact'
                    },
                    {
                        data: 'contract_start_date',
                        name: 'contract_start_date'
                    },
                    {
                        data: 'contract_end_date',
                        name: 'contract_end_date'
                    },

                    //  {  data: 'ownership_doc', name: 'ownership_doc',render: function( data, type, full, meta ) {
                    //     return '<a href="'+ data +'" class="btn btn-warning btn-xs mrg"><i class=" far fa-file-alt"></i></a>';
                    //  }},
                    //  {  data:'requested_date',name:'requested_date'},
                    {
                        data: 'actions',
                        name: 'actions'
                    }
                ]
            });
            $('.filter').change(function() {
                table.draw();
            });

        });
    </script>
    <script>
        // function contractModal(bookid, property_to, amount) {


        //     $('#bookid').val(bookid);
        //     if (property_to == 1) {
        //         $('.strat_title').text('Contract Issued Date');
        //         $('.end_date').hide();
        //     }
        //     $('#final_amount').val(amount);
        //     $('#contractUploadModal').modal()
        // }
        function renewContractFunction(bookid, property_to, amount,contract_no) {
            $('#bookid').val(bookid);

            if (property_to == 1) {
                $('.strat_title').text('Contract Issued Date');
                $('.end_date').hide();
            }
            $('#final_amount').val(amount);
            $('#contractUploadModal').modal()
        }


        function verification(bookid) {
            $('#id').val(bookid);
            $('#verification').modal();

        }
    </script>
    <script>
        $('.submitBtn').click(function() {
            var propertyId = $('#id').val()
            $.ajax({
                url: url + "/property_verification/" + propertyId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                }
            })
            return false
        })
    </script>

    <script>
        $('.upload-button').click(function() {
            var booked_id = $(this).data('property');
            $('#bookedid').val(booked_id);
            $('#contractUploadModal').modal();
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                container: '#contractUploadModal modal-body'
            });
        });
        $("#contract-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                contract_start_date: {
                    required: true,
                },
                contract_end_date: {
                    required: true,
                }


            },
            submitHandler: function(form) {
                var form = document.getElementById("contract-form");
                var formData = new FormData(form);
                $(".contractBtn").prop("disabled", true);
                $(".contractcloseBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('editUserBookingContract') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".contractBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".contractBtn").text('Submit');
                        $(".contractBtn").prop("disabled", false);
                        $(".contractcloseBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>
@endsection
