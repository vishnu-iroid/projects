@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Bookings</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Bookings</h4>
                        <p class="card-category">List of all bookings</p>
                    </div>
                    <div class="col-md-6 row justify-content-center">
                        <a href="{{ route('agentBookings',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-align-justify tx-20"></i>&nbspProperty Visit
                        </a>
                        <a href="{{ route('agentBookings',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-check-circle tx-20"></i>&nbspBooked
                        </a>
                        <a href="{{ route('agentBookings',['status' => 2]) }}" class="nav-link tx-white d-flex align-items-center">
                            <i class="fad fa-check-circle tx-20"></i>&nbspCompleted
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                        <center><label>CATEGORY</label></center><select class="form-control filter" id="category">
                            <option selected disabled>SELECT ONE</option>
                            <option value="0">RESIDENTIAL</option>
                            <option value="1">COMMERICIAL</option>
                        </select>
                    </div>
                    <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                        <center><label>OWNER</label></center>
                        <select class="form-control filter" id="owner_id">
                            <option selected disabled>SELECT ONE</option>
                            @foreach ($owners as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div id="datatableclient_filter" class="dataTables_filter col-md-3"><center><label>AREA</label></center><input type="search" class="filter" placeholder="" aria-controls="datatableclient"></div> --}}
                    <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                        <center><label>TYPE</label></center> <select class="form-control filter" id="type">
                            <option selected disabled>SELECT ONE</option>

                        </select>

                    </div>


                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive wrap">
                    <thead>
                        <tr>
                            <th class="wd-10p">Serial No</th>
                            <th class="wd-10p">User Name</th>
                            <th class="wd-10p">Property</th>
                            <th class="wd-10p">Agent</th>
                            <th class="wd-20p">Requested</th>
                            <th class="wd-10p"> Status</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $row)
                        <tr>

                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->user_rel->name }}</td>
                            <td>{{ $row->property_reg_no }} </td>

                            <td>{{ $row->agent_id}}</td>
                            <td>{{ $row->booked_date }} /{{ $row->time_range}}</td>
                            <td>
                                @if($row->tstatus == "0")
                                Not Started
                                @elseif($row->tstatus == "1")
                                vist-started
                                @endif
                            </td>
                            <td>
                                <!-- <a class="fad fa-eye tx-20 view-button mr-1" href="}}" data-booking="{{$row->id}}" title="View"></a>
                                <a class="fad fa-ban tx-20 cancel-button mr-1" style="" href="javascript:0;" data-booking="{{$row->id}}" title="Cancel"></a> -->
                            </td>
                        </tr>

                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $users->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
</div>

<!-- Reject Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Block Admin</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to cancel this booking?
                    <input type="hidden" name="adminId" id="bookId">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class deletetBtn">Cancel Booking</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script>
    const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Bookings`);
    $("#user_book").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });



    $('.cancel-button').click(function() {
        var id = $(this).attr('data-booking');
        $('#bookId').val(id);
        $('#cancelModal').modal()
    });

    $('.deletetBtn').click(function() {
        var id = $('#bookId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/booking/cancel/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".addBtn").text('Processing..');
            },
            success: function(data) {
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                toastr["success"](data.response);
                setTimeout(function() {
                    window.location.href = "";
                }, 1000);
            },
        });
    });
</script>

@endsection
