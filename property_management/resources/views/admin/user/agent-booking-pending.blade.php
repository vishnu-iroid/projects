@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Sales</span>
        <span class="breadcrumb-item active">Customer Visit Request</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Customer Visit Request</h4>
                        <p class="card-category">List of all Visit Request</p>
                    </div>
                    <div class="col-md-6 row justify-content-center">
                        <a href="{{ route('agentBookings',['status' => 0]) }}" class=" nav-link tx-white d-flex align-items-center card-active">
                            <input type="hidden" class="filter booking-status" value="0" />
                            <i class="fad fa-align-justify tx-20"></i>&nbspProperty Visit
                        </a>
                        <a href="{{ route('agentBookings',['status' => 1]) }}" class="booking-status nav-link tx-white d-flex align-items-center">
                            <input type="hidden" class="filter booking-status" value="1" />

                            <i class="fad fa-check-circle tx-20"></i>&nbspProceed Booking
                        </a>
                        <a href="{{ route('agentBookings',['status' => 2]) }}" class="booking-status nav-link tx-white d-flex align-items-center">
                            <input type="hidden" class="filter booking-status" value="2" />

                            <i class="fad fa-check-circle tx-20"></i>&nbspCompleted
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="datatableclient_filter" class="dataTables_filter col-md-3">
                      <label>Date</label>
                        <input type="date" class="form-control filter" name="" id="tour_data">
                    </div>
                    <div id="datatableclient_filter" class="dataTables_filter col-md-3">
                      <label>Property</label>
                        <select class="form-control filter" id="property_id">
                            <option selected disabled>Select One</option>
                            @foreach ($property as $pr)
                                <option value="{{ $pr->id }}">{{ $pr->property_reg_no }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div id="datatableclient_filter" class="dataTables_filter col-md-3">
                      <label>Owner</label>
                        <select class="form-control filter" id="owner_id">
                            <option selected disabled>Select One</option>
                            @foreach ($owners as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div id="datatableclient_filter" class="dataTables_filter col-md-3"><label>AREA</label><input type="search" class="filter" placeholder="" aria-controls="datatableclient"></div> --}}
                    <div id="datatableclient_filter" class="dataTables_filter col-md-3">
                      <label>Agents</label> <select class="form-control filter" id="type">
                            <option selected disabled>Select One</option>
                                @foreach ($agents as $ag)
                                 <option value="{{ $ag->id }}">{{ $ag->name }}</option>
                                @endforeach
                        </select>

                    </div>
                </div>
            </div>

            <div class="card-body">
                <table id="datatable1" class="table yajra-datatable "style="width:100%">

                    <thead>
                        <tr>
                            <th>Serial No</th>
                            <th>User Name</th>
                            <th>Property</th>
                            <th>Unit</th>
                            <th>Agent</th>
                            <th>Requested</th>
                            <th>Status</th>
                             <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
</div>

<!-- Reject Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Block Admin</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to cancel this booking?
                    <input type="hidden" name="adminId" id="bookId">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class deletetBtn">Cancel Booking</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Tour Booking Detail</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                   <p>Booked Date : <b id="date">  </b>  </p>
                   <p>Booked Time : <b id="time"></b></p>
                   <p>Property : <b id="property_name">  </b>  </p>
                   <p>Property Type : <b id="property_to"></b></p>
                   <p>Rent : <b id="rent"></b> </p>
                   <hr>
                   <br>
                   <p>Applicant  : <b id="user_name">  </b>  </p>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection @section('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script>
    const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Tour | Bookings`);
    $("#user_book").addClass("active");
    $(function () {

    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [
            'print'
        ],
        ajax: {
                url: "{{ route('agentBookings') }}",
                data: function (d)
                {
                    d.status    = $('.booking-status').val();
                    d.agent     = $('#agent').val();
                    d.date      = $('#tour_data').val();
                    d.owner     = $('#owner_id').val();
                    d.property  = $('#property_id').val();
                }
            },
        columns: [
            {data:'ids', name: 'ids'},
            {data:'user', name:'user'},
            {data:'property_name',name:'property_name'},
            {data:'unit',name:'unit'},
            {data:'agent',name:'agent'},
            {data:'booked_date',name:'booked_date'},
            {data:'status',name:'status'},
            {
                data: 'action',
                name: 'action',
                orderable: true,
            },
        ]
    });
    $('.filter').change(function(){
          table.draw();
      });
    });

    $('.view-button').click(function()
    {
        alert('view')
    })

    $('.cancel-button').click(function() {
        var id = $(this).attr('data-booking');
        $('#bookId').val(id);
        $('#cancelModal').modal()
    });

    function viewFunction(id)
    {
        $.ajax({
            type: "GET",
            url: url + "/agent-bookings/details/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function()
            {
               console.log('fetch')
            },
            success: function(data) {
                if (data.status === true) {
                    $('#date').html(data.response.date);
                    $('#time').html(data.response.time);
                    $('#property_name').html(data.response.property_reg_no);
                    $('#rent').html(data.response.rent)
                    if(data.response.property_to == 1)
                    {
                        $('#property_to').html("RENT");
                    }
                    else
                    {
                        $('#property_to').html("BUY");
                    }
                    $('#user_name').html(data.response.user_rel.name);

                    $('#viewModal').modal();
                }
            },
        });
    }
    $('.deletetBtn').click(function() {
        var id = $('#bookId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/booking/cancel/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".addBtn").text('Processing..');
            },
            success: function(data) {
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                toastr["success"](data.response);
                setTimeout(function() {
                    window.location.href = "";
                }, 1000);
            },
        });
    });
</script>

@endsection
