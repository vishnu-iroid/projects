@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Documentation</span>
            <span class="breadcrumb-item active">Document Verification</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
                <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                    <i class="fas fa-plus-square"></i>&nbsp Add Client
                </button>
        </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Document Verification</h4>
                            <p class="card-category">List of all Document Verification</p>
                        </div>
                        {{-- <div class="col-md-6 row justify-content-center">
                            <a href="{{ route('userBookings', ['status' => 0]) }}"
                                class="nav-link tx-white d-flex align-items-center ">
                                <i class="fad fa-align-justify tx-20"></i>&nbspBookings
                            </a>
                            <a href="{{ route('userBookings', ['status' => 1]) }}"
                                class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fad fa-check-circle tx-20"></i>&nbspDocument Verification
                            </a>
                            <a href="{{ route('userBookings', ['status' => 2]) }}"
                                class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-ban tx-20"></i>&nbspUpload Contract
                            </a>

                        </div> --}}
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="">Id</th>
                                <th class="">Applicant Name</th>
                                <th class="">Agent Name</th>
                                <th class="">Property</th>
                                <th class="">Unit</th>
                                <th class="">Requested</th>
                                <th class="">Doc</th>
                                <th class="">Property Status</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->user_rel->name }}</td>
                                    <td>{{isset($row->prop_rel->assigned_agent_details->agent_name)? $row->prop_rel->assigned_agent_details->agent_name:"---"}}</td>
                                    {{-- <td>
                                        @if ($row->prop_rel)
                                            {{-- {{ $row->prop_rel->property_name }} @endif -}}
                                    </td> --}}
                                    @if ($row->prop_rel->builder_id != null)
                                            @php
                                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->prop_rel->builder_id)->first();
                                            @endphp
                                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                        <td>{{ $row->prop_rel->property_name }}</td>
                                    @else
                                        <td>{{ $row->prop_rel->property_name }}</td>
                                        <td>--</td>
                                    @endif
                                    <td>In - {{ date('d-m-Y', strtotime($row->check_in)) }}<br>
                                        @if ($row->check_out) Out -
                                            {{ date('d-m-Y', strtotime($row->check_out)) }} @endif
                                    </td>
                                    @if ($row->doc_rel)
                                        <td>
                                            @foreach ($row->doc_rel as $key)
                                                <a href="{{ url($key->document) }}" class="btn btn-warning btn-xs mrg "
                                                    data-placement="top" data-toggle="tooltip"
                                                    data-original-title="Download"><i class=" far fa-file-alt"></i></a>
                                            @endforeach

                                        </td>
                                    @endif
                                    <td>
                                        @if ($row->prop_rel)
                                            @if ($row->prop_rel->occupied == '0')
                                                Vacant
                                            @else
                                                occupied
                                            @endif
                                        @endif

                                        <a class="fad fa-check tx-20 upload-button ml-1 btn btn-primary mb-1 bt_cus" href="javascript:0;"
                                            data-property="{{ $row->id }}" 
                                            onClick="verification('{{ $row->id }}')" title="Document Verification"></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $users->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>

    <!-- Reject Modal -->
    <div class="modal fade" id="verification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Document Verification</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to Verification?
                        <input type="hidden" name="id" id="id">

                        <!--Action -  1-unblock , 0- block -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class submitBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{-- <div class="modal fade contractUpload" id="contractUploadModal" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Upload Contract Details</h4>
                </div>
                <form action="javascript:;" id="contract-form" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="strat_title">Contract Start Date </label><span class="tx-danger">*</span>
                                    <input type="hidden" name="bookid" id="bookid">
                                    <input type="date" class="form-control fc-datepicker" name="contract_start_date"
                                        id="contract_start_date">
                                </div>
                            </div>
                            <div class="col-md-6 end_date">
                                <div class="form-group">
                                    <label>Contract End Date<span class="tx-danger">*</span></label>
                                    <input type="date" class="form-control fc-datepicker" name="contract_end_date"
                                        id="contract_end_date">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contract File</label>
                                    <input type="file" class="form-control" name="contract_file" id="contract_file">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Final Amount</label>
                                    <input type="text" class="form-control" name="final_amount" id="final_amount">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. of Free Maintenance <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="free_maintenance" id="selling_price">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Maintenance Charge <span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="maintenance_charge" id="mrp">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white contractcloseBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class contractBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script> --}}
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script>
        const url = '{{ url('/') }}';

    </script>
    <script>
        $("#site_title").html(`Property | Bookings`);
        $("#user_book").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                autoWidth:false,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                // dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });

    </script>
    <script>
        function contractModal(bookid, property_to, amount) {


            $('#bookid').val(bookid);
            if (property_to == 1) {
                $('.strat_title').text('Contract Issued Date');
                $('.end_date').hide();
            }
            $('#final_amount').val(amount);
            $('#contractUploadModal').modal()
        }


        function verification(bookid) {
            $('#id').val(bookid);
            $('#verification').modal();

        }

    </script>
    <script>
        $('.submitBtn').click(function() {
            var propertyId = $('#id').val()
            $.ajax({
                url: url + "/property_verification/" + propertyId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                }
            })
            return false
        })

    </script>

    <script>
        $('.upload-button').click(function() {
            var booked_id = $(this).data('property');
            $('#bookedid').val(booked_id);
            $('#contractUploadModal').modal();
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                container: '#contractUploadModal modal-body'
            });
        });
        $("#contract-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                contract_start_date: {
                    required: true,
                },
                contract_end_date: {
                    required: true,
                }


            },
            submitHandler: function(form) {
                var form = document.getElementById("contract-form");
                var formData = new FormData(form);
                $(".contractBtn").prop("disabled", true);
                $(".contractcloseBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('addUserBookingContract') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".contractBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".contractBtn").text('Submit');
                        $(".contractBtn").prop("disabled", false);
                        $(".contractcloseBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

    </script>

@endsection
