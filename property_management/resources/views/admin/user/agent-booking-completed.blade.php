@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Bookings</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Bookings</h4>
                        <p class="card-category">List of all bookings</p>
                    </div>
                    <div class="col-md-6 row justify-content-center">
                        <a href="{{ route('agentBookings',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-align-justify tx-20"></i>&nbspBooking
                        </a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                        <center><label>CATEGORY</label></center><select class="form-control filter" id="category">
                            <option selected disabled>SELECT ONE</option>
                            <option value="0">RESIDENTIAL</option>
                            <option value="1">COMMERICIAL</option>
                        </select>
                    </div>
                    <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                        <center><label>OWNER</label></center>
                        <select class="form-control filter" id="owner_id">
                            <option selected disabled>SELECT ONE</option>
                            @foreach ($owners as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div id="datatableclient_filter" class="dataTables_filter col-md-3"><center><label>AREA</label></center><input type="search" class="filter" placeholder="" aria-controls="datatableclient"></div> --}}
                    <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                        <center><label>TYPE</label></center> <select class="form-control filter" id="type">
                            <option selected disabled>SELECT ONE</option>

                        </select>

                    </div>


                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive wrap">

                    <thead>
                        <tr>
                            <th class="wd-10p">Serial No</th>
                            <th class="wd-10p">User Name</th>
                            <th class="wd-10p">Property</th>
                            <th class="wd-10p">Agent</th>
                            <th class="wd-20p">Requested</th>
                            <th class="wd-10p"> Status</th>

                        </tr>
                    </thead>
                    <tbody>
                        {{-- @foreach($users as $row)
                        <tr>

                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->user_rel->name }}</td>
                            <td>{{ $row->property_reg_no }} </td>

                            <td>{{ $row->agent_id}}</td>
                            <td>{{ $row->booked_date }} /{{ $row->time_range}}</td>
                            <td>
                                @if($row->tstatus == "0")
                                Not Started
                                @elseif($row->tstatus == "1")
                                vist-started
                                @endif
                            </td>
                            <td>
                                <!-- <a class="fad fa-upload tx-20 upload-button ml-1" href="javascript:0;" data-property="{{$row->id}}" style="color:rgb(0, 119, 255)" title="Upload Contract"></a> -->
                                <!-- <a class="fad fa-eye tx-20 view-button mr-1" href="{{route('bookingDetails', ['id' => $row->id])}}" data-booking="{$row->id" title="View"></a> -->
                                <!-- <a class="fad fa-ban tx-20 cancel-button mr-1" style="" href="javascript:0;" data-booking="{$row->id" title="Cancel"></a> -->
                            </td>
                        </tr>

                        @endforeach --}}
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {{-- {!! $users->links() !!} --}}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
</div>

<!-- Reject Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Block Admin</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to cancel this booking?
                    <input type="hidden" name="adminId" id="bookId">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class deletetBtn">Cancel Booking</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade contractUpload" id="contractUploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Upload Contract Details</h4>
            </div>
            <form action="javascript:;" id="contract-form" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <!-- <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract No</label>
                                <input type="text" class="form-control" name="contract_no" id="contract_no">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract Type</label>
                                <input class="form-control" id="contract_type" name="contract_type" type="text" placeholder="contract type"/>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract Start Date<span class="tx-danger">*</span></label>
                                <input type="text" name="bookedid" id="bookedid">
                                <input type="text" class="form-control fc-datepicker" name="contract_start_date" id="contract_start_date">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract End Date<span class="tx-danger">*</span></label>
                                <input type="text" class="form-control fc-datepicker" name="contract_end_date" id="contract_end_date">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contract File</label>
                                <input type="file" class="form-control" name="contract_file" id="contract_file">
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label>Property To<span class="tx-danger">*</span></label>
                                <select class="form-control select2 agent-select" name="property_to" id="property_to" onchange="properties(this.value)">
                                    <option value="">Select</option>
                                    <option value="0">Rent</option>
                                    <option value="1">Buy</option>
                                </select>
                            </div>
                        </div> -->
                    </div>
                    <!-- <div class="row frequency_rent" style="display: none">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Rent<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="rent" id="rent">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Security Deposit<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="security_deposit" id="security_deposit">
                                </div>
                            </div>
                        </div>
                        <div class="row sellingprice" style="display: none">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Selling Price<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="selling_price" id="selling_price">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>MRP<span class="tx-danger">*</span></label>
                                    <input type="text" class="form-control" name="mrp" id="mrp">
                                </div>
                            </div>
                        </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white contractcloseBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class contractBtn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script>
    const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Bookings`);
    $("#user_book").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });



    $('.cancel-button').click(function() {
        var id = $(this).attr('data-booking');
        $('#bookId').val(id);
        $('#cancelModal').modal()
    });

    $('.deletetBtn').click(function() {
        var id = $('#bookId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/booking/cancel/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".addBtn").text('Processing..');
            },
            success: function(data) {
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                toastr["success"](data.response);
                setTimeout(function() {
                    window.location.href = "";
                }, 1000);
            },
        });
    });

    $('.upload-button').click(function() {
        var property_id = $(this).data('property');
        $('#bookedid').val(property_id);
        $('#contractUploadModal').modal();
        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            container: '#contractUploadModal modal-body'
        });
    });
    $("#contract-form").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            contract_start_date: {
                required: true,
            },
            contract_end_date: {
                required: true,
            },
            bookedid: {
                required: true,
            }

        },
        submitHandler: function(form) {
            var form = document.getElementById("contract-form");
            var formData = new FormData(form);
            $(".contractBtn").prop("disabled", true);
            $(".contractcloseBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addAgentBookingContract')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".contractBtn").text('Processing..');
                },
                success: function(data) {
                    $(".contractBtn").text('Submit');
                    $(".contractBtn").prop("disabled", false);
                    $(".contractcloseBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>

@endsection
