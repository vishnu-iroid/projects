  @extends('admin.layout.app')
  <style>
      .br-section-wrapper-dept {
          background-color: #fff;
          padding: 10px 10px;
          box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
          border-radius: 6px;
      }

      .select2-container {
          z-index: 10050;
          width: 100% !important;
          padding: 0;
      }

      .modal {
          overflow: auto !important;
      }

      .card {
          position: relative;
          display: flex;
          flex-direction: column;
          min-width: 0;
          word-wrap: break-word;
          background-color: #fff;
          background-clip: border-box;
          border: 1px solid #eee;
          border-radius: .25rem;
      }

      .h4-card {
          font-size: 1.125rem;
          font-weight: 300 !important;
      }
  </style>
  @section('content')
  <div class="br-pageheader">
      <nav class="breadcrumb pd-0 mg-0 tx-12">
          <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
          <a class="breadcrumb-item" href="{{route('userBookings')}}">Bookings</a>
          <span class="breadcrumb-item active">Details</span>
      </nav>
  </div>
  <!-- br-pageheader -->
  <!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
  <div class="br-pagebody">
      <div class="col-md-12 mg-b-30 mg-t-60">
          <div class="card shadow">
              <div class="card-header card-header-blueheader">
                  <div class="row d-flex justify-content-between">
                      <div class="col-md-4">
                          <h4 class="card-title h4-card">Service Request Details</h4>
                          <p class="card-category"></p>
                      </div>
                  </div>
              </div>
              <div class="card-body">
                  <div class="card-header card-header-tabs-line">
                      <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
                          <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" id="main-property" href="#property-status" role="tab" aria-controls="property-status" aria-selected="true">Service Details</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" id="main-approve" href="#approve-booking" role="tab" aria-selected="false"></a>
                          </li>
                      </ul>
                  </div>

                  <div class="tab-content pt-3">
                      <div class="tab-pane fade show active" id="property-status" role="tabpanel" aria-labelledby="main-property">
                          <div class="row justify-content-between align-items-center mb-8">
                              <div class="col-md-auto col-12 ">
                                  <div class="col-md-12">
                                      <div class="property-details">
                                          @if($datas->user_related)

                                          @if($datas->user_related->name)
                                          <p>User :- <b>{{$datas->user_related->name}}</b></p>
                                          <p>User Contact:- <b>{{$datas->user_related->phone}}</b></p>

                                          @endif
                                          @endif

                                          @if($datas->service_related)
                                          <p>Requested Service :- <b>{{$datas->service_related->service}}</b></p>
                                          @endif
                                          @if($datas->time)<p>Requested Time :- <b>{{$datas->time}}</b></p> @endif
                                          @if($datas->date)<p>Requested Date :- <b>{{$datas->date}}</b></p> @endif
                                          @if($datas->description)<p>Description :- <b>{{$datas->description}}</b></p> @endif
                                          @if($datas->prop_related)
                                          @if($datas->prop_related->property_name)
                                          <p>Property Name :- <b>{{$datas->prop_related->property_name}}</b></p>

                                          @endif
                                          @endif

                                          @if($datas->status == 0 && $datas->user_service_related = '')
                                          <p>Owner Approval :-
                                              <a class=" tx-20 reject-button" href="javascript:0;" data-property="" title="send owner approval" style="color: green " onclick="ApprovelFunction('{{$datas->id}}','1')">
                                                  <button class="btn btn-primary float-right approveBtn"> Send Owner Approve</button>
                                              </a>
                                          </p>
                                          @elseif($datas->user_service_related)
                                          @if($datas->user_service_related->status == 1 )

                                          <p>Inspection :-
                                              <a class="fad  tx-20 reject-button" href="javascript:0;" data-property="" title="Select Inspection Team" style="color: green " onclick="Inspection('{{$datas->id}}','1')">
                                                  <button class="btn btn-primary float-right approveBtn">Send For Inspection</button>
                                              </a>
                                          </p>
                                          @elseif($datas->user_service_related->status == 0)
                                          <p class="text-primary"> Request Is In Waiting List </p>
                                          @elseif($datas->user_service_related->status == 2)
                                          <p class="text-danger"> Request Was Rejected By Owner</p>
                                          @endif
                                          @else
                                          <p>Inspection :-
                                              <a class="fad  tx-20 reject-button" href="javascript:0;" data-property="" title="Select Inspection Team" style="color: green " onclick="Inspection('{{$datas->id}}','1')">
                                                  <button class="btn btn-primary float-right approveBtn">Send For Inspection</button>
                                              </a>
                                          </p>

                                          @endif





                                      </div>
                                  </div>

                              </div>

                          </div>
                      </div>
                      <div class="tab-pane fade show" id="approve-booking" role="tabpanel" aria-labelledby="main-approve">

                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- br-section-wrapper -->
  </div>
  <!-- br-pagebody -->

  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
  </div>

  <!-- Owner Approval Request -->
  <div class="modal fade" id="ApprovelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content delete-popup">
              <div class="modal-header">
                  <h4 class="modal-title tx-danger reject-modal-title">Send Approvel Request</h4>
              </div>
              <form action="javascript:;" id="delete-form">
                  <div class="modal-body">
                      Approval Request Send to Owner
                      <input type="hidden" name="ownerId" id="ownerId">
                      <input type="hidden" name="action" id="action">

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                      <button type="submit" class="btn btn-danger reject-class sendApprovelBtn">Send</button>
                  </div>
              </form>
          </div>
      </div>
  </div>

  @endsection @section('scripts')
  <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

  <script>
      const url = '{{url("/")}}';
  </script>
  <script>
      function ApprovelFunction(id, action) {

          $('#ownerId').val(id);
          $('#action').val(action);
          if (action == 1) {
              $('.reject-modal-title').text('Owner Approval Request');
              $('.sendApprovelBtn').text('Submit');
          }
          $('#ApprovelModal').modal()
      }
  </script>
  <script>
      $('.sendApprovelBtn').click(function() {
          var id = $('#ownerId').val()
          var action = $('#action').val()


          $(".sendApprovelBtn").prop("disabled", true);
          $(".closeBtn").prop("disabled", true);
          $.ajax({
              type: "GET",
              url: url + "/owner/approval/" + id + '/' + action,
              processData: false,
              async: true,
              headers: {
                  "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
              },
              beforeSend: function() {

                  $(".sendApprovelBtn").text('Processing..');
              },
              success: function(data) {

                  toastr["success"](data.response);
                  setTimeout(function() {
                      window.location.href = "";
                  }, 1000);

              },
              error: function(xhr, data) {
                  console.log(xhr);
              }

          });
      })
  </script>

  @endsection
