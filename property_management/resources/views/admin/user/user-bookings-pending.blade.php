@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Documentation</span>
            <span class="breadcrumb-item active">Reservation Requests</span>
        </nav>
    </div>
    <!-- br-pageheader -->

    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Reservation Requests</h4>
                            <p class="card-category">List of all Reservation Requests</p>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive wrap">
                        <thead>
                            <tr>
                                <th class="">Id</th>
                                <th class="">Applicant Name</th>
                                <th class="">Applicant Contact No</th>
                                <th class="">Property Name</th>
                                <th class="">Unit No/Name</th>
                                
                                <th class="">Assigned Agent</th>
                                <th class="">Agent Contact No</th>
                                <th class="">Check In</th>
                                <th class="">Check Out</th>
                                <th class="">Net Amount</th>
                                <th class="">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                // dd($users);
                            @endphp
                            @foreach ($users as $row)

                                <tr>
                                    <td>{{ $loop->iteration }}</td>

                                    <td>
                                        @if ($row->user_details->name)
                                            {{ $row->user_details->name }}
                                        @else
                                            {{--  --}}
                                        @endif
                                    </td>

                                    <td>
                                        @if ($row->user_details->phone)
                                            {{ $row->user_details->phone }}
                                        @else
                                            {{--  --}}
                                        @endif
                                    </td>
                                    @if ($row->property_relation->builder_id != null)
                                            @php
                                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->property_relation->builder_id)->first();
                                            @endphp
                                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                        <td>{{ $row->property_relation->property_name }}</td>
                                    @else
                                        <td>{{ $row->property_relation->property_name }}</td>
                                        <td>--</td>
                                    @endif

                                    <td>
                                        @if ($row->agent_name)
                                            {{ $row->agent_name }}
                                        @else
                                            ----
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->phone)
                                            {{ $row->phone }}
                                        @else
                                            ----
                                        @endif
                                    </td>

                                    <td>{{ date('d-m-Y', strtotime($row->check_in)) }} </td>
                                    <td>
                                        @if ($row->check_out)
                                            {{ date('d-m-Y', strtotime($row->check_out)) }}
                                        @endif
                                    </td>
                                    {{-- <td>
                                @if ($row->property_details)
                                @if ($row->property_details->occupied == '0')
                                Vacant
                                @else
                                occupied
                                @endif
                                @endif
                            </td> --}}
                                    <td>
                                        @if (isset($row->property_details))
                                            @php
                                                $netAmount = $row->property_details->security_deposit + $row->property_details->rent;
                                            @endphp
                                            {{ number_format($netAmount,2) }}
                                        @else
                                            'nil'
                                        @endif
                                    </td>
                                    <td>

                                        <a class="fad fa-eye tx-20 view-button mb-1 btn btn-primary bt_cus"
                                            href="{{ route('bookingDetails', ['id' => $row->id]) }}"
                                            data-booking="{{ $row->id }}" title="View"></a>
                                        <!-- <a class="fad fa-ban tx-20 mb-1 btn btn-danger bt_cus" href="javascript:0;" title="Reject" 
                                            onclick="deleteFunction('{{ $row->id }}')"></a> -->

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $users->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="overflow: hidden">
    </div>
    <!-- Reject Modal -->
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Cancel Booking</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to cancel this booking?
                        <input type="hidden" name="adminId" id="bookId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtn">Cancel Booking</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

    <script>
        const url = '{{ url('/') }}';
    </script>
    <script>
        $("#site_title").html(`Property | Bookings`);
        $("#user_book").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: false,
                
                autoWidth: false,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                // dom: "Bfrtip",
            });
            // Select2
            // $(".dataTables_length select").select2({
            //     minimumResultsForSearch: Infinity,
            // });
        });



        function deleteFunction(id) {
           
            $('#bookId').val(id);
            $('#cancelModal').modal();
        };

        $('.deletetBtn').click(function() {
            var id = $('#bookId').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/booking/cancel/" + id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                },
            });
        });
    </script>
@endsection
