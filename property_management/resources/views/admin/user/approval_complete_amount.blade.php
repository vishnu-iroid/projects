@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Accounts Management</span>
        <span class="breadcrumb-item active">Closing Rent Collection</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Closing Rent Collection</h4>
                        <p class="card-category">List of Collections</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="data-table table display wrap">
                    <thead>
                        <tr>
                            {{-- <th>Id</th> --}}
                            <th>Applicant Name</th>
                            <th>Contact No</th>
                            <th>Property</th>
                            <th>Unit</th>
                            <th>Rent</th>
                            <th>Net Amount</th>
                            <th>Advance Payment</th>
                            <th>Received Amount</th>
                            <th>Balance </th>
                            <th>Attachment</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
            <!-- table-wrapper -->
        </div>
        <!-- Accept Modal -->
<div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-success reject-modal-title">Approve Pay Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to Approve this Payment?
                    <input type="hidden" name="agentId" id="agentId">
                    <input type="hidden" name="id" id="id">
                    <!--Action -  1-accept , 0- reject -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-success reject-class AcceptBtn">Approve</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-danger reject-modal-title">Reject Request</h4>
            </div>
            <form action="javascript:;" id="delete-form">
                <div class="modal-body">
                    Are you sure you want to reject this request?
                    <input type="hidden" name="documentId" id="documentId">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-danger reject-class deletetBtn">Reject</button>
                </div>
            </form>
        </div>
    </div>
</div>
    </div>
    <!-- br-section-wrapper -->

</div>
@endsection
@section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>

    <script>

    $(function () {
            var table = $('.data-table').DataTable({
            processing: true,
            searching: false,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4],
                        modifer: {
                            page: 'all',
                        }
                    }
                }],
            ajax: {
                url: "{{ route('completeUserTour') }}",
                data: function (d) {
                    d.search    = $('input[type="search"]').val();
                    d.owner     = $('#owner_id').val();
                }
            },
            
            columns: [
              
                    //  {  data: 'id', name: 'id',orderable: true},
                     {  data:'user',name:'user'},
                     {  data:'user_contact',name:'user_contact'},
                     {  data:'property',name:'property'},
                     {  data:'unit',name:'unit'},
                     {  data:'rent_amount',name:'rent_amount'},
                     {  data:'net_amount',name:'net_amount'},
                     {  data:'advance_payment',name:'advance_payment'},
                     {  data:'received_payment',name:'received_payment'},
                     {  data:'balance_amount',name:'balance_amount'},
                     {  data: 'imageurl', name: 'imageurl',render: function( data, type, full, meta ) {
                        //  <i class=" far fa-file-alt"></i></a>
                        return '<a href="'+ data +'" class="btn btn-warning btn-xs mrg"><i class=" far fa-file-alt"></i></a>';
                     }},
                     { data: 'action', name:'action'}

                 ]
             });
             $('.filter').change(function() {
                 table.draw();
             });

        });
    </script>
    <script>
        const url = '{{url("/")}}';
        function AcceptFunction(id) {
        $('#agentId').val(id);
        $('#acceptModal').modal()
    }

    $('.AcceptBtn').click(function() {
        var id = $('#agentId').val()
        var action = $('#id').val()
        $(".AcceptBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/complete-user-payment/" + id ,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".AcceptBtn").text('Processing..');
            },
            success: function(data) {
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    function rejectFunction(id) {
        $('#documentId').val(id);
        $('#rejectModal').modal()
    }

    $(document).on('click', '.deletetBtn', function() {
        var id = $('#documentId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/reject/complete-user-payment/" + id ,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                console.log(url);
                $(".deletetBtn").text('Processing..');
            },
            success: function(data) {
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })


    </script>

@endsection

