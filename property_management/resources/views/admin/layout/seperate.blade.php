<?php
header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title id="site_title"></title>

    <!-- vendor css -->
    <link href="{{asset('assets/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/Ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/highlightjs/github.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link href="{{asset('assets/lib/select2/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/ckeditor/sample/css/sample.css')}}" rel="stylesheet">
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bracket-new.css')}}">

    <style>
        .sideleft-scrollbar {
            margin-left: -250px;
            left: 250px;
            width: 250px;
            position: fixed;
            z-index: 1000;
        }
    </style>

</head>

<body>
    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo"><a href="{{route('showDashboard')}}">
            <image src="{{asset('assets/backdrops/navlogo.jpg')}}" width="150px" height="30px" class="mr-2 ml-3">
        </a></div>
    <div class="br-sideleft sideleft-scrollbar transparent overflow-auto">
        <!-- <label class="sidebar-label">Navigation</label> -->
        <ul class="br-sideleft-menu">
            <li class="br-menu-item">
                <a href="{{route('showDashboard')}}" class="br-menu-link" id="dashboard">
                    <i class="menu-item-icon icon fal fa-house-flood fa-lg"></i>
                    <span class="menu-item-label">Dashboardsdsd</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="departments">
                    <i class="menu-item-icon icon fal fa-building fa-lg"></i>
                    <span class="menu-item-label">Departments</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{route('showDepartment')}}" class="sub-link" id="department">Departments</a></li>
                    <li class="sub-item"><a href="{{route('showDesignation')}}" class="sub-link" id="designation">Designations</a></li>
                </ul>
            </li>
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="region_nav">
                    <i class="menu-item-icon icon fal fa-location-circle fa-lg"></i>
                    <span class="menu-item-label">Region Management</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{route('showRegion')}}" class="sub-link" id="country_state_nav">Country/State</a></li>
                    <li class="sub-item"><a href="{{route('showPincode')}}" class="sub-link" id="city_pincode_nav">City/Pincode</a></li>
                </ul>
            </li>
            <li class="br-menu-item">
                <a href="{{route('showService')}}" class="br-menu-link with-sub" id="service_nav">
                    <i class="menu-item-icon icon fal fa-handshake fa-lg"></i>
                    <span class="menu-item-label">Service Management</span>
                </a><!-- br-menu-link -->
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('showUserRequest') }}" class="sub-link" id="about_us">Service Request</a></li>
                    <li class="sub-item"><a href="{{ route('showProvider') }}" class="sub-link" id="privacy_policy">Service Provider</a></li>
                    <li class="sub-item"><a href="{{ route('showInspectionTeam') }}" class="sub-link" id="legal_informations">Inspection Team</a></li>
                    <li class="sub-item"><a href="{{ route('showService') }}" class="sub-link" id="faq">Services</a></li>
                    {{-- <li class="sub-item"><a href="{{ route('showCancelRequest') }}" class="sub-link" id="faq">Cancellation Request</a></li> --}}
                    <li class="sub-item"><a href="{{ route('cancelledRequest')}}" class="sub-link" id="faq">Cancelled Request </a></li>
                    <li class="sub-item"><a href="{{ route('completedRequest')}}" class="sub-link" id="faq">Completed Request</a></li>

                </ul>
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{route('showAdmins')}}" class="br-menu-link" id="admin">
                    <i class="menu-item-icon icon fal fa-user-cog fa-lg"></i>
                    <span class="menu-item-label">Admin Management</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{route('showAgent')}}" class="br-menu-link with-sub" id="agent_nav">
                    <i class="menu-item-icon icon fal fa-people-carry fa-lg"></i>
                    <span class="menu-item-label">Agent Management</span>
                </a><!-- br-menu-link -->
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('showAgent') }}" class="sub-link" id="agent">Agent </a></li>
                    <li class="sub-item"><a href="{{ route('showAgentCashPayment') }}" class="sub-link" id="agent_cash">Agent Cash Pay Request</a></li>

                </ul>
            </li><!-- br-menu-item -->

            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="master_nav">
                    <i class="menu-item-icon icon fal fa-user-graduate fa-lg"></i>
                    <span class="menu-item-label">SALES</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('showAbout') }}" class="sub-link" id="about_us">About Us</a></li>
                    <li class="sub-item"><a href="{{ route('showPrivacyPolicy') }}" class="sub-link" id="privacy_policy">Privacy Policy</a></li>
                    <li class="sub-item"><a href="{{ route('showLegalInformation') }}" class="sub-link" id="legal_informations">Legal Information</a></li>
                    <li class="sub-item"><a href="{{ route('showFaq') }}" class="sub-link" id="faq">FAQ</a></li>
                </ul>
            </li>
            <!-- Cms Pages -->
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="master_nav">
                    <i class="menu-item-icon icon fal fa-user-graduate fa-lg"></i>
                    <span class="menu-item-label">CMS</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('showAbout') }}" class="sub-link" id="about_us">About Us</a></li>
                    <li class="sub-item"><a href="{{ route('showPrivacyPolicy') }}" class="sub-link" id="privacy_policy">Privacy Policy</a></li>
                    <li class="sub-item"><a href="{{ route('showLegalInformation') }}" class="sub-link" id="legal_informations">Legal Information</a></li>
                    <li class="sub-item"><a href="{{ route('showFaq') }}" class="sub-link" id="faq">FAQ</a></li>
                </ul>
            </li>
            <li class="br-menu-item">
                <a href="{{route('showUser')}}" class="br-menu-link" id="user_nav">
                    <i class="menu-item-icon icon fal fa-user-friends fa-lg"></i>
                    <span class="menu-item-label">User Management</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{route('userBookings')}}" class="br-menu-link with-sub" id="user_book">
                    <i class="menu-item-icon icon fal fa-book fa-lg"></i>
                    <span class="menu-item-label">Bookings</span>
                </a><!-- br-menu-link -->
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('userBookings') }}" class="sub-link" id="about_us">User Booking</a></li>
                    <li class="sub-item"><a href="{{ route('userBookings',['status' => 1]) }}" class="sub-link" id="about_us">Document Verification</a></li>
                    <li class="sub-item"><a href="{{ route('userBookings',['status' => 2]) }}" class="sub-link" id="about_us">Upload Contract</a></li>

                    <li class="sub-item"><a href="{{ route('agentBookings') }}" class="sub-link" id="privacy_policy">Tour Booking </a></li>

                </ul>
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{route('showOwner')}}" class="br-menu-link" id="owner_nav">
                    <i class="menu-item-icon icon fal fa-user fa-lg"></i>
                    <span class="menu-item-label">Owner Management</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{route('showPropertyManage')}}" class="br-menu-link with-sub" id="property_manage_nav">
                    <i class="menu-item-icon icon fal fa-building fa-lg"></i>
                    <span class="menu-item-label">Property Management</span>
                </a><!-- br-menu-link -->
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('showPropertyManage') }}" class="sub-link" id="about_us">Property Approved</a></li>
                    <li class="sub-item"><a href="{{ route('showProperty') }}" class="sub-link" id="about_us">Property Approval</a></li>

                </ul>
                <li class="br-menu-item">
                    <a href="#" class="br-menu-link with-sub" id="report_nav">
                        <i class="menu-item-icon icon fal fa-document fas fa-file fa-lg"></i>
                        <span class="menu-item-label">Reports</span>
                    </a>
                    <ul class="br-menu-sub">
                        <li class="sub-item"><a href="{{ route('showOwnerReport') }}" class="sub-link" id="master_frequency_nav"> Owner Report</a></li>
                        <li class="sub-item"><a href="{{route('showAgentReport') }}" class="sub-link" id="master_frequency_nav">Agent Report </a></li>
                        <li class="sub-item"><a href="{{route('showTenantReport')}}" class="sub-link" id="master_frequency_nav">Tenant Report </a></li>
                        <li class="sub-item"><a href="{{route('showRentalReport')}}" class="sub-link" id="master_frequency_nav">Rental Report </a></li>



                    </ul>
                </li>
            </li><!-- br-menu-item -->

            {{-- <li class="br-menu-item">


            {{-- <li class="br-menu-item">
          <a href="#" class="br-menu-link with-sub" id="property_nav">
            <i class="menu-item-icon icon fal fa-building fa-lg"></i>
            <span class="menu-item-label">Property</span>
          </a>
          <ul class="br-menu-sub">

          </ul>
        </li> --}}
            <li class="br-menu-item">
                <a href="{{ route('showFeedbacks') }}" class="br-menu-link" id="feedbacks">
                    <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                    <span class="menu-item-label">Feedbacks</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="master">
                    <i class="menu-item-icon icon fal fa-user-graduate fa-lg"></i>
                    <span class="menu-item-label">Masters</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{ route('showFrequency') }}" class="sub-link" id="master_frequency_nav">Frequency</a></li>
                    <li class="sub-item"><a href="{{route('showTypes')}}" class="sub-link" id="property_type_nav">Type</a></li>
                    <li class="sub-item"><a href="{{route('showAmenity')}}" class="sub-link" id="property_amenity_nav">Amenities</a></li>
                    <li class="sub-item"><a href="{{route('showDetails')}}" class="sub-link" id="property_detail_nav">Details</a></li>
                    {{-- <li class="sub-item"><a href="{{route('showProperty')}}" class="sub-link" id="property_sub_nav">Property</a></li> --}}
                </ul>
            </li>
            <li class="br-menu-item">
                <a href="{{ route('showEvents') }}" class="br-menu-link" id="events">
                    <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                    <span class="menu-item-label">Events</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{ route('showTenants') }}" class="br-menu-link" id="tenant">
                    <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                    <span class="menu-item-label">Tenant</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{ route('showBecomeOwnerList') }}" class="br-menu-link" id="become_owner">
                    <i class="menu-item-icon icon fal fa-user fa-lg"></i>
                    <span class="menu-item-label">Become An Owner</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="{{ route('showDesiredProperty') }}" class="br-menu-link" id="desired_property">
                    <i class="menu-item-icon icon fal fa-user fa-lg"></i>
                    <span class="menu-item-label">Desired Property</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="account_nav">
                    <i class="menu-item-icon icon fal fa-people-carry fa-lg"></i>
                    <span class="menu-item-label">Accounts</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{route('showPaidService')}}" class="sub-link" id="master_frequency_nav"> Service</a></li>
                    <li class="sub-item"><a href="{{route('showPropertyToken')}}" class="sub-link" id="master_frequency_nav">User Token</a></li>
                    <li class="sub-item"><a href="{{route('showUserRent')}}" class="sub-link" id="master_frequency_nav">User Rent</a></li>
                    <li class="sub-item"><a href="{{route('showAgentPayment')}}" class="sub-link" id="master_frequency_nav">Agent Commission </a></li>



                </ul>
            </li>
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="account_nav">
                    <i class="menu-item-icon icon fal fa-people-carry fa-lg"></i>
                    <span class="menu-item-label">Account List</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{route('showUserServiceList')}}" class="sub-link" id="master_frequency_nav">User Service </a></li>
                    <li class="sub-item"><a href="{{route('showOwnerServiceList')}}" class="sub-link" id="master_frequency_nav">Owner Service </a></li>
                    <li class="sub-item"><a href="{{route('showTokenList')}}" class="sub-link" id="master_frequency_nav">UserTokens </a></li>
                    <li class="sub-item"><a href="{{route('showUserRental')}}" class="sub-link" id="master_frequency_nav">UserRental </a></li>
                    <li class="sub-item"><a href="{{route('showOwnerPaymentList')}}" class="sub-link" id="master_frequency_nav">Owner Property Payment </a></li>
                    <li class="sub-item"><a href="{{route('showUserSecurityAmountList')}}" class="sub-link" id="master_frequency_nav">Security Payment </a></li>
                    <li class="sub-item"><a href="{{route('showUserMaintenanceList')}}" class="sub-link" id="master_frequency_nav">Maintanence Payment </a></li>
                    <li class="sub-item"><a href="{{route('showAgentPaidList')}}" class="sub-link" id="master_frequency_nav">Agent Paid List </a></li>


                </ul>
            </li>
            <li class="br-menu-item">
                <a href="{{ route('notifications') }}" class="br-menu-link" id="notifications">
                    <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                    <span class="menu-item-label">Notifications</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" id="rewards">
                    <i class="menu-item-icon icon fal fa-ticket fa-lg"></i>
                    <span class="menu-item-label">Rewards</span>
                </a>
                <ul class="br-menu-sub">
                    {{-- <li class="sub-item"><a href="" class="sub-link" id="coupon_types">Coupon Types</a></li> --}}
                    <li class="sub-item"><a href="{{route('showCoupon')}}" class="sub-link" id="coupons">Coupons </a></li>
                </ul>
            </li>

        </ul><!-- br-sideleft-menu -->
        <li class="br-menu-item">
            <a href="{{ route('showVacateRequest') }}" class="br-menu-link" id="vacate_request">
                <i class="menu-item-icon icon fal fa-user fa-lg"></i>
                <span class="menu-item-label">Vacate Request</span>
            </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->


        <br>
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->
    <div class="br-header ">
        <div class="br-header-left">
            <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="fad fa-layer-group"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="fad fa-layer-group"></i></a></div>
        </div><!-- br-header-left -->
        <div class="br-header-right">
            <nav class="nav">
                <div class="dropdown">
                    <a href="" class="nav-link pd-x-7 pos-relative mt-2" data-toggle="dropdown">
                        <i class="fal fa-bell tx-20"></i>
                        <!-- start: if statement -->
                        <span class="square-8 bg-danger pos-absolute t-10 r-5 rounded-circle"></span>
                        <!-- end: if statement -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-header">
                        <div class="dropdown-menu-label">
                            <label>Notifications</label>
                            <a href="">Mark All as Read</a>
                        </div><!-- d-flex -->

                        <div class="media-list">
                            <!-- loop starts here -->
                            <a href="" class="media-list-link read">
                                <div class="media">
                                    <img src="https://via.placeholder.com/500" alt="">
                                    <div class="media-body">
                                        <p class="noti-text"><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                        <span>October 03, 2017 8:45am</span>
                                        {{-- <li class="br-menu-item">
                        <a href="{{ route('showFeedbacks') }}" class="br-menu-link" id="feedbacks">
                                        <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                                        <span class="menu-item-label">Feedbacks</span>
                            </a><!-- br-menu-link -->
                            </li><!-- br-menu-item -->

                            <li class="br-menu-item">
                                <a href="#" class="br-menu-link with-sub" id="master_nav">
                                    <i class="menu-item-icon icon fal fa-user-graduate fa-lg"></i>
                                    <span class="menu-item-label">Masters</span>
                                </a>
                                <ul class="br-menu-sub">
                                    <li class="sub-item"><a href="{{ route('showFrequency') }}" class="sub-link" id="master_frequency_nav">Frequency</a></li>
                                    <li class="sub-item"><a href="{{route('showTypes')}}" class="sub-link" id="property_type_nav">Type</a></li>
                                    <li class="sub-item"><a href="{{route('showAmenity')}}" class="sub-link" id="property_amenity_nav">Amenities</a></li>
                                    <li class="sub-item"><a href="{{route('showDetails')}}" class="sub-link" id="property_detail_nav">Details</a></li>
                                    <li class="sub-item"><a href="{{route('showProperty')}}" class="sub-link" id="property_sub_nav">Property</a></li>
                                </ul>
                            </li>
                            <li class="br-menu-item">
                                <a href="{{ route('showEvents') }}" class="br-menu-link" id="events">
                                    <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                                    <span class="menu-item-label">Events</span>
                                </a><!-- br-menu-link -->
                            </li><!-- br-menu-item -->
                            <li class="br-menu-item">
                                <a href="{{ route('showBecomeOwnerList') }}" class="br-menu-link" id="become_owner">
                                    <i class="menu-item-icon icon fal fa-user fa-lg"></i>
                                    <span class="menu-item-label">Become An Owner</span>
                                </a><!-- br-menu-link -->
                            </li><!-- br-menu-item --> --}}



                            {{-- </ul><!-- br-sideleft-menu -->

                <br>
            </div><!-- br-sideleft -->
            <!-- ########## END: LEFT PANEL ########## -->

            <!-- ########## START: HEAD PANEL ########## -->
            <div class="br-header ">
                <div class="br-header-left">
                    <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="fad fa-layer-group"></i></a></div>
                    <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="fad fa-layer-group"></i></a></div>
                </div><!-- br-header-left -->
                <div class="br-header-right">
                    <nav class="nav">
                        <div class="dropdown">
                            <a href="" class="nav-link pd-x-7 pos-relative mt-2" data-toggle="dropdown">
                                <i class="fal fa-bell tx-20"></i>
                                <!-- start: if statement -->
                                <span class="square-8 bg-danger pos-absolute t-10 r-5 rounded-circle"></span>
                                <!-- end: if statement -->
                            </a>
                            <div class="dropdown-menu dropdown-menu-header">
                                <div class="dropdown-menu-label">
                                    <label>Notifications</label>
                                    <a href="">Mark All as Read</a>
                                </div><!-- d-flex -->

                                <div class="media-list">
                                    <!-- loop starts here -->
                                    <a href="" class="media-list-link read">
                                        <div class="media">
                                            <img src="https://via.placeholder.com/500" alt="">
                                            <div class="media-body">
                                                <p class="noti-text"><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                                <span>October 03, 2017 8:45am</span>
                                            </div>
                                        </div><!-- media -->
                                    </a>
                                    <div class="dropdown-footer">
                                        <a href=""><i class="fa fa-angle-down"></i> Show All Notifications</a> --}}
                        </div>
                    </div><!-- media -->
                    </a>
                    <div class="dropdown-footer">
                        <a href=""><i class="fa fa-angle-down"></i> Show All Notifications</a>
                    </div>
                </div><!-- media-list -->
        </div><!-- dropdown-menu -->
    </div><!-- dropdown -->
    <div class="dropdown">
        <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
            <!-- <span class="logged-name hidden-md-down">{{ Session()->get('name') }}</span> -->
            <img src="https://via.placeholder.com/500" class="wd-32 rounded-circle" alt="">
            <span class="square-10 bg-success"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-header wd-250">
            <div class="tx-center">
                <a href=""><img src="https://via.placeholder.com/500" class="wd-80 rounded-circle" alt=""></a>
                <h6 class="logged-fullname">{{session()->get('name')}}</h6>
                <p>{{session()->get('email')}}</p>
            </div>
            <hr>
            <ul class="list-unstyled user-profile-nav">
                <li><a href=""><i class="icon ion-ios-person"></i> Edit Profile</a></li>
                <li><a href="{{route('logout')}}"><i class="icon ion-power"></i> Sign Out</a></li>
            </ul>
        </div><!-- dropdown-menu -->
    </div><!-- dropdown -->
    </nav>
    </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        @yield('content')
        <footer class="br-footer">
            {{-- <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2017. Bracket Plus. All Rights Reserved.</div>
            <div>Attentively and carefully made by ThemePixels.</div>
          </div>
          <div class="footer-right d-flex align-items-center">
            <span class="tx-uppercase mg-r-10">Share:</span>
            <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-facebook tx-20"></i></a>
            <a target="_blank" class="pd-x-5" href="https://twitter.com/home?status=Bracket%20Plus,%20your%20best%20choice%20for%20premium%20quality%20admin%20template%20from%20Bootstrap.%20Get%20it%20now%20at%20http%3A//themepixels.me/bracketplus/intro"><i class="fab fa-twitter tx-20"></i></a>
          </div> --}}
        </footer>
    </div><!-- br-mainpanel -->

    <!-- ########## END: MAIN PANEL ########## -->
    <script src="{{asset('assets/lib/jquery/jquery.js')}}"></script>
    <script src="{{asset('assets/lib/popper.js/popper.js')}}"></script>
    <script src="{{asset('assets/js/datepicker.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/lib/moment/moment.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-ui/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-switchbutton/jquery.switchButton.js')}}"></script>
    <script src="{{asset('assets/lib/peity/jquery.peity.js')}}"></script>
    <script src="{{asset('assets/lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/bracket.js')}}"></script>
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>
    <!--toastr Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    @yield('scripts')
</body>

</html>
