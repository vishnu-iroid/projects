<?php
// new changes at side nav bar  old changes are at seperate.blade.php
header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">
    <title id=""></title>

    <!-- vendor css -->
    <link href="{{asset('assets/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/Ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/highlightjs/github.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link href="{{asset('assets/lib/select2/css/select2.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/lib/ckeditor/sample/css/sample.css')}}" rel="stylesheet">
    {{-- <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> --}}

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css" rel="stylesheet">

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.11.1/dist/css/uikit.min.css" />




    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bracket-new.css')}}">

    <style>

        .br-menu-sub-link {
            display: flex;
            align-items: center;
            justify-content: flex-start;
            padding: 0 11px;
            height: 40px;
            letter-spacing: 0.2px;
            color: #3c4858;
            font-size: 14px;
            position: relative;
            border-radius: 2px;
            transition: all 0.2s ease-in-out;
        }

        @media screen and (prefers-reduced-motion: reduce) {
            .br-menu-sub-link {
                transition: none;
            }
        }

        .br-menu-sub-link.with-sub::after {
            content: "\f32d";
            font-family: "Font Awesome 5 Pro";
            font-weight: 900;
            position: absolute;
            font-size: 12px;
            top: calc(50% - 6px);
            right: 10px;
            color: #fff;
            transition: all 0.2s ease-in-out;
        }

        @media screen and (prefers-reduced-motion: reduce) {
            .br-menu-sub-link.with-sub::after {
                transition: none;
            }
        }

        .br-menu-sub-link:hover,
        .br-menu-sub-link:focus {
            color: #fff;
            background-color: #c5c5c5;
        }

        .br-menu-sub-link:focus,
        .br-menu-sub-link:active {
            outline: none;
        }

        .br-menu-sub-link.active {
            /* background-image: linear-gradient(to right, #bb7fae 0%, #17A2B8 100%); */
            /* border: 2px solid #17A2B8; */
            /* background-color: #099840; */
            border-radius: 5px;
            background-repeat: repeat-x;
            /* box-shadow: 0 4px 20px 0 rgb(0 0 0 / 20%), 0 7px 10px -5px rgb(60 72 88 / 40%); */
            color: #fff;
        }

        .br-menu-sub-link.active::after {
            color: #fff;
        }

        .br-menu-sub-link.active+.br-menu-sub {
            display: block;
        }

        .br-menu-sub-link.show-sub {
            background-color: #099840;
            color: #fff;
        }


        .br-menu-super-sub {
            padding-top: 0px;
            padding-bottom: 5px;
            padding-left: 10px;
            background-color: #eeeeee;
            display: none;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }

        .br-menu-super-sub .sub-item {
            display: block;
            position: relative;
        }

        .br-menu-super-sub .sub-link {
            display: block;
            font-size: 13px;
            color: #adb5bd;
            padding: 7px 0 7px 17px;
            white-space: nowrap;
            position: relative;
            transition: all 0.2s ease-in-out;
        }

        @media screen and (prefers-reduced-motion: reduce) {
            .br-menu-super-sub .sub-link {
                transition: none;
            }
        }

        .br-menu-super-sub .sub-link::before {
            content: '';
            position: absolute;
            top: 50%;
            left: -3px;
            margin-top: -.5px;
            width: 5px;
            height: 1px;
            background-color: #fff;
            opacity: .5;
            transition: all 0.2s ease-in-out;
        }

        @media screen and (prefers-reduced-motion: reduce) {
            .br-menu-super-sub .sub-link::before {
                transition: none;
            }
        }

        .br-menu-super-sub .sub-link:hover,
        .br-menu-super-sub .sub-link:focus {
            color: #0866C6;
        }

        .br-menu-super-sub .sub-link:hover::before,
        .br-menu-super-sub .sub-link:focus::before {
            width: 20px;
            left: -11px;
            opacity: 1;
            background-color: #17A2B8;
        }

        .br-menu-super-sub .sub-link.active {
            color: #0866C6;
        }

        .br-menu-super-sub .sub-link.active::before {
            width: 20px;
            left: -11px;
            opacity: 1;
            background-color: #17A2B8;
        }


        .br-menu-sub .sub-link.sub-drop-link::before {
            content: '';
            position: absolute;
            top: 50%;
            left: -3px;
            margin-top: -.5px;
            width: 5px;
            background-color: #fff;
            opacity: .5;
            transition: all 0.2s ease-in-out;
        }


        .br-menu-sub-link.sub-drop-link.with-sub::after {
            content: "\f32d";
            font-family: "Font Awesome 5 Pro";
            font-weight: 900;
            position: absolute;
            font-size: 12px;
            top: calc(50% - 6px);
            right: 10px;
            color: #fff;
            transition: all 0.2s ease-in-out;
        }

        .show-sub{
            color: #fff;
        }
        .blink_me {
  animation: blinker 1s linear infinite;
}

@keyframes blinker {
  50% {
    opacity: 0;
  }
}



    </style>

</head>

<body>

    <div class="br-logo"><a href="{{route('showDashboard')}}">
        <image src="{{asset('assets/backdrops/Logo-dashboard.png')}}" width="120" height="" class="img-fluid">
    </a></div>
    <div class="br-sideleft sideleft-scrollbar transparent overflow-auto">
        <!-- <label class="sidebar-label">Navigation</label> -->
        <ul class="br-sideleft-menu">

                <li class="br-menu-item">
                    <a href="{{route('showDashboard')}}" class="br-menu-link" id="dashboard">
                        <i class="menu-item-icon icon fal fa-house-flood fa-lg"></i>
                        <span class="menu-item-label">Dashboard</span>


                    </a><!-- br-menu-link -->
                </li><!-- br-menu-item -->


                 @canany(['view_employee','edit_employee'])
                <li class="br-menu-item">
                    <a href="{{route('showAdmins')}}" class="br-menu-link" id="admin">
                        <i class="menu-item-icon icon fal fa-user-cog fa-lg"></i>
                        <span class="menu-item-label">Employees Management</span>
                    </a><!-- br-menu-link -->
                </li><!-- br-menu-item -->

                @endcan



                {{-- <li class="br-menu-item">
                    <a href="{{route('roles.index')}}" class="br-menu-link" id="admin">
                        <i class="menu-item-icon icon fal fa-user-cog fa-lg"></i>
                        <span class="menu-item-label">Roles</span>
                    </a><!-- br-menu-link -->
                </li> --}}
            {{-- --}}


              @canany(['view_accounts' , 'edit_accounts'])
             <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" title="Accounts Management" id="account-departments">
                    <i class="menu-item-icon icon fal fa-file-invoice-dollar fa-lg"></i>
                    <span class="menu-item-label">Accounts Management</span>
                </a>
                <ul class="br-menu-sub ">

                        <li class="sub-item"><a href="{{route('showPropertyToken')}}" class="sub-link" id="master_frequency_nav">Property Reservation Fees</a></li>
                        <li class="sub-item"><a href="{{ route('completeUserTour') }}" class="sub-link" id="about_us">Closing Rent Collection</a></li>


                    {{--
                        <li class="sub-item"><a href="{{route('showPropertyToken')}}" class="sub-link" id="master_frequency_nav">Deposit</a></li>
                    --}}

                        <li class="sub-item"><a href="{{route('showUserRent')}}" class="sub-link" id="master_frequency_nav">Rent Collections</a></li>
                        <li class="sub-item"><a href="{{route('showUserCommission')}}" class="sub-link" id="master_frequency_nav">Brokerage Fee</a></li>



                        <li class="sub-item"><a href="{{route('showPaidService')}}" class="sub-link" id="master_frequency_nav">Service Payment Notifications</a></li>


                     <li class="sub-item"><a href="{{ route('showAgentCashPayment') }}" class="sub-link" id="agent_cash">Agent Commission Requests</a></li>
                     {{-- @can('view_reserve_units') --}}
                {{-- @endcan --}}
                    {{-- <li class="sub-item ">
                        <a href="#" class="br-menu-sub-link with-sub sub-link sub-drop-link ">Reports</a>
                        <ul class="br-menu-super-sub br-menu-item">


                            <li class="sub-item"><a href="{{route('showCommissionReport')}}" class="sub-link" id="master_frequency_nav">Property mangement Fee </a></li>

                        </ul>
                        {{-- <a href="{{ route('showAgentCashPayment') }}" class="sub-link" id="agent_cash">Report</a> --}}
                    {{-- </li> --}}
                </ul>
            </li>
             @endcan
            {{-- --}}


               @canany(['view_documentation' , 'edit_documentation'])
              <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" title="Documentation Management" id="document-departments">
                    <i class="menu-item-icon icon fal fa-file-alt fa-lg"></i>
                    <span class="menu-item-label">Documentation</span>
                </a>
                <ul class="br-menu-sub">
                    {{-- @can('view_document_verification' || 'add_document_verification') --}}
                        <li class="sub-item"><a href="{{ route('userBookings', ['status' => 1]) }}"
                                class="sub-link" id="about_us">Document Verification</a></li>
                    {{-- @endcan --}}
                    {{-- @can('view_pending_contract') --}}
                        <li class="sub-item"><a href="{{ route('requestUserContract') }}" class="sub-link"
                                id="about_us">Contract Requests @php
                                $pending_contract_count = \App\Models\AgentRequestingContract::where('upload_status', '0')->count();
                            @endphp
                                @if ($pending_contract_count > 0)
                                    <span
                                        class="blink_me badge badge-pill badge-danger badge-outline-danger">{{ $pending_contract_count ? $pending_contract_count : 0 }}</span>
                                @endif</a></li>
                    {{-- @endcan --}}
                    {{-- @can('view_reserve_units')
                             <li class="sub-item"><a href="{{ route('completeUserTour') }}" class="sub-link" id="about_us">Reserve Unit</a></li>
                        @endcan --}}
                    {{-- @can('view_issue_contract') --}}
                        <li class="sub-item"><a href="{{ route('issuedContract') }}" class="sub-link"
                                id="about_us">Renew/End Lease</a></li>
                    {{-- @endcan --}}

                    {{-- @can('view_terminated_contract') --}}
                    @php
                    $pending_contract_count = \App\Models\AgentRequestingContract::where('upload_status', '2')->count();
                @endphp
                        <li class="sub-item"><a href="{{ route('userBookings', ['status' => 2]) }}"
                                class="sub-link" id="about_us">Terminated Contracts
                                 @if ($pending_contract_count > 0)
                        <span
                            class="blink_me badge badge-pill badge-danger badge-outline-danger">{{ $pending_contract_count ? $pending_contract_count : 0 }}</span>
                    @endif
                            </a>
                            </li>
                    {{-- @endcan --}}
                    <li class="sub-item">
                        <a href="#" class="br-menu-sub-link with-sub sub-link sub-drop-link ">Reports</a>
                        <ul class="br-menu-super-sub br-menu-item">
                            {{-- @can('view_residents_report') --}}
                                <li class="sub-item"><a href="{{ route('showTenantReport') }}"
                                        class="sub-link" id="master_frequency_nav">Residents Report </a></li>
                            {{-- @endcan --}}
                        </ul>
                    </li>

                </ul>
            </li>

              @endcan
            {{-- --}}

            @canany(['view_property_management' , 'edit_property_management'])
           <li class="br-menu-item">
            <a href="#" class="br-menu-link with-sub" title="Property Management" id="document-departments">
                <i class="menu-item-icon icon fal fa-hospital-user fa-lg"></i>
                <span class="menu-item-label" >Property Management</span>
            </a>
            <ul class="br-menu-sub">

                    <li class="sub-item"><a href="{{ route('showOwner') }}" class="sub-link" >Add Landloards/Owner</a></li>


                    <li class="sub-item"><a href="{{ route('userBookings')}}" class="sub-link" >Reservation Requests </a></li>


                    <li class="sub-item"><a href="{{ route('showPropertyManage')}}" class="sub-link" >Property List </a></li>

                    <li class="sub-item"><a href="{{ route('showOwnedProperty')}}" class="sub-link" >Add Lease / Teants</a></li>

                   <li class="sub-item"><a href="{{ route('showProperty')}}" class="sub-link" >Request to List Property </a></li>


                    <li class="sub-item"><a href="{{ route('showTenants')}}" class="sub-link" >Residents </a></li>


                     <li class="sub-item"><a href="{{ route('showBecomeOwnerList')}}" class="sub-link" >Request For Ownership </a></li>


                    <li class="sub-item"><a href="{{ route('showVacateRequest')}}" class="sub-link" >Vacate Request </a></li>

                {{-- <li class="sub-item"><a href="{{ route('showBecomeOwnerList')}}" class="sub-link" >Request For Ownership </a></li> --}}

                <li class="sub-item">
                    <a href="#" class="br-menu-sub-link with-sub sub-link sub-drop-link ">Master</a>
                    <ul class="br-menu-super-sub br-menu-item">

                           <li class="sub-item"><a href="{{ route('showFrequency') }}" class="sub-link" id="master_frequency_nav">Lease Type</a></li>


                            <li class="sub-item"><a href="{{route('showTypes')}}" class="sub-link" id="property_type_nav">Type</a></li>


                            <li class="sub-item"><a href="{{route('showAmenity')}}" class="sub-link" id="property_amenity_nav">Amenities</a></li>


                            <li class="sub-item"><a href="{{route('showDetails')}}" class="sub-link" id="property_detail_nav">Details</a></li>

                            <!-- <li class="sub-item"><a href="{{route('banks.index')}}" class="sub-link" id="property_detail_nav">Banks Name</a></li> -->

                    </ul>
                </li>


            </ul>
        </li>
           @endcan
             @canany(['view_sales' , 'edit_sales'])
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" title="Property Management" id="document-departments">
                    <i class="menu-item-icon icon fas fa-chart-bar fa-lg"></i>

                    <span class="menu-item-label" >Sales</span>
                </a>
                <ul class="br-menu-sub">

                    <li class="sub-item"><a href="{{ route('agentBookings') }}" class="sub-link" >Customer Visit Request</a></li>

                    <li class="sub-item"><a href="{{ route('showAgent') }}" class="sub-link" id="agent">Sale Agent </a></li>

                </ul>
            </li>
            @endcan


            {{-- @endif
            @if(session()->get('access_mode') == 'service'  session()->get('access_mode') == 'super') --}}
             @canany(['view_facility_management' , 'edit_facility_management'])
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" title="Facility Management" id="document-departments">
                    <i class="menu-item-icon icon fal fas fa-wrench fa-lg"></i>
                    <span class="menu-item-label" >Facility Management</span>
                </a>
                <ul class="br-menu-sub">

                        <li class="sub-item"><a href="{{ route('showUserRequest') }}" class="sub-link" id="about_us">Service Requests</a></li>

                      <li class="sub-item"><a href="{{ route('cancelledRequest')}}" class="sub-link" id="faq">Cancelled Requests </a></li>



                    <li class="sub-item">
                        <a href="#" class="br-menu-sub-link with-sub sub-link sub-drop-link ">Masters</a>
                        <ul class="br-menu-super-sub br-menu-item">
                                <li class="sub-item"><a href="{{ route('showUserRequest') }}" class="sub-link" id="about_us">Service Request</a></li>


                                <li class="sub-item"><a href="{{ route('showService') }}" class="sub-link" id="faq">Services</a></li>

                                <li class="sub-item"><a href="{{ route('showProvider') }}" class="sub-link" id="privacy_policy">Service Providers</a></li>


                                <li class="sub-item"><a href="{{ route('showInspectionTeam') }}" class="sub-link" id="legal_informations">Inspection Team</a></li>

                        </ul>
                    </li>

                </ul>
            </li>
            @endcan
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" title="Report " id="report">
                    <i class="menu-item-icon icon fal fa-users-cog fa-lg"></i>
                    <span class="menu-item-label" >Reports</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item">
                        <a href="{{ route('showOwnerReport') }}" class="sub-link" id="about_us">Owners Report</a>
                        <a href="{{route('proprtyReport')}}" class=" sub-link  " id="region_nav">Property report</a>
                        <a href="{{route('vacateReport')}}" class=" sub-link  " id="region_nav">Vacant report</a>
                        <a href="{{ route('showTenantReport') }}" class="sub-link" id="legal_informations">Residence Report</a>
                        <a href="{{route('showRentalReport')}}" class="sub-link" id="master_frequency_nav">Rental Report </a>
                        <a href="{{ route('dueReport') }}" class="sub-link" id="legal_informations">Due Report</a>
                        <a href="{{ route('showReservationFeeReport') }}" class="sub-link" id="legal_informations">Reservation Fee Report</a>
                        <a href="{{route('showCommissionReport')}}" class="sub-link" id="master_frequency_nav">Mangement Fee Report</a>
                        <a href="{{route('depoisteReport')}}" class=" sub-link  " id="region_nav">Deposit report</a>
                        <a href="{{route('agentReport')}}" class=" sub-link " id="region_nav">Agent report</a>
                        <a href="{{ route('showServiceReport') }}" class="sub-link" id="legal_informations">Service Report</a>


                      {{-- <a href="{{ route('showReservationFeeReport') }}" class="sub-link" id="legal_informations">Property Reservation Fee Report</a> --}}
                    </li>
                    {{-- --}}


                </ul>
            </li>



            {{-- @endif

            @if(session()->get('access_mode') == 'service'  session()->get('access_mode') == 'super') --}}
             @canany(['view_set_up_management' , 'edit_set_up_management'])
            <li class="br-menu-item">
                <a href="#" class="br-menu-link with-sub" title="Set Up Management" id="setup-departments">
                    <i class="menu-item-icon icon fal fa-users-cog fa-lg"></i>
                    <span class="menu-item-label" >Set Up Management</span>
                </a>
                <ul class="br-menu-sub">
                    <li class="sub-item"><a href="{{route('companyDetails')}}" class="sub-link" id="about_us">Company Info</a></li>

                    <li class="sub-item">

                        <a href="#" class="br-menu-sub-link with-sub sub-link sub-drop-link " id="region_nav">Region Management</a>
                        <ul class="br-menu-super-sub br-menu-item">

                                <li class="sub-item"><a href="{{route('showRegion')}}" class="sub-link" id="country_state_nav">Country/State</a></li>


                                <li class="sub-item"><a href="{{route('showPincode')}}" class="sub-link" id="city_pincode_nav">City/Pincode</a></li>

                        </ul>
                    </li>
                    {{-- --}}

                    <li class="sub-item ">
                        <a href="#" class="br-menu-sub-link with-sub sub-link sub-drop-link " id="departments">Departments</a>
                        <ul class="br-menu-super-sub br-menu-item">

                            <li class="sub-item"><a href="{{route('showDepartment')}}" class="sub-link" id="department">Departments</a></li>

                            <li class="sub-item"><a href="{{route('showDesignation')}}" class="sub-link" id="designation">Designations</a></li>

                        </ul>
                    </li>
                    <li class="sub-item"><a href="{{route('banks.index')}}" class="sub-link" id="about_us">Banks Name</a></li>
                    <li class="sub-item"><a href="{{route('roles.index')}}" class="sub-link" id="agent">User Roles </a></li>
                    <li class="sub-item"><a href="{{route('showUser')}}" class="sub-link" id="user_nav">User Management </a></li>
                </ul>
            </li>
            @endcan

            {{-- @endif --}}
             @canany(['view_events' , 'edit_events'])
            <li class="br-menu-item">
                <a href="{{route('showEvents')}}" class="br-menu-link" id="owner_nav">
                    <i class="menu-item-icon icon fal fa-user fa-lg"></i>
                    <span class="menu-item-label">Events</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            @endcan
                 @canany(['view_notification' , 'edit_notification'])
                <li class="br-menu-item">
                    <a href="{{ route('notifications') }}" class="br-menu-link" id="notifications">
                        <i class="menu-item-icon icon fal fa-bell fa-lg"></i>
                        <span class="menu-item-label">Notifications</span>
                    </a><!-- br-menu-link -->
                </li><!-- br-menu-item -->
                @endcan

             @canany(['view_feedbacks' , 'edit_feedbacks'])
            <li class="br-menu-item">
                <a href="{{ route('showFeedbacks') }}" class="br-menu-link" id="feedbacks">
                    <i class="menu-item-icon icon fal fa-comments fa-lg"></i>
                    <span class="menu-item-label">Feedbacks</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            @endcan
             @canany(['view_rewards' , 'edit_rewards'])
            <li class="br-menu-item">
                <a href="{{ route('showCoupon') }}" class="br-menu-link" id="rewards">
                    <i class="menu-item-icon icon fal fa-coffee fa-lg"></i>
                    <span class="menu-item-label">Rewards</span>
                </a><!-- br-menu-link -->
            </li><!-- br-menu-item -->
            @endcan
            @canany(['view_cms', 'edit_cms'])
                <li class="br-menu-item">
                    <a href="#" class="br-menu-link with-sub" id="master_nav">
                        <i class="menu-item-icon icon fal fa-user-graduate fa-lg"></i>
                        <span class="menu-item-label">CMS</span>
                    </a>
                    <ul class="br-menu-sub">
                        <li class="sub-item"><a href="{{ route('showAbout') }}" class="sub-link" id="about_us">About Us</a></li>
                       <li class="sub-item"><a href="{{ route('showPrivacyPolicy') }}" class="sub-link" id="privacy_policy">Privacy Policy</a></li>
                       <li class="sub-item"><a href="{{ route('showLegalInformation') }}" class="sub-link" id="legal_informations">Legal Information</a></li>
                        <li class="sub-item"><a href="{{ route('showFaq') }}" class="sub-link" id="faq">FAQ</a></li>
                    </ul>
                </li>
                @endcan

            {{-- --}}

        </ul>
    </div>
    <div class="br-header ">
        <div class="br-header-left">
            <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="fad fa-layer-group"></i></a></div>
            <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="fad fa-layer-group"></i></a></div>
        </div><!-- br-header-left -->
        <div class="br-header-right">
            <nav class="nav">
                <div class="dropdown">
                    <a href="" class="nav-link pd-x-7 pos-relative mt-2" data-toggle="dropdown">
                        <i class="fal fa-bell tx-20"></i>
                        <!-- start: if statement -->
 @php
      $serviceCount=App\Models\Notifications::where('type_of_notification','Service')->where('status','unread')->count();
        // $services=DB::table('user_property_service_requests')
        // ->where('status',0)
        // ->join('services','services.id','user_property_service_requests.service_id')
        // ->get();
        $services=App\Models\Notifications::where('type_of_notification','Service')->where('status','unread')->get();
 @endphp
                        <span class="square-8 bg-danger pos-absolute rounded-circle">{{$serviceCount}}</span>
                        <!-- end: if statement -->
                    </a>


                    <div class="dropdown-menu dropdown-menu-header">
                        <div class="dropdown-menu-label">
                            <label>Notifications</label>
                             <a href="">Mark All as Read</a>
                        </div><!-- d-flex -->

                        <div class="media-list">
                            <!-- loop starts here -->

                                        @foreach ($services as $service)
                                        {{--  --}}
                                        <a href="" class="media-list-link">
                                            <div class="">
                                        <div class="media-body  d-flex Px-2 py-2">

                                        <div class="noti_img">
                                        <img src="{{$service->image}}" alt="" width="">
                                    </div>

                                        <div>
                                        <span class="noti-text"><strong>{{$service->notification_heading}}</strong><a href="{{route('notifications')}}">{{$service->service}}</a></strong> </span>
                                    </div>
                                </div>
                            </div><!-- media -->
                        </a>

                                        @endforeach

                            <!-- <div class="dropdown-footer text-dark">
                                <a href="{{route('notifications')}}">Clear All Notifications</a>
                            </div> -->
                        </div><!-- media-list -->
                    </div>

                    <!-- dropdown-menu -->
                </div><!-- dropdown -->
                <div class="dropdown">
                    <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
                        <!-- <span class="logged-name hidden-md-down">{{ Session()->get('name') }}</span> -->
                        <img src="https://via.placeholder.com/500" class="wd-32 rounded-circle" alt="">
                        <span class="square-10 bg-success"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-header wd-250">
                        <div class="tx-center">
                            <a href=""><img src="https://via.placeholder.com/500" class="wd-80 rounded-circle" alt=""></a>
                            <h6 class="logged-fullname">{{session()->get('name')}}</h6>
                            <p>{{session()->get('email')}}</p>
                        </div>
                        <hr>
                        <ul class="list-unstyled user-profile-nav">
                            <li><a href=""><i class="icon ion-ios-person"></i> Edit Profile</a></li>
                            <li><a href="{{route('logout')}}"><i class="icon ion-power"></i> Sign Out</a></li>
                        </ul>
                    </div><!-- dropdown-menu -->
                </div><!-- dropdown -->
            </nav>
        </div><!-- br-header-right -->
    </div>

    <div class="br-mainpanel">
        @yield('content')
        <!-- <footer class="br-footer">
        </footer> -->
    </div><!-- br-mainpanel -->


    <!-- ########## END: MAIN PANEL ########## -->
    <script src="{{asset('assets/lib/jquery/jquery.js')}}"></script>
    <script src="{{asset('assets/lib/popper.js/popper.js')}}"></script>
    <script src="{{asset('assets/js/datepicker.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/lib/moment/moment.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-ui/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-switchbutton/jquery.switchButton.js')}}"></script>
    <script src="{{asset('assets/lib/peity/jquery.peity.js')}}"></script>
    <script src="{{asset('assets/lib/highlightjs/highlight.pack.js')}}"></script>
    {{-- <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script> --}}
    <script src="{{asset('assets/js/bracket.js')}}"></script>
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>

    {{-- <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script> --}}

    {{-- <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script> --}}
    <!--toastr Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.11.1/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.11.1/dist/js/uikit-icons.min.js"></script>
    <script>

        $('.br-sideleft').on('click', '.br-menu-sub-link', function(){
            var nextElem = $(this).next();
            var thisLink = $(this);

            if(nextElem.hasClass('br-menu-super-sub')) {

            if(nextElem.is(':visible')) {
                thisLink.removeClass('show-sub');
                nextElem.slideUp();
            } else {
                $('.br-menu-sub-link').each(function(){
                    $(this).removeClass('show-sub');
                });

                $('.br-menu-super-sub').each(function(){
                    $(this).slideUp();
                });

                thisLink.addClass('show-sub');
                nextElem.slideDown();
            }
            return false;
            }
        });

    </script>
    @yield('scripts')
</body>

</html>
