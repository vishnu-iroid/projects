@extends('admin.layout.seperate')
<link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
<style>
  .card {
     position: relative;
     display: flex;
     flex-direction: column;
     min-width: 0;
     word-wrap: break-word;
     background-color: #fff;
     background-clip: border-box;
     border: 1px solid #eee;
     border-radius: .25rem;
 }
 .card-header-b{
   height : 300px !important;
 }
</style>
@section('content')
   <div class="br-pagetitle">
     <!-- <i class="icon fad fa-home tx-70 lh-0"></i> -->
     <div>
       <h4>Dashboard</h4>
       <!-- <p class="mg-b-0">Do bigger things with Bracket plus, the responsive bootstrap 4 admin template.</p> -->
     </div>
   </div><!-- d-flex -->

   <div class="br-pagebody">
     <div class="row row-sm mg-b-20">
       <div class="col-sm-6 col-xl-3">
         <div class="MalibuBeach rounded overflow-hidden shadow">
           <a href="{{route('showUser')}}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <i class="fad fa-user-friends tx-60 lh-0 tx-white op-7"></i>
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Users</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{ $users }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $users_active }} active users</span>
               </div>
             </div>
           </a>
           <div id="ch1" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="50">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
       <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
         <div class="purple-gradient rounded overflow-hidden shadow">
           <a href="{{route('showOwner')}}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <i class="fad fa-user tx-60 lh-0 tx-white op-7"></i>
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Owners</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $owners }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $owners_active }} active owners</span>
               </div>
             </div>
           </a>
           <div id="ch3" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="50">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
       <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
         <div class="HappyAcid rounded overflow-hidden shadow">
           <a href="{{ route('showAgent') }}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <i class="<i fad fa-people-carry tx-60 lh-0 tx-white op-7"></i>
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Agents</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $agents }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $agents_active }} active agents</span>
               </div>
             </div>
           </a>
           <div id="ch2" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="50">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
       <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
         <div class="DeepBlue rounded overflow-hidden shadow">
           <a href="{{ route('showProvider') }}">
             <div class="pd-x-20 pd-t-20 d-flex align-items-center">
               <i class="fad fa-handshake tx-60 lh-0 tx-white op-7"></i>
               <div class="mg-l-20">
                 <p class="tx-13 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-3">Service Providers</p>
                 <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">{{ $service_provider }}</p>
                 <span class="tx-13 tx-roboto tx-white-8">{{ $service_provider }} active</span>
               </div>
             </div>
           </a>
           <div id="ch4" class="ht-50 tr-y-1 rikshaw_graph">
             <svg width="435.5" height="50">
                 <g><path d="M0,25Q31.452777777777776,40.541666666666664,36.291666666666664,40.625C43.55,40.75,65.32499999999999,27.8125,72.58333333333333,26.25S101.61666666666667,24.625,108.875,25S137.90833333333333,30.625,145.16666666666666,30S174.2,20.8125,181.45833333333331,18.75S210.49166666666667,10.625,217.75,9.375S246.7833333333333,5,254.04166666666663,6.25S283.075,20.9375,290.3333333333333,21.875S319.3666666666667,16.5625,326.625,15.625S355.6583333333333,12.1875,362.91666666666663,12.5S391.95,17.5,399.2083333333333,18.75Q404.0472222222222,19.583333333333332,435.5,25L435.5,50Q404.0472222222222,50,399.2083333333333,50C391.95,50,370.17499999999995,50,362.91666666666663,50S333.8833333333333,50,326.625,50S297.59166666666664,50,290.3333333333333,50S261.29999999999995,50,254.04166666666663,50S225.00833333333333,50,217.75,50S188.71666666666664,50,181.45833333333331,50S152.42499999999998,50,145.16666666666666,50S116.13333333333333,50,108.875,50S79.84166666666667,50,72.58333333333333,50S43.55,50,36.291666666666664,50Q31.452777777777776,50,0,50Z" class="area" fill="rgba(255,255,255,0.5)"></path></g>
             </svg>
           </div>
         </div>
       </div><!-- col-3 -->
     </div><!-- row -->
     <div class="row mg-t-40">
       <div class="col-md-6">
         <div class="card card-chart shadow" data-count="0">
             <div data-header-animation="true" class="card-header-b card-header-success">
               <div id="dailySalesChart" class="ct-chart">

               </div>
             </div>
             <div class="card-body">
               <h4 class="card-title">Total Revenue</h4>
               <p class="card-category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
             </div>
             <div class="card-footer">
               <div class="stats"><i class="fad fa-clock"></i> updated 4 minutes ago </div>
             </div>
         </div>
       </div>
     </div>
   </div>
     </div>

   </div><!-- br-pagebody -->
 @endsection
 @section('scripts')
 <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
 <script>
       var myChart = new Chartist.Line('.ct-chart',{
           labels: ['jan', 'feb', 'mar', 'apr', 'may','jun','jul','aug','sep','oct','nov','dec'],
           series: [
             [120, 90, 105, 85, 65, 90, 78, 110, 40, 65, 55, 40]
           ]
       },{
         low: 0,
         showArea: true,
         showPoint: true,
         fullWidth: true
       });
       myChart.on('draw', function(data) {
         if(data.type === 'line' || data.type === 'area') {
           data.element.animate({
             d: {
               begin: 2000 * data.index,
               dur: 2000,
               from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
               to: data.path.clone().stringify(),
               easing: Chartist.Svg.Easing.easeOutQuint
             }
           });
         }
       });
 </script>
 <script>
     $('#site_title').html(`Property | Dashboard`)
     $('#dashboard').addClass(`active`)
 </script>
 @endsection
