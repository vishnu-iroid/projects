   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
   </style>
   @section('content')
   <div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="http://127.0.0.1:8000/dashboard">Home</a>
        <span class="breadcrumb-item active">CMS</span>
        <span class="breadcrumb-item active">Privacy Policy</span>
    </nav>


</div>
      <div class="br-pagebody">
      <div class="col-md-12 mg-b-30">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6">
                            <h6 class="card-title h4-card">Privacy Policy</h6>
                           
                        </div>
                    </div>
                </div>
                <div class="card-body">
        <form action="{{ route('addPrivacy') }}" method="POST">
                <div class=" mg-t-10">
                    <h6 class="">English</h6>
                        @csrf
                        <div class="form-layout form-layout-1">
                            <div class="col-lg-12">
                                <input type="hidden" id="id" name="id" value="">
                                <div class="form-group">
                                    <label class="form-control-label">Privacy Policy: <span class="tx-danger">*</span></label>
                                    <textarea class="form-control ckeditor" type="text" rows="5" name="english_description">@if(isset($privacy->english_description)) {{ $privacy->english_description }} @endif</textarea>
                                </div>
                            </div><!-- col-4 -->

                        </div><!-- form-layout -->
                </div>
                <div class=" mg-t-2 mt-4">
                    <h6 class="">Arabic</h4>
                        <div class="form-layout form-layout-1">
                        <div class="row ">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Privacy Policy: <span class="tx-danger">*</span></label>
                                    <textarea id="editor2" class="form-control  ckeditor" type="text" rows="5" name="arabic_description">@if(isset($privacy->arabic_description)) {{ $privacy->arabic_description }} @endif</textarea>
                                </div>
                            </div><!-- col-4 -->
                        </div><!-- row -->

                        <div class="form-layout-footer">
                            <button class="btn btn-info">Submit</button>
                        </div><!-- form-layout-footer -->
                        </div><!-- form-layout -->
                </div>

            </form>
      </div><!-- br-pagebody -->
      </div><!-- br-pagebody -->
    @endsection
    @section('scripts')
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
    <script>
    CKEDITOR.replace('editor2', {
      extraPlugins: 'bidi',
      // Setting default language direction to right-to-left.
      contentsLangDirection: 'rtl',
      height: 270,
      scayt_customerId: '1:Eebp63-lWHbt2-ASpHy4-AYUpy2-fo3mk4-sKrza1-NsuXy4-I1XZC2-0u2F54-aqYWd1-l3Qf14-umd',
      scayt_sLang: 'auto'
    });
  </script>
    @endsection