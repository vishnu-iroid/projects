@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <a class="breadcrumb-item" href="#">Accounts</a>
        <span class="breadcrumb-item active">Details</span>
    </nav>
</div>

<div class="br-pagebody">
    <div class="col-md-12 mg-b-50 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Agent Tracking Details</h4>
                        <p class="card-category"></p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="card-header card-header-tabs-line">
                    <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" id="main-property" href="#property-status" role="tab" aria-controls="property-status" aria-selected="true">Properties Details</a>
                        </li>

                    </ul>
                </div>

                <div class="tab-content pt-12">
                    <div class="tab-pane fade show active" id="property-status" role="tabpanel" aria-labelledby="main-property">
                        <div class="col-md-auto col-12 ">
                            <div class="col-md-12">
                                <div class="property-details">

                                    <form class="form-admin" id="form-add" action="{{route('payAgent')}}" method="POST">
                                        <input type="hidden" name="agent" id="check_in" value="{{$agent_id}}" name="check_in">

                                        <table>
                                            <tbody>

                                                @foreach($datas as $row)
                                                <tr>

                                                    <div class="row justify-content-between">

                                                        <div class="col-md-3">
                                                            @if($row->user_details->name)
                                                            <div class="form-group">
                                                                <label> User: </label>
                                                                <input type="hidden" name="user[]" id="check_in" value="{{$row->user_details->id}}" name="check_in">
                                                                <b>{{$row->user_details->name}}</b>
                                                            </div>
                                                            @endif
                                                        </div>

                                                        <div class="col-md-3">
                                                            @if($row->property_details->property_reg_no)
                                                            <div class="form-group">
                                                                <label> Property Reg NO : </label>
                                                                <b>{{$row->property_details->property_reg_no}}</b>
                                                            </div>
                                                            @endif
                                                        </div>


                                                        <div class="col-md-3">
                                                            @if($row->property_details->property_name)
                                                            <div class="form-group">
                                                                <label> Property Name : </label>
                                                                <input type="hidden" id="property_name[]" value="{{$row->property_details->id}}" name="property_name[]">
                                                                <b>{{$row->property_details->property_name}}</b>
                                                            </div>
                                                            @endif
                                                        </div>

                                                    </div>

                                                    @endforeach
                                                </tr>
                                            </tbody>
                                        </table>




                                        @if(count($datas) > 0)
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label> Date : <span class="tx-danger">*</span></label>
                                                    <input class="form-control address" required id="date" name="date" type="date">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Amount : <span class="tx-danger">*</span></label>
                                                    <input class="form-control address" required name="amount" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary float-right approveBtn">pay</button>
                                            </div>
                                        </div>
                                        @else
                                        <p class="text-danger"> <b>There is No Property Belongs to this Agent</b></p>

                                        @endif
                                    </form>


                                </div>
                            </div>

                        </div>



                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
</div>



@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Accounts Management`);
    $("#account_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    $("#form-add").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            amount: {
                required: true,
            }
        },
        submitHandler: function(form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".approveBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('payAgent')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".approveBtn").text('Processing..');
                },
                success: function(data) {
                    $(".approveBtn").text('Approve');
                    $(".approveBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "{{route('showIncome')}}";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
