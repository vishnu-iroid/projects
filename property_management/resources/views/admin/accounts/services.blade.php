@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Accounts Management</span>
            <span class="breadcrumb-item active">Service Payment Notifications</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Service Payment Notifications</h4>
                            <p class="card-category">List of Payments</p>
                        </div>

                        <div class="col-md-6 d-flex justify-content-end">
                            @if ($status == '1')
                                <a href="{{ route('showPaidService', ['status' => 1]) }}"
                                    class="nav-link tx-white d-flex align-items-center card-active">
                                    <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                                </a>

                                <a href="{{ route('showPaidService', ['status' => 0]) }}"
                                    class="nav-link tx-white d-flex align-items-center ">
                                    <i class="fad fa-check-user tx-20"></i>&nbspUser
                                </a>
                            @elseif($status == '0')
                                <a href="{{ route('showPaidService', ['status' => 1]) }}"
                                    class="nav-link tx-white d-flex align-items-center ">
                                    <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                                </a>

                                <a href="{{ route('showPaidService', ['status' => 0]) }}"
                                    class="nav-link tx-white d-flex align-items-center card-active">
                                    <i class="fad fa-check-user tx-20"></i>&nbspUser
                                </a>
                            @endif

                        </div>
                        <!-- <div class="col-md-8 row d-flex justify-content-center">

                            </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-10p">Date</th>
                                @if ($status == 0)
                                    <th class="wd-10p">Tenant Name</th>
                                @endif
                                @if ($status == 1)
                                    <th class="wd-10p">Owner Name</th>
                                @endif
                                <th class="wd-10p">Contact No</th>
                                <th class="wd-10p">Property Name</th>
                                <th class="wd-10p">Unit Name</th>

                                <th class="wd-5">Service Provider </th>
                                <th class="wd-10">Description</th>
                                @if ($status == 0)
                                <th class="wd-20">Status</th>
                                @endif
                                <th class="wd-10p">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if ($status == 0)
                                @foreach ($request as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->date }}</td>
                                        @if ($row->user_related)
                                            <td>
                                                @if ($row->user_related->name)
                                                    {{ $row->user_related->name }}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                @if ($row->user_related->phone)
                                                    {{ $row->user_related->phone }}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                        @else
                                            <td>---</td>
                                            <td>---</td>
                                        @endif
                                        @if ($row->builder_id != null)
                                            @php
                                                $data = DB::table('owner_properties')
                                                    ->select('id', 'property_name')
                                                    ->where('id', $row->builder_id)
                                                    ->first();
                                            @endphp
                                            <td>{{ isset($data->property_name) ? $data->property_name : '---' }}</td>
                                            <td>{{ $row->property_name }}</td>
                                        @else
                                            <td>{{ $row->property_name }}</td>
                                            <td>--</td>
                                        @endif
                                        <td>{{ $row->service_related->service }}</td>
                                        <td>{{ $row->description }}</td>
                                        @if ($row->owner_payment_checked !=null)
                                        @if ($row->owner_payment_checked == 1)
                                            <td><span class="badge badge-success">Applicable</span></td>
                                        @else
                                            <td><span class="badge badge-danger">Not Applicable</span></td>
                                        @endif
                                        @else
                                        <td>---</td>
                                         @endif
                                        <td>
                                            <a class="fad fa-eye tx-20 view-button mr-1 "
                                                href="{{ route('userServiceDetails', $row->id) }}"
                                                data-booking="{{ $row->id }}" title="View"></a>

                                        </td>


                                    </tr>
                                @endforeach
                            @elseif($status == '1')
                                @foreach ($request as $row)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $row->date }}</td>
                                        @if (isset($row->owner_related->name))
                                            <td>{{ $row->owner_related->name }}</td>
                                            <td>{{ $row->owner_related->phone }}</td>
                                        @else
                                            <td>---</td>
                                            <td>---</td>
                                        @endif
                                        @if ($row->builder_id != null)
                                            @php
                                                $data = DB::table('owner_properties')
                                                    ->select('id', 'property_name')
                                                    ->where('id', $row->builder_id)
                                                    ->first();
                                            @endphp
                                            <td>{{ isset($data->property_name) ? $data->property_name : '---' }}</td>
                                            <td>{{ $row->property_name }}</td>
                                        @else
                                            <td>{{ $row->property_name }}</td>
                                            <td>--</td>
                                        @endif
                                        <td>{{ $row->service_related->service }}</td>
                                        <td>{{ $row->description }}</td>
                                        {{-- @if (isset($row->owner_payment_checked))
                                            @if ($row->owner_payment_checked == 1)
                                                <td><span class="badge badge-success">Applicable</span></td>
                                            @else
                                                <td><span class="badge badge-danger">Not Applicable</span></td>
                                            @endif
                                        @else
                                            <td>--</td>
                                        @endif --}}

                                        <td> <a class="fad fa-eye tx-20 view-button mr-1 "
                                                href="{{ route('ownerServiceDetails', $row->o_id) }}"
                                                data-booking="{{ $row->id }}" title="View">{{ $row->id }}</a>
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {{ $request->links() }}
                    </div>
                    <!-- <div class="d-flex justify-content-end">

                        </div> -->
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->





    <!-------------------------------------------- End ------------------------->


    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>
    <script>
        const url = '{{ url('/') }}';

        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                autoWidth: false,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {List Cancelled Products
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Property | Accounts Management`);
        $("#accounts_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable2").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
    </script>
@endsection
