@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Accounts Management </span>
            <span class="breadcrumb-item active">Reservation Fees </span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-3">
                            <h4 class="card-title h4-card">Reservation Fees</h4>
                            <p class="card-category">List of all Reseservation fee</p>
                        </div>

                    </div>
                </div>
                <div class="card-body">

                    <table id="datatable2" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-15p">Date</th>
                                <th class="wd-20p">User</th>
                                <th class="wd-20p">Property Name</th>
                                <th class="wd-15p">Amount Type</th>
                                <th class="wd-15p">Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->date }}</td>
                                    <td>{{ $row->user_rel->name }}</td>
                                    <td>{{ $row->prop_rel->property_name }}</td>
                                    @if($row->amount_type== '1')
                                    <td>Revenue</td>
                                    @else
                                    <td>Expense</td>
                                    @endif
                                    <td>{{ $row->amount }}</td>
                                    <td><a class="fad fa-view tx-20 viewPayment-button mr-1 btn btn-primary " href="javascript:0;"data-paymentid="{{ $row->id }}" title="view"><i class="fa fa-eye"aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {{-- {!! $details->links() !!} --}}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->

    {{-- view Modal --}}
    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title tx-primary" id="exampleModalLabel">View Payment Details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
           <table class="table">
               <thead>
                <th class="wd-15p">Id</th>
                <th class="wd-15p">Date</th>
                <th class="wd-15p">User</th>
                <th class="wd-15p">Property Name</th>
                <th class="wd-15p">Amount</th>
                <th class="wd-15p"> Balance Amount</th>

               </thead>
               <tbody>
                   <tr>
                       <td class="pid"></td>
                       <td class="pdate"></td>
                       <td class="puser"></td>
                       <td class="pname"></td>
                       <td class="pamount"></td>
                       <td class="balance_amount"></td>
                   </tr>
               </tbody>
           </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </div>
        </div>
      </div>
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    {{-- <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script> --}}
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Property | Accounts Management`);
        $("#account_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable2").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });



        // Revenue payment  view
        $(document).on('click', '.viewPayment-button', function() {
            var paymentid = $(this).data('paymentid');
            var amount = "";
            $.ajax({
                url: url + "/payment-list/view/" + paymentid,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                 console.log(data);
                    if (data.status === true) {
                        $(".pid").html(data.response.id);
                        $(".pdate").html(data.response.date);
                        $(".puser").html(data.response.user_rel.name);
                        $(".pname").html(data.response.prop_rel.property_name);
                        $(".pamount").html(data.response.amount);
                        if(data.response.prop_rel.property_to == 0){
                         amount = data.response.prop_rel.rent - data.response.amount ;
                        }
                        else if(data.response.prop_rel.property_to == 1){
                            amount = data.response.prop_rel.selling_price - data.response.amount ;

                        }
                        else{
                            amount = 0 ;
                        }
                        $('.balance_amount').html(amount);

                        $('#viewModal').modal();
                    } else {
                        toastr["error"](data.response);
                    }
                }
            });

        });
    </script>
@endsection
