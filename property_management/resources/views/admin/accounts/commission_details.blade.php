@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Accounts Management</span>
            <span class="breadcrumb-item active">Brokerage Fee</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-6">
                            <h4 class="card-title h4-card">Brokerage Fee</h4>
                            <p class="card-category">List of all Brokerage Fee</p>
                        </div>

                        <div class="col-md-6 row d-flex justify-content-center">

                            {{-- @if ($status == '0')
                        <a href="{{ route('showUserRent',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-align-user-circle tx-20"></i>&nbspUser Rental
                        </a>

                        <a href="{{ route('showUserRent',['status' => 1])}}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-check-user tx-20"></i>&nbspOwner Commission
                        </a>
                        @elseif($status == '1')
                        <a href="{{ route('showUserRent',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-align-user-circle tx-20"></i>&nbspUser Rental
                        </a>
                        <a href="{{ route('showUserRent',['status' => 1])}}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-check-user tx-20"></i>&nbspOwner Commission
                        </a>
                        @endif --}}


                        </div>
                        <!-- <div class="col-md-8 row d-flex justify-content-center">

                            </div> -->
                    </div>
                </div>
                <div class="card-body table-striped">
                    <table id="datatable1" class="table table-bordered nowrap text-center">
                        <thead>
                            <tr>

                                <th class="wd-5p">Id</th>
                                <th class="wd-15p">Owner</th>
                                <th class="wd-15p">Property Name</th>
                                <th class="wd-15p">Unit Name</th>
                                <th class="wd-20p">Lease Type</th>
                                <th class="wd-20p">Rental Amount</th>
                                <th class="wd-20p">Expected Amount</th>
                                <th class="wd-10p">Attachment</th>
                                <th class="wd-10p">Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $row)
                                <tr>
                                    @if ($row->property_to == '0')
                                        <td>{{ $row->id }} </td>
                                        <td>{{ $row->owner_rel->name }} </td>
                                        @if ($row->builder_id != null)
                                        @php
                                         $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->builder_id)->first();
                                        @endphp
                                    <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                    <td>{{ $row->property_name }}</td>
                                @else
                                    <td>{{ $row->property_name }}</td>
                                    <td>--</td>
                                @endif
                                        @if ($row->frequency)
                                            <td>
                                                @if ($row->frequency == 1)
                                                    weekly
                                                @elseif($row->frequency == '2')
                                                    Monthly
                                                @elseif($row->frequency == '3')
                                                    Bimonthly
                                                @elseif($row->frequency == '4')
                                                    Biweekly
                                                @endif

                                            </td>
                                        @else
                                            <td>--</td>
                                        @endif
                                        <td>{{ $row->rent }}</td>
                                        @if ($row->expected_amount)
                                            <td>{{number_format($row->expected_amount,2) }}</td>
                                        @else
                                            <td>--</td>
                                        @endif
                                        @if ($row->property_priority_image)
                                            <td>
                                                <a href="{{ $row->property_priority_image->document }}"
                                                    class="btn btn-warning btn-xs mrg" target="_blank"><i
                                                        class=" far fa-file-alt"></i></a>

                                            </td>
                                        @else
                                            <td>--</td>
                                        @endif
                                        <td>
                                            <a class="fad fa-check tx-20 accept-button btn btn-primary mb-1 bt_cus" href="javascript:0;" data-admin=""
                                                title="Accept Payment"
                                                onclick="expectedPaymentFunction('{{ $row->id }}','{{ $row->created_at }}')">
                                            </a>
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {{ $details->links() }}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- expectedPaymentModal -->
    <div class="modal fade" id="expectedPaymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" style="overflow: hidden">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-primary reject-modal-title">Owner Payment</h4>
                </div>
                <form action="javascript:;" id="add-payment-form">
                    <div class="modal-body">
                        <label>Date<span class="tx-primary">*</span></label>
                        <input type="hidden" name="propertyId" id="propertyId">

                        <input type="date" class="form-control" name="propertybookingdata" id="propertybookingdata">
                        <label>Amount<span class="tx-primary">*</span></label>
                        <input type="number" class="form-control" name="amount" id="amount">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class add">Accept</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Payment Modal -->
    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">

                <div class="modal-header">
                    <h4 class="modal-title tx-primary reject-modal-title">Rental Payment</h4>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link modalnav active" id="rent-tab" data-toggle="tab" href="#rent" role="tab"
                                aria-controls="profile" aria-selected="true">User Rent</a>
                        </li>
                        <li class="nav-item">
                            <a id="security-tab" class="nav-link modalnav" data-toggle="tab" href="#security" role="tab"
                                aria-controls="security" aria-selected="false">Security Amount</a>
                        </li>
                        <li class="nav-item">
                            <a id="maintenance-tab" class="nav-link modalnav" data-toggle="tab" href="#maintenance"
                                role="tab" aria-controls="maintenance" aria-selected="false">Maintenance</a>
                        </li>
                    </ul>
                </div>
                <form action="javascript:;" id="add-form">
                    <div class="modal-body">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="rent" role="tabpanel" aria-labelledby="rent-tab">
                                <label>Date<span class="tx-primary">*</span></label>
                                <input type="hidden" name="tableId" id="tableId">
                                <input type="text" name="refrence_id" id="refrence_id">
                                <input type="date" class="form-control" name="userbookingdata" id="userbookingdata">

                            </div>
                            <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                                <label>Security Amount<span class="tx-primary">*</span></label>
                                <input type="number" class="form-control" name="sercurity_amount" id="sercurity_amount">
                            </div>
                            <div class="tab-pane fade" id="maintenance" role="tabpanel" aria-labelledby="maintenance-tab">
                                <label>Maintenance Charge<span class="tx-primary">*</span></label>
                                <input type="number" class="form-control" name="maintenance_amount"
                                    id="maintenance_amount">
                            </div>
                        </div>
                        <div class="errorTxt shadow"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                                onclick="window.location.reload();">Cancel</button>
                            <button type="submit" class="btn btn-primary  addBtn" id="SubmitBtn" disabled>Accept</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>




    <!-------------------------------------------- End ------------------------->


    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>
    <script>
        const url = '{{ url('/') }}';

        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id != 'maintenance-tab') {
                $('#SubmitBtn').attr('disabled', true)
            } else {
                $('#SubmitBtn').attr('disabled', false)
            }
        })

        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        // $('.modalnav').click(function() {
        //     var id = $(this).attr('id')
        //     if (id == 'rent-tab') {
        //         $('#SubmitBtn').attr('disabled', false)
        //     } else {
        //         $('#SubmitBtn').attr('disabled', true)
        //     }
        // })
    </script>
    <script>
        $("#site_title").html(`Property | Accounts Management`);
        $("#account_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable2").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
    </script>
    <script>
        function expectedPaymentFunction(id, date, refreal_id) {
            $('#propertyId').val(id);
            $('#refreal_id').val(refreal_id)

            var now = new Date(date);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear() + "-" + (month) + "-" + (day);

            $('#propertybookingdata').val(today);



            $('#expectedPaymentModal').modal()
        }
    </script>
    <script>
        function acceptPaymentFunction(id, date, refrence_id) {
            $('#tableId').val(id);
            $('#refrence_id').val(refrence_id);

            var now = new Date(date);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear() + "-" + (month) + "-" + (day);

            $('#userbookingdata').val(today);



            $('#paymentModal').modal()
        }
    </script>
    <script>
        $("#add-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                userbookingdata: {
                    required: true,
                },
                messages: {
                    date: "Date is required",

                },
            },
            submitHandler: function(form) {
                var form = document.getElementById("add-form");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('payRental') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>
    <script>
        $("#add-payment-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                propertybookingdata: {
                    required: true,
                },
                amount: {
                    required: true,
                },

                messages: {
                    userbookingdata: "Date is required",
                    amount: "Amount Field is Required"

                },
            },
            submitHandler: function(form) {
                var form = document.getElementById("add-payment-form");
                var formData = new FormData(form);
                $(".add").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('payOwnerCommission') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".add").text('Processing..');
                    },
                    success: function(data) {
                        $(".add").text('Submit');
                        $(".add").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>
@endsection
