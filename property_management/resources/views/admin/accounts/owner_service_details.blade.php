@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <a class="breadcrumb-item" href="#">Accounts</a>
        <span class="breadcrumb-item active">Details</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Services Details</h4>
                        <p class="card-category"></p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="card-header card-header-tabs-line">
                    <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" id="main-property" href="#property-status" role="tab" aria-controls="property-status" aria-selected="true">Payment</a>
                        </li>

                    </ul>
                </div>

                <div class="tab-content pt-3">
                    <div class="tab-pane fade show active" id="property-status" role="tabpanel" aria-labelledby="main-property">
                        <div class="row justify-content-between align-items-center mb-5">
                            <div class="col-md-auto col-12 ">
                                <div class="col-md-12">
                                    <div class="property-details">

                                        @if(isset($request->property_name))
                                        <p> Property Name :- <b>{{ $request->property_name }} </b></p>
                                        @else
                                        <p>---</p>
                                        @endif
                                        <p>Property Registration Id :- <b>{{ $request->property_reg_no }} </b></p>
                                        <p>Property Location :-<b> {{ $request->street_address_1 }}</b></p>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <form class="form-admin" id="form-add" action="" method="POST">
                            <div class="row">

                                @if($request->owner_related->name)
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Owner : </label>
                                        <input type="hidden" id="owner" value="{{$request->owner_id}}" name="owner">
                                        <b>{{$request->owner_related->name}}</b>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Owner Address : </label>
                                        <b>{{$request->owner_related->address}}</b>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    @if($request->service_related->id)
                                    <div class="form-group">
                                        <label>Service : </label>
                                        <input type="hidden" value="{{$request->id}}" name="refreal_id">
                                        <input type="hidden" value="{{$request->property_id}}" name="property_id">

                                        <input type="hidden" id="check_out" value="{{$request->service_related->id}}" name="service_id">
                                        <b>{{$request->service_related->service}}</b>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label>Documents : </label>

                                    @if($request->document)
                                    <a href="{{url($request->document)}}" target="_blank" class="btn btn-primary float-right "><img src="{{url($request->document)}}" width="100" height="100" alt="image"></a>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Request Date : </label>

                                        <input type="text" class="form-control" id="check_out" name="date" value="{{ date_format($request->created_at, 'd-m-Y') }}" name="check_out">

                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Amount : <span class="tx-danger">*</span></label>
                                            <input class="form-control address" required id="service_amount" name="service_amount" type="number">
                                        </div>
                                        <button type="submit" class="btn btn-primary float-right payBtn">Add Payment</button>

                                    </div>
                                </div>



                            </div>


                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
</div>



@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script>
    const url = '{{url("/")}}';
</script>
<script>
    $("#form-add").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            service_amount: {
                required: true,
            }
        },
        submitHandler: function(form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".approveBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('ownerServicePayment')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").text('Processing..');
                },
                success: function(data) {
                    $(".approveBtn").text('Approve');
                    $(".approveBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "{{route('showPaidService')}}";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>

@endsection
