@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Service Management</span>
        <span class="breadcrumb-item active">Service Requset</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-6">
                        <h4 class="card-title h4-card">Agents</h4>
                        <p class="card-category">List of Agents</p>
                    </div>

                    <!-- <div class="col-md-6 row d-flex justify-content-center">

                        <a href="{{ route('showPaidService',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                        </a>

                        <a href="{{ route('showPaidService',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-check-user tx-20"></i>&nbspUser
                        </a>

                        <a href="{{ route('showPaidService',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center ">
                            <i class="fad fa-align-user-circle tx-20"></i>&nbspOwner
                        </a>

                        <a href="{{ route('showPaidService',['status' => 0])}}" class="nav-link tx-white d-flex align-items-center card-active">
                            <i class="fad fa-check-user tx-20"></i>&nbspUser
                        </a>


                    </div> -->
                    <!-- <div class="col-md-8 row d-flex justify-content-center">

                    </div> -->
                </div>
            </div>
            <div class="card-body">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-15p">Agent </th>
                            <th class="wd-15p">Address</th>
                            <th class="wd-15p">Contact Number</th>
                            <th class="wd-20">Email</th>
                            <th class="wd-20">Request Amount</th>
                            <th class="wd-10p">Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        @foreach($agents as $row )
                        <tr>

                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->agent_rel->name}}</td>
                            <td>{{$row->agent_rel->address}}</td>
                            <td>{{$row->agent_rel->phone}}</td>
                            <td>{{$row->agent_rel->email}}</td>
                            <td>{{$row->amount}}</td>
                            <td>
                                <a class="fad fa-check tx-20 accept-button" href="javascript:0;" data-admin="" title="PAY" style="color: green " onclick="PaymentFunction('{{$row->id}}','{{ $row->created_at}}')">

                            </td>



                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <!-- <div class="d-flex justify-content-end">

                </div> -->
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div class="modal fade" id="PaymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog">
        <div class="modal-content delete-popup">
            <div class="modal-header">
                <h4 class="modal-title tx-primary reject-modal-title">Owner Payment</h4>
            </div>
            <form action="javascript:;" id="add-payment-form">
                <div class="modal-body">
                    <label>Date<span class="tx-primary">*</span></label>
                    <input type="hidden" name="refreal_id" id="refreal_id">

                    <input type="date" class="form-control" name="date" id="date">
                    <label>Amount<span class="tx-primary">*</span></label>
                    <input type="number" class="form-control" name="amount" id="amount">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                    <button type="submit" class="btn btn-success reject-class add">PAY</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-------------------------------------------- End ------------------------->


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Accounts Management`);
    $("#account_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable2").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    function PaymentFunction(id, date) {
       
        $('#refreal_id').val(id);

        var now = new Date(date);

        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);

        var today = now.getFullYear() + "-" + (month) + "-" + (day);

        $('#date').val(today);

        $('#PaymentModal').modal()
    }
</script>
<script>
    $("#add-payment-form").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            date: {
                required: true,
            },
            amount: {
                required: true,
            },

            messages: {
                date: "Date is required",
                amount: "Amount Field is Required"

            },
        },
        submitHandler: function(form) {
            var form = document.getElementById("add-payment-form");
            var formData = new FormData(form);
            $(".add").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('payAgent')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".add").text('Processing..');
                },
                success: function(data) {
                    $(".add").text('Submit');
                    $(".add").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>

@endsection
