@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Accounts Management</span>
            <span class="breadcrumb-item active">Property Reservation Fees</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property Reservation Fees</h4>
                            <p class="card-category">List of reservations fees</p>
                        </div>
                        <div class="pd-5 mg-r-20">
                            <a href="{{ route('showTokenList') }}"
                                class="nav-link card-active tx-white d-flex align-items-center"><i
                                    class="fad fa-eye tx-20"></i>&nbspView Payments</a>
                        </div>
                    </div>
                </div>
                <div class="card-body p-4">
                    <div class="row">
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Property</label>
                            <select class="form-control filter " id="property_name_filter">
                                <option selected disabled>Select One</option>
                                @foreach ($property as $t)
                                    <option value="{{ $t->id }}"> {{ $t->property_name }} </option>
                                @endforeach
                            </select>
                        </div>
                       
                       
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Owner</label>
                            <select class="form-control filter" id="owner_id">
                                <option selected disabled>Select One</option>
                                <option value="0">All</option>
                                @foreach ($owners as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="datatableclient_filter" class="dataTables_filter col-md-4">
                           <label>Agents</label>
                           <select class="form-control filter" id="agent_id">
                            <option selected disabled>Select One</option>
                            <option value="0">All</option>
                            @foreach ($agents as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                        </div>
                        {{-- <div id="datatableclient_filter" class="dataTables_filter col-md-3"><center><label>AREA</label><input type="search" class="filter" placeholder="" aria-controls="datatableclient"></div> --}}



                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table  table-responsive nowrap">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Applicant Name</th>
                                <th>Contact No</th>
                                <th>Property Name</th>
                                <th>Unit Name</th>
                                <th>Net Amount</th>
                                <th>Recieved Amount</th>
                                <th>Balnce Amount</th>
                                <th>Attachments</th>
                                <th> BookingId</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $row)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        @if ($row->user_details)
                                            @if ($row->user_details->name)
                                                {{ $row->user_details->name }}
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if ($row->user_details)
                                            @if ($row->user_details->phone)
                                                {{ $row->user_details->phone }}
                                            @endif
                                        @endif
                                    </td>
                                       
                                    @if ($row->property_relation->builder_id != null)
                                            @php
                                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->property_relation->builder_id)->first();
                                            @endphp
                                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                        <td>{{ $row->property_relation->property_name }}</td>
                                    @else
                                        <td>{{ $row->property_relation->property_name }}</td>
                                        <td>--</td>
                                    @endif
                                    @if ($row->property_details)
                                        @php
                                            $rent = $row->property_details->rent;
                                            $security_deposit = $row->property_details->security_deposit;
                                            $net_amount = $rent + $security_deposit;
                                        @endphp
                                        <td>
                                            {{  number_format($net_amount,2) }}
                                        </td>
                                    @endif

                                    @if ($row->property_details)
                                        <td>{{ number_format($row->property_details->token_amount,2) }}</td>
                                    @endif
                                    @if ($row->property_details)
                                        @php
                                            $balance = $net_amount - $row->property_details->token_amount;
                                        @endphp
                                        <td>{{ number_format($balance,2) }}</td>
                                    @endif

                                    @if ($row->booking_doc)
                                        <td> <a href="{{ url($row->booking_doc->document) }}"
                                                class="btn btn-warning btn-xs mrg" data-placement="top"
                                                data-toggle="tooltip" data-original-title="Download"><i
                                                    class=" far fa-file-alt"></i></a></td>
                                    @else
                                        <td> --- </td>
                                    @endif


                                    <td>{{ $row->booking_id }}</td>
                                    <td>
                                        <a class="fas fa-money-bill tx-20 accept-button btn btn-primary bt_cus mr-1" href="javascript:0;" data-admin=""
                                            title="Accept Payment"
                                            onclick="PaymentFunction('{{ $row->id }}','{{ $row->created_at }}','@if ($row->booking_doc) {{ $row->booking_doc->id }}@else 0 @endif','@if ($row->property_details) {{ $row->property_details->token_amount }} @endif')">
                                        </a>
                                        <a class="fad fa-check tx-20 btn  btn-danger  bt_cus" href="javascript:0;" onclick="isRejected({{ $row->id }})" data-accountid="{{ $row->id }}" title="Reject"></a>

                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <div class="d-flex justify-content-end">
                        {!! $details->links() !!}
                    </div>
                    <!-- <div class="d-flex justify-content-end">

                                                    </div> -->
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Payment Modal -->
    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-primary reject-modal-title">Token Payment</h4>
                </div>
                <form action="javascript:;" id="add-form">
                    <div class="modal-body">
                        <label>Date<span class="tx-primary">*</span></label>
                        <input type="hidden" name="refrenceId" id="refrenceId">
                        <input type="hidden" name="tableId" id="tableId">
                        <input type="date" class="form-control" name="data" id="data">
                        <label>Amount<span class="tx-primary">*</span></label>
                        <input type="number" class="form-control paymenttopay" name="amount" id="amount">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                            onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class addBtn">Accept</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Reject Modal -->
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger reject-modal-title">Reject Request</h4>
                </div>
                <form action="javascript:;" id="delete-form">
                    <div class="modal-body">
                        Are you sure you want to Reject this Request?
                        <input type="hidden" name="bookingId" id="bookingId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtn">Reject</button>
                    </div>
                </form>
            </div>
        </div>
    </div>





    <!-------------------------------------------- End ------------------------->
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script> --}}
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    {{-- <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script> --}}
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Property | Accounts Management`);
        $("#account_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            // $(".dataTables_length select").select2({
            //     minimumResultsForSearch: Infinity,
            // });
        });
    </script>
    <script>
        function PaymentFunction(id, date, refrenceId, amount) {
            $('#tableId').val(id);
            $('#refrenceId').val(refrenceId);
            var now = new Date(date);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear() + "-" + (month) + "-" + (day);

            $('#data').val(today);
            var Famount = amount.replace(/\s/g, '')
            $('#amount').val(Famount);
            $('#paymentModal').modal()
        }

        function viewpaymentFunction(id, date, refrenceId, amount) {
            $('#tableId').val(id);
            $('#refrenceId').val(refrenceId);
            var now = new Date(date);

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear() + "-" + (month) + "-" + (day);

            $('#data').val(today);
            var Famount = amount.replace(/\s/g, '')
            $('#amount').val(Famount);
            $('#paymentModal').modal()
        }
    </script>


<script>
    function isRejected(id) {
        $('#bookingId').val(id);
        $('#rejectModal').modal()
    }
</script>
<script>
        // function isVerified(id) {
        //     $.ajax({
        //         url: url + "/account/verify/" + id,
        //         type: "GET",
        //         processData: false,
        //         async: true,
        //         header: {
        //             "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
        //         },
        //         success: function(data) {
        //             console.log(data);
        //             if (data.status === true) {
        //                 toastr["success"](data.response);
        //                 setTimeout(function() {
        //                     window.location.href = "";
        //                 }, 1000);
        //             } else {
        //                 toastr["error"](data.response);
        //             }
        //         }
        //     });
        // }

        $('.deletetBtn').click(function() {
        var id = $('#bookingId').val()

        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/cancel/pay/request/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".deletetBtn").text('Processing..');
            },
            success: function(data) {

                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    </script>
    <script>
        $("#add-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                data: {
                    required: true,
                },
                messages: {
                    date: "Date is required",

                },
            },
            submitHandler: function(form) {
                var form = document.getElementById("add-form");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $(".closeBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{ route('payToken') }}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").text('Processing..');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        $(".closeBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>
@endsection
