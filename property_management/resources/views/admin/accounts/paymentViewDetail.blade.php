@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Income </span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-3">
                        <h4 class="card-title h4-card">Property Payment View</h4>
                        <p class="card-category">List of  UserToken Payment View</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="{{ route('showPropertyToken') }}"
                            class="nav-link card-active tx-white d-flex align-items-center"><i
                                class="fas fa-chevron-left tx-20" ></i>&nbspBack</a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <table id="datatable2" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Date</th>
                            <th class="wd-15p">User</th>
                            <th class="wd-15p">Property Name</th>
                            <th class="wd-15p">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- {{dd($user)}} --}}
                       {{--  --}}
                        <tr>
                            <td>{{ $user->b_id}}</td>
                            <td>{{ $user->date}}</td>
                            <td>{{ $user->user_details->name}}</td>
                            <td>{{ $user->property_details->property_name}}</td>
                            @if($user->account_rel)
                            @if($user->account_rel->payment_type ==2)
                            <td>{{$user->account_rel->amount}}</td>
                            @else
                            <td>{{0}}</td>
                            @endif
                            @else
                            <td>{{0}}</td>
                         
                            @endif

                        </tr>
                      

                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {{-- {!! $details->links() !!} --}}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<!-- Add and Edit Modal -->


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
{{-- <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script> --}}
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Property | Accounts Management`);
    $("#account_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable2").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
@endsection
