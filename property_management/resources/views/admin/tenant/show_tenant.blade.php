@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <a class="breadcrumb-item" href="">Residents</a>
            <span class="breadcrumb-item active">Details</span>
        </nav>
    </div>

    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Residents Details</h4>
                            <p class="card-category"></p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-header card-header-tabs-line">
                        <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" id="main-property" href="#property-status"
                                    role="tab" aria-controls="property-status" aria-selected="true">Property</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" id="main-approve" href="#service-request" role="tab"
                                    aria-selected="false">Service Requests</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" id="main-approve" href="#payment-history" role="tab"
                                    aria-selected="false">Payment History</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content pt-3">
                        <div class="tab-pane fade show active" id="property-status" role="tabpanel"
                            aria-labelledby="main-property">
                            <div class="row justify-content-between align-items-center mb-5">
                                <div class="col-md-auto col-6 ">
                                    <div class="col-md-12">

                                            <div class="property-details">
                                                @if($tenant)
                                                    @if ($tenant->prop_rel->property_reg_no)
                                                            <p>Property :- <b>{{ $tenant->prop_rel->property_reg_no }}</b></p>
                                                    @endif
                                                @endif
                                                @if ($tenant->prop_rel->property_name)
                                                        <p>Property Name :-
                                                            <b>{{ $tenant->prop_rel->property_name }}</b>
                                                        </p>
                                                @endif
                                                @if ($tenant->prop_rel->property_to == '0')
                                                    <p >Property for :- <b>Rent</b></p>
                                                @else
                                                    <p >Property for :- <b>Sale</b></p>
                                                @endif
                                                @if ($tenant->contract_rel)
                                                   @if ($tenant->contract_rel->contract_status)
                                                        @if ($tenant->contract_rel->contract_status == 1)
                                                        @if ($tenant->contract_rel->contract_start_date )

                                                        <p> Contract Start Date : <b> {{$tenant->contract_rel->contract_start_date }} </b> </p>

                                                        @endif
                                                        @if ($tenant->contract_rel->contract_end_date )

                                                        <p> Contract End Date : <b> {{$tenant->contract_rel->contract__date }} </b> </p>

                                                        @endif
                                                        @if($tenant->contract_rel->contract_no)
                                                            <p>Contract No <b> {{ $tenant->contract_rel->contract_no }} </b></p>
                                                        @endif
                                                        @if($tenant->contract_rel->contract_file)
                                                                <a href="{{ url($tenant->contract_rel->contract_no) }}" target="_blank"
                                                                class="btn btn-primary float-right ">Contract Copy</a>
                                                        @endif
                                                        @endif
                                                   @endif
                                                @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show " id="service-request" role="tabpanel" aria-labelledby="main-property">
                            <div class="row justify-content-between align-items-center mb-5">
                                <div class="col-md-auto col-6 ">
                                    <div class="col-md-12">

                                            <div class="service-request">
                                               @if ($request)
                                               <table class="table table-striped">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">No</th>
                                                    <th scope="col">SERVICE</th>
                                                    <th scope="col">DATE</th>
                                                    <th scope="col">TIME</th>
                                                    <th scope="col">STATUS</th>

                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @if ($request)
                                                    @foreach ($request as $a )
                                                    <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    @if ($a->service_related)
                                                        @if ($a->service_related->service)<td>{{$a->service_related->service }} </td>@endif
                                                        @if ($a->date)<td>{{ $a->date }}</td>@endif
                                                        @if ($a->time)<td>{{ $a->time }} </td>@endif
                                                        @if ($a->description)<td>{{ $a->description }} </td>@endif
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                               </table>
                                               @endif
                                            </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade show " id="payment-history" role="tabpanel" aria-labelledby="main-property">
                            <div class="row justify-content-between align-items-center mb-5">
                                <div class="col-md-auto col-6 ">
                                    <div class="col-md-12">

                                            <div class="service-request">
                                                @if($accounts)
                                                <table class="table table-striped">
                                                    <thead>
                                                      <tr>
                                                        <th scope="col">No</th>
                                                        <th scope="col">DATE</th>
                                                        <th scope="col">PAYMENT TYPE</th>
                                                        <th scope="col">PROPERTY</th>
                                                        <th scope="col">AMOUNT</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                          @if ($accounts)
                                                            @foreach ($accounts as $a )
                                                            <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>{{ $a->date }} </td>
                                                            <td>
                                                            @if ($a->payment_type == 7)

                                                            {{"Owner Property Payment"}}

                                                            @elseif ($a->payment_type == 5)

                                                            {{"User rent payment"}}

                                                            @elseif ($a->payment_type == 1 )

                                                            {{"User Service Payment"}}

                                                            @elseif ($a->payment_type == 3 )

                                                           {{"User Token Payment"}}

                                                           @elseif ($a->payment_type == 2 )

                                                           {{"User Security Payment"}}

                                                           @elseif ($a->payment_type = 4)

                                                           {{ "User Full Amount Payment" }}

                                                           @elseif ($a->payment_type = 6)

                                                           {{"Owner Service Payment" }}

                                                           @elseif ($a->payment_type = 8)

                                                           {{"Maintenance Amount Payment" }}

                                                           @elseif ($a->payment_type = 9)

                                                           {{ "Agent Commission" }}

                                                            @endif
                                                            </td>
                                                            @if($a->prop_rel)
                                                            <td>{{ $a->prop_rel->property_reg_no }}</td >
                                                            @endif
                                                            <td>{{$a->amount}}</td>
                                                            </tr>

                                                             @endforeach
                                                          @else
                                                              <b>No Data Found </b>
                                                          @endif
                                                    </tbody>
                                                  </table>
                                                @endif

                                            </div>

                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->


    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

    <script>
        const url = '{{ url('/') }}';

    </script>


@endsection
