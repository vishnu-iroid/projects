@extends('admin.layout.app')

<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Property Management</span>
            <span class="breadcrumb-item active">Residents</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <!-- <div class="float-right mg-t-8 mg-r-40">
            <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
                <i class="fas fa-plus-square"></i>&nbsp Add Client
            </button>
    </div> -->
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Residents</h4>
                            <p class="card-category">List of all Residents </p>
                        </div>
                        <div class="col-md-6 row justify-content-center">
                            @if ($status == 0)
                            <a href="{{ route('showTenants',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fas fa-warehouse tx-20"></i>&nbspRental
                            </a>
                            <a href="{{ route('showTenants',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fas fa-house-user  tx-20"></i>&nbspOwned
                            </a>
                            @elseif ($status == 1)
                            <a href="{{ route('showTenants',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fas fa-warehouse tx-20"></i>&nbspRental
                            </a>
                            <a href="{{ route('showTenants',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fas fa-house-user  tx-20"></i>&nbspOwned
                            </a>
                            @endif


                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                @if($status== 0)
                                <th class="wd-5p">Residents</th>
                                @elseif($status== 1)
                                <th class="wd-5p">Owner</th>
                                @endif
                                {{-- <th class="wd-5p">Email</th> --}}
                                <th class="wd-5p">Contact</th>
                                <th class="wd-5p">Due Amount</th>
                                <th class="wd-5p">Property Name</th>
                                <th class="wd-5p">Unit Name</th>

                                <th class="wd-5p">Security Deposit </th>
                                @if($status== 0)
                                <th class="wd-5p">Lease type </th>
                               
                                <th class="wd-5p">Due Date </th>
                                @endif
                                <th class="wd-5p">status </th>

                                <th class="wd-5p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- @if($status== 00) --}}
                            @foreach ($tenant as $row)
                                <tr>

                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $row->user_rel->name }}</td>
                                    <td>{{ $row->user_rel->phone }}</td>
                                    <td>{{ $row->rent }}
                                        @if ($row->prop_rel->builder_id != null)
                                        @php
                                         $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->prop_rel->builder_id)->first();
                                        @endphp
                                    <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                    <td>{{ $row->prop_rel->property_name }}</td>
                                @else
                                    <td>{{ $row->prop_rel->property_name }}</td>
                                    <td>--</td>
                                @endif
                                    <td>
                                        @if ($row->prop_rel->security_deposit)
                                            {{ $row->prop_rel->security_deposit }}
                                        @else
                                        --
                                        @endif
                                    </td>
                                    @if($status== 0)
                                    <td>
                                        @if ($row->prop_rel->frequency == '1' || $row->prop_rel->frequency == '4')
                                            weekly
                                        @elseif($row->prop_rel->frequency == '2' || $row->prop_rel->frequency == '3')
                                            Monthly

                                        @endif
                                    </td>
                                    @endif
                                    @if($status== 0)
                                    <td>
                                        {{$row->due_date}}
                                    </td>
                                    @endif
                                   
                                    <td>
                                        @if($row->status== 0)
                                        <label for="">Rental</label>
                                        @elseif ($row->status== 1)
                                        <label for="">Owned</label>
                                        @elseif ($row->status== 2)
                                        <label for="">Booked</label>
                                        @elseif ($row->status== 3)
                                        <label for="">Vacated</label>
                                        @endif
                                    </td>
                                    <td>     <a class="fad fa-eye tx-20 view-button mr-1 btn btn-primary bt_cus" href="{{route('residentsDetails', ['id' => $row->id])}}" data-booking="{{$row->id}}" title="View"></a>
                                    </td>


                                </tr>

                            @endforeach
                            {{-- @endif --}}
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $tenant->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->



    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script> --}}
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

    <script>
        const url = '{{ url('/') }}';

    </script>
    <script>
        $("#site_title").html(`Tenant | Tenants`);
        $("#tenant").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });

    </script>

@endsection
