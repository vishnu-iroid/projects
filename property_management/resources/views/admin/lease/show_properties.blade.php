@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }
    .error {
        color: red !important;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <span class="breadcrumb-item active">Property Management</span>
        <span class="breadcrumb-item active">Add Lease / Teants</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Add Lease / Teants</h4>
                        <p class="card-category">List of all Lease / Teants</p>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatableproperty" class="table data-table printTable" style="width:100%">

                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-15p">Property Name</th>
                            <th class="wd-10">Unit</th>
                            <th class="wd-10p">Owner</th>
                            <th class="wd-15p">Contract start date</th>
                            <th class="wd-15p">Contract End Date</th>
                            <th class="wd-10p">rent</th>
                            <th class="wd-15p">Frequency</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
     <!-- ----owner add modal---- -->
     <div class="modal fade contractUpload" id="addPropertyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content delete-popup">
             <div class="modal-header">
                 <h4 class="modal-title tx-danger reject-modal-title">Add Lease</h4>
             </div>
             <div class="col-md-12">
                <ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link modalnav active" id="profile-tab" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile" aria-selected="true">Tenant</a>
                    </li>
                    <li class="nav-item">
                        <a id="bank-tab" class="nav-link modalnav" data-toggle="tab" href="#bank2" role="tab" aria-controls="bank" aria-selected="false">Contract Details</a>
                    </li>
                    <li class="nav-item">
                        <a id="bank-tab" class="nav-link modalnav" data-toggle="tab" href="#rent" role="tab" aria-controls="bank" aria-selected="false">Rental Details</a>
                    </li>
                </ul>
            </div>
             <form action="javascript:;" id="lease-type-form" enctype="multipart/form-data">
                 @csrf
                 <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="profile1" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group ">
                                        <label>Existing Tenant <span class="tx-danger">*</span></label>
                                        <input class="form-check-input ml-2" type="checkbox" id="existing_tenant" name="existing_tenant" value="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="new-tenant">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name <span class="tx-danger">*</span></label>
                                            <input type="hidden" id="property_id" name="property_id">
                                            <input class="form-control" id="name" name="name" type="text" placeholder="Enter your name"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email<span class="tx-danger">*</span></label>
                                                <input class="form-control" id="email" name="email" type="text" placeholder="Enter your email"/>
                                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Country<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="country" id="country">
                                            <option value=""></option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>State<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 state-select" name="state" id="state">
                                            <option value=""></option>
                                            @foreach($states as $state)
                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>City<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 city-select" name="city" id="city">
                                            <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Zipcode<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 zipcode-select" name="zipcode" id="zipcode">
                                                <option value=""></option>

                                            </select>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone<span class="tx-danger">*</span></label>
                                            <input class="form-control" id="phone" name="phone" type="text" placeholder="Enter your Phone"/>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="status" id="status">
                                                <option value="0">Inactive</option>
                                                <option value="1" selected>Active</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Address <span class="tx-danger">*</span></label>
                                    <textarea class="form-control address" id="address" name="address" type="text" placeholder="Enter address"></textarea>
                                </div>
                                <div id="map"></div>
                                <div class="row">
                                    <input type="hidden" class="form-control col-md-3" id="lat" name="lat">
                                    <input type="hidden" class="form-control col-md-3" id="lng" name="lng">
                                </div>
                            </div>
                            <div class='existing-tenant'>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tenant<span class="tx-danger">*</span></label>
                                            <select class="form-control select2" name="tenant_id" id="tenant_id">
                                                <option></option>
                                                @foreach ($user as  $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- tab-2 start -->
                        <div class="tab-pane fade" id="bank2" role="tabpanel" aria-labelledby="bank-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract No</label>
                                            <input type="text" class="form-control" name="contract_no" id="contract_no">
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract File</label>
                                            <input type="file" class="form-control" name="contract_file" id="contract_file">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Check-In Date<span class="tx-danger">*</span></label>
                                            <input type="date" class="form-control fc-datepicker" name="check_in_date"
                                                id="contract_start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Check-Out Date<span class="tx-danger">*</span></label>
                                            <input type="date" class="form-control fc-datepicker" name="check_out_date"
                                                id="contract_end_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract Start Date<span class="tx-danger">*</span></label>
                                            <input type="date" class="form-control fc-datepicker" name="contract_start_date"
                                                id="contract_start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contract End Date<span class="tx-danger">*</span></label>
                                            <input type="date" class="form-control fc-datepicker" name="contract_end_date"
                                                id="contract_end_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No. of Free Maintenance <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="free_maintenance"
                                                id="selling_price">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Maintenance Charge <span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="maintenance_charge" id="mrp">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <!-- tab-2 end -->
                        <!-- tab-2 start -->
                        <div class="tab-pane fade" id="rent" role="tabpanel" aria-labelledby="bank-tab">
                            <div class="rentdetails">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Lease Type<span class="tx-danger">*</span></label>
                                            <select class="form-control select2 rentfrequency" name="frequency"
                                                id="frequency">
                                                <option value="">select</option>
                                                @foreach ($frequency as $freq)
                                                    <option value="{{ $freq->id }}">{{ $freq->type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Rent<span class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="min_rent"
                                                name="min_rent" placeholder="Enter rent">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Security Deposit<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" name="sercurity_amount"
                                                id="security_deposit" placeholder="Enter Security deposit">
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Payment Attachment<span
                                                    class="tx-danger">*</span></label>
                                            <input type="file" name="attachment" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="sellingdetails">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Min Selling price<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="min_sellingprice"
                                                name="min_sellingprice"
                                                placeholder="Enter minimum selling price">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Max Selling price<span
                                                    class="tx-danger">*</span></label>
                                            <input type="text" class="form-control" id="max_sellingprice"
                                                name="max_sellingprice"
                                                placeholder="Enter maximum selling price">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}

                    </div>
                    <!-- tab-2 end -->
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-default bg-purple tx-white closeBtn"
                         onclick="window.location.reload();">Cancel</button>
                     <button type="submit" class="btn btn-danger reject-class addBtn">Submit</button>
                 </div>
                 </div>
             </form>
         </div>
     </div>
 </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
@endsection
@section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly" async></script>
<script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>

    <script>
        const url = '{{url("/")}}';
        $('.existing-tenant').hide();


        $(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ route('showOwnedProperty') }}",
                    data: function(d){
                        d.search = $('input[type="search"]').val();
                    }
                },
                columns: [
                    {
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },
                    {
                        data: 'property_name',
                        name: 'property_name',

                    },
                    {
                        data: 'unit',
                        name: 'unit',

                    },
                    {
                        data:'owner',
                        name:'owner'
                    },
                    {
                        data: 'contract_start_date',
                        name: 'contract_start_date'
                    },
                    {
                        data: 'contract_end_date',
                        name: 'contract_end_date'
                    },
                    {
                        data: 'rent',
                        name: 'rent'
                    },
                    {
                        data: 'frequency',
                        name: 'frequency'
                    },
                    {
                        data:'actions',
                        name:'actions'
                    }

                ]
            });
            $('.filter').change(function() {
                table.draw();
            });


        });
    </script>
     <script>
        $('.state-select').change(function(){
            var stateId = $(this).val()
            $.ajax({
                url : url+"/get-cities/"+stateId,
                type : "GET",
                processData : false,
                async : true,
                header : {
                    "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
                },
                success : function(data){
                    $('.city-select').html('')
                    var html = '<option value=""></option>';
                    if(data.status === true){
                        if(data.response.length > 0){
                            data.response.forEach((val,key)=>{
                                html += `<option value=`+val.id+`>`+val.name+`</option>`
                            })
                            $('.city-select').append(html)
                        }else{
                            $('.city-select').html('')
                        }
                    }else{
                        $('.city-select').html('')
                    }
                }
            })
            return false
        })
    </script>
    <script>
        $('.city-select').change(function(){
            var cityId = $(this).val()
            $.ajax({
                url : url+"/get-pincodes/"+cityId,
                type : "GET",
                processData : false,
                async : true,
                header : {
                    "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
                },
                success : function(data){
                    $('.zipcode-select').html('')
                    var html = '<option value=""></option>';
                    if(data.status === true){
                        if(data.response.length > 0){
                            data.response.forEach((val,key)=>{
                                html += `<option value=`+val.id+`>`+val.pincode+`</option>`
                            })
                            $('.zipcode-select').append(html)
                        }else{
                            $('.zipcode-select').html('')
                        }
                    }else{
                        $('.zipcode-select').html('')
                    }
                }
            })
            return false
        })
    </script>
    <script>

        $("#lease-type-form").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            // name: {
            //     required: true,
            // },
            // email: {
            //     required: true,
            // },
            // phone: {
            //     required: true,
            //     maxlength : 10,
            // },
            // owner: {
            //     required: true,
            // },
            // address: {
            //     required: true,
            // },
            // latitude: {
            //     required: true,
            // },
            // longitude: {
            //     required: true,
            // },
            // status: {
            //     required: true,
            // },
            // contract_start_date: {
            //     required: true,
            // },
            // contract_end_date: {
            //     required: true,
            // },
            // check_in_date :{
            //     required: true,
            // },
            tenant_id:{
                required:'#existing_tenant:checked',
            }

        },
        submitHandler: function (form) {
            var form = document.getElementById("lease-type-form");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addLeaseType')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
    </script>
    <script>
        $(document).on("click", "#existing_tenant", function() {

            var check_val = $(this).prop("checked");
            if (check_val) {
                $('.existing-tenant').show();
                $('.new-tenant').hide();

            } else {
                $('.existing-tenant').hide();
                $('.new-tenant').show();
            }
            });
    </script>
    <script>
        // for add User modal
        function assignUserFunction(id) {
                $('#property_id').val(id);
                $('#addPropertyModal').modal();
            }
    </script>
    <script>

        function initMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            if ($('#lat').val() != "" && $('#lng').val() != "") {
                const uluru = {
                    lat: $('#lat').val(),
                    lng: $('#lng').val()
                };
            }
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#lat').val() == "" && $('#lng').val() == "") {
                        $('#lat').val(parseFloat(position.coords.latitude))
                        $('#lng').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#lat').val()
                    var lng = $('#lng').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat').val(marker.position.lat())
                $('#lng').val(marker.position.lng())
            })
        }
    </script>
    <script>
        function view_initMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            if ($('#view_lat').val() != "" && $('#view_lng').val() != "") {
                const uluru = {
                    lat: $('#view_lat').val(),
                    lng: $('#view_lng').val()
                };
            }
            const map = new google.maps.Map(document.getElementById("view_map"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#view_lat').val() == "" && $('#view_lng').val() == "") {
                        $('#view_lat').val(parseFloat(position.coords.latitude))
                        $('#view_lng').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#view_lat').val()
                    var lng = $('#view_lng').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#view_lat').val(marker.position.lat())
                $('#view_lng').val(marker.position.lng())
            })
        }
    </script>


@endsection
