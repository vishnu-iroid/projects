@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }
    .modal { overflow: auto !important; }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Employees Management</span>
    </nav>
</div>
<!-- br-pageheader -->
<!-- <div class="float-right mg-t-8 mg-r-40">
        <button class="btn btn-primary btn-md mg-r-30 mt-1 mb-1 shadow-lg" data-toggle="modal" data-target="#addModal" >
            <i class="fas fa-plus-square"></i>&nbsp Add Client
        </button>
</div> -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Employees Management</h4>
                            <p class="card-category">List of all Employees</p>
                        </div>
                        <div class="pd-5 mg-r-20">
                            <a href="#" class="nav-link card-active tx-white d-flex align-items-center"  data-toggle="modal" data-target="#addModal"><i class="fad fa-plus-hexagon tx-15"></i>&nbspAdd Employee</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive wrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th class="wd-15p">First Name</th>
                                <th class="wd-15p">Last  Name</th>
                                <th class="wd-15p">Phone</th>
                                {{-- <th class="wd-15p">Role</th> --}}

                                <th class="wd-20p">Satus</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($admin as $row)
                            <tr>
                                <td>{{$loop->iteration}}</td> 
                                <td>{{$row->first_name}}</td>
                                <td>{{$row->last_name}}</td>
                                <td>{{$row->phone}}</td>
                                {{-- <td>{{$row->role}}</td> --}}
                                <td>
                                    {{-- <span class="badge rounded-pill bg-warning text-dark">
                                        {{ $role_name }}
                                    </span> --}}
                                    @if($row->active == 0)
                                        <span class="badge rounded-pill bg-danger text-white"> Blocked </span>
                                    @else
                                        <span class="badge rounded-pill bg-active text-white"> Active </span>
                                    @endif
                                </td>
                           
                                <td>
                                    <a class="fad fa-edit tx-15 edit-button btn btn-primary mb-1 bt_cus" href="javascript:0;" data-admin="{{$row->id}}" title="Edit"></a>
                                    <a class="fad fa-trash tx-15  btn btn-danger mb-1 bt_cus" href="javascript:0;" data-employee="{{$row->id}}" title="Delete" onclick="deleteFunction('{{$row->id}}')">
                            </a>
                                    @if($row->active == 0)
                                        <a class="fad fa-unlock-alt tx-15 reject-button " href="javascript:0;" data-admin="" title="Unblock"
                                                style="color: green " onclick="blockFunction('{{$row->id}}','1')">
                                        </a>
                                        
                                    @else
                                        <a class="fad fa-ban tx-15 reject-button btn btn-info mb-1 bt_cus" href="javascript:0;" data-admin="" title="Block"
                                                 onclick="blockFunction('{{$row->id}}','0')">
                                        </a>
                                    @endif
                                    &nbsp;
                                    {{-- <a class="fad fa-plus tx-20  mr-1" href="{{url('/roles/addpermission').'/'.$row->id}}"  title="Assign Role"></a> --}}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    {!! $admin->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<div
    class="modal fade"
    id="addModal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel"
    aria-hidden="true"
    style="overflow: hidden"
>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center tx-primary" id="myModalLabel">Add Employee</h4>
            </div>
                <form class="form-admin" id="form-add" action="javascript:;">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>First name <span class="tx-danger">*</span></label>
                                    <input type="hidden" id="id" name="id">
                                    <input class="form-control" id="first_name" name="first_name" type="text" placeholder="Enter first name"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last name <span class="tx-danger">*</span></label>
                                        <input class="form-control" id="last_name" name="last_name" type="text" placeholder="Enter last name"/>
                                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Country<span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="country" id="country">
                                    <option value="">Select Country</option>
                                    @foreach($country as $con)
                                        <option value="{{$con->id}}">{{$con->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>State<span class="tx-danger">*</span></label>
                                    <select class="form-control select2 state-select" name="state" id="state">
                                    {{-- <option value=""></option>
                                    @foreach($state as $st)
                                        <option value="{{$con->id}}">{{$st->name}}</option>
                                    @endforeach --}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>City<span class="tx-danger">*</span></label>
                                    <select class="form-control select2 city-select" id="city" name="city">
                                    {{-- <option value=""></option> --}}

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Zipcode<span class="tx-danger">*</span></label>
                                    <select class="form-control select2 zipcode-select" id="zipcode" name="zipcode">
                                        {{-- <option value=""></option> --}}

                                    </select>
                                    {{-- </select> --}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Address <span class="tx-danger">*</span></label>
                            <textarea class="form-control" id="address" name="address" type="text" placeholder="Enter address"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email <span class="tx-danger">*</span></label>
                                    <input class="form-control" id="email" name="email" type="text" placeholder="Enter email">
                                </div>
                            </div>

                            <div class="col-md-6">

                                <!-- <div class="form-group">
                                    <label>Phone <span class="tx-danger">*</span></label>
                                    <input class="form-control inner" id="phone" name="phone" type="text" placeholder="Enter phone" style="position:relative">
                                    <select name="countryCode" id="countryCode">
                                        {{-- <option value="">Select</option> --}}
                                        @foreach ($countries as $country )
                                        <option value="{{$country->country_code}}">{{$country->country_code}}</option>
                                    @endforeach
                                    </select>
                                </div> -->
                                <div class="form-group">
                                    <label>Phone <span class="tx-danger">*</span></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">
                                            <select name="countryCode" id="countryCode">
                                                    {{-- <option value="">Select</option> --}}
                                                    @foreach ($countries as $country )
                                                    <option value="{{$country->country_code}}">{{$country->country_code}}</option>
                                                @endforeach
                                                </select>
                                            </span>
                                        </div>
                                        <input class="form-control inner" id="phone" name="phone" type="text" placeholder="Enter phone" style="position:relative">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password <span class="tx-danger">*</span></label>
                                    <input class="form-control" id="password" name="password" type="text" placeholder="Enter password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Role<span class="tx-danger">*</span></label>
                                    <select class="form-control" name="role" id="role" required>
                                    <option value="">Select One</option>
                                    @if(!empty($roles))
                                    @foreach ($roles as $it)
                                       <option value="{{$it->id}}" @if(old('role') == $it->id)  selected @endif>{{$it->name}} </option>
                                    @endforeach
                                    @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Department<span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="dept" id="dept">
                                    <option value=""></option>
                                    @foreach($department as $dept)
                                        <option value="{{$dept->id}}">{{$dept->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Designation<span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="des" id="des">
                                    <option value=""></option>
                                    @foreach($designation as $des)
                                        <option value="{{$des->id}}">{{$des->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div><div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status<span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="status" id="status">
                                        <option value="0">Inactive</option>
                                        <option value="1" selected>Active</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();" > Close </button>
                        <button type="submit" class="btn btn-primary addBtn">Submit</button>
                    </div>
                </form>
        </div>
    </div>
</div>

<!-- Reject Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content delete-popup">
         <div class="modal-header">
            <h4 class="modal-title tx-danger reject-modal-title">Block Employee</h4>
         </div>
         <form action="javascript:;" id="delete-form">
            <div class="modal-body">
                Are you sure you want to block this Employee?
               <input type="hidden" name="adminId" id="adminId">
               <input type="hidden" name="action" id="action">
               <!--Action -  1-unblock , 0- block -->
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
               <button type="submit" class="btn btn-danger reject-class deletetBtn">Block</button>
            </div>
         </form>
      </div>
   </div>
</div>
{{-- Delete Modal --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content delete-popup">
          <div class="modal-header">
             <h4 class="modal-title tx-danger delete-modal-title">Delete Employee</h4>
          </div>
          <form action="javascript:;" id="delete-form">
             <div class="modal-body">
                 Are you sure you want to Delete this Employee?
                <input type="hidden" name="empId" id="empId">
                <input type="hidden" name="action" id="action">
                <!--Action -  1-unblock , 0- block -->
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Cancel</button>
                <button type="submit" class="btn btn-danger reject-class delete-employee-button">Delete</button>
             </div>
          </form>
       </div>
    </div>
 </div>

@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
        const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Admin Management`);
    $("#admin").addClass("active");
    $(function () {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            autoWidth:false,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        // $(".dataTables_length select").select2({minimumResultsForSearch: Infinity,});
    });
</script>
<script>
    function blockFunction(id,action){
        $('#adminId').val(id);
        $('#action').val(action);
        if(action == 1 ){
            $('.reject-modal-title').text('Unblock Employee');
            $('.deletetBtn').text('Unblock');
        }
        $('#rejectModal').modal()
    }
    function deleteFunction(id){
        $('#empId').val(id);
        if(action == 1 ){
            $('.deletetBtn').text('Unblock');
        }
        $('#deleteModal').modal()
    }
</script>
<script>
    $('.deletetBtn').click(function(){
        var id = $('#adminId').val()
        var action = $('#action').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
                type: "GET",
                url: url+"/admin/block/"+id+'/'+action,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data){
                    if(action == 0 ){
                        $(".deletetBtn").text('Block');
                    }else{
                        $(".deletetBtn").text('Unblock');
                    }
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
    })

    $('tbody').on('click','.edit-button',function(){
        var adminid = $(this).data('admin')
        $.ajax({
            url : url+"/admin/view/"+adminid,
            type : "GET",
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            success : function(data){
                if(data.status === true){
                   console.log(data.response.state)
                    $('#myModalLabel').text('Edit Employee')
                    $('#id').val(data.response.id);
                    $('#first_name').val(data.response.first_name);
                    $('#last_name').val(data.response.last_name);
                    $('#country').val(data.response.country).trigger("change");
                    $('#state').val(data.response.state).trigger("change");
                    $('#city').val(data.response.city).trigger("change");
                    $('#zipcode').val(data.response.zip_code).trigger("change");
                    setTimeout(function(){
                        $('#city').val(data.response.city).trigger("change");
                        setTimeout(function(){
                            $('#zipcode').val(data.response.zip_code).trigger("change");
                        } ,500)
                    }, 200);
                    $('#dept').val(data.response.department).trigger("change");
                    $('#des').val(data.response.designation).trigger("change");
                    $('#address').val(data.response.address);
                    $('#email').val(data.response.email);
                    $('#phone').val(data.response.phone);
                    $('#countryCode').val(data.response.country_code).trigger("change");
                    $('#role').val(data.response.role).trigger("change");
                    $('#dept').val(data.response.department).trigger("change");
                    $('#des').val(data.response.designation).trigger("change");
                    $('#status').val(data.response.active).trigger("change");
                    $('#password').attr('placeholder','change password');
                    $('#addModal').modal()
                }else{
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })
    
    $('.delete-employee-button').click(function(){
        var id = $('#empId').val();
         $(".delete-employee-button").prop("disabled", true);
        $(".closeBtn").prop("disabled", true); 
        $.ajax({
            type: 'GET',
            url : url+"/admin/delete/"+id,
            processData : false,
            async : true,
            header : {
                "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                    $(".delete-employee-button").text('Processing..');
                },
                success: function (data){
                    $(".delete-employee-button").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
        })
        
        return false
    });


    $("#form-add").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            country: {
                required: true,
            },
            state: {
                required: true,
            },
            city: {
                required: true,
            },
            zipcode: {
                required: true,
            },
            address: {
                required: true,
            },
            email: {
                required: true,
            },
            phone: {
                required: true,
                maxlength:10,
            },
            password: {
                required: function() {
                    if($('#id').val()){
                        return false
                    }
                    return true
                }
            },
            department: {
                required: true,
            },
            designation: {
                required: true,
            },
            role: {
                required: true,
            },
            status: {
                required: true,
            },
        },
        submitHandler: function (form) {
            var form = document.getElementById("form-add");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addAdmin')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function () {
                    $(".addBtn").text('Processing..');
                },
                success: function (data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function (key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
<script>
    $('#country').change(function() {
        var countryId = $(this).val()
        //  alert(countryId);
        $.ajax({
            url: url + "/get-states/" + countryId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $('.state-select').html('')
                var html = '<option value="">Select State</option>';
                if (data.status === true) {
                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.name + `</option>`
                        })
                        $('.state-select').append(html);

                    } else {
                        $('.state-select').html('')
                    }
                } else {
                    $('.state-select').html('')
                }
            }
        });
        return false
    });
    $('.state-select').change(function() {
        var stateId = $(this).val()
        $.ajax({
            url: url + "/get-cities/" + stateId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $('.city-select').html('')
                var html = '<option value="">Select City</option>';
                if (data.status === true) {
                    if (data.response.length > 0) {
                        data.response.forEach((val, key) => {
                            html += `<option value=` + val.id + `>` + val.name + `</option>`
                        })
                        $('.city-select').append(html);

                    } else {
                        $('.city-select').html('')
                    }
                } else {
                    $('.city-select').html('')
                }
            }
        });
        return false
    });
$('.city-select').change(function() {
        var cityId = $(this).val()
        if (cityId != null) {
            $.ajax({
                url: url + "/get-pincodes/" + cityId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    //  $('.zipcode-select').html('')
                    var html = '<option value="">Select Zipcode</option>';
                    if (data.status === true) {
                        if (data.response.length > 0) {
                            data.response.forEach((val, key) => {
                                html += `<option value=` + val.id + `>` + val.pincode +
                                    `</option>`
                            })
                            $('.zipcode-select').append(html)
                        } else {
                            $('.zipcode-select').html('')
                        }
                    } else {
                        $('.zipcode-select').html('')
                    }
                }
            });
        }
        return false
    });
</script>
@endsection
