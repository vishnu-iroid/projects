@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }

    .modal { overflow: auto !important; }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    .card-warning {
        background: linear-gradient(60deg, #ffa726, #fb8c00);
        height : 20px;
        border-radius: 5px  !important
    }
    #map { height: 450px; width: 100%; }
    .errorTxt{
        background: linear-gradient(60deg, #ffcee8, #ff7a7a);
        color: #000000;
        padding : 10px;
        border-radius :10px;
    }
    .error{
        color : red;
    }

 </style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Property Management</span>
        <span class="breadcrumb-item active">vacate request</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Vacate Request</h4>
                            <p class="card-category">List of all Vacate requests</p>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <a href="{{ route('showVacateRequest',['status' => 0]) }}" class="nav-link tx-white d-flex align-items-center card-active">
                                <i class="fad fa-align-justify tx-20"></i>&nbspAll Requests
                            </a>
                            <a href="{{ route('showVacateRequest',['status' => 1]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-check-circle tx-20"></i>&nbspApproved
                            </a>
                            <a href="{{ route('showVacateRequest',['status' => 2]) }}" class="nav-link tx-white d-flex align-items-center">
                                <i class="fad fa-ban tx-20"></i>&nbspRejected
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-15p">Id</th>
                                <th class="wd-15p">Tenant Name</th>
                                <th class="wd-15p">Property Name</th>
                                <th class="wd-15p">Unit Name</th>
                                <th class="wd-15p">Due Amount</th>
                                <th class="wd-15p">Balance</th>
                                <th class="wd-15p">Vacating Date</th>
                                <th class="wd-15p">Status</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = ($vacate_request->currentpage()-1)* $vacate_request->perpage() + 1;?>
                        @foreach($vacate_request as $row)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $row->user_rel->name ?? '--' }}</td>

                                @if ($row->user_property_related->builder_id != null)
                                @php
                                 $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->user_property_related->builder_id)->first();
                                @endphp
                                <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                <td>{{ $row->user_property_related->property_name }}</td>
                                @else
                                    <td>{{ $row->user_property_related->property_name }}</td>
                                    <td>--</td>
                                @endif
                                <td>--</td>
                                <td>--</td>
                                <td>{{ $row->vacating_date ?? '' }}</td>
                                <td>
                                    @if( $row->status == 0 )
                                        <span class="badge rounded-pill bg-primary p-1 text-white"> Active </span>
                                    @elseif( $row->status == 1 )
                                        <span class="badge rounded-pill p-1 bg-success text-white"> Approved </span>
                                    @elseif( $row->status == 2 )
                                        <span class="badge rounded-pill p-1 bg-danger text-white"> Rejected </span>
                                    @endif
                                </td>
                                <td>
                                    <a class="fad fa-check mr-1 btn btn-primary mb-1 bt_cus approve-button" href="javascript:0;" data-id="{{$row->id}}" title="approve"></a>
                                    <a class="fad fa-times tx-30 reject-button btn btn-danger mb-1 bt_cus mr-1" href="javascript:0;" data-id="{{$row->id}}"  title="Reject"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                    {!! $vacate_request->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
@endsection
@section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
    const url = '{{url("/")}}';
    $("#site_title").html(`Property | Vacate Requests`);
    $("#vacate_request").addClass("active");
    $(document).ready(function(){
        //Approve function
        $('body').on('click','.approve-button',function(){
            let id = $(this).data('id');
            //alert(id);
            $.ajax({
                url : url+"/approve/request/"+id,
                type : "GET",
                processData : false,
                async : true,
                header : {
                    "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
                },
                success : function(data){
                    console.log(data);
                    if(data.status === true){
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 500);
                    }else{
                        toastr["error"](data.response);
                    }
                }
            })
        });
        //Reject Function
        $('body').on('click','.reject-button',function(){
            let id = $(this).data('id');
            //alert(id);
            $.ajax({
                url : url+"/reject/request/"+id,
                type : "GET",
                processData : false,
                async : true,
                header : {
                    "X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
                },
                success : function(data){
                    console.log(data);
                    if(data.status === true){
                        toastr["success"](data.response);
                        setTimeout(function () {
                            window.location.href = "";
                        }, 500);
                    }else{
                        toastr["error"](data.response);
                    }
                }
            })
        });
    });
</script>
@endsection
