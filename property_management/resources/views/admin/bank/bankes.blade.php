@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .modal { overflow: auto !important; }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Property Management</span>
        <span class="breadcrumb-item active">Banks Name</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
    <div class="row">
        <div class="col-md-12">

            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-12">
                            <h2 class="card-title" style="font-size: 1.125rem;font-weight: 300 !important;">Add Bank Names</h2>
                            <!-- <p class="card-category"></p> -->
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form class="form-service" id="form-add-service" action="javascript:;">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name<span class="tx-danger">*</span></label>
                                <input class="form-control" id="bankId" name="bankId" type="hidden" />
                                <input class="form-control" id="bank_name" name="bank_name" type="text" placeholder="Enter Bank name" />
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary addBtn">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Bank Names</h4>
                            <p class="card-category">List of all Bank</p>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-15p">Id</th>
                                <th class="wd-15p">Bank</th>
                                <th class="wd-15p">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($banks as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->bank_name }}</td>
                                <td>
                                    <a class="fad fa-edit tx-20 edit-button mr-1" href="javascript:0;" data-dataid="{{$row->id}}" data-url="/bank/view/" title="Edit"></a>
                                    <a class="fad fa-trash-alt tx-20 reject-button" href="javascript:0;" title="Delete" style="color: red " onclick="deleteFunction('{{$row->id}}')"></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $banks->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    </div>

    <!-- br-pagebody -->
    <!-- Reject Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content delete-popup">
                <div class="modal-header">
                    <h4 class="modal-title tx-danger">Delete Services</h4>
                </div>
                <form action="javascript:;" id="form-delete">
                    <div class="modal-body">
                        Are you sure you want to delete this Service?
                        <input type="hidden" name="serviceId" id="serviceId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary closeBtn" onclick="window.location.reload();">Cancel</button>
                        <button type="submit" class="btn btn-danger reject-class deletetBtn">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    @endsection @section('scripts')
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script>
        const url = '{{url("/")}}';
    </script>
    <script>
        $("#site_title").html(`Property | Bank Details`);
        $("#service_nav").addClass("active");
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
        $(function() {
            "use strict";
            $("#datatable2").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
    </script>
    <script>
    function deleteFunction(id){
            $('#serviceId').val(id);
            $('#deleteModal').modal()
        }
    </script>
    <script>
        $('.deletetBtn').on('click',function() {
            var id = $('#serviceId').val()
            $(".deletetBtn").prop("disabled", true);
            $(".closeBtn").prop("disabled", true);
            $.ajax({
                type: "GET",
                url: url + "/bank/delete/" + id,
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".deletetBtn").html('Deleting..');
                },
                success: function(data) {
                    $(".deletetBtn").text('Delete');
                    $(".deletetBtn").prop("disabled", false);
                    $(".closeBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        toastr["error"](data.response);
                    }
                },
            });
        })

        $('tbody').on('click','.edit-button',function() {
            var dataId = $(this).data('dataid')
            var toURL = $(this).data('url')
            $.ajax({
                url: url + toURL + dataId,
                type: "GET",
                processData: false,
                async: true,
                header: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                     console.log(url + toURL + dataId);
                    },
                success: function(data) {
                    if (data.status === true) {
                            $('#bankId').val(data.response.id)
                            $('#bank_name').val(data.response.bank_name)
                    } else {
                        toastr["error"](data.response);
                    }
                }
            })
            return false
        })

        $("#form-add-service").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                bank_name: {
                    required: true,
                },
                image: {
                    required: function() {
                        if ($('#serviceId').val() != "") {
                            return false
                        }
                        return true
                    }
                },
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-service");
                var formData = new FormData(form);
                $(".addServiceBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{route('banks.store')}}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });

        $("#form-add-provider").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            ignore: [],
            rules: {
                provider: {
                    required: true,
                },
                phone: {
                    required: true,
                    maxlength: 10,
                },
                company: {
                    required: function() {
                        if ($('#company_phone').val() != "") {
                            return true
                        }
                        return false
                    },
                },
                company_phone: {
                    required: function() {
                        if ($('#company').val() != "") {
                            return true
                        }
                        return false
                    },
                    maxlength: 10,
                },
                service_category: {
                    required: true
                },
                technician_name: {
                    required: true,
                }
            },
            submitHandler: function(form) {
                var form = document.getElementById("form-add-provider");
                var formData = new FormData(form);
                $(".addBtn").prop("disabled", true);
                $.ajax({
                    data: formData,
                    type: "post",
                    url: "{{route('addServiceProvider')}}",
                    processData: false,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    async: true,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    },
                    beforeSend: function() {
                        $(".addBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                    },
                    success: function(data) {
                        $(".addBtn").text('Submit');
                        $(".addBtn").prop("disabled", false);
                        if (data.status == 1) {
                            toastr["success"](data.response);
                            setTimeout(function() {
                                window.location.href = "";
                            }, 1000);
                        } else {
                            var html = "";
                            $.each(data.response, function(key, value) {
                                html += value + "</br>";
                            });
                            toastr["error"](html);
                        }
                    },
                });
            },
        });
    </script>
    @endsection
