<?php
header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title id="site_title"></title>

    <!-- vendor css -->
        <link href="{{asset('assets/lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/Ionicons/css/ionicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/highlightjs/github.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <link href="{{asset('assets/lib/select2/css/select2.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/lib/ckeditor/sample/css/sample.css')}}" rel="stylesheet">
    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bracket-new.css')}}">

    <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
    .br-mainpanel{
        margin:0;
    }
   </style>
  </head>

<body>

   

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel col-md-12 col-lg-12 col-sm-12 col-xs-12">
         <div class="br-pagetitle">
        <i class="icon fad fa-sun fa-lg tx-70 lh-0"></i>
        <div>
          <h4>@if($lang == 1) About Us @else نبذة عنا@endif</h4>
        </div>
      </div><!-- d-flex -->

        <div class="br-pagebody">
          @if($about)
            <div class="br-section-wrapper mg-t-10">
                <div class="form-layout form-layout-1">
                        <div class="col-lg-12 row">
                            <div class="form-group">
                                @if($about->image)
                                <img src="{{ asset($about->image) ?? '' }}" alt="image" height="100" width="100">
                                @endif
                            </div>
                        </div><!-- col-4 --> 
                        <div class="col-lg-12">
                            <div class="form-group">
                                {{-- <label class="form-control-label">About Us: <span class="tx-danger">*</span></label> --}}
                                <div>@if($about->description){!! $about->description !!} @endif</div>
                            </div>
                        </div><!-- col-4 -->
                        {{-- <div class="col-lg-12">
                            <div class="form-group">
                                <label class="form-control-label">About Us: <span class="tx-danger">*</span></label>
                                <textarea id="editor2" class="form-control  ckeditor" type="text" rows="5" name="arabic_description">@if($about->description){{ $about->description }} @endif</textarea>
                            </div>
                        </div><!-- col-4 --> --}}
                </div><!-- form-layout -->
              </div>
            @endif
        </div><!-- br-pagebody -->
        <footer class="br-footer">
           
        </footer>
    </div><!-- br-mainpanel -->

    <!-- ########## END: MAIN PANEL ########## -->

    <script src="{{asset('assets/lib/jquery/jquery.js')}}"></script>
    <script src="{{asset('assets/lib/popper.js/popper.js')}}"></script>
    <script src="{{asset('assets/js/datepicker.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/lib/moment/moment.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-ui/jquery-ui.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-switchbutton/jquery.switchButton.js')}}"></script>
    <script src="{{asset('assets/lib/peity/jquery.peity.js')}}"></script>
    <script src="{{asset('assets/lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/bracket.js')}}"></script>
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/lib/select2/js/select2.min.js')}}"></script>
    <!--toastr Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
</body>

</html>
