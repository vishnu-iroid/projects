   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
       .card {
           position: relative;
           display: flex;
           flex-direction: column;
           min-width: 0;
           word-wrap: break-word;
           background-color: #fff;
           background-clip: border-box;
           border: 1px solid #eee;
           border-radius: .25rem;
       }


       .card-header-b {
           height: 300px !important;
       }

       #map {
           height: 450px;
           width: 100%;
       }

   </style>
   @section('content')
   <div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <span class="breadcrumb-item active">Events</span>
    </nav>
</div>
   <div class="br-pagetitle">
    
       <div>
           <h4>Add Events</h4>
       </div>
   </div><!-- d-flex -->
   <div class="br-pagebody">
       <form action="{{ route('addEvents') }}" method="post" enctype="multipart/form-data">
           <div class="br-section-wrapper mg-t-10">
               @csrf
               <div class="form-layout form-layout-1 row">
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Event Name: <span class="tx-danger">*</span></label>
                           <input type="text" class="form-control" name="name" placeholder="Enter Events Name">
                       </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Event Date: <span class="tx-danger">*</span></label>
                           <input type="date" class="form-control" name="event_date">
                       </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-4">
                       {{-- <div class="form-group">
                           <label class="form-control-label">Admin: <span class="tx-danger">*</span></label>
                           <select class="form-control select2 city-select" name="admin_id" id="admin_id">
                               @foreach($admins as $ad)
                               <option value="{{ $ad->id }}">{{ $ad->first_name }}</option>
                               @endforeach
                           </select>
                       </div> --}}
                       <input type="hidden" name="admin_id" id="admin_id"  value="{{ $logged_admin_id }}">
                   </div><!-- col-4 -->

               </div><!-- form-layout -->
               <div class="form-layout form-layout-1 row">
                   <div class="col-lg-8">
                       <div id="map"></div>
                       <div>
                           <input type="hidden" id="lat" name="latitude">
                           <input type="hidden" id="lng" name="longitude">
                       </div>
                   </div>
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Image: <span class="tx-danger">*</span></label>
                           <input type="file" class="form-control" name="image">
                       </div>
                   </div><!-- col-4 -->

               </div><!-- form-layout -->
           </div>
           <div class="br-section-wrapper mg-t-2">
               <div class="form-layout form-layout-1 row">
                   <div class="col-lg-6">
                       <div class="form-group">
                           <label class="form-control-label">Enter short description: <span class="tx-danger">*</span></label>
                           <textarea class="form-control ckeditor" type="text" rows="5" name="short_description"></textarea>
                       </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-6">
                       <div class="form-group">
                           <label class="form-control-label">Enter description: <span class="tx-danger">*</span></label>
                           <textarea class="form-control ckeditor" type="text" rows="5" name="description"></textarea>
                       </div>
                   </div><!-- col-4 -->
               </div><!-- form-layout -->
           </div>
           <h3 class="mt-5">Add Packages</h3>
           <td><button type="button" class=" btn btn-primary" style="float:right" id="add" name="add">Add Package</button></td>
           <table class="table table-bordered table-hover col-md-12 col-lg-12 col-sm-12" id="table-fields" style="width: 100%">
               <tbody class="flex-column">

               </tbody>
           </table>
           <div class="form-layout-footer mt-5">
               <button class="btn btn-info">Submit</button>
           </div><!-- form-layout-footer -->
   </div>
   </form>
   </div><!-- br-pagebody -->
   @endsection
   @section('scripts')
   <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
   <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>

   <script>
       function initMap() {
           const uluru = {
               lat: 24.466667
               , lng: 54.366669
           };
           if ($('#lat').val() != "" && $('#lng').val() != "") {
               const uluru = {
                   lat: $('#lat').val()
                   , lng: $('#lng').val()
               };
           }
           const map = new google.maps.Map(document.getElementById("map"), {
               zoom: 10
               , center: uluru
           , });
           const marker = new google.maps.Marker({
               position: uluru
               , map: map
               , draggable: true
           });
           navigator.geolocation.getCurrentPosition(
               function(position) { // success cb
                   if ($('#lat').val() == "" && $('#lng').val() == "") {
                       $('#lat').val(parseFloat(position.coords.latitude))
                       $('#lng').val(parseFloat(position.coords.longitude))
                   }
                   var lat = $('#lat').val()
                   var lng = $('#lng').val()
                   map.setCenter(new google.maps.LatLng(lat, lng));
                   var latlng = new google.maps.LatLng(lat, lng);
                   marker.setPosition(latlng);
               }
               , function() {}
           );
           google.maps.event.addListener(marker, 'dragend', function() {
               $('#lat').val(marker.position.lat())
               $('#lng').val(marker.position.lng())
           })
       }

   </script>
   <script>
       $(document).ready(function() {
           var i = 1;
           $('#add').click(function() {

               var html = '<div class="col-md-8 col-lg-8 col-sm-8 mb-2" id="row' + i + '">'
               html += '<div class="form-group"><input type="text" class="form-control" id="package_name" name="package_name[]" placeholder="Enter Package Name"></div>'
               html += '<div class="form-group"><input type="text" class="form-control" id="price" name="price[]" placeholder="Enter Package Price"></div>'
               html += '<div class="form-group"><input type="file" class="form-control" id="package_image" name="package_image[]"></div>'
               html += '<div class="clo" id="f_clo' + i + '">'
               html += '<input type="text" class="form-control mb-1" id="feature_name" name="feature_name[]" placeholder="Enter Feature Name">'
               html += '<input type="button" class="btn-xs btn-primary mb-1 add_feature" style="float:right" id="' + i + '"  name="add_feature" value="+">'
               html += '</div>'
               html += '<div class="form-group mt-1"><button type="button" class="btn btn-danger remove" id="' + i + '" name="remove">Remove Package</button></div>'
               html += '</div>'
               $('#table-fields').append(html);
               i++;

           });
           // removing parent clone
           $(document).on('click', '.remove', function() {
               var button_id = $(this).attr('id');
               $("#row" + button_id + "").remove();
           });
           var fclone = '<div class="clo" id="f_clo' + i + '">'
           fclone += '<input type="text" class="form-control" id="feature_name" name="feature_name[]" placeholder="Enter Feature Name">'
           fclone += '<input type="button" style="float:right" class="btn-xs btn-danger mt-1 remove_feature" id="' + i + '" name="remove_feature" value="-">'
           fclone += '</div>'

           //  adding feature row
           $(document).on('click', '.add_feature', function() {
               var cl_id = $(this).attr('id');
               $("#f_clo" + cl_id + "").append(fclone);
           });
           // removing child clone
           $(document).on('click', '.remove_feature', function() {
               $(this).closest('.clo').remove()
           });


       });

   </script>

   @endsection
