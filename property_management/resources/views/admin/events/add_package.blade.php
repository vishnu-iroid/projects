   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
       .card {
           position: relative;
           display: flex;
           flex-direction: column;
           min-width: 0;
           word-wrap: break-word;
           background-color: #fff;
           background-clip: border-box;
           border: 1px solid #eee;
           border-radius: .25rem;
       }

       .card-header-b {
           height: 300px !important;
       }

       #map {
           height: 450px;
           width: 100%;
       }

   </style>
   @section('content')
   <div class="br-pagetitle">
       <i class="icon fad fa-sun fa-lg tx-70 lh-0"></i>
       <div>
           <h4>Add Events</h4>
       </div>
   </div><!-- d-flex -->
   <div class="br-pagebody">
       <form action="{{ route('addEvents') }}" method="post" enctype="multipart/form-data">
           <div class="br-section-wrapper mg-t-10">
               @csrf
               <div class="form-layout form-layout-1 row">
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Event Name: <span class="tx-danger">*</span></label>
                           <input type="text" class="form-control" name="name" placeholder="Enter Events Name">
                       </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Event Date: <span class="tx-danger">*</span></label>
                           <input type="date" class="form-control" name="event_date">
                       </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Admin: <span class="tx-danger">*</span></label>
                           <select class="form-control select2 city-select" name="admin_id" id="admin_id">
                               <option value=""></option>
                               @foreach($admins as $ad)
                               <option value="{{ $ad->id }}">{{ $ad->id }}</option>
                               @endforeach
                           </select>
                       </div>
                   </div><!-- col-4 -->

               </div><!-- form-layout -->
               <div class="form-layout form-layout-1 row">
                   <div class="col-lg-8">
                       <div id="map"></div>
                       <div>
                           <input type="hidden" id="lat" name="latitude">
                           <input type="hidden" id="lng" name="longitude">
                       </div>
                   </div>
                   <div class="col-lg-4">
                       <div class="form-group">
                           <label class="form-control-label">Image: <span class="tx-danger">*</span></label>
                           <input type="file" class="form-control" name="image">
                       </div>
                   </div><!-- col-4 -->

               </div><!-- form-layout -->
           </div>
           <div class="br-section-wrapper mg-t-2">
               <div class="form-layout form-layout-1 row">
                   <div class="col-lg-6">
                       <div class="form-group">
                           <label class="form-control-label">Enter short description: <span class="tx-danger">*</span></label>
                           <textarea class="form-control ckeditor" type="text" rows="5" name="short_description"></textarea>
                       </div>
                   </div><!-- col-4 -->
                   <div class="col-lg-6">
                       <div class="form-group">
                           <label class="form-control-label">Enter description: <span class="tx-danger">*</span></label>
                           <textarea class="form-control ckeditor" type="text" rows="5" name="description"></textarea>
                       </div>
                   </div><!-- col-4 -->
               </div><!-- form-layout -->
           </div>
           <h3 class="mt-5">Add Packages</h3>
           <div class="br-section-wrapper mg-t-2">
               <table>
                   <tr>
                       <td>
                           <div class="form-layout form-layout-1 row" id="clone">
                               <div class="col-lg-3">
                                   <div class="form-group">
                                       <label class="form-control-label">Package Name: <span class="tx-danger">*</span></label>
                                       <input type="text" class="form-control" id="package_name" name="package_name[]" placeholder="Enter Events Name">
                                   </div>
                               </div><!-- col-4 -->
                               <div class="col-lg-3">
                                   <div class="form-group">
                                       <label class="form-control-label">Package Price: <span class="tx-danger">*</span></label>
                                       <input type="text" class="form-control" id="price" name="price[]" placeholder="Enter Package Price">
                                   </div>
                               </div><!-- col-4 -->
                               <div class="col-lg-3">
                                   <div class="form-group">
                                       <label class="form-control-label">Package description: <span class="tx-danger">*</span></label>
                                       <textarea class="form-control" type="text" rows="5" id="package_description[]" name="package_description" placeholder="Enter Package Description"></textarea>
                                   </div>
                               </div><!-- col-4 -->
                               <div class="col-lg-3">
                                   <div class="form-group">
                                       <label class="form-control-label">Package Image: <span class="tx-danger">*</span></label>
                                       <input type="file" name="package_image[]" id="package_image">
                                   </div>
                               </div><!-- col-4 -->
                               <div class="col-lg-12 row" id="clone-div">
                                   <div class="form-group col-lg-5 col-md-5 col-sm-5">
                                       <label class="form-control-label">Feature Name: <span class="tx-danger">*</span></label>
                                       <input type="text" class="form-control" name="feature_name[]" id="feature_name" placeholder="Enter Feature name">
                                   </div>
                                   <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                       <label class="form-control-label">Feature Description: <span class="tx-danger">*</span></label>
                                       <textarea name="feature_description[]" class="form-control" id="feature_description" rows="5" placeholder="Enter Feature Description"></textarea>
                                   </div>
                                   <div class="form-group col-lg-1 col-md-1 col-sm-1">
                                       <input type="button" id="clone" class="btn btn-success" value="+" onclick="addItems();" />
                                   </div>
                               </div><!-- col-4 -->
                           </div><!-- form-layout -->
           </div>
           </td>
           </tr>
           </table>
           <div class="form-layout-footer mt-5">
               <button class="btn btn-info">Submit</button>
           </div><!-- form-layout-footer -->
   </div>
   </form>
   </div><!-- br-pagebody -->
   @endsection
   @section('scripts')
   <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
   <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
   <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
   <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>

   <script>
       function initMap() {
           const uluru = {
               lat: 24.466667
               , lng: 54.366669
           };
           if ($('#lat').val() != "" && $('#lng').val() != "") {
               const uluru = {
                   lat: $('#lat').val()
                   , lng: $('#lng').val()
               };
           }
           const map = new google.maps.Map(document.getElementById("map"), {
               zoom: 10
               , center: uluru
           , });
           const marker = new google.maps.Marker({
               position: uluru
               , map: map
               , draggable: true
           });
           navigator.geolocation.getCurrentPosition(
               function(position) { // success cb
                   if ($('#lat').val() == "" && $('#lng').val() == "") {
                       $('#lat').val(parseFloat(position.coords.latitude))
                       $('#lng').val(parseFloat(position.coords.longitude))
                   }
                   var lat = $('#lat').val()
                   var lng = $('#lng').val()
                   map.setCenter(new google.maps.LatLng(lat, lng));
                   var latlng = new google.maps.LatLng(lat, lng);
                   marker.setPosition(latlng);
               }
               , function() {}
           );
           google.maps.event.addListener(marker, 'dragend', function() {
               $('#lat').val(marker.position.lat())
               $('#lng').val(marker.position.lng())
           })
       }

   </script>
   <script>
       var items = 0;

       function addItems() {
           items++;
           var html = '<div class="form-group col-lg-5 col-md-5 col-sm-5" id="fea">'
           html += '<label class="form-control-label">Feature Name: <span class="tx-danger">*</span></label>'
           html += '<input type="text"class="form-control" name="feature_name[]" id="feature_name"  placeholder="Enter Feature name"></div>'
           html += '<div class="form-group col-lg-6 col-md-6 col-sm-6"><label class="form-control-label">Feature Description: <span class="tx-danger">*</span></label>'
           html += '<textarea name="feature_description[]" class="form-control" id="feature_description"  rows="5"  placeholder="Enter Feature Description"></textarea></div>'
           html += '<div class="form-group col-lg-1 col-md-1 col-sm-1"><input type="button" id="remove_clone" class="btn btn-danger" value="-"/>'
           html += '</div>'
           $("#clone-div").append(html);
       }
       $(document).ready(function() {
           $('#feature-clone').on('click', '#remove_clone', function() {
               $("#fea").remove();
           })
       })

   </script>
   {{-- <script>
       $(document).ready(function() {
           var i = 1;
           $('#add').click(function() {
               i++;
               var html = '<tr id="row' + i + '">'
               html += '<td><input type="text" class="form-control" id="package_name" name="package_name[]" placeholder="Enter Package Name"></td>'
               html += '<td><input type="text" class="form-control" id="price" name="price[]" placeholder="Enter Package Price"></td>'
               html += '<td><textarea name="package_description[]" id="package_description" class="form-control" cols="30" rows="10" placeholder="Enter Description"></textarea></td>'
               html += '<td><input type="file" class="form-control" id="package_image" name="package_image[]"></td>'
               html += '<td class="clo" id="f_clo' + i + '">'
               html += '<input type="text" class="form-control mb-1" id="feature_name" name="feature_name[]" placeholder="Enter Feature Name">'
               html += '<span><textarea name="feature_description[]" id="feature_description" class="form-control mb-1" cols="30" rows="10" placeholder="Enter Description"></textarea></span>'
               html += '<input type="button" class="btn-xs btn-primary mb-1 add_feature" style="float:right" id="' + i + '"  name="add_feature" value="+">'
               html += '</td>'
               html += '<td><button type="button" class="btn btn-danger remove" id="' + i + '" name="remove">-</button></td>'
               html += '</tr>'
               $('#table-fields').append(html);
           });
           // removing parent clone
           $(document).on('click', '.remove', function() {
               var button_id = $(this).attr('id');
               $("#row" + button_id + "").remove();
           });
           var fclone = '<input type="button" class="btn-xs btn-danger mb-1 remove_feature"  id="' + i + '"  name="remove_feature" value="-">'
           fclone += '<input type="text" class="form-control" id="feature_name" name="feature_name[]" placeholder="Enter Feature Name">'
           fclone += '<span><textarea name="feature_description[]" id="feature_description" class="form-control" cols="30" rows="10" placeholder="Enter Description"></textarea></span>'
           //  adding feature row
           $(document).on('click', '.add_feature', function() {
               var cl_id = $(this).attr('id');
               $("#f_clo" + cl_id + "").append(fclone);
           });
           // removing child clone
           // $(document).on('click','.remove_feature',function(){
           //     var s_id = $(this).attr('id');
           //     $("#f_clo"+s_id+"").closest('td').remove();
           // });

       });

   </script> --}}

   @endsection
