   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }
    .select2-container {
      z-index:10050;
      width: 100% !important;
      padding: 0;
    }
    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }
    #map { height: 450px; width: 100%; }
   </style>

 </style>
   @section('content')
   <div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
        <span class="breadcrumb-item active">Events</span>
    </nav>
</div>
      <div class="br-pagetitle">

      
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <div class="">
             <div class="card shadow">  
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Events</h4>
                            <p class="card-category">List of all Events</p>
                        </div>
                        <div class="pd-5 mg-r-20">
                            <a href="{{ url('add-events-view') }}" class="nav-link card-active tx-white d-flex align-items-center" ><i class="fad fa-plus-hexagon tx-20"></i>&nbspAdd Event</a>
                        </div>
                    </div>
                </div>
             
          
          <div class="card-body">
            <div id="datatable2" class="bd bd-gray-300  table-responsive">
              <table class="table table-striped mg-b-0">
                <thead>
                  <tr>
                    <th width="5%">No.</th>
                    <th width="20%">Name</th>
                    <th width="20%">Image</th>
                    <th width="20%">Short Description</th>
                    <th width="20%">Description</th>
                    <th width="15%">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @php $i =1; @endphp
                    @foreach($events as $row)
                    <tr>
                      <th scope="row">{{ $i }} @php $i++; @endphp</th>
                      <td>{{ $row->name }}</td>
                      <td><img src="{{ isset($row->event_priority_image->image)?$row->event_priority_image->image:''  }}" alt="image" height="100" width="100"></td>
                      <td>{{ $row->short_description }}</td>
                      <td>{{ substr($row->description,0,250) }}</td>
                      <td>
                          <a href="{{ url('edit-event/'.$row->id) }}" class="btn btn-primary btn-icon"><div><i class="fa fa-edit"></i></div></a>
                          <span><a href="{{ url('delete-event/'.$row->id) }}" class="btn btn-danger btn-icon"><div><i class="fa fa-trash"></i></div></a></span>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          </div>
        </div>

      </div>


    @endsection
    @section('scripts')
    <script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>


    @endsection
