@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Facility Management</span>
        <span class="breadcrumb-item active">Services Provider</span>
    </nav>
</div>
<!-- br-pageheader -->
<div class="br-pagebody">
    <!--  -->
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Sevice Providers</h4>
                        <p class="card-category">List of all service provider</p>
                    </div>
                    <div class="pd-5 mg-r-20">
                        <a href="#" class="nav-link card-active tx-white d-flex align-items-center" data-toggle="modal" data-target="#addModalProvider"><i class="fad fa-plus-hexagon tx-20"></i>&nbspService Provider</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive nowrap table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="wd-15p">Id</th>
                            <th class="wd-15p">Company Name</th>
                            <th class="wd-15p">Technician Name</th>
                            <th class="wd-15p">Phone</th>
                            <th class="wd-15p">Service</th>
                            <th class="wd-15p">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($service_providers as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->company}}</td>
                            <td>{{$row->technician_name}}</td>
                            <td>{{$row->technician_phone}}</td>
                            <td>@if($row->service_rel){{$row->service_rel->service}}@endif</td>
                            <td>
                                <a class="fad fa-edit tx-20 edit-button btn btn-primary bt_cus" href="javascript:0;" data-dataid="{{$row->id}}" data-url="/service-provider/view/" title="Edit"></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">
                    {!! $service_providers->links() !!}
                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->

<!-- service provider  Modal -->
<div class="modal fade" id="addModalProvider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow: hidden">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center tx-primary" id="myModalLabelProvider">Add Service Provider</h4>
            </div>
            <form action="javascript:;" id="form-add-provider">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Name <span class="tx-danger">*</span></label>
                        <input type="hidden" id="providerId" name="providerId">
                        <input class="form-control" id="provider" name="provider" type="text" placeholder="Enter name" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Technician Name <span class="tx-danger">*</span></label>
                        <input class="form-control" id="technician_name" name="technician_name" type="text" placeholder="Enter Technician Name" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Technician Phone <span class="tx-danger">*</span></label>
                        <input class="form-control" id="phone" name="phone" type="text" placeholder="Enter phone number" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company <span class="tx-danger"></span></label>
                        <input class="form-control" id="company" name="company" type="text" placeholder="Enter company" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company Phone<span class="tx-danger"></span></label>
                        <input class="form-control" id="company_phone" name="company_phone" type="text" placeholder="Enter company phone" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service <span class="tx-danger">*</span></label>
                        <select class="form-control select2" name="service_category" id="service_category">
                            <option value=""></option>
                            @foreach($services_list as $row5)
                            <option value="{{$row5->id}}">{{$row5->service}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-purple tx-white closeBtn" onclick="window.location.reload();">Close</button>
                    <button type="submit" class="btn btn-primary addBtn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script>
    const url = '{{url("/")}}';
</script>
<script>
    $("#site_title").html(`Property | Services`);
    $("#service_nav").addClass("active");
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
    $(function() {
        "use strict";
        $("#datatable2").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            autoWidth:false,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>
<script>
    $('.deletetBtn').on('click', function() {
        var id = $('#departmentId').val()
        $(".deletetBtn").prop("disabled", true);
        $(".closeBtn").prop("disabled", true);
        $.ajax({
            type: "GET",
            url: url + "/department/delete/" + id,
            processData: false,
            async: true,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function() {
                $(".deletetBtn").html('Deleting..');
            },
            success: function(data) {
                $(".deletetBtn").text('Delete');
                $(".deletetBtn").prop("disabled", false);
                $(".closeBtn").prop("disabled", false);
                if (data.status == 1) {
                    toastr["success"](data.response);
                    setTimeout(function() {
                        window.location.href = "";
                    }, 1000);
                } else {
                    toastr["error"](data.response);
                }
            },
        });
    })

    $('tbody').on('click', '.edit-button', function() {
        var dataId = $(this).data('dataid')
        var toURL = $(this).data('url')
        $.ajax({
            url: url + toURL + dataId,
            type: "GET",
            processData: false,
            async: true,
            header: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                if (data.status === true) {
                    if (data.type == "service") {
                        // $('#myModalLabelProvider').text('Edit Service Provi')
                        $('#serviceId').val(data.response.id)
                        $('#service_name').val(data.response.service)
                        $('#current_image').val(data.response.image)
                        $('.image-class').css('display', 'block')
                        $('.image-display').prop('src', url + data.response.image)
                    } else {
                        $('#myModalLabelProvider').html('Edit Service Provider')
                        $('#providerId').val(data.response.id)
                        $('#provider').val(data.response.name)
                        $('#technician_name').val(data.response.technician_name)
                        $('#phone').val(data.response.technician_phone)
                        $('#company').val(data.response.company)
                        $('#company_phone').val(data.response.company_phone)
                        $('#service_category').val(data.response.service).trigger('change')
                        $('#addModalProvider').modal()
                    }
                } else {
                    toastr["error"](data.response);
                }
            }
        })
        return false
    })

    $("#form-add-service").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            service_name: {
                required: true,
            },
            image: {
                required: function() {
                    if ($('#serviceId').val() != "") {
                        return false
                    }
                    return true
                }
            },
        },
        submitHandler: function(form) {
            var form = document.getElementById("form-add-service");
            var formData = new FormData(form);
            $(".addServiceBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addService')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addServiceBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                },
                success: function(data) {
                    $(".addServiceBtn").text('Submit');
                    $(".addServiceBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });

    $("#form-add-provider").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        ignore: [],
        rules: {
            provider: {
                required: true,
            },
            phone: {
                required: true,
                maxlength: 10,
            },
            company: {
                required: function() {
                    if ($('#company_phone').val() != "") {
                        return true
                    }
                    return false
                },
            },
            company_phone: {
                required: function() {
                    if ($('#company').val() != "") {
                        return true
                    }
                    return false
                },
                maxlength: 10,
            },
            service_category: {
                required: true
            },
            technician_name: {
                required: true,
            }
        },
        submitHandler: function(form) {
            var form = document.getElementById("form-add-provider");
            var formData = new FormData(form);
            $(".addBtn").prop("disabled", true);
            $.ajax({
                data: formData,
                type: "post",
                url: "{{route('addServiceProvider')}}",
                processData: false,
                dataType: "json",
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                async: true,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
                beforeSend: function() {
                    $(".addBtn").html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
                },
                success: function(data) {
                    $(".addBtn").text('Submit');
                    $(".addBtn").prop("disabled", false);
                    if (data.status == 1) {
                        toastr["success"](data.response);
                        setTimeout(function() {
                            window.location.href = "";
                        }, 1000);
                    } else {
                        var html = "";
                        $.each(data.response, function(key, value) {
                            html += value + "</br>";
                        });
                        toastr["error"](html);
                    }
                },
            });
        },
    });
</script>
@endsection
