@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 100px;
        width: 100px;
    }

    #map-2 {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
            <span class="breadcrumb-item active">Property Reports</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property Report</h4>

                        </div>

                    </div>
                </div>
                @php
                    $i = 0;
                @endphp
                <div class="card-body">
                    {{-- <a href="{{route('proprtyReportPdf')}}" class="btn btn-primary" target="_blank">print</a> --}}
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Id</th>
                            <th>Property Name</th>
                            {{-- <th>Unit Name</th> --}}
                            <th>Type</th>
                            <th>Category</th>
                            <th>Lease Type</th>
                            <th>Location</th>
                            <th>View More</th>

                        </thead>
                        <tbody>
                            @foreach ($proprtyReport as $property)
                            <tr>
                                <td>{{$property->id}}</td>
                                    {{-- @if ($property->builder_id != null)
                                            @php
                                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $property->builder_id)->first();
                                            @endphp
                                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                        <td>{{ $property->property_name }}</td>
                                    @else
                                        <td>{{ $property->property_name }}</td>
                                        <td>--</td>
                                    @endif --}}
                                    <td>{{ $property->property_name }}</td>
                                    @if($property->furnished== '0')
                                    <td>Not Furnished</td>
                                    @elseif($property->furnished== '1')
                                    <td>Semi Furnished</td>
                                    @elseif($property->furnished== '2')
                                    <td>Fully Furnished</td>
                                    @else
                                    <td>--</td>
                                    @endif
                                    @if ($property->category=='0')
                                    @if($property->property_to=='0')
                                    <td><span class="badge badge-danger mb-1">Residential</span> <span class="badge badge-warning text-white">Rent</span></td>
                                    @else
                                    <td><span class="badge badge-danger">Residential</span><span class="badge badge-warning"> Buy</span></td>
                                    @endif

                                       @elseif ($property->category=='1')
                                       @if($property->property_to=='0')
                                       <td><span class="badge badge-danger mb-1">Commercial</span><span class="badge badge-warning text-white">Rent</span></td>
                                       @else
                                       <td><span class="badge badge-danger">Commercial</span><span class="badge badge-warning">|Buy</span></td>
                                       @endif

                                       @else
                                       <td>---</td>
                                    @endif
                                    {{-- @if($property->occupied=='0')
                                    <td><span class="badge badge-success">Vacant</span></td>
                                    @else
                                    <td><span class="badge badge-info">Occupied</span></td>
                                    @endif --}}
                                    <td>{{isset($property->frequency_rel->type)?$property->frequency_rel->type:"--"}}</td>
                                    {{-- <td>{{$property->city_rel->name}}|{{$property->state_rel->name}}</td> --}}
                                  
                                    <td>
                                        <a class="fad fa-map-marker tx-20 btn btn-primary mb-1 bt_cus mr-1" target="_blank" href="https://maps.google.com/?q={{$property->latitude}},{{$property->longitude}}"></a>
                                    </td>
                                    <td><a class="fa fa-eye" href="{{route('proprtyReportPdf',['id'=>$property->id])}}" target="_blank"></a></td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
                <div class="d-flex justify-content-end">
                    {{$proprtyReport->links()}}
                </div>
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection 
    @section('scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>

<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQljK3pY_D-t5TRilbmT_JofMVlRReLqU&callback=initMap&libraries=&v=weekly"
async></script>
    <script>
        function initMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            if ($('#lat').val() != "" && $('#lng').val() != "") {
                const uluru = {
                    lat: $('#lat').val(),
                    lng: $('#lng').val()
                };
            }

            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            // navigator.geolocation.getCurrentPosition(
            //     function(position) { // success cb
            //         if ($('#lat').val() == "" && $('#lng').val() == "") {
            //             $('#lat').val(parseFloat(position.coords.latitude))
            //             $('#lng').val(parseFloat(position.coords.longitude))
            //         }
            //         var lat = $('#lat').val()
            //         var lng = $('#lng').val()
            //         map.setCenter(new google.maps.LatLng(lat, lng));
            //         var latlng = new google.maps.LatLng(lat, lng);
            //         marker.setPosition(latlng);
            //     },
            //     function() {}
            // );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat').val(marker.position.lat())
                $('#lng').val(marker.position.lng())
            })
        }

        function initPropertyMap() {
            const uluru = {
                lat: 24.466667,
                lng: 54.366669
            };
            console.log('lat', $('#lat-prop').val())
            if ($('#lat-prop').val() != "" && $('#lng-prop').val() != "") {
                const uluru = {
                    lat: $('#lat-prop').val(),
                    lng: $('#lng-prop').val()
                };
            }
            const map = new google.maps.Map(document.getElementById("map-2"), {
                zoom: 10,
                center: uluru,
            });
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
                draggable: true
            });
            navigator.geolocation.getCurrentPosition(
                function(position) { // success cb
                    if ($('#lat-prop').val() == "" && $('#lng-prop').val() == "") {
                        $('#lat-prop').val(parseFloat(position.coords.latitude))
                        $('#lng-prop').val(parseFloat(position.coords.longitude))
                    }
                    var lat = $('#lat-prop').val()
                    var lng = $('#lng-prop').val()
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var latlng = new google.maps.LatLng(lat, lng);
                    marker.setPosition(latlng);
                },
                function() {}
            );
            google.maps.event.addListener(marker, 'dragend', function() {
                $('#lat-prop').val(marker.position.lat())
                $('#lng-prop').val(marker.position.lng())
            })
        }
    </script>
@endsection
