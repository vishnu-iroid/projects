@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
            <span class="breadcrumb-item active">Property Vacant Report</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property Vacant Report</h4>

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    {{-- <a href="{{route('vacateReportPdf')}}" class="btn btn-primary" target="_blank">print</a> --}}
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Id</th>
                            <th>Property Name</th>
                            <th>Contract Start Date</th>
                            <th>Contract End Date</th>
                            <th>Token Amount</th>
                            <th>Selling price</th>
                            <th>Security Deposit</th>
                            <th>Rent</th>
                            <th>Commission</th>
                            <th>Management Fee</th>
                            <th>Ads Number</th>
                            <th>Guard Number</th>
                          

                        </thead>
                        <tbody>
                          
                            <tr>
                                <td>{{$property->id}}</td>
                                <td>{{$property->property_name}}</td>
                                <td>{{$property->contract_start_date}}</td>
                                <td>{{$property->contract_end_date}}</td>
                                <td>{{$property->token_amount}}</td>
                                <td>{{$property->selling_price}}</td>
                                <td>{{$property->security_deposit}}</td>
                                <td>{{$property->rent}}</td>
                                @if($property->commission_in=='amount')
                                <td>{{$property->commission}}</td>
                                @else
                                @php
                                    $commission=($property->rent * $property->commission)/100;
                                @endphp
                                <td>{{$commission}}</td>
                                @endif
                                @if($property->property_management_fee_type=='amount')
                                <td>{{$property->management_fee}}</td>
                                @else
                                @php
                                    $mangementfee=($property->rent * $property->management_fee)/100;
                                @endphp
                                <td>{{ $mangementfee}}</td>
                                @endif
                              
                                <td>{{$property->ads_number}}</td>
                                <td>{{$property->guards_number}}</td>
    
                            </tr>
                           

                        </tbody>

                    </table>
                </div>
                
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
@endsection
