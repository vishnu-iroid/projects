@extends('admin.layout.app')
@section('content')
    <style>
        .br-section-wrapper-dept {
            background-color: #fff;
            padding: 10px 10px;
            box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
            border-radius: 6px;
        }

        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid #eee;
            border-radius: .25rem;
        }

        .h4,
        h4 {
            font-size: 1.125rem;
            font-weight: 300 !important;
        }

        .card-warning {
            background: linear-gradient(60deg, #ffa726, #fb8c00) !important;
            height: 20px !important;
            border-radius: 5px !important;
        }

        .card-warning-2 {
            background: linear-gradient(60deg, #ffa726, #fb8c00) !important;
            height: 20px !important;
            border-radius: 5px !important;
        }

    </style>
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            {{-- <span class="breadcrumb-item active">Accounts Management</span> --}}
            <span class="breadcrumb-item active">Report</span>
            <span class="breadcrumb-item active">Property mangement Fee Report</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <div class="br-pagebody">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header card-header-blueheader">
                        <div class="row d-flex justify-content-between">
                            <div class="col-md-4">
                                <h4 class="card-title">Property mangement Fee</h4>
                                <p class="card-category">List of all Property mangement </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table data-table " style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>OWNER NAME </th>
                                    <th>PROPERTY NAME</th>
                                    <th>UNIT NO.</th>
                                    <th>Management Fee</th>
                                    <th>COLLECTED</th>
                                    <th>BALACNE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    </div>
    </div>
    <!-- br-pagebody -->
@endsection
@section('scripts')
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>

    <script>
        $(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                "scrollX": true,
                dom: 'Bfrtip',

                ajax: {
                    url: "{{ route('showCommissionReport') }}",

                },
                columns: [{
                        data: 'property_id',
                        name: 'property_id'
                    },
                    {
                        data: 'owner',
                        name: 'owner'
                    },
                    {
                        data: 'property',
                        name: 'property'
                    },
                    {
                        data: 'unit',
                        name: 'unit'
                    },
                    {
                        data: 'management_fee',
                        name: 'management_fee'
                    },
                    {
                        data:'collected',
                        name:'collected'
                    },
                    {
                        data:null,
                        render:function(data,type,row)
                        {
                            return data.management_fee - data.collected ;
                        }
                    }
                ]
            });
            $('.filter').change(function() {
                table.draw();
            });

        });
    </script>
@endsection
