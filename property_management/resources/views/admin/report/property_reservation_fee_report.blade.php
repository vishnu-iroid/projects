@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
            <span class="breadcrumb-item active">Property Reservation Fee Report</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property Reservation Fee Report</h4>
                            <p class="card-category">List of all Reservation Fee</p>
                        </div>

                    </div>
                </div>
                <div class="card-body">

                    <table id="tanantDatatable" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                <th>date</th>
                                <th>TANANT NAME</th>
                                <th>CONTACT NO.</th>
                                <th>Property Name</th>
                                <th>Unit Name</th>
                                <th> Amount</th>
                                <th> Paid </th>

                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                <div class="d-flex justify-content-end">

                </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Report | Tenant Report`);
        $("#report_nav").addClass("active");

        $(function() {
            var table = $('#tanantDatatable').DataTable({
                processing: true,
                autoWidth: false,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7,],
                        modifer: {
                            page: 'all',
                        }
                    }
                }],
                ajax: {
                    url: "{{ route('showReservationFeeReport') }}",
                },
                columns: [
                      { data:'id'    ,  name:'id'},
                      { data:'date',   name:'date'},
                      { data:'name'  ,  name:'name' },
                      { data:'phone' ,  name:'phone'},
                      { data:'property', name:'property'},
                      { data:'unit' , name:'unit'},
                      { data:'expected_amount', name:'expected_amount'},
                      { data:'status', name:'status'},
                ]
            });
        });
    </script>
@endsection
