@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
            <span class="breadcrumb-item active">Property Vacant Report</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Property Vacant Report</h4>

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <a href="{{route('vacateReportPdf')}}" class="btn btn-primary" target="_blank">print</a>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Id</th>
                            <th>Property Name</th>
                            <th>Unit Name</th>
                            <th>Type</th>
                            <th>Category</th>
                            <th>Lease Type</th>
                            <th>Location</th>
                            <th>Rent</th>
                            <th>Deposit</th>
                            <th>Commission</th>
                            <th>View</th>

                        </thead>
                        <tbody>
                            @foreach ($proprtyvacateReport as $property)
                            <tr>
                                <td>{{$property->id}}</td>
                                @if ($property->builder_id != null)
                                            @php
                                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $property->builder_id)->first();
                                            @endphp
                                        <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                        <td>{{ $property->property_name }}</td>
                                    @else
                                        <td>{{ $property->property_name }}</td>
                                        <td>--</td>
                                    @endif
                                    @if($property->furnished== '0')
                                    <td>Not Furnished</td>
                                    @elseif($property->furnished== '1')
                                    <td>Semi Furnished</td>
                                    @elseif($property->furnished== '2')
                                    <td>Fully Furnished</td>
                                    @else
                                    <td>--</td>
                                    @endif
                                    @if ($property->category=='0')
                                    @if($property->property_to=='0')
                                    <td><span class="badge badge-danger">Residential</span>| <span class="badge badge-warning">Rent</span></td>
                                    @else
                                    <td><span class="badge badge-danger">Residential</span>|<span class="badge badge-warning"> Buy</span></td>
                                    @endif

                                       @elseif ($property->category=='1')
                                       @if($property->property_to=='0')
                                       <td><span class="badge badge-danger">Commercial</span>|<span class="badge badge-warning">Rent</span></td>
                                       @else
                                       <td><span class="badge badge-danger">Commercial</span><span class="badge badge-warning">|Buy</span></td>
                                       @endif

                                       @else
                                       <td>---</td>
                                    @endif

                                    <td>{{isset($property->frequency_rel->type)?$property->frequency_rel->type:"--"}}</td>
                                    <td>
                                        <a class="fad fa-map-marker tx-20 btn btn-primary mb-1 bt_cus mr-1" href="https://maps.google.com/?q={{$property->latitude}},{{$property->longitude}}"></a>
                                    </td>
                                    <td>{{ isset($property->rent) ? $property->rent : "--" }}</td>
                                    <td>{{ isset($property->security_deposit) ? $property->security_deposit : "--" }}</td>
                                    <td>
                                        @if ($property->commission)
                                        {{$property->commission}} @if($property->commission_in) @if($property->commission_in == 'percentage'){{ '%' }}@endif @endif
                                        @endif
                                    </td>
                                    <td><a class="fad fa-eye tx-20 btn btn-primary mb-1 bt_cus mr-1" href=""></a> </td>


                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
                <div class="d-flex justify-content-end">
                    {{$proprtyvacateReport->links()}}
                </div>
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
@endsection
