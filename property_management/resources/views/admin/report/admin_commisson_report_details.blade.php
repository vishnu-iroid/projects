@extends('admin.layout.app')
@section('content')
    <style>
        .br-section-wrapper-dept {
            background-color: #fff;
            padding: 10px 10px;
            box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
            border-radius: 6px;
        }

        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid #eee;
            border-radius: .25rem;
        }

        .h4,
        h4 {
            font-size: 1.125rem;
            font-weight: 300 !important;
        }

        .card-warning {
            background: linear-gradient(60deg, #ffa726, #fb8c00) !important;
            height: 20px !important;
            border-radius: 5px !important;
        }

        .card-warning-2 {
            background: linear-gradient(60deg, #ffa726, #fb8c00) !important;
            height: 20px !important;
            border-radius: 5px !important;
        }

    </style>
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Property mangement Fee Report</span>
        </nav>
    </div>
    <!-- br-pageheader -->
    <div class="br-pagebody">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header card-header-blueheader">
                        <div class="row d-flex justify-content-between">
                            <div class="col-md-4">
                                <h4 class="card-title">Property mangement Fee</h4>
                                <p class="card-category">List of all Property mangement </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body ">
                        @foreach ($properties as $property)
                            <div class="row">
                                <div class="col">
                                    <label for="">Property Id</label>
                                    <p class="form-control">{{ $property->property_id }}</p>
                                </div>
                                <div class="col">
                                    <label for="">Rent</label>
                                    <p class="form-control">{{ $property->prop_rel->rent }}</p>
                                </div>
                                <div class="col">
                                    <label for="">Property</label>
                                    <p class="form-control">{{ $property->prop_rel->property_name }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="">Property Type</label>
                                    @if (isset($property_to->prop_rel->property_to) == '0')
                                        <p class="form-control">Rent</p>
                                    @else
                                        <p class="form-control">Sell</p>
                                    @endif

                                </div>
                                <div class="col">
                                    <label for="">Frequency</label>
                                    @if (isset($property->prop_rel->frequency_rel))
                                        {{-- @php
                                            $frequency = Frequency::find($property->prop_rel->frequency);
                                        @endphp --}}
                                        <p class="form-control">{{ $property->prop_rel->frequency_rel->type }}</p>
                                    @endif

                                </div>
                                <div class="col">
                                    <label for="">Next Due Date</label>
                                    @if ($property)
                                        @php
                                            // $pay_date = AdminAccount::select('payment_type','date')->get();
                                            // $payment = AdminAccount::select('payment_type','date')->where('property_id',$property->prop_rel->id)->get();
                                            
                                            if ($property->prop_rel->admin_account_type->contains('payment_type', '5')) {
                                                $pay_date = \App\Models\AdminAccount::select('payment_type', 'date')
                                                    ->where('payment_type', '5')
                                                    ->where('property_id', $property->prop_rel->id)
                                                    ->orderBy('id', 'DESC')
                                                    ->first();
                                            } else {
                                                $pay_date = \App\Models\AdminAccount::select('payment_type', 'date')
                                                    ->where('payment_type', '2')
                                                    ->where('property_id', $property->prop_rel->id)
                                                    ->orderBy('id', 'DESC')
                                                    ->first();
                                            }
                                            if ($pay_date) {
                                                $date = $pay_date->date;
                                                if ($property->prop_rel) {
                                                    $no_of_days = \App\Models\Frequency::select('id', 'days')
                                                        ->where('id', $property->prop_rel->frequency)
                                                        ->first();
                                                    // Add days to date and display it
                                                    $property->due_date = date('Y-m-d', strtotime($date . ' + ' . $no_of_days->days . '  days'));
                                                    // return $property->due_date;
                                                }
                                            }
                                        @endphp
                                    @endif

                                    <p class="form-control">{{ $property->due_date }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="">Due Date</label>

                                    <p class="form-control">{{ $property->due_date }}</p>
                                </div>
                                <div class="col">
                                    <label for="">contract start date</label>
                                    <p class="form-control">{{ $property->prop_rel->contract_start_date }}</p>
                                </div>
                                <div class="col">
                                    <label for="">contract end date</label>
                                    <p class="form-control">{{ $property->prop_rel->contract_end_date }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="">Check In</label>
                                    <p class="form-control">{{ $property->check_in }}</p>
                                </div>
                                <div class="col">
                                    <label for="">Check Out</label>
                                    <p class="form-control">{{ $property->check_out }}</p>
                                </div>
                                <div class="col">
                                    <label for="">Security Deposit</label>
                                    <p class="form-control">{{ $property->prop_rel->security_deposit }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="">Token Amount</label>
                                    <p class="form-control">{{ $property->prop_rel->token_amount }}</p>
                                </div>
                                <div class="col">
                                    <label for="">Due Amount</label>
                                    @if ($property->prop_rel->property_to == '0')
                                        @php
                                            $amount = $property->prop_rel->rent;
                                            
                                        @endphp
                                        @php
                                            if ($property->prop_rel) {
                                                // dd($property->rent_rel->amount);
                                                if ($property->prop_rel->rent) {
                                                    $amount = $property->prop_rel->rent + $property->prop_rel->security_deposit - $property->prop_rel->token_amount;
                                                }
                                            }
                                        @endphp
                                    @else
                                        @php
                                            if ($property->prop_rel) {
                                                if ($property->prop_rel->selling_price) {
                                                    $amount = $property->prop_rel->selling_price + $property->prop_rel->security_deposit - $property->prop_rel->token_amount;
                                                }
                                            }
                                        @endphp
                                    @endif
                                    <p class="form-control">{{ $amount }}</p>
                                </div>
                                <div class="col">
                                    <label for="">Commission</label>
                                    @if ($property->prop_rel->commission_in == 'percentage')
                                        @if ($property->prop_rel->property_to == '0')
                                            @php
                                                $rent = $property->prop_rel->rent;
                                                // $rent_commssion_percentage = ($property->prop_rel->property/100) * $rent;
                                                // $rent = $property->rent;
                                                $rent_commssion_percentage = ($property->prop_rel->commission / 100) * $rent;
                                                $rent_commssion_percentage_balnce = $rent - $rent_commssion_percentage;
                                            @endphp
                                            <p class="form-control">
                                                {{ $rent_commssion_percentage_balnce ? $rent_commssion_percentage_balnce : 'Nil' }}
                                            </p>
                                        @else
                                            @php
                                                $selling_price = $property->prop_rel->selling_price;
                                                $selling_price_commssion_percentage = $property->prop_rel->commission * $selling_price;
                                                $selling_price_commssion_percentage_balnce = $selling_price - $selling_price_commssion_percentage;
                                            @endphp

                                            <p class="form-control">
                                                {{ $selling_price_commssion_percentage_balnce ? $selling_price_commssion_percentage_balnce : 'Nil' }}
                                            </p>
                                        @endif
                                    @else
                                        @if ($property->prop_rel->property_to == '0')
                                            @php
                                                $rent = $property->prop_rel->rent;
                                                // $rent_commssion_amount_balnce = $rent - $property->prop_rel->property;
                                                // $rent = $property->rent;
                                                $rent_commssion_amount_balnce = $rent - $property->commission;
                                            @endphp

                                            <p class="form-control">
                                                {{ $rent_commssion_amount_balnce ? $rent_commssion_amount_balnce : 'Nil' }}
                                            </p>
                                        @else
                                            @php
                                                $selling_price = $property->prop_rel->selling_price;
                                                $selling_price_commssion_amount_balnce = $selling_price - $property->prop_rel->commission;
                                            @endphp

                                            <p class="form-control">
                                                {{ $selling_price_commssion_amount_balnce ? $selling_price_commssion_amount_balnce : 'Nil' }}
                                            </p>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
    </div>
    </div>
    <!-- br-pagebody -->
@endsection
