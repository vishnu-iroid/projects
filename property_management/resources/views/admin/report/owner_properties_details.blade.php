@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Reports</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Owner Property Report</h4>
                        <p class="card-category">List of all Owner Properties</p>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table display responsive nowrap">
                    <thead>
                        <tr>
                            <th class="wd-5p">Id</th>
                            <th class="wd-5p">Property Name</th>
                            <th class="wd-10p">Reg No.</th>
                            <th class="wd-5p"> To</th>
                            <th class="wd-5p"> Category</th>
                            <th class="wd-15p">Owner Payment Amount</th>
                            <th class="wd-15p">Owner Service Amount</th>
                        </tr>
                    </thead>
                    <tbody>


                        <div style="display: none">
                            {{-- Declare a counter variable and
                            initialize it with 0 --}}
                            @php
                                $total = 0 ;
                                $counter = 0;
                            @endphp  
                             </div>
                        @foreach ( $details as $row )
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->property_name }} </td>
                            <td>{{ $row->property_reg_no}} </td>
                            <td>{{ $row->property_to == '0' ? 'Rent' : 'Sell' }}</td>
                            <td>
                                @if($row->category == 0)
                                    Residential
                                @elseif($row->category == 1)
                                    Commercial
                                @endif
                            </td>
                            <td>
                                @if ($row->owner_amount )
                                    @foreach ($row->owner_amount as $key )
                                    <div style="display: none">
                                    {{ $total += $key->amount }}
                                    </div>

                                    @if( $counter == count( $row->owner_amount ) - 1)
                                    {{  $total }}

                                    @endif
                                    <div style="display: none">
                                        @php
                                            $counter = $counter + 1;
                                        @endphp
                                     </div>

                                    @endforeach
                                     @else
                                     {{ 0 }}
                                @endif
                                {{ 0  }}
                            </td>
                            <td>
                                @if ($row->owner_service_amount )
                                    @foreach ($row->owner_service_amount as $key )
                                    <div style="display: none">
                                    {{ $total += $key->amount }}
                                    </div>

                                    @if( $counter == count( $row->owner_service_amount ) - 1)
                                    {{  $total }}

                                    @endif
                                    <div style="display: none">
                                        @php
                                            $counter = $counter + 1;
                                        @endphp
                                     </div>

                                    @endforeach
                                     @else
                                     {{ 0 }}
                                @endif
                                {{ 0  }}

                            </td>

                         </tr>
                        @endforeach


                    </tbody>
                </table>
                <div class="d-flex justify-content-end">

                    {{ $details->links() }}

                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<!-- Add and Edit Modal -->


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Report | Properties Report`);
    $("#report_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>






@endsection


