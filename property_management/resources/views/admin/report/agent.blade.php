@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
            <span class="breadcrumb-item active">Agents Reports</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Agents Report</h4>

                        </div>

                    </div>
                </div>
                @php
                    $i = 0;
                @endphp
                <div class="card-body">
                    <table id="agentDatatable" class="table table-bordered table-striped">
                        <thead>
                            <th>Id</th>
                            <th>Agent Name</th>
                            <th>CONTACT NO</th>
                            <th>TOTAL PROPERTY</th>
                            <th>ASSIGNED LEADS</th>
                            <th>ON GOING</th>
                            <th>CLOSED</th>
                            <th>Terminated</th>
                            <th>Pending</th>
                            <th>TOTAL ERNINGS</th>

                        </thead>
                        <tbody>

                        </tbody>

                    </table>
                </div>
                {{-- <div class="d-flex justify-content-end">
                    {{ $agents->links() }}
                </div> --}}
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.print.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script>
        $(function() {
            var table = $('#agentDatatable').DataTable({
                processing: true,
                searching: false,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2],
                        modifer: {
                            page: 'all',
                        }
                    }

                }],
                ajax: {
                    url: "{{ route('agentReport') }}",

                },
                columns: [{
                        data: 'id',
                        name: 'id',
                        orderable: true
                    },

                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        data: 'counts',
                        name: 'counts',

                    },
                    {
                        data:'opendeal',
                        name:'opendeal'
                    },
                    {
                        data:'closing_deal',
                        name:'closing_deal'
                    },
                    {
                        data:'terminated',
                        name:'terminated'
                    },
                    {
                        data:'pending',
                        name:'pending'
                    },
                    {
                        data:'pending',
                        name:'pending'
                    },
                    {
                        data:'pending',
                        name:'pending'
                    }


                ]
            });

        });
    </script>
@endsection
