@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Tenant Report</h4>
                            @php
                                $st = isset($details[0]) ? $details[0] : false;
                            @endphp
                            @if ($st)
                                <p class="card-category">Tenant Name : <b>{{ $details[0]->user_rel->name }}</b></p>
                                <p class="card-category">Contact No : <b>{{ $details[0]->user_rel->phone }}</b></p>
                                <p class="card-category">Email: <b>{{ $details[0]->user_rel->email }}</b></p>
                                <p class="card-category">Address: <b>{{ $details[0]->user_rel->address }}</b></p>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-end mb-4">
                        {{-- <a class="btn btn-primary" href="">Export to PDF</a> --}}
                    </div>
                    <table id="datatable2" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                {{-- <th class="wd-15p">Tanant NAME</th> --}}
                                <th class="wd-15p">Property Name</th>
                                <th class="wd-15p">Unit Name</th>
                                <th>Property Type</th>
                                <th class="wd-10p">Property_for</th>
                                <th>Lease Type</th>
                                <th>Contract Expiry</th>
                                <th>Due Date</th>
                                <th>Due Amount</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $row)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    {{-- @if ($row->user_rel)
                          <td>{{ $row->user_rel->name}} </td>
                         @endif --}}
                                    @if ($row->prop_rel)
                                        @if ($row->prop_rel->builder_id != null)
                                            @php
                                                $data = DB::table('owner_properties')
                                                    ->select('id', 'property_name')
                                                    ->where('id', $row->prop_rel->builder_id)
                                                    ->first();
                                            @endphp
                                            <td>{{ isset($data->property_name) ? $data->property_name : '---' }}</td>
                                            <td>{{ $row->prop_rel->property_name }}</td>
                                        @else
                                            <td>{{ $row->prop_rel->property_name }}</td>
                                            <td>--</td>
                                        @endif
                                        @if ($row->prop_rel->category== 0)
                                            <td>Residential</td>
                                            @else
                                            <td>Commercial</td>
                                        @endif
                                        <td>
                                            @if ($row->prop_rel)
                                                @if ($row->prop_rel->property_to == 0)
                                                    <p class="text-success">{{ 'RENT' }} </p>
                                                @elseif ($row->prop_rel->property_to == 1)
                                                    <p class="text-success"> {{ 'BUY' }} </p>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if ($row->prop_rel)
                                                @if ($row->prop_rel->frequecy_rel)
                                                 {{$row->prop_rel->frequecy_rel->type}}
                                                @endif
                                            @endif

                                        </td>
                                        <td>
                                            @if ($row->contract_rel)
                                                @if ($row->contract_rel->contract_end_date)
                                                    {{ $row->contract_rel->contract_end_date }}
                                                @endif
                                            @endif
                                        </td>
                                        <td>{{ isset($row->due_date) ? $row->due_date : "--"  }}</td>
                                        <td>{{ isset($row->rent) ? $row->rent : 0 }}</td>
                                        {{-- <td>{{ isset($row->rent_rel->amount) ? $row->rent_rel->amount : 0 }}</td> --}}
                                    @endif
                                    <td><a href="{{route('tenantDetailedReport',['id'=>$row->prop_rel->id])}}" class="btn btn-primary" target= "_blank" >pdf</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">

                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Report | Tenant Properties`);
        $("#report_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable2").DataTable({
                responsive: true,
                autoWidth: false,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
    </script>
@endsection
