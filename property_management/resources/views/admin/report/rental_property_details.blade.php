@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Rental Reports</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Rental Report</h4>
                            <p class="card-category">Property Name :
                                <b>{{ $details->prop_rel->property_name ? $details->prop_rel->property_name : $details->prop_rel->property_reg_no }}</b>
                            </p>
                        </div>

                    </div>
                </div>
                @php
                    $i = 0;
                @endphp
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>

                            <th>Owner</th>
                            <th>Check In</th>
                            <th>Check out</th>
                            <th>Contract Start Date</th>
                            <th>Contract End Date</th>
                            <th>Rent</th>
                            <th>Security Deposit</th>
                            <th>Token Amount</th>
                            <th>Net Amount</th>
                            <th>Balance</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <tr>


                                <td>{{isset($details->prop_rel->owner_rel->name)?$details->prop_rel->owner_rel->name:"--"}}</td>
                                <td>{{isset($details->check_in)?$details->check_in:"--"}}</td>
                                <td>{{isset($details->check_out)?$details->check_out:"--"}}</td>
                                <td>{{isset($details->prop_rel->contract_start_date)?$details->prop_rel->contract_start_date:"--"}}</td>
                                <td>{{isset($details->prop_rel->contract_end_date)?$details->prop_rel->contract_end_date:"--"}}</td>
                                <td>{{isset($details->prop_rel->rent)?$details->prop_rel->rent:"--"}}</td>
                                <td>{{isset($details->prop_rel->security_deposit)?$details->prop_rel->security_deposit:"--"}}</td>
                                <td>{{isset($details->prop_rel->token_amount)?$details->prop_rel->token_amount:'--'}}</td>

                                <td>{{ $netamount}}</td>
                                <td>{{isset($balance)?$balance:0}}</td>
                                <td><a href="{{route('rentalReport',['id'=>$details->prop_rel->id])}}" class="btn btn-primary" target= "_blank" >print</a></td>
                            </tr>
                        </tbody>

                    </table>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Report | Tenant Properties`);
        $("#report_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
    </script>
@endsection
