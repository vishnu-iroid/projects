@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
</style>
@section('content')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item" href="{{route('showDashboard')}}">Home</a>
        <span class="breadcrumb-item active">Accounts Management</span>
        <span class="breadcrumb-item active">Reports</span>
        <span class="breadcrumb-item active">Rental Report</span>
    </nav>
</div>
<div class="br-pagebody">
    <div class="col-md-12 mg-b-30 mg-t-60">
        <div class="card shadow">
            <div class="card-header card-header-blueheader">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-4">
                        <h4 class="card-title h4-card">Rental Report</h4>
                        <p class="card-category">List of all Rental Properties</p>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <table id="datatable2" class="table table-striped table-bordered display responsive nowrap text-center">
                    <thead>
                        <tr class="">
                            <th class="wd-5p">Id</th>
                            <th class="wd-20p">Property Name</th>
                            <th class="wd-10p">Unit </th>
                            <th class="wd-15p">Tenant</th>
                            <th class="wd-20p">Expiry Date</th>
                            <th class="wd-20p">Remaining Amount</th>
                            <th class="wd-15p">View More</th>
                        </tr>
                    </thead>
                    <tbody class="">
                        @foreach ($list as  $row)
                        <tr>
                            <td>{{ $loop->iteration }} </td>

                            @if ($row->prop_rel->builder_id != null)
                            @php
                             $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->prop_rel->builder_id)->first();
                            @endphp
                                <td>{{ isset($row->prop_rel->property_name) ? $row->prop_rel->property_name : "---"  }}</td>
                                <td>{{ $row->prop_rel->property_name }}</td>
                            @else
                                <td>{{ $row->prop_rel->property_name }}</td>
                                <td>--</td>
                            @endif

                            <td>{{ $row->user_rel->name }}</td>
                            <td>{{ isset($row->contract_rel->contract_end_date) ? $row->contract_rel->contract_end_date : "---"  }}</td>
                            <td>{{  isset($row->remaining_amount) ? floor($row->remaining_amount ): "---"   }}</td>
                            <td><a href='{{ route('showRentalPropertyDetails',['id' => $row->id])}}' class="fad fa-eye tx-20  mr-1 btn btn-primary mb-1 bt_cus" href="javascript:0;" title="View"></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="d-flex justify-content-end">

                </div>
            </div>
            <!-- table-wrapper -->
        </div>
    </div>
    <!-- br-section-wrapper -->
</div>
<!-- br-pagebody -->
<!-- Add and Edit Modal -->


@endsection @section('scripts')
<script src="{{asset('assets/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly" async></script>
<script>
    const url = '{{url("/")}}';
    $('#image').on('change', function() {
        var fileName = $(this).val();
        $(this).next('#file_label').html(fileName);
    });
    $('.modalnav').click(function() {
        var id = $(this).attr('id')
        if (id == 'location-tab') {
            $('#propAddBtn').attr('disabled', false)
        } else {
            $('#propAddBtn').attr('disabled', true)
        }
    })
</script>
<script>
    $("#site_title").html(`Report | Agent Report`);
    $("#report_nav").addClass("active");
    $(function() {
        "use strict";
        $("#datatable1").DataTable({
            responsive: true,
            language: {
                searchPlaceholder: "Search...",
                sSearch: "",
                lengthMenu: "_MENU_ items/page",
            },
            paging: false,
            ordering: true,
            searching: true,
            info: false,
            bLengthChange: false,
            dom: "Bfrtip",
        });
        // Select2
        $(".dataTables_length select").select2({
            minimumResultsForSearch: Infinity,
        });
    });
</script>






@endsection

