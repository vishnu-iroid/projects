@extends('admin.layout.app')
<style>
    .br-section-wrapper-dept {
        background-color: #fff;
        padding: 10px 10px;
        box-shadow: 0px 1px 3px 0px rgb(0 0 0 / 21%);
        border-radius: 6px;
    }

    .select2-container {
        z-index: 10050;
        width: 100% !important;
        padding: 0;
    }

    .modal {
        overflow: auto !important;
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }

    .h4-card {
        font-size: 1.125rem;
        font-weight: 300 !important;
    }

    #map {
        height: 450px;
        width: 100%;
    }
    table {
    display: block;
    overflow-x: auto;
    white-space: nowrap;
}

</style>
@section('content')
    <div class="br-pageheader">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
            <span class="breadcrumb-item active">Reports</span>
            <span class="breadcrumb-item active">Owner Property Report</span>
        </nav>
    </div>
    <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 mg-t-60">
            <div class="card shadow">
                <div class="card-header card-header-blueheader">
                    <div class="row d-flex justify-content-between">
                        <div class="col-md-4">
                            <h4 class="card-title h4-card">Owner Property Report</h4>
                            @php
                                $st = isset($details[0]) ? $details[0] : false;
                            @endphp
                            @if ($st)
                                <p class="card-category">Tenant Name : <b>{{ $details[0]->owner_rel->name }}</b></p>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table id="datatable2" class="table display table-striped responsive nowrap">
                        <thead>
                            <tr>
                                <th class="wd-5p">Id</th>
                                {{-- <th class="wd-15p">Owner Name</th> --}}
                                <th class="wd-15p">Property Name</th>
                                <th class="wd-15p">Unit Name</th>
                                <th class="wd-15p">Property Category</th>
                                <th class="wd-20p">Property_for</th>
                                <th class="wd-20p">Property Status</th>
                                <th class="wd-20p">Contract Start Date</th>
                                <th class="wd-20p">Contract End Date</th>
                                <th class="wd-20p">Ads Number</th>
                                <th class="wd-20p">Guard Number</th>
                                <th class="wd-20p">Total Rent</th>
                                {{-- <th class="wd-15p">Actions</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $row)
                                <tr>
                                    <td> {{ $loop->iteration }} </td>
                                    {{-- @if ($row->owner_rel)
                          <td>{{ $row->owner_rel->name}} </td>
                         @endif --}}
                                    @if ($row)
                                    @if ($row->builder_id != null)
                                    @php
                                     $data = DB::table('owner_properties')->select('id','property_name')->where('id', $row->builder_id)->first();
                                    @endphp
                                <td>{{ isset($data->property_name) ? $data->property_name : "---"  }}</td>
                                <td>{{ $row->property_name }}</td>
                            @else
                                <td>{{ $row->property_name }}</td>
                                <td>--</td>
                            @endif
                                        
                                        <td> @if ($row->category == 0)
                                            <p >Residential</p>
                                        @elseif ($row->category == 1)
                                            <p >Commercial</p>
                                        
                                        @endif
                                    </td>

                                        <td>
                                            @if ($row->status == 0)
                                                <p class="text-success">{{ 'Rental' }}</p>
                                            @elseif ($row->status == 1)
                                                <p class="text-warning">{{ 'Owned' }} </p>
                                            @elseif ($row->status == 2)
                                                <p class="text-primary"> {{ 'Booked' }} </p>
                                            @endif
                                        </td>
                                        <td> @if ($row->occupied == 0)
                                            <p>Vacant</p>
                                            @else
                                            <p>Occupied</p>
                                            @endif
                                        </td>
                                        <td>
                                            <p>{{$row->contract_start_date}}</p>
                                        </td>
                                        <td>
                                            <p>{{$row->contract_end_date}}</p>
                                        </td>
                                        <td>
                                            <p>{{$row->ads_number}}</p>
                                        </td>
                                        <td>
                                            <p>{{$row->guards_number}}</p>
                                        </td>
                                        @if ($row->status == 0)
                                        <td> {{ $row->rent }}</td>
                                        @else
                                        <td>--</td>
                                        @endif
                                        <td>
                                    @endif



                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                    <div class="d-flex justify-content-end">
                        {!! $details->links() !!}
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
        </div>
        <!-- br-section-wrapper -->
    </div>
    <!-- br-pagebody -->
    <!-- Add and Edit Modal -->
    @endsection @section('scripts')
    <script src="{{ asset('assets/lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwMLIJNRckTkpa28XgA1AxANFoK2r4-dM&callback=initMap&libraries=&v=weekly"
        async></script>
    <script>
        const url = '{{ url('/') }}';
        $('#image').on('change', function() {
            var fileName = $(this).val();
            $(this).next('#file_label').html(fileName);
        });
        $('.modalnav').click(function() {
            var id = $(this).attr('id')
            if (id == 'location-tab') {
                $('#propAddBtn').attr('disabled', false)
            } else {
                $('#propAddBtn').attr('disabled', true)
            }
        })
    </script>
    <script>
        $("#site_title").html(`Report | Tenant Properties`);
        $("#report_nav").addClass("active");
        $(function() {
            "use strict";
            $("#datatable1").DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: "Search...",
                    sSearch: "",
                    lengthMenu: "_MENU_ items/page",
                },
                paging: false,
                ordering: true,
                searching: true,
                info: false,
                bLengthChange: false,
                dom: "Bfrtip",
            });
            // Select2
            $(".dataTables_length select").select2({
                minimumResultsForSearch: Infinity,
            });
        });
    </script>
@endsection
