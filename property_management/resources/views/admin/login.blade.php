<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- vendor css -->
    <link href="{{asset('assets/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bracket.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        .login-min{
            background-color: #fff;
        }
    </style>
  </head>

  <body>
    <div class="d-flex align-items-center justify-content-center ht-100v">
      <img src="{{asset('assets/images/login_background.jpg')}}" class="wd-100p ht-100p object-fit-cover" alt="">
      <div class="overlay-body d-flex align-items-center justify-content-center">
        <div class="login-wrapper login-min wd-300 wd-xs-350 pd-25 pd-xs-40 rounded bd bd-white-2 bg-white-7">
          <div class="signin-logo tx-center tx-28 tx-bold tx-white">
            <img src="{{asset('assets/backdrops/Logo-dashboard.png')}}" class="img-fluid">
          </div>
          <!-- <div class="tx-white-5 tx-center mg-b-60">The Admin Template For Perfectionist</div> -->
          <form id="loginForm">
            <div class="msgDiv"></div>
            <div class="form-group">
              <input type="text" class="form-control  login-form-input" name="email" placeholder="Enter your email">
            </div><!-- form-group -->
            <div class="form-group">
              <input type="password" class="form-control  login-form-input" name="password" placeholder="Enter your password">
              <a href="" class="tx-success tx-12 d-block mg-t-10" data-toggle="modal" data-target="#forgotModal">Forgot password?</a>
            </div><!-- form-group -->
            <div class="row">
                <div class="col-6">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember_me" name="remember_me">
                        <label for="remember">
                            Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <button type="submit" class="btn btn-property-success btn-block" id="signin">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
          </form>
          <!-- <div class="mg-t-60 tx-center">Not yet a member? <a href="" class="tx-info">Sign Up</a></div> -->
        </div><!-- login-wrapper -->
      </div><!-- overlay-body -->
    </div><!-- d-flex -->

     <!-- Forgot Password Modal -->
     <div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 380px!important;transition: all 0.5s ease 0s; transform: translate(0px, 0px); opacity: 1;">
                <div class="modal-content delete-popup">
                <div class="modal-header bg-gold">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title tx-white"></h4>
                </div>
                <form action="javascript:;" id="resetForm">
                    <div class="modal-body">
                        <div class="d-flex justify-content-center">
                            <img src="{{asset('assets/backdrops/forgot.jpg')}}" alt="" class="w-50 h-50">
                        </div>
                        <h3 class="d-flex justify-content-center tx-black">Forgot your Password?</h3>
                        <p class="d-flex justify-content-center text-center">Enter your email address below to receive password reset link</p>
                        <div class="d-flex justify-content-center msgDiv"></div>
                        <div class="form-group d-flex justify-content-center">
                            <input type="text" class="form-control col-lg-8" name="forgotemail" id="forgotemail" placeholder="Email Address">
                            <label for="forgotemail" id="11" generated="true" class="error"></label>
                        </div>
                        <div class="d-flex justify-content-center mb-4">
                          <button type="submit" class="btn btn-property-success btn-block forgotBtn col-lg-8">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="max-width: 380px!important;transition: all 3s ease 0s; transform: translate(0px, 0px); opacity: 1;">
                <div class="modal-content delete-popup">
                <div class="modal-header bg-gold">
                    <button type="button" class="close" data-dismiss="modal" onclick="window.location.reload();">&times;</button>
                    <h4 class="modal-title tx-white"></h4>
                </div>
                    <div class="modal-body">
                        <div class="d-flex justify-content-center">
                            <img src="{{asset('assets/backdrops/sent.svg')}}" alt="" height="120" width="150">
                        </div>
                        <h3 class="d-flex justify-content-center tx-black">Check your Mail!</h3>
                        <p class="d-flex justify-content-center text-center">Password reset link has been sent to your email</p>
                    </div>
                    <div class="d-flex justify-content-center mg-b-40">
                        <div class="col-lg-8">
                            <button type="button" class="btn btn-gold btn-block successBtn" onclick="window.location.reload();">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <script src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>
<script>
  $('#passwordreset').click(function(){
      $('#forgotModal').modal();
  })

  $('#loginForm').validate({
    normalizer : function(value){
      return $.trim(value)
    },
    rules : {
      email : {
        required : true,
        email : true,
      },
      password : {required : true }
    },
    messages : {
      email : {
        required: "Email is required",
        email : "Email is invalid",
      },
      password : {
        required : "Password is required",
      },
    },
    submitHandler: function(form){
      $('.msgDiv').html(``);
      $('#signin').prop('disabled',true)
      $.ajax({
        url : '{{route("login")}}',
        type : "POST",
        data : $(form).serialize(),
        dataType : "json",
        async : true,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend : function() {
          $('#signin').html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
        },
        success : function(data){
          $('#signin').html('Sign In')
          $('#signin').prop('disabled',false)
          if(data.status == 1){
            toastr['success'](data.response)
            setTimeout(() => {
              window.location.href="{{route('showDashboard')}}"
            }, 1000);
          }else {
            // var html = ""
            // $.each(data.response, (key,value) => html += value + '</br>')
            // $('.msgDiv').html('<div class="alert alert-danger"><b>'+html+'</b></div>')
            // setTimeout(() => {
            //   $('.msgDiv').html(``)
            // },2000);
            window.location.href="{{url('/')}}"

          }
        }
      })
      return false;
    }
  })

  jQuery.validator.messages.required = "";
  jQuery.validator.messages.email = "";
  $('#resetForm').validate({
    normalizer: function (value) {
      return $.trim(value)
    },
    onclick: false,
    rules: {
      forgotemail: {
        required: true,
        email: true
      },
    },
    highlight: function (element, errorClass) {
      $(element).fadeOut(function () {
        $(element).fadeIn();
      });
    },
    submitHandler: function (form) {
      $.ajax({
        type: "POST",
        url: "",
        data: $(form).serialize(),
        dataType: "json",
        async: true,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function () {
          $('.forgotBtn').html('<i class="fas fa-circle-notch fa-1x fa-spin"></i>');
        },
        success: function (data) {
          $('.forgotBtn').html('Reset');
          if (data.status == 1) {
            $('#forgotModal').fadeOut();
            $('#successModal').modal();
          } else {
            var html = "";
            $.each(data.response, function (key, value) {
              html += value + "<br/>";
            });
            $('.msgDiv').html(` <div class="alert alert-danger">` + html + `</div> `);
            setTimeout(function () {
              $('.msgDiv').html(``);
            }, 2000);
          }
        }
      });
      return false;
    },
  })
</script>
