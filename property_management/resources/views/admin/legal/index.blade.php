   @extends('admin.layout.app')
   <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
   <style>
     .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid #eee;
        border-radius: .25rem;
    }
    .card-header-b{
      height : 300px !important;
    }
   </style>
   @section('content')
  
        <div class="br-pageheader">
          <nav class="breadcrumb pd-0 mg-0 tx-12">
              <a class="breadcrumb-item" href="{{ route('showDashboard') }}">Home</a>
              <a class="breadcrumb-item" href="">CMS</a>
              <span class="breadcrumb-item active">Legal Information</span>
          </nav>
   
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <div class="col-md-12 mg-b-30 ">
        <div class="card shadow">
        <div class="card-header card-header-blueheader">
          <div class="row d-flex justify-content-between">
              <div class="col-md-6">
                  <h4 class="card-title h4-card">Legal Information</h4>
                 
              </div>
          </div>
      </div>
        <form action="{{ route('addLegal') }}" method="POST">
                <div class="br-section-wrapper mg-t-10 mb-3">
                    <h4 class="br-section-label">English</h4>
                        @csrf
                        <div class="form-layout form-layout-1">
                            <div class="col-lg-12">
                                <input type="hidden" id="id" name="id" value="">
                                <div class="form-group">
                                    <label class="form-control-label">Legal Information: <span class="tx-danger">*</span></label>
                                    <textarea class="form-control ckeditor" type="text" rows="5" name="english_description">{{ $legal->english_description ?? '' }}</textarea>
                                </div>
                            </div><!-- col-4 -->

                        </div><!-- form-layout -->
                </div>

                <div class="br-section-wrapper mg-t-2">
                    <h4 class="br-section-label">Arabic</h4>
                        <div class="form-layout form-layout-1">
                        <div class="row mg-b-25">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label">Legal Information: <span class="tx-danger">*</span></label>
                                    <textarea id="editor2" class="form-control  ckeditor" type="text" rows="5" name="arabic_description">{{ $legal->arabic_description ?? '' }}</textarea>
                                </div>
                            </div><!-- col-4 -->
                        </div><!-- row -->

                        <div class="form-layout-footer">
                            <button class="btn btn-info">Submit</button>
                        </div><!-- form-layout-footer -->
                        </div><!-- form-layout -->
                </div>

            </form>
      </div><!-- br-pagebody -->
      </div><!-- br-pagebody -->
      </div>  <!-- br-pagebody -->
    @endsection
    @section('scripts')
    <script src="{{asset('assets/lib/ckeditor/ckeditor.js')}}"></script>
    <script>
    CKEDITOR.replace('editor2', {
      extraPlugins: 'bidi',
      // Setting default language direction to right-to-left.
      contentsLangDirection: 'rtl',
      height: 270,
      scayt_customerId: '1:Eebp63-lWHbt2-ASpHy4-AYUpy2-fo3mk4-sKrza1-NsuXy4-I1XZC2-0u2F54-aqYWd1-l3Qf14-umd',
      scayt_sLang: 'auto'
    });
  </script>
    @endsection
