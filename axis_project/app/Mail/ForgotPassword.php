<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'verify@axistc.com';
        $subject = 'Axis Insight password assistance';
        $name   = 'Axis Technology Company';
        return $this->view('emails.forgotpassword')
            ->from($address)
            ->subject($subject)
            ->with($this->data);
    }
}
