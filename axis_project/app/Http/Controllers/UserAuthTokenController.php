<?php

namespace App\Http\Controllers;

use App\UserAuthToken;
use Illuminate\Http\Request;

class UserAuthTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserAuthToken  $userAuthToken
     * @return \Illuminate\Http\Response
     */
    public function show(UserAuthToken $userAuthToken)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserAuthToken  $userAuthToken
     * @return \Illuminate\Http\Response
     */
    public function edit(UserAuthToken $userAuthToken)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserAuthToken  $userAuthToken
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserAuthToken $userAuthToken)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserAuthToken  $userAuthToken
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserAuthToken $userAuthToken)
    {
        //
    }
}
