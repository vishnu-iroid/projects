<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
// // use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\DB;
use Auth;
use DB;
use Response;
use DateTime;

// use Carbon\Carbon;
// use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\UserDetails;
use App\UserAuthToken;
use App\Company;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;

class UserDetailsController extends Controller
{
    use AuthorizesRequests;
    public function checkcompany(Request $request){

        $company_code = $request->company_code;
        $verification ='';
        if($company_code!=''){
           $verification =  DB::table('company')
          ->select('CompanyName')
          ->where('CompanyCode','=',$company_code)
          ->get();
          if(count($verification)<=0){
             echo json_encode(["status" => "false", "message" => "Please enter valid company code"]); exit();
          }else{
            echo json_encode(["status" => "true", "message" => "Successful valid company id"]); exit();
          }
        }else{
            echo json_encode(["status" => "false", "message" => "Company code is required"]); exit();
       }
    }
    public function userdata(Request $request){

    $userid =$this->checkAuth($request);
    if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
    $user = UserDetails::where('id','=',$userid->user_id)->first();
    $company_id = $user->CompanyID;
    $verification ='';
    if($company_id!=''){
       $verification =  DB::table('company')
                            ->select('CompanyName','CompanyCode','conhostname','condbname','conusername','conpassword')
                            ->where('CompanyID','=',$company_id)
                            ->get();
      if(count($verification)<=0){
         echo json_encode(["status" => "false", "message" => "Authentication key mismatched..."]); exit();
      }
    }
    if($user) {
      $this->getConnectionDetails($company_id);
      DB::beginTransaction();
      DB::enableQueryLog();
      $user_data = DB::connection('mysql_source')
      ->table('chainaccess')
      ->leftjoin('locationaccess','locationaccess.UserId','=','chainaccess.UserID')
      ->leftjoin('chain', 'chainaccess.ChainID', '=', 'chain.ChainId')
      ->leftjoin('location', 'locationaccess.LocationID', '=', 'location.LocationId')
      ->select('chainaccess.ChainID','locationaccess.LocationID','ChainName','LocationName','chainaccess.FullAccess')
      ->where('chainaccess.UserId','=',$user->user_id)
      ->get();
      $date = date('Y-m-d').' 00:00:00';
      $date1 =date('Y-m-d');
      $user_type="Normal";
      if($user->CompanyID == 'KCL'){
        $date ='2020-01-15'.' 00:00:00';
        $date1 ="2020-01-15";
        $user_type ="Dummy";
      }
      $chainid = $user_data[0]->ChainID;
      $companyid = $user->CompanyID;
      $fname =$user->firstname;
      $lname =$user->lastname;
      $user_name =$fname.' '.$lname;
      $image     =$user->image;
      $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
      if (!strpos($image,$url)) {
        $image     = url('/').'/'.$image;
      }
      //$image     = url('/').'/'.$image;
      $chainname =$user_data[0]->ChainName;
      if($user_data[0]->FullAccess =='1'){
        $location= DB::connection('mysql_source')
        ->table('location')
      ->join('locationaccess', 'locationaccess.ChainId', '=', 'location.ChainId')
      ->select('location.LocationId','LocationName')
      ->where('location.ChainId','=',$chainid)
      ->where('locationaccess.UserId','=',$user->user_id)
      ->groupBy('location.LocationId')
      ->groupBy('location.LocationName')
      ->get();
     }else{
       $location    =   DB::connection('mysql_source')
                            ->table('locationaccess')
                            ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                            ->select('locationaccess.LocationId','LocationName')
                            ->where('locationaccess.ChainId','=',$chainid)
                            ->where('locationaccess.UserId','=',$user->user_id)
                            ->groupBy('locationaccess.LocationId')
                            ->groupBy('location.LocationName')
                            ->get();
     }
     $locationid =$location[0]->LocationId;
     $locationname =$location[0]->LocationName;
      $data =array('user_name'=>$user_name,'image'=>$image,'UserType'=>$user_type,'CompanyName'=>$verification[0]->CompanyName,'CompanyID'=>$companyid,'ChainID'=>$chainid,'date'=>$date1,'ChainName'=>$chainname,'LocationID'=>$locationid,'LocationName'=>$locationname);
      $data2 =  DB::connection('mysql_source')
                        ->table('sales_header')
                        ->select('OrderType',DB::raw('COUNT(VoucherNo) as trx'),DB::raw('SUM(GuestCount) as GuestCount'),DB::raw('SUM(SalesAmount) as sales'))
                        ->where('sales_header.CompanyID','=',$company_id)
                        ->where('ChainId','=',$chainid)
                        ->where('LocationId','=',$locationid)
                        ->where('sales_header.VoucherStatus','LIKE',"%S%")
                        ->where('VoucherDate','=',$date)
                        ->groupBy('OrderType')
                        ->get();

                        // ->table('sales_line')
                        // ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                        // DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date. '" AND `CompanyID`= "'.$company_id.'" AND `LocationID` = "' . $locationid .'"  AND `ChainID` = "'.$chainid.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                        // DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                        // ->leftJoin("sales_header",function($join){
                        // $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        //     ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        //     ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        //     ->on('sales_line.ChainID','=','sales_header.ChainID')
                        //     ->on('sales_line.LocationID','=','sales_header.LocationID');
                        // })
                        // ->where('sales_line.CompanyID','=', $company_id)
                        // ->where('sales_line.ChainID','=', $chainid)
                        // ->where('sales_line.LocationID', $locationid)
                        // ->where('sales_line.VoucherDate', $date)
                        // ->where('sales_line.SalesStatus','S')
                        // ->groupBy('sales_header.OrderType')
                        // ->get();
      $query = DB::getQueryLog();
      $data2 = json_decode(json_encode($data2), true);
      $i=0;
      $datatwo =array();
      foreach($data2 as $arr){
         $datatwo[$i]['OrderType'] = $arr['OrderType'];
         $datatwo[$i]['trx'] = strval(round($arr['trx'],2));
         $datatwo[$i]['GuestCount'] = strval(round($arr['GuestCount'],2));
         $datatwo[$i]['Sales'] = strval(number_format($arr['sales'],2));
         $i++;
      }

      $data_total= DB::connection('mysql_source')
      ->table('sales_header')
      ->select(DB::raw('COUNT(VoucherNo) as trx'),DB::raw('COUNT(GuestCount) as GuestCount'),DB::raw('SUM(SalesAmount) as sales'))
      ->where('sales_header.CompanyID','=',$company_id)
      ->where('ChainId','=',$chainid)
      ->where('LocationId','=',$locationid)
      ->where('sales_header.VoucherStatus','LIKE',"%S%")
      ->where('VoucherDate','=',$date)
      ->get();
      $data_total1 = json_decode(json_encode($data_total), true);
      $i=0;
      $data_total2= array();
      foreach($data_total1 as $arr1){

         $data_total2[$i]['trx'] = strval(round($arr1['trx'],2));
         $data_total2[$i]['GuestCount'] = strval(round($arr1['GuestCount'],2));
         $data_total2[$i]['sales'] = strval(number_format($arr1['sales'],2));

      }
       $chain= DB::connection('mysql_source')
       ->table('chainaccess')
       ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
      ->select('chainaccess.ChainId','chain.ChainName')
      ->where('chainaccess.UserID','=',$user->user_id)
      ->where('chainaccess.CompanyId','=',$companyid)
      ->groupBy('chainaccess.ChainId')
      ->groupBy('chain.ChainName')
      ->get();
      DB::commit();
      DB::disconnect('mysql_source');
       if((count($location)>0) && (count($chain)>0)){
         echo json_encode(["status" => "success","data" => $data, "Orders"=>$datatwo,"Total"=>$data_total2,"Location"=>$location,"Chain"=>$chain]);
       }else{
          echo json_encode(["status" => "error", "message" => "Please enter valid PIN"]);
       }
    }
    else{
        echo json_encode(["status" => "error", "message" => "Authentication key mismatched..."]);
    }

   }

   public function userdata_testing(Request $request){

    $userid  =  $this->checkAuth($request);
    if(!$userid){
        return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']);
    }
    $user         = UserDetails::where('id','=',$userid->user_id)->first();
    $company_id   = $user->CompanyID;
    $verification = '';
    if($company_id != ''){
       $verification =  DB::table('company')
                            ->select('CompanyName','CompanyCode','conhostname','condbname','conusername','conpassword')
                            ->where('CompanyID','=',$company_id)
                            ->get();
      if(count($verification)<=0){
         echo json_encode(["status" => "false", "message" => "Authentication key mismatched..."]); exit();
      }
    }
    if($user) {
        $this->getConnectionDetails($company_id);

        DB::beginTransaction();
        DB::enableQueryLog();
        $user_data = DB::connection('mysql_source')
                            ->table('chainaccess')
                            ->leftjoin('locationaccess','locationaccess.UserId','=','chainaccess.UserID')
                            ->leftjoin('chain', 'chainaccess.ChainID', '=', 'chain.ChainId')
                            ->leftjoin('location', 'locationaccess.LocationID', '=', 'location.LocationId')
                            ->select('chainaccess.ChainID','locationaccess.LocationID','ChainName','LocationName','chainaccess.FullAccess')
                            ->where('chainaccess.UserId','=',$user->user_id)
                            ->get();
        $date  = date('Y-m-d').' 00:00:00';
        $date1 = date('Y-m-d');
        $user_type="Normal";
        if($user->CompanyID == 'KCL'){
            $date ='2020-01-15'.' 00:00:00';
            $date1 ="2020-01-15";
            $user_type ="Dummy";
        }
        $chainid   = $user_data[0]->ChainID;
        $companyid = $user->CompanyID;
        $fname     = $user->firstname;
        $lname     = $user->lastname;
        $user_name = $fname.' '.$lname;
        $image     = $user->image;
        if($image){
            $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if (!strpos($image,$url)) {
                $image     = url('/').'/'.$image;
            }
        }else{
            $image     = "";
        }
        $chainname =$user_data[0]->ChainName;
        if($user_data[0]->FullAccess =='1'){
            $location   = DB::connection('mysql_source')
                            ->table('location')
                            ->join('locationaccess', 'locationaccess.ChainId', '=', 'location.ChainId')
                            ->select('location.LocationId','LocationName')
                            ->where('location.ChainId','=',$chainid)
                            ->where('locationaccess.UserId','=',$user->user_id)
                            ->groupBy('location.LocationId')
                            ->groupBy('location.LocationName')
                            ->get();
        }else{
            $location = DB::connection('mysql_source')
                            ->table('locationaccess')
                            ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                            ->select('locationaccess.LocationId','LocationName')
                            ->where('locationaccess.ChainId','=',$chainid)
                            ->where('locationaccess.UserId','=',$user->user_id)
                            ->groupBy('locationaccess.LocationId')
                            ->groupBy('location.LocationName')
                            ->get();
        }
        $locationid   = $location[0]->LocationId;
        $locationname = $location[0]->LocationName;
        $data = array('user_name'=>$user_name,'image'=>$image,'UserType'=>$user_type,'CompanyName'=>$verification[0]->CompanyName,'CompanyID'=>$companyid,'ChainID'=>$chainid,'date'=>$date1,'ChainName'=>$chainname,'LocationID'=>$locationid,'LocationName'=>$locationname);
        $data_total=DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                        DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `CompanyID` = "' .$company_id. '"  AND `VoucherDate` = "' .$date. '"  AND `LocationID` = "' . $locationid .'"  AND `ChainID` = "'.$chainid.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                        DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                            ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                            ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                            ->on('sales_line.ChainID','=','sales_header.ChainID')
                            ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.ChainID','=', $chainid)
                        ->where('sales_line.CompanyID','=',$company_id)
                        ->where('sales_line.LocationID', $locationid)
                        ->where('sales_line.VoucherDate', $date)
                        ->where('sales_line.SalesStatus','S')
                        ->groupBy('sales_header.OrderType')
                        ->get();

        $data_total1 = json_decode(json_encode($data_total), true);
        $i=0;
        $transTotal = 0;
        $guestTotal = 0;
        $salesTotal = 0;
        $data_total2= array();
        $datatwo    = [];
        foreach($data_total1 as $arr1){
            $datatwo[$i]['OrderType']  = $arr1['Type'];
            $datatwo[$i]['trx']        = strval(round($arr1['Transaction'],2));
            $datatwo[$i]['GuestCount'] = strval(round($arr1['Guest'],2));
            $datatwo[$i]['Sales']      = strval(number_format($arr1['Sales'],2));
            $transTotal += $arr1['Transaction'];
            $guestTotal += $arr1['Guest'];
            $salesTotal += $arr1['Sales'];
            $i++;
        }
        $data_total2['trx']        = strval(round($transTotal,2));
        $data_total2['GuestCount'] = strval(round($guestTotal,2));
        $data_total2['sales']      = strval(number_format($salesTotal,2));

        $chain= DB::connection('mysql_source')
                    ->table('chainaccess')
                    ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
                    ->select('chainaccess.ChainId','chain.ChainName')
                    ->where('chainaccess.UserID','=',$user->user_id)
                    ->where('chainaccess.CompanyId','=',$companyid)
                    ->groupBy('chainaccess.ChainId')
                    ->groupBy('chain.ChainName')
                    ->get();
        DB::commit();
        DB::disconnect('mysql_source');
       if((count($location)>0) && (count($chain)>0)){
         echo json_encode(["status" => "success","data" => $data, "Orders"=>$datatwo,"Total"=>array($data_total2),"Location"=>$location,"Chain"=>$chain]);
       }else{
          echo json_encode(["status" => "error", "message" => "Please enter valid PIN"]);
       }
    }
    else{
        echo json_encode(["status" => "error", "message" => "Authentication key mismatched..."]);
    }

   }

   public function userdata_testing1(Request $request){

      $userid  =  $this->checkAuth($request);
      if(!$userid){
          return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']);
      }
      $user         = UserDetails::where('id','=',$userid->user_id)->first();
      $company_id   = $user->CompanyID;
      $verification = '';
      if($company_id != ''){
         $verification =  DB::table('company')
                              ->select('CompanyName','CompanyCode','conhostname','condbname','conusername','conpassword')
                              ->where('CompanyID','=',$company_id)
                              ->get();
        if(count($verification)<=0){
           echo json_encode(["status" => "false", "message" => "Authentication key mismatched..."]); exit();
        }
      }
      if($user) {
          $this->getConnectionDetails($company_id);
          DB::beginTransaction();
          DB::enableQueryLog();
          $user_data = DB::connection('mysql_source')
                              ->table('chainaccess')
                              ->leftjoin('locationaccess','locationaccess.UserId','=','chainaccess.UserID')
                              ->leftjoin('chain', 'chainaccess.ChainID', '=', 'chain.ChainId')
                              ->leftjoin('location', 'locationaccess.LocationID', '=', 'location.LocationId')
                              ->select('chainaccess.ChainID','locationaccess.LocationID','ChainName','LocationName','chainaccess.FullAccess')
                              ->where('chainaccess.UserId','=',$user->user_id)
                              ->get();
          $date  = date('Y-m-d').' 00:00:00';
          $date1 = date('Y-m-d');
          $user_type="Normal";
          if($user->CompanyID == 'KCL'){
              $date ='2020-01-15'.' 00:00:00';
              $date1 ="2020-01-15";
              $user_type ="Dummy";
          }
          $date = new DateTime('tomorrow');
          $date = $date->format('Y-m-d H:i:s');
          $chainid   = $user_data[0]->ChainID;
          $companyid = $user->CompanyID;
          $fname     = $user->firstname;
          $lname     = $user->lastname;
          $user_name = $fname.' '.$lname;
          $image     = $user->image;
          $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
          if (!strpos($image,$url)) {
              $image     = url('/').'/'.$image;
          }
          $chainname =$user_data[0]->ChainName;
          if($user_data[0]->FullAccess =='1'){
              $location   = DB::connection('mysql_source')
                              ->table('location')
                              ->join('locationaccess', 'locationaccess.ChainId', '=', 'location.ChainId')
                              ->select('location.LocationId','LocationName')
                              ->where('location.ChainId','=',$chainid)
                              ->where('locationaccess.UserId','=',$user->user_id)
                              ->groupBy('location.LocationId')
                              ->groupBy('location.LocationName')
                              ->get();
          }else{
              $location = DB::connection('mysql_source')
                              ->table('locationaccess')
                              ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                              ->select('locationaccess.LocationId','LocationName')
                              ->where('locationaccess.ChainId','=',$chainid)
                              ->where('locationaccess.UserId','=',$user->user_id)
                              ->groupBy('locationaccess.LocationId')
                              ->groupBy('location.LocationName')
                              ->get();
          }
          $locationid   = $location[0]->LocationId;
          $locationname = $location[0]->LocationName;
          $data = array('user_name'=>$user_name,'image'=>$image,'UserType'=>$user_type,'CompanyName'=>$verification[0]->CompanyName,'CompanyID'=>$companyid,'ChainID'=>$chainid,'date'=>$date1,'ChainName'=>$chainname,'LocationID'=>$locationid,'LocationName'=>$locationname);
          $data_total=DB::connection('mysql_source')
                          ->table('sales_line')
                          ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                          DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `CompanyID` = "' .$company_id. '"  AND `VoucherDate` = "' .$date. '"  AND `LocationID` = "' . $locationid .'"  AND `ChainID` = "'.$chainid.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                          DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                          ->leftJoin("sales_header",function($join){
                          $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                              ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                              ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                              ->on('sales_line.ChainID','=','sales_header.ChainID')
                              ->on('sales_line.LocationID','=','sales_header.LocationID');
                          })
                          ->where('sales_line.ChainID','=', $chainid)
                          ->where('sales_line.CompanyID','=',$company_id)
                          ->where('sales_line.LocationID', $locationid)
                          ->where('sales_line.VoucherDate', $date)
                          ->where('sales_line.SalesStatus','S')
                          ->groupBy('sales_header.OrderType')
                          ->get();

          $data_total1 = json_decode(json_encode($data_total), true);
          $i=0;
          $transTotal = 0;
          $guestTotal = 0;
          $salesTotal = 0;
          $data_total2= array();
          $datatwo    = [];
          foreach($data_total1 as $arr1){
              $datatwo[$i]['OrderType']  = $arr1['Type'];
              $datatwo[$i]['trx']        = strval(round($arr1['Transaction'],2));
              $datatwo[$i]['GuestCount'] = strval(round($arr1['Guest'],2));
              $datatwo[$i]['Sales']      = strval(number_format($arr1['Sales'],2));
              $transTotal += $arr1['Transaction'];
              $guestTotal += $arr1['Guest'];
              $salesTotal += $arr1['Sales'];
              $i++;
          }
          $data_total2['trx']        = strval(round($transTotal,2));
          $data_total2['GuestCount'] = strval(round($guestTotal,2));
          $data_total2['sales']      = strval(number_format($salesTotal,2));

          $chain= DB::connection('mysql_source')
                      ->table('chainaccess')
                      ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
                      ->select('chainaccess.ChainId','chain.ChainName')
                      ->where('chainaccess.UserID','=',$user->user_id)
                      ->where('chainaccess.CompanyId','=',$companyid)
                      ->groupBy('chainaccess.ChainId')
                      ->groupBy('chain.ChainName')
                      ->get();
          DB::commit();
          DB::disconnect('mysql_source');
         if((count($location)>0) && (count($chain)>0)){
           echo json_encode(["status" => "success","data" => $data, "Orders"=>$datatwo,"Total"=>array($data_total2),"Location"=>$location,"Chain"=>$chain]);
         }else{
            echo json_encode(["status" => "error", "message" => "Please enter valid PIN"]);
         }
      }
      else{
          echo json_encode(["status" => "error", "message" => "Authentication key mismatched..."]);
      }

     }

   public function userdataonchange(Request $request){

      $userid =$this->checkAuth($request);
      if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
      $user = UserDetails::where('id','=',$userid->user_id)->first();
      $date_only=date('Y-m-d',strtotime($request->date)).' 00:00:00';
      $chin_id = $request->chain;
      $location_id =$request->location;
      $this->getConnectionDetails($user->CompanyID);
      $data2 =DB::connection('mysql_source')
      ->table('sales_header')
      ->select('OrderType',DB::raw('COUNT(VoucherNo) as trx'),DB::raw('SUM(GuestCount) as GuestCount'),DB::raw('sum(SalesAmount) as sales'))
      ->where('ChainId','=',$chin_id)
      ->where('LocationId','=',$location_id)
      ->where('VoucherDate','=',$date_only)
      ->where('sales_header.VoucherStatus','LIKE',"%S%")
      ->groupBy('OrderType')
      ->get();
      \DB::enableQueryLog();
      if(count($data2)<=0){

        $islocation=DB::connection('mysql_source')
         ->table('location')
         ->select('LocationName')
         ->where('location.ChainId','=',$chin_id)
         ->where('location.LocationId','=',$location_id)
         ->get();
        if(count($islocation)<=0){
            $location=DB::connection('mysql_source')
            ->table('locationaccess')
            ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
            ->select('locationaccess.LocationId')
            ->where('locationaccess.ChainId','=',$chin_id)
            ->where('locationaccess.UserId','=',$user->user_id)
            ->groupBy('locationaccess.LocationId')
            ->get();
            $location_id =$location[0]->LocationId;
            $data2 =DB::connection('mysql_source')
           ->table('sales_header')
           ->select('OrderType',DB::raw('COUNT(VoucherNo) as trx'),DB::raw('SUM(GuestCount) as GuestCount'),DB::raw('sum(SalesAmount) as sales'))
           ->where('ChainId','=',$chin_id)
           ->where('LocationId','=',$location_id)
           ->where('VoucherDate','=',$date_only)
           ->where('sales_header.VoucherStatus','LIKE',"%S%")
           ->groupBy('OrderType')
           ->get();

        }
      }
      $query = DB::getQueryLog();
      //print_r($query); exit();
      $data2 = json_decode(json_encode($data2), true);

      $i=0;
      $datatwo=array();
      foreach($data2 as $arr){
        $datatwo[$i]['OrderType'] = $arr['OrderType'];
         $datatwo[$i]['trx'] = strval(round($arr['trx'],2));
         $datatwo[$i]['GuestCount'] = strval(round($arr['GuestCount'],2));
         $datatwo[$i]['Sales'] = strval(number_format($arr['sales'],2));
         $i++;
      }
     $data_total=DB::connection('mysql_source')
                    ->table('sales_header')
                    ->select(DB::raw('COUNT(VoucherNo) as trx'),DB::raw('SUM(GuestCount) as GuestCount'),DB::raw('sum(SalesAmount) as sales'))
                    ->where('ChainId','=',$chin_id)
                    ->where('LocationId','=',$location_id)
                    ->where('VoucherDate','=',$date_only)
                    ->where('sales_header.VoucherStatus','LIKE',"%S%")
                    ->get();
                    // ->table('sales_line')
                    // ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                    // DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date_only. '"  AND `LocationID` = "' . $location_id .'"  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                    // DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                    // ->leftJoin("sales_header",function($join){
                    // $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                    //     ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                    //     ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                    //     ->on('sales_line.ChainID','=','sales_header.ChainID')
                    //     ->on('sales_line.LocationID','=','sales_header.LocationID');
                    // })
                    // ->where('sales_line.ChainID','=', $chin_id)
                    // ->where('sales_line.LocationID', $location_id)
                    // ->where('sales_line.VoucherDate', $date_only)
                    // ->where('sales_line.SalesStatus','S')
                    // ->groupBy('sales_header.OrderType')
                    // ->get();

      $data_total1 = json_decode(json_encode($data_total), true);
      $i=0;
      $data_total2= array();
      foreach($data_total1 as $arr1){

        $data_total2[$i]['trx'] = strval(round($arr1['trx'],2));
        $data_total2[$i]['GuestCount'] = strval(round($arr1['GuestCount'],2));
        $data_total2[$i]['sales'] = strval(number_format($arr1['sales'],2));
     $i++;
     }

      // $location=DB::table('location')
      // ->select('LocationId')
      // ->where(array('ChainId'=>$chainid))
      // ->get();


      @$company=DB::connection('mysql_source')
      ->table('sales_header')
      ->select('CompanyId')
      ->where('ChainId','=',$chin_id)
      ->groupBy('CompanyId')
      ->get();
     $chain=DB::connection('mysql_source')
     ->table('chainaccess')
       ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
      ->select('chainaccess.ChainId','chain.ChainName')
      ->where('chainaccess.CompanyId','=', @$company[0]->CompanyId)
      ->groupBy('chainaccess.ChainId')
      ->groupBy('chain.ChainName')
      ->get();
      $activechain =DB::connection('mysql_source')
      ->table('chainaccess')
        ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
       ->select('chain.ChainName')
       ->where('chain.ChainId','=', $chin_id)
       ->groupBy('chain.ChainName')
       ->get();
      $location=DB::connection('mysql_source')
      ->table('locationaccess')
      ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
       ->select('locationaccess.LocationId','LocationName')
       ->where('locationaccess.ChainId','=',$chin_id)
       ->where('locationaccess.UserId','=',$user->user_id)
       ->groupBy('locationaccess.LocationId')
       ->groupBy('location.LocationName')
       ->get();

       $activelocation =DB::connection('mysql_source')
       ->table('locationaccess')
       ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
        ->select('LocationName')
        ->where('locationaccess.LocationId','=',$location_id)
        ->groupBy('locationaccess.LocationId')
        ->get();
       DB::commit();
       DB::disconnect('mysql_source');
       $udata =array('user_name'=>$user['user_name'],
       'image'=>$user['image'],
       'CompanyID'=>$user['CompanyID'],
       'ChainID'=>$chin_id,
       'ChainName'=>$activechain[0]->ChainName,
       'LocationID'=>$location_id,
       'LocationName'=>$activelocation[0]->LocationName);
      echo json_encode(["data"=>$udata,"Orders"=>$datatwo,"Total"=>$data_total2,"Location"=>$location,"Chain"=>$chain]);
    }

    public function userdataonchange_testing(Request $request){

        $userid  =  $this->checkAuth($request);
        if(!$userid){
            return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']);
        }
        $user        = UserDetails::where('id','=',$userid->user_id)->first();
        $date_only   = date('Y-m-d',strtotime($request->date)).' 00:00:00';
        $chin_id     = $request->chain;
        $location_id = $request->location;
        $this->getConnectionDetails($user->CompanyID);
        // $data2  =    DB::connection('mysql_source')
        //                 ->table('sales_header')
        //                 ->select('OrderType',DB::raw('COUNT(VoucherNo) as trx'),DB::raw('SUM(GuestCount) as GuestCount'),DB::raw('sum(SalesAmount) as sales'))
        //                 ->where('ChainId','=',$chin_id)
        //                 ->where('LocationId','=',$location_id)
        //                 ->where('VoucherDate','=',$date_only)
        //                 ->where('sales_header.VoucherStatus','LIKE',"%S%")
        //                 ->groupBy('OrderType')
        //                 ->get();
        // \DB::enableQueryLog();
        // if(count($data2)<=0){

        //     $islocation   =   DB::connection('mysql_source')
        //                             ->table('location')
        //                             ->select('LocationName')
        //                             ->where('location.ChainId','=',$chin_id)
        //                             ->where('location.LocationId','=',$location_id)
        //                             ->get();
        //     if(count($islocation)<=0){
        //         $location=DB::connection('mysql_source')
        //                     ->table('locationaccess')
        //                     ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
        //                     ->select('locationaccess.LocationId')
        //                     ->where('locationaccess.ChainId','=',$chin_id)
        //                     ->where('locationaccess.UserId','=',$user->user_id)
        //                     ->groupBy('locationaccess.LocationId')
        //                     ->get();
        //         $location_id =$location[0]->LocationId;
        //         $data2 =DB::connection('mysql_source')
        //                     ->table('sales_header')
        //                     ->select('OrderType',DB::raw('COUNT(VoucherNo) as trx'),DB::raw('SUM(GuestCount) as GuestCount'),DB::raw('sum(SalesAmount) as sales'))
        //                     ->where('ChainId','=',$chin_id)
        //                     ->where('LocationId','=',$location_id)
        //                     ->where('VoucherDate','=',$date_only)
        //                     ->where('sales_header.VoucherStatus','LIKE',"%S%")
        //                     ->groupBy('OrderType')
        //                     ->get();
        //     }
        // }
        // $query = DB::getQueryLog();
        // $data2 = json_decode(json_encode($data2), true);

        // $i=0;
        // $datatwo=array();
        // foreach($data2 as $arr){
        //     $datatwo[$i]['OrderType']  = $arr['OrderType'];
        //     $datatwo[$i]['trx']        = strval(round($arr['trx'],2));
        //     $datatwo[$i]['GuestCount'] = strval(round($arr['GuestCount'],2));
        //     $datatwo[$i]['Sales']      = strval(number_format($arr['sales'],2));
        //     $i++;
        // }
       $data_total=DB::connection('mysql_source')
                      ->table('sales_line')
                      ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                      DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date_only. '"  AND `LocationID` = "' . $location_id .'"  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                      DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                      ->leftJoin("sales_header",function($join){
                      $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                          ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                          ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                          ->on('sales_line.ChainID','=','sales_header.ChainID')
                          ->on('sales_line.LocationID','=','sales_header.LocationID');
                      })
                      ->where('sales_line.ChainID','=', $chin_id)
                      ->where('sales_line.LocationID', $location_id)
                      ->where('sales_line.VoucherDate', $date_only)
                      ->where('sales_line.SalesStatus','S')
                      ->groupBy('sales_header.OrderType')
                      ->get();

        $data_total1 = json_decode(json_encode($data_total), true);
        $i=0;
        $transTotal = 0;
        $guestTotal = 0;
        $salesTotal = 0;
        $data_total2= array();
        $datatwo    = [];
        foreach($data_total1 as $arr1){
            $datatwo[$i]['OrderType']  = $arr1['Type'];
            $datatwo[$i]['trx']        = strval(round($arr1['Transaction'],2));
            $datatwo[$i]['GuestCount'] = strval(round($arr1['Guest'],2));
            $datatwo[$i]['Sales']      = strval(number_format($arr1['Sales'],2));
            $transTotal += $arr1['Transaction'];
            $guestTotal += $arr1['Guest'];
            $salesTotal += $arr1['Sales'];
            $i++;
        }
        $data_total2['trx']        = strval(round($transTotal,2));
        $data_total2['GuestCount'] = strval(round($guestTotal,2));
        $data_total2['sales']      = strval(number_format($salesTotal,2));

        @$company   =   DB::connection('mysql_source')
                            ->table('sales_header')
                            ->select('CompanyId')
                            ->where('ChainId','=',$chin_id)
                            ->groupBy('CompanyId')
                            ->get();
        $chain      =   DB::connection('mysql_source')
                            ->table('chainaccess')
                            ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
                            ->select('chainaccess.ChainId','chain.ChainName')
                            ->where('chainaccess.CompanyId','=', @$company[0]->CompanyId)
                            ->groupBy('chainaccess.ChainId')
                            ->groupBy('chain.ChainName')
                            ->get();
        $activechain =  DB::connection('mysql_source')
                            ->table('chainaccess')
                            ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
                            ->select('chain.ChainName')
                            ->where('chain.ChainId','=', $chin_id)
                            ->groupBy('chain.ChainName')
                            ->get();
        $location   =   DB::connection('mysql_source')
                            ->table('locationaccess')
                            ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                            ->select('locationaccess.LocationId','LocationName')
                            ->where('locationaccess.ChainId','=',$chin_id)
                            ->where('locationaccess.UserId','=',$user->user_id)
                            ->groupBy('locationaccess.LocationId')
                            ->groupBy('location.LocationName')
                            ->get();

         $activelocation =  DB::connection('mysql_source')
                                ->table('locationaccess')
                                ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                                ->select('LocationName')
                                ->where('locationaccess.LocationId','=',$location_id)
                                ->groupBy('locationaccess.LocationId')
                                ->get();
         DB::commit();
         DB::disconnect('mysql_source');
         $udata =array(
            'user_name'=>$user['user_name'],
            'image'=>$user['image'],
            'CompanyID'=>$user['CompanyID'],
            'ChainID'=>$chin_id,
            'ChainName'=>$activechain[0]->ChainName,
            'LocationID'=>$location_id,
            'LocationName'=>$activelocation[0]->LocationName
        );
        echo json_encode(["data"=>$udata,"Orders"=>$datatwo,"Total"=>array($data_total2),"Location"=>$location,"Chain"=>$chain]);
    }

    public function all_data(Request $request){

      $userid =$this->checkAuth($request);
      if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
      $user = UserDetails::where('id','=',$userid->user_id)->first();
      $date_only=date('Y-m-d',strtotime($request->date)).' 00:00:00';
      $chin_id =$request->chain;
      @$location_id =$request->location;
      $selecctedcategory =$request->category;
      if($selecctedcategory=='') {
         $selecctedcategory ='Category1';
      }
      $this->getConnectionDetails($user->CompanyID);
      DB::beginTransaction();
      DB::enableQueryLog();
      $company = @$user->CompanyID;
      $username =@$user->username;
      $userdata =array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);
     // $company=$company_id[0]['CompanyID'];

     // -----------------order type----------------------
     $data_ordertype = DB::connection('mysql_source')
     ->table('sales_header')
     ->select('OrderType',DB::raw('SUM(SalesAmount) as Sales'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(VoucherNo) as Transaction'))
      ->where('LocationID','=',$location_id)
     ->where('ChainID','=',$chin_id)
     ->where('CompanyID','=',$company)
     ->where('VoucherDate','=',$date_only)
     ->where('sales_header.VoucherStatus','LIKE',"%S%")
     ->groupBy('OrderType')
     ->get();
     $data_ordertype1 = json_decode(json_encode($data_ordertype), true);
    $i=0;
    $data_ordertype=array();
     $data_ordertype_total['Sales']=0;
     $trxtotal ='0';
     $guesttotal ='0';
    foreach($data_ordertype1 as $arr){
       $data_ordertype[$i]['OrderType']   = $arr['OrderType'];
       $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
       $data_ordertype[$i]['Guest']       = strval(round($arr['Guest'],2));
       $data_ordertype[$i]['Sales']       = strval(number_format($arr['Sales'],2));
       $data_ordertype_total['Sales']     = $data_ordertype_total['Sales']+strval(round($arr['Sales'],2));
       $trxtotal                          =$trxtotal +$data_ordertype[$i]['Transaction'];
       $guesttotal                        =$guesttotal +$data_ordertype[$i]['Guest'];
       $i++;
    }
   $data_ordertype_total['Sales'] = strval(number_format($data_ordertype_total['Sales'],2));
   $data_ordertype_total['Trx'] = strval(round($trxtotal,2));
   $data_ordertype_total['Guest'] = strval(round($guesttotal,2));
  // ---------------Category 1-----------------------
   $data_category1 = DB::connection('mysql_source')
   ->table('sales_line')
   ->select('CategoryDes1',DB::raw('SUM(SellingPrice*Qty) as sales'),DB::raw('round(SUM(Qty),3) as Guest'))
   ->where('LocationID','=',$location_id)
   ->where('ChainID','=',$chin_id)
   ->where('CompanyID','=',$company)
   ->where('VoucherDate','=',$date_only)
   ->where('SalesStatus','LIKE','%S%')
   ->groupBy('CategoryDes1')
   ->get();
   $data_category11 = json_decode(json_encode($data_category1), true);
   $guests =0;
    $sales=0;
   $i=0;
   $data_category1 = array();
   foreach($data_category11 as $arr){
      $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
      $data_category1[$i]['Guests']       = strval(round($arr['Guest'],2));
      $data_category1[$i]['sales']        = strval(number_format($arr['sales'],2));
      $guests                             = $guests+strval($arr['Guest']);
      $sales                              = $sales +strval(round($arr['sales'],2));
      $i++;
   }
   $data_category1_total['Guests']     = strval($guests);
   $data_category1_total['sales']      = strval(number_format($sales,2));
   // --------------Payment---------------------------

   $payment_type =DB::connection('mysql_source')
   ->table('sales_payment')
   ->select('PaymentType')
   ->where('CompanyID','=',$company)
   ->where('LocationID','=',$location_id)
   ->where('ChainID','=',$chin_id)
   ->where('VoucherDate','=',$date_only)
   ->groupBy('PaymentType')
   ->get();
  // $payment_type=array("CASH","VISA CARD",'MASTER CARD','STAFF DISCOUNT 30','ONLINE PAYMENTS');
   $data_paymenttype =array();
    $i=0;
    $cartype ='0';
  foreach ($payment_type as $value) {

      @$query_paymenttype = DB::connection('mysql_source')
      ->table('sales_payment')
      ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
      ->where('CompanyID','=',$company)
      ->where('LocationID','=',$location_id)
      ->where('ChainID','=',$chin_id)
      ->where('VoucherDate','=',$date_only)
      ->where('PaymentType','=',$value->PaymentType)
      ->groupBy('PaymentType')
      ->get();


      @$typevalue = DB::connection('mysql_source')
      ->table('payment_types')
      ->select('*')
      ->where('CompanyID','=',$company)
      ->where('ChainID','=',$chin_id)
      ->where('PaymentType','=',$value->PaymentType)
      ->get();
      $quer = @$query_paymenttype[0]->Amount;
      $tcount = @$query_paymenttype[0]->Count;
      $id=2;
      if(($value->PaymentType == 'VISA CARD') || ($value->PaymentType == 'MASTER CARD') || ($value->PaymentType == 'ONLINE PAYMENTS')){
          $id=1;
      }else if($value->PaymentType == 'CASH'){
          $id=2;
      }else if($value->PaymentType == 'STAFF DISCOUNT 30'){
          $id=3;
      }else if($value->PaymentType == 'CREDIT'){
         $id=4;
      }else if(@$typevalue[0]->IsCreditCard=='0'){
        $id=2;
      }else if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
        $id=3;
      }
      else if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsCCard=='1')){
        $id=4;
      }else{
          $id =5;
      }
      $data_paymenttype[]=array("Group Id"=>$id,"Payment Type"=>$value->PaymentType,"Amount"=>strval(round($quer,2)),"Count"=>strval(round($tcount,2)));
      $i++;
    }
    // print_r($data_paymenttype); exit();
    $visa_amount['Visa Amount']=strval(number_format(@$data_paymenttype[1]['Amount'],2));
    $master_amount['Master Amount']=strval(number_format(@$data_paymenttype[2]['Amount'],2));
    $data_visamastertotal[]=array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));

   // ---------------Category 2-----------------------

   $data_category2 = DB::connection('mysql_source')
   ->table('sales_line')
   ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
   ->where('LocationID','=',$location_id)
   ->where('ChainID','=',$chin_id)
   ->where('CompanyID','=',$company)
   ->where('VoucherDate','=',$date_only)
   ->where('SalesStatus','LIKE','%S%')
   ->groupBy('CategoryDes2')
   ->get();
   $data_category12 = json_decode(json_encode($data_category2), true);
   $data_category2_total['Quantity'] =0;
   $data_category2_total['price'] = 0;
   $quantity =0;
   $price=0;
   $i=0;
   $data_category2 = array();
   foreach($data_category12 as $arr){
      $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
      $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
      $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
      $quantity = $quantity+strval(round($arr['Quantity'],2));
      $price = $price+strval(round($arr['sales'],2));
      $i++;
   }
    $data_category2_total['Quantity'] = strval($quantity);
    $data_category2_total['price'] =strval(number_format($price,2));

  // -----------------Sales By Items Category 1--------------------------

 if($selecctedcategory =='Category1'){
    $categorydes1_array = DB::connection('mysql_source')
      ->table('sales_line')
    ->select('CategoryDes1')
    ->where('LocationID','=',$location_id)
    ->where('ChainID','=',$chin_id)
    ->where('CompanyID','=',$company)
    ->where('VoucherDate','=',$date_only)
    ->distinct('CategoryDesc1')
    ->get()->toArray();
    $i=0;
    foreach($categorydes1_array as $key => $value) {
       $query_items  = DB::connection('mysql_source')
       ->table('sales_line')
       ->select('ItemDesc as item',DB::raw('SUM(Qty) as quantity'),DB::raw('SUM(SellingPrice*Qty) as price'))
       ->where('CompanyID','=',$company)
       ->where('LocationID','=',$location_id)
       ->where('ChainID','=',$chin_id)
       ->where('VoucherDate','=',$date_only)
       ->where('SalesStatus','LIKE','%S%')
       ->where('CategoryDes1','=',$value->CategoryDes1)
       ->groupBy('ItemDesc')
       ->get();
       $query_items1 = json_decode(json_encode($query_items), true);
    $i=0;
    $query_items = array();
    $query_items_total['qty'] ='0';
    $query_items_total['total'] ='0';
    $total =0;
    foreach($query_items1 as $arr){
       $query_items[$i]['item'] = $arr['item'];
       $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
       $query_items[$i]['price'] = strval(number_format($arr['price'],2));
       $query_items_total['qty'] =$query_items_total['qty']+$arr['quantity'];
       $query_items_total['total'] =$query_items_total['total']+$arr['price'];
       $i++;
    }
       $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);
       $i=$i+1;
    }

    $data_payment_total = DB::connection('mysql_source')
    ->table('sales_line')
    ->select(DB::raw('SUM(Qty) as Quantity'),DB::raw('SUM(SellingPrice*Qty) as Price'))
    ->where('CompanyID','=',$company)
    ->where('LocationID','=',$location_id)
    ->where('ChainID','=',$chin_id)
    ->where('VoucherDate','=',$date_only)
    ->get();

    $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
    $data_payment_total = array();
    $i=0;
    foreach($data_payment_total1 as $arr){
       $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
       $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
       $i++;
    }
 }
  // -----------------Sales By Items Category 2--------------------------
  if($selecctedcategory=='Category2'){
    $categorydes2_array = DB::connection('mysql_source')
    ->table('sales_line')
    ->select('CategoryDes2')
    ->where('LocationID','=',$location_id)
    ->where('ChainID','=',$chin_id)
    ->where('CompanyID','=',$company)
    ->where('VoucherDate','=',$date_only)
    ->distinct('CategoryDesc2')
    ->get()->toArray();
    $i=0;
    foreach($categorydes2_array as $key => $value) {
       $query_items =DB::connection('mysql_source')
       ->table('sales_line')
       ->select('ItemDesc as item',DB::raw('SUM(Qty) as quantity'),DB::raw('SUM(SellingPrice*Qty) as price'))
       ->where('CompanyID','=',$company)
       ->where('LocationID','=',$location_id)
       ->where('ChainID','=',$chin_id)
       ->where('VoucherDate','=',$date_only)
       ->where('SalesStatus','LIKE','%S%')
       ->where('CategoryDes2','=',$value->CategoryDes2)
       ->groupBy('ItemDesc')
       ->get();
       $query_items23 = json_decode(json_encode($query_items), true);
       $i=0;
       $query_items2 = array();
       $query_items_total2['qty'] ='0';
       $query_items_total2['total'] ='0';
       $total =0;
       foreach($query_items23 as $arr){
          $query_items2[$i]['item'] = $arr['item'];
          $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
          $query_items2[$i]['price'] = strval(round($arr['price'],2));
          $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
          $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
          $i++;
       }
       $items2[]=array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);
       $i=$i+1;
    }

    $data_payment_total2 = DB::connection('mysql_source')
    ->table('sales_line')
    ->select(DB::raw('SUM(Qty) as Quantity'),DB::raw('SUM(SellingPrice*Qty) as Price'))
    ->where('CompanyID','=',$company)
    ->where('LocationID','=',$location_id)
    ->where('ChainID','=',$chin_id)
    ->where('VoucherDate','=',$date_only)
    ->get();

    $data_payment_total21 = json_decode(json_encode($data_payment_total2), true);
    $data_payment_total2 = array();
    $i=0;
    foreach($data_payment_total21 as $arr){
       $data_payment_total2['Quantity'] = strval(round($arr['Quantity'],2));
       $data_payment_total2['Price'] = strval(number_format($arr['Price'],2));
       $i++;
    }
  }

//---------------------------------Hourly Sale---------------------------

$query_items =DB::connection('mysql_source')
->table('sales_header')
->select(DB::raw('hour( StartTime ) as Hour'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(VoucherNo) as Transaction'),DB::raw('SUM(SalesAmount) as Sales'))
->where('CompanyID','=',$company)
->where('LocationID','=',$location_id)
->where('VoucherDate','=',$date_only)
->where('VoucherStatus','LIKE','%S%')
->where('ChainID','=',$chin_id)
->groupBy(DB::raw('hour(StartTime)'),DB::raw('day(StartTime)'))
->get();
$i=0;
$data_hourly_sale=array();
$hourly_sale ='0';
$guesttotal  ='0';
$trxtotal    ='0';
$data_hourly_sale_total =array();
foreach($query_items as $data){

  if($data->Hour  < 10)
   {
    $data->Hour = '0'.$data->Hour;
   }
   $to =$data->Hour+1;
   if($to <10 )
   {
    $to ='0'.$to;
   }
  $data_hourly_sale[$i]['Hours']=strval($data->Hour.':00 to '.$to.':00');
  $data_hourly_sale[$i]['Guest']=strval(round($data->Guest,2));
  $data_hourly_sale[$i]['Transcation']=strval(round($data->Transaction,2));
  $data_hourly_sale[$i]['Sales']=strval(number_format($data->Sales,2));
  $hourly_sale                  = $hourly_sale + round($data->Sales,2);
  $guesttotal                  = $guesttotal + round($data->Guest,2);
  $trxtotal                  = $trxtotal + round($data->Transaction,2);
  $i++;
}
$data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
$data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
$data_hourly_sale_total['trx']   = strval(round($trxtotal,2));
//------------------------------------Credit Customer------------------------------------------
   $data_credit_customer =DB::connection('mysql_source')
   ->table('sales_payment')
  ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
  ->where('sales_payment.CompanyID','=',$company)
  ->where('sales_payment.LocationID','=',$location_id)
  ->where('sales_payment.ChainID','=',$chin_id)
  ->where('sales_payment.VoucherDate','=',$date_only)
  ->where('sales_payment.CustomerName','!=','')
  ->groupBy('CustomerName','CardNo')
  ->get();
//------------------------Sales Summary----------------------------------------------------------

//------------------------Total Sales----------------------------------------------------------
$data_total_sales = DB::connection('mysql_source')
->table('sales_line')
->select(DB::raw('round(SUM(Qty*SellingPrice),3) as TotalSales'))
->where('CompanyID','=',$company)
->where('ChainID','=',$chin_id)
->where('LocationID','=',$location_id)
->where('VoucherDate','=',$date_only)
->where('SalesStatus','LIKE','%S%')
->get();
$data_total_sales1 = json_decode(json_encode($data_total_sales), true);
  $data_total_sales = array();
  $i=0;
  foreach($data_total_sales1 as $arr){
     $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
     $i++;
  }
//------------------------Delivery Charges----------------------------------------------------------
$data_delivery_charge = DB::connection('mysql_source')
->table('sales_header')
->select(DB::raw('SUM(ServiceTaxAmount+ServiceTaxBaseAmount) as DeliveryCharges'))
->where('CompanyID','=',$company)
->where('ChainID','=',$chin_id)
->where('LocationID','=',$location_id)
->where('VoucherDate','=',$date_only)
->where('VoucherStatus','LIKE','%S%')
->where('OrderType','=','DL')
->get();
$data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
  $data_delivery_charge = array();
  $i=0;
  foreach($data_delivery_charge1 as $arr){
     $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
     $i++;
  }
//------------------------Service Charges----------------------------------------------------------
$data_service_charge = DB::connection('mysql_source')
->table('sales_header')
->select(DB::raw('SUM(ServiceTaxAmount+ServiceTaxBaseAmount) as ServiceCharges'))
->where('CompanyID','=',$company)
->where('ChainID','=',$chin_id)
->where('LocationID','=',$location_id)
->where('VoucherDate','=',$date_only)
->where('VoucherStatus','LIKE','%S%')
->where('OrderType','!=','DL')
->get();
$data_service_charge1 = json_decode(json_encode($data_service_charge), true);
$data_service_charge = array();
$i=0;
foreach($data_service_charge1 as $arr){
   $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
   $i++;
}


//------------------------Gross Sales----------------------------------------------------------
$data_gross_sales['Gross Sale']=strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));
//------------------------Staff Meal----------------------------------------------------------
$data_staff_meal =DB::connection('mysql_source')
->table('sales_line')
->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('SUM(Qty*SellingPrice) as Amount'))
->where('sales_line.CompanyID','=',$company)
->where('sales_line.ChainID','=',$chin_id)
->where('sales_line.LocationID','=',$location_id)
->where('sales_line.VoucherDate','=',$date_only)
 ->where('sales_header.OrderType','=','SM')
->leftJoin("sales_header",function($join){
  $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
      ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
      ->on('sales_line.CompanyID','=','sales_header.CompanyID')
      ->on('sales_line.ChainID','=','sales_header.ChainID')
      ->on('sales_line.LocationID','=','sales_header.LocationID');
   })
   ->where('sales_line.CompanyID','=',$company)
->where('sales_line.ChainID','=',$chin_id)
->where('sales_line.LocationID','=',$location_id)
->where('sales_line.VoucherDate','=',$date_only)
 ->where('sales_header.OrderType','=','SM')
->get();
$data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);

$data_staff_meal = array();
$i=0;
foreach($data_staff_meal1 as $arr){
   $data_staff_meal['Count'] = strval(round($arr['Count'],2));
   if($data_staff_meal['Count']>1){
       $data_staff_meal['Count']='1';
   }
   $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
   $i++;
}

//------------------------Discount----------------------------------------------------------
\DB::enableQueryLog();
$data_discount = DB::connection('mysql_source')
->table('sales_payment')
->select(DB::raw('SUM(Amount) as Discount'))
->leftJoin("sales_header",function($join){
$join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
    ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
    ->on('sales_header.CompanyID','=','sales_payment.CompanyID')
    ->on('sales_header.ChainID','=','sales_payment.ChainID')
    ->on('sales_header.LocationID','=','sales_payment.LocationID');
})
->leftJoin("payment_types",function($join1){
$join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
    ->on('sales_payment.ChainID','=','payment_types.ChainID')
    ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
})
->where('sales_header.CompanyID','=',$company)
->where('sales_header.ChainID','=',$chin_id)
->where('sales_header.LocationID','=',$location_id)
->where('sales_header.VoucherDate','=',$date_only)
->where('sales_header.VoucherStatus','LIKE','%S%')
->where('sales_header.OrderType','!=','SM')
->where('payment_types.IsDiscountCard','=','1')
->get();
$query = \DB::getQueryLog();
//print_r($query); exit();
$data_discount1 = json_decode(json_encode($data_discount), true);
$data_discount = array();
$i=0;
foreach($data_discount1 as $arr){
   $data_discount['Discount'] = strval(round($arr['Discount'],2));
   $i++;
}

//------------------------VAT----------------------------------------------------------
$data_vat = DB::connection('mysql_source')
->table('sales_header')
->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
->where('CompanyID','=',$company)
->where('ChainID','=',$chin_id)
->where('VoucherDate','=',$date_only)
->where('LocationId','=',$location_id)
->where('VoucherStatus','LIKE','%S%')
->get();
$data_vat1 = json_decode(json_encode($data_vat), true);
$data_vat = array();
$i=0;
foreach($data_vat1 as $arr){
   $data_vat['VAT'] = strval(round($arr['VAT'],2));
   $i++;
}

//------------------------Net Sales----------------------------------------------------------
$data_net_sales['Net Sales'] = strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));


//--------------------------------------Taxable Value--------------------------------------------
$data_tax =array();
 $data_tax['TaxableValue'] = strval(round($data_net_sales['Net Sales'],2)-$data_vat['VAT']);
//------------------------PayOut----------------------------------------------------------
$data_payout['Payout']='0';
//------------------------Credit Sales----------------------------------------------------------
$data_creditsales = DB::connection('mysql_source')
->table('sales_payment')
->select(DB::raw('SUM(Amount) as CreditSales'))
->where('sales_header.CompanyID','=',$company)
->where('sales_header.ChainID','=',$chin_id)
->where('sales_header.LocationID','=',$location_id)
->where('sales_header.VoucherDate','=',$date_only)
->where('sales_header.VoucherStatus','LIKE','%S%')
->leftJoin("sales_header",function($join){
$join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
    ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
    ->on('sales_payment.CompanyID','=','sales_header.CompanyID')
    ->on('sales_payment.ChainID','=','sales_header.ChainID')
    ->on('sales_payment.LocationID','=','sales_header.LocationID');
})
->leftJoin("payment_types",function($join1){
$join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
    ->on('sales_payment.ChainID','=','payment_types.ChainID')
    ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
})
->where('payment_types.IsCreditCard','=','1')
->where('payment_types.IsDiscountCard','!=','1')
->where('sales_header.OrderType','!=','SM')
->get();
$data_creditsales1 = json_decode(json_encode($data_creditsales), true);
$data_creditsales = array();
$i=0;
foreach($data_creditsales1 as $arr){
   $data_creditsales['CreditSales'] = strval(round($arr['CreditSales'],2));
   $i++;
}
$data_locationlist =DB::connection('mysql_source')
->table('sales_header')
->select('LocationID')
->groupBy('LocationID')
->get();

//------------------------Actual Banking----------------------------------------------------------
$data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

//------------------------Voids----------------------------------------------------------
//item order item
$data_items_order = DB::connection('mysql_source')
->table('sales_line')
->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
->leftJoin("sales_header",function($join){
$join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
    ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
    ->on('sales_header.CompanyID','=','sales_line.CompanyID')
    ->on('sales_header.ChainID','=','sales_line.ChainID')
    ->on('sales_header.LocationID','=','sales_line.LocationID');
})
->where('sales_line.CompanyID','=',$company)
->where('sales_line.ChainID','=',$chin_id)
->where('sales_line.LocationID',$location_id)
->where('sales_line.VoucherDate',$date_only)
->where('sales_header.VoucherStatus','LIKE','%V%')
->get();

$data_items_order1 = json_decode(json_encode($data_items_order), true);
$data_items_order = array();
$i=0;
foreach($data_items_order1 as $arr){
$data_items_order['Count'] = strval(round($arr['Count'],2));
$data_items_order['Amount'] = strval(round($arr['Amount'],2));
$i++;
}
//item  total
$data_void_items_total =DB::connection('mysql_source')
->table('sales_line')
->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
->leftJoin("sales_header",function($join){
$join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
    ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
    ->on('sales_header.CompanyID','=','sales_line.CompanyID')
    ->on('sales_header.ChainID','=','sales_line.ChainID')
    ->on('sales_header.LocationID','=','sales_line.LocationID');
})
->where('sales_line.CompanyID','=',$company)
->where('sales_line.ChainID','=',$chin_id)
->where('sales_line.LocationID',$location_id)
->where('sales_line.VoucherDate',$date_only)
->where('sales_line.SalesStatus','LIKE','%V%')
->get();
$data_void_items_total1 = json_decode(json_encode($data_void_items_total), true);
$data_void_items_total = array();
$i=0;
foreach($data_void_items_total1 as $arr){
$data_void_items_total['Count'] = strval(round($arr['Count'],2));
$data_void_items_total['Amount'] = strval(round($arr['Amount'],2));
$i++;
}
//item void
$data_items_void = array();
$data_items_void['Amount'] = strval(round($data_void_items_total['Amount']-$data_items_order['Amount'],3));
if($data_items_void['Amount']>0){
   $data_items_void['Count'] ="1";
}else{
   $data_items_void['Count'] ="0";
}
//------------------------Total of Voids And Staff Meal----------------------------------------------------------
$data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount']+@$data_void_items_total['Amount'],3));




//----------------------------------------------Chain---------------------------------------------------------------
$chain =DB::connection('mysql_source')
->table('chainaccess')
->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
->select('chainaccess.ChainId','chain.ChainName')
->where('chainaccess.CompanyId','=',$company)
->groupBy('chainaccess.ChainId')
->groupBy('chain.ChainName')
->get();
//----------------------------------------------Location---------------------------------------------------------------
$location =DB::connection('mysql_source')
->table('locationaccess')
->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
->select('locationaccess.LocationId','LocationName')
->where('locationaccess.ChainId','=',$chin_id)
->groupBy('locationaccess.LocationId')
->groupBy('location.LocationName')
->get();
DB::commit();
DB::disconnect('mysql_source');
if($selecctedcategory=='Category1'){
    echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>@$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,'Location'=>$location,"Chain"=>$chain]);
 }else if($selecctedcategory=='Category2'){
    echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>@$items2,"Items_total"=>$data_payment_total2,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,'Location'=>$location,"Chain"=>$chain]);
 }
}

public function all_data_testing(Request $request){

    $userid = $this->checkAuth($request);
    if(!$userid){
       return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']);
    }
    $user         = UserDetails::where('id','=',$userid->user_id)->first();
    $date_only    = date('Y-m-d',strtotime($request->date)).' 00:00:00';
    $chin_id      = $request->chain;
    @$location_id = $request->location;
    $selecctedcategory =$request->category;
    if($selecctedcategory=='') {
       $selecctedcategory ='Category1';
    }
    $this->getConnectionDetails($user->CompanyID);
    DB::beginTransaction();
    DB::enableQueryLog();
    $company  = @$user->CompanyID;
    $username = @$user->username;
    $userdata =array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);
   // $company=$company_id[0]['CompanyID'];

   // -----------------order type----------------------
   $data_ordertype = DB::connection('mysql_source')
                        // ->table('sales_header')
                        // ->select('OrderType',DB::raw('SUM(SalesAmount) as Sales'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(VoucherNo) as Transaction'))
                        // ->where('LocationID','=',$location_id)
                        // ->where('ChainID','=',$chin_id)
                        // ->where('CompanyID','=',$company)
                        // ->where('VoucherDate','=',$date_only)
                        // ->where('sales_header.VoucherStatus','LIKE',"%S%")
                        // ->groupBy('OrderType')
                        // ->get();
                        ->table('sales_line')
                        ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                        DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date_only. '" AND `CompanyID`= "'.$company.'" AND `LocationID` = "' . $location_id .'"  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                        DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                            ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                            ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                            ->on('sales_line.ChainID','=','sales_header.ChainID')
                            ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.CompanyID','=', $company)
                        ->where('sales_line.ChainID','=', $chin_id)
                        ->where('sales_line.LocationID', $location_id)
                        ->where('sales_line.VoucherDate', $date_only)
                        ->where('sales_line.SalesStatus','S')
                        ->groupBy('sales_header.OrderType')
                        ->get();
   $data_ordertype1 = json_decode(json_encode($data_ordertype), true);
   $i = 0;
   $data_ordertype = array();
   $data_ordertype_total['Sales'] = 0;
   $trxtotal   = '0';
   $guesttotal = '0';
  foreach($data_ordertype1 as $arr){

     $data_ordertype[$i]['OrderType']   = $arr['Type'];
     $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
     $data_ordertype[$i]['Guest']       = strval(round($arr['Guest'],2));
     $data_ordertype[$i]['Sales']       = strval(number_format($arr['Sales'],2));
     $data_ordertype_total['Sales']     = $data_ordertype_total['Sales']+strval(round($arr['Sales'],2));
     $trxtotal                          = $trxtotal +$data_ordertype[$i]['Transaction'];
     $guesttotal                        = $guesttotal +$data_ordertype[$i]['Guest'];
     $i++;
  }
   $data_ordertype_total['Sales'] = strval(number_format($data_ordertype_total['Sales'],2));
   $data_ordertype_total['Trx']   = strval(round($trxtotal,2));
   $data_ordertype_total['Guest'] = strval(round($guesttotal,2));

   // ---------------Category 1-----------------------
   $data_category1 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes1',DB::raw('SUM(SellingPrice*Qty) as sales'),DB::raw('round(SUM(Qty),3) as Guest'))
                        ->where('LocationID','=',$location_id)
                        ->where('ChainID','=',$chin_id)
                        ->where('CompanyID','=',$company)
                        ->where('VoucherDate','=',$date_only)
                        ->where('SalesStatus','LIKE','%S%')
                        ->groupBy('CategoryDes1')
                        ->get();
   $data_category11 = json_decode(json_encode($data_category1), true);
   $guests =0;
   $sales=0;
   $i=0;
   $data_category1 = array();
   foreach($data_category11 as $arr){

      $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
      $data_category1[$i]['Guests']       = strval(round($arr['Guest'],2));
      $data_category1[$i]['sales']        = strval(number_format($arr['sales'],2));
      $guests                             = $guests+strval($arr['Guest']);
      $sales                              = $sales +strval(round($arr['sales'],2));
      $i++;
   }
   $data_category1_total['Guests']  = strval($guests);
   $data_category1_total['sales']   = strval(number_format($sales,2));

   // --------------Payment---------------------------
   $payment_type = DB::connection('mysql_source')
                     ->table('sales_payment')
                     ->select('PaymentType')
                     ->where('CompanyID','=',$company)
                     ->where('LocationID','=',$location_id)
                     ->where('ChainID','=',$chin_id)
                     ->where('VoucherDate','=',$date_only)
                     ->groupBy('PaymentType')
                     ->get();
   // $payment_type=array("CASH","VISA CARD",'MASTER CARD','STAFF DISCOUNT 30','ONLINE PAYMENTS');
   $data_paymenttype =array();
   $i=0;
   $cartype ='0';
   $totalDiscount = 0;
   $creditSaleTotal = 0;
   foreach ($payment_type as $value) {

      @$query_paymenttype = DB::connection('mysql_source')
                              ->table('sales_payment')
                              ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('PaymentType','=',$value->PaymentType)
                              ->groupBy('PaymentType')
                              ->get();
      @$typevalue = DB::connection('mysql_source')
                        ->table('payment_types')
                        ->select('*')
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->where('PaymentType','=',$value->PaymentType)
                        ->get();
      $quer   = @$query_paymenttype[0]->Amount;
      $tcount = @$query_paymenttype[0]->Count;

      /*****************************Payment Types *************************/
      if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
         $id = 1;
         $totalDiscount += $quer;
      }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCCard == '1')){
         $id = 2;
         $creditSaleTotal += $quer;
      }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCustCredit == '1')){
         $id = 3;
         $creditSaleTotal += $quer;
      }else if((@$typevalue[0]->IsStaffMeal== '1')){
         $id = 4;
      }else{
         $id = 5;
      }

      $data_paymenttype[] = array("Group Id"=>$id, "Payment Type"=>$value->PaymentType, "Amount"=>strval(round($quer,2)), "Count"=>strval(round($tcount,2)));
      $i++;
   }
   $visa_amount['Visa Amount']      = strval(number_format(@$data_paymenttype[1]['Amount'],2));
   $master_amount['Master Amount']  = strval(number_format(@$data_paymenttype[2]['Amount'],2));
   $data_visamastertotal[]          = array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));

   // ---------------Category 2-----------------------

   $data_category2 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                        ->where('LocationID','=',$location_id)
                        ->where('ChainID','=',$chin_id)
                        ->where('CompanyID','=',$company)
                        ->where('VoucherDate','=',$date_only)
                        ->where('SalesStatus','LIKE','%S%')
                        ->groupBy('CategoryDes2')
                        ->get();

   $data_category12 = json_decode(json_encode($data_category2), true);
   $data_category2_total['Quantity'] =0;
   $data_category2_total['price'] = 0;
   $quantity =0;
   $price=0;
   $i=0;
   $data_category2 = array();
   foreach($data_category12 as $arr){

      $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
      $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
      $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
      $quantity = $quantity+strval(round($arr['Quantity'],2));
      $price = $price+strval(round($arr['sales'],2));
      $i++;
   }
   $data_category2_total['Quantity'] = strval($quantity);
   $data_category2_total['price'] =strval(number_format($price,2));

   // -----------------Sales By Items Category 1--------------------------

   if($selecctedcategory =='Category1'){
      $categorydes1_array = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('CategoryDes1')
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('CompanyID','=',$company)
                              ->where('VoucherDate','=',$date_only)
                              ->distinct('CategoryDesc1')
                              ->get()->toArray();
      $i = 0;
      foreach($categorydes1_array as $key => $value) {
         $query_items  = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('ItemDesc as item',DB::raw('SUM(Qty) as quantity'),DB::raw('SUM(SellingPrice*Qty) as price'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('SalesStatus','LIKE','%S%')
                              ->where('CategoryDes1','=',$value->CategoryDes1)
                              ->groupBy('ItemDesc')
                              ->get();
         $query_items1 = json_decode(json_encode($query_items), true);
         $i=0;
         $query_items = array();
         $query_items_total['qty']   = '0';
         $query_items_total['total'] ='0';
         $total =0;
         foreach($query_items1 as $arr){

            $query_items[$i]['item']     = $arr['item'];
            $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items[$i]['price']    = strval(number_format($arr['price'],2));
            $query_items_total['qty']    = $query_items_total['qty']+$arr['quantity'];
            $query_items_total['total']  = $query_items_total['total']+$arr['price'];
            $i++;
         }
         $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);
         $i=$i+1;
      }

      $data_payment_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('SUM(Qty) as Quantity'),DB::raw('SUM(SellingPrice*Qty) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->get();

      $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
      $data_payment_total  = array();
      $i=0;
      foreach($data_payment_total1 as $arr){
         $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
         $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
         $i++;
      }
   }
   // -----------------Sales By Items Category 2--------------------------
   if($selecctedcategory=='Category2'){
   $categorydes2_array = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select('CategoryDes2')
                           ->where('LocationID','=',$location_id)
                           ->where('ChainID','=',$chin_id)
                           ->where('CompanyID','=',$company)
                           ->where('VoucherDate','=',$date_only)
                           ->distinct('CategoryDesc2')
                           ->get()->toArray();
      $i = 0;
      foreach($categorydes2_array as $key => $value) {

         $query_items =DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select('ItemDesc as item',DB::raw('SUM(Qty) as quantity'),DB::raw('SUM(SellingPrice*Qty) as price'))
                           ->where('CompanyID','=',$company)
                           ->where('LocationID','=',$location_id)
                           ->where('ChainID','=',$chin_id)
                           ->where('VoucherDate','=',$date_only)
                           ->where('SalesStatus','LIKE','%S%')
                           ->where('CategoryDes2','=',$value->CategoryDes2)
                           ->groupBy('ItemDesc')
                           ->get();
         $query_items23 = json_decode(json_encode($query_items), true);
         $i=0;
         $query_items2 = array();
         $query_items_total2['qty'] ='0';
         $query_items_total2['total'] ='0';
         $total = 0;
         foreach($query_items23 as $arr){

            $query_items2[$i]['item'] = $arr['item'];
            $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items2[$i]['price'] = strval(round($arr['price'],2));
            $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
            $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
            $i++;
         }
         $items2[] = array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);
         $i = $i+1;
      }

      $data_payment_total2 = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('SUM(Qty) as Quantity'),DB::raw('SUM(SellingPrice*Qty) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->get();

      $data_payment_total21 = json_decode(json_encode($data_payment_total2), true);
      $data_payment_total2  = array();
      $i=0;
      foreach($data_payment_total21 as $arr){
         $data_payment_total2['Quantity'] = strval(round($arr['Quantity'],2));
         $data_payment_total2['Price'] = strval(number_format($arr['Price'],2));
         $i++;
      }
   }

   //---------------------------------Hourly Sale---------------------------
   $query_items =DB::connection('mysql_source')
                    // ->table('sales_header')
                    // ->select(DB::raw('hour( StartTime ) as Hour'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(VoucherNo) as Transaction'),DB::raw('SUM(SalesAmount) as Sales'))
                    // ->where('CompanyID','=',$company)
                    // ->where('LocationID','=',$location_id)
                    // ->where('VoucherDate','=',$date_only)
                    // ->where('VoucherStatus','LIKE','%S%')
                    // ->where('ChainID','=',$chin_id)
                    // ->groupBy(DB::raw('hour(StartTime)'),DB::raw('day(StartTime)'))
                    // ->get();
                    ->table('sales_line')
                    ->select(DB::raw('hour( sales_header.StartTime ) as Hour'),
                      DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date_only. '" AND `CompanyID`= "'.$company.'" AND `LocationID` = "' . $location_id .'"  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND hour(`StartTime`) =  Hour) as Guest'),
                      DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'),
                      DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'))
                    ->leftJoin("sales_header",function($join){
                    $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                    })
                    ->where('sales_line.CompanyID','=',$company)
                    ->where('sales_line.ChainID','=',$chin_id)
                    ->where('sales_line.LocationID',$location_id)
                    ->where('sales_line.VoucherDate', $date_only)
                    ->where('sales_line.SalesStatus','S')
                    ->groupBy(DB::raw('hour(sales_header.StartTime)'))
                    ->get();
   $i=0;
   $data_hourly_sale=array();
   $hourly_sale ='0';
   $guesttotal  ='0';
   $trxtotal    ='0';
   $data_hourly_sale_total =array();
   foreach($query_items as $data){

      if($data->Hour  < 10){
         $data->Hour = '0'.$data->Hour;
      }
      $to =$data->Hour+1;
      if($to <10 ){
         $to ='0'.$to;
      }
      $data_hourly_sale[$i]['Hours']       = strval($data->Hour.':00 to '.$to.':00');
      $data_hourly_sale[$i]['Guest']       = strval(round($data->Guest,2));
      $data_hourly_sale[$i]['Transcation'] = strval(round($data->Transaction,2));
      $data_hourly_sale[$i]['Sales']       = strval(number_format($data->Sales,2));
      $hourly_sale                         = $hourly_sale + round($data->Sales,2);
      $guesttotal                          = $guesttotal + round($data->Guest,2);
      $trxtotal                            = $trxtotal + round($data->Transaction,2);
      $i++;
   }
   $data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
   $data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
   $data_hourly_sale_total['trx']   = strval(round($trxtotal,2));

   //------------------------------------Credit Customer------------------------------------------
   $data_credit_customer =DB::connection('mysql_source')
                              ->table('sales_payment')
                              ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
                              ->where('sales_payment.CompanyID','=',$company)
                              ->where('sales_payment.LocationID','=',$location_id)
                              ->where('sales_payment.ChainID','=',$chin_id)
                              ->where('sales_payment.VoucherDate','=',$date_only)
                              ->where('sales_payment.CustomerName','!=','')
                              ->groupBy('CustomerName','CardNo')
                              ->get();
   //------------------------Sales Summary----------------------------------------------------------

   //------------------------Total Sales----------------------------------------------------------
   $data_total_sales = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select(DB::raw('round(SUM(Qty*SellingPrice),3) as TotalSales'))
                           ->where('CompanyID','=',$company)
                           ->where('ChainID','=',$chin_id)
                           ->where('LocationID','=',$location_id)
                           ->where('VoucherDate','=',$date_only)
                           ->where('SalesStatus','LIKE','%S%')
                           ->get();
   $data_total_sales1 = json_decode(json_encode($data_total_sales), true);
   $data_total_sales  = array();
   $i=0;
   foreach($data_total_sales1 as $arr){
      $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
      $i++;
   }
   //------------------------Delivery Charges----------------------------------------------------------
   $data_delivery_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('SUM(ServiceTaxAmount+ServiceTaxBaseAmount) as DeliveryCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('LocationID','=',$location_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','=','DL')
                              ->get();
   $data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
   $data_delivery_charge  = array();
   $i=0;
   foreach($data_delivery_charge1 as $arr){
      $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
      $i++;
   }
   //------------------------Service Charges----------------------------------------------------------
   $data_service_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('SUM(ServiceTaxAmount+ServiceTaxBaseAmount) as ServiceCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('LocationID','=',$location_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','!=','DL')
                              ->get();
   $data_service_charge1 = json_decode(json_encode($data_service_charge), true);
   $data_service_charge  = array();
   $i=0;
   foreach($data_service_charge1 as $arr){
      $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
      $i++;
   }

   //------------------------Gross Sales----------------------------------------------------------
   $data_gross_sales['Gross Sale']=strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));

   //------------------------Staff Meal----------------------------------------------------------
   $data_staff_meal =DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('SUM(Qty*SellingPrice) as Amount'))
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->where('sales_line.LocationID','=',$location_id)
                        ->where('sales_line.VoucherDate','=',$date_only)
                        ->where('sales_header.OrderType','=','SM')
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                           ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                           ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                           ->on('sales_line.ChainID','=','sales_header.ChainID')
                           ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->where('sales_line.LocationID','=',$location_id)
                        ->where('sales_line.VoucherDate','=',$date_only)
                        ->where('sales_header.OrderType','=','SM')
                        ->get();
   $data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);

   $data_staff_meal  = array();
   $i=0;
   foreach($data_staff_meal1 as $arr){
      $data_staff_meal['Count'] = strval(round($arr['Count'],2));
      if($data_staff_meal['Count']>1){
         $data_staff_meal['Count']='1';
      }
      $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
      $i++;
   }

   //------------------------Discount----------------------------------------------------------
   \DB::enableQueryLog();
   $data_discount = DB::connection('mysql_source')
                        ->table('sales_payment')
                        ->select(DB::raw('SUM(Amount) as Discount'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
                           ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
                           ->on('sales_header.CompanyID','=','sales_payment.CompanyID')
                           ->on('sales_header.ChainID','=','sales_payment.ChainID')
                           ->on('sales_header.LocationID','=','sales_payment.LocationID');
                        })
                        ->leftJoin("payment_types",function($join1){
                        $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
                           ->on('sales_payment.ChainID','=','payment_types.ChainID')
                           ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
                        })
                        ->where('sales_header.CompanyID','=',$company)
                        ->where('sales_header.ChainID','=',$chin_id)
                        ->where('sales_header.LocationID','=',$location_id)
                        ->where('sales_header.VoucherDate','=',$date_only)
                        ->where('sales_header.VoucherStatus','LIKE','%S%')
                        ->where('sales_header.OrderType','!=','SM')
                        ->where('payment_types.IsDiscountCard','=','1')
                        ->get();
   $query          = \DB::getQueryLog();
   $data_discount1 = json_decode(json_encode($data_discount), true);
   $data_discount  = array();
   $i=0;
   foreach($data_discount1 as $arr){
      //  $data_discount['Discount'] = strval(round($arr['Discount'],2));
      $i++;
   }
   $data_discount['Discount'] = strval(round($totalDiscount, 2));

   //------------------------VAT----------------------------------------------------------
   $data_vat = DB::connection('mysql_source')
                  ->table('sales_header')
                  ->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
                  ->where('CompanyID','=',$company)
                  ->where('ChainID','=',$chin_id)
                  ->where('VoucherDate','=',$date_only)
                  ->where('LocationId','=',$location_id)
                  ->where('VoucherStatus','S')
                  ->get();
   $data_vat1 = json_decode(json_encode($data_vat), true);
   $data_vat   = array();
   $i=0;
   foreach($data_vat1 as $arr){
      $data_vat['VAT'] = strval(round($arr['VAT'],2));
      $i++;
   }

   //------------------------Net Sales----------------------------------------------------------
   $data_net_sales['Net Sales'] = strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));

   //--------------------------------------Taxable Value--------------------------------------------
   $data_tax                  = array();
   $data_tax['TaxableValue'] =  strval(round($data_net_sales['Net Sales'],2)-$data_vat['VAT']);

   //------------------------PayOut----------------------------------------------------------
   $data_payout['Payout']='0';

   //------------------------Credit Sales----------------------------------------------------------
//    $data_creditsales = DB::connection('mysql_source')
//                         ->table('sales_payment')
//                         ->select(DB::raw('SUM(Amount) as CreditSales'))
//                         ->where('sales_header.CompanyID','=',$company)
//                         ->where('sales_header.ChainID','=',$chin_id)
//                         ->where('sales_header.LocationID','=',$location_id)
//                         ->where('sales_header.VoucherDate','=',$date_only)
//                         ->where('sales_header.VoucherStatus','LIKE','%S%')
//                         ->leftJoin("sales_header",function($join){
//                         $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
//                            ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
//                            ->on('sales_payment.CompanyID','=','sales_header.CompanyID')
//                            ->on('sales_payment.ChainID','=','sales_header.ChainID')
//                            ->on('sales_payment.LocationID','=','sales_header.LocationID');
//                         })
//                         ->leftJoin("payment_types",function($join1){
//                         $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
//                            ->on('sales_payment.ChainID','=','payment_types.ChainID')
//                            ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
//                         })
//                         ->where('payment_types.IsCreditCard','=','1')
//                         ->where('payment_types.IsDiscountCard','!=','1')
//                         ->where('sales_header.OrderType','!=','SM')
//                         ->get();
//    $data_creditsales1 = json_decode(json_encode($data_creditsales), true);
//    $data_creditsales  = array();
//    $i=0;
//    foreach($data_creditsales1 as $arr){
//     //   $data_creditsales['CreditSales'] = strval(round($arr['CreditSales'],2));
//       $i++;
//    }
   $data_creditsales['CreditSales'] = strval(round($creditSaleTotal,2));
   $data_locationlist =DB::connection('mysql_source')
                           ->table('sales_header')
                           ->select('LocationID')
                           ->groupBy('LocationID')
                           ->get();

   //------------------------Actual Banking----------------------------------------------------------
   $data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

   //------------------------Voids----------------------------------------------------------
   //item order item
   $data_items_order = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                           ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                           ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                           ->on('sales_header.ChainID','=','sales_line.ChainID')
                           ->on('sales_header.LocationID','=','sales_line.LocationID');
                        })
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->where('sales_line.LocationID',$location_id)
                        ->where('sales_line.VoucherDate',$date_only)
                        ->where('sales_header.VoucherStatus', 'V')
                        ->get();

   $data_items_order1 = json_decode(json_encode($data_items_order), true);
   $data_items_order  = array();
   $i=0;
   foreach($data_items_order1 as $arr){

      $data_items_order['Count']  = strval(round($arr['Count'],2));
      $data_items_order['Amount'] = strval(round($arr['Amount'],2));
      $i++;
   }

   //item  total
   $data_void_items_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
                              ->leftJoin("sales_header",function($join){
                              $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                 ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                 ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                 ->on('sales_header.ChainID','=','sales_line.ChainID')
                                 ->on('sales_header.LocationID','=','sales_line.LocationID');
                              })
                              ->where('sales_line.CompanyID','=',$company)
                              ->where('sales_line.ChainID','=',$chin_id)
                              ->where('sales_line.LocationID',$location_id)
                              ->where('sales_line.VoucherDate',$date_only)
                              ->where('sales_line.SalesStatus', 'V')
                              ->first();
   // $data_void_items_total1 = json_decode(json_encode($data_void_items_total), true);

   // $data_void_items_total  = array();
   // $i=0;
   // foreach($data_void_items_total1 as $arr){
   //    $data_void_items_total['Count']  = strval(round($arr['Count'],2));
   //    $data_void_items_total['Amount'] = strval(round($arr['Amount'],2));
   //    $i++;
   // }
   //item void
   $data_items_void           = array();
   $data_items_void['Amount'] = strval(round($data_void_items_total->Amount,3)) ?? '0';
   $data_items_void['Count']  = strval(($data_void_items_total->Count)) ?? '0';
   // if($data_items_void['Amount']>0){
   //    $data_items_void['Count'] ="1";
   // }else{
   //    $data_items_void['Count'] ="0";
   // }
   //------------------------Total of Voids And Staff Meal----------------------------------------------------------
   $data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount'] + @$data_items_void['Amount'] + @$data_items_order['Amount'], 3));

   //----------------------------------------------Chain---------------------------------------------------------------
   $chain = DB::connection('mysql_source')
               ->table('chainaccess')
               ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
               ->select('chainaccess.ChainId','chain.ChainName')
               ->where('chainaccess.CompanyId','=',$company)
               ->groupBy('chainaccess.ChainId')
               ->groupBy('chain.ChainName')
               ->get();

   //----------------------------------------------Location---------------------------------------------------------------
   $location =DB::connection('mysql_source')
                  ->table('locationaccess')
                  ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                  ->select('locationaccess.LocationId','LocationName')
                  ->where('locationaccess.ChainId','=',$chin_id)
                  ->groupBy('locationaccess.LocationId')
                  ->groupBy('location.LocationName')
                  ->get();
   DB::commit();
   DB::disconnect('mysql_source');
   if($selecctedcategory=='Category1'){
      echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>@$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,'Location'=>$location,"Chain"=>$chain]);
   }else if($selecctedcategory=='Category2'){
      echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>@$items2,"Items_total"=>$data_payment_total2,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,'Location'=>$location,"Chain"=>$chain]);
   }
}

public function all_data_testing1(Request $request){

    $userid = $this->checkAuth($request);
    if(!$userid){
       return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']);
    }
    $user         = UserDetails::where('id','=',$userid->user_id)->first();
    $date_only    = date('Y-m-d',strtotime($request->date)).' 00:00:00';
    $chin_id      = $request->chain;
    @$location_id = $request->location;
    $selecctedcategory =$request->category;
    if($selecctedcategory=='') {
       $selecctedcategory ='Category1';
    }
    $this->getConnectionDetails($user->CompanyID);
    DB::beginTransaction();
    DB::enableQueryLog();
    $company  = @$user->CompanyID;
    $username = @$user->username;
    $userdata =array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);

   // -----------------order type----------------------
   $data_ordertype = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                        DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date_only. '" AND `CompanyID`= "'.$company.'" AND `LocationID` = "' . $location_id .'"  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                        DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                            ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                            ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                            ->on('sales_line.ChainID','=','sales_header.ChainID')
                            ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.CompanyID','=', $company)
                        ->where('sales_line.ChainID','=', $chin_id)
                        ->where('sales_line.LocationID', $location_id)
                        ->where('sales_line.VoucherDate', $date_only)
                        ->where('sales_line.SalesStatus','S')
                        ->groupBy('sales_header.OrderType')
                        ->get();
   $data_ordertype1 = json_decode(json_encode($data_ordertype), true);
   $i = 0;
   $data_ordertype = array();
   $data_ordertype_total['Sales'] = 0;
   $trxtotal   = '0';
   $guesttotal = '0';
  foreach($data_ordertype1 as $arr){

     $data_ordertype[$i]['OrderType']   = $arr['Type'];
     $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
     $data_ordertype[$i]['Guest']       = strval(round($arr['Guest'],2));
     $data_ordertype[$i]['Sales']       = strval(number_format($arr['Sales'],2));
     $data_ordertype_total['Sales']     = $data_ordertype_total['Sales']+strval(round($arr['Sales'],2));
     $trxtotal                          = $trxtotal +$data_ordertype[$i]['Transaction'];
     $guesttotal                        = $guesttotal +$data_ordertype[$i]['Guest'];
     $i++;
  }
   $data_ordertype_total['Sales'] = strval(number_format($data_ordertype_total['Sales'],2));
   $data_ordertype_total['Trx']   = strval(round($trxtotal,2));
   $data_ordertype_total['Guest'] = strval(round($guesttotal,2));

   // ---------------Category 1-----------------------
   $data_category1 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes1',DB::raw('SUM(SellingPrice*Qty) as sales'),DB::raw('round(SUM(Qty),3) as Guest'))
                        ->where('LocationID','=',$location_id)
                        ->where('ChainID','=',$chin_id)
                        ->where('CompanyID','=',$company)
                        ->where('VoucherDate','=',$date_only)
                        ->where('SalesStatus','LIKE','%S%')
                        ->groupBy('CategoryDes1')
                        ->get();
   $data_category11 = json_decode(json_encode($data_category1), true);
   $guests =0;
   $sales=0;
   $i=0;
   $data_category1 = array();
   foreach($data_category11 as $arr){

      $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
      $data_category1[$i]['Guests']       = strval(round($arr['Guest'],2));
      $data_category1[$i]['sales']        = strval(number_format($arr['sales'],2));
      $guests                             = $guests+strval($arr['Guest']);
      $sales                              = $sales +strval(round($arr['sales'],2));
      $i++;
   }
   $data_category1_total['Guests']  = strval($guests);
   $data_category1_total['sales']   = strval(number_format($sales,2));

   // --------------Payment---------------------------
   $payment_type = DB::connection('mysql_source')
                     ->table('sales_payment')
                     ->select('PaymentType')
                     ->where('CompanyID','=',$company)
                     ->where('LocationID','=',$location_id)
                     ->where('ChainID','=',$chin_id)
                     ->where('VoucherDate','=',$date_only)
                     ->groupBy('PaymentType')
                     ->get();
   // $payment_type=array("CASH","VISA CARD",'MASTER CARD','STAFF DISCOUNT 30','ONLINE PAYMENTS');
   $data_paymenttype =array();
   $i=0;
   $cartype ='0';
   $totalDiscount = 0;
   $creditSaleTotal = 0;
   foreach ($payment_type as $value) {

      @$query_paymenttype = DB::connection('mysql_source')
                              ->table('sales_payment')
                              ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('PaymentType','=',$value->PaymentType)
                              ->groupBy('PaymentType')
                              ->get();
      @$typevalue = DB::connection('mysql_source')
                        ->table('payment_types')
                        ->select('*')
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->where('PaymentType','=',$value->PaymentType)
                        ->get();
      $quer   = @$query_paymenttype[0]->Amount;
      $tcount = @$query_paymenttype[0]->Count;

      /*****************************Payment Types *************************/
      if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
         $id = 1;
         $totalDiscount += $quer;
      }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCCard == '1')){
         $id = 2;
         $creditSaleTotal += $quer;
      }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCustCredit == '1')){
         $id = 3;
         $creditSaleTotal += $quer;
      }else if((@$typevalue[0]->IsStaffMeal== '1')){
         $id = 4;
      }else{
         $id = 5;
      }

      $data_paymenttype[] = array("Group Id"=>$id, "Payment Type"=>$value->PaymentType, "Amount"=>strval(round($quer,2)), "Count"=>strval(round($tcount,2)));
      $i++;
   }
   $visa_amount['Visa Amount']      = strval(number_format(@$data_paymenttype[1]['Amount'],2));
   $master_amount['Master Amount']  = strval(number_format(@$data_paymenttype[2]['Amount'],2));
   $data_visamastertotal[]          = array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));

   // ---------------Category 2-----------------------

   $data_category2 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                        ->where('LocationID','=',$location_id)
                        ->where('ChainID','=',$chin_id)
                        ->where('CompanyID','=',$company)
                        ->where('VoucherDate','=',$date_only)
                        ->where('SalesStatus','LIKE','%S%')
                        ->groupBy('CategoryDes2')
                        ->get();

   $data_category12 = json_decode(json_encode($data_category2), true);
   $data_category2_total['Quantity'] =0;
   $data_category2_total['price'] = 0;
   $quantity =0;
   $price=0;
   $i=0;
   $data_category2 = array();
   foreach($data_category12 as $arr){

      $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
      $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
      $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
      $quantity = $quantity+strval(round($arr['Quantity'],2));
      $price = $price+strval(round($arr['sales'],2));
      $i++;
   }
   $data_category2_total['Quantity'] = strval($quantity);
   $data_category2_total['price'] =strval(number_format($price,2));

   // -----------------Sales By Items Category 1--------------------------

   if($selecctedcategory =='Category1'){
      $categorydes1_array = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('CategoryDes1')
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('CompanyID','=',$company)
                              ->where('VoucherDate','=',$date_only)
                              ->distinct('CategoryDesc1')
                              ->get()->toArray();
      $i = 0;
      foreach($categorydes1_array as $key => $value) {
         $query_items  = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('ItemDesc as item',DB::raw('SUM(Qty) as quantity'),DB::raw('SUM(SellingPrice*Qty) as price'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('SalesStatus','LIKE','%S%')
                              ->where('CategoryDes1','=',$value->CategoryDes1)
                              ->groupBy('ItemDesc')
                              ->get();
         $query_items1 = json_decode(json_encode($query_items), true);
         $i=0;
         $query_items = array();
         $query_items_total['qty']   = '0';
         $query_items_total['total'] ='0';
         $total =0;
         foreach($query_items1 as $arr){

            $query_items[$i]['item']     = $arr['item'];
            $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items[$i]['price']    = strval(number_format($arr['price'],2));
            $query_items_total['qty']    = $query_items_total['qty']+$arr['quantity'];
            $query_items_total['total']  = $query_items_total['total']+$arr['price'];
            $i++;
         }
         $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);
         $i=$i+1;
      }

      $data_payment_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('SUM(Qty) as Quantity'),DB::raw('SUM(SellingPrice*Qty) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->get();

      $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
      $data_payment_total  = array();
      $i=0;
      foreach($data_payment_total1 as $arr){
         $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
         $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
         $i++;
      }
   }
   // -----------------Sales By Items Category 2--------------------------
   if($selecctedcategory=='Category2'){
   $categorydes2_array = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select('CategoryDes2')
                           ->where('LocationID','=',$location_id)
                           ->where('ChainID','=',$chin_id)
                           ->where('CompanyID','=',$company)
                           ->where('VoucherDate','=',$date_only)
                           ->distinct('CategoryDesc2')
                           ->get()->toArray();
      $i = 0;
      foreach($categorydes2_array as $key => $value) {

         $query_items =DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select('ItemDesc as item',DB::raw('SUM(Qty) as quantity'),DB::raw('SUM(SellingPrice*Qty) as price'))
                           ->where('CompanyID','=',$company)
                           ->where('LocationID','=',$location_id)
                           ->where('ChainID','=',$chin_id)
                           ->where('VoucherDate','=',$date_only)
                           ->where('SalesStatus','LIKE','%S%')
                           ->where('CategoryDes2','=',$value->CategoryDes2)
                           ->groupBy('ItemDesc')
                           ->get();
         $query_items23 = json_decode(json_encode($query_items), true);
         $i=0;
         $query_items2 = array();
         $query_items_total2['qty'] ='0';
         $query_items_total2['total'] ='0';
         $total = 0;
         foreach($query_items23 as $arr){

            $query_items2[$i]['item'] = $arr['item'];
            $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items2[$i]['price'] = strval(round($arr['price'],2));
            $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
            $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
            $i++;
         }
         $items2[] = array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);
         $i = $i+1;
      }

      $data_payment_total2 = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('SUM(Qty) as Quantity'),DB::raw('SUM(SellingPrice*Qty) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('LocationID','=',$location_id)
                              ->where('ChainID','=',$chin_id)
                              ->where('VoucherDate','=',$date_only)
                              ->get();

      $data_payment_total21 = json_decode(json_encode($data_payment_total2), true);
      $data_payment_total2  = array();
      $i=0;
      foreach($data_payment_total21 as $arr){
         $data_payment_total2['Quantity'] = strval(round($arr['Quantity'],2));
         $data_payment_total2['Price'] = strval(number_format($arr['Price'],2));
         $i++;
      }
   }

   //---------------------------------Hourly Sale---------------------------
   $query_items =DB::connection('mysql_source')
                    ->table('sales_line')
                    ->select(DB::raw('hour( sales_header.StartTime ) as Hour'),
                      DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` = "' .$date_only. '" AND `CompanyID`= "'.$company.'" AND `LocationID` = "' . $location_id .'"  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND hour(`StartTime`) =  Hour) as Guest'),
                      DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'),
                      DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'))
                    ->leftJoin("sales_header",function($join){
                    $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                    })
                    ->where('sales_line.CompanyID','=',$company)
                    ->where('sales_line.ChainID','=',$chin_id)
                    ->where('sales_line.LocationID',$location_id)
                    ->where('sales_line.VoucherDate', $date_only)
                    ->where('sales_line.SalesStatus','S')
                    ->groupBy(DB::raw('hour(sales_header.StartTime)'))
                    ->get();
   $i=0;
   $data_hourly_sale=array();
   $hourly_sale ='0';
   $guesttotal  ='0';
   $trxtotal    ='0';
   $data_hourly_sale_total =array();
   foreach($query_items as $data){

      if($data->Hour  < 10){
         $data->Hour = '0'.$data->Hour;
      }
      $to =$data->Hour+1;
      if($to <10 ){
         $to ='0'.$to;
      }
      $data_hourly_sale[$i]['Hours']       = strval($data->Hour.':00 to '.$to.':00');
      $data_hourly_sale[$i]['Guest']       = strval(round($data->Guest,2));
      $data_hourly_sale[$i]['Transcation'] = strval(round($data->Transaction,2));
      $data_hourly_sale[$i]['Sales']       = strval(number_format($data->Sales,2));
      $hourly_sale                         = $hourly_sale + round($data->Sales,2);
      $guesttotal                          = $guesttotal + round($data->Guest,2);
      $trxtotal                            = $trxtotal + round($data->Transaction,2);
      $i++;
   }
   $data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
   $data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
   $data_hourly_sale_total['trx']   = strval(round($trxtotal,2));

   //------------------------------------Credit Customer------------------------------------------
   $data_credit_customer =DB::connection('mysql_source')
                              ->table('sales_payment')
                              ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
                              ->where('sales_payment.CompanyID','=',$company)
                              ->where('sales_payment.LocationID','=',$location_id)
                              ->where('sales_payment.ChainID','=',$chin_id)
                              ->where('sales_payment.VoucherDate','=',$date_only)
                              ->where('sales_payment.CustomerName','!=','')
                              ->groupBy('CustomerName','CardNo')
                              ->get();
   //------------------------Sales Summary----------------------------------------------------------

   //------------------------Total Sales----------------------------------------------------------
   $data_total_sales = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select(DB::raw('round(SUM(Qty*SellingPrice),3) as TotalSales'))
                           ->where('CompanyID','=',$company)
                           ->where('ChainID','=',$chin_id)
                           ->where('LocationID','=',$location_id)
                           ->where('VoucherDate','=',$date_only)
                           ->where('SalesStatus','LIKE','%S%')
                           ->get();
   $data_total_sales1 = json_decode(json_encode($data_total_sales), true);
   $data_total_sales  = array();
   $i=0;
   foreach($data_total_sales1 as $arr){
      $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
      $i++;
   }
   //------------------------Delivery Charges----------------------------------------------------------
   $data_delivery_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('SUM(ServiceTaxAmount+ServiceTaxBaseAmount) as DeliveryCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('LocationID','=',$location_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','=','DL')
                              ->get();
   $data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
   $data_delivery_charge  = array();
   $i=0;
   foreach($data_delivery_charge1 as $arr){
      $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
      $i++;
   }
   //------------------------Service Charges----------------------------------------------------------
   $data_service_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('SUM(ServiceTaxAmount+ServiceTaxBaseAmount) as ServiceCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('LocationID','=',$location_id)
                              ->where('VoucherDate','=',$date_only)
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','!=','DL')
                              ->get();
   $data_service_charge1 = json_decode(json_encode($data_service_charge), true);
   $data_service_charge  = array();
   $i=0;
   foreach($data_service_charge1 as $arr){
      $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
      $i++;
   }

   //------------------------Gross Sales----------------------------------------------------------
   $data_gross_sales['Gross Sale']=strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));

   //------------------------Staff Meal----------------------------------------------------------
   $data_staff_meal =DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('SUM(Qty*SellingPrice) as Amount'))
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->where('sales_line.LocationID','=',$location_id)
                        ->where('sales_line.VoucherDate','=',$date_only)
                        ->where('sales_header.OrderType','=','SM')
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                           ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                           ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                           ->on('sales_line.ChainID','=','sales_header.ChainID')
                           ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->where('sales_line.LocationID','=',$location_id)
                        ->where('sales_line.VoucherDate','=',$date_only)
                        ->where('sales_header.OrderType','=','SM')
                        ->get();
   $data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);

   $data_staff_meal  = array();
   $i=0;
   foreach($data_staff_meal1 as $arr){
      $data_staff_meal['Count'] = strval(round($arr['Count'],2));
      if($data_staff_meal['Count']>1){
         $data_staff_meal['Count']='1';
      }
      $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
      $i++;
   }

   //------------------------Discount----------------------------------------------------------
   $data_discount['Discount'] = strval(round($totalDiscount, 2));

   //------------------------VAT----------------------------------------------------------
   $data_vat = DB::connection('mysql_source')
                  ->table('sales_header')
                  ->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
                  ->where('CompanyID','=',$company)
                  ->where('ChainID','=',$chin_id)
                  ->where('VoucherDate','=',$date_only)
                  ->where('LocationId','=',$location_id)
                  ->where('VoucherStatus','S')
                  ->get();
   $data_vat1 = json_decode(json_encode($data_vat), true);
   $data_vat   = array();
   $i=0;
   foreach($data_vat1 as $arr){
      $data_vat['VAT'] = strval(round($arr['VAT'],2));
      $i++;
   }

   //------------------------Net Sales----------------------------------------------------------
   $data_net_sales['Net Sales'] = strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));

   //--------------------------------------Taxable Value--------------------------------------------
   $data_tax                  = array();
   $data_tax['TaxableValue']  = strval(round($data_net_sales['Net Sales'],2)-$data_vat['VAT']);

   //------------------------PayOut----------------------------------------------------------
   $data_payout['Payout']='0';

   //------------------------Credit Sales----------------------------------------------------------

   $data_creditsales['CreditSales'] = strval(round($creditSaleTotal,2));
   $data_locationlist =DB::connection('mysql_source')
                           ->table('sales_header')
                           ->select('LocationID')
                           ->groupBy('LocationID')
                           ->get();

   //------------------------Actual Banking----------------------------------------------------------
   $data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

   //------------------------Voids----------------------------------------------------------
   //item order item
   $data_items_order = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                           ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                           ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                           ->on('sales_header.ChainID','=','sales_line.ChainID')
                           ->on('sales_header.LocationID','=','sales_line.LocationID');
                        })
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->where('sales_line.LocationID',$location_id)
                        ->where('sales_line.VoucherDate',$date_only)
                        ->where('sales_header.VoucherStatus', 'V')
                        ->get();

   $data_items_order1 = json_decode(json_encode($data_items_order), true);
   $data_items_order  = array();
   $i=0;
   foreach($data_items_order1 as $arr){

      $data_items_order['Count']  = strval(round($arr['Count'],2));
      $data_items_order['Amount'] = strval(round($arr['Amount'],2));
      $i++;
   }

   //item  total
   $data_void_items_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
                              ->leftJoin("sales_header",function($join){
                              $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                 ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                 ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                 ->on('sales_header.ChainID','=','sales_line.ChainID')
                                 ->on('sales_header.LocationID','=','sales_line.LocationID');
                              })
                              ->where('sales_line.CompanyID','=',$company)
                              ->where('sales_line.ChainID','=',$chin_id)
                              ->where('sales_line.LocationID',$location_id)
                              ->where('sales_line.VoucherDate',$date_only)
                              ->where('sales_line.SalesStatus', 'V')
                              ->where('sales_header.VoucherStatus', '!=', 'V')
                              ->first();
   //item void
   $data_items_void           = array();
   $data_items_void['Amount'] = strval(round($data_void_items_total->Amount,3)) ?? '0';
   $data_items_void['Count']  = strval(($data_void_items_total->Count)) ?? '0';
   //------------------------Total of Voids And Staff Meal----------------------------------------------------------
   $data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount'] + @$data_items_void['Amount'] + + @$data_items_order['Amount'],3));

   //----------------------------------------------Chain---------------------------------------------------------------
   $chain = DB::connection('mysql_source')
               ->table('chainaccess')
               ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
               ->select('chainaccess.ChainId','chain.ChainName')
               ->where('chainaccess.CompanyId','=',$company)
               ->groupBy('chainaccess.ChainId')
               ->groupBy('chain.ChainName')
               ->get();

   //----------------------------------------------Location---------------------------------------------------------------
   $location =DB::connection('mysql_source')
                  ->table('locationaccess')
                  ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
                  ->select('locationaccess.LocationId','LocationName')
                  ->where('locationaccess.ChainId','=',$chin_id)
                  ->groupBy('locationaccess.LocationId')
                  ->groupBy('location.LocationName')
                  ->get();
   DB::commit();
   DB::disconnect('mysql_source');
   if($selecctedcategory=='Category1'){
      echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>@$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,'Location'=>$location,"Chain"=>$chain]);
   }else if($selecctedcategory=='Category2'){
      echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>@$items2,"Items_total"=>$data_payment_total2,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,'Location'=>$location,"Chain"=>$chain]);
   }
}


public function all_dataonchange(Request $request){

   $datefrom=$request->datefrom;
   $dateto=$request->dateto;
   $chin_id = $request->chain;
   $location_id =$request->location;
   $pass   = $request->pass;
   $selecctedcategory =$request->category;
    if($selecctedcategory=='') {
       $selecctedcategory ='Category1';
    }

  if($datefrom > $dateto){
     $datef=$datefrom;
     $datefrom = $dateto;
     $dateto   = $datef;
   }


   @$location =explode(',',$location_id);
   if(!empty($location)){
     foreach($location as $loc){
       if($loc!='') $locationlist[]=$loc;
     }
   }
  //print_r($locationlist); exit();
 $userid =$this->checkAuth($request);
 if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
 $user = UserDetails::where('id','=',$userid->user_id)->first();
 $company_id = $user->CompanyID;
 $company = @$user->CompanyID;
 $username =@$user->username;
 $userdata =array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);
 // $company=$company_id[0]['CompanyID'];

 $this->getConnectionDetails($user->CompanyID);
 \DB::enableQueryLog();
 // -----------------order type----------------------
 $data_ordertype = DB::connection('mysql_source')
 ->table('sales_header')
 ->select('OrderType',DB::raw('round(SUM(SalesAmount),3) as Sales'),DB::raw('round(SUM(GuestCount),3) as Guest'),DB::raw('COUNT(VoucherNo) as Transaction'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->where('VoucherStatus','LIKE','%S%')
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->groupBy('OrderType')
 ->get();

 //print_r($data_ordertype); exit();
 $data_ordertype1 = json_decode(json_encode($data_ordertype), true);
 $data_ordertype_total = array();
 $i=0;
 $data_ordertype=array();
 $ototal ='0';
 $trxtotal ='0';
 $guesttotal = '0';
 foreach($data_ordertype1 as $arr){
  $data_ordertype[$i]['OrderType']   = $arr['OrderType'];
  $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
  $data_ordertype[$i]['Guest'] = strval(round($arr['Guest'],2));
  $data_ordertype[$i]['Sales'] = strval(number_format($arr['Sales'],2));
  $ototal                      = $ototal +$arr['Sales'];
  $trxtotal                      =$trxtotal +$data_ordertype[$i]['Transaction'];
  $guesttotal                      =$guesttotal +$data_ordertype[$i]['Guest'];
  $i++;
 }
 $data_ordertype_total['Sales'] = strval(number_format($ototal,2));
 $data_ordertype_total['Trx'] = strval(round($trxtotal,2));
 $data_ordertype_total['Guest'] = strval(round($guesttotal,2));

 // ---------------Category 1-----------------------
 //   $query_category1=$this->db->query("select CategoryDes1, sum(SellingPrice) as sales, sum(Qty) as Guests from sales_line where CompanyID='$company' and DATE(VoucherDate) = '$date_only' group by CategoryDes1");
 //   $data_category1=$query_category1->result();

 $data_category1 = DB::connection('mysql_source')
 ->table('sales_line')
 ->select('CategoryDes1',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(sum(Qty),3) as Guest'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->where('SalesStatus','LIKE','%S%')
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->groupBy('CategoryDes1')
 ->get();
 $data_category11 = json_decode(json_encode($data_category1), true);
 $i=0;
 $data_category1 = array();
 $data_category1_total = array();
 $guest                = 0;
 $sales                = 0;
 foreach($data_category11 as $arr){
 $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
 $data_category1[$i]['Guests'] = strval(round($arr['Guest'],2));
 $data_category1[$i]['sales'] = strval(number_format($arr['sales'],2));
 $guest                       = $guest + $data_category1[$i]['Guests'];
 $sales                       = $sales + $arr['sales'];
 $i++;
 }
 $data_category1_total['Guests'] = strval(round($guest,2));
 $data_category1_total['sales'] = strval(number_format($sales,2));

 // --------------Payment---------------------------
    $payment_type = DB::connection('mysql_source')
    ->table('sales_payment')
     ->select('PaymentType')
     ->where('CompanyID','=',$company)
     ->whereIn('LocationID',$locationlist)
     ->where('ChainID','=',$chin_id)
     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
     ->groupBy('PaymentType')
     ->get();


    // $payment_type=array("CASH","VISA CARD",'MASTER CARD','STAFF DISCOUNT 30','ONLINE PAYMENTS');
      $i=0;
    foreach ($payment_type as $value) {

        @$query_paymenttype = DB::connection('mysql_source')
        ->table('sales_payment')
        ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
        ->where('CompanyID','=',$company)
        ->whereIn('LocationID',$locationlist)
        ->where('ChainID','=',$chin_id)
        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
        ->where('PaymentType','=',$value->PaymentType)
        ->groupBy('PaymentType')
        ->get();

        $quer = @$query_paymenttype[0]->Amount;
        $tcount = @$query_paymenttype[0]->Count;
        $id=2;

        @$typevalue = DB::connection('mysql_source')
        ->table('payment_types')
        ->select('*')
        ->where('CompanyID','=',$company)
        ->where('ChainID','=',$chin_id)
        ->where('PaymentType','=',$value->PaymentType)
        ->get();
        $quer = @$query_paymenttype[0]->Amount;
        $tcount = @$query_paymenttype[0]->Count;
        $id=2;
        if(($value->PaymentType == 'VISA CARD') || ($value->PaymentType == 'MASTER CARD') || ($value->PaymentType == 'ONLINE PAYMENTS')){
            $id=1;
        }else if($value->PaymentType == 'CASH'){
            $id=2;
        }else if($value->PaymentType == 'STAFF DISCOUNT 30'){
            $id=3;
        }else if($value->PaymentType == 'CREDIT'){
           $id=4;
        }else if(@$typevalue[0]->IsCreditCard=='0'){
          $id=2;
        }else if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
          $id=3;
        }
        else if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsCCard=='1')){
          $id=4;
        }else{
            $id =5;
        }
        if($value->PaymentType!=''){
          $data_paymenttype[]=array("Group Id"=>$id,"Payment Type"=>$value->PaymentType,"Amount"=>strval(round($quer,2)),"Count"=>strval(round($tcount,2)));
        }
        $i++;
      }
      $visa_amount['Visa Amount']=strval(round(@$data_paymenttype[1]['Amount'],2));
      $master_amount['Master Amount']=strval(round(@$data_paymenttype[2]['Amount'],2));
      $data_visamastertotal[]=array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));






 // ---------------Category 2-----------------------
 $data_category2 = DB::connection('mysql_source')
 ->table('sales_line')
 ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('SalesStatus','LIKE','%S%')
 ->groupBy('CategoryDes2')
 ->get();
 $data_category12 = json_decode(json_encode($data_category2), true);
 $i=0;
 $data_category2 = array();
 foreach($data_category12 as $arr){
 $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
 $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
 $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
 $i++;
 }

 $data_category2_total= DB::connection('mysql_source')
 ->table('sales_line')
 ->select(DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->where('SalesStatus','LIKE','%S%')
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->get();
 $data_category2_total2 = json_decode(json_encode($data_category2_total), true);
 $data_category2_total = array();
 $i=0;
 foreach($data_category2_total2 as $arr){
 $data_category2_total['Quantity'] = strval(round($arr['Quantity'],2));
 $data_category2_total['price'] = strval(number_format($arr['sales'],2));
 $i++;
 }


 // -----------------Sales Items by Category 1--------------------------
 if($selecctedcategory=='Category1') {

   $categorydes1_array = DB::connection('mysql_source')
   ->table('sales_line')
   ->select('CategoryDes1')
   ->where('CompanyID','=',$company)
   ->where('ChainID','=',$chin_id)
   ->whereIn('LocationID',$locationlist)
   ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->where('SalesStatus','LIKE','%S%')
   ->distinct('CategoryDesc1')
   ->get()->toArray();
   $i=0;
   $items=[];
   foreach($categorydes1_array as $key => $value) {
   $query_items = DB::connection('mysql_source')
   ->table('sales_line')
   ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
   ->where('CompanyID','=',$company)
   ->where('ChainID','=',$chin_id)
   ->whereIn('LocationID',$locationlist)
   ->where('SalesStatus','LIKE','%S%')
   ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->where('CategoryDes1','=',$value->CategoryDes1)
   ->groupBy('ItemDesc')
   ->get();
   $query_items1 = json_decode(json_encode($query_items), true);
   $i=0;
   $query_items = array();
   $query_items_total['qty'] ='0';
      $query_items_total['total'] ='0';
      $total =0;
      foreach($query_items1 as $arr){
         $query_items[$i]['item'] = $arr['item'];
         $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
         $query_items[$i]['price'] = strval(number_format($arr['price'],2));
         $query_items_total['qty'] =$query_items_total['qty']+$arr['quantity'];
         $query_items_total['total'] =$query_items_total['total']+$arr['price'];
         $i++;
      }
         $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);


   $i=$i+1;
   }

   $data_payment_total = DB::connection('mysql_source')
   ->table('sales_line')
   ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
   ->where('CompanyID','=',$company)
   ->where('ChainID','=',$chin_id)
   ->where('SalesStatus','LIKE','%S%')
   ->whereIn('LocationID',$locationlist)
   ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->get();

   $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
   $data_payment_total = array();
   $i=0;
   foreach($data_payment_total1 as $arr){
   $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
   $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
   $i++;
   }
 }
   // -----------------Sales Items by Category 2--------------------------
if($selecctedcategory=='Category2') {
   $categorydesc2_array = DB::connection('mysql_source')
   ->table('sales_line')
   ->select('CategoryDes2')
   ->where('CompanyID','=',$company)
   ->where('ChainID','=',$chin_id)
   ->whereIn('LocationID',$locationlist)
   ->where('SalesStatus','LIKE','%S%')
   ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->distinct('CategoryDes2')
   ->get()->toArray();

   $i=0;
   $items2=[];
   foreach($categorydesc2_array as $key => $value) {
   $query_items23 = DB::connection('mysql_source')
   ->table('sales_line')
   ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
   ->where('CompanyID','=',$company)
   ->where('ChainID','=',$chin_id)
   ->where('SalesStatus','LIKE','%S%')
   ->whereIn('LocationID',$locationlist)
   ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->where('CategoryDes2','=',$value->CategoryDes2)
   ->groupBy('ItemDesc')
   ->get();
   $query_items12 = json_decode(json_encode($query_items23), true);
   $i=0;
   $query_items2 = array();
   $query_items_total2['qty'] ='0';
      $query_items_total2['total'] ='0';
      $total =0;
      foreach($query_items12 as $arr){
         $query_items2[$i]['item'] = $arr['item'];
         $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
         $query_items2[$i]['price'] = strval(number_format($arr['price'],2));
         $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
         $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
         $i++;
      }
         $items2[]=array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);


   $i=$i+1;
   }

   $data_payment_total2 = DB::connection('mysql_source')
   ->table('sales_line')
   ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
   ->where('CompanyID','=',$company)
   ->where('ChainID','=',$chin_id)
   ->where('SalesStatus','LIKE','%S%')
   ->whereIn('LocationID',$locationlist)
   ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->get();

   $data_payment_total12 = json_decode(json_encode($data_payment_total2), true);
   $data_payment_total1 = array();
   $i=0;
   foreach($data_payment_total12 as $arr){
   $data_payment_total1['Quantity'] = strval(round($arr['Quantity'],2));
   $data_payment_total1['Price'] = strval(round($arr['Price'],2));
   $i++;
   }
}
 //---------------------------------Hourly Sale---------------------------
 $query_items = DB::connection('mysql_source')
 ->table('sales_header')
 ->select(DB::raw('hour( StartTime ) as Hour'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(DISTINCT VoucherNo) as Transaction'),DB::raw('SUM(SalesAmount) as Sales'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('ChainID','=',$chin_id)
 ->where('VoucherStatus','LIKE','%S%')
 //->groupBy(DB::raw('hour(StartTime)'),DB::raw('day(StartTime)'))
 ->groupBy(DB::raw('hour(StartTime)'))
 ->get();
 $i=0;
 $data_hourly_sale=array();
 $hourly_sale ='0';
 $guesttotal  = '0';
 $trxtotal    = '0';
 $data_hourly_sale_total =array();
 foreach($query_items as $data){
  if($data->Hour  < 10)
 {
  $data->Hour = '0'.$data->Hour;
 }
 $to =$data->Hour+1;
 if($to <10 )
 {
  $to ='0'.$to;
 }
 $data_hourly_sale[$i]['Hours']=strval($data->Hour.':00 to '.$to.':00');
 $data_hourly_sale[$i]['Guest']=strval(round($data->Guest,2));
 $data_hourly_sale[$i]['Transcation']=strval(round($data->Transaction,2));
 $data_hourly_sale[$i]['Sales']=strval(number_format($data->Sales,2));
    $hourly_sale                  = $hourly_sale + round($data->Sales,2);
    $guesttotal                  = $guesttotal + round($data->Guest,2);
    $trxtotal                  = $trxtotal + round($data->Transaction,2);
    $i++;
  }
  $data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
  $data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
  $data_hourly_sale_total['trx']   = strval(round($trxtotal,2));
 //------------------------------------Credit Customer------------------------------------------
     $data_credit_customer= DB::connection('mysql_source')
     ->table('sales_payment')
    ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
    ->where('sales_payment.CompanyID','=',$company)
    ->whereIn('sales_payment.LocationID',$locationlist)
    ->where('sales_payment.ChainID','=',$chin_id)
    ->whereBetween('sales_payment.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
    ->where('sales_payment.CustomerName','!=','')
    ->groupBy('CustomerName','CardNo')
    ->get();
 //------------------------Sales Summary----------------------------------------------------------

 //------------------------Total Sales----------------------------------------------------------


 $data_total_sales= DB::connection('mysql_source')
 ->table('sales_line')
 ->select(DB::raw('SUM(Qty*SellingPrice) as TotalSales'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('SalesStatus','LIKE','%S%')
 ->get();
 $data_total_sales1 = json_decode(json_encode($data_total_sales), true);
 $data_total_sales = array();
 $i=0;
 foreach($data_total_sales1 as $arr){
 $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
 $i++;
 }
 //------------------------Delivery Charges----------------------------------------------------------

 $data_delivery_charge= DB::connection('mysql_source')
 ->table('sales_header')
 ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as DeliveryCharges'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('VoucherStatus','LIKE','%S%')
 ->where('OrderType','=','DL')
 ->get();

 $data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
 $data_delivery_charge = array();
 $i=0;
 foreach($data_delivery_charge1 as $arr){
 $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
 $i++;
 }



 //------------------------Service Charges----------------------------------------------------------

 $data_service_charge= DB::connection('mysql_source')
 ->table('sales_header')
 ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as ServiceCharges'))
 ->where('CompanyID','=',$company)
 ->where('ChainID','=',$chin_id)
 ->whereIn('LocationID',$locationlist)
 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('VoucherStatus','LIKE','%S%')
 ->where('OrderType','!=','DL')
 ->get();
 $data_service_charge1 = json_decode(json_encode($data_service_charge), true);
 $data_service_charge = array();
 $i=0;
 foreach($data_service_charge1 as $arr){
 $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
 $i++;
 }


 //------------------------Gross Sales----------------------------------------------------------


 $data_gross_sales['Gross Sale'] =strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));
 //------------------------Staff Meal----------------------------------------------------------
 $data_staff_meal= DB::connection('mysql_source')
 ->table('sales_line')
 ->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('round(SUM(Qty*SellingPrice),3) as Amount'))
 ->where('sales_line.CompanyID','=',$company)
 ->where('sales_line.ChainID','=',$chin_id)
 ->whereIn('sales_line.LocationID',$locationlist)
 ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('sales_header.OrderType','=','SM')
 ->leftJoin("sales_header",function($join){
 $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
 ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
 ->on('sales_line.CompanyID','=','sales_header.CompanyID')
 ->on('sales_line.ChainID','=','sales_header.ChainID')
 ->on('sales_line.LocationID','=','sales_header.LocationID');
 })
 ->get();

 $data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);
 $data_staff_meal = array();
 $i=0;
 foreach($data_staff_meal1 as $arr){
 $data_staff_meal['Count'] = strval(round($arr['Count'],2));
 if($data_staff_meal['Count']>1){
         $data_staff_meal['Count']='1';
     }
 $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
 $i++;
 }

 //------------------------Discount----------------------------------------------------------
  $data_discount= DB::connection('mysql_source')
  ->table('sales_payment')
  ->select(DB::raw('SUM(Amount) as Discount'))
  ->leftJoin("sales_header",function($join){
  $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
      ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
      ->on('sales_header.CompanyID','=','sales_payment.CompanyID')
      ->on('sales_header.ChainID','=','sales_payment.ChainID')
      ->on('sales_header.LocationID','=','sales_payment.LocationID');
  })
  ->leftJoin("payment_types",function($join1){
  $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
      ->on('sales_payment.ChainID','=','payment_types.ChainID')
      ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
  })
  ->where('sales_header.CompanyID','=',$company)
  ->where('sales_header.ChainID','=',$chin_id)
  ->whereBetween('sales_header.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
  ->whereIn('sales_payment.LocationID',$locationlist)
  ->where('sales_header.VoucherStatus','LIKE','%S%')
  ->where('sales_header.OrderType','!=','SM')
  ->where('payment_types.IsDiscountCard','=','1')
  ->get();
  $query = \DB::getQueryLog();
  //print_r($query); exit();
  $data_discount1 = json_decode(json_encode($data_discount), true);
  $data_discount = array();
  $i=0;
  foreach($data_discount1 as $arr){
     $data_discount['Discount'] = strval(round($arr['Discount'],2));
     $i++;
  }


 //------------------------VAT----------------------------------------------------------
  $data_vat= DB::connection('mysql_source')
  ->table('sales_header')
  ->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
  ->where('CompanyID','=',$company)
  ->where('ChainID','=',$chin_id)
  ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
  ->whereIn('LocationID',$locationlist)
  ->where('VoucherStatus','LIKE','%S%')
  ->get();
  $data_vat1 = json_decode(json_encode($data_vat), true);
  $data_vat = array();
  $i=0;
  foreach($data_vat1 as $arr){
     $data_vat['VAT'] = strval(round($arr['VAT'],2));
     $i++;
  }

 //------------------------Net Sales----------------------------------------------------------
 $data_net_sales['Net Sales'] =strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));

 //------------------------Taxable Value----------------------------------------------------------
 $data_tax['TaxableValue'] =strval(round($data_net_sales['Net Sales']-$data_vat['VAT'],2));


 //------------------------PayOut----------------------------------------------------------
 $data_payout['Payout']='0';
 //------------------------Credit Sales----------------------------------------------------------
 $data_creditsales = DB::connection('mysql_source')
 ->table('sales_payment')
  ->select(DB::raw('SUM(Amount) as CreditSales'))
  ->where('sales_header.CompanyID','=',$company)
  ->where('sales_header.ChainID','=',$chin_id)
  ->whereBetween('sales_header.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   ->whereIn('sales_header.LocationID',$locationlist)
  ->where('sales_header.VoucherStatus','LIKE','%S%')
  ->leftJoin("sales_header",function($join){
  $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
      ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
      ->on('sales_payment.CompanyID','=','sales_header.CompanyID')
      ->on('sales_payment.ChainID','=','sales_header.ChainID')
      ->on('sales_payment.LocationID','=','sales_header.LocationID');
  })
  ->leftJoin("payment_types",function($join1){
  $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
      ->on('sales_payment.ChainID','=','payment_types.ChainID')
      ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
  })
  ->where('payment_types.IsCreditCard','=','1')
  ->where('payment_types.IsDiscountCard','!=','1')
  ->where('sales_header.OrderType','!=','SM')
  ->get();
  $data_creditsales1 = json_decode(json_encode($data_creditsales), true);
  $data_creditsales = array();
  $i=0;
  foreach($data_creditsales1 as $arr){
     $data_creditsales['CreditSales'] = strval(round($arr['CreditSales'],2));
     $i++;
  }
  $data_locationlist= DB::connection('mysql_source')
  ->table('sales_header')
  ->select('LocationID')
  ->groupBy('LocationID')
  ->get();
 //------------------------Actual Banking----------------------------------------------------------
 $data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

 //------------------------Voids----------------------------------------------------------


//item order item
 $data_items_order = DB::connection('mysql_source')
 ->table('sales_line')
 ->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
 ->leftJoin("sales_header",function($join){
  $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
      ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
      ->on('sales_header.CompanyID','=','sales_line.CompanyID')
      ->on('sales_header.ChainID','=','sales_line.ChainID')
      ->on('sales_header.LocationID','=','sales_line.LocationID');
  })
 ->where('sales_line.CompanyID','=',$company)
 ->where('sales_line.ChainID','=',$chin_id)
 ->whereIn('sales_line.LocationID',$locationlist)
 ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('sales_header.VoucherStatus','LIKE','%V%')
 ->get();

 $data_items_order1 = json_decode(json_encode($data_items_order), true);
 $data_items_order = array();
 $i=0;
 foreach($data_items_order1 as $arr){
 $data_items_order['Count'] = strval(round($arr['Count'],2));
 $data_items_order['Amount'] = strval(round($arr['Amount'],2));
 $i++;
 }
 //item  total
 $data_void_items_total = DB::connection('mysql_source')
 ->table('sales_line')
 ->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
  ->leftJoin("sales_header",function($join){
  $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
      ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
      ->on('sales_header.CompanyID','=','sales_line.CompanyID')
      ->on('sales_header.ChainID','=','sales_line.ChainID')
      ->on('sales_header.LocationID','=','sales_line.LocationID');
  })
 ->where('sales_line.CompanyID','=',$company)
 ->where('sales_line.ChainID','=',$chin_id)
 ->whereIn('sales_line.LocationID',$locationlist)
 ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
 ->where('sales_line.SalesStatus','LIKE','%V%')
 ->get();
 $data_void_items_total1 = json_decode(json_encode($data_void_items_total), true);
 $data_void_items_total = array();
 $i=0;
 foreach($data_void_items_total1 as $arr){
 $data_void_items_total['Count'] = strval(round($arr['Count'],2));
 $data_void_items_total['Amount'] = strval(round($arr['Amount'],2));
 $i++;
 }
 //item void
 $data_items_void = array();
 $data_items_void['Amount'] = strval(round($data_void_items_total['Amount']-$data_items_order['Amount'],3));
  if($data_items_void['Amount']>0){
     $data_items_void['Count'] ="1";
 }else{
     $data_items_void['Count'] ="0";
 }
 //------------------------Total of Voids And Staff Meal----------------------------------------------------------
  $data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount']+@$data_void_items_total['Amount'],3));


  //----------------------------------------------Chain---------------------------------------------------------------
  $chain=DB::connection('mysql_source')
  ->table('chainaccess')
  ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
 ->select('chainaccess.ChainId','chain.ChainName')
 ->where('chainaccess.CompanyId','=',$company)
 ->groupBy('chainaccess.ChainId')
 ->groupBy('chain.ChainName')
 ->get();
//----------------------------------------------Location---------------------------------------------------------------
 $location=DB::connection('mysql_source')
 ->table('locationaccess')
 ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
  ->select('locationaccess.LocationId','LocationName')
  ->where('locationaccess.ChainId','=',$chin_id)
  ->groupBy('locationaccess.LocationId')
  ->groupBy('location.LocationName')
  ->get();

  DB::commit();
  DB::disconnect('mysql_source');
  if($selecctedcategory=='Category1'){
      echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
  }else if($selecctedcategory=='Category2'){
      echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items2,"Items_total"=>$data_payment_total1,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
  }
}

public function all_dataonchange_testing(Request $request){

    $datefrom=$request->datefrom;
    $dateto=$request->dateto;
    $chin_id = $request->chain;
    $location_id =$request->location;
    $pass   = $request->pass;
    $selecctedcategory =$request->category;
    if($selecctedcategory=='') {
         $selecctedcategory ='Category1';
    }

   if($datefrom > $dateto){
      $datef=$datefrom;
      $datefrom = $dateto;
      $dateto   = $datef;
    }


    @$location =explode(',',$location_id);
    if(!empty($location)){
      foreach($location as $loc){
        if($loc!='') $locationlist[]=$loc;
      }
    }
   //print_r($locationlist); exit();
  $userid =$this->checkAuth($request);
  if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
  $user = UserDetails::where('id','=',$userid->user_id)->first();
  $company_id = $user->CompanyID;
  $company    = @$user->CompanyID;
  $username   = @$user->username;
  $userdata   = array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);
  // $company=$company_id[0]['CompanyID'];

  $this->getConnectionDetails($user->CompanyID);
  \DB::enableQueryLog();
  // -----------------order type----------------------
  $data_ordertype = DB::connection('mysql_source')
                        ->table('sales_header')
                        ->select('OrderType',DB::raw('round(SUM(SalesAmount),3) as Sales'),DB::raw('round(SUM(GuestCount),3) as Guest'),DB::raw('COUNT(VoucherNo) as Transaction'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->where('VoucherStatus','LIKE','%S%')
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->groupBy('OrderType')
                        ->get();

  //print_r($data_ordertype); exit();
  $data_ordertype1      = json_decode(json_encode($data_ordertype), true);
  $data_ordertype_total = array();
  $i=0;
  $data_ordertype=array();
  $ototal ='0';
  $trxtotal ='0';
  $guesttotal = '0';
  foreach($data_ordertype1 as $arr){
      $data_ordertype[$i]['OrderType']   = $arr['OrderType'];
      $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
      $data_ordertype[$i]['Guest']       = strval(round($arr['Guest'],2));
      $data_ordertype[$i]['Sales']       = strval(number_format($arr['Sales'],2));
      $ototal      = $ototal +$arr['Sales'];
      $trxtotal    = $trxtotal +$data_ordertype[$i]['Transaction'];
      $guesttotal  = $guesttotal +$data_ordertype[$i]['Guest'];
      $i++;
  }
  $data_ordertype_total['Sales'] = strval(number_format($ototal,2));
  $data_ordertype_total['Trx']   = strval(round($trxtotal,2));
  $data_ordertype_total['Guest'] = strval(round($guesttotal,2));

  // ---------------Category 1-----------------------
  //   $query_category1=$this->db->query("select CategoryDes1, sum(SellingPrice) as sales, sum(Qty) as Guests from sales_line where CompanyID='$company' and DATE(VoucherDate) = '$date_only' group by CategoryDes1");
  //   $data_category1=$query_category1->result();

  $data_category1 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes1',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(sum(Qty),3) as Guest'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->where('SalesStatus','LIKE','%S%')
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->groupBy('CategoryDes1')
                        ->get();
  $data_category11 = json_decode(json_encode($data_category1), true);
  $i=0;
  $data_category1 = array();
  $data_category1_total = array();
  $guest                = 0;
  $sales                = 0;
  foreach($data_category11 as $arr){
      $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
      $data_category1[$i]['Guests'] = strval(round($arr['Guest'],2));
      $data_category1[$i]['sales'] = strval(number_format($arr['sales'],2));
      $guest                       = $guest + $data_category1[$i]['Guests'];
      $sales                       = $sales + $arr['sales'];
      $i++;
  }
  $data_category1_total['Guests'] = strval(round($guest,2));
  $data_category1_total['sales']  = strval(number_format($sales,2));

  // --------------Payment---------------------------
     $payment_type = DB::connection('mysql_source')
                        ->table('sales_payment')
                           ->select('PaymentType')
                           ->where('CompanyID','=',$company)
                           ->whereIn('LocationID',$locationlist)
                           ->where('ChainID','=',$chin_id)
                           ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                           ->groupBy('PaymentType')
                           ->get();

      // $payment_type=array("CASH","VISA CARD",'MASTER CARD','STAFF DISCOUNT 30','ONLINE PAYMENTS');
       $i=0;
       $totalDiscount = 0;
       $creditSaleTotal = 0;
     foreach ($payment_type as $value) {

         @$query_paymenttype = DB::connection('mysql_source')
                                 ->table('sales_payment')
                                 ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
                                 ->where('CompanyID','=',$company)
                                 ->whereIn('LocationID',$locationlist)
                                 ->where('ChainID','=',$chin_id)
                                 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                                 ->where('PaymentType','=',$value->PaymentType)
                                 ->groupBy('PaymentType')
                                 ->get();

         $quer = @$query_paymenttype[0]->Amount;
         $tcount = @$query_paymenttype[0]->Count;
         $id=2;

         @$typevalue = DB::connection('mysql_source')
                           ->table('payment_types')
                           ->select('*')
                           ->where('CompanyID','=',$company)
                           ->where('ChainID','=',$chin_id)
                           ->where('PaymentType','=',$value->PaymentType)
                           ->get();
         $quer   = @$query_paymenttype[0]->Amount;
         $tcount = @$query_paymenttype[0]->Count;

         /***************************** Payment Types *************************/
         if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
            $id = 1;
            $totalDiscount += $quer;
         }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCCard == '1')){
            $id = 2;
            $creditSaleTotal += $quer;
         }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCustCredit == '1')){
            $id = 3;
            $creditSaleTotal += $quer;
         }else if((@$typevalue[0]->IsStaffMeal== '1')){
            $id = 4;
         }else{
            $id = 5;
         }
         if($value->PaymentType!=''){
           $data_paymenttype[]=array("Group Id"=>$id,"Payment Type"=>$value->PaymentType,"Amount"=>strval(round($quer,2)),"Count"=>strval(round($tcount,2)));
         }
         $i++;
       }
       $visa_amount['Visa Amount']     = strval(round(@$data_paymenttype[1]['Amount'],2));
       $master_amount['Master Amount'] = strval(round(@$data_paymenttype[2]['Amount'],2));
       $data_visamastertotal[]         = array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));

  // ---------------Category 2-----------------------
  $data_category2 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('SalesStatus','LIKE','%S%')
                        ->groupBy('CategoryDes2')
                        ->get();
   $data_category12 = json_decode(json_encode($data_category2), true);
   $i =  0;
   $data_category2 = array();
  foreach($data_category12 as $arr){
      $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
      $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
      $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
      $i++;
  }

  $data_category2_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->where('SalesStatus','LIKE','%S%')
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();
  $data_category2_total2 = json_decode(json_encode($data_category2_total), true);
  $data_category2_total  = array();
  $i = 0;
  foreach($data_category2_total2 as $arr){
      $data_category2_total['Quantity'] = strval(round($arr['Quantity'],2));
      $data_category2_total['price'] = strval(number_format($arr['sales'],2));
      $i++;
  }


  // -----------------Sales Items by Category 1--------------------------
  if($selecctedcategory=='Category1') {

    $categorydes1_array = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('CategoryDes1')
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('SalesStatus','LIKE','%S%')
                              ->distinct('CategoryDesc1')
                              ->get()->toArray();
    $i = 0;
    $items = [];
    foreach($categorydes1_array as $key => $value) {
    $query_items = DB::connection('mysql_source')
                     ->table('sales_line')
                     ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
                     ->where('CompanyID','=',$company)
                     ->where('ChainID','=',$chin_id)
                     ->whereIn('LocationID',$locationlist)
                     ->where('SalesStatus','LIKE','%S%')
                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                     ->where('CategoryDes1','=',$value->CategoryDes1)
                     ->groupBy('ItemDesc')
                     ->get();
       $query_items1 = json_decode(json_encode($query_items), true);
       $i=0;
       $query_items = array();
       $query_items_total['qty'] ='0';
       $query_items_total['total'] ='0';
       $total =0;
       foreach($query_items1 as $arr){
          $query_items[$i]['item'] = $arr['item'];
          $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
          $query_items[$i]['price'] = strval(number_format($arr['price'],2));
          $query_items_total['qty'] =$query_items_total['qty']+$arr['quantity'];
          $query_items_total['total'] =$query_items_total['total']+$arr['price'];
          $i++;
       }
       $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);
       $i=$i+1;
    }

    $data_payment_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('SalesStatus','LIKE','%S%')
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();

    $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
    $data_payment_total  = array();
    $i=0;
    foreach($data_payment_total1 as $arr){
      $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
      $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
      $i++;
    }
  }
    // -----------------Sales Items by Category 2--------------------------
 if($selecctedcategory=='Category2') {
    $categorydesc2_array = DB::connection('mysql_source')
    ->table('sales_line')
    ->select('CategoryDes2')
    ->where('CompanyID','=',$company)
    ->where('ChainID','=',$chin_id)
    ->whereIn('LocationID',$locationlist)
    ->where('SalesStatus','LIKE','%S%')
    ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
    ->distinct('CategoryDes2')
    ->get()->toArray();

    $i=0;
    $items2=[];
    foreach($categorydesc2_array as $key => $value) {
    $query_items23 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->where('SalesStatus','LIKE','%S%')
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('CategoryDes2','=',$value->CategoryDes2)
                        ->groupBy('ItemDesc')
                        ->get();
         $query_items12  = json_decode(json_encode($query_items23), true);
         $i = 0;
         $query_items2 = array();
         $query_items_total2['qty'] ='0';
         $query_items_total2['total'] ='0';
         $total =0;
         foreach($query_items12 as $arr){
            $query_items2[$i]['item'] = $arr['item'];
            $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items2[$i]['price'] = strval(number_format($arr['price'],2));
            $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
            $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
            $i++;
         }
         $items2[]=array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);
         $i=$i+1;
    }

    $data_payment_total2 = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('SalesStatus','LIKE','%S%')
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();

    $data_payment_total12 = json_decode(json_encode($data_payment_total2), true);
    $data_payment_total1  = array();
    $i=0;
    foreach($data_payment_total12 as $arr){
      $data_payment_total1['Quantity'] = strval(round($arr['Quantity'],2));
      $data_payment_total1['Price'] = strval(round($arr['Price'],2));
      $i++;
    }
 }
  //---------------------------------Hourly Sale---------------------------
  $datefrom = $datefrom." 00:00:00";
  $dateto   = $dateto." 23:59:59";
  $query_items = DB::connection('mysql_source')
                    // ->table('sales_header')
                    // ->select(DB::raw('hour( StartTime ) as Hour'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(DISTINCT VoucherNo) as Transaction'),DB::raw('SUM(SalesAmount) as Sales'))
                    // ->where('CompanyID','=',$company)
                    // ->where('ChainID','=',$chin_id)
                    // ->whereIn('LocationID',$locationlist)
                    // ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                    // ->where('VoucherStatus','LIKE','%S%')
                    // //->groupBy(DB::raw('hour(StartTime)'),DB::raw('day(StartTime)'))
                    // ->groupBy(DB::raw('hour(StartTime)'))
                    // ->get();
                    ->table('sales_line')
                    ->select(DB::raw('hour( sales_header.StartTime ) as Hour'),
                      DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` BETWEEN "'.$datefrom.'" AND "'.$dateto.'" AND `CompanyID`= "'.$company.'" AND `LocationID` IN ("' . implode('","', $locationlist) . '")  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND hour(`StartTime`) =  Hour) as Guest'),
                      DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'),
                      DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'))
                    ->leftJoin("sales_header",function($join){
                    $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                    })
                    ->where('sales_line.CompanyID','=',$company)
                    ->where('sales_line.ChainID','=',$chin_id)
                    ->whereIn('sales_line.LocationID',$locationlist)
                    ->whereBetween('sales_line.VoucherDate', [$datefrom, $dateto])
                    ->where('sales_line.SalesStatus','S')
                    ->groupBy(DB::raw('hour(sales_header.StartTime)'))
                    ->get();
//   $query_items = DB::connection('mysql_source')
//                     ->table('sales_header')
//                     ->select(DB::raw('hour( StartTime ) as Hour'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(DISTINCT VoucherNo) as Transaction'),DB::raw('SUM(SalesAmount) as Sales'))
//                     ->where('CompanyID','=',$company)
//                     ->where('ChainID','=',$chin_id)
//                     ->whereIn('LocationID',$locationlist)
//                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
//                     ->where('ChainID','=',$chin_id)
//                     ->where('VoucherStatus','LIKE','%S%')
//                     //->groupBy(DB::raw('hour(StartTime)'),DB::raw('day(StartTime)'))
//                     ->groupBy(DB::raw('hour(StartTime)'))
//                     ->get();
  $i=0;
  $data_hourly_sale=array();
  $hourly_sale ='0';
  $guesttotal  = '0';
  $trxtotal    = '0';
  $data_hourly_sale_total =array();
  foreach($query_items as $data){
      if($data->Hour  < 10){
         $data->Hour = '0'.$data->Hour;
      }
      $to =$data->Hour+1;
      if($to <10 ){
         $to ='0'.$to;
      }
      $data_hourly_sale[$i]['Hours']       = strval($data->Hour.':00 to '.$to.':00');
      $data_hourly_sale[$i]['Guest']       = strval(round($data->Guest,2));
      $data_hourly_sale[$i]['Transcation'] = strval(round($data->Transaction,2));
      $data_hourly_sale[$i]['Sales'] = strval(number_format($data->Sales,2));
      $hourly_sale = $hourly_sale + round($data->Sales,2);
      $guesttotal  = $guesttotal + round($data->Guest,2);
      $trxtotal    = $trxtotal + round($data->Transaction,2);
      $i++;
   }
   $data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
   $data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
   $data_hourly_sale_total['trx']   = strval(round($trxtotal,2));

  //------------------------------------Credit Customer------------------------------------------
   $data_credit_customer = DB::connection('mysql_source')
                                 ->table('sales_payment')
                                 ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
                                 ->where('sales_payment.CompanyID','=',$company)
                                 ->whereIn('sales_payment.LocationID',$locationlist)
                                 ->where('sales_payment.ChainID','=',$chin_id)
                                 ->whereBetween('sales_payment.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                                 ->where('sales_payment.CustomerName','!=','')
                                 ->groupBy('CustomerName','CardNo')
                                 ->get();
  //------------------------Sales Summary----------------------------------------------------------

  //------------------------Total Sales----------------------------------------------------------
   $data_total_sales= DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('SUM(Qty*SellingPrice) as TotalSales'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('SalesStatus','LIKE','%S%')
                        ->get();
  $data_total_sales1 = json_decode(json_encode($data_total_sales), true);
  $data_total_sales = array();
  $i=0;
  foreach($data_total_sales1 as $arr){
      $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
      $i++;
  }
  //------------------------Delivery Charges----------------------------------------------------------

  $data_delivery_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as DeliveryCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','=','DL')
                              ->get();

  $data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
  $data_delivery_charge  = array();
  $i=0;
  foreach($data_delivery_charge1 as $arr){
      $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
      $i++;
  }



  //------------------------Service Charges----------------------------------------------------------

  $data_service_charge  = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as ServiceCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','!=','DL')
                              ->get();
  $data_service_charge1 = json_decode(json_encode($data_service_charge), true);
  $data_service_charge  = array();
  $i=0;
  foreach($data_service_charge1 as $arr){
      $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
      $i++;
  }


  //------------------------Gross Sales----------------------------------------------------------

  $data_gross_sales['Gross Sale'] = strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));
  //------------------------Staff Meal----------------------------------------------------------
  $data_staff_meal= DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('round(SUM(Qty*SellingPrice),3) as Amount'))
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->whereIn('sales_line.LocationID',$locationlist)
                        ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('sales_header.OrderType','=','SM')
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->get();

  $data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);
  $data_staff_meal = array();
  $i=0;
  foreach($data_staff_meal1 as $arr){
      $data_staff_meal['Count'] = strval(round($arr['Count'],2));
      if($data_staff_meal['Count']>1){

         $data_staff_meal['Count']='1';
      }
      $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
      $i++;
  }

  //------------------------Discount----------------------------------------------------------
   // $data_discount= DB::connection('mysql_source')
   // ->table('sales_payment')
   // ->select(DB::raw('SUM(Amount) as Discount'))
   // ->leftJoin("sales_header",function($join){
   // $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
   //     ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
   //     ->on('sales_header.CompanyID','=','sales_payment.CompanyID')
   //     ->on('sales_header.ChainID','=','sales_payment.ChainID')
   //     ->on('sales_header.LocationID','=','sales_payment.LocationID');
   // })
   // ->leftJoin("payment_types",function($join1){
   // $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
   //     ->on('sales_payment.ChainID','=','payment_types.ChainID')
   //     ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
   // })
   // ->where('sales_header.CompanyID','=',$company)
   // ->where('sales_header.ChainID','=',$chin_id)
   // ->whereBetween('sales_header.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   // ->whereIn('sales_payment.LocationID',$locationlist)
   // ->where('sales_header.VoucherStatus','LIKE','%S%')
   // ->where('sales_header.OrderType','!=','SM')
   // ->where('payment_types.IsDiscountCard','=','1')
   // ->get();
   // $query = \DB::getQueryLog();
   // //print_r($query); exit();
   // $data_discount1 = json_decode(json_encode($data_discount), true);
   // $data_discount = array();
   // $i=0;
   // foreach($data_discount1 as $arr){
   //    // $data_discount['Discount'] = strval(round($arr['Discount'],2));
   //    $i++;
   // }
   $data_discount['Discount'] = strval(round($totalDiscount, 2));

  //------------------------VAT----------------------------------------------------------
   $data_vat   = DB::connection('mysql_source')
                     ->table('sales_header')
                     ->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
                     ->where('CompanyID','=',$company)
                     ->where('ChainID','=',$chin_id)
                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                     ->whereIn('LocationID',$locationlist)
                     ->where('VoucherStatus','S')
                     ->get();
   $data_vat1 = json_decode(json_encode($data_vat), true);
   $data_vat = array();
   $i=0;
   foreach($data_vat1 as $arr){
      $data_vat['VAT'] = strval(round($arr['VAT'],2));
      $i++;
   }

  //------------------------Net Sales----------------------------------------------------------
  $data_net_sales['Net Sales'] = strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));

  //------------------------Taxable Value----------------------------------------------------------
  $data_tax['TaxableValue'] = strval(round($data_net_sales['Net Sales']-$data_vat['VAT'],2));


  //------------------------PayOut----------------------------------------------------------
  $data_payout['Payout']='0';
  //------------------------Credit Sales----------------------------------------------------------
//   $data_creditsales = DB::connection('mysql_source')
//   ->table('sales_payment')
//    ->select(DB::raw('SUM(Amount) as CreditSales'))
//    ->where('sales_header.CompanyID','=',$company)
//    ->where('sales_header.ChainID','=',$chin_id)
//    ->whereBetween('sales_header.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
//     ->whereIn('sales_header.LocationID',$locationlist)
//    ->where('sales_header.VoucherStatus','LIKE','%S%')
//    ->leftJoin("sales_header",function($join){
//    $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
//        ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
//        ->on('sales_payment.CompanyID','=','sales_header.CompanyID')
//        ->on('sales_payment.ChainID','=','sales_header.ChainID')
//        ->on('sales_payment.LocationID','=','sales_header.LocationID');
//    })
//    ->leftJoin("payment_types",function($join1){
//    $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
//        ->on('sales_payment.ChainID','=','payment_types.ChainID')
//        ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
//    })
//    ->where('payment_types.IsCreditCard','=','1')
//    ->where('payment_types.IsDiscountCard','!=','1')
//    ->where('sales_header.OrderType','!=','SM')
//    ->get();
//    $data_creditsales1 = json_decode(json_encode($data_creditsales), true);
//    $data_creditsales = array();
//    $i=0;
//    foreach($data_creditsales1 as $arr){
//     //   $data_creditsales['CreditSales'] = strval(round($arr['CreditSales'],2));
//       $i++;
//    }
   $data_creditsales['CreditSales'] = strval(round($creditSaleTotal, 2));

   $data_locationlist= DB::connection('mysql_source')
                           ->table('sales_header')
                           ->select('LocationID')
                           ->groupBy('LocationID')
                           ->get();
  //------------------------Actual Banking----------------------------------------------------------
  $data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

  //------------------------Voids----------------------------------------------------------

  //item order item
  $data_items_order = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
                           ->leftJoin("sales_header",function($join){
                              $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                 ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                 ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                 ->on('sales_header.ChainID','=','sales_line.ChainID')
                                 ->on('sales_header.LocationID','=','sales_line.LocationID');
                              })
                           ->where('sales_line.CompanyID','=',$company)
                           ->where('sales_line.ChainID','=',$chin_id)
                           ->whereIn('sales_line.LocationID',$locationlist)
                           ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                           ->where('sales_header.VoucherStatus', 'V')
                           ->get();

  $data_items_order1 = json_decode(json_encode($data_items_order), true);
  $data_items_order  = array();
  $i=0;
  foreach($data_items_order1 as $arr){
      $data_items_order['Count'] = strval(round($arr['Count'],2));
      $data_items_order['Amount'] = strval(round($arr['Amount'],2));
      $i++;
  }
  //item  total
  $data_void_items_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
                                 ->leftJoin("sales_header",function($join){
                                 $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                    ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                    ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                    ->on('sales_header.ChainID','=','sales_line.ChainID')
                                    ->on('sales_header.LocationID','=','sales_line.LocationID');
                                 })
                              ->where('sales_line.CompanyID','=',$company)
                              ->where('sales_line.ChainID','=',$chin_id)
                              ->whereIn('sales_line.LocationID',$locationlist)
                              ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('sales_line.SalesStatus', 'V')
                              ->first();
//   $data_void_items_total1 = json_decode(json_encode($data_void_items_total), true);
//   $data_void_items_total = array();
//   $i=0;
//   foreach($data_void_items_total1 as $arr){
//   $data_void_items_total['Count'] = strval(round($arr['Count'],2));
//   $data_void_items_total['Amount'] = strval(round($arr['Amount'],2));
//   $i++;
//   }
  //item void
  $data_items_void           = array();
  $data_items_void['Amount'] = strval(round($data_void_items_total->Amount,3)) ?? '0';
  $data_items_void['Count']  = strval(($data_void_items_total->Count)) ?? '0';
  //------------------------Total of Voids And Staff Meal----------------------------------------------------------
  $data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount']+@$data_items_void['Amount'],3));


   //----------------------------------------------Chain---------------------------------------------------------------
   $chain   =DB::connection('mysql_source')
               ->table('chainaccess')
               ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
               ->select('chainaccess.ChainId','chain.ChainName')
               ->where('chainaccess.CompanyId','=',$company)
               ->groupBy('chainaccess.ChainId')
               ->groupBy('chain.ChainName')
               ->get();
               //----------------------------------------------Location---------------------------------------------------------------
               $location=DB::connection('mysql_source')
               ->table('locationaccess')
               ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
               ->select('locationaccess.LocationId','LocationName')
               ->where('locationaccess.ChainId','=',$chin_id)
               ->groupBy('locationaccess.LocationId')
               ->groupBy('location.LocationName')
               ->get();

   DB::commit();
   DB::disconnect('mysql_source');
   if($selecctedcategory=='Category1'){
       echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
   }else if($selecctedcategory=='Category2'){
       echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items2,"Items_total"=>$data_payment_total1,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
   }
 }

 public function all_dataonchange_testing2(Request $request){

    $datefrom=$request->datefrom;
    $dateto=$request->dateto;
    $chin_id = $request->chain;
    $location_id =$request->location;
    $pass   = $request->pass;
    $selecctedcategory =$request->category;
    if($selecctedcategory=='') {
         $selecctedcategory ='Category1';
    }

   if($datefrom > $dateto){
      $datef=$datefrom;
      $datefrom = $dateto;
      $dateto   = $datef;
    }


    @$location =explode(',',$location_id);
    if(!empty($location)){
      foreach($location as $loc){
        if($loc!='') $locationlist[]=$loc;
      }
    }
   //print_r($locationlist); exit();
  $userid =$this->checkAuth($request);
  if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
  $user = UserDetails::where('id','=',$userid->user_id)->first();
  $company_id = $user->CompanyID;
  $company    = @$user->CompanyID;
  $username   = @$user->username;
  $userdata   = array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);
  // $company=$company_id[0]['CompanyID'];

  $this->getConnectionDetails($user->CompanyID);
  \DB::enableQueryLog();
  // -----------------order type----------------------
  $datefrom = $datefrom." 00:00:00";
  $dateto   = $dateto." 23:59:59";
  $data_ordertype = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                        DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` BETWEEN "'.$datefrom.'" AND "'.$dateto.'" AND `CompanyID`= "'.$company.'" AND `LocationID` IN ("' . implode('","', $locationlist) . '")  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                        DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                            ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                            ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                            ->on('sales_line.ChainID','=','sales_header.ChainID')
                            ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.CompanyID','=', $company)
                        ->where('sales_line.ChainID','=', $chin_id)
                        ->whereIn('sales_line.LocationID', $locationlist)
                        ->whereBetween('sales_line.VoucherDate', [$datefrom, $dateto])
                        ->where('sales_line.SalesStatus','S')
                        ->groupBy('sales_header.OrderType')
                        ->get();
  $data_ordertype1      = json_decode(json_encode($data_ordertype), true);
  $data_ordertype_total = array();
  $i=0;
  $data_ordertype=array();
  $ototal ='0';
  $trxtotal ='0';
  $guesttotal = '0';
  foreach($data_ordertype1 as $arr){
      $data_ordertype[$i]['OrderType']   = $arr['Type'];
      $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
      $data_ordertype[$i]['Guest']       = strval(round($arr['Guest'],2));
      $data_ordertype[$i]['Sales']       = strval(number_format($arr['Sales'],2));
      $ototal      = $ototal +$arr['Sales'];
      $trxtotal    = $trxtotal +$data_ordertype[$i]['Transaction'];
      $guesttotal  = $guesttotal +$data_ordertype[$i]['Guest'];
      $i++;
  }
  $data_ordertype_total['Sales'] = strval(number_format($ototal,2));
  $data_ordertype_total['Trx']   = strval(round($trxtotal,2));
  $data_ordertype_total['Guest'] = strval(round($guesttotal,2));

  // ---------------Category 1-----------------------
  //   $query_category1=$this->db->query("select CategoryDes1, sum(SellingPrice) as sales, sum(Qty) as Guests from sales_line where CompanyID='$company' and DATE(VoucherDate) = '$date_only' group by CategoryDes1");
  //   $data_category1=$query_category1->result();

  $data_category1 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes1',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(sum(Qty),3) as Guest'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->where('SalesStatus','LIKE','%S%')
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->groupBy('CategoryDes1')
                        ->get();
  $data_category11 = json_decode(json_encode($data_category1), true);
  $i=0;
  $data_category1 = array();
  $data_category1_total = array();
  $guest                = 0;
  $sales                = 0;
  foreach($data_category11 as $arr){
      $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
      $data_category1[$i]['Guests'] = strval(round($arr['Guest'],2));
      $data_category1[$i]['sales'] = strval(number_format($arr['sales'],2));
      $guest                       = $guest + $data_category1[$i]['Guests'];
      $sales                       = $sales + $arr['sales'];
      $i++;
  }
  $data_category1_total['Guests'] = strval(round($guest,2));
  $data_category1_total['sales']  = strval(number_format($sales,2));

  // --------------Payment---------------------------
     $payment_type = DB::connection('mysql_source')
                        ->table('sales_payment')
                           ->select('PaymentType')
                           ->where('CompanyID','=',$company)
                           ->whereIn('LocationID',$locationlist)
                           ->where('ChainID','=',$chin_id)
                           ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                           ->groupBy('PaymentType')
                           ->get();
       $i=0;
       $totalDiscount = 0;
       $creditSaleTotal = 0;
     foreach ($payment_type as $value) {

         @$query_paymenttype = DB::connection('mysql_source')
                                 ->table('sales_payment')
                                 ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
                                 ->where('CompanyID','=',$company)
                                 ->whereIn('LocationID',$locationlist)
                                 ->where('ChainID','=',$chin_id)
                                 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                                 ->where('PaymentType','=',$value->PaymentType)
                                 ->groupBy('PaymentType')
                                 ->get();

         $quer = @$query_paymenttype[0]->Amount;
         $tcount = @$query_paymenttype[0]->Count;
         $id=2;

         @$typevalue = DB::connection('mysql_source')
                           ->table('payment_types')
                           ->select('*')
                           ->where('CompanyID','=',$company)
                           ->where('ChainID','=',$chin_id)
                           ->where('PaymentType','=',$value->PaymentType)
                           ->get();
         $quer   = @$query_paymenttype[0]->Amount;
         $tcount = @$query_paymenttype[0]->Count;

         /***************************** Payment Types *************************/
         if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
            $id = 1;
            $totalDiscount += $quer;
         }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCCard == '1')){
            $id = 2;
            $creditSaleTotal += $quer;
         }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCustCredit == '1')){
            $id = 3;
            $creditSaleTotal += $quer;
         }else if((@$typevalue[0]->IsStaffMeal== '1')){
            $id = 4;
         }else{
            $id = 5;
         }
         if($value->PaymentType!=''){
           $data_paymenttype[]=array("Group Id"=>$id,"Payment Type"=>$value->PaymentType,"Amount"=>strval(round($quer,2)),"Count"=>strval(round($tcount,2)));
         }
         $i++;
       }
       $visa_amount['Visa Amount']     = strval(round(@$data_paymenttype[1]['Amount'],2));
       $master_amount['Master Amount'] = strval(round(@$data_paymenttype[2]['Amount'],2));
       $data_visamastertotal[]         = array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));

  // ---------------Category 2-----------------------
  $data_category2 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('SalesStatus','LIKE','%S%')
                        ->groupBy('CategoryDes2')
                        ->get();
   $data_category12 = json_decode(json_encode($data_category2), true);
   $i =  0;
   $data_category2 = array();
  foreach($data_category12 as $arr){
      $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
      $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
      $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
      $i++;
  }

  $data_category2_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->where('SalesStatus','LIKE','%S%')
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();
  $data_category2_total2 = json_decode(json_encode($data_category2_total), true);
  $data_category2_total  = array();
  $i = 0;
  foreach($data_category2_total2 as $arr){
      $data_category2_total['Quantity'] = strval(round($arr['Quantity'],2));
      $data_category2_total['price'] = strval(number_format($arr['sales'],2));
      $i++;
  }


  // -----------------Sales Items by Category 1--------------------------
  if($selecctedcategory=='Category1') {

    $categorydes1_array = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('CategoryDes1')
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('SalesStatus','LIKE','%S%')
                              ->distinct('CategoryDesc1')
                              ->get()->toArray();
    $i = 0;
    $items = [];
    foreach($categorydes1_array as $key => $value) {
    $query_items = DB::connection('mysql_source')
                     ->table('sales_line')
                     ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
                     ->where('CompanyID','=',$company)
                     ->where('ChainID','=',$chin_id)
                     ->whereIn('LocationID',$locationlist)
                     ->where('SalesStatus','LIKE','%S%')
                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                     ->where('CategoryDes1','=',$value->CategoryDes1)
                     ->groupBy('ItemDesc')
                     ->get();
       $query_items1 = json_decode(json_encode($query_items), true);
       $i=0;
       $query_items = array();
       $query_items_total['qty'] ='0';
       $query_items_total['total'] ='0';
       $total =0;
       foreach($query_items1 as $arr){
          $query_items[$i]['item'] = $arr['item'];
          $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
          $query_items[$i]['price'] = strval(number_format($arr['price'],2));
          $query_items_total['qty'] =$query_items_total['qty']+$arr['quantity'];
          $query_items_total['total'] =$query_items_total['total']+$arr['price'];
          $i++;
       }
       $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);
       $i=$i+1;
    }

    $data_payment_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('SalesStatus','LIKE','%S%')
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();

    $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
    $data_payment_total  = array();
    $i=0;
    foreach($data_payment_total1 as $arr){
      $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
      $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
      $i++;
    }
  }
    // -----------------Sales Items by Category 2--------------------------
 if($selecctedcategory=='Category2') {
    $categorydesc2_array = DB::connection('mysql_source')
    ->table('sales_line')
    ->select('CategoryDes2')
    ->where('CompanyID','=',$company)
    ->where('ChainID','=',$chin_id)
    ->whereIn('LocationID',$locationlist)
    ->where('SalesStatus','LIKE','%S%')
    ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
    ->distinct('CategoryDes2')
    ->get()->toArray();

    $i=0;
    $items2=[];
    foreach($categorydesc2_array as $key => $value) {
    $query_items23 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->where('SalesStatus','LIKE','%S%')
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('CategoryDes2','=',$value->CategoryDes2)
                        ->groupBy('ItemDesc')
                        ->get();
         $query_items12  = json_decode(json_encode($query_items23), true);
         $i = 0;
         $query_items2 = array();
         $query_items_total2['qty'] ='0';
         $query_items_total2['total'] ='0';
         $total =0;
         foreach($query_items12 as $arr){
            $query_items2[$i]['item'] = $arr['item'];
            $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items2[$i]['price'] = strval(number_format($arr['price'],2));
            $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
            $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
            $i++;
         }
         $items2[]=array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);
         $i=$i+1;
    }

    $data_payment_total2 = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('SalesStatus','LIKE','%S%')
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();

    $data_payment_total12 = json_decode(json_encode($data_payment_total2), true);
    $data_payment_total1  = array();
    $i=0;
    foreach($data_payment_total12 as $arr){
      $data_payment_total1['Quantity'] = strval(round($arr['Quantity'],2));
      $data_payment_total1['Price'] = strval(round($arr['Price'],2));
      $i++;
    }
 }
  //---------------------------------Hourly Sale---------------------------
  $datefrom = $datefrom." 00:00:00";
  $dateto   = $dateto." 23:59:59";
  $query_items = DB::connection('mysql_source')
                    // ->table('sales_header')
                    // ->select(DB::raw('hour( StartTime ) as Hour'),DB::raw('SUM(GuestCount) as Guest'),DB::raw('COUNT(DISTINCT VoucherNo) as Transaction'),DB::raw('SUM(SalesAmount) as Sales'))
                    // ->where('CompanyID','=',$company)
                    // ->where('ChainID','=',$chin_id)
                    // ->whereIn('LocationID',$locationlist)
                    // ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                    // ->where('VoucherStatus','LIKE','%S%')
                    // //->groupBy(DB::raw('hour(StartTime)'),DB::raw('day(StartTime)'))
                    // ->groupBy(DB::raw('hour(StartTime)'))
                    // ->get();
                    ->table('sales_line')
                    ->select(DB::raw('hour( sales_header.StartTime ) as Hour'),
                      DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` BETWEEN "'.$datefrom.'" AND "'.$dateto.'" AND `CompanyID`= "'.$company.'" AND `LocationID` IN ("' . implode('","', $locationlist) . '")  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND hour(`StartTime`) =  Hour) as Guest'),
                      DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'),
                      DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'))
                    ->leftJoin("sales_header",function($join){
                    $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                    })
                    ->where('sales_line.CompanyID','=',$company)
                    ->where('sales_line.ChainID','=',$chin_id)
                    ->whereIn('sales_line.LocationID',$locationlist)
                    ->whereBetween('sales_line.VoucherDate', [$datefrom, $dateto])
                    ->where('sales_line.SalesStatus','S')
                    ->groupBy(DB::raw('hour(sales_header.StartTime)'))
                    ->get();
  $i=0;
  $data_hourly_sale=array();
  $hourly_sale ='0';
  $guesttotal  = '0';
  $trxtotal    = '0';
  $data_hourly_sale_total =array();
  foreach($query_items as $data){
      if($data->Hour  < 10){
         $data->Hour = '0'.$data->Hour;
      }
      $to =$data->Hour+1;
      if($to <10 ){
         $to ='0'.$to;
      }
      $data_hourly_sale[$i]['Hours']       = strval($data->Hour.':00 to '.$to.':00');
      $data_hourly_sale[$i]['Guest']       = strval(round($data->Guest,2));
      $data_hourly_sale[$i]['Transcation'] = strval(round($data->Transaction,2));
      $data_hourly_sale[$i]['Sales'] = strval(number_format($data->Sales,2));
      $hourly_sale = $hourly_sale + round($data->Sales,2);
      $guesttotal  = $guesttotal + round($data->Guest,2);
      $trxtotal    = $trxtotal + round($data->Transaction,2);
      $i++;
   }
   $data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
   $data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
   $data_hourly_sale_total['trx']   = strval(round($trxtotal,2));

  //------------------------------------Credit Customer------------------------------------------
   $data_credit_customer = DB::connection('mysql_source')
                                 ->table('sales_payment')
                                 ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
                                 ->where('sales_payment.CompanyID','=',$company)
                                 ->whereIn('sales_payment.LocationID',$locationlist)
                                 ->where('sales_payment.ChainID','=',$chin_id)
                                 ->whereBetween('sales_payment.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                                 ->where('sales_payment.CustomerName','!=','')
                                 ->groupBy('CustomerName','CardNo')
                                 ->get();
  //------------------------Sales Summary----------------------------------------------------------

  //------------------------Total Sales----------------------------------------------------------
   $data_total_sales= DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('SUM(Qty*SellingPrice) as TotalSales'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('SalesStatus','LIKE','%S%')
                        ->get();
  $data_total_sales1 = json_decode(json_encode($data_total_sales), true);
  $data_total_sales = array();
  $i=0;
  foreach($data_total_sales1 as $arr){
      $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
      $i++;
  }
  //------------------------Delivery Charges----------------------------------------------------------

  $data_delivery_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as DeliveryCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','=','DL')
                              ->get();

  $data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
  $data_delivery_charge  = array();
  $i=0;
  foreach($data_delivery_charge1 as $arr){
      $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
      $i++;
  }



  //------------------------Service Charges----------------------------------------------------------

  $data_service_charge  = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as ServiceCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('VoucherStatus','LIKE','%S%')
                              ->where('OrderType','!=','DL')
                              ->get();
  $data_service_charge1 = json_decode(json_encode($data_service_charge), true);
  $data_service_charge  = array();
  $i=0;
  foreach($data_service_charge1 as $arr){
      $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
      $i++;
  }


  //------------------------Gross Sales----------------------------------------------------------

  $data_gross_sales['Gross Sale'] = strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));
  //------------------------Staff Meal----------------------------------------------------------
  $data_staff_meal= DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('round(SUM(Qty*SellingPrice),3) as Amount'))
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->whereIn('sales_line.LocationID',$locationlist)
                        ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('sales_header.OrderType','=','SM')
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->get();

  $data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);
  $data_staff_meal  = array();
  $i=0;
  foreach($data_staff_meal1 as $arr){
      $data_staff_meal['Count'] = strval(round($arr['Count'],2));
      if($data_staff_meal['Count']>1){

         $data_staff_meal['Count']='1';
      }
      $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
      $i++;
  }

  //------------------------Discount----------------------------------------------------------
   // $data_discount= DB::connection('mysql_source')
   // ->table('sales_payment')
   // ->select(DB::raw('SUM(Amount) as Discount'))
   // ->leftJoin("sales_header",function($join){
   // $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
   //     ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
   //     ->on('sales_header.CompanyID','=','sales_payment.CompanyID')
   //     ->on('sales_header.ChainID','=','sales_payment.ChainID')
   //     ->on('sales_header.LocationID','=','sales_payment.LocationID');
   // })
   // ->leftJoin("payment_types",function($join1){
   // $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
   //     ->on('sales_payment.ChainID','=','payment_types.ChainID')
   //     ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
   // })
   // ->where('sales_header.CompanyID','=',$company)
   // ->where('sales_header.ChainID','=',$chin_id)
   // ->whereBetween('sales_header.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
   // ->whereIn('sales_payment.LocationID',$locationlist)
   // ->where('sales_header.VoucherStatus','LIKE','%S%')
   // ->where('sales_header.OrderType','!=','SM')
   // ->where('payment_types.IsDiscountCard','=','1')
   // ->get();
   // $query = \DB::getQueryLog();
   // //print_r($query); exit();
   // $data_discount1 = json_decode(json_encode($data_discount), true);
   // $data_discount = array();
   // $i=0;
   // foreach($data_discount1 as $arr){
   //    // $data_discount['Discount'] = strval(round($arr['Discount'],2));
   //    $i++;
   // }
   $data_discount['Discount'] = strval(round($totalDiscount, 2));

  //------------------------VAT----------------------------------------------------------
   $data_vat   = DB::connection('mysql_source')
                     ->table('sales_header')
                     ->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
                     ->where('CompanyID','=',$company)
                     ->where('ChainID','=',$chin_id)
                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                     ->whereIn('LocationID',$locationlist)
                     ->where('VoucherStatus','S')
                     ->get();
   $data_vat1 = json_decode(json_encode($data_vat), true);
   $data_vat = array();
   $i=0;
   foreach($data_vat1 as $arr){
      $data_vat['VAT'] = strval(round($arr['VAT'],2));
      $i++;
   }

  //------------------------Net Sales----------------------------------------------------------
  $data_net_sales['Net Sales'] = strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));

  //------------------------Taxable Value----------------------------------------------------------
  $data_tax['TaxableValue'] = strval(round($data_net_sales['Net Sales']-$data_vat['VAT'],2));


  //------------------------PayOut----------------------------------------------------------
  $data_payout['Payout']='0';
  //------------------------Credit Sales----------------------------------------------------------
//   $data_creditsales = DB::connection('mysql_source')
//   ->table('sales_payment')
//    ->select(DB::raw('SUM(Amount) as CreditSales'))
//    ->where('sales_header.CompanyID','=',$company)
//    ->where('sales_header.ChainID','=',$chin_id)
//    ->whereBetween('sales_header.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
//     ->whereIn('sales_header.LocationID',$locationlist)
//    ->where('sales_header.VoucherStatus','LIKE','%S%')
//    ->leftJoin("sales_header",function($join){
//    $join->on('sales_header.VoucherNo','=','sales_payment.VoucherNo')
//        ->on('sales_header.VoucherDate','=','sales_payment.VoucherDate')
//        ->on('sales_payment.CompanyID','=','sales_header.CompanyID')
//        ->on('sales_payment.ChainID','=','sales_header.ChainID')
//        ->on('sales_payment.LocationID','=','sales_header.LocationID');
//    })
//    ->leftJoin("payment_types",function($join1){
//    $join1->on('sales_payment.CompanyID','=','payment_types.CompanyID')
//        ->on('sales_payment.ChainID','=','payment_types.ChainID')
//        ->on('sales_payment.PaymentType','=','payment_types.PaymentType');
//    })
//    ->where('payment_types.IsCreditCard','=','1')
//    ->where('payment_types.IsDiscountCard','!=','1')
//    ->where('sales_header.OrderType','!=','SM')
//    ->get();
//    $data_creditsales1 = json_decode(json_encode($data_creditsales), true);
//    $data_creditsales = array();
//    $i=0;
//    foreach($data_creditsales1 as $arr){
//     //   $data_creditsales['CreditSales'] = strval(round($arr['CreditSales'],2));
//       $i++;
//    }
   $data_creditsales['CreditSales'] = strval(round($creditSaleTotal, 2));

   $data_locationlist= DB::connection('mysql_source')
                           ->table('sales_header')
                           ->select('LocationID')
                           ->groupBy('LocationID')
                           ->get();
  //------------------------Actual Banking----------------------------------------------------------
  $data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

  //------------------------Voids----------------------------------------------------------

  //item order item
  $data_items_order = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
                           ->leftJoin("sales_header",function($join){
                              $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                 ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                 ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                 ->on('sales_header.ChainID','=','sales_line.ChainID')
                                 ->on('sales_header.LocationID','=','sales_line.LocationID');
                              })
                           ->where('sales_line.CompanyID','=',$company)
                           ->where('sales_line.ChainID','=',$chin_id)
                           ->whereIn('sales_line.LocationID',$locationlist)
                           ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                           ->where('sales_header.VoucherStatus', 'V')
                           ->get();

  $data_items_order1 = json_decode(json_encode($data_items_order), true);
  $data_items_order  = array();
  $i=0;
  foreach($data_items_order1 as $arr){
      $data_items_order['Count'] = strval(round($arr['Count'],2));
      $data_items_order['Amount'] = strval(round($arr['Amount'],2));
      $i++;
  }
  //item  total
  $data_void_items_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
                                 ->leftJoin("sales_header",function($join){
                                 $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                    ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                    ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                    ->on('sales_header.ChainID','=','sales_line.ChainID')
                                    ->on('sales_header.LocationID','=','sales_line.LocationID');
                                 })
                              ->where('sales_line.CompanyID','=',$company)
                              ->where('sales_line.ChainID','=',$chin_id)
                              ->whereIn('sales_line.LocationID',$locationlist)
                              ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('sales_line.SalesStatus', 'V')
                              ->first();
//   $data_void_items_total1 = json_decode(json_encode($data_void_items_total), true);
//   $data_void_items_total = array();
//   $i=0;
//   foreach($data_void_items_total1 as $arr){
//   $data_void_items_total['Count'] = strval(round($arr['Count'],2));
//   $data_void_items_total['Amount'] = strval(round($arr['Amount'],2));
//   $i++;
//   }
  //item void
  $data_items_void           = array();
  $data_items_void['Amount'] = strval(round($data_void_items_total->Amount,3)) ?? '0';
  $data_items_void['Count']  = strval(($data_void_items_total->Count)) ?? '0';
  //------------------------Total of Voids And Staff Meal----------------------------------------------------------
  $data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount']+@$data_items_void['Amount'] + $data_items_order['Amount'],3));


   //----------------------------------------------Chain---------------------------------------------------------------
   $chain   =DB::connection('mysql_source')
               ->table('chainaccess')
               ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
               ->select('chainaccess.ChainId','chain.ChainName')
               ->where('chainaccess.CompanyId','=',$company)
               ->groupBy('chainaccess.ChainId')
               ->groupBy('chain.ChainName')
               ->get();
               //----------------------------------------------Location---------------------------------------------------------------
               $location=DB::connection('mysql_source')
               ->table('locationaccess')
               ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
               ->select('locationaccess.LocationId','LocationName')
               ->where('locationaccess.ChainId','=',$chin_id)
               ->groupBy('locationaccess.LocationId')
               ->groupBy('location.LocationName')
               ->get();

   DB::commit();
   DB::disconnect('mysql_source');
   if($selecctedcategory=='Category1'){
       echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
   }else if($selecctedcategory=='Category2'){
       echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items2,"Items_total"=>$data_payment_total1,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
   }
 }

 public function all_dataonchange_testing1(Request $request){

    $datefrom=$request->datefrom;
    $dateto=$request->dateto;
    $chin_id = $request->chain;
    $location_id =$request->location;
    $pass   = $request->pass;
    $selecctedcategory =$request->category;
    if($selecctedcategory=='') {
         $selecctedcategory ='Category1';
    }

   if($datefrom > $dateto){
      $datef=$datefrom;
      $datefrom = $dateto;
      $dateto   = $datef;
    }


    @$location =explode(',',$location_id);
    if(!empty($location)){
      foreach($location as $loc){
        if($loc!='') $locationlist[]=$loc;
      }
    }
   //print_r($locationlist); exit();
  $userid =$this->checkAuth($request);
  if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
  $user = UserDetails::where('id','=',$userid->user_id)->first();
  $company_id = $user->CompanyID;
  $company    = @$user->CompanyID;
  $username   = @$user->username;
  $userdata   = array('user_name'=>$username,'CompanyID'=>$company,'ChainID'=>$chin_id,'LocationID'=>$location_id);
  // $company=$company_id[0]['CompanyID'];

  $this->getConnectionDetails($user->CompanyID);
  \DB::enableQueryLog();
  // -----------------order type----------------------
  $datefrom = $datefrom." 00:00:00";
  $dateto   = $dateto." 23:59:59";
  $data_ordertype = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('OrderType as Type', DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'),
                        // DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` BETWEEN "'.$datefrom.'" AND "'.$dateto.'" AND `CompanyID`= "'.$company.'" AND `LocationID` IN ("' . implode('","', $locationlist) . '")  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND sales_header.OrderType =  Type) as Guest'),
                        DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'))
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                            ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                            ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                            ->on('sales_line.ChainID','=','sales_header.ChainID')
                            ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->where('sales_line.CompanyID','=', $company)
                        ->where('sales_line.ChainID','=', $chin_id)
                        ->whereIn('sales_line.LocationID', $locationlist)
                        ->whereBetween('sales_line.VoucherDate', [$datefrom, $dateto])
                        ->where('sales_line.SalesStatus','S')
                        ->groupBy('sales_header.OrderType')
                        ->get();
    foreach($data_ordertype as $row){
        $guest  = DB::connection('mysql_source')
                    ->table('sales_header')
                    ->select(DB::raw('SUM(GuestCount) as Guest'))
                    ->where('CompanyID','=',$company)
                    ->where('ChainID','=',$chin_id)
                    ->whereIn('LocationID',$locationlist)
                    ->where('VoucherStatus', 'S')
                    ->whereBetween('VoucherDate', [$datefrom, $dateto])
                    ->where('OrderType', $row->Type)
                    ->first();
        $row->Guest = $guest->Guest;
    }
  $data_ordertype1      = json_decode(json_encode($data_ordertype), true);
  $data_ordertype_total = array();
  $i=0;
  $data_ordertype=array();
  $ototal ='0';
  $trxtotal ='0';
  $guesttotal = '0';
  foreach($data_ordertype1 as $arr){
      $data_ordertype[$i]['OrderType']   = $arr['Type'];
      $data_ordertype[$i]['Transaction'] = strval(round($arr['Transaction'],2));
      $data_ordertype[$i]['Guest']       = strval(round($arr['Guest'],2));
      $data_ordertype[$i]['Sales']       = strval(number_format($arr['Sales'],2));
      $ototal      = $ototal +$arr['Sales'];
      $trxtotal    = $trxtotal +$data_ordertype[$i]['Transaction'];
      $guesttotal  = $guesttotal +$data_ordertype[$i]['Guest'];
      $i++;
  }
  $data_ordertype_total['Sales'] = strval(number_format($ototal,2));
  $data_ordertype_total['Trx']   = strval(round($trxtotal,2));
  $data_ordertype_total['Guest'] = strval(round($guesttotal,2));

  // ---------------Category 1-----------------------

  $data_category1 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes1',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(sum(Qty),3) as Guest'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->where('SalesStatus','S')
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->groupBy('CategoryDes1')
                        ->get();
  $data_category11 = json_decode(json_encode($data_category1), true);
  $i=0;
  $data_category1 = array();
  $data_category1_total = array();
  $guest                = 0;
  $sales                = 0;
  foreach($data_category11 as $arr){
      $data_category1[$i]['CategoryDes1'] = $arr['CategoryDes1'];
      $data_category1[$i]['Guests'] = strval(round($arr['Guest'],2));
      $data_category1[$i]['sales'] = strval(number_format($arr['sales'],2));
      $guest                       = $guest + $data_category1[$i]['Guests'];
      $sales                       = $sales + $arr['sales'];
      $i++;
  }
  $data_category1_total['Guests'] = strval(round($guest,2));
  $data_category1_total['sales']  = strval(number_format($sales,2));

  // --------------Payment---------------------------
     $payment_type = DB::connection('mysql_source')
                        ->table('sales_payment')
                           ->select('PaymentType')
                           ->where('CompanyID','=',$company)
                           ->whereIn('LocationID',$locationlist)
                           ->where('ChainID','=',$chin_id)
                           ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                           ->groupBy('PaymentType')
                           ->get();
       $i=0;
       $totalDiscount = 0;
       $creditSaleTotal = 0;
     foreach ($payment_type as $value) {

         @$query_paymenttype = DB::connection('mysql_source')
                                 ->table('sales_payment')
                                 ->select('PaymentType as Payment Type',DB::raw('round(SUM(Amount),3) as Amount'),DB::raw('round(COUNT(VoucherNo),3) as Count'))
                                 ->where('CompanyID','=',$company)
                                 ->whereIn('LocationID',$locationlist)
                                 ->where('ChainID','=',$chin_id)
                                 ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                                 ->where('PaymentType','=',$value->PaymentType)
                                 ->groupBy('PaymentType')
                                 ->get();

         $quer = @$query_paymenttype[0]->Amount;
         $tcount = @$query_paymenttype[0]->Count;
         $id=2;

         @$typevalue = DB::connection('mysql_source')
                           ->table('payment_types')
                           ->select('*')
                           ->where('CompanyID','=',$company)
                           ->where('ChainID','=',$chin_id)
                           ->where('PaymentType','=',$value->PaymentType)
                           ->get();
         $quer   = @$query_paymenttype[0]->Amount;
         $tcount = @$query_paymenttype[0]->Count;

         /***************************** Payment Types *************************/
         if((@$typevalue[0]->IsCreditCard=='1') && (@$typevalue[0]->IsDiscountCard=='1')){
            $id = 1;
            $totalDiscount += $quer;
         }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCCard == '1')){
            $id = 2;
            $creditSaleTotal += $quer;
         }else if((@$typevalue[0]->IsCreditCard== '1') && (@$typevalue[0]->IsCustCredit == '1')){
            $id = 3;
            $creditSaleTotal += $quer;
         }else if((@$typevalue[0]->IsStaffMeal== '1')){
            $id = 4;
         }else{
            $id = 5;
         }
         if($value->PaymentType!=''){
           $data_paymenttype[]=array("Group Id"=>$id,"Payment Type"=>$value->PaymentType,"Amount"=>strval(round($quer,2)),"Count"=>strval(round($tcount,2)));
         }
         $i++;
       }
       $visa_amount['Visa Amount']     = strval(round(@$data_paymenttype[1]['Amount'],2));
       $master_amount['Master Amount'] = strval(round(@$data_paymenttype[2]['Amount'],2));
       $data_visamastertotal[]         = array("visa_master_total"=>strval(round($visa_amount+$master_amount,2)));

  // ---------------Category 2-----------------------
  $data_category2 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('CategoryDes2',DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('SalesStatus','S')
                        ->groupBy('CategoryDes2')
                        ->get();
   $data_category12 = json_decode(json_encode($data_category2), true);
   $i =  0;
   $data_category2 = array();
  foreach($data_category12 as $arr){
      $data_category2[$i]['CategoryDes2'] = $arr['CategoryDes2'];
      $data_category2[$i]['Qty'] = strval(round($arr['Quantity'],2));
      $data_category2[$i]['SellingPrice'] = strval(number_format($arr['sales'],2));
      $i++;
  }

  $data_category2_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(SellingPrice*Qty),3) as sales'),DB::raw('round(SUM(Qty),3) as Quantity'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->where('SalesStatus','S')
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();
  $data_category2_total2 = json_decode(json_encode($data_category2_total), true);
  $data_category2_total  = array();
  $i = 0;
  foreach($data_category2_total2 as $arr){
      $data_category2_total['Quantity'] = strval(round($arr['Quantity'],2));
      $data_category2_total['price'] = strval(number_format($arr['sales'],2));
      $i++;
  }


  // -----------------Sales Items by Category 1--------------------------
  if($selecctedcategory=='Category1') {

    $categorydes1_array = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select('CategoryDes1')
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('SalesStatus','S')
                              ->distinct('CategoryDesc1')
                              ->get()->toArray();
    $i = 0;
    $items = [];
    foreach($categorydes1_array as $key => $value) {
    $query_items = DB::connection('mysql_source')
                     ->table('sales_line')
                     ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
                     ->where('CompanyID','=',$company)
                     ->where('ChainID','=',$chin_id)
                     ->whereIn('LocationID',$locationlist)
                     ->where('SalesStatus','S')
                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                     ->where('CategoryDes1','=',$value->CategoryDes1)
                     ->groupBy('ItemDesc')
                     ->get();
       $query_items1 = json_decode(json_encode($query_items), true);
       $i=0;
       $query_items = array();
       $query_items_total['qty'] ='0';
       $query_items_total['total'] ='0';
       $total =0;
       foreach($query_items1 as $arr){
          $query_items[$i]['item'] = $arr['item'];
          $query_items[$i]['quantity'] = strval(round($arr['quantity'],2));
          $query_items[$i]['price'] = strval(number_format($arr['price'],2));
          $query_items_total['qty'] =$query_items_total['qty']+$arr['quantity'];
          $query_items_total['total'] =$query_items_total['total']+$arr['price'];
          $i++;
       }
       $items[]=array("Category"=>$value->CategoryDes1,"Qty"=>strval(round($query_items_total['qty'],2)),"Total"=>strval(number_format($query_items_total['total'],2)),"items"=>$query_items);
       $i=$i+1;
    }

    $data_payment_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('SalesStatus','S')
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();

    $data_payment_total1 = json_decode(json_encode($data_payment_total), true);
    $data_payment_total  = array();
    $i=0;
    foreach($data_payment_total1 as $arr){
      $data_payment_total['Quantity'] = strval(round($arr['Quantity'],2));
      $data_payment_total['Price'] = strval(number_format($arr['Price'],2));
      $i++;
    }
  }
    // -----------------Sales Items by Category 2--------------------------
 if($selecctedcategory=='Category2') {
    $categorydesc2_array = DB::connection('mysql_source')
    ->table('sales_line')
    ->select('CategoryDes2')
    ->where('CompanyID','=',$company)
    ->where('ChainID','=',$chin_id)
    ->whereIn('LocationID',$locationlist)
    ->where('SalesStatus','S')
    ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
    ->distinct('CategoryDes2')
    ->get()->toArray();

    $i=0;
    $items2=[];
    foreach($categorydesc2_array as $key => $value) {
    $query_items23 = DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select('ItemDesc as item',DB::raw('round(SUM(Qty),3) as quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as price'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->where('SalesStatus','S')
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('CategoryDes2','=',$value->CategoryDes2)
                        ->groupBy('ItemDesc')
                        ->get();
         $query_items12  = json_decode(json_encode($query_items23), true);
         $i = 0;
         $query_items2 = array();
         $query_items_total2['qty'] ='0';
         $query_items_total2['total'] ='0';
         $total =0;
         foreach($query_items12 as $arr){
            $query_items2[$i]['item'] = $arr['item'];
            $query_items2[$i]['quantity'] = strval(round($arr['quantity'],2));
            $query_items2[$i]['price'] = strval(number_format($arr['price'],2));
            $query_items_total2['qty'] =$query_items_total2['qty']+$arr['quantity'];
            $query_items_total2['total'] =$query_items_total2['total']+$arr['price'];
            $i++;
         }
         $items2[]=array("Category"=>$value->CategoryDes2,"Qty"=>strval(round($query_items_total2['qty'],2)),"Total"=>strval(number_format($query_items_total2['total'],2)),"items"=>$query_items2);
         $i=$i+1;
    }

    $data_payment_total2 = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('round(SUM(Qty),3) as Quantity'),DB::raw('round(SUM(SellingPrice*Qty),3) as Price'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->where('SalesStatus','S')
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->get();

    $data_payment_total12 = json_decode(json_encode($data_payment_total2), true);
    $data_payment_total1  = array();
    $i=0;
    foreach($data_payment_total12 as $arr){
      $data_payment_total1['Quantity'] = strval(round($arr['Quantity'],2));
      $data_payment_total1['Price'] = strval(round($arr['Price'],2));
      $i++;
    }
 }
  //---------------------------------Hourly Sale---------------------------
  $datefrom = $datefrom." 00:00:00";
  $dateto   = $dateto." 23:59:59";
  $query_items = DB::connection('mysql_source')
                    ->table('sales_line')
                    ->select(DB::raw('hour( sales_header.StartTime ) as Hour'),
                    //   DB::raw('(SELECT SUM(GuestCount) as Guest FROM sales_header WHERE  `VoucherDate` BETWEEN "'.$datefrom.'" AND "'.$dateto.'" AND `CompanyID`= "'.$company.'" AND `LocationID` IN ("' . implode('","', $locationlist) . '")  AND `ChainID` = "'.$chin_id.'" AND `VoucherStatus` = "S" AND hour(`StartTime`) =  Hour) as Guest'),
                      DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Transaction'),
                      DB::raw('SUM(sales_line.SellingPrice * sales_line.Qty) as Sales'))
                    ->leftJoin("sales_header",function($join){
                    $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                    })
                    ->where('sales_line.CompanyID','=',$company)
                    ->where('sales_line.ChainID','=',$chin_id)
                    ->whereIn('sales_line.LocationID',$locationlist)
                    ->whereBetween('sales_line.VoucherDate', [$datefrom, $dateto])
                    ->where('sales_line.SalesStatus','S')
                    ->groupBy(DB::raw('hour(sales_header.StartTime)'))
                    ->get();
                    foreach($query_items as $row){
                        $guest = DB::connection('mysql_source')
                                    ->table('sales_header')
                                    ->select(DB::raw('SUM(GuestCount) as Guest'))
                                    ->where('CompanyID','=',$company)
                                    ->where('ChainID','=',$chin_id)
                                    ->whereIn('LocationID',$locationlist)
                                    ->whereBetween('VoucherDate', [ $datefrom, $dateto ])
                                    ->where('VoucherStatus','S')
                                    ->where(DB::raw('HOUR(StartTime)'), $row->Hour)
                                    ->first();
                        $row->Guest = $guest->Guest;
                    }
  $i=0;
  $data_hourly_sale=array();
  $hourly_sale ='0';
  $guesttotal  = '0';
  $trxtotal    = '0';
  $data_hourly_sale_total =array();
  foreach($query_items as $data){
      if($data->Hour  < 10){
         $data->Hour = '0'.$data->Hour;
      }
      $to =$data->Hour+1;
      if($to <10 ){
         $to ='0'.$to;
      }
      $data_hourly_sale[$i]['Hours']       = strval($data->Hour.':00 to '.$to.':00');
      $data_hourly_sale[$i]['Guest']       = strval(round($data->Guest,2));
      $data_hourly_sale[$i]['Transcation'] = strval(round($data->Transaction,2));
      $data_hourly_sale[$i]['Sales'] = strval(number_format($data->Sales,2));
      $hourly_sale = $hourly_sale + round($data->Sales,2);
      $guesttotal  = $guesttotal + round($data->Guest,2);
      $trxtotal    = $trxtotal + round($data->Transaction,2);
      $i++;
   }
   $data_hourly_sale_total['total'] = strval(number_format($hourly_sale,2));
   $data_hourly_sale_total['guest'] = strval(round($guesttotal,2));
   $data_hourly_sale_total['trx']   = strval(round($trxtotal,2));

  //------------------------------------Credit Customer------------------------------------------
   $data_credit_customer = DB::connection('mysql_source')
                                 ->table('sales_payment')
                                 ->select('CustomerName','CardNo',DB::raw('SUM(Amount) as Amount'))
                                 ->where('sales_payment.CompanyID','=',$company)
                                 ->whereIn('sales_payment.LocationID',$locationlist)
                                 ->where('sales_payment.ChainID','=',$chin_id)
                                 ->whereBetween('sales_payment.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                                 ->where('sales_payment.CustomerName','!=','')
                                 ->groupBy('CustomerName','CardNo')
                                 ->get();
  //------------------------Sales Summary----------------------------------------------------------

  //------------------------Total Sales----------------------------------------------------------
   $data_total_sales= DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('SUM(Qty*SellingPrice) as TotalSales'))
                        ->where('CompanyID','=',$company)
                        ->where('ChainID','=',$chin_id)
                        ->whereIn('LocationID',$locationlist)
                        ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('SalesStatus','LIKE','%S%')
                        ->get();
  $data_total_sales1 = json_decode(json_encode($data_total_sales), true);
  $data_total_sales = array();
  $i=0;
  foreach($data_total_sales1 as $arr){
      $data_total_sales['TotalSales'] = strval(round($arr['TotalSales'],2));
      $i++;
  }
  //------------------------Delivery Charges----------------------------------------------------------

  $data_delivery_charge = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as DeliveryCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('VoucherStatus','S')
                              ->where('OrderType','=','DL')
                              ->get();

  $data_delivery_charge1 = json_decode(json_encode($data_delivery_charge), true);
  $data_delivery_charge  = array();
  $i=0;
  foreach($data_delivery_charge1 as $arr){
      $data_delivery_charge['DeliveryCharges'] = strval(round($arr['DeliveryCharges'],2));
      $i++;
  }



  //------------------------Service Charges----------------------------------------------------------

  $data_service_charge  = DB::connection('mysql_source')
                              ->table('sales_header')
                              ->select(DB::raw('round(SUM(ServiceTaxAmount+ServiceTaxBaseAmount),3) as ServiceCharges'))
                              ->where('CompanyID','=',$company)
                              ->where('ChainID','=',$chin_id)
                              ->whereIn('LocationID',$locationlist)
                              ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('VoucherStatus','S')
                              ->where('OrderType','!=','DL')
                              ->get();
  $data_service_charge1 = json_decode(json_encode($data_service_charge), true);
  $data_service_charge  = array();
  $i=0;
  foreach($data_service_charge1 as $arr){
      $data_service_charge['ServiceCharges'] = strval(round($arr['ServiceCharges'],2));
      $i++;
  }


  //------------------------Gross Sales----------------------------------------------------------

  $data_gross_sales['Gross Sale'] = strval(round($data_total_sales['TotalSales']+$data_delivery_charge['DeliveryCharges']+$data_service_charge['ServiceCharges'],2));
  //------------------------Staff Meal----------------------------------------------------------
  $data_staff_meal= DB::connection('mysql_source')
                        ->table('sales_line')
                        ->select(DB::raw('COUNT(sales_header.OrderType) as Count'),DB::raw('round(SUM(Qty*SellingPrice),3) as Amount'))
                        ->where('sales_line.CompanyID','=',$company)
                        ->where('sales_line.ChainID','=',$chin_id)
                        ->whereIn('sales_line.LocationID',$locationlist)
                        ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                        ->where('sales_header.OrderType','=','SM')
                        ->leftJoin("sales_header",function($join){
                        $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                        ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                        ->on('sales_line.CompanyID','=','sales_header.CompanyID')
                        ->on('sales_line.ChainID','=','sales_header.ChainID')
                        ->on('sales_line.LocationID','=','sales_header.LocationID');
                        })
                        ->get();

  $data_staff_meal1 = json_decode(json_encode($data_staff_meal), true);
  $data_staff_meal  = array();
  $i=0;
  foreach($data_staff_meal1 as $arr){
      $data_staff_meal['Count'] = strval(round($arr['Count'],2));
      if($data_staff_meal['Count']>1){

         $data_staff_meal['Count']='1';
      }
      $data_staff_meal['Amount'] = strval(round($arr['Amount'],2));
      $i++;
  }

  //------------------------Discount----------------------------------------------------------
   $data_discount['Discount'] = strval(round($totalDiscount, 2));

  //------------------------VAT----------------------------------------------------------
   $data_vat   = DB::connection('mysql_source')
                     ->table('sales_header')
                     ->select(DB::raw('SUM(ServiceTaxAmount+SalesTaxAmount) as VAT'))
                     ->where('CompanyID','=',$company)
                     ->where('ChainID','=',$chin_id)
                     ->whereBetween('VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                     ->whereIn('LocationID',$locationlist)
                     ->where('VoucherStatus','S')
                     ->get();
   $data_vat1 = json_decode(json_encode($data_vat), true);
   $data_vat = array();
   $i=0;
   foreach($data_vat1 as $arr){
      $data_vat['VAT'] = strval(round($arr['VAT'],2));
      $i++;
   }

  //------------------------Net Sales----------------------------------------------------------
  $data_net_sales['Net Sales'] = strval(round($data_gross_sales['Gross Sale']-$data_staff_meal['Amount']-$data_discount['Discount'],3));

  //------------------------Taxable Value----------------------------------------------------------
  $data_tax['TaxableValue'] = strval(round($data_net_sales['Net Sales']-$data_vat['VAT'],2));


  //------------------------PayOut----------------------------------------------------------
  $data_payout['Payout']='0';
  //------------------------Credit Sales----------------------------------------------------------
   $data_creditsales['CreditSales'] = strval(round($creditSaleTotal, 2));

   $data_locationlist= DB::connection('mysql_source')
                           ->table('sales_header')
                           ->select('LocationID')
                           ->groupBy('LocationID')
                           ->get();
  //------------------------Actual Banking----------------------------------------------------------
  $data_actualbanking['Actual Banking'] =strval(round($data_net_sales['Net Sales']-$data_creditsales['CreditSales']-$data_payout['Payout'],3));

  //------------------------Voids----------------------------------------------------------

  //item order item
  $data_items_order = DB::connection('mysql_source')
                           ->table('sales_line')
                           ->select(DB::raw('COUNT(DISTINCT sales_line.VoucherNo) as Count'),DB::raw('round(SUM(sales_line.Qty*sales_line.SellingPrice),3) as Amount'))
                           ->leftJoin("sales_header",function($join){
                              $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                 ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                 ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                 ->on('sales_header.ChainID','=','sales_line.ChainID')
                                 ->on('sales_header.LocationID','=','sales_line.LocationID');
                              })
                           ->where('sales_line.CompanyID','=',$company)
                           ->where('sales_line.ChainID','=',$chin_id)
                           ->whereIn('sales_line.LocationID',$locationlist)
                           ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                           ->where('sales_header.VoucherStatus', 'V')
                           ->get();

  $data_items_order1 = json_decode(json_encode($data_items_order), true);
  $data_items_order  = array();
  $i=0;
  foreach($data_items_order1 as $arr){
      $data_items_order['Count'] = strval(round($arr['Count'],2));
      $data_items_order['Amount'] = strval(round($arr['Amount'],2));
      $i++;
  }
  //item  total
  $data_void_items_total = DB::connection('mysql_source')
                              ->table('sales_line')
                              ->select(DB::raw('COUNT(sales_line.VoucherNo) as Count'),DB::raw('round(SUM(SellingPrice*Qty),3) as Amount'))
                                 ->leftJoin("sales_header",function($join){
                                 $join->on('sales_header.VoucherNo','=','sales_line.VoucherNo')
                                    ->on('sales_header.VoucherDate','=','sales_line.VoucherDate')
                                    ->on('sales_header.CompanyID','=','sales_line.CompanyID')
                                    ->on('sales_header.ChainID','=','sales_line.ChainID')
                                    ->on('sales_header.LocationID','=','sales_line.LocationID');
                                 })
                              ->where('sales_line.CompanyID','=',$company)
                              ->where('sales_line.ChainID','=',$chin_id)
                              ->whereIn('sales_line.LocationID',$locationlist)
                              ->whereBetween('sales_line.VoucherDate', [$datefrom." 00:00:00",$dateto." 23:59:59"])
                              ->where('sales_line.SalesStatus', 'V')
                              ->where('sales_header.VoucherStatus', '!=', 'V')
                              ->first();
  //item void
  $data_items_void           = array();
  $data_items_void['Amount'] = strval(round($data_void_items_total->Amount,3)) ?? '0';
  $data_items_void['Count']  = strval(($data_void_items_total->Count)) ?? '0';
  //------------------------Total of Voids And Staff Meal----------------------------------------------------------
  $data_totalofvoidsandstaffmeal['TotalofVoidsandStaffMeals'] =strval(round(@$data_staff_meal['Amount'] + @$data_items_void['Amount'] + @$data_items_order['Amount'],3));


   //----------------------------------------------Chain---------------------------------------------------------------
   $chain   =DB::connection('mysql_source')
               ->table('chainaccess')
               ->join('chain', 'chain.ChainId', '=', 'chainaccess.ChainID')
               ->select('chainaccess.ChainId','chain.ChainName')
               ->where('chainaccess.CompanyId','=',$company)
               ->groupBy('chainaccess.ChainId')
               ->groupBy('chain.ChainName')
               ->get();
               //----------------------------------------------Location---------------------------------------------------------------
               $location=DB::connection('mysql_source')
               ->table('locationaccess')
               ->join('location', 'location.LocationId', '=', 'locationaccess.LocationId')
               ->select('locationaccess.LocationId','LocationName')
               ->where('locationaccess.ChainId','=',$chin_id)
               ->groupBy('locationaccess.LocationId')
               ->groupBy('location.LocationName')
               ->get();

   DB::commit();
   DB::disconnect('mysql_source');
   if($selecctedcategory=='Category1'){
       echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items,"Items_total"=>$data_payment_total,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
   }else if($selecctedcategory=='Category2'){
       echo json_encode(["data"=>$userdata,"Sales by Category 1"=>$data_category1,"Category1_total"=>$data_category1_total,"Sales by Category 2"=>$data_category2,"Category2_total"=>$data_category2_total,"Sales by Items"=>$items2,"Items_total"=>$data_payment_total1,"Sales by Order Type"=>$data_ordertype,"Order_type_total"=>$data_ordertype_total,"Hourly Sale"=>$data_hourly_sale,"Hourly Sale Total"=>$data_hourly_sale_total,"Credit Customer"=>$data_credit_customer,"Total Sales"=>$data_total_sales,"Delivery Charges"=>$data_delivery_charge,"Service Charges"=>$data_service_charge,"Gross Sales"=>$data_gross_sales,"Staff Meals"=>$data_staff_meal,"TotalofVoidsandStaffMeals"=>$data_totalofvoidsandstaffmeal,"Discount"=>$data_discount,"Taxable Values"=>$data_tax,"VAT"=>$data_vat,"Net Sales"=>$data_net_sales,"Credit Sales"=>$data_creditsales,"Actual Banking"=>$data_actualbanking,"Locations"=>$data_locationlist,"Item Void"=>$data_items_void,"Item Order"=>$data_items_order,"Visa Card"=>$visa_amount,"Master Amount"=>$master_amount,"Payment Card"=>@$data_paymenttype,"Location"=>$location,"Chain"=>$chain]);
   }
 }

   //----------------------------------------------------Get User Details ----------------------------------------------------
public function new_user(Request $request){

   $rules = [
      'email'     => "required|email|unique:local_users,user_email",
      'firstname' => "required",
      'lastname'  => "required",
      'dob'       => "required",
      'username'  => "required|unique:local_users,user_name",
      'password'  => "required",
      'gender'    => "required",
    ];
    $messages = [
        'email.required'     => "Email is required",
        'firstname.required' => "Firstname is required",
        'lastname.required'  => "Lastname is required",
        'dob.required'       => "Date of birth required",
        'username.required'  => "Username is required",
        'password.required'  => "Password is required",
        'gender.required'    => "Gender is required"
    ];
    $validator = Validator::make(request()->all(), $rules, $messages);
    if (!$validator->passes()) {
        $messages = $validator->messages();
        $errors = [];
        foreach ($rules as $key => $value) {
            $err = $messages->first($key);
            if ($err) {
                $errors[$key] = $err;
            }
        }
        return response()->json(['status' => 0, 'errors' => $errors]);
    }
    $email             = $request->email;
    $firstname         = $request->firstname;
    $lastname          = $request->lastname;
    $username          = $request->username;
    $password          = bcrypt($request->password);
    $gender            = $request->gender;
    $dob               = $request->dob;
    $google_token      = $request->google_token;
    $userid            = $this->generateUserId();
    $image             = '';
    if($request->image){
      define('UPLOAD_DIR', 'public/images/local_users/');
      $file = $request->file('image');
      //Display File Name
      $destinationPath = UPLOAD_DIR;
      $uniqueFileName = uniqid() . $file->getClientOriginalName();
      $file->move($destinationPath,$uniqueFileName);
      $image_name = $destinationPath.$uniqueFileName;
      $image = $image_name;
   }
    try {
        DB::beginTransaction();
        $attr_array[] = [
            'user_email' =>  $email,
            'firstname'  => $firstname,
            'lastname'   => $lastname,
            'username'   => $username,
            'password'   => $password,
            'dob'        => date('Y-m-d',strtotime($dob)),
            'gender'     => $gender,
            'user_id'    => $userid,
            'CompanyID'  => 'KCL',
            'google_token'=>$google_token,
            'image'      =>$image,
            'reset_otp'  =>'',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        if (!empty($attr_array)) {
            UserDetails::insert($attr_array);
            DB::commit();
            //Login
           $user=UserDetails::where('username',$request->username)->first();
           //add dummy chain access to new user
           $chain = DB::table('chain')->where('CompanyId',$user->CompanyID)->select('ChainId')->groupby('ChainId')->get();
           foreach($chain as $ch){
               try {
                  DB::beginTransaction();
                  $charray[] = [
                     'ChainID' =>  $ch->ChainId,
                     'CompanyId'   =>  $user['CompanyID'],
                     'UserID'   =>  $user['user_id'],
                     'FullAccess' => 1,
                  ];
                  if (!empty($charray)) {
                     DB::table('chainaccess')->insert($charray);
                     DB::commit();
                  }else{
                     //print_r($e->getMessage());exit;
                     $errors['couldnot_save'] = "We are unable to register now please try again later";
                     return response()->json(['status' => 0, 'errors' => $errors]);
                  }
               } catch (\Exception $e) {
                        DB::rollback();
                        //print_r($e->getMessage());exit;
                        $errors['couldnot_save'] = "We are unable to register now please try again later";
                        return response()->json(['status' => 0, 'errors' => $errors]);
               }
           }
           //add dummy location access to new user
           $location = DB::table('location')->where('CompanyId',$user->CompanyID)->select('ChainId','LocationId')->get();
           foreach($location as $lo){
               try {
                  DB::beginTransaction();
                  $loarray[] = [
                     'UserId'      =>  $user['user_id'],
                     'CompanyId'   =>  $user['CompanyID'],
                     'LocationId'  =>  $lo->LocationId,
                     'ChainId'     =>  $lo->ChainId,
                  ];
                  if (!empty($loarray)) {
                     DB::table('locationaccess')->insert($loarray);
                     DB::commit();
                  }else{
                     $errors['couldnot_save'] = "We are unable to register now please try again later";
                     return response()->json(['status' => 0, 'errors' => $errors]);
                  }
               } catch (\Exception $e) {
                        DB::rollback();
                        //print_r($e->getMessage());exit;
                        $errors['couldnot_save'] = "We are unable to register now please try again later";
                        return response()->json(['status' => 0, 'errors' => $errors]);
               }
           }

           if($user){
               Auth::login($user);
               $success['status']=1;
               $success['_token']=$user->createToken('MyApp')->accessToken;
               try {
                  DB::beginTransaction();
                  $autharray[] = [
                      'user_id' =>  $user['id'],
                      'token'   =>  $success['_token'],
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s')
                  ];
                  if (!empty($autharray)) {
                      UserAuthToken::insert($autharray);
                      DB::commit();
                      return response()->json(['status'=>$success['status'],'token'=>$success['_token']]);
                  }else{
                      $errors['couldnot_save'] = "We are unable to register now please try again later";
                      return response()->json(['status' => 0, 'errors' => $errors]);
                  }
              } catch (\Exception $e) {
                  DB::rollback();
                  //print_r($e->getMessage());exit;
                  $errors['couldnot_save'] = "We are unable to register now please try again later";
                  return response()->json(['status' => 0, 'errors' => $errors]);
              }

           }
           else
           {
               return response()->json(["message"=>"wrong credentials"]);
           }
            if(!$user){
               return response()->json(['status'=>0,'message'=>'We are unable to register now please try again later']);
            }
        }

    } catch (\Exception $e) {
        DB::rollback();
        print_r($e->getMessage());exit;
        $errors['couldnot_save'] = "We are unable to register now please try again later";
        return response()->json(['status' => 0, 'errors' => $errors]);
    }
}

public function appLogin(Request $request){

   $username = $request->username;
   $password = $request->password;
   $user = UserDetails::where('user_email',$username)->first();
   if(!$user){
      $user = UserDetails::where('username',$username)->first();
   }
   if(!$user){
      return response()->json(['status'=>0,'message'=>'Invalid login credentials']);
   }else{
      if (Hash::check($password, $user->password)) {
         Auth::login($user);
         $success['status']=1;
         $success['_token']=$user->createToken('MyApp')->accessToken;
         try {
            DB::beginTransaction();
            $autharray[] = [
               'user_id' =>  $user['id'],
               'token'   =>  $success['_token'],
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date('Y-m-d H:i:s')
           ];
            if (!empty($autharray)) {
                UserAuthToken::insert($autharray);
                DB::commit();
                return response()->json(['status'=>$success['status'],'token'=>$success['_token']]);
            }else{
                $errors['couldnot_save'] = "We are unable to register now please try again later";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            print_r($e->getMessage());exit;
            $errors['couldnot_save'] = "We are unable to register now please try again later";
            return response()->json(['status' => 0, 'errors' => $errors]);
        }
      }else{
         return response()->json(['status'=>0,'message'=>'Invalid login credentials']);
      }
   }
}

public function googleLogin(Request $request){
   $google_token = $request->google_token;
   $user = UserDetails::where(['user_email' => $google_token])->first();
   if($user){
      Auth::login($user);
      $success['details']=Auth::user();
      $success['status']=1;
      $success['_token']=$user->createToken('MyApp')->accessToken;
      try {
         DB::beginTransaction();
         $autharray[] = [
            'user_id' =>  $user['id'],
            'token'   =>  $success['_token'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
         if (!empty($autharray)) {
             UserAuthToken::insert($autharray);
             DB::commit();
             return response()->json(['status'=>$success['status'],'token'=>$success['_token'],]);
         }else{
             $errors['couldnot_save'] = "We are unable to register now please try again later";
             return response()->json(['status' => 0, 'errors' => $errors]);
         }
     } catch (\Exception $e) {
         DB::rollback();
         //print_r($e->getMessage());exit;
         $errors['couldnot_save'] = "We are unable to register now please try again later";
         return response()->json(['status' => 0, 'errors' => $errors]);
     }
   }
   return response()->json(['status'=>0,'message'=>'Invalid login credentials']);
}
public function view_profile(Request $request){

   $userid =$this->checkAuth($request);
   if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
   $user = UserDetails::where('id','=',$userid->user_id)->first();
   $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
   if (!strpos($user['image'],$url)) {
     $user['image']     = url('/').'/'.$user['image'];
   }
   if($user){
      echo json_encode(["status" => "success","data" => $user]);
    }else{
       echo json_encode(["status" => "error", "message" => "Please enter valid PIN"]);
    }

}
public function edit_profile(Request $request){

   $userid =$this->checkAuth($request);
   if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
   $user = UserDetails::where('id','=',$userid->user_id)->first();
   $rules = [
       'firstname' => 'required',
       'lastname' => 'required',
       'gender'=>'required',
       'email'=>'required|email|unique:local_users,user_email,'.$userid->user_id.',id',
       'dob'=>'required',
       'image'=>'required',
   ];
   $validation = Validator::make($request->all(), $rules);
   if ($validation->fails()) {
       return response()->json(['status' => false, 'message' => $validation->errors()->first()]);
   }
   $image=$request->image;
   $updateData   =   [
               'firstname'        => $request->firstname,
               'lastname'         => $request->lastname,
               'gender'           => $request->gender,
               'user_email'       => $request->email,
               'dob'              => date('Y-m-d',strtotime($request->dob)),
               'updated_at'       => date('Y-m-d H:i:s')
   ];
   if($request->image){
      define('UPLOAD_DIR', 'public/images/local_users/');
      $file = $request->file('image');
      //Display File Name
      $destinationPath = UPLOAD_DIR;
      $uniqueFileName = uniqid() . $file->getClientOriginalName();
      $file->move($destinationPath,$uniqueFileName);
      $image_name = $destinationPath.$uniqueFileName;
      $updateData['image'] = $image_name;
   }
   try{
           DB::beginTransaction();
           DB::table('local_users')->where('id',$userid->user_id)->update($updateData);
           DB::commit();
           return response()->json(['status'=>true,"message"=>'Your account information has been updated successfully.']);
   }catch(\Exception $e){
           DB::rollback();
           throw $e;
           return response()->json(['status'=>false,"message"=>"Something Went Wrong..! Please Try Again"]);
   }

}
public function edit_password(Request $request){

   $userid =$this->checkAuth($request);
   if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
   $user = UserDetails::where('id','=',$userid->user_id)->first();
   $rules = [
       'password'=>'required',
   ];
   $validation = Validator::make($request->all(), $rules);
   if ($validation->fails()) {
       return response()->json(['status' => false, 'message' => 'Please enter new password']);
   }

   $updateData   =   [
        'password'        =>bcrypt($request->password),
   ];
   try{
           DB::beginTransaction();
           DB::table('local_users')->where('id',$userid->user_id)->update($updateData);
           DB::commit();
           return response()->json(['status'=>true,"message"=>'Password Successfully Updated']);
   }catch(\Exception $e){
           DB::rollback();
           throw $e;
           return response()->json(['status'=>false,"message"=>"Something Went Wrong..! Please Try Again"]);
   }

}
public function edit_email(Request $request){

   $userid =$this->checkAuth($request);
  if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
  $user = UserDetails::where('id','=',$userid->user_id)->first();
   $rules = [

       'email'=>'required|email|unique:local_users,user_email,'.$userid->user_id.',id',
   ];
   $validation = Validator::make($request->all(), $rules);
   if ($validation->fails()) {
       return response()->json(['status' => false, 'message' => $validation->errors()->first()]);
   }

   $updateData   =   [
        'user_email'        =>$request->email,
   ];
   try{
           DB::beginTransaction();
           DB::table('local_users')->where('id',$userid->user_id)->update($updateData);
           DB::commit();
           return response()->json(['status'=>true,"message"=>'Your email updated Successfully.']);
   }catch(\Exception $e){
           DB::rollback();
           throw $e;
           return response()->json(['status'=>false,"message"=>"Something Went Wrong..! Please Try Again"]);
   }

}
public function view_faq(Request $request){

   $userid =$this->checkAuth($request);
   if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
   $questions = DB::table('faqs')->select('title')->get();
   $answers = DB::table('faqs')->select('description')->get();
   if(count($questions)>0){
      echo json_encode(["status" =>true,"questions" => $questions,"answers"=>$answers]);
    }else{
       echo json_encode(["status" => false, "message" => "No Results Found.."]);
    }

}
public function get_company(Request $request){

   $userid =$this->checkAuth($request);
   if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
   $user = UserDetails::where('id','=',$userid->user_id)->first();
   $company_id = $user->CompanyID;
   $verification ='';
   if($company_id!=''){
      $verification =  DB::table('company')
     ->select('CompanyName','CompanyCode')
     ->where('CompanyID','=',$company_id)
     ->get();
     if(count($verification)<=0){
       echo json_encode(["status" => "false", "message" => "Authentication key mismatched..."]); exit();
     }else{
       echo json_encode(["status" => "success","CompanyName" =>$verification[0]->CompanyName,"CompanyCode"=>$verification[0]->CompanyCode]);
     }
   }else{
       echo json_encode(["status" => "false", "message" => "Authentication key mismatched..."]); exit();
   }
}
public function edit_company(Request $request){

   $userid =$this->checkAuth($request);
  if(!$userid){ return response()->json(['status'=>0,'message'=>'Authentication key mismatched...']); }
  $user = UserDetails::where('id','=',$userid->user_id)->first();
  $rules = [

        'company_code'=>"required",
        'password'=>"required",
    ];
    $validation = Validator::make($request->all(), $rules);
    if ($validation->fails()) {
        return response()->json(['status' => false, 'message' => $validation->errors()->first()]);
    }
    $password = $request->password;
  if (Hash::check($password, $user->password)) {

    // $company = $location=DB::table('usercompanyaccess')
    //     ->join('company', 'company.CompanyID', '=', 'usercompanyaccess.CompanyID')
    //     ->select('usercompanyaccess.CompanyID')
    //     ->where('usercompanyaccess.UserID','=',$user->user_id)
    //     ->where('company.CompanyCode','=',$request->company_code)
    //     ->first();

    $company = $location=DB::table('local_users_company_accesses')
    ->join('company', 'company.CompanyID', '=', 'local_users_company_accesses.companyid')
    ->select('local_users_company_accesses.companyid')
    ->where('local_users_company_accesses.userid','=',$user->user_id)
    ->where('company.CompanyCode','=',$request->company_code)
    ->first();
   if($company){
      $updateData   =   [
               'CompanyID'        =>$company->CompanyID,
      ];
      try{
               DB::beginTransaction();
               DB::table('local_users')->where('id',$userid->user_id)->update($updateData);
               DB::commit();
               return response()->json(['status'=>true,"message"=>'Your company updated successfully.']);
      }catch(\Exception $e){
               DB::rollback();
               throw $e;
               return response()->json(['status'=>false,"message"=>"Something Went Wrong..! Please Try Again"]);
      }
   }else{
      return response()->json(['status'=>false,"message"=>'Company Access Permission Required']);
   }
  }else{
         return response()->json(['status'=>0,'message'=>'Invalid Password']);
    }

}
public function forgotPassword(Request $request)
{
    $email = $request->email;
    if ($email) {
        $user = UserDetails::where('user_email', '=', $request->email)->first();
        if ($user) {
            $string = "";
            $chars = "0123456789";
            $size = strlen($chars);
            for ($i = 0; $i < 6; $i++) {
                $string .= $chars[rand(0, $size - 1)];
            }
            $user->reset_otp = $string;
            try {
                $user->save();
                $data = array('reset_otp' => $string,'name'=>$user->firstname.' '.$user->lastname,'email'=> $user->user_email);
                Mail::to($email)->send(new ForgotPassword($data));
                return response()->json(['status' => true, 'message' => 'Otp has been sent to your email']);
            } catch (\Exception $e) {
                print_r($e->getMessage());
                $errors['email'] = "Could not send otp";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        } else {
            return response()->json(['status' => false, 'errors' => ['email' => 'Email entered is not registered with us']]);
        }
    } else {
        return response()->json(['status' => false, 'errors' => ['email3' => 'Email is not provided']]);
    }
}
public function verifyOtp(Request $request)
{
    $email = $request->email;
    $otp = $request->otp;
    if ($email) {
        $user = UserDetails::where('user_email', '=', $request->email)->first();
        if ($user) {
            if($user->reset_otp == $otp){
               return response()->json(['status' => 1,"message"=>"You have successfully varified."]);
            }else{
               return response()->json(['status' => 0,"message"=>"Invalid OTP."]);
            }
        } else {
            return response()->json(['status' => false, 'errors' => ['email' => 'Email entered is not registered with us']]);
        }
    } else {
        return response()->json(['status' => false, 'errors' => ['email3' => 'Email is not provided']]);
    }
}
public function resetPassword(Request $request)
{
    $email    = $request->email;
    $password = $request->password;
    if ($email) {
        $user = UserDetails::where('user_email', '=', $request->email)->first();
        if ($user) {
            $user->password = bcrypt($password);
            try {
                $user->save();
                return response()->json(['status' => true, 'message' => 'Password has been updated successfully.']);
            } catch (\Exception $e) {
                $errors['email'] = "Something went wrong please try again later.";
                return response()->json(['status' => 0, 'errors' => $errors]);
            }
        } else {
            return response()->json(['status' => false, 'errors' => ['email' => 'Email entered is not registered with us']]);
        }
    } else {
        return response()->json(['status' => false, 'errors' => ['email3' => 'Email is not provided']]);
    }
}
public function enquiry(Request $request){
   $rules = [
   'email'     => "required|email",
   'name'      => "required",
   'mobile'    => "required",
   'message'   => "required",
   ];
   $messages = [
       'email.required'     => "Email is required",
       'name.required' => "Full Name is required",
       'mobile.required'  => "Mobile Number is required",
       'message.required'       => "Message is required",
   ];
   $validator = Validator::make(request()->all(), $rules, $messages);
   if (!$validator->passes()) {
       $messages = $validator->messages();
       $errors = [];
       foreach ($rules as $key => $value) {
           $err = $messages->first($key);
           if ($err) {
               $errors[$key] = $err;
           }
       }
       return response()->json(['status' => 0, 'errors' => $errors]);
   }
   $email             = $request->email;
   $name              = $request->name;
   $mobile            = $request->mobile;
   $message           = $request->message;
   try {
       DB::beginTransaction();
       $attr_array[] = [
           'email' =>  $email,
           'name'  => $name,
           'mobile'   => $mobile,
           'subject'   => $message,
           'created_at' => date('Y-m-d H:i:s'),
           'updated_at' => date('Y-m-d H:i:s')
       ];
       if (!empty($attr_array)) {
           DB::table('enquiries')->insert($attr_array);
           DB::commit();
           return response()->json(['status' => 1,"message"=>"Your enquiry has been submitted."]);
           }else{
               return response()->json(['status' => 0,"message"=>"Unale to register now "]);
           }
   } catch (\Exception $e) {
       DB::rollback();
       print_r($e->getMessage());exit;
       $errors['couldnot_save'] = "We are unable to register now please try again later";
       return response()->json(['status' => 0, 'errors' => $errors]);
   }
}
   /// generate user id
   public function generateUserId(){
      $userid   =   mt_rand(10000,99999);
      if(DB::table('local_users')->where('user_id',$userid)->exists()){
         return generateUserId();
      }
      return $userid;
   }
   /// check authentication
   public function checkAuth($data){
   $userid = DB::table('user_auth_tokens')->where('token',$data->token)->select('user_id')->first();
   return $userid;
   }
   // connect with company
   public function getConnectionDetails($companyID){

   // Artisan::call('config:cache');
   $getConnectionDetails = Company::select('conhostname', 'condbname', 'conusername', 'conpassword')
                                    ->where('CompanyID', $companyID)
                                    ->first();
   if($getConnectionDetails->conhostname != '' && $getConnectionDetails->condbname != '' && $getConnectionDetails->conusername != ''){
      config(['database.connections.mysql_source.host' => $getConnectionDetails->conhostname]);
      config(['database.connections.mysql_source.database' => $getConnectionDetails->condbname]);
      config(['database.connections.mysql_source.username' => $getConnectionDetails->conusername]);
      config(['database.connections.mysql_source.password' => $getConnectionDetails->conpassword]);

   }else{
      return response()->json(['status'=>false,"message"=> $e->getMessage()]);
   }
   }

}
