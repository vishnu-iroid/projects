<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;

use App\Enquiry;


class EnquiryController extends Controller
{   
    public function getEnquiry(Request $request){
        $search = $request->search;
        $enquiry = Enquiry::orderBy('created_at','ASC')
        ->where(function($enquiry) use ($search){
            $enquiry->orWhere('name','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%')
                    ->orWhere('mobile','like','%'.$search.'%');
        })->paginate(10);
        return response()->json(['status'=>true,'enquiry'=>$enquiry]);
    }
    public function deleteEnquiry(Request $request){
        print_r($request->all());exit;
       if($request->enquiryid){
           try{
                Enquiry::where('id',$request->enquiryid)->delete();
                return response()->json(['status'=>1,'message'=>'Successfully deleted']);
           }catch(\Exeption $e){
                return response()->json(['status'=>0,'message'=>$e->getMessage()]);
           }
       }else{
           return response()->json(['status'=>0,'message'=>'Something went wrong.']);
       } 
    }
}
