<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;

use App\Faq;

class FaqController extends Controller
{
    public function getFaq(Request $request){
        $data = Faq::orderBY('created_at',"ASC")->paginate(10);
        return response()->json(['status'=>true,'enquiry'=>$data]);
    }
    public function addFaq(Request $request){
        $rules = [
            'question' => 'required|max:250',
            'answer'   => 'required',
        ];
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>true,'message'=>$validation->errors()->first()]);
        }
        $faq              = new Faq;
        $faq->title       = $request->question;
        $faq->description = $request->answer;
        try{
            DB::beginTransaction();
            $faq->save();
            DB::commit();
            return response()->json(['status'=>true,'message'=>'Successfully added']);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>true,'message'=>$e->getMessage()]);
        }
    }
    public function editFaq(Request $request){
        $rules = [
            'question' => 'required|max:250',
            'answer'   => 'required',
        ];
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>true,'message'=>$validation->errors()->first()]);
        }
        $faq              = Faq::where('id',$request->id)->first();
        $faq->title       = $request->question;
        $faq->description = $request->answer;
        try{
            DB::beginTransaction();
            $faq->save();
            DB::commit();
            return response()->json(['status'=>true,'message'=>'Successfully updated']);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>true,'message'=>$e->getMessage()]);
        }
    }
    public function deleteFaq(Request $request){
        if($request->faqid){
            try{
                DB::beginTransaction();
                Faq::where('id',$request->faqid)->delete();
                DB::commit();
                return response()->json(['status'=>true,'message'=>"Seccessfully deleted"]);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=>true,'message'=>$e->getMessage()]);
            }
        }else{
            return response()->json(['status'=>true,'message'=>"Something went wrong."]);
        }
    }
}
