<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Auth;
use DB;
use Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\File;

use App\Company;
use App\User;
use App\User2;
use App\LocalUsersCompanyAccess;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // // @superAdmin password change
    // public function superAdminSecondLogin(Request $request){
    //     $user=user2::where('password',$request->password)->first();
    //     if($user){
    //         Auth::login($user);
    //         $success['details']=Auth::user();
    //         $success['token']=$user->createToken('MyApp')->accessToken;
    //         return response()->json(['details'=>$success]);
    //     }
    //     else
    //     {
    //         return response()->json(["message"=>"wrong credentials"]);
    //     }

    // }

    // @superAdmin password change
    public function changePasswordSuperAdmmin(Request $request){
        $superAdmin = User2::where('isAdmin',1)->first();
        $rules = [
            'password' => 'required',
            'email'    =>  'required|email|unique:user,email,'.$superAdmin->id,
        ];
        $validation = validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>false,'message'=>$validation->error()->first()]);
        }
        $superAdmin->email        = $request->email;
        $superAdmin->password     = bcrypt($request->password);
        $superAdmin->password_ref = $request->password;
        try{
            DB::beginTransaction();
            $superAdmin->save();
            DB::commit();
            return response()->json(['status'=>true,'message'=>'Successfully updated']);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }

    // @Super admin dashboard
    public function adminDashboardData(){
        $company=DB::table('company')->select(DB::raw('COUNT(CompanyID) as companies'))->get();
        // $chain=DB::table('chain')->select(DB::raw('COUNT(ChainId) as chains'))->get();
        $users=DB::table('user')->select(DB::raw('COUNT(user_id) as users'))->get();
        // $location=DB::table('location')->select(DB::raw('COUNT(LocationId) as locations'))->get();
        $data=array(
            // 'companies'=>$company[0]->companies,
            // 'chains'=>$chain[0]->chains,
            // 'users'=>$users[0]->users,
            // 'locations'=>$location[0]->locations,
            'companies'=>$company[0]->companies,
            'chains'=> "",
            'users'=>$users[0]->users,
            'locations'=> "",
        );
        return response()->json([$data]);
    }
       // -------------------------------------------------------------------
    // company db connection
    public function getCompanyConnection($companyID){
        $getCompanyConnection = Company::select('conhostname', 'condbname', 'conusername', 'conpassword')
                                        ->where('CompanyID',$companyID)->first();

        return $getCompanyConnection;

        if($getCompanyConnection->conhostname != '' && $getCompanyConnection->condbname != '' && $getCompanyConnection->conusername != '' ){
            config(['database.connections.mysql_source.host'     => $getCompanyConnection->conhostname]);
            config(['database.connections.mysql_source.database' => $getCompanyConnection->condbname]);
            config(['database.connections.mysql_source.username' => $getCompanyConnection->conusername]);
            config(['database.connections.mysql_source.password' => $getCompanyConnection->conpassword]);
        }else{
            // return response()->json(['status'=>false,"message"=> $e->getMessage()]);
            return response()->json(['status'=>false,"message"=> "Failed to establish company database connection."]);
        }
    }

    // -------------------------------------------------------------------


    public function userTableDetails(Request $request){
        $search=$request->search;
        $user_data = user2::select("*")->where('isAdmin','!=',1)
                    ->orderBy('created_at','desc')
                    ->where(function($user_data) use ($search){
                        $user_data->orWhere('user_name', 'like', '%'.$search.'%')
                                  ->orWhere('email', 'like', '%'.$search.'%')
                                  ->orWhere('companyid', 'like', '%'.$search.'%');
                    })->paginate(10);
        foreach($user_data as $value){
            $companyID = array();$companyName = array();
            $companies = DB::table('usercompanyaccess')->select('CompanyID')->where('UserID',$value->user_id)->get();
            foreach($companies as $row){
                $details = Company::where('CompanyID',$row->CompanyID)->select('CompanyID','CompanyName')->first();
                if($details){
                    $companyID[] = $details->CompanyID;
                    $companyName[] = $details->CompanyName;
                }
            }
            $value->companyid = $companyID;
            $value->companyName = $companyName;
            unset($companyID); unset($companyName);
        }
        return response()->json(['details'=>$user_data,]);
    }
    public function userSearch(Request $request){
            $search=$request->search;
            $user_data = user2::select("*")
            ->orderBy('created_at','desc')->where('isAdmin','!=',1)
            ->where(function($user_data) use ($search){
                $user_data->orWhere('user_name', 'like', '%'.$search.'%')
                          ->orWhere('email', 'like', '%'.$search.'%')
                          ->orWhere('companyid', 'like', '%'.$search.'%');
            })->paginate(10);
            return response()->json([
                'details'=>$user_data,
            ]);
    }

    public function companyTableDetails(Request $request){
        $search=$request->search;
        $user_data=DB::table('company')->select("*")
        ->orderBy('created_at','desc')
        ->whereNull('deleted_at')
        ->where(function($user_data) use ($search){
            $user_data->orWhere('CompanyName','like', '%'.$search.'%')
                     ->orWhere('Email', 'like', '%'.$search.'%')
                     ->orWhere('City', 'like', '%'.$search.'%')
                     ->orWhere('Country', 'like', '%'.$search.'%');
        })->paginate(10);
        return response()->json([
                'details'=>$user_data,
        ]);
            // print_r($search);
    }
    public function companySearch(Request $request){
            $search=$request->search;
            $user_data=DB::table('company')->select("*")
            ->orderBy('created_at','desc')
            ->whereNull('deleted_at')
            ->where(function($query) use ($search){
              $query->where('CompanyName','like', '%'.$search.'%')
                     ->orWhere('Email', 'like', '%'.$search.'%')
                     ->orWhere('City', 'like', '%'.$search.'%')
                     ->orWhere('Country', 'like', '%'.$search.'%');
            })
            ->paginate(10);
            return response()->json([
                'details'=>$user_data,
            ]);
            // print_r($search);
    }

    public function register_company(Request $request){
        // id=$request->id;
        $rules=[
            'companyid'=>'required|min:3|unique:company,CompanyID',
            'companyname'=>'required',
            'contact'=>'required',
            'address1'=>'nullable',
            'address2'=>'nullable',
            'city'=>'nullable',
            'country'=>'nullable',
            'phone'=>'nullable|unique:company,Phone1',
            'fax'=>'nullable',
            'email'=>'required|email|unique:company,Email',
            'currency'=>'nullable',
            'retainedaccount'=>'nullable',
            'accuretainedaccount'=>'nullable',
            'erroraccount'=>'nullable',
            'errortolerance'=>'required',
            'manual'=>'nullable',
            'comm'=>'nullable',
            'postingtype'=>'required',
            'qtyminor'=>'required',
            'image'=>'nullable',
            'conhostname'=>'required',
            'condbname'=>'required',
            'conusername'=>'required',
            'conpassword'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $id=$request->chainid;
        $image=$request->image;
        $code=substr(md5(time()),0,12);
        $CompanyCode = $request->companyid."".$code;
        if(strlen($CompanyCode)>12){
            $CompanyCode=substr($CompanyCode,0,12);
        }
        $code_check=DB::table('company')->where('CompanyCode',$CompanyCode)->count();
        if($code_check >=1){
            return response()->json(['status' => false, 'message' => 'Failed to register']);
        }
        if($request->Image){
           define('UPLOAD_DIR', 'public/images/company/');
               $img = $request->Image;
               preg_match("/data:image\/(.*?);/",$img,$image_extension); // extract the image extension
               $img = preg_replace('/data:image\/(.*?);base64,/','',$img);
               $img = str_replace(' ', '+', $img);
               $data = base64_decode($img);
               $file = UPLOAD_DIR . str::random(10). '.' . $image_extension[1];
               $imgname =   "http://businessapp.axistc.com/axis/".$file;
               $success = file_put_contents($file, $data);
            $data  =   [
                'CompanyID'             =>$request->companyid,
                'CompanyName'           =>$request->companyname,
                'CompanyCode'           =>$CompanyCode,
                'Contact'               =>$request->contact,
                'Address1'              =>$request->address1,
                'Address2'              =>$request->address2,
                'City'                  =>$request->city,
                'Country'               =>$request->country,
                'Phone1'                =>$request->phone,
                'FaxNo'                 =>$request->fax,
                'Email'                 =>$request->email,
                'Website'               =>$request->website,
                'CurrencyId'            =>$request->currency,
                'conhostname'           =>$request->conhostname,
                'condbname'             =>$request->condbname,
                'conusername'           =>$request->conusername,
                'conpassword'           =>$request->conpassword,
                'RetainedAccountId'     =>$request->retainedaccount,
                'AccuRetainedAccountId' =>$request->accuretainedaccount,
                'SysErrorAccount'       =>$request->erroraccount,
                'SysErrorTolerance'     =>$request->errortolerance,
                'ManualPath'            =>$request->manual,
                'CommPath'              =>$request->comm,
                'SopPostingType'        =>$request->postingtype,
                'SopQtyMinor'           =>$request->qtyminor,
                'CompanyLogo'           =>$imgname,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
            ];
        }
        else{
            $data    =   [
                'CompanyID'             =>$request->companyid,
                'CompanyName'           =>$request->companyname,
                'CompanyCode'           =>$CompanyCode,
                'Contact'               =>$request->contact,
                'Address1'              =>$request->address1,
                'Address2'              =>$request->address2,
                'City'                  =>$request->city,
                'Country'               =>$request->country,
                'Phone1'                =>$request->phone,
                'FaxNo'                 =>$request->fax,
                'Email'                 =>$request->email,
                'Website'               =>$request->website,
                'CurrencyId'            =>$request->currency,
                'conhostname'           =>$request->conhostname,
                'condbname'             =>$request->condbname,
                'conusername'           =>$request->conusername,
                'conpassword'           =>$request->conpassword,
                'RetainedAccountId'     =>$request->retainedaccount,
                'AccuRetainedAccountId' =>$request->accuretainedaccount,
                'SysErrorAccount'       =>$request->erroraccount,
                'SysErrorTolerance'     =>$request->errortolerance,
                'ManualPath'            =>$request->manual,
                'CommPath'              =>$request->comm,
                'SopPostingType'        =>$request->postingtype,
                'SopQtyMinor'           =>$request->qtyminor,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
            ];
        }
        try{
                DB::beginTransaction();
                DB::table('company')->insert($data);
                DB::commit();
                return response()->json(['status'=>true,"message"=>'Successfully Added']);
        }catch(\Exception $e){
                // throw $e;
                DB::rollback();
                return response()->json(['status'=>false,"message"=>$e->getMessage()]);
        }
    }
    public function updateComp(Request $request){
          $rules=[
            'companyid'     =>'required|min:3|unique:company,CompanyID,'.$request->id,
            'companyname'   =>'required',
            'contact'       =>'required',
            'address1'      =>'nullable',
            'address2'      =>'nullable',
            'city'          =>'nullable',
            'country'       =>'nullable',
            'phone'         =>'nullable|unique:company,Phone1,'.$request->id,
            'fax'           =>'nullable',
            'email'         =>'required|unique:company,Email,'.$request->id,
            'currency'      =>'nullable',
            'retainedaccount'=>'nullable',
            'accuretainedaccount'=>'nullable',
            'erroraccount'  =>'nullable',
            'errortolerance'=>'required',
            'manual'        =>'nullable',
            'comm'          =>'nullable',
            'postingtype'   =>'required',
            'qtyminor'      =>'required',
            'image'         =>'nullable',
            'conhostname'   =>'required',
            'condbname'     =>'required',
            'conusername'   =>'required',
            'conpassword'   =>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // return response()->json(['status' => false, 'message' => 'Failed to update']);
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $image=$request->image;
        $companyold=DB::table('company')->select('CompanyID')->where('id',$request->id)->get()->toArray();
        $oldcompany=$companyold[0]->CompanyID;
        $this->getCompanyConnection($oldcompany);
        // return response()->json($oldcom);
        $update_data = [
                    'CompanyID'             =>$request->companyid,
                    'CompanyName'           =>$request->companyname,
                    'Contact'               =>$request->contact,
                    'Address1'              =>$request->address1,
                    'Address2'              =>$request->address2,
                    'City'                  =>$request->city,
                    'Country'               =>$request->country,
                    'Phone1'                =>$request->phone,
                    'FaxNo'                 =>$request->fax,
                    'Email'                 =>$request->email,
                    'Website'               =>$request->website,
                    'CurrencyId'            =>$request->currency,
                    'conhostname'           =>$request->conhostname,
                    'condbname'             =>$request->condbname,
                    'conusername'           =>$request->conusername,
                    'conpassword'           =>$request->conpassword,
                    'RetainedAccountId'     =>$request->retainedaccount,
                    'AccuRetainedAccountId' =>$request->accuretainedaccount,
                    'SysErrorAccount'       =>$request->erroraccount,
                    'SysErrorTolerance'     =>$request->errortolerance,
                    'ManualPath'            =>$request->manual,
                    'CommPath'              =>$request->comm,
                    'SopPostingType'        =>$request->postingtype,
                    'SopQtyMinor'           =>$request->qtyminor,
                    'updated_at'            => date('Y-m-d H:i:s'),
        ];
        if($request->Image){
            $data = DB::table('company')->where('id',$request->id)->first();
            $name   =   basename($data->CompanyLogo);
            if(file_exists(public_path('images/company/').$name)){
                File::delete(public_path('images/company/').$name);
            }
            define('UPLOAD_DIR', 'public/images/company/');
           $img = $request->Image;
           preg_match("/data:image\/(.*?);/",$img,$image_extension); // extract the image extension
           $img = preg_replace('/data:image\/(.*?);base64,/','',$img);
           $img = str_replace(' ', '+', $img);
           $data = base64_decode($img);
           $file = UPLOAD_DIR . str::random(10). '.' . $image_extension[1];
           $imgname =   "http://businessapp.axistc.com/axis/".$file;
           $success = file_put_contents($file, $data);
           $update_data['CompanyLogo'] = $imgname;
        }
    	try{
    		DB::beginTransaction();
    		$comp_update=DB::table('company')->where('id',$request->id)->update($update_data);
        	if($request->companyid != ""){
        	DB::table('usercompanyaccess')->where('CompanyID',$oldcompany)->update([
                "CompanyID"=>$request->companyid,
                "Companyname"=>$request->companyname
            ]);
            if($request->companyid == $oldcompany){
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>'Successfully Updated']);
            }else{
                DB::table('user')->where('companyid',$oldcompany)->update([
                    "companyid"=>$request->companyid,
                ]);
                DB::table('local_users')->where('CompanyID',$oldcompany)->update([
                    "CompanyID"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('chain')->where('CompanyId',$oldcompany)->update([
                    "CompanyId"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('chainaccess')->where('CompanyId',$oldcompany)->update([
                    "CompanyId"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('location')->where('CompanyId',$oldcompany)->update([
                    "CompanyId"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('locationaccess')->where('CompanyID',$oldcompany)->update([
                    "CompanyID"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('payment_types')->where('CompanyID',$oldcompany)->update([
                    "CompanyID"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('sales_header')->where('CompanyID',$oldcompany)->update([
                    "CompanyID"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('sales_line')->where('CompanyID',$oldcompany)->update([
                    "CompanyID"=>$request->companyid,
                ]);
                DB::connection('mysql_source')->table('sales_payment')->where('CompanyID',$oldcompany)->update([
                    "CompanyID"=>$request->companyid,
                ]);
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>'Successfully Updated']);
                }
        	}
        }catch(\Exception $e){
            // throw $e;
            DB::rollback();
            // return response()->json(['status'=>false,"message"=>$e->getMessage()]);
            return response()->json(['status'=>false,"message"=>"Something went wrong. please try again."]);
        }
    }

    public function deleteCompany(Request $request){
        if($request->companyid){
            $this->getCompanyConnection($request->companyid);
            $data = DB::table('company')->where('CompanyID',$request->companyid)->first();
            $name   =   basename($data->CompanyLogo);
            if(file_exists(public_path('images/company/').$name)){
                File::delete(public_path('images/company/').$name);
            }
            DB::beginTransaction();
            try{
                Company::where('CompanyID',$request->companyid)->delete();
                $users_id = DB::table('usercompanyaccess')->where('CompanyID',$request->companyid)->select('UserID')->get();
                if($users_id){
                    foreach($users_id as $user){
                        $user_count = DB::table('usercompanyaccess')->where('UserID',$user->UserID)->count();
                        DB::table('usercompanyaccess')->where('UserID',$user->UserID)->where('CompanyID',$request->companyid)->delete();
                        if($user_count == 1){
                            $user_count = User2::where('user_id',$user->UserID)->delete();
                        }
                    }
                }
                $local_users_id = DB::table('local_users_company_accesses')->where('companyid',$request->companyid)->select('userid')->get();
                if($local_users_id){
                    foreach($local_users_id as $local_user){
                        $local_user_count = DB::table('local_users_company_accesses')->where('userid',$local_user->userid)->count();
                        DB::table('local_users_company_accesses')->where('userid',$local_user->userid)->where('companyid',$request->companyid)->delete();
                        if($local_user_count = 1){
                            $local_user_count = DB::table('local_users')->where('user_id',$local_user->userid)->update(['CompanyID'=>'KCL']);
                        }
                    }
                }
                DB::Commit();
                return response()->json(['status'=>true,"message"=>'Successfully Deleted']);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=>false,"message"=>$e->getMessage()]);
            }
        }else{
            return response()->json(['status'=>false,"message"=>"Something went wrong."]);
        }
    }

    public function updateUser(Request $request){
        $old_user_id = DB::table('user')->where('id',$request->id)->select('user_id')->first();
        $rules=[
            'userid'   => 'required|unique:user,user_id,'.$request->id,
            'username' => 'required|unique:user,user_name,'.$request->id,
            'email'    => 'required|email|unique:user,email,'. $request->id,
            "companyid"    => "required|array|min:1",
            "companyid.*"  => "required|string|distinct|min:1",
            'status'   => 'required',
            'password' => 'required',
        ];
        // $messages =[
        //     'userid.required'=>'User Id is required',
        //     'username.required'=>'User Name is required',
        //     'email.required'=>'Email is required',
        //     'status.required'=>'Status is Required',
        //     'password.required'=>'Password is required',
        // ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
             return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $password=bcrypt($request->password);
        DB::beginTransaction();
        try{
            $company_ext = DB::table('user')->where('id',$request->id)->select('companyid')->first();
            if(!in_array($company_ext->companyid,$request->companyid)){
                DB::table('user')->where('id',$request->id)->update(["companyid"=>$request->companyid[0]]);
            }
            $user_update=DB::table('user')->where('id',$request->id)
            ->update([
                'user_id'       =>$request->userid,
                'user_name'     =>$request->username,
                'email'         =>$request->email,
                'status'        =>$request->status,
                'password'      =>$password,
                'password_ref'  =>$request->password,
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);
            DB::table('usercompanyaccess')->where('UserID',$request->userid)->delete();
            foreach($request->companyid as $company){
                $company_name=DB::table('company')->where('CompanyID',$company)->select('CompanyName')->first();
                DB::table('usercompanyaccess')->where('UserID',$old_user_id->user_id)->insert([
                    'UserID'        => $request->userid,
                    'CompanyID'     => $company,
                    'CompanyName'   => $company_name->CompanyName,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
            return response()->json(['status'=>true,"message"=>'Successfully Updated']);
        }catch(\Exception $e){
            // throw $e;
            DB::rollback();
            return response()->json(['status'=>false,"message"=>$e->getMessage()]);
        }
    }

    public function register_user(Request $request){
        $rules=[
            'userid'=>'required|numeric|unique:user,user_id',
            'username'=>'required|unique:user,user_name',
            'email'=>'required|email|unique:user,email',
            "companyid"    => "required|array|min:1",
            "companyid.*"  => "required|string|distinct|min:1",
            'status'=>'required',
            'password'=>'required',
        ];
        // $messages =[
        //     'userid.required'=>'User Id is required',
        //     'username.required'=>'User Name is required',
        //     'email.required'=>'Email is required',
        //     'status.required'=>'Status is Required',
        //     'password.required'=>'Password is required',
        // ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
             return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $password=bcrypt($request->password);
        DB::beginTransaction();
        try{
            $user_insert=DB::table('user')
                        ->insert([
                            'user_id'       =>$request->userid,
                            'user_name'     =>$request->username,
                            'companyid'     =>$request->companyid[0],
                            'email'         =>$request->email,
                            'status'        =>$request->status,
                            'password'      =>$password,
                            'password_ref'  =>$request->password,
                            "isAdmin"       =>0,
                            "adminCode"     =>NULL,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
            foreach($request->companyid as $company){
                $company_name=DB::table('company')->where('CompanyID',$company)->select('CompanyName')->first();
                DB::table('usercompanyaccess')->insert([
                    'UserID'        =>$request->userid,
                    'CompanyID'     =>$company,
                    'CompanyName'   =>$company_name->CompanyName,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
            return response()->json(['status'=>true,"message"=>'Successfully Added']);
        }catch(\Exception $e){
            DB::rollback();
            // throw $e;
            return response()->json(['status'=>false,"message"=>$e->getMessage()]);
        }
    }

    public function deleteUser(Request $request){
        if($request->userid){
             try{
                DB::beginTransaction();
                DB::table('user')->where('user_id',$request->userid)->delete();
                DB::table('usercompanyaccess')->where('UserID',$request->userid)->delete();
                DB::Commit();
                return response()->json(['status'=>true,"message"=>'Successfully Deleted']);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=>false,"message"=>$e->getMessage()]);
            }
        }else{
            return response()->json(['status'=>false,"message"=>"Something went wrong."]);
        }
    }

    public function companyAccesstable(Request $request){
        $search=$request->search;
        $data=DB::table('user')->where('isAdmin','!=',1)
                ->select('user_id','user_name')
                ->where(function($data) use($search){
                    $data->orWhere('user.user_name','like', '%'.$search.'%');
                })->paginate(10);
        foreach($data as $value){
            $companyID = array();$companyName = array();
            $companies = DB::table('usercompanyaccess')->select('CompanyID')->where('UserID',$value->user_id)->get();
            foreach($companies as $row){
                $details = Company::where('CompanyID',$row->CompanyID)->select('id','CompanyID','CompanyName','CompanyCode')->first();
                if($details){
                    $companyID[] = $details->CompanyID;
                    $companyName[] = $details->CompanyName;
                }
            }
            $value->companyID = $companyID;
            $value->companyName = $companyName;
            unset($companyID); unset($companyName);
        }
        return response()->json(["data"=>$data]);
    }
    public function updateCompanyAccess(Request $request){
        $rules=[
            'userid'      =>'required',
            "companyid"   => "required|array|min:1",
            "companyid.*" => "required|string|distinct|min:1",
        ];
        // $messages =[
        //     'userid.required'=>'User Id is required',
        //     'companyid.required'=>'Company Id is required',
        // ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $company_ext = DB::table('user')->where('user_id',$request->userid)->select('companyid')->first();
        if(!in_array($company_ext->companyid,$request->companyid)){
            DB::table('user')->where('user_id',$request->userid)->update(["companyid"=>$request->companyid[0]]);
        }
        try{
            DB::beginTransaction();
            DB::table('usercompanyaccess')->where('UserID',$request->userid)->delete();
            foreach($request->companyid as $company){
                $company_name=DB::table('company')->where('CompanyID',$company)->select('CompanyName')->first();
                DB::table('usercompanyaccess')->insert([
                    'UserID'     => $request->userid,
                    "CompanyName"=> $company_name->CompanyName,
                    'CompanyID'  => $company,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
            return response()->json(['status'=>true,"message"=>'Successfully Updated']);
        }catch(\Exception $e){
            // throw $e;
            DB::rollback();
            return response()->json(['status'=>false,"message"=>$e->getMessage()]);
        }
    }

    public function deleteCompanyAccess(Request $request){
        if($request->userid){
            try{
                DB::beginTransaction();
                DB::table('usercompanyaccess')->where('UserID',$request->userid)->delete();
                DB::table('user')->where('user_id','=',$request->userid)->update(['companyid'=>NULL]);
                DB::commit();
                return response()->json(['status'=>true,"message"=>'Successfully Deleted']);
            }catch(\Exception $e){
                DB::rollback();
                // throw $e;
                return response()->json(['status'=>false,"message"=>$e->getMessage()]);
            }
        }else{
             return response()->json(['status'=>false,"message"=>"Something went wrong."]);
        }
    }

    public function companyList(Request $request){
        $data= Company::select('CompanyID','CompanyName')->get();
        return response()->json(["data"=>$data]);
    }

    public function regCurrency(Request $request){
        $rules=[
            'currencyid'=>'required|unique:currency,CurrencyId',
        ];
        // $messages =[
        //     'currencyid.required'=>'urrency Id is required',
        //     'currencyid.unique'=>'Currency Id must be unique',
        // ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
           return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $datacurrency=array(
            'CurrencyId'        =>$request->currencyid,
            'CurrencyDefault'   =>$request->currencydefault,
            'CurrencyName'      =>$request->description,
            'CurrencySymbol'    =>$request->symbol,
            'CurrencyUnit'      =>$request->currencyunit,
            'CurrencyConnect'   =>$request->currencyconnect,
            'CurrencySubUnit'   =>$request->currencysubunit,
            'CurrencyRate'      =>$request->currencyrate,
            'Currencydecimals'  =>$request->currencydecimals,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        DB::beginTransaction();
        try{
            DB::table('currency')->insert($datacurrency);
            DB::commit();
            return response()->json(['status'=>true,"message"=>'Successfully Added']);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false,"message"=>$e->getMessage()]);
        }
    }
    public function currencyList(Request $request){
        $search=$request->search;
        $currencylist=DB::table('currency')->select('*')
        ->where(function($currencylist) use ($search){
            $currencylist->orWhere('CurrencyName','like', '%'.$search.'%')
                         ->orWhere('CurrencySymbol', 'like', '%'.$search.'%')
                         ->orWhere('CurrencyUnit', 'like', '%'.$search.'%');
        })->paginate(10);
        return response()->json(['currency'=>$currencylist]);
    }

    public function currencySearch(Request $request){
        $search=$request->search;
        $currencylist=DB::table('currency')->select('*')
        ->orderBy('created_at','desc')
        ->where('CurrencyName','like', '%'.$search.'%')
        ->orWhere('CurrencySymbol', 'like', '%'.$search.'%')
        ->orWhere('CurrencyUnit', 'like', '%'.$search.'%')->get();
        return response()->json(['currency'=>$currencylist]);
    }

    public function currencyDelete(Request $request){
        if($request->id){
            try{
                DB::beginTransaction();
                $currencylist=DB::table('currency')->where('id',$request->id)->delete();
                DB::commit();
                return response()->json(['status'=>true,"message"=>'Successfully Deleted']);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=>false,"message"=>$e->getMessage()]);
            }
        }else{
            return response()->json(['status'=>false,"message"=>"Something went wrong."]);
        }
    }

    public function currencyEdit(Request $request){
        $rules=[
            'currencyid'=>'required|unique:currency,CurrencyId,'.$request->id,
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
           return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        DB::beginTransaction();
        try{
            DB::table('currency')->where('id',$request->id)->update([
                'CurrencyId'        =>$request->currencyid,
                'CurrencyDefault'   =>$request->currencydefault,
                'CurrencyName'      =>$request->description,
                'CurrencySymbol'    =>$request->symbol,
                'CurrencyUnit'      =>$request->currencyunit,
                'CurrencyConnect'   =>$request->currencyconnect,
                'CurrencySubUnit'   =>$request->currencysubunit,
                'CurrencyRate'      =>$request->currencyrate,
                'Currencydecimals'  =>$request->currencydecimals,
            ]);
            DB::commit();
            return response()->json(['status'=>true,"message"=>'Successfully Updated']);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false,"message"=>$e->getMessage()]);
        }
    }
    public function currencyDropDown(Request $request){
        $currecies=DB::table('currency')->select('CurrencyId','CurrencyName')->get();
        return response()->json(['currencies'=>$currecies]);
    }
    public function getUsersAppReg(Request $request){
        $search = $request->search;
        $data = DB::table('local_users')->where('CompanyID','KCL')->orderBy('created_at','DESC')->where(function($data) use ($search){
                        $data->orWhere('user_name','like', '%'.$search.'%')
                                    ->orWhere('user_email', 'like', '%'.$search.'%')
                                    ->orWhere('user_phone', 'like', '%'.$search.'%');
                    })->paginate(10);
        return response()->json(['status'=>true,'data'=>$data]);
    }
    public function UsersAppRegAction(Request $request){
        $rules = [
            'userid'    => 'required',
            "companyid"    => "required|array|min:1",
            "companyid.*"  => "required|string|distinct|min:1",
        ];
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return response()->json(['status'=>false,'message'=>$validation->errors()->first()]);
        }
        try{
            DB::beginTransaction();
            DB::table('local_users')->where('user_id',$request->userid)->update(['CompanyID'=>$request->companyid[0]]);
            LocalUsersCompanyAccess::where('userid',$request->userid)->delete();
            foreach($request->companyid as $company){
                LocalUsersCompanyAccess::insert([
                    'userid'     => $request->userid,
                    'companyid'  => $company,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
            DB::Commit();
            return response()->json(['status'=>true,'message'=>'Successfully Added']);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>true,'message'=>$e->getMessage()]);
        }
    }
}
