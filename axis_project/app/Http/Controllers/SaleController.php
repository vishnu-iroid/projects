<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DB;
use App\Company;
use App\SalesLine;
use App\SalesHeader;
use App\PaymentType;
use App\SalesPayment;

class SaleController extends Controller
{
    /*
    * To inseting the values to sales_header table
    * @params CompanyID, ChainID, LocationID, VoucherNo, VoucherDate, Discount, SalesTaxAmount, ServiceTaxAmount, SalesTaxBaseAmount,
        ServiceTaxBaseAmount, NetSalesAfterDiscountAndTax
    */
    public function SalesHeader(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'                   => 'required',
            'ChainID'                     => 'required',
            'LocationID'                  => 'required',
            "VoucherNo"                   => "required",
            "VoucherDate"                 => "required",
            "Discount"                    => "required",
            "SalesTaxAmount"              => "required",
            'ServiceTaxAmount'            => "required",
            'SalesTaxBaseAmount'          => 'required',
            'ServiceTaxBaseAmount'        => 'required',
            'NetSalesAfterDiscountAndTax' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $attributes = $request->all();
                $this->getConnectionDetails($request->CompanyID);
                unset($attributes['api_token']);
                DB::connection('mysql_source')
                    ->table('sales_header')
                    ->insert($attributes);
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);
            }
        }
    }

    /*
    * To inseting the values to sales_line table
    * @params CompanyID, ChainID, LocationID, VoucherNo, VoucherDate
    */
    public function salesLine(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'   => 'required',
            'ChainID'     => 'required',
            'LocationID'  => 'required',
            "VoucherNo"   => "required",
            "VoucherDate" => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $attributes = $request->all();
                $this->getConnectionDetails($request->CompanyID);
                unset($attributes['api_token']);
                DB::connection('mysql_source')
                    ->table('sales_line')
                    ->insert($attributes);
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    /*
    * To inseting the values to sales_line table
    * @params CompanyID, ChainID, LocationID, VoucherNo, VoucherDate, OrderNo
    */
    public function salesPayment(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'    => 'required',
            'ChainID'      => 'required',
            'LocationID'   => 'required',
            "VoucherNo"    => "required",
            "VoucherDate"  => "required",
            "OrderNo"      => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $attributes                 = $request->all();
                unset($attributes['api_token']);
                $attributes['CustomerName'] = ($request->CustomerName) ?? "";
                $attributes['CardNo']       = ($request->CardNo) ?? "";
                $this->getConnectionDetails($request->CompanyID);
                DB::connection('mysql_source')
                    ->table('sales_payment')
                    ->insert($attributes);
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function testApi(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'   => 'required',
            'ChainID'     => 'required',
            'LocationID'  => 'required',
            "VoucherNo"   => "required",
            "VoucherDate" => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                unset($request->all()['api_token']);
                SalesLine::create($request->all());
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function deletePayment(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'    => 'required',
            'ChainID'      => 'required',
            'LocationID'   => 'required',
            "VoucherNo"    => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::connection('mysql_source')
                    ->table('sales_payment')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->where('VoucherNo', $request->VoucherNo)
                    ->delete();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Deleted Successfully"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function updateSalesLine(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'    => 'required',
            'ChainID'      => 'required',
            'LocationID'   => 'required',
            "VoucherNo"    => "required",
            "ProductID"    => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::connection('mysql_source')
                    ->table('sales_line')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->where('VoucherNo', $request->VoucherNo)
                    ->where('ProductID', $request->ProductID)
                    ->update([ "SalesStatus" => "V"]);
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Updated Successfully"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function updateSalesHeader(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'    => 'required',
            'ChainID'      => 'required',
            'LocationID'   => 'required',
            "VoucherNo"    => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::connection('mysql_source')
                    ->table('sales_header')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->where('VoucherNo', $request->VoucherNo)
                    ->update([ "VoucherStatus" => "V"]);
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Updated Successfully"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function updateTransactionByDate(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'     => 'required',
            'ChainID'       => 'required',
            'LocationID'    => 'required',
            "VoucherDate"   => "required",
            "VoucherNo"     => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::beginTransaction();
                DB::connection('mysql_source')
                    ->table('sales_header')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->where('VoucherNo', $request->VoucherNo)
                    ->where('VoucherDate', $request->VoucherDate)
                    ->update(['VoucherStatus' => 'V']);
                DB::connection('mysql_source')
                    ->table('sales_line')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->where('VoucherNo', $request->VoucherNo)
                    ->where('VoucherDate', $request->VoucherDate)
                    ->update(['SalesStatus' => 'V']);
                DB::connection('mysql_source')
                    ->table('sales_payment')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->where('VoucherNo', $request->VoucherNo)
                    ->where('VoucherDate', $request->VoucherDate)
                    ->update(['amount' => 0, 'TenderAmount' => 0]);
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Updated Successfully"]);
            }catch(\Exception $e){
                DB::rollBack();
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function deleteTransactionByDate(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'     => 'required',
            'ChainID'       => 'required',
            'LocationID'    => 'required',
            "startDate"     => "required",
            "endDate"       => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::beginTransaction();
                DB::connection('mysql_source')
                    ->table('sales_header')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->whereBetween('VoucherDate', [[$request->startDate, $request->endDate]])
                    ->delete();
                DB::connection('mysql_source')
                    ->table('sales_line')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->whereBetween('VoucherDate', [[$request->startDate, $request->endDate]])
                    ->delete();
                DB::connection('mysql_source')
                    ->table('sales_payment')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->where('LocationID', $request->LocationID)
                    ->whereBetween('VoucherDate', [[$request->startDate, $request->endDate]])
                    ->delete();
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Deleted Successfully"]);
            }catch(\Exception $e){
                DB::rollBack();
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }
    }

    public function salesPaymentType(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'         => 'required',
            'ChainID'           => 'required',
            'PaymentType'       => 'required',
            "IsCreditCard"      => "required",
            "IsDiscountCard"    => "required",
            "DisplayOrder"      => "required",
            "IsCCard"           => "required",
            'IsStaffMeal'       => "required",
            'IsOtherStaffMeal'  => 'required',
            'Active'            => 'required',
            'IsSwipeCard'       => 'required',
            "MaxDiscount"       => "required",
            "IsCustCredit"      => "required",
            "IsCOTDiscount"     => "required",
            "MinDiscount"       => "required"
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $attributes = $request->all();
                $this->getConnectionDetails($request->CompanyID);
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsCreditCard'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsDiscountCard'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsCCard'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsStaffMeal'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsOtherStaffMeal'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['Active'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsSwipeCard'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsCustCredit'] ='1';
                if(($request->IsCreditCard =="true") || ($request->IsCreditCard =="True")) $attributes['IsCOTDiscount'] ='1';
                unset($attributes['api_token']);
                DB::connection('mysql_source')
                    ->table('payment_types')
                    ->insert($attributes);
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }

    }

    public function deletePaymentType(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'         => 'required',
            'ChainID'           => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::connection('mysql_source')
                    ->table('payment_types')
                    ->where('CompanyID', $request->CompanyID)
                    ->where('ChainID', $request->ChainID)
                    ->delete();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Deleted Successfully"]);
            }catch(\Exception $e){
                return response()->json(['status'=>false,"message"=> "Something went wrong.Please Try Again"]);
            }
        }

    }

    public function salesTransaction(Request $request){

        $validator  =   Validator::make($request->all(),[
            'CompanyID'                   => 'required',
            'ChainID'                     => 'required',
            'LocationID'                  => 'required',
            "VoucherNo"                   => "required",
            "VoucherDate"                 => "required",
            "OrderNo"                     => "required",
            "EmployeeID"                  => "required",
            "Discount"                    => "required",
            "SalesTaxAmount"              => "required",
            'ServiceTaxAmount'            => "required",
            'SalesTaxBaseAmount'          => 'required',
            'ServiceTaxBaseAmount'        => 'required',
            'NetSalesAfterDiscountAndTax' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $this->getConnectionDetails($request->CompanyID);
                DB::beginTransaction();
                $salesHeader = ['CompanyID'                         => $request->CompanyID,
                                'ChainID'                           => $request->ChainID,
                                'LocationID'                        => $request->LocationID,
                                'VoucherNo'                         => $request->VoucherNo,
                                'VoucherDate'                       => $request->VoucherDate,
                                'OrderNo'                           => $request->OrderNo,
                                'OrderType'                         => $request->OrderType,
                                'EmployeeID'                        => $request->EmployeeID,
                                'VoucherStatus'                     => $request->VoucherStatus,
                                'SalesAmount'                       => $request->SalesAmount,
                                'VoidCount'                         => $request->VoidCount,
                                'VoidAmount'                        => $request->VoidAmount,
                                'GuestCount'                        => $request->GuestCount,
                                'StartTime'                         => $request->StartTime,
                                'Discount'                          => $request->Discount,
                                'SalesTaxAmount'                    => $request->SalesTaxAmount,
                                'ServiceTaxAmount'                  => $request->ServiceTaxAmount,
                                'SalesTaxBaseAmount'                => $request->SalesTaxBaseAmount,
                                'ServiceTaxBaseAmount'              => $request->ServiceTaxBaseAmount,
                                'NetSalesAfterDiscountAndTax'       => $request->NetSalesAfterDiscountAndTax
                            ];
                DB::connection('mysql_source')
                    ->table('sales_header')
                    ->insert($salesHeader);

                $k = 0; $salesLines = [];
                foreach($request->ProductID as $row){
                    $salesLines[]  = [  "CompanyID"     => $request->CompanyID,
                                        "ChainID"       => $request->ChainID,
                                        "LocationID"    => $request->LocationID,
                                        "VoucherDate"   => $request->VoucherDate,
                                        "VoucherNo"     => $request->VoucherNo,
                                        "EmployeeID"    => $request->EmployeeID,
                                        "OrderNo"       => $request->OrderNo,
                                        "ProductID"     => $request->ProductID[$k],
                                        "ItemDesc"      => $request->ItemDesc[$k],
                                        "CategoryID1"   => $request->CategoryID1[$k],
                                        "CategoryDes1"  => $request->CategoryDes1[$k],
                                        "CategoryID2"   => $request->CategoryID2[$k],
                                        "CategoryDes2"  => $request->CategoryDes2[$k],
                                        "SellingPrice"  => $request->SellingPrice[$k],
                                        "Qty"           => $request->Qty[$k],
                                        "SalesStatus"   => $request->SalesStatus[$k]
                                    ];
                    $k++;
                }
                DB::connection('mysql_source')
                    ->table('sales_line')
                    ->insert($salesLines);

                $j = 0; $salesPayment = [];
                foreach($request->PaymentType as $row){
                    $salesPayment  = [ "CompanyID"      => $request->CompanyID,
                                       "ChainID"        => $request->ChainID,
                                       "LocationID"     => $request->LocationID,
                                       "EmployeeID"     => $request->EmployeeID,
                                       "OrderTakenBy"   => $request->OrderTakenBy[$j],
                                       "VoucherNo"      => $request->VoucherNo,
                                       "VoucherDate"    => $request->VoucherDate,
                                       "OrderNo"        => $request->OrderNo,
                                       "CustomerName"   => ($request->CustomerName[$j]) ?? "",
                                       "PaymentType"    => $request->PaymentType[$j],
                                       "Amount"         => $request->Amount[$j],
                                       "CardNo"         => ($request->CardNo[$j]) ?? "",
                                       "TenderAmount"   => $request->TenderAmount[$j]
                                    ];
                    $j++;
                }
                DB::connection('mysql_source')
                    ->table('sales_payment')
                    ->insert($salesPayment);
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                DB::rollBack();
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);
            }
        }

    }

    public function salesAllTransactions(Request $request){

        $validator  =   Validator::make($request->all(),[
            'data'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $dataArray = json_decode($request->data, true);
                $this->getConnectionDetails((string)$dataArray['sales']['CompanyID']);
                DB::beginTransaction();
                $salesHeader =  [   'CompanyID'                     => (string)$dataArray['sales']['CompanyID'],
                                    'ChainID'                       => (string)$dataArray['sales']['ChainID'],
                                    'LocationID'                    => (string)$dataArray['sales']['LocationID'],
                                    'VoucherNo'                     => $dataArray['sales']['VoucherNo'],
                                    'VoucherDate'                   => (string)$dataArray['sales']['VoucherDate'],
                                    'OrderNo'                       => $dataArray['sales']['OrderNo'],
                                    'OrderType'                     => (string)$dataArray['sales']['OrderType'],
                                    'EmployeeID'                    => $dataArray['sales']['EmployeeID'],
                                    'VoucherStatus'                 => (string)$dataArray['sales']['VoucherStatus'],
                                    'SalesAmount'                   => $dataArray['sales']['SalesAmount'],
                                    'VoidCount'                     => $dataArray['sales']['VoidCount'],
                                    'VoidAmount'                    => $dataArray['sales']['VoidAmount'],
                                    'GuestCount'                    => $dataArray['sales']['GuestCount'],
                                    'StartTime'                     => (string)$dataArray['sales']['StartTime'],
                                    'Discount'                      => $dataArray['sales']['Discount'],
                                    'SalesTaxAmount'                => $dataArray['sales']['SalesTaxAmount'],
                                    'ServiceTaxAmount'              => $dataArray['sales']['ServiceTaxAmount'],
                                    'SalesTaxBaseAmount'            => $dataArray['sales']['SalesTaxBaseAmount'],
                                    'ServiceTaxBaseAmount'          => $dataArray['sales']['ServiceTaxBaseAmount'],
                                    'NetSalesAfterDiscountAndTax'   => $dataArray['sales']['NetSalesAfterDiscountAndTax']
                                ];
                DB::connection('mysql_source')
                        ->table('sales_header')
                        ->insert($salesHeader);
                $k = 0; $salesLines = [];
                foreach($dataArray['sales']['sales_lines'] as $row){
                    $salesLines[]  = [  "CompanyID"     => (string)$dataArray['sales']['CompanyID'],
                                        "ChainID"       => (string)$dataArray['sales']['ChainID'],
                                        "LocationID"    => (string)$dataArray['sales']['LocationID'],
                                        "VoucherDate"   => (string)$dataArray['sales']['VoucherDate'],
                                        "VoucherNo"     => $dataArray['sales']['VoucherNo'],
                                        "EmployeeID"    => $dataArray['sales']['EmployeeID'],
                                        "OrderNo"       => $dataArray['sales']['OrderNo'],
                                        "ProductID"     => $row['ProductID'],
                                        "ItemDesc"      => $row['ItemDesc'],
                                        "CategoryID1"   => $row['CategoryID1'],
                                        "CategoryDes1"  => $row['CategoryDes1'],
                                        "CategoryID2"   => $row['CategoryID2'],
                                        "CategoryDes2"  => $row['CategoryDes2'],
                                        "SellingPrice"  => $row['SellingPrice'],
                                        "Qty"           => $row['Qty'],
                                        "SalesStatus"   => $row['SalesStatus']
                                    ];
                    $k++;
                }
                DB::connection('mysql_source')
                    ->table('sales_line')
                    ->insert($salesLines);

                $j = 0; $salesPayment = [];
                foreach($dataArray['sales']['sales_payments'] as $row){
                    $salesPayment[] = [ "CompanyID"     => (string)$dataArray['sales']['CompanyID'],
                                       "ChainID"        => (string)$dataArray['sales']['ChainID'],
                                       "LocationID"     => (string)$dataArray['sales']['LocationID'],
                                       "EmployeeID"     => $dataArray['sales']['EmployeeID'],
                                       "OrderTakenBy"   => $row['OrderTakenBy'],
                                       "VoucherNo"      => $dataArray['sales']['VoucherNo'],
                                       "VoucherDate"    => (string)$dataArray['sales']['VoucherDate'],
                                       "OrderNo"        => $dataArray['sales']['OrderNo'],
                                       "CustomerName"   => ($row['CustomerName']) ?? "",
                                       "PaymentType"    => $row['PaymentType'],
                                       "Amount"         => $row['Amount'],
                                       "CardNo"         => ($row['CardNo']) ?? "",
                                       "TenderAmount"   => $row['TenderAmount']
                                    ];
                    $j++;
                }
                DB::connection('mysql_source')
                    ->table('sales_payment')
                    ->insert($salesPayment);
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                DB::rollBack();
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);
            }
        }
    }

    public function salesPaymentTypes(Request $request){

        $validator  =   Validator::make($request->all(),[
            'data'  => 'required'
        ]);
        if($validator->fails()) {
            return response()->json([ 'status' => 'error', 'message' => $validator->errors()->first() ]);
        }else{
            try{
                $dataArray = json_decode($request->data, true);
                $this->getConnectionDetails((string)$dataArray['payment']['CompanyID']);
                DB::beginTransaction();
                $k = 0; $payemntTypes = [];
                foreach($dataArray['payment']['ModeofPayment'] as $row){

                    if(($row['IsCreditCard'] =="true") || ($row['IsCreditCard'] =="True")) $row['IsCreditCard'] ='1';
                    if(($row['IsDiscountCard'] =="true") || ($row['IsDiscountCard'] =="True")) $row['IsDiscountCard'] ='1';
                    if(($row['DisplayOrder'] =="true") || ($row['DisplayOrder'] =="True")) $row['DisplayOrder'] ='1';
                    if(($row['IsCCard'] =="true") || ($row['IsCCard'] =="True")) $row['IsCCard'] ='1';
                    if(($row['IsStaffMeal'] =="true") || ($row['IsStaffMeal'] =="True")) $row['IsStaffMeal'] ='1';
                    if(($row['IsOtherStaffMeal'] =="true") || ($row['IsOtherStaffMeal'] =="True")) $row['IsOtherStaffMeal'] ='1';
                    if(($row['IsSwipeCard'] =="true") || ($row['IsSwipeCard'] =="True")) $row['IsSwipeCard'] ='1';
                    if(($row['IsCustCredit'] =="true") || ($row['IsCustCredit'] =="True")) $row['IsCustCredit'] ='1';
                    if(($row['IsCOTDiscount'] =="true") || ($row['IsCOTDiscount'] =="True")) $row['IsCOTDiscount'] ='1';
                    $payemntTypes[]  = [    "CompanyID"          => (string)$dataArray['payment']['CompanyID'],
                                            "ChainID"            => (string)$dataArray['payment']['ChainID'],
                                            "PaymentType"        => $row['PaymentType'],
                                            "IsCreditCard"       => $row['IsCreditCard'],
                                            "LoginGroupID"       => $row['LoginGroupID'],
                                            "IsDiscountCard"     => $row['IsDiscountCard'],
                                            "DisplayOrder"       => $row['DisplayOrder'],
                                            "IsCCard"            => $row['IsCCard'],
                                            "IsStaffMeal"        => $row['IsStaffMeal'],
                                            "IsOtherStaffMeal"   => $row['IsOtherStaffMeal'],
                                            "Active"             => $row['Active'],
                                            "OrderTypes"         => $row['OrderTypes'],
                                            "IsSwipeCard"        => $row['IsSwipeCard'],
                                            "MaxDiscount"        => $row['MaxDiscount'],
                                            "IsCustCredit"       => $row['IsCustCredit'],
                                            "IsCOTDiscount"      => $row['IsCOTDiscount'],
                                            "MinDiscount"         => $row['MinDiscount']
                                    ];
                    $k++;
                }
                DB::connection('mysql_source')
                    ->table('payment_types')
                    ->insert($payemntTypes);
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                DB::rollBack();
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);
            }
        }
    }

    public function getConnectionDetails($companyID){

        // Artisan::call('config:cache');
        $getConnectionDetails = Company::select('conhostname', 'condbname', 'conusername', 'conpassword')
                                         ->where('CompanyID', $companyID)
                                         ->first();
        if($getConnectionDetails->conhostname != '' && $getConnectionDetails->condbname != '' && $getConnectionDetails->conusername != ''){
            config(['database.connections.mysql_source.host' => $getConnectionDetails->conhostname]);
            config(['database.connections.mysql_source.database' => $getConnectionDetails->condbname]);
            config(['database.connections.mysql_source.username' => $getConnectionDetails->conusername]);
            config(['database.connections.mysql_source.password' => $getConnectionDetails->conpassword]);

        }else{
            return response()->json(['status'=>false,"message"=> $e->getMessage()]);
        }
    }
}
