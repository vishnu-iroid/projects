<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
// use App\Http\Controllers\str;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Auth\Authenticatable;
// use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File; 
use DB;
use Session;
use Auth;

use App\Company;
use App\User2;
use App\LocalUsersCompanyAccess;

class UserController extends Controller
{   


   public function secuserLogin(Request $request){
        // print_r($request->all());
        $validator= Validator::make($request->all(), [
            'email'=>'required|email',
            'password'=>'required',
        ]);
        if($validator->fails()){
            return response()->json(["message"=>'wrong credentials']);
        }
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
                $user = Auth::user();
                if($user->isAdmin == 1){
                    $userRole = "SuperAdmin";
                }
                else{
                    if($user->status == 0){
                        return response()->json(['status'=>false,'error'=>'Inactive user']);  
                    }
                    $userRole= "User";
                    $companyDetails = DB::table('usercompanyaccess')->where('UserID',$user->user_id)->select('id','CompanyID','CompanyName')->get();
                    if(count($companyDetails) == 1){
                        try{
                            DB::beginTransaction();
                            DB::table('user')->where('user_id',$user->user_id)->update(['companyid'=>$companyDetails[0]->CompanyID]);
                            DB::Commit();
                        }catch(\Exception $e){
                            DB::rollback();
                            return response()->json(['status'=>false,"error"=>"Something went wrong."]);
                        }
                        Session::flush();
                        Session::put('companyid', $companyDetails[0]->CompanyID);
                        Session::put('companyname', $companyDetails[0]->CompanyName);
                    }elseif(count($companyDetails) == 0){
                        return response()->json(['status'=>false,'error'=> "User must have atleast one company assigned."]);  
                    }
                    $success['companyDetails'] = $companyDetails;
                }
                $success['userdetails'] = $user;
                $success['token'] = $user->createToken('MyApp')->accessToken;
                $superAdmin=DB::table('user')->select('email')->where('isAdmin','1')->first();
                return response()->json(["message"=>"login success","UserRole"=>$userRole,"data"=>$success,'superadmin'=>$superAdmin->email]);
        }
        else{
            return response()->json(['status'=>false,"error"=>"Invalid username or password"]);
        }

    }
    
    public function secuserLogout(Request $request){
        // auth()->user()->token()->revoke();
        // Auth::logout();
        if (Auth::check()) {
            Session::flush();
            Auth::user()->AauthAcessToken()->revoke();
        }   
        return response()->json(['status'=>true,'message'=>"logout success"]);
    }
    
    // ---------------------------------------------------------------------
        // company selection on login 

    public function getCompany(Request $request){
        if($request->companyid){
            $data = Company::where('CompanyID',$request->companyid)
                            ->select('CompanyID','CompanyName','CompanyCode')
                            ->first();
            if($data && $data->CompanyID && $data->CompanyName){
                try{
                    DB::beginTransaction();
                    DB::table('user')->where('user_id',$request->user_id)->update(['companyid'=>$request->companyid]);
                    DB::commit();
                    return response()->json(['status'=>true,'comapny_id'=>"Company updated"]);
                }catch(\Exception $e){
                    DB::rollback();
                    return response()->json(['status'=>true,'message'=>$e->getMessage()]);
                }               
            }else{
                return response()->json(['status'=>false,'message'=>"Something went wrong."]); 
            }
        }else{
            return response()->json(['status'=>false,'message'=>"Something went wrong."]);
        }
    } 

    // -------------------------------------------------------------------
    // company db connection
    public function getCompanyConnection($companyID){
        $getCompanyConnection = Company::select('conhostname', 'condbname', 'conusername', 'conpassword')
                                        ->where('CompanyID',$companyID)->first();
        
        if($getCompanyConnection->conhostname != '' && $getCompanyConnection->condbname != '' && $getCompanyConnection->conusername != '' ){
            config(['database.connections.mysql_source.host'     => $getCompanyConnection->conhostname]);
            config(['database.connections.mysql_source.database' => $getCompanyConnection->condbname]);
            config(['database.connections.mysql_source.username' => $getCompanyConnection->conusername]);
            config(['database.connections.mysql_source.password' => $getCompanyConnection->conpassword]);
        }else{
            // return response()->json(['status'=>false,"message"=> $e->getMessage()]);
             return response()->json(['status'=>false,"message"=> "Failed to establish company database connection."]);
        }
    }

    // -------------------------------------------------------------------
    
    public function viewChains(Request $request){
        $company    =   Auth::user()->companyid;
        $search=$request->search;
        $this->getCompanyConnection($company);
        $chain_data=DB::connection('mysql_source')
                        ->table('chain')->select("*")
                        ->where('CompanyId',$company)
                        ->where(function($chain_data) use ($search){
                                $chain_data->orWhere('ChainName', 'like', '%'.$search.'%')
                                        ->orWhere('Email', 'like', '%'.$search.'%');
                        })->orderBy('created_at','desc')->paginate(10);
        DB::disconnect('mysql_source');
        if($chain_data){
            return response()->json(['chain_data'=>$chain_data]);
        }
        else{
            return response()->json(["message"=>"No Data"]);
        }
            
    }

    public function addChain(Request $request){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $rules = [
            'chainid' => 'required|unique:mysql_source.chain,Chainid',
            'description'=>'required',
            'email'=>'required|email|unique:mysql_source.chain,Email',
            'isvirtual'=>'nullable',
            'cgaontransac'=>'nullable',
            'cgaonguestcount'=>'nullable',
            
        ];
        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'message' => $validation->errors()->first()]);
        }
        $image=$request->image;
        if($request->image){
               define('UPLOAD_DIR', 'public/images/chains/');
               $img = $request->image;
               preg_match("/data:image\/(.*?);/",$img,$image_extension); // extract the image extension
               $img = preg_replace('/data:image\/(.*?);base64,/','',$img);
               $img = str_replace(' ', '+', $img);
               $data = base64_decode($img);
               $file = UPLOAD_DIR . str::random(10). '.' . $image_extension[1];
               $imgname =   "http://businessapp.axistc.com/axis/".$file;
               $success = file_put_contents($file, $data);
               $data    =   [
                        'ChainId'           =>$request->chainid,
                        'ChainName'         =>$request->description,
                        'Email'             =>$request->email,
                        'isVirtualChain'    =>$request->isvirtual,
                        'cgaontransactions' =>$request->cgaontransac,
                        'gcaonguestcount'   =>$request->gcaonguestcount,
    		            'CompanyId'		    =>$company,
                        'created_at' 	    => date('Y-m-d H:i:s'),
                        'updated_at' 	    => date('Y-m-d H:i:s'),
                        'Image'             =>  $imgname,
               ];            
        }else{
            $data   =   [
                    'ChainId'           =>$request->chainid,
                    'ChainName'         =>$request->description,
                    'Email'             =>$request->email,
                    'isVirtualChain'    =>$request->isvirtual,
                    'cgaontransactions' =>$request->cgaontransac,
                    'gcaonguestcount'   =>$request->gcaonguestcount,
		            'CompanyId'		    =>$company,
                    'created_at' 	    => date('Y-m-d H:i:s'),
                    'updated_at' 	    => date('Y-m-d H:i:s'),
            ];
        }
        try{
                DB::beginTransaction();
                DB::connection('mysql_source')->table('chain')->insert($data);  
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>'Successfully added']);
        }catch(\Exception $e){
                DB::rollback();
                // throw $e;
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);  
        }
    }
    
    public function deleteChain(Request $request){
        $id=$request->chainid;
        $company    =   Auth::user()->companyid;
        $data = DB::table('chain')->where('ChainId',$id)->first();
        if($data && $data->Image){
            $name   =   basename($data->Image);
            if(File::exists(public_path('images/chains/').$name)){
                File::delete(public_path('images/chains/').$name);
            }
        }
        try{
            DB::beginTransaction();
            $this->getCompanyConnection($company);
            DB::connection('mysql_source')->table('chain')->where('ChainId','=',$id)->delete();   
            DB::connection('mysql_source')->table('chainaccess')->where('ChainID','=',$id)->delete();   
            DB::connection('mysql_source')->table('location')->where('ChainId','=',$id)->delete();  
            DB::connection('mysql_source')->table('locationaccess')->where('ChainId','=',$id)->delete();
            DB::commit();
            DB::disconnect('mysql_source');
            return response()->json(['status'=>true,"message"=>'Successfully Deleted']);
        }catch(\Exception $e){
            DB::rollback();
             return response()->json(['status'=>false,"message"=> $e->getMessage()]);  
        }
        
    }

    public function updateChain(Request $request){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $old_chainid = DB::connection('mysql_source')->table('chain')->where('id',$request->id)->select('ChainID')->first();
        $rules = [
            'chainid'       => 'required|unique:mysql_source.chain,Chainid,'.$request->id,
            'description'   => 'required',
            'email'         => 'required|email|unique:mysql_source.chain,Email,'.$request->id,
            'isvirtual'     => 'nullable',
            'cgaontransac'  => 'nullable',
            'cgaonguestcount'=>'nullable',
            
        ];
        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return response()->json(['status' => false, 'message' => $validation->errors()->first()]);
        }
        $id=$request->id;
        // $id=$request->chainid;
        $image=$request->image;
        $updateData   =   [
                    'ChainId'           =>$request->chainid,
                    'ChainName'         =>$request->description,
                    'Email'             =>$request->email,
                    'isVirtualChain'    =>$request->isvirtual,
                    'cgaontransactions' =>$request->cgaontransac,
                    'gcaonguestcount'   =>$request->gcaonguestcount,
                    'updated_at'        => date('Y-m-d H:i:s'),
        ];
        if($request->image){
                $data = DB::connection('mysql_source')->table('chain')->where('id',$id)->first();
                $name   =   basename($data->Image);
                if(file_exists(public_path('images/chains/').$name)){
                    File::delete(public_path('images/chains/').$name);
                }
               define('UPLOAD_DIR', 'public/images/chains/');
               $img = $request->image;
               preg_match("/data:image\/(.*?);/",$img,$image_extension); // extract the image extension
               $img = preg_replace('/data:image\/(.*?);base64,/','',$img);
               $img = str_replace(' ', '+', $img);
               $data = base64_decode($img);
               $file = UPLOAD_DIR . str::random(10). '.' . $image_extension[1];
               $imgname =   "http://businessapp.axistc.com/axis/".$file;
               $success = file_put_contents($file, $data);
               $updateData['Image']  = $imgname;
        }
        try{
                DB::beginTransaction();
                DB::connection('mysql_source')->table('chain')->where('id',$id)->update($updateData); 
                DB::connection('mysql_source')->table('chainaccess')->where('ChainID',$old_chainid->ChainID)->update([
                    'ChainID' => $request->chainid,
                ]);
                DB::connection('mysql_source')->table('location')->where('ChainId',$old_chainid->ChainID)->update([
                    'ChainId' => $request->chainid,
                ]);
                DB::connection('mysql_source')->table('locationaccess')->where('ChainId',$old_chainid->ChainID)->update([
                    'ChainId' => $request->chainid,
                ]);
                DB::connection('mysql_source')->table('payment_types')->where('ChainID',$old_chainid->ChainID)->update([
                    "ChainID" => $request->chainid,
                ]);
                DB::connection('mysql_source')->table('sales_header')->where('ChainID',$old_chainid->ChainID)->update([
                    "ChainID" => $request->chainid,
                ]);
                DB::connection('mysql_source')->table('sales_line')->where('ChainID',$old_chainid->ChainID)->update([
                    "ChainID" => $request->chainid,
                ]);
                DB::connection('mysql_source')->table('sales_payment')->where('ChainID',$old_chainid->ChainID)->update([
                    "ChainID" => $request->chainid,
                ]);
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>'Successfully Updated']);
        }catch(\Exception $e){
                DB::rollback();
                // throw $e;
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);  
        }
    }

    public function viewLocations(Request $request){
        $company    =   Auth::user()->companyid;
         $search=$request->search;
         $this->getCompanyConnection($company);
         $loc_data=DB::connection('mysql_source')
                    ->table('location')
                    ->join('chain','chain.ChainId','=','location.ChainId')
                    ->select("location.*","chain.ChainName")
                    ->where('location.CompanyId',$company)
                    ->where(function($loc_data) use ($search){
                            $loc_data->orWhere('LocationName', 'like', '%'.$search.'%')
                                    ->orWhere('LocationId', 'like', '%'.$search.'%')
                                    ->orWhere('chain.ChainId', 'like', '%'.$search.'%');
                    })->orderBy('created_at','desc')->paginate(10);
        DB::disconnect('mysql_source');
        if($loc_data){
             return response()->json(['location_data'=>$loc_data]);
        }
        else{
             return response()->json(["message"=>"No Data"]);
        }   
    }

    public function addLocation(Request $request){
        $id     =   $request->chainid;
        $date   =   $request->startdate;
        $strdate        =date('Y-m-d',strtotime($date));
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $rules=[
            'chainid'=>'required',
            'locationid'=>'required|unique:mysql_source.location,LocationId',
            'Locationname'=>'required',
            'Contact'=>'nullable',
            'address1'=>'nullable',
            'address2'=>'nullable',
            'city'=>'nullable',
            'country'=>'nullable',
            'phone'=>'nullable|unique:mysql_source.location,Phone',
            'fax'=>'nullable',
            'email'=>'nullable|email|unique:mysql_source.location,Email',
            'notes'=>'nullable',
            'costcenter'=>'nullable',
            'locationtype'=>'nullable',
            'inventory'=>'required',
            'startdate'=>'nullable',
            'service'=>'nullable',
            'costcentertype'=>'nullable',
            'seats'=>'required',
            'area'=>'nullable',
            'active'=>'required',
            'planned'=>'required',
            'enablecutoff'=>'required',
            'ordercutof'=>'nullable',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $locdata=array(
            'ChainId'       =>$request->chainid,
            'LocationId'    =>$request->locationid,
            'LocationName'  =>$request->Locationname,
            'Contact'       =>$request->Contact,
            'Address1'      =>$request->address1,
            'Address2'      =>$request->address2,
            'City'          =>$request->city,
            'Country'       =>$request->country,
            'Phone'         =>$request->phone,
            'Fax'           =>$request->fax,
            'Email'         =>$request->email,
            'Notes'         =>$request->notes,
            'CostCenter'    =>$request->costcenter,
            'LocationType'  =>$request->locationtype,
            'ManageStock'   =>$request->inventory,
            'StartDate'     =>$strdate,
            'Service'       =>$request->service,
            'CostCenterType'=>$request->costcentertype,
            'Seats'         =>$request->seats,
            'Area'          =>$request->area,
            'Active'        =>$request->active,
            'Planned'       =>$request->planned,
            'EnableOrderCutoff'=>$request->enablecutoff,
            'OrderCutoff'   =>$request->ordercutof,
            'CompanyId'     =>$company,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        DB::beginTransaction();
        try{
            DB::connection('mysql_source')->table('location')->insert([$locdata]);
            $chainaccess=DB::connection('mysql_source')
                            ->table('chainaccess')->where([
                                ['CompanyId',$company],
                                ['ChainID',$request->chainid],
                                ['FullAccess',1],
                            ])->select('UserID')->get();
            if(count($chainaccess)>0){
                foreach($chainaccess as $chainloc){
                    DB::connection('mysql_source')
                        ->table('locationaccess')->insert([
                            'CompanyID' =>$company,
                            'UserId'    =>$chainloc->UserID,
                            'ChainId'   =>$request->chainid,
                            'LocationId'=>$request->locationid,
                        ]);
                }
            }
            DB::commit();
            DB::disconnect('mysql_source');
            return response()->json(['status'=>true,"message"=>'Successfully Added']);
        }catch(\Exception $e){
            DB::rollback();
            // throw $e;
             return response()->json(['status'=>false,"message"=> $e->getMessage()]);  
        }
    }
    public function deleteLocation(Request $request){
            $company    =   Auth::user()->companyid;
            try{
                DB::beginTransaction();
                $this->getCompanyConnection($company);
                DB::connection('mysql_source')->table('location')->where('LocationId',$request->locationid)->delete();
                DB::connection('mysql_source')->table('locationaccess')->where('LocationId',$request->locationid)->delete();
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>'Successfully Deleted']);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=>false,"message"=> $e->getMessage()]);  
            }
    }
    public function updateLocation(Request $request){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $old_locationid = DB::connection('mysql_source')->table('location')->where('id',$request->id)->select('LocationId')->first();
        $rules=[
            'chainid'=>'required',
            'locationid'=>'required|unique:location,LocationId,'.$request->id,
            'Locationname'=>'required',
            'Contact'=>'nullable',
            'address1'=>'nullable',
            'address2'=>'nullable',
            'city'=>'nullable',
            'country'=>'nullable',
            'phone'=>'nullable|unique:location,Phone,'.$request->id,
            'fax'=>'nullable',
            'email'=>'nullable|email|unique:location,Email,'.$request->id,
            'notes'=>'nullable',
            'costcenter'=>'nullable',
            'locationtype'=>'nullable',
            'inventory'=>'required',
            'startdate'=>'nullable',
            'service'=>'nullable',
            'costcentertype'=>'nullable',
            'seats'=>'required',
            'area'=>'nullable',
            'active'=>'nullable',
            'planned'=>'nullable',
            'enablecutoff'=>'nullable',
            'ordercutof'=>'nullable',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $date=$request->startdate;
        $strdate=date('Y-m-d',strtotime($date));
        if($request->locationid !=""){
            if($request->enablecutoff == 0){
                try{
                    DB::beginTransaction();
                    $this->getCompanyConnection($company);
                    DB::connection('mysql_source')->table('location')->where('id',$request->id)->update([
                        'chainid'       =>$request->chainid,
                        'LocationId'    =>$request->locationid,
                        'LocationName'  =>$request->Locationname,
                        'Contact'       =>$request->Contact,
                        'Address1'      =>$request->address1,
                        'Address2'      =>$request->address2,
                        'City'          =>$request->city,
                        'Country'       =>$request->country,
                        'Phone'         =>$request->phone,
                        'Fax'           =>$request->fax,
                        'Email'         =>$request->email,
                        'Notes'         =>$request->notes,
                        'CostCenter'    =>$request->costcenter,
                        'LocationType'  =>$request->locationtype,
                        'ManageStock'   =>$request->inventory,
                        'StartDate'     =>$strdate,
                        'Service'       =>$request->service,
                        'CostCenterType'=>$request->costcentertype,
                        'Seats'         =>$request->seats,
                        'Area'          =>$request->area,
                        'Active'        =>$request->active,
                        'Planned'       =>$request->planned,
                        'EnableOrderCutoff'=>$request->enablecutoff,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    DB::connection('mysql_source')->table('locationaccess')->where('LocationId',$old_locationid->LocationId)->update(['LocationId'=>$request->locationid]);
                    DB::connection('mysql_source')->table('sales_header')->where('LocationID',$old_locationid->LocationId)->update([
                        "LocationID"=>$request->locationid,
                    ]);
                    DB::connection('mysql_source')->table('sales_line')->where('LocationID',$old_locationid->LocationId)->update([
                        "LocationID"=>$request->locationid,
                    ]);
                    DB::connection('mysql_source')->table('sales_payment')->where('LocationID',$old_locationid->LocationId)->update([
                        "LocationID"=>$request->locationid,
                    ]);
                    Db::commit();
                    DB::disconnect('mysql_source');
                    return response()->json(['status'=>true,"message"=>"successfully Updated"]); 
                }catch(\Exception $e){
                    DB::rollback();
                    // throw $e;
                    return response()->json(['status'=>false,"message"=> $e->getMessage()]);  
                }
            }
            else{
                    try{
                        DB::beginTransaction();
                        $this->getCompanyConnection($company);
                        DB::connection('mysql_source')->table('location')->where('id',$request->id)->update([
                             'chainid'       =>$request->chainid,
                            'LocationId'    =>$request->locationid,
                            'LocationName'  =>$request->Locationname,
                            'Contact'       =>$request->Contact,
                            'Address1'      =>$request->address1,
                            'Address2'      =>$request->address2,
                            'City'          =>$request->city,
                            'Country'       =>$request->country,
                            'Phone'         =>$request->phone,
                            'Fax'           =>$request->fax,
                            'Email'         =>$request->email,
                            'Notes'         =>$request->notes,
                            'CostCenter'    =>$request->costcenter,
                            'LocationType'  =>$request->locationtype,
                            'ManageStock'   =>$request->inventory,
                            'StartDate'     =>$strdate,
                            'Service'       =>$request->service,
                            'CostCenterType'=>$request->costcentertype,
                            'Seats'         =>$request->seats,
                            'Area'          =>$request->area,
                            'Active'        =>$request->active,
                            'Planned'       =>$request->planned,
                            'EnableOrderCutoff'=>$request->enablecutoff,
                            'orderCutoff'   =>$request->ordercutof, 
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                        DB::connection('mysql_source')->table('locationaccess')->where('LocationId',$old_locationid->LocationId)->update(['LocationId'=>$request->locationid]);
                        DB::connection('mysql_source')->table('sales_header')->where('LocationID',$old_locationid->LocationId)->update([
                            "LocationID"=>$request->locationid,
                        ]);
                        DB::connection('mysql_source')->table('sales_line')->where('LocationID',$old_locationid->LocationId)->update([
                            "LocationID"=>$request->locationid,
                        ]);
                        DB::connection('mysql_source')->table('sales_payment')->where('LocationID',$old_locationid->LocationId)->update([
                            "LocationID"=>$request->locationid,
                        ]);
                        DB::commit();
                        DB::disconnect('mysql_source');
                        return response()->json(['status'=>true,"message"=>"successfully Updated"]); 
                    }catch(\Exception $e){
                    DB::rollback();
                    // throw $e;
                    return response()->json(['status'=>false,"message"=>$e->getMessage()]);  
                }      
            }
        }
    else{
           return response()->json(['status'=>false,"message"=>"Something Went Wrong..! Please Try Again"]);  
        }    
    }

    public function viewLocalUsers(Request $request){
        $search=$request->search;
        $company    =   Auth::user()->companyid;
        // $this->getCompanyConnection($company);
        $user_data=DB::table('local_users')->select("*")
                    // ->where('CompanyID',$company)
                    ->where(function($user_data) use ($search){
                            $user_data->orWhere('user_name', 'like', '%'.$search.'%')
                                    ->orWhere('user_email', 'like', '%'.$search.'%')
                                    ->orWhere('CompanyID', 'like', '%'.$search.'%');
                    })->orderBy('created_at','desc')->paginate(10);
        foreach($user_data as $row){
            $companyID = array();$companyName = array();$company_details=array();
            $companies = LocalUsersCompanyAccess::select('companyid')->where('userid',$row->user_id)->get();
            $i=0;
            foreach($companies as $value){
                $company = Company::select('CompanyID','CompanyName')->where('CompanyID',$value->companyid)->first();
                if($company){
                    $companyID[] = $company->CompanyID;
                    $companyName[] = $company->CompanyName;
                    $company_details[$i]['CompanyID'] = $company->CompanyID;
                    $company_details[$i]['CompanyName'] = $company->CompanyName;
                }
                $i=$i+1;
            }
            $row->CompanyID = $companyID;
            $row->companyName = $companyName;
            $row->company = $company_details;
            unset($company_details);unset($companyID);unset($companyName);
        }
        // DB::disconnect('mysql_source');
        if($user_data){
             return response()->json(['location_data'=>$user_data]);
        }
        else{
             return response()->json(["message"=>"No Data"]);
        }
    }

    public function addLocalUser(Request $request){
        // return response()->json(['status'=>true,"message"=>$request->all()]); 
        $rules=[
           'user_id'   =>'required|unique:local_users,user_id',
           'username'  =>'required|unique:local_users,user_name',
           'firstname' =>'required',
           'lastname'  =>'required',
           'user_phone'=>'required|unique:local_users,user_phone',
           'user_email'=>'required|email|unique:local_users,user_email',
           "companyid" => "required|array|min:1",
           "companyid.*" => "required|string|distinct|min:1",
           'password'  =>'required',
           'gender'    =>'required',
           'dob'       =>'required',
           'image'     =>'required',
       ];
       $validator = Validator::make($request->all(), $rules);
       if ($validator->fails()) {
           return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
       }
       define('UPLOAD_DIR', 'public/images/local_users/');
       $img = $request->image;
       preg_match("/data:image\/(.*?);/",$img,$image_extension); // extract the image extension
       $img = preg_replace('/data:image\/(.*?);base64,/','',$img);
       $img = str_replace(' ', '+', $img);
       $data = base64_decode($img);
       $file = UPLOAD_DIR . str::random(10). '.' . $image_extension[1];
       $imgname =   "http://businessapp.axistc.com/axis/".$file;
       $success = file_put_contents($file, $data);
       $company    =   Auth::user()->companyid;
    //    $loginpin   =   $this->generateLoginPin();
       try{
            DB::beginTransaction();
            $data=DB::table('local_users')->insert([
               'user_id'    =>  $request->user_id,
               'username'   =>  $request->username,
               'user_phone' =>  $request->user_phone,
               'firstname'  =>  $request->firstname,
               'lastname'   =>  $request->lastname,
               'user_email' =>  $request->user_email,
               'password'   =>  $request->password,
               'image'      =>  $imgname,
               'gender'     =>  $request->gender,
               'dob'        =>  $request->dob,
               // 'UDID'    =>$request->udid,
               // 'LoginPin'=>  $loginpin,
               "CompanyID"  =>  Auth::user()->companyid,
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date('Y-m-d H:i:s'),
            ]);
            foreach($request->companyid as $company){
                LocalUsersCompanyAccess::insert([
                    'userid'    => $request->user_id,
                    'companyid' => $company,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
            // DB::disconnect('mysql_source');
            return response()->json(['status'=>true,"message"=>"successfully Added"]);   
       }catch(\Exception $e){
        //    throw $e;
           DB::rollback();
           return response()->json(['status'=>false,"message"=>$e->getMessage()]);  
       }
   }
   
   public function generateLoginPin(){
       $loginpin   =   mt_rand(100000,999999);
       if(DB::table('local_users')->where('LoginPin',$loginpin)->exists()){
           return generateLoginPin();
       }
       return $loginpin;
   }
    
    public function deleteLocalUser(Request $request){
        $id     =   $request->id;
        $company    =   Auth::user()->companyid;
        $data   =   DB::table('local_users')->where('id',$id)->first();
        $name   =   basename($data->image);
        if(file_exists(public_path('images/local_users/').$name)){
            File::delete(public_path('images/local_users/').$name);
        }
        if($request->id){
            try{
                DB::beginTransaction();
                DB::table('local_users')->where('id',$id)->delete();
                DB::table('local_users_company_accesses')->where('UserID',$data->user_id)->delete();
                $this->getCompanyConnection($company);
                DB::connection('mysql_source')->table('locationaccess')->where('UserId',$data->user_id)->delete();
                DB::connection('mysql_source')->table('chainaccess')->where('UserID',$data->user_id)->delete();
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>1,"message"=>"successfully deleted"]);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=>0,"message"=>"Something Went Wrong"]);
            }
        }
    }
    public function updateLocalUser(Request $request){
        $id=$request->id;
        $old_data=DB::table('local_users')->where('id',$id)->first();
        $company    =   Auth::user()->companyid;
        $rules=[
            'user_id'    =>'required|unique:local_users,user_id,'.$request->id,
            'firstname' =>'required',
            'lastname'  =>'required',
            'username'  =>'required|unique:local_users,user_name,'.$request->id,
            'user_phone'    =>'required|unique:local_users,user_phone,'.$request->id,
            'user_email'     =>'required|email|unique:local_users,user_email,'.$request->id,
            "companyid"   => "required|array|min:1",
            "companyid.*" => "required|string|distinct|min:1",
            // 'loginpin'  =>'nullable|numeric',
            // 'udid'      =>'required',
            'image'     =>'nullable',
            'password'  =>'nullable',
            'gender'    =>'required',
            'dob'       =>'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $update_data   =   [
            'user_id'    =>  $request->user_id,
            'username'   =>  $request->username,
            'user_phone' =>  $request->user_phone,
            'firstname'  =>  $request->firstname,
            'lastname'   =>  $request->lastname,
            'user_email' =>  $request->user_email,
            'gender'     =>  $request->gender,
            'dob'        =>  $request->dob,
            // 'UDID'    =>$request->udid,
            // 'LoginPin'=>  $loginpin,
            "CompanyID"  =>  Auth::user()->companyid,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        if($request->password != ""){
            $update_data['password']   =  bcrypt($request->password);
        }
        if($request->image !=""){
            $name   =   basename($old_data->image);
            if(file_exists(public_path('images/local_users/').$name)){
                File::delete(public_path('images/local_users/').$name);
            }
            define('UPLOAD_DIR', 'public/images/local_users/');
            $img = $request->image;
            preg_match("/data:image\/(.*?);/",$img,$image_extension); // extract the image extension
            $img = preg_replace('/data:image\/(.*?);base64,/','',$img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $file = UPLOAD_DIR . str::random(10). '.' . $image_extension[1];
            $imgname =   "http://businessapp.axistc.com/axis/".$file;
            $success = file_put_contents($file, $data);
            $update_data['image'] = $imgname;
        }
        try{
            DB::beginTransaction();
            $company_ext = DB::table('local_users')->where('id',$request->id)->select('companyid')->first();
            if(!in_array($company_ext->companyid,$request->companyid)){
                DB::table('local_users')->where('id',$request->id)->update(["CompanyID"=>$request->companyid[0]]);
            }
            $this->getCompanyConnection($company);
            LocalUsersCompanyAccess::where('userid',$old_data->user_id)->delete();
            $data=DB::table('local_users')->where('id',$id)->update($update_data);
            DB::connection('mysql_source')->table('locationaccess')->where('UserId',$old_data->user_id)->update(["UserId"=>$request->user_id]);
            DB::connection('mysql_source')->table('chainaccess')->where('UserID',$old_data->user_id)->update(['UserID'=>$request->user_id]);
            foreach($request->companyid as $company){
                LocalUsersCompanyAccess::insert([
                    'userid'    => $request->user_id,
                    'companyid' => $company,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
            DB::disconnect('mysql_source');
            return response()->json(['status'=>true,"message"=>"Successfully Updated"]);   
        }catch(\Exception $e){
            // throw $e;
            DB::rollback();
            return response()->json(['status'=>false,"message"=>$e->getMessage()]);  
        }
    }
    
    public function dashboardData(){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $chain=DB::connection('mysql_source')->table('chain')->select(DB::raw('COUNT(ChainId) as chains'))->get();
        $users=DB::table('local_users_company_accesses')->select(DB::raw('COUNT(userid) as users'))->where('companyid',$company)->count();
        $location=DB::connection('mysql_source')->table('location')->select(DB::raw('COUNT(LocationId) as locations'))->get();
        $company = DB::table('company')->where('CompanyID',Auth::user()->companyid)
                    ->select('CompanyID','CompanyName','Address1','Address2','City','Country')->first();
        $company_address = $company->Address1 ." ". $company->Address2 .",". $company->City .",". $company->Country;
        DB::disconnect('mysql_source');
        return response()->json(['chains'=>$chain,'users'=>$users,'locations'=>$location,'company_address'=>$company_address]);
    }
    
    public function costCenterAccessView(Request $request){
        $search = $request->search;
        $company = Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $users_data = DB::table('local_users')->join('local_users_company_accesses','local_users.user_id','=','local_users_company_accesses.userid')
                    ->where('local_users_company_accesses.companyid',$company)->select('local_users.user_id','local_users.username')->get()->unique();
                // return response()->json([$users_data]);exit;
                $users = array();
                foreach($users_data as $row){
                    $ca=DB::connection('mysql_source')->table('chainaccess')->where('UserID',$row->user_id)->first();
                    // return response()->json([$ca]);exit;
                    if($ca){
                        $users[] = $row;
                    }
                }
                // return response()->json([$users]);exit;
        $i=0;$j=0;
        $data=array();
        if($users){
            foreach($users as $user){
                $j=0;
                $data[$i]['userid']=$user->user_id;
                $data[$i]['username']=$user->username;
                $fa=DB::connection('mysql_source')->table('chainaccess')->where('UserID',$user->user_id)->first('fullaccess');
                $data[$i]['fullaccess']=$fa->fullaccess;
                $chains=DB::connection('mysql_source')->table('chainaccess')->where('UserID',$user->user_id)
                            ->select('ChainID')->distinct()->get();
                            // print_r($chains);exit;
                            // return response()->json([$chains]);
                foreach($chains as $chain){
                    $data[$i]['selected'][$j]['Chain Id']=$chain->ChainID;
                    // print_r($data);exit;
                    $locations=DB::connection('mysql_source')->table('locationaccess')->where('UserId',$user->user_id)
                                ->where('ChainId',$chain->ChainID)
                                ->select('locationaccess.LocationId')->get();
                    foreach($locations as $location){
                        $data[$i]['selected'][$j]['location Id'][]=$location->LocationId;
                        // print_r($data);exit;
                    }
                    $j++;
                }$i++;
            }
            $items=collect($data);
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // $pageno=$request->pageno;
            $perpage=10;
            $currentPageItems=$items->slice(($currentPage-$perpage)-$perpage,$perpage)->all();
            $paginatedItems= new LengthAwarePaginator($currentPageItems ,count($items), $perpage);
            $paginatedItems->setPath($request->url());
            // print_r($paginatedItems);exit;
            DB::disconnect('mysql_source');
            return response()->json($paginatedItems);   
        }
        else{
            return response()->json(["message"=>"No Data"]);
        }
        // print_r($data);exit;
    }

    public function addCostCenterAccess(Request $request){
        $companyid  = Auth::user()->companyid;
        $this->getCompanyConnection($companyid);
        $rules=[
            'userid'=>'required',
            'username'=>'required',
            'Fullaccess'=>'required',
        ];
        $messages=[
            'userid.required'=>'user id is required',
            'username.required'=>'user name Id is required',
            'Fullaccess.required'=>'access Id is required',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }
        $userid     = $request->userid;
        $username   = $request->username;
        $fullaccess = $request->Fullaccess;
        if($fullaccess == 1){
            try{
                    DB::beginTransaction();
                    $arraychain=DB::connection('mysql_source')->table('chain')->where('CompanyId','=',$companyid)
                                ->select('ChainId')->get();
                    $i=0;$j=0;
                    foreach($arraychain as $chain){
                        $chainarray[$i]= array(
                            "UserID"        =>$userid,
                            "CompanyId"     =>$companyid,
                            "ChainID"       =>$chain->ChainId,
                            "FullAccess"    =>$fullaccess,
                        );
                        $i=$i+1;
                    }
                    while($i!= 0 && $arraychain!=""){
                        DB::connection('mysql_source')->table('chainaccess')->insert([$chainarray[$j]]);
                        $i--;$j++;
                    }
                     $arrayloc=DB::connection('mysql_source')->table('location')->join('chain','chain.ChainId','=','location.ChainId')
                                ->where('location.CompanyId','=',$companyid)
                                ->select('location.CompanyId','location.LocationId','chain.ChainId')->get();
                    $k=0;$s=0;
                    foreach($arrayloc as $location){
                        $locarray[$k]= array(
                            "UserId"        =>$userid,
                            "CompanyID"     =>$companyid,
                            "ChainId"       =>$location->ChainId,
                            "LocationId"    =>$location->LocationId,
                        );
                        $k=$k+1;
                    }
                    if($arrayloc){
                        while($k> 0){
                            DB::connection('mysql_source')->table('locationaccess')->insert([$locarray[$s]]);
                            $k--;$s++;
                        }
                        DB::commit();
                        DB::disconnect('mysql_source');
                        return response()->json(['status'=>true,"message"=>"Successfully Added"]);      
                    }
            }catch(\Exception $e) {
                DB::rollback();
                // throw $e;
                 return response()->json(['status'=>false,"message"=>$e->getMessage()]);  
                        
            }
        }
        else{
            try{
                    DB::beginTransaction();
                    $data=$request->selected;
            		foreach($data as $chain){
            		        $chains=$chain['chain'];
            		        $chainins=array(
                		            "UserID"        =>$userid,
                                    "CompanyId"     =>$companyid,
                                    "ChainID"       =>$chains,
                                    "FullAccess"    =>$fullaccess,
                            );
            		        DB::connection('mysql_source')->table('chainaccess')->insert([$chainins]);
            		        $locations=$chain['locations'];
            		        foreach($locations as $loc){
            		            $locins=array(
                		            "UserId"        =>$userid,
                                    "CompanyID"     =>$companyid,
                                    "ChainId"       =>$chains,
                                    "LocationId"    =>$loc,
            		            );
            		            DB::connection('mysql_source')->table('locationaccess')->insert([$locins]);
            		        }
            	}
                DB::commit();
                DB::disconnect('mysql_source');
                return response()->json(['status'=>true,"message"=>"Successfully Added"]);
            }catch(\Exception $e){
                DB::rollback(); 
                // throw $e;
                return response()->json(['status'=>false,"message"=>$e->getMessage()]);  
            }
        }
    }
    
    public function updateCostCenterAccess(Request $request){
        $companyid  = Auth::user()->companyid;
        $this->getCompanyConnection($companyid);
        $userid     = $request->userid;        
        $fullaccess = $request->Fullaccess;
        if($fullaccess== 1){
            
            try{    
                    DB::beginTransaction();
                    DB::connection('mysql_source')->table('locationaccess')->where('UserId',$userid)
                        ->where('CompanyID',$companyid)
                        ->delete();
                    DB::connection('mysql_source')->table('chainaccess')->where('UserID',$userid)
                        ->where('CompanyId',$companyid)
                        ->delete();
                    $arraychain=DB::connection('mysql_source')->table('chain')->where('CompanyId','=',$companyid)
                                ->select('ChainId')->get();
                    $i=0;$j=0;
                    foreach($arraychain as $chain){
                        $chainarray[$i]= array(
                            "UserId"        =>$userid,
                            "CompanyId"     =>$companyid,
                            "ChainID"       =>$chain->ChainId,
                            "FullAccess"    =>$fullaccess,
                        );
                        $i=$i+1;
                    }
                    while($i!= 0 && $arraychain!=""){
                        DB::connection('mysql_source')->table('chainaccess')->insert([$chainarray[$j]]);
                        $i--;$j++;
                    }
                    $arrayloc=DB::connection('mysql_source')->table('location')->where('CompanyId','=',$companyid)
                                ->select('LocationId','ChainId')->get();
                    $k=0;$s=0;
                    foreach($arrayloc as $location){
                        $locarray[$k]= array(
                            "UserId"        =>$userid,
                            "CompanyID"     =>$companyid,
                            "ChainId"       =>$location->ChainId,
                            "LocationId"    =>$location->LocationId,
                        );
                        $k=$k+1;
                    }
                    while($k!= 0 && $arrayloc!=""){
                        DB::connection('mysql_source')->table('locationaccess')->insert([$locarray[$s]]);
                        $k--;$s++;
                    }
                    DB::commit();
                    DB::disconnect('mysql_source');
                    return response()->json(['status'=>true,"message"=>"Successfully Updated"]);        
            }
            catch(\Exception $e) {
                DB::rollback();
                return response()->json(['status'=>false,"message"=>$e->getMessage()]); 
                    // throw $e;
            }
        }
        else{
           DB::beginTransaction();
            try{
                    $data = $request->selected;
                    DB::beginTransaction();
                	DB::connection('mysql_source')->table('locationaccess')->where('UserId',$userid)
                        ->where('CompanyID',$companyid)
                        ->delete();
                    DB::connection('mysql_source')->table('chainaccess')->where('UserID',$userid)
                        ->where('CompanyId',$companyid)
                        ->delete();
            		foreach($data as $chain){
            		        $chains=$chain['chain'];
            		        DB::connection('mysql_source')->table('chainaccess')
                                ->insert([
                                            "UserID"        =>$userid,
                                            "CompanyId"     =>$companyid,
                                            "ChainID"       =>$chains,
                                            "FullAccess"    =>$fullaccess,
                                ]);
            		      $locations=$chain['locations'];
            		      if(!empty($locations)){
            		        foreach($locations as $loc){
            		            DB::connection('mysql_source')->table('locationaccess')
                                    ->insert([
                                        "UserId"        =>$userid,
                                        "CompanyID"     =>$companyid,
                                        "ChainId"       =>$chains,
                                        "LocationId"    =>$loc,
                                    ]);   
            		        }   
            		      }
                    }      
                    DB::commit();
                    DB::disconnect('mysql_source');
                    return response()->json(['status'=>true,"message"=>"Successfully Updated"]);
            }catch(\Exception $e){
                DB::rollback();
                // throw $e;
                return response()->json(['status'=>false,"message"=>$e->getMessage()]); 
            }
        }
    }

    public function deleteCostCenterAccess(Request $request){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $userid = $request->userid;
        try{
            DB::beginTransaction();
            DB::connection('mysql_source')->table('locationaccess')->where('UserId',$userid)->delete();
            DB::connection('mysql_source')->table('chainaccess')->where('UserID',$userid)->delete();
            DB::commit();
            DB::disconnect('mysql_source');
            return response()->json(['status'=>true,"message"=>"Successfully Deleted"]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>false,"message"=>$e->getMessage()]); 
        }
    }
     
    // --------------------Lists----------------------------------------
     public function companyList(Request $request){
        if($request->userid){
            $data = DB::table('usercompanyaccess')->where('UserID',$request->userid)->get();
            return response()->json(['status'=>true,'comapny_id'=>$data]);
        }else{
            return response()->json(['status'=>false,'message'=>"Something went wrong."]); 
        }
    }
    
    public function chainlessUsers(Request $request){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $company_users = DB::table('local_users')->join('local_users_company_accesses','local_users.user_id','=','local_users_company_accesses.userid')
                    ->where('local_users_company_accesses.companyid',$company)
                    ->select('local_users.user_id','local_users.username','local_users.user_name')
                    ->get()->unique();
        $users = array();
        foreach($company_users as $user){
            $ext_data = DB::connection('mysql_source')->table('chainaccess')->where('UserID',$user->user_id)->first();
            if(!$ext_data){
                $users[] = $user;
            }
        }
        DB::disconnect('mysql_source');
        return response()->json(['users'=>$users]);
    }    
        
    public function chainAndLocationList(Request $request){
        $company    =   Auth::user()->companyid;
        $i=0;
        $this->getCompanyConnection($company);
        $chains=DB::connection('mysql_source')->table('chain')
                ->select("ChainId",'ChainName')->where('CompanyId',$company)->get();
        $data=array();
        foreach($chains as $chain){
            $data[$i]['chainid']=$chain->ChainId;
            $data[$i]['chainname']=$chain->ChainName;
            $locations=DB::connection('mysql_source')->table('location')
                        ->select('LocationId','LocationName')
                        ->where('ChainId',$chain->ChainId)
                        ->get();
            foreach($locations as $location){
                $data[$i]['locations'][]=array("id"=>$location->LocationId,'Name'=>$location->LocationName);
            }
            $i++;
        }
        DB::disconnect('mysql_source');
        return response()->json($data);
    }
    
    public function locationList(){
        $company    =   Auth::user()->companyid;
        $this->getCompanyConnection($company);
        $locations = DB::connection('mysql_source')->table('location')
                    ->select('location_code','location_name')
                    ->get();
        DB::disconnect('mysql_source');
        return response()->json(['locationlist'=>$locations]);
    }
    
}
