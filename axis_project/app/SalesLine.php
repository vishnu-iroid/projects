<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\SalesLine;

class SalesLine extends Model
{
    //
    protected $table= 'sales_line';

    protected $fillable = [ 'CompanyID', 'ChainID', 'LocationID', 'VoucherDate', 'VoucherNo', 'EmployeeID', 'OrderNo', 'ProductID',
    'ItemDesc', 'CategoryID1', 'CategoryDes1', 'CategoryID2', 'CategoryDes2', 'SellingPrice', 'Qty', 'SalesStatus' ];

    public $timestamps = false;
}
