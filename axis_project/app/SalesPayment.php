<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesPayment extends Model
{
    //
    protected $table= 'sales_payment';

    protected $fillable = [ 'CompanyID', 'ChainID', 'LocationID', 'EmployeeID', 'OrderTakenBy', 'VoucherNo', 'VoucherDate', 'OrderNo',
    'CustomerName', 'PaymentType', 'Amount', 'CardNo', 'TenderAmount' ];

    public $timestamps = false;
}
