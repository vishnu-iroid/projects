<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesHeader extends Model
{
    //
    protected $table= 'sales_header';

    protected $fillable = [ 'CompanyID', 'ChainID', 'LocationID', 'VoucherNo', 'VoucherDate', 'OrderNo', 'OrderType', 'EmployeeID',
    'VoucherStatus', 'SalesAmount', 'VoidCount', 'VoidAmount', 'GuestCount', 'StartTime', 'Discount', 'SalesTaxAmount', 'ServiceTaxAmount',
    'SalesTaxBaseAmount', 'ServiceTaxBaseAmount', 'NetSalesAfterDiscountAndTax' ];

    public $timestamps = false;
}
