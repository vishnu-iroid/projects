<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    //
    protected $fillable = [ 'CompanyID', 'ChainID', 'PaymentType', 'IsCreditCard', 'LoginGroupID', 'IsDiscountCard', 'DisplayOrder', 'IsCCard',
    'IsStaffMeal', 'IsOtherStaffMeal', 'Active', 'OrderTypes', 'IsSwipeCard', 'MaxDiscount', 'IsCustCredit', 'IsCOTDiscount', 'MinDiscount' ];

    public $timestamps = false;
}
