<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('registerform',[
//     'uses' => 'Controller@registerForm',
//   ], function () {
//      header('Access-Control-Allow-Origin: *');
//      header('Access-Control-Allow-Headers: Origin, Content-Type');
//   }); //product content

// Route::post('admin/login','Controller@adminLogin')->name('adminLogin');
// Route::post('admin/logout','Controller@adminLogout')->name('adminLogout');

Route::post('user-admin/login','UserController@secuserLogin')->name('secuserlogin');
Route::post('user-admin/logout','UserController@secuserLogout')->name('UserLogout');

Route::post('superadmin/login','Controller@superAdminLogin')->name('UserLogin');
Route::post('user/superadmin/secondlogin','UserController@superAdminSecondLogin');

Route::middleware('auth:api')->group( function () {

        Route::middleware('CheckRole:admin')->group( function () {

                Route::post('admin/dashboard','Controller@adminDashboardData');
                 Route::post('admin/change-password','Controller@changePasswordSuperAdmmin');

                Route::get('admin/tabledata','Controller@userTableDetails')->name('userTableDetails');
                Route::get('admin/company','Controller@companyTableDetails')->name('companyTableDetails');

                Route::post('admin/update/company','Controller@updateComp')->name('updateComp');
                Route::post('admin/update/user','Controller@updateUser')->name('updateUser');
                Route::post('admin/register/user','Controller@register_user')->name('register_user');
                Route::post('admin/register/company','Controller@register_company')->name('register_company');
                Route::post('admin/delete/company','Controller@deleteCompany')->name('deleteCompany');
                Route::post('admin/delete/user','Controller@deleteUser')->name('deleteUser');

                Route::get('admin/view/CompanyAccess','Controller@companyAccesstable');
                Route::post('admin/update/CompanyAccess','Controller@updateCompanyAccess');
                Route::post('admin/delete/CompanyAccess','Controller@deleteCompanyAccess');
                Route::post('admin/companyList','Controller@companyList');

                Route::post('admin/currencyreg','Controller@regCurrency');
                Route::get('admin/currencylist','Controller@currencyList');
                Route::post('admin/currencydelete','Controller@currencyDelete');
                Route::post('admin/currencyedit','Controller@currencyEdit');
                Route::post('admin/currencydrop','Controller@currencyDropDown');

                // Route::post('admin/user/search','Controller@userSearch');
                // Route::post('admin/company/search','Controller@companySearch');
                // Route::post('admin/currency/search','Controller@currencySearch');

                Route::post('admin/enquiries','EnquiryController@getEnquiry');
                Route::post('admin/enquiries-delete','EnquiryController@deleteEnquiry');

                Route::post('admin/app-users-registration','Controller@getUsersAppReg');
                Route::post('admin/app-users-registration-action','Controller@UsersAppRegAction');

                Route::post('admin/faq','FaqController@getFaq');
                Route::post('admin/faq-add','FaqController@addFaq');
                Route::post('admin/faq-edit','FaqController@editFaq');
                Route::post('admin/faq-delete','FaqController@deleteFaq');
        });


        Route::middleware('CheckRole:user')->group( function () {


                Route::post('user/dashboard','UserController@dashboardData');

                Route::get('user/view/chains','UserController@viewChains');
                Route::post('user/add/chain','UserController@addChain')->name('secuser');
                Route::post('user/delete/chain','UserController@deleteChain')->name('secuser');
                Route::post('user/update/chain','UserController@updateChain')->name('secuser');

                Route::get('user/view/locations','UserController@viewLocations');
                Route::post('user/add/location','UserController@addLocation')->name('secuser');
                Route::post('user/delete/location','UserController@deleteLocation')->name('secuser');
                Route::post('user/update/location','UserController@updateLocation')->name('secuser');

                Route::get('user/view/localusers','UserController@viewLocalUsers');
                Route::post('user/add/user','UserController@addLocalUser')->name('secuser');
                Route::post('user/delete/user','UserController@deleteLocalUser')->name('secuser');
                Route::post('user/update/user','UserController@updateLocalUser')->name('secuser');

                Route::get('user/view/costcenteraccess','UserController@costCenterAccessView');
                Route::post('user/add/costcenteraccess','UserController@addCostCenterAccess');
                Route::post('user/update/costcenteraccess','UserController@updateCostCenterAccess')->name('secuser');
                Route::post('user/delete/costcenteraccess','UserController@deleteCostCenterAccess')->name('secuser');

                Route::post('user/chainlist','UserController@chainAndLocationList');
                Route::post('user/locationlist','UserController@locationList');
                Route::post('user/chainless-users','UserController@chainlessUsers');

                Route::post('user/company-session','UserController@getCompany');

                Route::post('user/company-list','UserController@companyList');
        });


});
//Route::middleware('auth:userapp')->group( function () {
        Route::post('userdata','UserDetailsController@userdata')->name('userdata');
        Route::post('userdata_testing','UserDetailsController@userdata_testing')->name('userdata_testing');
        Route::post('userdata_testing1','UserDetailsController@userdata_testing1')->name('userdata_testing1');
        Route::post('checkcompany','UserDetailsController@checkcompany')->name('checkcompany');
        Route::post('all_data','UserDetailsController@all_data')->name('all_data');
        Route::post('all_data_testing','UserDetailsController@all_data_testing')->name('all_data_testing');
        Route::post('all_data_testing1','UserDetailsController@all_data_testing1')->name('all_data_testing1');
        Route::post('userdataonchange','UserDetailsController@userdataonchange')->name('userdataonchange');
        Route::post('userdataonchange_testing','UserDetailsController@userdataonchange_testing')->name('userdataonchange_testing');
        Route::post('all_dataonchange','UserDetailsController@all_dataonchange')->name('all_dataonchange');
        Route::post('all_dataonchange_testing','UserDetailsController@all_dataonchange_testing')->name('all_dataonchange_testing');
        Route::post('all_dataonchange_testing1','UserDetailsController@all_dataonchange_testing1')->name('all_dataonchange_testing1');
        Route::post('all_dataonchange_testing2','UserDetailsController@all_dataonchange_testing2')->name('all_dataonchange_testing2');
        Route::post('new_user','UserDetailsController@new_user')->name('new_user');
        Route::post('appLogin','UserDetailsController@appLogin')->name('appLogin');
        Route::post('googleLogin','UserDetailsController@googleLogin')->name('googleLogin');
        Route::post('view_profile','UserDetailsController@view_profile')->name('view_profile');
        Route::post('edit_profile','UserDetailsController@edit_profile')->name('edit_profile');
        Route::post('edit_email','UserDetailsController@edit_email')->name('edit_email');
        Route::post('edit_password','UserDetailsController@edit_password')->name('edit_password');
        Route::post('view_faq','UserDetailsController@view_faq')->name('view_faq');
        Route::post('get_company','UserDetailsController@get_company')->name('get_company');
        Route::post('edit_company','UserDetailsController@edit_company')->name('edit_company');
        Route::post('enquiry','UserDetailsController@enquiry')->name('enquiry');
        Route::post('forgotPassword','UserDetailsController@forgotPassword')->name('forgotPassword');
        Route::post('verifyOtp','UserDetailsController@verifyOtp')->name('verifyOtp');
        Route::post('resetPassword','UserDetailsController@resetPassword')->name('resetPassword');
//});
        /************************************* New API ******************************/
    Route::middleware('api_token')->group( function () {

        // Route::post('sales-header','SaleController@SalesHeader');
        // Route::post('sales-line','SaleController@salesLine');
        // Route::post('sales-payment','SaleController@salesPayment');
        // Route::post('test-api','SaleController@testApi');
        Route::delete('delete-payment', 'SaleController@deletePayment');
        // Route::put('update-sales-line', 'SaleController@updateSalesLine');
        // Route::put('update-sales-header', 'SaleController@updateSalesHeader');
        Route::put('update-transaction-by-date', 'SaleController@updateTransactionByDate');
        Route::delete('delete-transaction-by-date', 'SaleController@deleteTransactionByDate');
        Route::post('sales-payment-type', 'SaleController@salesPaymentType');
        Route::delete('delete-payment-type', 'SaleController@deletePaymentType');
        Route::post('sales-transaction', 'SaleController@salesTransaction');
    });
    Route::post('sales-all-transactions', 'SaleController@salesAllTransactions');
    Route::post('sales-payment-types', 'SaleController@salesPaymentTypes');


